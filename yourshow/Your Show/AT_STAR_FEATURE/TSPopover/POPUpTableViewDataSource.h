
#import <Foundation/Foundation.h>
#import "FeatureModel.h"


@class POPupTableView;

@protocol POPUpTableViewDataSource <NSObject>

@optional

@required

- (NSInteger)rowsForBubbleTable:(POPupTableView *)tableView;
- (FeatureModel*)bubbleTableView:(POPupTableView *)tableView dataForRow:(NSInteger)row;

@end


