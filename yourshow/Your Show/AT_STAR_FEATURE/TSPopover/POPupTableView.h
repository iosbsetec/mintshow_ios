//
//  POPupTableView.h
//  YourShowLabel
//
//  Created by Siba Prasad Hota on 02/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POPUpTableViewDataSource.h"
#import "EntityModel.h"

@protocol POPUpTableDelegate;


@interface POPupTableView : UITableView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic,assign)EntityType suggestType;


@property (nonatomic, assign) IBOutlet id<POPUpTableViewDataSource> popupDataSource;
@property (nonatomic, assign) IBOutlet id<POPUpTableDelegate> popupDelegate;
@property(nonatomic, strong) UITableView *ptableView;
@property(assign)BOOL isFromKeyword;

@end



@protocol POPUpTableDelegate
- (void) PopupTableSelectedIndex :(NSInteger) selectedRowNo;
@end