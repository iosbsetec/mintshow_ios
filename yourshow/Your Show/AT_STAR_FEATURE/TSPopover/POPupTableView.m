//
//  POPupTableView.m
//  YourShowLabel
//
//  Created by Siba Prasad Hota on 02/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "POPupTableView.h"



static NSString *Cell_Identifier        = @"Cell_Identifier";


@implementation POPupTableView

- (void)initializator
{
    // UITableView properties
    self.backgroundColor = [UIColor whiteColor];
    self.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    assert(self.style == UITableViewStylePlain);
    
    self.delegate = self;
    self.dataSource = self;
    
}

- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) [self initializator];
    return self;
}



- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    [super reloadRowsAtIndexPaths:indexPaths withRowAnimation:animation];
}

#pragma mark - UITableViewDelegate implementation

#pragma mark - UITableViewDataSource implementation

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return   (!_isFromKeyword)?36.0:25;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.popupDataSource rowsForBubbleTable:self ];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.popupDelegate PopupTableSelectedIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
-(UIView *)creatorDevider
{
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];/// change size as you need.
    separatorLineView.backgroundColor = BACKGROUNDCOLOR;// you can also put image here
    
    return separatorLineView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeatureModel *dataDict = [self.popupDataSource bubbleTableView:self dataForRow:indexPath.row];
    UITableViewCell *cell  = (UITableViewCell *)[self.ptableView dequeueReusableCellWithIdentifier:Cell_Identifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell_Identifier];
    }
    
    // Set first cell background color as orange
//
    if (!_isFromKeyword)
    {
        cell.backgroundColor  = (indexPath.row==0)?[UIColor colorWithRed:237.0/255.0 green:71.0/255.0 blue:22.0/255.0 alpha:0.9]:[UIColor clearColor];
    }
    else
    {
        tableView.separatorColor = [UIColor clearColor];
        cell.backgroundColor  = [UIColor clearColor];
    }
    cell.selectionStyle   = UITableViewCellSelectionStyleNone;
    CGFloat widthString   = [YSSupport getStringWidth:dataDict.name];
    
    CGFloat txtLableWidth = (widthString>170)?170:widthString;

    if (dataDict.imgUrl) {
        
  
    UIImageView *imageView       = [[UIImageView alloc] init];
    [imageView setFrame:CGRectMake(5, 2, 30, 30)];
    imageView.mintImageURL           = [NSURL URLWithString:dataDict.imgUrl];
    imageView.backgroundColor    = [UIColor lightGrayColor];
    imageView.layer.cornerRadius = 30/2;
    [imageView setClipsToBounds:YES];
    [cell addSubview:imageView];
    }
    

//    
//    if ([[dataDict imgUrl] length]) {
// 
//       UIImageView *imgShowrromType = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width - 20, 13, 10, 10)];
//    if (![dataDict.type isEqualToString:@"anyone"] && !dataDict.is_member && !dataDict.is_private) {
//        imgShowrromType.image    = [UIImage imageNamed:@"Lock_Icon.png"];
//    }
//    else  if (dataDict.is_private) {
//            imgShowrromType.image       = [UIImage imageNamed:@"key_icon_gray"];
//    }
//
//        [cell.contentView addSubview:imgShowrromType];
//    }
    
    if ([dataDict.type length]) {
        
        UIImageView *imgShowrromType = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width - 20, 13, 10, 10)];
        if ([dataDict.type isEqualToString:@"public_open"]) {
            imgShowrromType.image       = [UIImage imageNamed:@"Star_Icon.png"];
        }
        else  if ([dataDict.type isEqualToString:@"private"]) {
            imgShowrromType.image       = [UIImage imageNamed:@"key_icon_gray"];
        }
        else
        {
            imgShowrromType.image    = [UIImage imageNamed:@"Lock_Icon.png"];
            UIImageView *addShowroom = [[UIImageView alloc]initWithFrame:CGRectMake(txtLableWidth+45, 13, 10, 10)];
            addShowroom.image        = [UIImage imageNamed:(indexPath.row==0)?@"Addshowroom_grey":@"Addshowroom"];
            [self bringSubviewToFront:addShowroom];
            [cell.contentView addSubview:addShowroom];
        }
        
        
        [cell.contentView addSubview:imgShowrromType];
    }
    
    
    
    
    UILabel *ttitle      = [[UILabel alloc] initWithFrame:CGRectMake((dataDict.imgUrl)?40:((!_isFromKeyword)?20:10), 8,txtLableWidth, 20)];
    ttitle.font          = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    ttitle.textColor     = (!_isFromKeyword)?((indexPath.row==0)?[UIColor whiteColor]:[UIColor darkGrayColor]):[UIColor darkGrayColor];
    ttitle.textAlignment = NSTextAlignmentLeft;
    [ttitle setText:dataDict.name];
    
    [cell.contentView addSubview:ttitle];
    return cell;
    
}


@end
