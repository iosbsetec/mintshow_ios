//
//  EntitySupport.m
//  YourShowLabel
//
//  Created by Siba Prasad Hota on 02/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "EntitySupport.h"
#import "FeatureModel.h"


@implementation EntitySupport


+(NSString *)scanAndConvert:(NSString*)tempString
{
    NSRange r1 = [tempString rangeOfString:@"["];
    NSRange r2 = [tempString rangeOfString:@"]"];
    
    if (r1.location == NSNotFound || r2.location ==NSNotFound || r1.location>r2.location)
        return tempString;
    

    
    @try {
        NSRange rSub = NSMakeRange(r1.location, r2.location - r1.location - r1.length+2);
        NSString *sub = [tempString substringWithRange:rSub];
        sub = [sub stringByReplacingOccurrencesOfString:@"[" withString:@"<"];
        sub = [sub stringByReplacingOccurrencesOfString:@"]" withString:@">"];
        sub = [sub stringByReplacingOccurrencesOfString:@" " withString:@"|"];
        
        tempString = [tempString stringByReplacingCharactersInRange:rSub withString:sub];
        if ([tempString rangeOfString:@"["].location == NSNotFound)
            return tempString;
        else
            return [self scanAndConvert:tempString];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }

}


+(NSMutableArray *)returnFromServerText:(NSString*)str
{
    NSMutableArray *entities = [NSMutableArray new];
    NSString  *tempstr = [self scanAndConvert:str];
    NSArray *allStringArray = [tempstr componentsSeparatedByString:@" "];
   
 
    NSInteger startrange =0;
    for (int i=0;i<allStringArray.count;i++)
    {
        NSString *aString = [allStringArray objectAtIndex:i];
        if ([aString rangeOfString:@">("].location == NSNotFound)
        {
            EntityModel *entity = [EntityModel new];
            entity.name = aString;
            entity.entityType= EntityTypeNormalText;
            entity.start = startrange;
            entity.end =  entity.name.length;
            startrange =  startrange + entity.name.length+1;
            if(entity.name.length) [entities addObject:entity];
        }
        else
        {
            aString = [aString stringByReplacingOccurrencesOfString:@"<" withString:@""];
            EntityModel *entity = [EntityModel new];
            NSString *firstLetter = [aString substringToIndex:1];
            if ([firstLetter isEqualToString:@"@"])
                entity.entityType= EntityTypeMention;
            else if ([firstLetter isEqualToString:@"#"])
                entity.entityType= EntityTypeHashTag;
            else if ([firstLetter isEqualToString:@"*"])
                entity.entityType= EntityTypeshowroom;
            else if ([firstLetter isEqualToString:@"√"])
                entity.entityType= EntityTypeUserName;
            
            
            NSArray *mentionArray = [aString componentsSeparatedByString:@">("] ;
            NSString *ContactString = [mentionArray objectAtIndex:1];
            NSArray *ContactArray = [ContactString componentsSeparatedByString:@":"] ;
            NSString *IdString = [ContactArray objectAtIndex:1];
            entity.name = [[mentionArray objectAtIndex:0] stringByReplacingOccurrencesOfString:@"|" withString:@" "];
            entity.idString = [IdString stringByReplacingOccurrencesOfString:@")" withString:@""];
            if ([firstLetter isEqualToString:@"√"])
            {
                entity.name = [entity.name stringByReplacingOccurrencesOfString:@"√" withString:@" "];
                entity.start = startrange;
                entity.end =  entity.name.length;
                startrange =  startrange + entity.name.length+1;
            }
            else
            {
                entity.start = startrange;
                entity.end =  entity.name.length;
                startrange =  startrange + entity.name.length+1;
            }
           
            if(entity.name.length)[entities addObject:entity];
        }
    }
    return entities;
}


+(NSMutableArray*)showRoomDataWithDictioNary:(NSDictionary*)dictionary
{
    NSMutableArray *showroomDataArray = [NSMutableArray new];
    for (NSDictionary *userData in [dictionary valueForKey:@"showroom_list"])
    {
        FeatureModel *feature = [FeatureModel new];
        feature.name          = [userData valueForKey:@"showroom_tag"];
        feature.ct_id         = [userData valueForKey:@"showroom_id"];
        feature.imgUrl        = [userData valueForKey:@"showroom_img_small"];
        feature.type          = [userData valueForKey:@"privacy_type"];
        feature.is_member     = ([[userData valueForKey:@"is_member"] isEqualToString:@"no"])?NO:YES;
        feature.is_private    = ([[userData valueForKey:@"privacy_settings"] isEqualToString:@"off"])?NO:YES;
        
        [showroomDataArray addObject:feature];
    }
    return showroomDataArray;
}


+(NSMutableArray*)TagDataWithDictioNary:(NSDictionary*)dictionary
{
    NSMutableArray *tagDataArray = [NSMutableArray new];
    NSDictionary *someDict       = [dictionary valueForKey:@"success_message"];
    for (NSDictionary *userData in [someDict valueForKey:@"hashtag_list"])
    {
        FeatureModel *feature = [FeatureModel new];
        feature.name          = [userData valueForKey:@"name"];
        feature.ct_id         = [userData valueForKey:@"id"];
        feature.imgUrl        = nil;
        feature.type          = nil;

        [tagDataArray addObject:feature];
    }
    return tagDataArray;
}

+(NSMutableArray*)UserlistDataWithDictioNary:(NSDictionary*)dictionary
{
    NSMutableArray *userDataArray = [NSMutableArray new];
    NSDictionary *someDict        = [dictionary valueForKey:@"success_message"];
    for (NSDictionary *userData in [someDict valueForKey:@"users_list"])
    {
        NSString *fulName     = [userData valueForKey:@"name"];
        FeatureModel *feature = [FeatureModel new];
        feature.name          = fulName;
        feature.ct_id         = [userData valueForKey:@"ID"];
        feature.imgUrl        = [userData valueForKey:@"image_url"];
        feature.type          = nil;


        [userDataArray addObject:feature];
    }
    return userDataArray;
}


@end
