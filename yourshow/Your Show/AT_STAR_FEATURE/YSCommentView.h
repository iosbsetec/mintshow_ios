//
//  YSCommentView.h
//  YourShowLabel
//
//  Created by Siba Prasad Hota on 02/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EntityModel.h"


@protocol textSuggestDelegate;


@interface YSCommentView : UIView

@property (nonatomic) EntityType suggestionType;
@property (nonatomic, assign) BOOL textSelectable;
@property (nonatomic, assign)  NSInteger  counterCount;
@property (strong) UITextView *textView;

@property (nonatomic, strong) UIColor *selectionColor;
@property (nonatomic,copy)  void (^shouldBeginSuggestingBlock)(void);
@property (nonatomic,copy)  void (^shouldReloadSuggestionsBlock)(EntityType suggestionType, NSString *suggestionQuery, NSRange suggestionRange);
@property (nonatomic,copy)  void (^shouldEndSuggestingBlock)(void);

@property (nonatomic,strong) id <textSuggestDelegate> textDelegate;

-(void)setTextAllignment:(NSTextAlignment)allignment;
-(void)resetTextView;
-(NSString *)getServerStringForFinalOutPut;
-(void)setViewText:(NSString*)atext;
-(void)setIneraction:(BOOL)interaction;
- (id)initWithFrame:(CGRect)frame;
-(void)wordSelectedForSuggestType:(EntityType)textSuggestion  AndString:(NSString*)fullWord andID:(NSString*)contactID  forRange:(NSRange)range;
-(void)appandAStringWithEntityType:(EntityType)textSuggestion AndString:(NSString*)fullWord andID:(NSString*)contactID;

-(NSMutableArray *)getFeatureArrayFOrPostMint;
-(NSMutableArray *)getShowroomArrayFOrPostMint;
- (void)determineYSWordsForAddmint:(NSString *)strText
;


@end


@protocol textSuggestDelegate

- (void)beginSuggestingForTextView:(EntityType)textSuggestType query:(NSString *)suggestionQuery Inrange:(NSRange)range;
- (void)endSuggesting;
- (void)reloadSuggestionsWithType:(EntityType)suggestionType query:(NSString *)suggestionQuery range:(NSRange)suggestionRange;
- (void)wordDeletedForSuggestType:(EntityType)textSuggestion andID:(NSString*)contactID;
- (void)textDidChange:(YSCommentView*)commentView;
@end



