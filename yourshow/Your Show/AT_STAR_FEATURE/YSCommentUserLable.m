//
//  YSCommentUserLable.m
//  Your Show
//
//  Created by JITENDRA on 11/4/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "YSCommentUserLable.h"
#import "NSString+YaacMention.h"
#import "EntitySupport.h"





@interface YSCommentUserLable () <UITextViewDelegate>

@property (nonatomic, strong) NSRegularExpression *urlRegex;

@property (strong) NSTextStorage *textStorage;
@property (strong) NSLayoutManager *layoutManager;
@property (strong) NSTextContainer *textContainer;

@property (nonatomic, strong) NSString *cleanText;
@property (nonatomic, strong) NSString *uncleanText;


@property (nonatomic, copy) NSAttributedString *cleanAttributedText;

@property (strong) NSMutableArray *rangesOfYSWords;

@property (nonatomic, strong) NSDictionary *attributeUserName;
@property (nonatomic, strong) NSDictionary *attributesText;
@property (nonatomic, strong) NSDictionary *attributesMention;
@property (nonatomic, strong) NSDictionary *attributesHashtag;
@property (nonatomic, strong) NSDictionary *attributesShowroom;
@property (nonatomic, strong) NSDictionary *attributesURL;

@property (strong) UITextView *textView;

@end


@implementation YSCommentUserLable
{
    BOOL _isTouchesMoved;
    NSRange _selectableRange;
    NSInteger _firstCharIndex;
    CGPoint _firstTouchLocation;
}


#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame withFont : (CGFloat ) fontSize  attributeTextColor : (UIColor *) attributeTextColor {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setupLabel:fontSize attributeTextColor:attributeTextColor];
        [self setupTextView];
    }
    
    return self;
}



-(void)setAllignmentForLabel:(NSTextAlignment)allignment{
    _textView.textAlignment = allignment;
}
-(void)setAttributeForfont:(UIFont *)font
{

    NSDictionary *orangeAttribute = @{NSForegroundColorAttributeName: [UIColor colorWithRed:237.0/255.0 green:71.0/255.0 blue:22.0/255.0 alpha:0.9], NSFontAttributeName: font};
    _attributesText = @{NSForegroundColorAttributeName: [[UIColor blackColor] colorWithAlphaComponent:.8], NSFontAttributeName: font};
    _attributesMention  = orangeAttribute;
    _attributesHashtag  = orangeAttribute;
    _attributesShowroom = orangeAttribute;


}

-(void)setAttributeForTextWithType:(EntityType)type color:(UIColor *)color font:(UIFont *)font isforAll:(BOOL)forAll

{

    NSDictionary *CustomAttributeAttribute = @{NSForegroundColorAttributeName: color, NSFontAttributeName: font};
    
    if (forAll) {
        _attributesMention  = CustomAttributeAttribute;
        _attributesHashtag  = CustomAttributeAttribute;
        _attributesShowroom = CustomAttributeAttribute;
        _attributesURL      = CustomAttributeAttribute;
        _attributeUserName  = CustomAttributeAttribute;
        return;
    }
    switch (type) {
        case EntityTypeMention:
            _attributesMention = CustomAttributeAttribute;
            break;
        case EntityTypeHashTag:
            _attributesHashtag = CustomAttributeAttribute;
            break;
        case EntityTypeNormalText:
            _attributesText = CustomAttributeAttribute;
            break;
        case EntityTypeshowroom:
            _attributesShowroom = CustomAttributeAttribute;
            break;
        case EntityTypeUrl:
            _attributesURL = CustomAttributeAttribute;
            break;
        case EntityTypeUserName:
            
            _attributeUserName = CustomAttributeAttribute;
            break;
            
        default:
            break;
    }

}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setupLabel:11.0 attributeTextColor:[UIColor blackColor]];
        [self setupTextView];
    }
    return self;
}


- (void)setupTextView {
    
    _textStorage   = [NSTextStorage new];
    _layoutManager = [NSLayoutManager new];
    _textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)];
    
    [_layoutManager addTextContainer:_textContainer];
    [_textStorage addLayoutManager:_layoutManager];
    
    _textView = [[UITextView alloc] initWithFrame:self.bounds textContainer:_textContainer];
    _textView.delegate                          = self;
    _textView.autoresizingMask                  = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _textView.backgroundColor                   = [UIColor clearColor];
    _textView.textContainer.lineFragmentPadding = 0;
    _textView.textContainerInset                = UIEdgeInsetsZero;
    _textView.userInteractionEnabled            = NO;
    [self addSubview:_textView];
}



#pragma mark - Responder

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return (action == @selector(copy:));
}

- (void)copy:(id)sender {
    [[UIPasteboard generalPasteboard] setString:[_cleanText substringWithRange:_selectableRange]];
    
    @try {
        [_textStorage removeAttribute:NSBackgroundColorAttributeName range:_selectableRange];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}

#pragma mark - Setup

- (void)setupLabel : (CGFloat) fontSize attributeTextColor : (UIColor *) attributeColor {
    // Set the basic properties
    [self setBackgroundColor:[UIColor clearColor]];
    [self setClipsToBounds:NO];
    [self setUserInteractionEnabled:YES];
    [self setNumberOfLines:0];
    
    _textSelectable = YES;
    _selectionColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    NSDictionary *orangeAttribute = @{NSForegroundColorAttributeName: [UIColor colorWithRed:237.0/255.0 green:71.0/255.0 blue:22.0/255.0 alpha:0.9], NSFontAttributeName: [UIFont fontWithName:@"Helvetica Bold" size:fontSize]};
    NSDictionary *userNameAttribute = @{NSForegroundColorAttributeName: [UIColor blackColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica Bold" size:fontSize]};
    
    _attributeUserName = userNameAttribute;
    _attributesMention=  orangeAttribute;
    _attributesHashtag=  orangeAttribute;
    _attributesShowroom= orangeAttribute;
    
    _attributesText = @{NSForegroundColorAttributeName: [attributeColor colorWithAlphaComponent:.8], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:fontSize]};
    
}




- (void)determineYSWords
{
    // Need a text
    if (_cleanText == nil)
        return;
    _rangesOfYSWords = [EntitySupport returnFromServerText:_uncleanText];
    [self updateText];
}

- (void)updateText
{
    [_textStorage beginEditing];
    NSAttributedString *attributedString = _cleanAttributedText ?: [[NSMutableAttributedString alloc] initWithString:_cleanText];
    [_textStorage setAttributedString:attributedString];
    [_textStorage setAttributes:_attributesText range:NSMakeRange(0, attributedString.length)];
    for (EntityModel *entity in _rangesOfYSWords)
    {
        NSRange range = NSMakeRange(entity.start, entity.end);
        @try {
            [_textStorage setAttributes:[self attributesForYourShowWord:entity] range:range];
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
    }
    [_textStorage endEditing];
}

#pragma mark - Public methods

- (CGSize)suggestedFrameSizeToFitEntireStringConstrainedToWidth:(CGFloat)width {
    if (_cleanText == nil)
        return CGSizeZero;
    return [_textView sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
}

- (CGSize) intrinsicContentSize {
    CGSize size = [self suggestedFrameSizeToFitEntireStringConstrainedToWidth:CGRectGetWidth(self.frame)];
    return CGSizeMake(size.width, size.height + 1);
}

#pragma mark - Private methods

- (NSArray *)hotWordsList {
    return _rangesOfYSWords;
}

#pragma mark - Setters

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    [self invalidateIntrinsicContentSize];
}

- (void)setText:(NSString *)text
{
    [super setText:@""];
    _uncleanText = text;
    _cleanText =     [text StringByChangingComplimintStringToNormalString];
    _cleanText = [_cleanText stringByReplacingOccurrencesOfString:@"√" withString:@" "];
    _selectableRange = NSMakeRange(NSNotFound, 0);
    [self determineYSWords];
    [self invalidateIntrinsicContentSize];
}



- (void)setDetectionBlock:(void (^)(EntityModel *ysWord, NSRange range))detectionBlock {
    if (detectionBlock) {
        _detectionBlock = [detectionBlock copy];
        self.userInteractionEnabled = YES;
    } else {
        _detectionBlock = nil;
        self.userInteractionEnabled = NO;
    }
}

- (void)setAttributedText:(NSAttributedString *)attributedText {
    _cleanAttributedText = [attributedText copy];
    self.text = _cleanAttributedText.string;
}



#pragma mark - Getters

- (NSString *)text {
    return _cleanText;
}

- (NSDictionary *)attributes {
    return _attributesText;
}

- (NSDictionary *)attributesForYourShowWord:(EntityModel *)ysWord
{
    switch (ysWord.entityType) {
        case EntityTypeMention:
            return _attributesMention;
        case EntityTypeHashTag:
            return _attributesHashtag;
        case EntityTypeshowroom:
            return _attributesShowroom;
        case EntityTypeUserName:
            return _attributeUserName;
        default:
            return _attributesText;
            break;
    }
    return nil;
}



#pragma mark - Retrieve word after touch event

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (![self getTouchedHotword:touches])
    {
        [super touchesBegan:touches withEvent:event];
    }
    _isTouchesMoved = NO;
    @try {
        [_textStorage removeAttribute:NSBackgroundColorAttributeName range:_selectableRange];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    _selectableRange = NSMakeRange(0, 0);
    _firstTouchLocation = [[touches anyObject] locationInView:_textView];
}

/*- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([self getTouchedHotword:touches] == nil)
    {
        [super touchesMoved:touches withEvent:event];
    }
    if (!_textSelectable) {
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        [menuController setMenuVisible:NO animated:YES];
        return;
    }
    _isTouchesMoved = YES;
    NSInteger charIndex = [self charIndexAtLocation:[[touches anyObject] locationInView:_textView]];
    if (charIndex == NSNotFound)
        return;
    [_textStorage beginEditing];
    @try {
        [_textStorage removeAttribute:NSBackgroundColorAttributeName range:_selectableRange];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    if (_selectableRange.length == 0) {
        _selectableRange = NSMakeRange(charIndex, 1);
        _firstCharIndex = charIndex;
    } else if (charIndex > _firstCharIndex) {
        _selectableRange = NSMakeRange(_firstCharIndex, charIndex - _firstCharIndex + 1);
    } else if (charIndex < _firstCharIndex) {
        _firstTouchLocation = [[touches anyObject] locationInView:_textView];
        
        _selectableRange = NSMakeRange(charIndex, _firstCharIndex - charIndex);
    }
    NSAssert(_selectableRange.location >= 0, @"range < 0");
    NSAssert(NSMaxRange(_selectableRange) < _textStorage.length, @"range > max");
    @try {
        [_textStorage addAttribute:NSBackgroundColorAttributeName value:_selectionColor range:_selectableRange];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    [_textStorage endEditing];
}
*/

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint touchLocation = [[touches anyObject] locationInView:self];
    if (self.textSelectable && _isTouchesMoved) {
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        [menuController setTargetRect:CGRectMake(_firstTouchLocation.x, _firstTouchLocation.y, 1.0, 1.0) inView:self];
        [menuController setMenuVisible:YES animated:YES];
        [self becomeFirstResponder];
        return;
    }
    if (!CGRectContainsPoint(_textView.frame, touchLocation))
        return;
    EntityModel *touchedHotword = [self getTouchedHotword:touches];
    if(touchedHotword != nil && _detectionBlock != NULL)
    {
        NSRange range = NSMakeRange(touchedHotword.start, touchedHotword.end);
        _detectionBlock((EntityModel *)touchedHotword, range);
    } else {
        [super touchesEnded:touches withEvent:event];
    }
}

- (NSInteger)charIndexAtLocation:(CGPoint)touchLocation
{
    NSUInteger glyphIndex = [_layoutManager glyphIndexForPoint:touchLocation inTextContainer:_textView.textContainer];
    CGRect boundingRect = [_layoutManager boundingRectForGlyphRange:NSMakeRange(glyphIndex, 1) inTextContainer:_textView.textContainer];
    
    if (CGRectContainsPoint(boundingRect, touchLocation))
        return [_layoutManager characterIndexForGlyphAtIndex:glyphIndex];
    else
        return NSNotFound;
}

- (id)getTouchedHotword:(NSSet *)touches {
    NSInteger charIndex = [self charIndexAtLocation:[[touches anyObject] locationInView:_textView]];
    if (charIndex != NSNotFound)
    {
        for (EntityModel *obj in _rangesOfYSWords)
        {
            NSRange range = NSMakeRange(obj.start, obj.end);
            if (charIndex >= range.location && charIndex < range.location + range.length)
            {
                return obj;
            }
        }
    }
    return nil;
}


@end
