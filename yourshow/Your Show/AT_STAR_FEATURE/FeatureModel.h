//
//  FeatureModel.h
//  YourShowLabel
//
//  Created by Siba Prasad Hota on 03/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeatureModel : NSObject

@property (nonatomic, strong) NSString    *name;
@property (nonatomic, strong) NSString    *ct_id;
@property (nonatomic, strong) NSString    *imgUrl;
@property (nonatomic, strong) NSString    *type;

@property (nonatomic, assign) BOOL    is_private;
@property (nonatomic, assign) BOOL    is_member;


@end
