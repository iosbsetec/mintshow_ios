//
//  YSCommentLabel.h
//  YourShowLabel
//
//  Created by Siba Prasad Hota on 01/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EntityModel.h"





@interface YSCommentLabel : UILabel

@property (nonatomic, assign) BOOL textSelectable;


@property (nonatomic, retain) UIFont *lblFont;
@property (nonatomic, strong) UIColor *selectionColor;
@property (nonatomic, copy) void (^detectionBlock)(EntityModel *ysWord, NSRange range);

- (NSDictionary *)attributes;
- (CGSize)suggestedFrameSizeToFitEntireStringConstrainedToWidth:(CGFloat)width;


@end
