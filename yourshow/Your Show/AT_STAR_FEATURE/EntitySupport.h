//
//  EntitySupport.h
//  YourShowLabel
//
//  Created by Siba Prasad Hota on 02/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntityModel.h"

@interface EntitySupport : NSObject

+(NSMutableArray *)returnFromServerText:(NSString*)str;
+(NSString *)scanAndConvert:(NSString*)tempString;




+(NSMutableArray*)showRoomDataWithDictioNary:(NSDictionary*)dictionary;
+(NSMutableArray*)TagDataWithDictioNary:(NSDictionary*)dictionary;
+(NSMutableArray*)UserlistDataWithDictioNary:(NSDictionary*)dictionary;

@end
