//
//  PopupTableviewCell.h
//  Your Show
//
//  Created by JITENDRA on 11/7/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeatureModel.h"

@interface PopupTableviewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageShowroomType;
@property (weak, nonatomic) IBOutlet UILabel *lblText;

-(void)setCellInfo:(FeatureModel *)cellData;

@end
