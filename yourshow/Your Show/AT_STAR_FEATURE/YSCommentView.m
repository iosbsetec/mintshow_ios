//
//  YSCommentView.m
//  YourShowLabel
//
//  Created by Siba Prasad Hota on 02/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "YSCommentView.h"
#import "EntitySupport.h"

#import "NSString+YaacMention.h"

NSString * const SelectedTextRangePropertyKey = @"selectedTextRange";
NSString * const InputTextPropertyKey = @"text";


@interface YSCommentView ()<UITextViewDelegate>{
    NSRange _selectableRange;
    NSInteger _firstCharIndex;
    BOOL _isTouchesMoved;
    CGPoint _firstTouchLocation;
    BOOL isBackPressed;
    NSString *currentInput;
}
@property (strong) NSTextStorage     *textStorage;
@property (strong) NSLayoutManager   *layoutManager;
@property (strong) NSTextContainer   *textContainer;

@property (nonatomic,readwrite) NSRange             suggestionRange;
@property (nonatomic, strong)   NSString            *cleanText;
@property (nonatomic, strong)   NSString            *uncleanText;
@property (nonatomic, copy)     NSAttributedString  *cleanAttributedText;
@property (nonatomic,strong)    NSMutableArray      *entityArray;
@property (nonatomic,strong)    NSRegularExpression *textCheckingRegularExpression;

@property (nonatomic,readwrite,getter=isSuggesting)BOOL suggesting;

@property (nonatomic) BOOL observingSelectedTextRange;
@property (nonatomic) BOOL observingTextInputText;

@property (nonatomic, strong) NSDictionary *attributesText;
@property (nonatomic, strong) NSDictionary *attributesMention;
@property (nonatomic, strong) NSDictionary *attributesHashtag;
@property (nonatomic, strong) NSDictionary *attributesShowroom;

//@property (strong) UITextView *textView;

@end

@implementation YSCommentView


- (id)init
{
    self = [super init];
    if (self)
        [self initializator];
        [self setupTextView];

    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    [self setupTextView];

    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    [self setupTextView];

    return self;
}


- (void)setupTextView
{
    isBackPressed =NO;
    currentInput = @"";
    
    self.counterCount = 0;
    
    _textStorage   = [NSTextStorage new];
    _layoutManager = [NSLayoutManager new];
    _textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)];
    
    [_layoutManager addTextContainer:_textContainer];
    [_textStorage addLayoutManager:_layoutManager];
    [_textStorage addAttributes:_attributesText range:NSMakeRange(0, 0)];
    
    _textSelectable = YES;
    _selectionColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    
    _textView = [[UITextView alloc] initWithFrame:self.bounds textContainer:_textContainer];
    _textView.delegate                          = self;
    _textView.autoresizingMask                  = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _textView.backgroundColor                   = [UIColor clearColor];
    _textView.textContainer.lineFragmentPadding = 0;
    _textView.textContainerInset                = UIEdgeInsetsZero;
    _textView.userInteractionEnabled            = YES;
    _textView.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    _textView.text = @"Describe it positive...";
    _textView.textAlignment = NSTextAlignmentLeft;
    self.textView.textColor = [UIColor lightGrayColor];
    
    [self addSubview:_textView];
}

-(void)setIneraction:(BOOL)interaction{
    _textView.userInteractionEnabled            = interaction;
}

- (void)initializator{
    self.entityArray= [NSMutableArray new];
    UIColor *customOrangeColor  = [UIColor colorWithRed:237.0/255.0 green:71.0/255.0 blue:22.0/255.0 alpha:0.9];
    _attributesText             = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"HelveticaNeue" size:12.0],NSFontAttributeName,nil];
    _attributesMention          = [NSDictionary dictionaryWithObjectsAndKeys:customOrangeColor,NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:12.0],NSFontAttributeName,nil];
    _attributesHashtag          = [NSDictionary dictionaryWithObjectsAndKeys:customOrangeColor,NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:12.0],NSFontAttributeName,nil];
    _attributesShowroom         = [NSDictionary dictionaryWithObjectsAndKeys:customOrangeColor,NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:12.0],NSFontAttributeName,nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged) name:UITextViewTextDidChangeNotification object:self.textView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidBeginEditing:) name:UITextViewTextDidBeginEditingNotification object:self.textView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidEndEditing:) name:UITextViewTextDidEndEditingNotification object:self.textView];
}


#pragma mark - Responder

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return (action == @selector(copy:));
}

- (void)copy:(id)sender {
    [[UIPasteboard generalPasteboard] setString:[_cleanText substringWithRange:_selectableRange]];
    
    @try
    {
        [_textStorage removeAttribute:NSBackgroundColorAttributeName range:_selectableRange];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception);
    }
}


- (void)determineYSWords
{
    if (_cleanText == nil)
        return;
   // self.entityArray = [EntitySupport returnFromServerText:_uncleanText];
    [self updateText];
}



- (void)determineYSWordsForAddmint:(NSString *)strText {
    //    [self.textDelegate textDidChange:self];
    _uncleanText = strText;
    _cleanText =     [strText StringByChangingComplimintStringToNormalString];
    _selectableRange = NSMakeRange(NSNotFound, 0);
    self.entityArray = [EntitySupport returnFromServerText:_uncleanText];
      [self updateText];
}


- (void)updateText {
    [_textStorage beginEditing];
    NSAttributedString *attributedString = _cleanAttributedText ?: [[NSMutableAttributedString alloc] initWithString:_cleanText];
    [_textStorage setAttributedString:attributedString];
    [_textStorage setAttributes:_attributesText range:NSMakeRange(0, attributedString.length)];
    
    for (EntityModel *entity in self.entityArray) {
        @try {
            NSRange range = NSMakeRange(entity.start, entity.end);
            [_textStorage setAttributes:[self attributesForYourShowWord:entity] range:range];
        }@catch (NSException *exception) {
        }@finally  {
        }
    }
    [_textStorage endEditing];
}



-(void)resetTextView {
    [self.entityArray removeAllObjects];
    _textView.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    _textView.text = @"Describe it positive...";
    self.textView.textColor = [UIColor lightGrayColor];
    //CHANGED BY JITENDRA SINCE IF USER HAVE BEEN SELECT THE TEXTMINT BUT WITHOUT INSERTING HE LEFT BACK AND AGAIN COMING TO THE ADDMINT SCREEN THEN THE PLACE HOLDER WAS NOT SHOWING.:)
    [self.textView resignFirstResponder];
}



#pragma mark - Public methods
- (CGSize)suggestedFrameSizeToFitEntireStringConstrainedToWidth:(CGFloat)width {
    if (_cleanText == nil)
        return CGSizeZero;
    return [self.textView sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
}

- (CGSize) intrinsicContentSize {
    CGSize size = [self suggestedFrameSizeToFitEntireStringConstrainedToWidth:CGRectGetWidth(self.frame)];
    return CGSizeMake(size.width, size.height + 1);
}

#pragma mark - Private methods

- (NSArray *)hotWordsList {
    return self.entityArray;
}

#pragma mark - Setters

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    [self invalidateIntrinsicContentSize];
}

-(void)setViewText:(NSString*)atext{
    [self setText:atext];
}

-(void)setTextAllignment:(NSTextAlignment)allignment {
    _textView.textAlignment = allignment;
}

- (void)setText:(NSString *)text{
    
    _uncleanText = text;
    _cleanText =     [text StringByChangingComplimintStringToNormalString];
    _selectableRange = NSMakeRange(NSNotFound, 0);
    [self determineYSWords];
    [self invalidateIntrinsicContentSize];
}



- (void)setAttributedText:(NSAttributedString *)attributedText{
    _cleanAttributedText = [attributedText copy];
    self.textView.text = _cleanAttributedText.string;
}



#pragma mark - Getters

- (NSString *)text{
    return _cleanText;
}

- (NSDictionary *)attributes{
    return _attributesText;
}

- (NSDictionary *)attributesForYourShowWord:(EntityModel *)ysWord{
    switch (ysWord.entityType) {
        case EntityTypeMention:
            return _attributesMention;
        case EntityTypeHashTag:
            return _attributesHashtag;
        case EntityTypeshowroom:
            return _attributesShowroom;
        default:
            return _attributesText;
            break;
    }
    return nil;
}




-(void)setSuggesting:(BOOL)suggesting {
    if (_suggesting != suggesting) {
        _suggesting = suggesting;
        if (suggesting)  {
            if (self.shouldBeginSuggestingBlock)  {
                self.shouldBeginSuggestingBlock();
            }
        } else  {
            if (self.shouldEndSuggestingBlock)  {
                self.shouldEndSuggestingBlock();
            }
        }
    }
}




- (void)setObservingSelectedTextRange:(BOOL)observingSelectedTextRange {
    if (_observingSelectedTextRange != observingSelectedTextRange) {
        _observingSelectedTextRange = observingSelectedTextRange;
        if (observingSelectedTextRange) {
            [self addObserver:self forKeyPath:SelectedTextRangePropertyKey options:NSKeyValueObservingOptionNew context:NULL];
        } else {
            [self removeObserver:self forKeyPath:SelectedTextRangePropertyKey];
        }
    }
}

- (void)setObservingTextInputText:(BOOL)observingTextInputText {
    if (_observingTextInputText != observingTextInputText) {
        _observingTextInputText = observingTextInputText;
        if (observingTextInputText) {
            [self  addObserver:self forKeyPath:InputTextPropertyKey options:NSKeyValueObservingOptionNew context:NULL];
        } else {
            [self  removeObserver:self forKeyPath:InputTextPropertyKey];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self && ([keyPath isEqualToString:SelectedTextRangePropertyKey] || [keyPath isEqualToString:InputTextPropertyKey])) {
        
    }
}


-(void)textViewDidBeginEditing:(UITextView *)textView {
    if ([self.textView.text isEqualToString:@"Describe it positive..."]) {
        self.textView.text = @"";
        self.textView.textColor = [UIColor blackColor]; //optional
    }
    self.observingSelectedTextRange = YES;
    self.observingTextInputText = YES;
    [self performSelector:@selector(textChanged) withObject:nil afterDelay:0.1];
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    if ([self.textView.text isEqualToString:@""]) {
        self.textView.text = @"Describe it positive...";
        self.textView.textColor = [UIColor lightGrayColor]; //optional
    }
    self.observingSelectedTextRange = NO;
    self.observingTextInputText = NO;
    self.suggesting = NO;
}


- (NSRegularExpression *)textCheckingRegularExpression {
    if (!_textCheckingRegularExpression) {
        _textCheckingRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[@#*]([^\\s/:：@#*]?)+$?" options:NSRegularExpressionCaseInsensitive error:NULL];
    }
    return _textCheckingRegularExpression;
}



-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    currentInput = text;
    if ([text isEqualToString: @""])  {
         [self checkAndDeletetheWordInRange:range];
    } else if (self.counterCount >=300)
        return NO;
    else if([text isEqualToString:@"\n"]) {
        [self.textView resignFirstResponder];
        return NO;
    } else if([text isEqualToString:@"\n"]) {
        [self.textView resignFirstResponder];
        return NO;
    }
    return YES;
}



- (void)textChanged {
    __block NSString *word = nil;
    __block NSRange range = NSMakeRange(NSNotFound, 0);
    NSString *textViewTXT = self.textView.text;
    

    [self.textCheckingRegularExpression enumerateMatchesInString:textViewTXT
                                                         options:0
                                                           range:NSMakeRange(0, textViewTXT.length)
                                                      usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         NSRange textSelectedRange = self.textView.selectedRange;
         if (textSelectedRange.location > result.range.location && textSelectedRange.location <= result.range.location + result.range.length) {
             word = [textViewTXT substringWithRange:result.range];
             range = result.range;
             *stop = YES;
         }
     }];
    
    if (word.length >= 1 && range.location != NSNotFound)  {
        NSString *first = [word substringToIndex:1];
        NSString *rest = [word substringFromIndex:1];
        
        if ([first isEqualToString:@"@"]) {
            self.suggesting = YES;
            self.suggestionRange = NSMakeRange(range.location + 1, range.length - 1);
            if (self.textDelegate) {
                self.suggestionType = EntityTypeMention;
                [self.textDelegate beginSuggestingForTextView:EntityTypeMention query:rest  Inrange:self.suggestionRange];
            }
        }  else if ([first isEqualToString:@"#"]) {
            self.suggesting = YES;
            self.suggestionRange = NSMakeRange(range.location + 1, range.length - 1);
            if (self.textDelegate) {
                self.suggestionType = EntityTypeHashTag;
                [self.textDelegate beginSuggestingForTextView:EntityTypeHashTag query:rest Inrange:self.suggestionRange];
            }        }
        else if ([first isEqualToString:@"*"])  {
            self.suggesting = YES;
            self.suggestionRange = NSMakeRange(range.location + 1, range.length - 1);
            if (self.textDelegate) {
                self.suggestionType = EntityTypeshowroom;
                [self.textDelegate beginSuggestingForTextView:EntityTypeshowroom query:rest Inrange:self.suggestionRange];
            }        }else {
                self.suggestionRange = NSMakeRange(NSNotFound, 0);
                self.suggesting = NO;
                [self.textDelegate endSuggesting];
            }
    }  else  {
        self.suggestionRange = NSMakeRange(NSNotFound, 0);
        self.suggesting = NO;
        [self.textDelegate endSuggesting];
        if (currentInput.length>0) {
            self.counterCount= self.counterCount+1;
            [self.textDelegate textDidChange:self];
        }
    }
}


// [self checkAndDeletetheWordInRange:range];
-(void)checkAndDeletetheWordInRange:(NSRange)range{
    BOOL isDeleted= NO;
    NSMutableArray *somearray= [NSMutableArray new];
    for (int index=0; index<self.entityArray.count;index++)  {
        EntityModel *entity = [self.entityArray objectAtIndex:index];
        NSRange storedrange = NSMakeRange(entity.start, entity.end);
        NSRange intersection = NSIntersectionRange(storedrange,range);
        if (intersection.length <= 0) {
            
        }  else {
            NSString *finalString = [self getServerStringForFinalOutPut];
            NSLog(@"final String=%@",finalString);
            somearray = [EntitySupport returnFromServerText:finalString];
            //hi @[Siba Hota](contact:3861)  and @[jitentest jit](contact:2567)
            for (EntityModel *emodel in somearray)  {
                if ([emodel.idString isEqualToString:entity.idString]) {
                    [somearray removeObject:emodel];
                    NSString *finalStringNow = [@"" StringByConvertingPartialStringsToFullString:somearray];
                    @try  {
                        [self setText: finalStringNow];
                    }  @catch (NSException *exception) {
                        NSLog(@"%@", exception);
                    }
                    somearray = [EntitySupport returnFromServerText:finalStringNow];
                    break;
                }
            }
            
           
            isDeleted = YES;
            self.suggesting= NO;
            self.suggestionRange = NSMakeRange(NSNotFound, 0);
            [self.textDelegate endSuggesting];
            [self.textDelegate wordDeletedForSuggestType: entity.entityType andID:entity.idString];
            break;
           // NSLog(@"Intersection = %@", NSStringFromRange(intersection));
        }
        
    }
    if(isDeleted)  {
        self.entityArray = somearray;
        [self changeColorOfText];
    }  else {
        if (self.counterCount>0) {
            self.counterCount= self.counterCount-1;
            [self.textDelegate textDidChange:self];
        }
    }
    
    if ([self.textView.text length]<2) {
        [self.entityArray removeAllObjects];
    }
}

//[_textStorage setAttributes:[self attributesForYourShowWord:entity] range:range];

-(void)appandAStringWithEntityType:(EntityType)textSuggestion AndString:(NSString*)fullWord andID:(NSString*)contactID {
    if ([self.textView.text isEqualToString:@"Describe it positive..."]) {
        self.textView.text = @"";
        self.textView.textColor = [UIColor blackColor]; //optional
    }

    NSString *currentText = self.textView.text;
    EntityModel *newEntity = [EntityModel new];
    newEntity.name = fullWord;
    newEntity.entityType =textSuggestion;
    newEntity.start = currentText.length;
    newEntity.end = fullWord.length+1;
    newEntity.idString = contactID;
    
    [self.entityArray addObject:newEntity];
    currentText = [currentText stringByAppendingString:fullWord];
    self.textView.text = [currentText stringByAppendingString:@" "];
    
    [self changeColorOfText];
}



-(void)wordSelectedForSuggestType:(EntityType)textSuggestion
                        AndString:(NSString*)fullWord
                            andID:(NSString*)contactID
                         forRange:(NSRange)range
{
    EntityModel *newEntity = [EntityModel new];
    newEntity.name = fullWord;
    newEntity.entityType =textSuggestion;
    newEntity.start = range.location-1;
    newEntity.end = fullWord.length+1;
    newEntity.idString = contactID;
    [self.entityArray addObject:newEntity];
    
    [self constructThefullStringNow:fullWord withID:contactID andType:textSuggestion forRange:range];
}

-(void)constructThefullStringNow:(NSString*)name withID:(NSString *)idstr andType:(EntityType)type forRange:(NSRange)range
{
    NSRange suggestionRange = self.suggestionRange;
    NSString *suggestionString = name;
    NSString *insertString = [suggestionString stringByAppendingString:@" "];
    if (self.textView.text.length > suggestionRange.location + suggestionRange.length) {
        if ([[self.textView.text substringWithRange:NSMakeRange(suggestionRange.location, suggestionRange.length + 1)] hasSuffix:@" "]) {
            insertString = suggestionString;
        }
    }
    
    @try  {
        self.textView.text = [self.textView.text stringByReplacingCharactersInRange:suggestionRange withString:insertString];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    [self changeColorOfText];
    self.suggesting = NO;
    self.suggestionRange = NSMakeRange(NSNotFound, 0);
}

- (void)changeColorOfText {
    [_textStorage beginEditing];
    NSAttributedString *attributedString = _cleanAttributedText ?: [[NSMutableAttributedString alloc] initWithString:self.textView.text];
    [_textStorage setAttributedString:attributedString];
    [_textStorage setAttributes:_attributesText range:NSMakeRange(0, attributedString.length)];
    for (EntityModel *entity in self.entityArray)  {
        @try {
            NSRange range = NSMakeRange(entity.start, entity.end);
            if (self.textView.text.length >= range.location + range.length) {
                [_textStorage setAttributes:[self attributesForYourShowWord:entity] range:range];
            }
        } @catch (NSException *exception) {
                NSLog(@"%@", exception);
            }
    }
    [_textStorage endEditing];
}



-(NSString *)getServerStringForFinalOutPut
{
    NSString *finalText = self.textView.text;
    for (EntityModel *entity in self.entityArray) {
        if ([entity.name rangeOfString:@" " ].location != NSNotFound) {
            @try{
                NSRange range = NSMakeRange(entity.start, entity.end);
                finalText = [finalText stringByReplacingCharactersInRange:range withString:[entity.name stringByReplacingOccurrencesOfString:@" " withString:@"|"]];
            }
            @catch (NSException *exception) {
            }
            @finally {
            }
        }
    }
    
    NSString *newOutPutString = @"";
    NSInteger startRange = 0;
    NSInteger endRange = 0;
    finalText = [finalText stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    NSArray *allTextArray = [finalText componentsSeparatedByString:@" "];
    for (NSString *bString in allTextArray)
    {
        NSString *nameStr = [bString stringByReplacingOccurrencesOfString:@"|" withString:@" "];
        endRange = nameStr.length;
        EntityModel  *fakeEntity = [self getentityFromRange:NSMakeRange(startRange,endRange)];
        startRange = startRange + endRange +1;

        if(fakeEntity == nil)
        {
            newOutPutString = [newOutPutString stringByAppendingString:nameStr];
            newOutPutString = [newOutPutString stringByAppendingString:@" "];
        }
        else
        {
            newOutPutString = [newOutPutString stringByAppendingString:[nameStr StringByChangingNormalNameToServername:fakeEntity.idString entityType:fakeEntity.entityType]];
            newOutPutString = [newOutPutString stringByAppendingString:@" "];
        }
    }
    return newOutPutString;
}


-(EntityModel *)getentityFromRange:(NSRange)range
{
    for (EntityModel *entity in self.entityArray)
     {
         NSRange storedrange = NSMakeRange(entity.start, entity.end);
         NSRange intersection = NSIntersectionRange(storedrange,range);
         if (intersection.length <= 0) {
         }
         else {   return entity;
             break;
         }
     }
    return nil;
}

-(NSMutableArray *)getFeatureArrayFOrPostMint
{
    NSMutableArray *anArray = [NSMutableArray new];
    for(EntityModel *entity in self.entityArray)
    {
        if (entity.entityType == EntityTypeMention) {
            [anArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:entity.idString ,@"id",entity.name,@"name", nil]];
        }
        
    }
        return anArray;
}
-(NSMutableArray *)getShowroomArrayFOrPostMint {
    NSMutableArray *anArray = [NSMutableArray new];
    for(EntityModel *entity in self.entityArray)  {
        if (entity.entityType == EntityTypeshowroom) {
           [anArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:entity.idString ,@"id",entity.name,@"name",@"no",@"is_private", nil]];
        }
    }
    return anArray;
}

@end

