//
//  YSCommentUserLable.h
//  Your Show
//
//  Created by JITENDRA on 11/4/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EntityModel.h"
@interface YSCommentUserLable : UILabel
@property (nonatomic, assign) BOOL textSelectable;


@property (nonatomic, retain) UIFont *lblFont;
@property (nonatomic, strong) UIColor *selectionColor;
@property (nonatomic, copy) void (^detectionBlock)(EntityModel *ysWord, NSRange range);

- (NSDictionary *)attributes;
- (CGSize)suggestedFrameSizeToFitEntireStringConstrainedToWidth:(CGFloat)width;
- (void)setupLabel;
- (id)initWithFrame:(CGRect)frame withFont : (CGFloat ) fontSize  attributeTextColor : (UIColor *) attributeTextColor ;
-(void)setAllignmentForLabel:(NSTextAlignment)allignment;
-(void)setAttributeForTextWithType:(EntityType)type color:(UIColor *)color font:(UIFont *)font isforAll:(BOOL)forAll;
-(void)setAttributeForfont:(UIFont *)font;


@end
