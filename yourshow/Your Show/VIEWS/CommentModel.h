//
//  CommentModel.h
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 22/09/14.
//
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject


@property(nonatomic,strong)NSString *achieveCommentDate;
@property(nonatomic,strong)NSString *achieveCommentId;
@property(nonatomic,strong)NSString *achieveCommentText;
@property(nonatomic,strong)NSString *achieveCommentUserId;
@property(nonatomic,strong)NSString *achieveCommentUname;
@property(nonatomic,strong)NSString *achieveCommentUserImage;
@property(nonatomic,strong)NSString *achieveCommentUserLink;
@property(nonatomic,strong)NSString *cBuckNotifyText;
@property(nonatomic,strong)NSString *cBuckNotifyVal;

@property(nonatomic,assign)     CGFloat         commentTextHeight;


@property(nonatomic,strong)  NSString *status;
@property(nonatomic,strong)  NSString *status_Msg;
@property(nonatomic,strong)  NSString *status_code;
@property(nonatomic,strong)  NSString *status_commnetEdited;

@property (nonatomic,assign) BOOL postingStatus;
@property(nonatomic,strong)  NSMutableArray *atMentionedArray;


-(id)initWithComplimint : (NSDictionary *) complimintInfo;

@end



                                             