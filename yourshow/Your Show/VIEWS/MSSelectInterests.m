//
//  MSSelectInterests.m
//  Your Show
//
//  Created by Siba Prasad Hota on 12/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "MSSelectInterests.h"
#import "CellSelectInterests.h"
#import "DiscoverDataModel.h"
#import "RoundedRect.h"

@interface MSSelectInterests()<UICollectionViewDataSource,UICollectionViewDelegate>

{
     UICollectionView *selectInterestCollectionView;
     int selected;
     NSMutableArray *arrSelectedInterests;
     UIButton *btnSelectShowroom,*cancelBtn;
     UILabel *lblSelectInterstCount;
    
    NSMutableArray *arrInterests;
    BOOL isApiCalling,isDataFinished;
    int pagenumber;
    NSMutableArray *arrShowrooms;

    
    UIView *viewRoundRect;
   


}
@end
@implementation MSSelectInterests

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
#pragma mark -
#pragma mark Class methods

+ (MSSelectInterests *)showSelectInterestViewTo:(UIView *)view withViewcontainerFrame : (CGRect) containterframe;
{
    MSSelectInterests *hud = [[MSSelectInterests alloc] initWithFrame:containterframe ];
    [view addSubview:hud];
    [hud show:YES];
    return [hud autorelease];
}
- (void)show:(BOOL)animated {
    
    [self setNeedsDisplay];
}
- (void)hide:(BOOL)animated {
    
    [self removeFromSuperview];
}

-(void)cancelTapped
{
    arrInterests = nil;
    arrSelectedInterests = nil;
    [self removeFromSuperview];
}
+ (BOOL)hideSelectInterestView:(UIView *)view animated:(BOOL)animated {
    UIView *viewToRemove = nil;
    for (UIView *v in [view subviews]) {
        if ([v isKindOfClass:[MSSelectInterests class]]) {
            viewToRemove = v;
        }
    }
    if (viewToRemove != nil) {
        MSSelectInterests *HUD = (MSSelectInterests *)viewToRemove;
        [HUD hide:animated];
        return YES;
    } else {
        return NO;
    }
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        selectInterestCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(5, 60, CGRectGetWidth(self.frame)-10,CGRectGetHeight(self.frame)-65) collectionViewLayout:layout];
        [selectInterestCollectionView setDataSource:self];
        [selectInterestCollectionView setDelegate:self];
        [selectInterestCollectionView setBackgroundColor:[UIColor clearColor]];
        
        UINib *cellNib = [UINib nibWithNibName:@"CellSelectInterests" bundle:nil];
        [selectInterestCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"cellidentifier"];
        selected =0;
        arrSelectedInterests = [[NSMutableArray alloc]init];
        arrInterests = [[NSMutableArray alloc]init];
        arrShowrooms = [[NSMutableArray alloc]init];

       // btnSelectShowroom.layer.cornerRadius = 5.;
        [self addSubview:selectInterestCollectionView];

        isApiCalling = NO;
        isDataFinished = NO;
        pagenumber = 1;
        
        [self createUIForDisplay];
        [self setBackgroundColor:BACKGROUNDCOLOR];
    }
    return self;
}
-(id)createUIForDisplay
{
    viewRoundRect = [[UIView alloc]initWithFrame:
                     CGRectMake(0, 5, CGRectGetWidth(self.frame),50)];
    
    viewRoundRect.backgroundColor = [UIColor whiteColor];

    
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(4, 5,200, 40)];
        lblTitle.numberOfLines = 2;
        lblTitle.text = @"Join at least 3 Showrooms \nto customize your Show feed.";
        [lblTitle setFont:[UIFont fontWithName:@"Helvetica" size:15]];
        lblTitle.textColor = ORANGECOLOR;
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.backgroundColor = [UIColor clearColor];
    
    
    
    if ([GETVALUE(CEO_ISMINTALREADYPOSTED) isEqualToString:@"yes"]) {
        UIImage * buttonImage        = [UIImage imageNamed:@"UploadCancel.png"];
        cancelBtn                    = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setFrame:CGRectMake(CGRectGetWidth(viewRoundRect.frame)-34, 0, 38, 22)];
        cancelBtn.layer.cornerRadius = 4.0;
        [cancelBtn setImage:buttonImage forState:UIControlStateNormal];
        [cancelBtn setBackgroundColor:[UIColor clearColor]];
        [cancelBtn addTarget:self action:@selector(cancelTapped) forControlEvents:UIControlEventTouchUpInside];
        [viewRoundRect addSubview:cancelBtn];
    }
    
        [viewRoundRect addSubview:lblTitle];
//        btnSelectShowroom          = [UIButton buttonWithType:UIButtonTypeCustom];
//        [btnSelectShowroom setFrame:CGRectMake(CGRectGetWidth(viewRoundRect.frame)-110, 11, 70, 28)];
//        btnSelectShowroom.layer.cornerRadius = 4.0;
//        [btnSelectShowroom setBackgroundColor:[UIColor lightGrayColor]];
//        [btnSelectShowroom addTarget:self action:@selector(btnSelectShowroomTapped:) forControlEvents:UIControlEventTouchUpInside];
//        [viewRoundRect addSubview:btnSelectShowroom];
//        [btnSelectShowroom setTitle:@"Select" forState:UIControlStateNormal];
//        [btnSelectShowroom.titleLabel setTextColor:[UIColor whiteColor]];
//        [btnSelectShowroom.titleLabel setFont:[UIFont systemFontOfSize:12]];

    [self addSubview:viewRoundRect];
    [self getInterestList:pagenumber];

    return self;
}

#pragma mark -
- (void)getInterestList:(int)pagenum
{
    [self getAllShowroomForDiscoverTab:pagenum];
}


-(void)getAllShowroomForDiscoverTab : (int)pageNum
{
    // [MBProgressHUD showHUDAddedTo:_DiscoverAllTable animated:YES];
    NSData *submitData =  [[NSString stringWithFormat:@"access_token=%@&disc_type=2&page_number=%d",GETVALUE(CEO_AccessToken),
                            pageNum]dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *str = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding ];
    NSLog(@"request %@",str);
    
    [YSAPICalls getForDiscoverTab:submitData withSuccessionBlock:^(id newResponse) {
        
        //  [MBProgressHUD hideHUDForView:_DiscoverAllTable animated:YES];
        isApiCalling = NO;
        
        Generalmodel *gmodel = newResponse;
        if ([gmodel.status_code integerValue] && [gmodel.status isEqualToString:@"Success"]) {
            [self checkButtonStatus];
            [arrInterests addObjectsFromArray:gmodel.tempArray];
            [selectInterestCollectionView reloadData];
        }
    } andFailureBlock:^(NSError *error) {
        // [MBProgressHUD hideHUDForView:_DiscoverAllTable animated:YES];
    }];
}



-(void)getInterestDataFromAPI:(NSData*)submitData andAPIUrl:(NSURL *)myUrl
{
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"My Url %@",myUrl);
    NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self animated:YES];
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"status"] isEqualToString:@"true"])
         {
             [arrInterests addObjectsFromArray:[showroomDict valueForKey:@"list"]];
         }
         else
         {
             
         }
         
         [selectInterestCollectionView reloadData];
     }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrInterests.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(90, 115);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,4,0,4);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CellSelectInterests  *cell     =    [collectionView dequeueReusableCellWithReuseIdentifier:@"cellidentifier" forIndexPath:indexPath];
    //[[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    
    cell.lblIntShwrmName.text = nil;
    cell.imgVwIntShwrmImage.mintImageURL = nil;
    
    DiscoverDataModel *model = [arrInterests objectAtIndex:[indexPath row]];
    
    cell.lblIntShwrmName.text = model.discoverName;
    cell.imgVwIntShwrmImage.mintImageURL = [NSURL URLWithString:model.discoverImg];
    if ([self discoverIdexistInArray:model.discoverId]) {
        cell.viewCheckMark.alpha = 0.7;
        cell.imgTickMark.hidden = NO;
        cell.imgVwIntShwrmImage.layer.borderColor = ORANGECOLOR.CGColor;
        cell.imgVwIntShwrmImage.layer.borderWidth = 1.5;
    }else{
        cell.viewCheckMark.alpha = 0.0;
        cell.imgTickMark.hidden = YES;
        cell.imgVwIntShwrmImage.layer.borderColor = [UIColor clearColor].CGColor;
        cell.imgVwIntShwrmImage.layer.borderWidth = 0.0;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    DiscoverDataModel *model = [arrInterests objectAtIndex:[indexPath row]];
    
    if ([self discoverIdexistInArray:model.discoverId]) {
        [arrSelectedInterests removeObject:model.discoverId];
        [arrShowrooms removeObject:model.discoverName];
 
    }else{
        [arrSelectedInterests addObject:model.discoverId];
        [arrShowrooms addObject:model.discoverName];

    }
    [selectInterestCollectionView reloadItemsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil]];
    [self checkButtonStatus];
    
}

-(BOOL)discoverIdexistInArray:(NSString*)discoverID{
    for(NSString *did in arrSelectedInterests){
        if ([did isEqualToString:discoverID]) {
            return YES;
        }
    }
    return NO;
}



-(void)checkButtonStatus
{
    if (arrSelectedInterests.count==3)
    {
//        [btnSelectShowroom setUserInteractionEnabled:YES];
//        [btnSelectShowroom setBackgroundColor:ORANGECOLOR];
        [self btnSelectShowroomTapped:nil];
    }
//    else
//    {
//        [btnSelectShowroom setUserInteractionEnabled:NO];
//        [btnSelectShowroom setBackgroundColor:[UIColor colorWithRed:170.0/255  green:170.0/255  blue:170.0/255  alpha:1.0]];;
//    }
//
//    
//    [btnSelectShowroom setTitle:(arrSelectedInterests.count==0)?@"Select":[NSString stringWithFormat:@"Select (%lu)",(unsigned long)arrSelectedInterests.count] forState:UIControlStateNormal];

}

- (void)btnSelectShowroomTapped:(UIButton *)sender
{
    if (![arrSelectedInterests count]) {
        return;
    }
    NSString *showroomids = @"";
    
    for(int i=0;i<[arrSelectedInterests count];i++)
    {
        if(([arrSelectedInterests count]-1)==i)
            showroomids = [showroomids stringByAppendingString:[NSString stringWithFormat:@"%@",[arrSelectedInterests objectAtIndex:i]]];
        else
            showroomids = [showroomids stringByAppendingString:[NSString stringWithFormat:@"%@,",[arrSelectedInterests objectAtIndex:i]]];
        NSLog(@"%@",showroomids);
    }
    

    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&showroom_id=%@",GETVALUE(CEO_AccessToken),showroomids] dataUsingEncoding:NSUTF8StringEncoding];
    
    [MBProgressHUD showHUDAddedTo:self animated:YES];

    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_SELECTINTEREST]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
        NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
        NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self animated:YES];
         
         AppDelegate *delegateApp = MINTSHOWAPPDELEGATE
         delegateApp.showroomJoienedCount = [arrSelectedInterests count];
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"Status"] isEqualToString:@"Success"])
         {
             [MSMixPanelTrackings trackSelectInterestForUserDetails:[arrSelectedInterests count] listOfPeoples:arrShowrooms SelectionDuration:12 scrolled:(pagenumber>1)?YES:NO];
             SETVALUE(@"yes",CEO_ISJOINEDSHOWROOM);
             SYNCHRONISE;
             [self cancelTapped];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"SELECTINTERESTCOMPLTE" object:nil];
             NSLog(@"%@",[showroomDict valueForKey:@"StatusMsg"]);
         }
         else
         {
             NSLog(@"%@",[showroomDict valueForKey:@"StatusMsg"]);
         }
     }];
}

#pragma mark - UIScrollViewDelegate
//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UICollectionView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    NSLog(@"%d",(yOffset / height > kNewPageLoadScrollPercentageThreshold));
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (isApiCalling) {
        return;
    }
    
    if (isDataFinished) {
        return;
    }
    
    BOOL shouldLoadNextPage = ShouldLoadNextPage(selectInterestCollectionView);
    if (shouldLoadNextPage)
    {
        pagenumber = pagenumber +1;
        NSLog(@"*** Scroll End ****");
        isApiCalling = YES;
        [self getInterestList:pagenumber];
    }
}



@end
