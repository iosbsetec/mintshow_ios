//
//  InviteFriendGroups.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 21/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "InviteFriendGroups.h"

@implementation InviteFriendGroups

{

    float lastComponentVal;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)initWithFrame:(CGRect)frame withArray : (NSArray *) groupInFo
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"friends Info %@",groupInFo);
        [self setup:groupInFo];
    }
    
    return self;
}
#pragma mark -

- (void)setup : (NSArray *) arrayInfo
{
    CGFloat xCord = ([arrayInfo count]> 5)?95:80;
//    NSLog(@"xcodeinate is %@",xCord);
    
    int i = 0;
    for (FriendsInfo *obj in arrayInfo) {
        if (i==5) {
         goto doneWithLoops;
        }
        UIImageView *dot       =[[UIImageView alloc] initWithFrame:CGRectMake(xCord,2,26,26)];
        dot.layer.cornerRadius = 13;
//        dot.backgroundColor = [UIColor lightGrayColor];
        dot.layer.borderWidth = 1.5f;
        dot.layer.borderColor = [UIColor whiteColor].CGColor;
        [dot setClipsToBounds:YES];
        
        dot.mintImageURL           = [NSURL URLWithString:obj.profile_image_url];
        [self addSubview:dot];
        xCord = xCord - 20;
        i++;
    }
    doneWithLoops:
    if ([arrayInfo count]> 5) {
        [self createLable:[arrayInfo count]-5 xCord:xCord+ 14];
    }
}

-(void)createLable : (NSInteger) count xCord : (CGFloat) xCord
{
    CGFloat xCordVal = 125;


    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(xCordVal, 6, 40, 20)];
    fromLabel.text = [NSString stringWithFormat:@"+%ld",(long)count];
    fromLabel.font = [UIFont boldSystemFontOfSize:10];
    fromLabel.numberOfLines = 1;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
    fromLabel.adjustsFontSizeToFitWidth = YES;
  //  fromLabel.adjustsLetterSpacingToFitWidth = YES;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:fromLabel];

}


@end
