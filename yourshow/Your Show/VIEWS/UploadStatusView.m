//
//  UploadStatusView.m
//  Your Show
//
//  Created by Siba Prasad Hota on 06/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "UploadStatusView.h"
#import <AFNetworking/AFNetworking.h>
#import "SPHCreateUrl.h"
#import "Keys.h"
#import <Photos/PHAsset.h>
#import <AVFoundation/AVFoundation.h>


@interface UploadStatusView(){
    CGFloat currentprogres;
    CGPoint startPosition;

    BOOL cancelClicked;
    BOOL isVideo;
    BOOL isLeftSwiped;
    BOOL isStopDummyProgress;
    
    __weak IBOutlet UIButton *btnPlay;
    NSString *shareMintUrl;
    
    NSURLSessionDataTask *uploadTask;
}


@property (strong, nonatomic) IBOutlet UIView *baseView;

- (IBAction)btnPlayTapped:(UIButton *)sender;

@end

#define ImageDisposition @"Content-Disposition: form-data; name=\"images\"; filename=\"achievementPic.png\"\r\n"
#define VideoDisposition @"Content-Disposition: form-data; name=\"file\"; filename=\"somefile.mov\"\r\n"

@implementation UploadStatusView

+ (id)UploadStatusViewInit{
    UploadStatusView *customView = [[[NSBundle mainBundle] loadNibNamed:@"UploadStatusView" owner:nil options:nil] lastObject];
    // make sure customView is not nil or the wrong class!
    
    if ([customView isKindOfClass:[UploadStatusView class]])
        return customView;
    else
        return nil;
}


-(void)setUploadStatusData:(NSDictionary*)uploaddata{
    
    UISwipeGestureRecognizer* sgr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(headerViewSwipedLeft:)];
    [sgr setDirection:UISwipeGestureRecognizerDirectionLeft]; // change direction accordingly
    [self addGestureRecognizer:sgr];
    
    UISwipeGestureRecognizer* sgr2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(headerViewSwipedRight:)];
    [sgr2 setDirection:UISwipeGestureRecognizerDirectionRight]; // change direction accordingly
    [self addGestureRecognizer:sgr2];

    currentprogres = 0.0;
    isVideo = NO;
    isStopDummyProgress = YES;
    self.isCancelable = YES;
}



-(UIImage *)imageResize :(UIImage*)img andResizeToWidth:(CGFloat)newWidth{
    CGFloat newheight=(img.size.height/img.size.width)*newWidth;
    CGFloat scale = [[UIScreen mainScreen]scale];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, newheight), NO, scale);
    [img drawInRect:CGRectMake(0,0,newWidth,newheight)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

-(void)showDummyProgressToPrepareVideo{
    if (isVideo && (currentprogres>=0.99 || isStopDummyProgress)){
        return;
    }
    currentprogres = currentprogres+0.01;
    [self performSelector:@selector(showDummyProgressToPrepareVideo) withObject:nil afterDelay:0.1];
    [self setProgressUI];
}


-(void)uploadImageWithData:(NSData *)ImageData withImage:(UIImage*)Image{
    isVideo = NO;
    btnPlay.hidden = YES;
    self.uploadImageView.image = [self imageResize:Image andResizeToWidth:50];
    NSData *imagData = UIImageJPEGRepresentation([self imageResize:Image andResizeToWidth:640],90);
    [self uploadWithData:imagData  contentDisposition:[ImageDisposition dataUsingEncoding:NSUTF8StringEncoding] isImage:YES];
}

-(void)uploadVideoWithData:(NSData *)videoData withImage:(UIImage*)Image{
    isVideo = YES;
    btnPlay.hidden = NO;
    self.uploadImageView.image = [self imageResize:Image andResizeToWidth:50];
    
    SPHSingletonClass *sharedClass= [SPHSingletonClass sharedSingletonClass];
    
    NSString *movieSize = [NSByteCountFormatter stringFromByteCount:videoData.length countStyle:NSByteCountFormatterCountStyleFile];
    if([movieSize floatValue]>7.0){
        isStopDummyProgress = NO;
        [self performSelector:@selector(showDummyProgressToPrepareVideo) withObject:nil afterDelay:0.1];
        [self convertVideoToLowQuailtyWithInputURL:sharedClass.localVideopath size:[movieSize floatValue] Withhandler:^(NSData *outputData){
            isStopDummyProgress = YES;
            dispatch_async(dispatch_get_main_queue(), ^(void){
                sharedClass.selectVideoData = outputData;
                [self uploadWithData:outputData  contentDisposition:[VideoDisposition dataUsingEncoding:NSUTF8StringEncoding] isImage:NO];
            });
        }];
    }else{
        isStopDummyProgress = YES;
        sharedClass.selectVideoData = videoData;
        [self uploadWithData:videoData  contentDisposition:[VideoDisposition dataUsingEncoding:NSUTF8StringEncoding] isImage:NO];
    }
}

                // [self setupVideoForURL:[NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"OutPutFile.mp4"]]];
                //[self setupVideoForURL:localVideoUrl];

-(void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL size:(float)size Withhandler:(void (^)(NSData*))outputData{
    NSURL *outPutUrl = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"OutPutFile.mp4"]];
    [[NSFileManager defaultManager] removeItemAtURL:outPutUrl error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetMediumQuality];
    exportSession.outputURL = outPutUrl;
    exportSession.outputFileType = AVFileTypeMPEG4;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)  {
        switch ([exportSession status]) {
            case AVAssetExportSessionStatusUnknown:
                NSLog(@"Export SessionStatusUnknown");
                break;
            case AVAssetExportSessionStatusWaiting:
                NSLog(@"Export SessionStatusWaiting");
                break;
            case AVAssetExportSessionStatusExporting:
                NSLog(@"Export StatusExporting");
                break;
            case AVAssetExportSessionStatusFailed:
                NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Export canceled");
                break;
            case AVAssetExportSessionStatusCompleted: {
                NSData *urlData = [NSData dataWithContentsOfURL:exportSession.outputURL];
                NSString *movieSize = [NSByteCountFormatter stringFromByteCount:urlData.length countStyle:NSByteCountFormatterCountStyleFile];
                NSLog(@"After compression Movie size =%@",movieSize);
                outputData(urlData);
                break;
            }
            default: {
                break;
            }
        }
    }];
}


-(void)uploadWithData:(NSData *)someData contentDisposition:(NSData*)disposition isImage:(BOOL)isImage{
    currentprogres = 0.0;
    [self setProgressUI];
    self.uploadMessagelabel.text = @"Uploading, please be patient...";
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 2.4f);
    self.uploadProgressView.transform = transform;
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@?%@",VIDEO_UPLOAD_URL,self.mintDataStr] parameters:nil error:nil];
    
    if (isImage) {
        req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@?%@",IMAGE_UPLOAD_URL,self.mintDataStr] parameters:nil error:nil];
    }
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    
    NSString *boundary = [NSString  stringWithFormat:@"---------------------------14737809831466499882746641449"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [req addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:disposition];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:someData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [req setHTTPBody:body];
    
    uploadTask = [manager dataTaskWithRequest:req uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!cancelClicked)[self updateProgessForUpload:uploadProgress.fractionCompleted];
        });
    } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
        
    } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            _isCancelable = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *allData = [self dictionaryFromResponseData:responseObject];
                NSLog(@"allData =%@",allData);
                if ([[allData valueForKey:@"status"] isEqualToString:@"false"]) {
                    [self cancelUploadNow];
                    showAlert(@"Error!", [allData valueForKey:@"error_message"], @"Ok", nil);
                    return ;
                }
                if (!isVideo) {
                    currentprogres = 1.0;
                    self.uploadMessagelabel.text = @"Thats Minty...";
                    [self setProgressUI];
                    [self performSelector:@selector(uploadFinish) withObject:nil afterDelay:0.2];
                }else{
                    currentprogres = (currentprogres<0.52)?0.52:currentprogres+0.001;
                    [self setProgressUI];
                    self.uploadMessagelabel.text = @"Processing your mint...";
                    if(!cancelClicked){
                        [self showDummyProgressForProcessing];
                        [self updateProgressForVideoProcessing:[allData valueForKey:@"video_job_id"]];
                    }
                }
            });
        } else {
            self.uploadMessagelabel.text = @"Failed to Upload , Try again...";
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
        }
        
    }];
    [uploadTask resume];
}


-(void)updateProgressForVideoProcessing:(NSString *)jobID{
    NSURL *myURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://app.zencoder.com/api/v2/jobs/%@/progress.json?api_key=%@",jobID,ZENCODER_API_KEY]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:myURL];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        if(data) {
            NSDictionary *allData = [self dictionaryFromResponseData:data];
            CGFloat process = [[allData valueForKey:@"progress"] floatValue];
            if (![[[allData valueForKey:@"input"] valueForKey:@"state"] isEqualToString:@"finished"]) {
                currentprogres = (currentprogres<0.55)?0.55:currentprogres+0.001;
                [self performSelector:@selector(updateProgressForVideoProcessing:)withObject:jobID afterDelay:0.1];
            }else if (![[[[allData valueForKey:@"outputs"] objectAtIndex:0] valueForKey:@"state"] isEqualToString:@"finished"]) {
                [self performSelector:@selector(updateProgressForVideoProcessing:)withObject:jobID afterDelay:0.1];
            }else{
                currentprogres = (currentprogres<0.95)?0.95:currentprogres+0.001;
                [self updateDatabaseForVideoProcessingComplete:jobID];
            }
            [self updateProgressForProcessingVideo:process];
        }
    }];
}



-(void)updateProgessForUpload:(float)progress{
    if (progress-currentprogres > 0.9) {  // this is for Immideate wrong progress  Issue
        return;
    }
    NSLog(@"video upload progress= %f",progress);
    int progressFactor = (isVideo)?45:95;
    float newProgress = (progress/100)*progressFactor;
    currentprogres = (newProgress>currentprogres)?newProgress:currentprogres;
    [self setProgressUI];
    if (progress >= 0.95 && progress<1.0) {
        self.uploadMessagelabel.text = @"Saving your mint...";
    }else if (progress >= 1.0){
        [self showDummyProgress];
    }
}


-(void)showDummyProgress{
    if (isVideo && currentprogres>=0.5){
        return;
    }else if (currentprogres>=0.98){
        return;
    }
    currentprogres = currentprogres + 0.001;
    [self performSelector:@selector(showDummyProgress) withObject:nil afterDelay:0.1];
    [self setProgressUI];
}

-(void)showDummyProgressForProcessing{
    if (currentprogres>=0.55){
        return;
    }
    currentprogres = currentprogres + 0.001;
    [self performSelector:@selector(showDummyProgressForProcessing) withObject:nil afterDelay:0.1];
    [self setProgressUI];
}

-(void)showDummyProgressForFinalProcessing{
    if (currentprogres>=0.99){
        return;
    }
    currentprogres = currentprogres + 0.001;
    [self performSelector:@selector(showDummyProgressForFinalProcessing) withObject:nil afterDelay:0.1];
    [self setProgressUI];
}


-(void)setProgressUI{
    float percentage = currentprogres *100;
    self.uploadPercentage.text =[NSString stringWithFormat:@"%.f%%",percentage];
    self.uploadProgressView.progress= currentprogres;
}


-(void)updateProgressForProcessingVideo:(CGFloat)process{
    float zencoderProgress = 0.55 + (process/100)*.35;
    currentprogres = (zencoderProgress > currentprogres)?zencoderProgress:currentprogres;
    if (currentprogres <1.0)  {
        [self setProgressUI];
        if (currentprogres >=0.95 && currentprogres < 0.99 && isVideo)  {
            self.uploadMessagelabel.text = @"Almost Done...";
        }
    }
}

-(void)updateDatabaseForVideoProcessingComplete:(NSString *)jobID{
    [self performSelector:@selector(showDummyProgressForFinalProcessing) withObject:nil afterDelay:0.1];
    NSURL *myURL = [NSURL URLWithString:CLOUD_UPLOAD];
    NSData *submitData = [[NSString stringWithFormat:@"video_job_id=%@",jobID]  dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myURL];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
         currentprogres = 1.0;
         [self setProgressUI];
         self.uploadMessagelabel.text = @"Thats Minty...";
         [self performSelector:@selector(uploadFinish) withObject:nil afterDelay:0.1];
         if(data)  {
             NSLog(@"Response data = %@",[self dictionaryFromResponseData:data]);
         }
     }];
}



-(void)stopUpload{
    [uploadTask cancel];
}

-(void)stopConnection{
    [uploadTask cancel];
}

-(void)uploadFinish {
    [self.upDelegate uploadDidfinish:self];
}


-(void)cancelUploadNow{
    cancelClicked = YES;
    _isCancelable = YES;
    [uploadTask cancel];
    self.uploadMessagelabel.text = @"Upload Cancelled.";
    [self.upDelegate CanceluploadDidclicked:self];
}

-(void)headerViewSwipedLeft:(id)sender{
    CGRect finalFrame = self.baseView.frame;
    [UIView animateWithDuration:0.3 animations:^{
        self.baseView.frame = CGRectMake(-50.0, finalFrame.origin.y, finalFrame.size.width, finalFrame.size.height);
    }];
}

-(void)headerViewSwipedRight:(id)sender{
    CGRect finalFrame = self.baseView.frame;
    [UIView animateWithDuration:0.3 animations:^{
        self.baseView.frame = CGRectMake(0.0, finalFrame.origin.y, finalFrame.size.width, finalFrame.size.height);
    }];
}


- (IBAction)cancelUpload:(id)sender{
   if(self.isCancelable)
       [self cancelUploadNow];
    else
        [self.upDelegate CanceluploadDidclicked:self];
}


- (NSDictionary*)dictionaryFromResponseData:(NSData*)response{
    NSDictionary* dictionary = nil;
    NSString *myString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    if (response ) {
        id object = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            dictionary = (NSDictionary*)object;
        }  else  {
            dictionary = (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    NSLog(@"myString = %@",myString);
    return dictionary;
}

- (IBAction)btnPlayTapped:(UIButton *)sender {
    SPHSingletonClass  *sharedClass = [SPHSingletonClass sharedSingletonClass];
    NSURL *videourl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",sharedClass.localVideopath]];
    NSLog(@"videourl == %@",videourl);
    
    [self.upDelegate playUploadClicked:videourl ];
}


@end
