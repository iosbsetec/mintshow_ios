
#import <UIKit/UIKit.h>
#import "EntityModel.h"
typedef NS_ENUM(NSInteger, STTweetHotViewWord) {
    STTweetViewHandle = 0,
    STTweetViewHashtag,
    STTweetViewStartag,
    STTweetViewLink,
};

@protocol textSuggestDelegate;


@interface STTweetView : UIView
{
    NSString *currentInput;
}

@property (nonatomic) EntityType suggestionType;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) NSArray *validProtocols;
@property (nonatomic, assign) BOOL leftToRight;
@property (nonatomic, assign) BOOL textSelectable;
@property (nonatomic, assign) BOOL isStarCharBlocked; // In case of edit mint if is mint already content any private showroom in the description then no other showroom are allowed to be added to the description

@property (nonatomic, strong) UIColor *selectionColor;
@property (nonatomic, copy) void (^detectionBlock)(STTweetHotViewWord hotWord, NSString *string, NSString *protocol, NSRange range);

// *#@ Feature Implementation

@property (nonatomic, assign)  NSInteger  counterCount,nMaxTextLength;
@property (nonatomic,readwrite) NSRange   suggestionRange;
@property (nonatomic,readwrite,getter = isSuggesting)   BOOL      suggesting;
@property (nonatomic,strong) id <textSuggestDelegate> textDelegate;


- (void)setAttributes:(NSDictionary *)attributes;
- (void)setAttributes:(NSDictionary *)attributes hotWord:(STTweetHotViewWord)hotWord;
- (NSArray *)getAllShowRoomArrayFromTextView;
- (NSDictionary *)attributes;
- (NSDictionary *)attributesForHotWord:(STTweetHotViewWord)hotWord;
- (id)initWithFrame:(CGRect)frame;
- (CGSize)suggestedFrameSizeToFitEntireStringConstrainedToWidth:(CGFloat)width;

-(void)removeShowroomNameInTweetTextView:(NSString *)strSelectedShowRoomName;
-(void)appandAStringWithEntityType:(EntityType)textSuggestion AndString:(NSString*)fullWord andID:(NSString*)contactID;

-(void)wordSelectedForSuggestType:(EntityType)textSuggestion AndString:(NSString*)fullWord andID:(NSString*)contactID  forRange:(NSRange)range;
- (void)determineHotWordsNew;
@end

@protocol textSuggestDelegate
- (void)beginSuggestingForTextView:(EntityType)textSuggestType query:(NSString *)suggestionQuery Inrange:(NSRange)range;
- (void)endSuggesting;
- (void)reloadSuggestionsWithType:(EntityType)suggestionType query:(NSString *)suggestionQuery range:(NSRange)suggestionRange;
- (void)wordDeletedForSuggestType:(EntityType)textSuggestion andID:(NSString*)contactID;
- (void)textDidChange:(STTweetView*)commentView;
@end
