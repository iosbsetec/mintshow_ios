

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, STTweetHotWord) {
    STTweetMention = 0,
    STTweetHashtag,
    STTweetStartag,
    STTweetLink,
    STTEditTextTag,
    TweetActivityUserName,
  //  STTweetUserName
};

@interface STTweetLabel : UILabel
@property (strong) UITextView *textView;
@property (nonatomic, strong) NSArray *validProtocols;
@property (nonatomic, assign) BOOL leftToRight,isEditedMint;
@property (nonatomic, assign) BOOL textSelectable;
@property (nonatomic, strong) UIColor *selectionColor;
@property (nonatomic, strong) NSString *strUserName,*strFirstName,*strSecondName,*strDateTime;
@property (nonatomic, copy) void (^detectionBlock)(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range);
@property (nonatomic, assign) BOOL isActivityScreen;

- (void)setAttributes:(NSDictionary *)attributes;
- (void)setAttributes:(NSDictionary *)attributes hotWord:(STTweetHotWord)hotWord;

- (NSDictionary *)attributes;
- (NSDictionary *)attributesForHotWord:(STTweetHotWord)hotWord;

- (CGSize)suggestedFrameSizeToFitEntireStringConstrainedToWidth:(CGFloat)width;
-(void)setAttributeForfontTweetLabel:(UIFont *)font;
- (id)initWithFrame:(CGRect)frame withFont : (CGFloat ) fontSize  attributeTextColor : (UIColor *) attributeTextColor;
@end
