//
//  CameraFolderVw.m
//  Your Show
//
//  Created by BseTec on 11/19/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "CameraFolderVw.h"
#import "VideoModel.h"
#import <Photos/Photos.h>
@implementation CameraFolderVw
{


    NSString *title;
    
}




- (id)initWithFrame:(CGRect)frame mediaType : (BOOL)mediaType{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _mediaTypePhoto = mediaType;
        [self setupTableViewView];
       // [self getAllAlubums];
    }
    
    return self;
}
-(void)getAllAlubums
{

    PHFetchOptions *userAlbumsOptions = [PHFetchOptions new];
    userAlbumsOptions.predicate = [NSPredicate predicateWithFormat:@"estimatedAssetCount > 0"];
    
    PHFetchResult *userAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:userAlbumsOptions];
    
    [userAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
        NSLog(@"**** album titlellll %@", collection.localizedTitle);
    }];
}

/**
 A function that add the tableview to  the view which contain all the folderlist
 */

- (void)setupTableViewView
{
    UITableView *cameraDDTbl                = [[UITableView alloc] initWithFrame:CGRectMake(40,8, self.frame.size.width-80,self.frame.size.height-150)];
    cameraDDTbl.delegate                    = self;
    cameraDDTbl.dataSource                  = self;
    cameraDDTbl.separatorColor              = [UIColor blackColor];
    cameraDDTbl.backgroundColor             = [UIColor colorWithRed:(10.0/255.0) green:(19.0/255.0) blue:(40.0/255.0) alpha:1.0];
    cameraDDTbl.layer.cornerRadius          = 3.0;
    cameraDDTbl.layer.masksToBounds = YES;
    cameraDDTbl.layer.shadowOffset = CGSizeMake(-15, 20);
    cameraDDTbl.layer.shadowRadius = 5;
    cameraDDTbl.layer.shadowOpacity = 0.5;
    
    
    
    if (self.assetsLibrary == nil) {
        _assetsLibrary = [[ALAssetsLibrary alloc] init];
    }
    if (self.cameraItemsArray == nil) {
        _cameraItemsArray = [[NSMutableArray alloc] init];
    } else {
        [self.cameraItemsArray removeAllObjects];
    }
    
    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
        
        NSString *errorMessage = nil;
        switch ([error code]) {
            case ALAssetsLibraryAccessUserDeniedError:
            case ALAssetsLibraryAccessGloballyDeniedError:
                errorMessage = @"The user has declined access to it.";
                break;
            default:
                errorMessage = @"Reason unknown.";
                break;
        }

    };
    
    ALAssetsLibraryGroupsEnumerationResultsBlock listGroupBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        ALAssetsFilter *onlyPhotosFilter = (_mediaTypePhoto)?[ALAssetsFilter allPhotos]:[ALAssetsFilter allVideos];
        [group setAssetsFilter:onlyPhotosFilter];
        if ([group numberOfAssets] > 0)
        {
            [self.cameraItemsArray addObject:group];
        }
        else
        {
            [cameraDDTbl performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }
    };
    
    
    NSLog(@"Items %@",_cameraItemsArray);
    
    // enumerate only photos
    NSUInteger groupTypes = ALAssetsGroupAlbum | ALAssetsGroupEvent | ALAssetsGroupFaces | ALAssetsGroupSavedPhotos;
    [self.assetsLibrary enumerateGroupsWithTypes:groupTypes usingBlock:listGroupBlock failureBlock:failureBlock];

    [self addSubview:cameraDDTbl];
}


#pragma mark ---  TableView DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"Count =%lu",(unsigned long)self.cameraItemsArray.count);
    return self.cameraItemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cameraRollTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cameraRollTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cameraRollTableIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    
    cell.selectionStyle         = UITableViewCellSelectionStyleNone;
    ALAssetsGroup *groupForCell = self.cameraItemsArray[indexPath.row];
    cell.textLabel.text         = [groupForCell valueForProperty:ALAssetsGroupPropertyName];
    cell.textLabel.font         = [UIFont fontWithName:@"Helvetica" size:17];
    cell.textLabel.textColor    = [UIColor whiteColor];
    cell.detailTextLabel.text   = [@(groupForCell.numberOfAssets) stringValue];
    return cell;
}


#pragma mark ---  TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Did Select");
    _assetsGroup = self.self.cameraItemsArray[indexPath.row];
    [self changeSelectedImages];
}


#pragma mark --- *** End *** TableView Data Stores and Delegate ---


-(void)changeSelectedImages
{

    if ([self.assets count]) {
        [self.assets removeAllObjects];
    }
    
    
    if (!self.assets) {
        _assets = [[NSMutableArray alloc] init];
    } else {
        [self.assets removeAllObjects];
    }
    
    /**
     A string to represent the individual folder name
     */
    title = [self.assetsGroup valueForProperty:ALAssetsGroupPropertyName];

    
    /**
     An enumeration of all assets
     */

    ALAssetsGroupEnumerationResultsBlock assetsEnumerationBlock = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (result) {
            if (_mediaTypePhoto) {
                if (_isShowroomPicker)
                {
                    ALAssetRepresentation *defaultRepresentation = [result defaultRepresentation];
                    UIImage *latestPhoto= [UIImage imageWithCGImage:[defaultRepresentation fullScreenImage]];
                    [self.assets addObject:latestPhoto];
                }
                else
                {
                    [self.assets addObject:result];
                    [self.assets sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                        NSDate *date1 = [obj1 valueForProperty:ALAssetPropertyDate];
                        NSDate *date2 = [obj2 valueForProperty:ALAssetPropertyDate];
                        
                        return ([date1 compare:date2] == NSOrderedAscending ? NSOrderedDescending : NSOrderedAscending);
                    }];
                }
            }
            else
            {
                ALAssetRepresentation *rep  = [result defaultRepresentation];
                Byte *buffer                = (Byte*)malloc((NSUInteger)rep.size);
                NSUInteger buffered         = [rep getBytes:buffer fromOffset:0.0 length:(NSUInteger)rep.size error:nil];
                
                
                CGImageRef thum         = [result thumbnail];
                VideoModel *aModel      = [VideoModel new];
                aModel.dataGaleryVideo  = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
                aModel.videoDuration    = [result valueForProperty:ALAssetPropertyDuration];
                aModel.videoLocalUrlAsset = [result valueForProperty:ALAssetPropertyAssetURL];
                aModel.thumbImage       = [UIImage imageWithCGImage:thum];
                [self.assets addObject:aModel];
                aModel = nil;

            
            }
        }
    };
    
    ALAssetsFilter *mediaFilter =  (_mediaTypePhoto)?[ALAssetsFilter allPhotos]:[ALAssetsFilter allVideos];
    [self.assetsGroup setAssetsFilter:mediaFilter];
    [self.assetsGroup enumerateAssetsUsingBlock:assetsEnumerationBlock];
     NSLog(@"assetsArray %@",_assets);
    [self closeAnimationZoom];

    [self.aDelegate selectedFolderGroup:title AssetList:self.assets];
}

/**
 A function that show the view with some Animation
 */
-(void)showAnimationZoom
{
    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                self.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
}

/**
 A function that close the view with some Animation
 */
-(void)closeAnimationZoom
{
    [UIView animateWithDuration:0 animations:^{
        self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    }completion:^(BOOL finished){
        // Finished scaling down imageview, now resize it
        [UIView animateWithDuration:0.3/2 animations:^{
            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        }completion:^(BOOL finished){
            [self removeFromSuperview];
        }];
    }];
}


@end
