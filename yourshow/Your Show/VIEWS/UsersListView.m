//
//  ListViewCell.m
//  SampleDemo
//
//  Created by bsetec on 11/30/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#define API_GETREMINTEDUSERS                  @"mint/get_reminted_users_list"
#define API_GETNOMINATEDUSERS                @"achievements/get_nominated_users"
#define API_GETHIGHFIVEDUSERS                @"/achievements/get_highfived_users"


#import "UsersListView.h"
#import "UIView+Explode.h"
#import "objc/runtime.h"
#import "CellUserList.h"
#import "UsesListInfo.h"

typedef void(^GESTURE_Tapped)(void);
static NSString *GESTURE_BLOCK = @"GESTURE_BLOCK";

@interface UIView (PrivateExtensions)

-(void)setTappedGestureWithBlock:(GESTURE_Tapped)block;

@end



@implementation UsersListView
{
    UITableView *tblUserList;
    NSString *strViewTitle ;
    UIView *viewTable;
    
    NSMutableArray *arrUserList;
    UIButton *btnCancel ;
    NSString *strType;
    
    NSInteger numberOfRecords;

}


#pragma mark -
#pragma mark Class methods

+ (UsersListView *)showUserListViewTo:(UIView *)view viewTitle:(MSUserListType )userListType numberofPeoples:(NSInteger)peopleCount feedId:(NSString *)feedId delegate:(id<UsersListDelegate>)theDelegate
{
    
    UIView *viewToRemove = nil;
    for (UIView *v in [view subviews]) {
        if ([v isKindOfClass:[UsersListView class]]) {
            viewToRemove = v;
        }
    }
    
    
    if (viewToRemove == nil) {
      
    UsersListView *hud = [[UsersListView alloc] initWithFrame:[[UIScreen mainScreen] bounds] title:userListType withFeedId:feedId countVal:peopleCount];
        hud.aDelegate = theDelegate;
    [view addSubview:hud];
    [hud show:YES];
    return [hud autorelease];
    }
    return nil;
}
//static id<UsersListDelegate> _classDelegate = nil;
//
//+ (id<UsersListDelegate>)classDelegate
//{
//    return _classDelegate;
//}
//
//+ (void)setClassDelegate(id<UsersListDelegate>)delegate
//{
//    _classDelegate = delegate;
//}
+ (BOOL)isViewAlreadyAdded:(UIView *)view
{

    
    UIView *viewToRemove = nil;
    for (UIView *v in [view subviews]) {
        if ([v isKindOfClass:[UsersListView class]]) {
            viewToRemove = v;
        }
    }
    
    
    if (viewToRemove != nil) {
        return YES;
    } else {
        return NO;
    }
    
    
  

}
- (void)show:(BOOL)animated {
    
        [self setNeedsDisplay];
}
- (void)hide:(BOOL)animated {
    
    [self removeFromSuperview];
}
+ (BOOL)hideHUDForView:(UIView *)view animated:(BOOL)animated {
    UIView *viewToRemove = nil;
    for (UIView *v in [view subviews]) {
        if ([v isKindOfClass:[UsersListView class]]) {
            viewToRemove = v;
        }
    }
    if (viewToRemove != nil) {
        UsersListView *HUD = (UsersListView *)viewToRemove;
        [HUD hide:animated];
        return YES;
    } else {
        return NO;
    }
}



- (id)initWithFrame:(CGRect)frame title:(MSUserListType )userListType withFeedId:(NSString *)feedId countVal : (NSInteger )counter
{
    self = [super initWithFrame:frame];
    if (self)
    {
        numberOfRecords = counter;

        arrUserList = [NSMutableArray new];
        self.backgroundColor =  [[UIColor blackColor]colorWithAlphaComponent:0.6];
        NSString *urlString = nil;

        switch (userListType) {
            case 0:
                urlString = API_GETHIGHFIVEDUSERS;
                strViewTitle =  [NSString stringWithFormat:@"%ld %@",(long)counter,(counter>1)?@"High Fives":@"High Five"];
                strType = @"HighFiveCount";

                break;
                
            case 1:
              
                break;
                
                
            case 2:
                urlString = API_GETREMINTEDUSERS;
                strViewTitle =  [NSString stringWithFormat:@"%ld %@",(long)counter,(counter>1)?@"Remints":@"Remint"];
                strType = @"RemintCount";

                break;
                
                
            case 3:
                urlString = API_GETNOMINATEDUSERS;
                strViewTitle =  [NSString stringWithFormat:@"%ld %@",(long)counter,(counter>1)?@"Nominations":@"Nomination"];
                strType = @"NominationCount";

                break;
                
                
                
            default:
                break;
        }
        [self createTableviewWithFrame:120];

         [self getUserListwithFeedId:feedId url:urlString];

     }
    return self;
}

- (void)getUserListwithFeedId:(NSString *)feedID url:(NSString *)apiUrl
{
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&feed_id=%@",GETVALUE(CEO_AccessToken),feedID] dataUsingEncoding:NSUTF8StringEncoding];
    [self callAPI:submitData andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,apiUrl]]];
}
-(void)setTableViewFrame
{
        CGFloat height = tblUserList.rowHeight;
        height *= arrUserList.count;
        
        CGRect tableFrame = tblUserList.frame;
        tableFrame.size.height = height;
        tblUserList.frame = tableFrame;
        tblUserList.center = self.center;
   

}
-(void)callAPI:(NSData *)submitData andAPIUrl:(NSURL *)myUrl
{
    [MBProgressHUD showHUDAddedTo:self animated:YES];

    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
        [submitrequest setHTTPMethod:@"POST"];
        [submitrequest setHTTPBody:submitData];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSMutableDictionary *dataArray = [[NSMutableDictionary alloc]initWithDictionary:[YSSupport dictionaryFromResponseData:data]];
         NSLog(@"%@",dataArray);
         if ([[dataArray valueForKey:@"StatusCode"] integerValue]==1)
         {
             if ([strType isEqualToString:@"RemintCount"])
             {
                 for (NSDictionary *dict in [dataArray valueForKey:@"reminted_users"]) {
                     UsesListInfo *obj = [[UsesListInfo alloc]initWithUserInfo:dict];
                    [arrUserList addObject:obj];
                 }
             }
             else if ([strType isEqualToString:@"HighFiveCount"])
             {
                 for (NSDictionary *dict in [dataArray valueForKey:@"highfived_users"]) {
                     UsesListInfo *obj = [[UsesListInfo alloc]initWithUserInfo:dict];
                     [arrUserList addObject:obj];
                 }
             }
             else if ([strType isEqualToString:@"NominationCount"])
             {
                 for (NSDictionary *dict in [dataArray valueForKey:@"nominated_users"]) {
                     UsesListInfo *obj = [[UsesListInfo alloc]initWithUserInfo:dict];
                     [arrUserList addObject:obj];
                 }
             }
//             if (arrUserList.count>4) {
//                 [self setTableViewFrame];
//             }
             [tblUserList reloadData];
         }
         [MBProgressHUD hideHUDForView:self animated:YES];
     }];
}



-(id)createTableviewWithFrame:(CGFloat)yPos
{
    CGFloat selfWidth = CGRectGetWidth(self.frame);
    CGFloat selfheight = CGRectGetHeight(self.frame);
    CGFloat maxHeight =  selfheight- 120;
    CGFloat requiredHeight = 40+(45*numberOfRecords);
    if (requiredHeight>maxHeight) {
        requiredHeight = maxHeight;
    }
    
    viewTable = [[UIView alloc]initWithFrame:CGRectMake((selfWidth-250)/2.0,0,250,requiredHeight)];
    viewTable.layer.cornerRadius = 5;
    viewTable.center = self.center;
    viewTable.clipsToBounds = YES;
    
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,viewTable.frame.size.width , 40)];
    lblTitle.text = strViewTitle;
    [lblTitle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.backgroundColor = [UIColor colorWithRed:(9/255.0) green:(26/255.0) blue:(53/255.0) alpha:1.0];
    
    
    viewTable.backgroundColor = [UIColor colorWithRed:(9/255.0) green:(26/255.0) blue:(53/255.0) alpha:1.0];
    
    UIImage * buttonImage = [UIImage imageNamed:@"Close.png"];
    UIImageView *dot       =[[UIImageView alloc] initWithFrame:CGRectMake(10,12,16,16)];
    dot.image           = buttonImage;
    
    tblUserList = [[UITableView alloc] initWithFrame:CGRectMake(1,40, 248,viewTable.frame.size.height-40) style:UITableViewStylePlain];
    tblUserList.delegate = self;
    tblUserList.dataSource = self;
    tblUserList.backgroundColor = [UIColor whiteColor];
    tblUserList.layer.shadowOpacity = 0.8;
    tblUserList.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    tblUserList.scrollEnabled = YES;
    [tblUserList setClipsToBounds:YES];
    //tblUserList.layer.cornerRadius = 4.0;
    

//    UIBezierPath *maskPath  = [UIBezierPath bezierPathWithRoundedRect:tblUserList.bounds byRoundingCorners:UIRectCornerBottomLeft| UIRectCornerBottomRight                                                         cornerRadii:CGSizeMake(4.0, 4.0)];
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//    maskLayer.frame         = tblUserList.bounds;
//    maskLayer.path          = maskPath.CGPath;
//    tblUserList.layer.mask    = maskLayer;
    
    [viewTable setCenter:self.center];
    
    [viewTable addSubview:tblUserList];
    [viewTable addSubview:lblTitle];
    [viewTable addSubview:dot];
    [self addSubview:viewTable];
    
    btnCancel          = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setFrame:CGRectMake(0, 0, 35, 35)];
    btnCancel.layer.cornerRadius = 4.0;
    [btnCancel setBackgroundColor:[UIColor clearColor]];
    [btnCancel addTarget:self action:@selector(cancelTapped) forControlEvents:UIControlEventTouchUpInside];
    [viewTable addSubview:btnCancel];

    return self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return   [arrUserList count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CellUserList";
    CellUserList *cell = (CellUserList*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    }
    
    UsesListInfo *userInfo = [arrUserList objectAtIndex:indexPath.row];
    if ([userInfo.follow_status isEqualToString:@"OWNER"]) {
        [cell.btnFollow setHidden:YES];
    }
    else
    {
        [cell.btnFollow setHidden:NO];
        [cell.btnFollow setTitle:userInfo.follow_status forState: UIControlStateNormal];
        [cell.btnFollow setBackgroundColor: ([userInfo.follow_status isEqualToString:@"FOLLOW"]) ?ORANGECOLOR:[UIColor whiteColor]];
        [cell.btnFollow setTitleColor:([userInfo.follow_status isEqualToString:@"FOLLOW"])?[UIColor whiteColor]:ORANGECOLOR forState:UIControlStateNormal];
        [cell.btnFollow.layer setBorderColor:ORANGECOLOR.CGColor];
        [cell.btnFollow.layer setBorderWidth:1.0];
        [cell.btnFollow addTarget:self action:@selector(followUnfollowUser:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnFollow setTag:indexPath.row];
    }
    [cell.btnUserImage addTarget:self action:@selector(userImageSelect:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnUserImage.tag = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setInfo:userInfo.user_thumb_image userName:userInfo.user_name];

    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *userId =  [[arrUserList objectAtIndex:indexPath.row] user_id];
    [self.aDelegate selectedUserIdFromHighfiveList:userId];
    arrUserList = nil;
    [self removeFromSuperview];
}

-(void)cancelTapped
{
    arrUserList = nil;
    [self removeFromSuperview];
}

-(void)userImageSelect:(UIButton *) sender
{
    
    NSString *userId =  [[arrUserList objectAtIndex:sender.tag] user_id];
    [self.aDelegate selectedUserIdFromHighfiveList:userId];
    arrUserList = nil;
    [self removeFromSuperview];


}
-(void)createLetterPressImages
{
    //    strViewTitle = viewTitle;
    [self createTableviewWithFrame:120];
    
    __unsafe_unretained typeof(self) weakSelf = self;
//    [self addSubview:viewTable];
    
    [btnCancel setTappedGestureWithBlock:^{
        [weakSelf lp_explodeWithCallback:^{
            NSLog(@"callback");
            [weakSelf removeFromSuperview];
        }];
        [weakSelf setTappedGestureWithBlock:nil];
    }];
}
-(void)followUnfollowUser:(UIButton *)sender {
    UsesListInfo *feedlist = [arrUserList objectAtIndex:[sender tag]];
    [self changeFollowStatusForRow:sender.tag];

    [YSAPICalls followUnfollowMint:feedlist withSuccessionBlock:^(id newResponse)  {
        Generalmodel *gModel = newResponse;
        if ([gModel.status_code isEqualToString:@"1"]) {
            if ([gModel.status_Msg isEqualToString:@"followed"] || [gModel.status_Msg isEqualToString:@"unfollowed"]) {

                [self.aDelegate updateStatus:feedlist.user_id status:([gModel.status_Msg isEqualToString:@"followed"])?1:0];

        } else {
            NSLog(@"Failiur Msg=%@",gModel.status_Msg);
            [self changeFollowStatusForRow:sender.tag];

        }
        }
    } andFailureBlock:^(NSError *error){
        NSLog(@"Error = %@",error);
        [self changeFollowStatusForRow:sender.tag];

    }];
}






-(void)changeFollowStatusForRow:(NSInteger)row {
    UsesListInfo *userInfo = [arrUserList objectAtIndex:row];

    userInfo.follow_status   = ([userInfo.follow_status isEqualToString:@"FOLLOW"])?@"IN CIRCLE":@"FOLLOW";
    [arrUserList replaceObjectAtIndex:row withObject:userInfo];
    [tblUserList reloadData];
    [tblUserList reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];


}
@end

@implementation UIView (PrivateExtensions)

-(void)setTappedGestureWithBlock:(GESTURE_Tapped)block
{
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    tap.numberOfTapsRequired=1;
    [self addGestureRecognizer:tap];
    
    objc_setAssociatedObject(self,&GESTURE_BLOCK,block, OBJC_ASSOCIATION_COPY);
}

-(void)tapped:(UIGestureRecognizer *)gesture
{
    if (gesture.state==UIGestureRecognizerStateEnded)
    {
        GESTURE_Tapped block = (GESTURE_Tapped)objc_getAssociatedObject(self, &GESTURE_BLOCK);
        if (block)
        {
            block();
            block = nil;
            [self removeFromSuperview];
        }
    }
}

@end





