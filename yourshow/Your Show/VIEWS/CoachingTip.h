//
//  CoachingTip.h
//  Your Show
//
//  Created by BseTec on 2/2/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoachingTip : UIView
{
    NSString *strCurrentScreenName;
    BOOL isHeaderPresent;
}
+ (CoachingTip *)showCoachingTip:(UIView *)mainView ForScreen:(NSString *)screen headerPresent:(BOOL)isPresent;
//- (id)initWithFrame:(CGRect)frame screenName : (NSString *)scrName;
- (id)initWithFrame:(CGRect)frame screenName : (NSString *)scrName headerPresent:(BOOL)isPresent;
@end
