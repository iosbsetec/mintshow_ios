//
//  CoachingTip.m
//  Your Show
//
//  Created by BseTec on 2/2/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "CoachingTip.h"
#import "YSAPICalls.h"

@implementation CoachingTip

#define DISCOVERINFO
#define ADDPHOTO @"Add a photo or video"
#define ADDMINTDESCRIPTION @""
#define ADDMINTDESCRIPTIONDETAIL @"Use the * when you want to send\na Mint to a specific Showroom\nor choose one below."
#define ADDYOUTUBE  @"Search Youtube to find a video"
#define ABOUTDESC   @"Share your achievement and inspire\nothers by writing your thoughts here."
#define ADDCATEGORY @"Choose which category your Mints\nis related to"
#define ADDSHOWROOM @"Choose which showroom you want\nto post your mint."
#define ADDSHARE    @"Choose which platform you want\nto share your mint."

- (id)initWithFrame:(CGRect)frame screenName : (NSString *)scrName headerPresent:(BOOL)isPresent
{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self onCreateTransparentView];
        strCurrentScreenName = scrName;
        isHeaderPresent      = isPresent;
        [self onShowPopUpWithScreenName:scrName];
       // [self createCoachingTips:scrName];
        //[self performSelector:@selector(removeFromSuperview) withObject:self afterDelay:5.0f];
    }
    
    return self;
}

-(void)onCreateTransparentView
{
    UIView *viewBG = [[UIView alloc]initWithFrame:self.frame];
    viewBG.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.60];
    [self addSubview:viewBG];
}
+ (CoachingTip *)showCoachingTip:(UIView *)mainView ForScreen:(NSString *)screen headerPresent:(BOOL)isPresent
 {
    CoachingTip *coach = [[CoachingTip alloc] initWithFrame:mainView.frame screenName:screen headerPresent:isPresent];
    [[[UIApplication sharedApplication] keyWindow] addSubview:coach];
    return coach;
}
-(void)onShowPopUpWithScreenName:(NSString *)strScreenName
{
    if ([strScreenName isEqualToString:@"Discover"])
    {
        [self createCoachingTipViewWithText:@"Discover and be inspired!" Desc:@"Browse Mints and Showrooms from the community." xOrigin:10 yOrigin:self.frame.size.height-135 width:self.frame.size.width-20 tipHeight:75 arrowNeeded:NO attributedaString:nil];
    }
    else if ([strScreenName isEqualToString:@"AddMint"])
    {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:ADDMINTDESCRIPTIONDETAIL];
        
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
        textAttachment.image = [UIImage imageNamed:@"asterisk.png"];
        
        NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        
        [attributedString replaceCharactersInRange:NSMakeRange(8, 1) withAttributedString:attrStringWithImage];
        
        
        [self createCoachingTipViewWithText:ADDMINTDESCRIPTION Desc:nil xOrigin:10 yOrigin:150 width:self.frame.size.width-20 tipHeight:60 arrowNeeded:YES attributedaString:attributedString];
    }
    else if ([strScreenName isEqualToString:@"ParticularShowroomJoin"])
    {
        [self createCoachingTipViewWithText:nil Desc:@"Showrooms are where Mints are\nshowcased. Post your Mints to specific\nShowrooms to support a movement,\njoin a conversation, or inspire others." xOrigin:10 yOrigin:151 width:self.frame.size.width-70 tipHeight:80 arrowNeeded:YES attributedaString:nil];
    }
    else if ([strScreenName isEqualToString:@"Mintdetail"])
    {
        [self createCoachingTipViewWithText:@"Click here to follow." Desc:nil xOrigin:self.frame.size.width-175 yOrigin:(isHeaderPresent)?85:40 width:self.frame.size.width-150 tipHeight:40 arrowNeeded:YES attributedaString:nil];
    }
    
    

}

-(void)setBorder:(UILabel*)lbl
{
    lbl.layer.borderColor = [UIColor whiteColor].CGColor;
    lbl.layer.borderWidth = 2.0;
}

-(void)createCoachingTipViewWithText:(NSString *)strTitle Desc:(NSString *)strDesc  xOrigin:(CGFloat)xPos yOrigin:(CGFloat)yPos width:(CGFloat)width  tipHeight:(CGFloat)height arrowNeeded:(BOOL)arrowNeeded attributedaString:(NSAttributedString *)addmint
{
                      
    UIView *viewTips = [[UIView alloc]initWithFrame:CGRectMake(xPos, yPos, width, height)];
    viewTips.backgroundColor = ORANGECOLOR;
    viewTips.layer.cornerRadius = 4.0;
    [self addSubview:viewTips];
    
    if (arrowNeeded)
    {
        float xPos = ([strCurrentScreenName isEqualToString:@"Mintdetail"])?(self.frame.size.width-40):((viewTips.frame.size.width-15)/2)+viewTips.frame.origin.x;
        UIImageView *imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(xPos, yPos-10, 15,10)];
        imgArrow.image = [UIImage imageNamed:@"Uparrow.png"];
        [imgArrow setTag:123];
        [self addSubview:imgArrow];
    }
    else if([strCurrentScreenName isEqualToString:@"Discover"])
    {
        UIImageView *imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(88, height+yPos, 15,10)];
        imgArrow.image = [UIImage imageNamed:@"down_arrow.png"];
        [imgArrow setTag:123];
        [self addSubview:imgArrow];
    }


    if (strTitle)
    {
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, viewTips.frame.size.width-20, 30)];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.text = strTitle;
        [viewTips addSubview:lblTitle];

    }

    
    CGFloat lblHeight = (strTitle.length)?(height-30):height;
    UILabel *lblDescription = [[UILabel alloc]initWithFrame:CGRectMake(10, (strTitle.length)?30:0, viewTips.frame.size.width-20, lblHeight)];
    lblDescription.backgroundColor = [UIColor clearColor];
    lblDescription.textColor = [UIColor whiteColor];
    lblDescription.textAlignment = NSTextAlignmentCenter;
    lblDescription.numberOfLines = 0;
    if ([addmint length]) {
        lblDescription.attributedText = addmint;
        lblDescription.font = [UIFont fontWithName:@"Helvetica" size:14];
    }
    else
    {
        lblDescription.text = strDesc;
        lblDescription.font = [UIFont fontWithName:@"Helvetica" size:13];
    }
    [viewTips addSubview:lblDescription];
    
//    [self setBorder:lblDescription];
    
    UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    btnClose.frame = CGRectMake(viewTips.frame.size.width-20, 0, 25, 25);
    [btnClose addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgClose = [[UIImageView alloc]initWithFrame:CGRectMake(btnClose.frame.origin.x+6, btnClose.frame.origin.y+5, 10,10)];
    imgClose.image = [UIImage imageNamed:@"Close.png"];
    [viewTips addSubview:imgClose];
    [viewTips addSubview:btnClose];
    [viewTips setTag:1234];
}


-(void)onClose
{
    [self createCoachingTips:strCurrentScreenName];
    [[self viewWithTag:1234]removeFromSuperview];
    [[self viewWithTag:123]removeFromSuperview];
    [self removeFromSuperview];
}


/*-(void)onClose
{
    [[self viewWithTag:1234]removeFromSuperview];// Removing Orange BG Tip View
    [[self viewWithTag:123]removeFromSuperview]; // Removing Arrow Image
    
    if([strCurrentScreenName isEqualToString:@"AddMint"])
    {
        strCurrentScreenName = @"AddPhotoTip";
        [self createCoachingTipViewWithText:nil Desc:ADDPHOTO xOrigin:35 yOrigin:90 width:170 tipHeight:30 arrowNeeded:YES attributedaString:nil];
    }
    else if([strCurrentScreenName isEqualToString:@"AddPhotoTip"])
    {
        strCurrentScreenName = @"YoutubeTip";
        [self createCoachingTipViewWithText:nil Desc:ADDYOUTUBE xOrigin:(self.frame.size.width-140) yOrigin:90 width:140 tipHeight:40 arrowNeeded:YES attributedaString:nil];
    }
    else if([strCurrentScreenName isEqualToString:@"YoutubeTip"])
    {
        strCurrentScreenName = @"AddDescTip";
        [self createCoachingTipViewWithText:nil Desc:ABOUTDESC xOrigin:20 yOrigin:210 width:(self.frame.size.width-40) tipHeight:50 arrowNeeded:YES attributedaString:nil];
    }
    else if([strCurrentScreenName isEqualToString:@"AddDescTip"])
    {
        strCurrentScreenName = @"CategoryTip";
        [self createCoachingTipViewWithText:nil Desc:ADDCATEGORY xOrigin:20 yOrigin:240 width:(self.frame.size.width-40) tipHeight:50 arrowNeeded:YES attributedaString:nil];
    }
    else if([strCurrentScreenName isEqualToString:@"CategoryTip"])
    {
        strCurrentScreenName = @"ShowroomTip";
        [self createCoachingTipViewWithText:nil Desc:ADDSHOWROOM xOrigin:20 yOrigin:370 width:(self.frame.size.width-40) tipHeight:50 arrowNeeded:YES attributedaString:nil];
    }
    else if([strCurrentScreenName isEqualToString:@"ShowroomTip"])
    {
        strCurrentScreenName = @"ShareTip";
        [self createCoachingTipViewWithText:nil Desc:ADDSHARE xOrigin:20 yOrigin:340 width:(self.frame.size.width-40) tipHeight:50 arrowNeeded:YES attributedaString:nil];
        [self createCoachingTips:@"AddMint"];   // Need to call here

    }
   
    else
    {
        [self removeFromSuperview];
        [self createCoachingTips:strCurrentScreenName];   // Need to call here

    }
}
 */

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self removeFromSuperview];
}

-(void)createCoachingTips: (NSString *)screenName
{
//    query
//    1 => Discover All Page - when user close coaching tip, 2 => Particular Showroom Page Join - when user close coaching tip, 3 => Mint detail page - coaching tip
//    
    
    NSString* visittype;
    
    if ([screenName isEqualToString:@"Discover"]) {
        visittype = @"1";

    }
    else if ([screenName isEqualToString:@"ParticularShowroomJoin"]) {
        visittype = @"2";
        
    }
    if ([screenName isEqualToString:@"Mintdetail"]) {
        visittype = @"3";
        
    }
    
    if ([screenName isEqualToString:@"AddMint"]) {
        visittype = @"4";
        
    }
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&visit_type=%@",GETVALUE(CEO_AccessToken),visittype] dataUsingEncoding:NSUTF8StringEncoding];
    [YSAPICalls UpdateCoachingTipsFirstTimeVisit:submitData
                  ForSuccessionBlock:^(id newResponse) {
                      Generalmodel *model = newResponse;
                      
                      if ([model.status_code isEqualToString:@"1"])
                      {
                    
                          if ([screenName isEqualToString:@"Discover"]) {
                              SETVALUE(@"no", CEO_DISCOVERALLFIRSTTIMEVISIT);
                          }
                          else if ([screenName isEqualToString:@"ParticularShowroomJoin"]) {
                              SETVALUE(@"no",CEO_PARTCULARSHOWROOMFIRSTTIMEVISIT);
                          }
                          else if ([screenName isEqualToString:@"Mintdetail"]) {
                              SETVALUE(@"no", CEO_ISMINTDETAILFIRSTTIMEVISIT);
                          }
                          else if([screenName isEqualToString:@"AddMint"])
                          {
                              SETVALUE(@"no", CEO_ISADDMINTFIRSTTIMEVISIT);
                          }
                        
                          SYNCHRONISE;
                      }
                  } andFailureBlock:^(NSError *error) {
                      
                      
                  }];

}


@end
