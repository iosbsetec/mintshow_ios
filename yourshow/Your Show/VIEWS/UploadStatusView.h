//
//  UploadStatusView.h
//  Your Show
//
//  Created by Siba Prasad Hota on 06/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol uploadDelegate;

@interface UploadStatusView : UIView

@property (nonatomic,strong) id <uploadDelegate> upDelegate;

@property (strong, nonatomic) IBOutlet UIImageView *uploadImageView;
@property (strong, nonatomic) IBOutlet UILabel *uploadMessagelabel;
@property (strong, nonatomic) IBOutlet UIProgressView *uploadProgressView;
@property (strong, nonatomic) IBOutlet UILabel *uploadPercentage;
@property (strong, nonatomic) NSString *mintDataStr;
@property (assign, nonatomic) BOOL isCancelable;

+ (id)UploadStatusViewInit;

-(void)setUploadStatusData:(NSDictionary*)uploaddata;
-(void)uploadImageWithData:(NSData *)ImageData withImage:(UIImage*)Image;
-(void)uploadVideoWithData:(NSData *)videoData withImage:(UIImage*)Image;
-(void)stopUpload;

- (IBAction)cancelUpload:(id)sender;

@end


@protocol uploadDelegate

-(void)uploadDidfinish:(UploadStatusView*)uploadview;
-(void)CanceluploadDidclicked:(UploadStatusView*)uploadview;
-(void)playUploadClicked:(NSURL*)videoUrl;

@end

