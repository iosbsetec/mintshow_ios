//
//  CameraFolderVw.h
//  Your Show
//
//  Created by POTHIRAJ on 11/19/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//


/**
 Simple class to represent List of folder exist inthe Devide for Media like photo or videos.
 */

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@protocol CameraFolderVwDelegate <NSObject>

-(void)selectedFolderGroup : (NSString *)title AssetList : (NSArray *) list;

@end


@interface CameraFolderVw : UIView<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,assign)  id <CameraFolderVwDelegate> aDelegate;

@property (nonatomic, strong) ALAssetsLibrary   *assetsLibrary;/**< Some height measurement */
@property (nonatomic, strong) NSMutableArray    *cameraItemsArray; /**< list of folder names and contained list of individual folder */
@property (nonatomic, strong) NSMutableArray    *assets; /**< contained list of individual folder */
@property (nonatomic, strong) ALAssetsGroup     *assetsGroup; /**< ordered set of assets. The order of its elements is the order that the user sees in the Photos application */

@property (nonatomic,assign) BOOL mediaTypePhoto;
@property (nonatomic,assign) BOOL isShowroomPicker;

-(void)showAnimationZoom;
-(void)closeAnimationZoom;
- (id)initWithFrame:(CGRect)frame mediaType : (BOOL)mediaType;

@end
