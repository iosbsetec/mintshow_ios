//
//  MSSelectInterests.h
//  Your Show
//
//  Created by Siba Prasad Hota on 12/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MSSelectInterests : UIView

+ (MSSelectInterests *)showSelectInterestViewTo:(UIView *)view withViewcontainerFrame : (CGRect) containterframe;
+ (BOOL)hideSelectInterestView:(UIView *)view animated:(BOOL)animated;
@end
