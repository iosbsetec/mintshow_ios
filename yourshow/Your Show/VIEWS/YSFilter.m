

#import "YSFilter.h"
#import "Cell2.h"
#import "Cell1.h"
#define arrayRightTableData @[@"Search By"@"Search Category"]

#define arraySearchBy @[@"All Members",@"My Mints",@"My C-suite",@"Planned to do",@"Nomintated"]
#define arrayCategory   @[@"BeingHappy",@"Career",@"EnterPrenure",@"ExerCise"]

@interface YSFilter ()
{

    IBOutlet UITableView *tblFilter;
   


}

@property (nonatomic,retain)  NSMutableArray* catArray1;
@property (nonatomic,retain)  NSMutableArray* AllMintTypeArray;
@end
@implementation YSFilter


/*
+(void)presentInViewController:(UIViewController*) viewController
{
    // Instantiating encapsulated here.
    EPPZSubclassedViewOwner *owner = [EPPZSubclassedViewOwner new];
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:owner options:nil];
    
    // Add to the view hierarchy (thus retain).
    [viewController.view addSubview:owner.subclassedView];
}

-(IBAction)dismiss
{
    [self removeFromSuperview];
}
*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        //[self commonInit];
        [tblFilter reloadData];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       // [self commonInit];
    }
    return self;
}


#pragma mark - UITableView Delegates

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
   return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
           return 40;
    
   }
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
        if (self.isOpen) {
            if (self.selectIndex.section == section)
            {
                switch (section) {
                    case 0:
                        return arraySearchBy.count+1;
                        break;
                    case 1:
                        return  arrayCategory.count+1;
                        break;
                   
                        
                    default:
                        break;
                }
            }
        }
        return 1;
    }
    
    






-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        if (self.isOpen&&self.selectIndex.section == indexPath.section&&indexPath.row!=0) {
            static NSString *CellIdentifier = @"Cell2";
            Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (!cell) {
                cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
            }
            if (indexPath.section == 0) {
                cell.titleLabel.text = [arraySearchBy objectAtIndex:indexPath.row-1];
            }
            else if (indexPath.section == 1) {
                cell.titleLabel.text = [arrayCategory objectAtIndex:indexPath.row-1] ;
            }
       
            cell.titleLabel.textColor=[UIColor whiteColor];
            
            
            
            
            // NSArray *list = [[_dataList objectAtIndex:self.selectIndex.section] objectForKey:@"list"];
            return cell;
        }else
        {
            static NSString *CellIdentifier = @"Cell1";
            Cell1 *cell = (Cell1*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
            }
            NSString *name = [arrayRightTableData objectAtIndex:indexPath.section];
            cell.titleLabel.text = name;
            [cell changeArrowWithUp:([self.selectIndex isEqual:indexPath]?YES:NO)];
            return cell;
        }
    }
    
@end
