//
//  ListViewCell.h
//  SampleDemo
//
//  Created by bsetec on 11/30/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//


typedef NS_ENUM(NSInteger, MSUserListType) {
    MSUserlistTypeHighFive = 0,
    MSUserlistTypeComplimint,
    MSUserlistTypeRemint,
    MSUserlistTypeNomintation
};


#import <UIKit/UIKit.h>

@protocol UsersListDelegate

-(void)cancelCountryList;
-(void)selectedUserIdFromHighfiveList:(NSString *)selectdUserId;

@optional
-(void)updateStatus:(NSString*)currentuserid status:(int)currentStatus;
@end

@interface UsersListView : UIView<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,assign)id<UsersListDelegate>aDelegate;
- (id)initWithFrame:(CGRect)frame title:(NSString *)viewTitle;
- (void)getUserListwithFeedId:(NSString *)feedID url:(NSString *)apiUrl feedType:(NSString *)strType;

//+ (UsersListView *)showUserListViewTo:(UIView *)view viewTitle:(MSUserListType )userListType numberofPeoples:(NSInteger)peopleCount feedId:(NSString *)feedId;
+ (UsersListView *)showUserListViewTo:(UIView *)view viewTitle:(MSUserListType )userListType numberofPeoples:(NSInteger)peopleCount feedId:(NSString *)feedId delegate:(id<UsersListDelegate>)theDelegate;

+ (BOOL)isViewAlreadyAdded:(UIView *)view;
//+ (id<UsersListDelegate>)classDelegate;
//+ (void)setClassDelegate(id<UsersListDelegate>) delegate;


@end
