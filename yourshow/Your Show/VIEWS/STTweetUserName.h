//


#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, STTweetHotUserName) {
    STTweetUserHandle = 0,
    STTweetUserHashtag,
    STTweetUserStartag,
    STTweetUserNameTag,
    STTweetUserLink,
    STTweetNormalText

};

@interface STTweetUserName : UILabel

@property (strong) UITextView *textView;
@property (nonatomic, strong) NSArray *validProtocols;
@property (nonatomic, assign) BOOL leftToRight;
@property (nonatomic, assign) BOOL textSelectable;
@property (nonatomic, assign) NSInteger commentId;
@property (nonatomic, strong) UIColor *selectionColor;
@property (nonatomic, copy) void (^detectionBlock)(STTweetHotUserName hotWord, NSString *string, NSString *protocol, NSRange range);

- (void)setAttributes:(NSDictionary *)attributes;
- (void)setAttributes:(NSDictionary *)attributes hotWord:(STTweetHotUserName)hotWord;

- (id)initWithFrame:(CGRect)frame withFont:(int)fontSize attributeTextColor:(UIColor *)attributeTextColor name:(NSString *)userName;


- (NSDictionary *)attributes;
- (NSDictionary *)attributesForHotWord:(STTweetHotUserName)hotWord;
- (void)setTextAlignment:(NSTextAlignment)textAlignment;
- (CGSize)suggestedFrameSizeToFitEntireStringConstrainedToWidth:(CGFloat)width;

@end
