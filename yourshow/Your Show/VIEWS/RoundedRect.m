//
//  RoundedRect.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 27/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "RoundedRect.h"

@implementation RoundedRect

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder;
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit;
{
    CALayer *layer = self.layer;
    layer.cornerRadius  = 4.0f;
    layer.masksToBounds = YES;
 }



@end
