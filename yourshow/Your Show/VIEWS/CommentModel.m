//
//  CommentModel.m
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 22/09/14.
//
//

#import "CommentModel.h"

@implementation CommentModel


-(id)initWithComplimint : (NSDictionary *) complimintInfo
{

        self.achieveCommentDate         =    ((NSNull *)[complimintInfo valueForKey:@"comment_date"] != nil) ?[NSString stringWithFormat:@"%@",[complimintInfo valueForKey:@"comment_date"]]:[NSString stringWithFormat:@"%@",[complimintInfo valueForKey:@"comment_date"]];
        self.achieveCommentId           =   [NSString stringWithFormat:@"%@",[complimintInfo valueForKey:@"comment_id"]];
        self.achieveCommentText         =    [[complimintInfo valueForKey:@"comment_text"] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];

        self.achieveCommentUserId       =   [complimintInfo valueForKey:@"user_id"];
        self.achieveCommentUname        =    [complimintInfo valueForKey:@"user_name"];
        self.achieveCommentUserImage    =    [complimintInfo valueForKey:@"user_thumb_image"];
        self.status_code                =    @"1";
        self.status_commnetEdited        =    [complimintInfo valueForKey:@"is_edited"];

    if ([[complimintInfo valueForKey:@"json_cmt"] count]) {
        self.atMentionedArray  = [NSMutableArray array];
        self.atMentionedArray  =  [complimintInfo valueForKey:@"json_cmt"];
    }
    
        return  self;
}
@end
