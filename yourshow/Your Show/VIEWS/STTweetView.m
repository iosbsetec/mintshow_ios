

#import "STTweetView.h"
#import "Keys.h"
#import "NSString+RemoveEmoji.h"

#define STURLRegex @"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))"

@interface STTweetView () <UITextViewDelegate>

@property (nonatomic, strong) NSRegularExpression *textCheckingRegularExpression;
@property (nonatomic, strong) NSRegularExpression *urlRegex;

@property (strong) NSTextStorage   *textStorage;
@property (strong) NSLayoutManager *layoutManager;
@property (strong) NSTextContainer *textContainer;

@property (nonatomic, strong) NSString *cleanText;
@property (nonatomic, copy) NSAttributedString *cleanAttributedText;

@property (strong) NSMutableArray *rangesOfHotWords;

@property (nonatomic, strong) NSDictionary *attributesText;
@property (nonatomic, strong) NSDictionary *attributesHandle;
@property (nonatomic, strong) NSDictionary *attributesHashtag;
@property (nonatomic, strong) NSDictionary *attributesStartag;
@property (nonatomic, strong) NSDictionary *attributesLink;

//@property (strong) UITextView *textView;
@property(nonatomic) UIEdgeInsets composerBackgroundInsets;
@property(nonatomic) NSInteger    keyboardHeight;
@property(nonatomic) NSInteger    keyboardAnimationCurve;
@property(nonatomic) CGFloat      keyboardAnimationDuration;
@property(nonatomic) NSInteger    keyboardOffset;
@property(nonatomic) UIEdgeInsets composerTVInsets;
@property(nonatomic) CGFloat      composerTVMaxHeight;

@end

@implementation STTweetView {
    BOOL      _isTouchesMoved;
    NSRange   _selectableRange;
    NSInteger _firstCharIndex;
    CGPoint   _firstTouchLocation;
}



#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setupLabel];
        [self setupTextView];
        [self setupURLRegularExpression];
    }
    return self;
}

//- (id)initWithCoder:(NSCoder *)coder
//{
//    self = [super initWithCoder:coder];
//    if (self) {
//        [self setupLabel];
//        [self setupTextView];
//        [self setupURLRegularExpression];
//    }
//
//    return self;
//}


- (void)setupTextView {
     self.counterCount = 0;
    currentInput = @"";
    _textStorage   = [NSTextStorage new];
    _layoutManager = [NSLayoutManager new];
    _textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)];

    [_layoutManager addTextContainer:_textContainer];
    [_textStorage addLayoutManager:_layoutManager];

    _textView = [[UITextView alloc] initWithFrame:CGRectMake(2.0, 2.0, self.frame.size.width-4, self.frame.size.height-4) textContainer:_textContainer];
    _textView.delegate                          = self;
    _textView.autoresizingMask                  = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _textView.backgroundColor                   = [UIColor clearColor];
    _textView.textContainer.lineFragmentPadding = 0;
    _textView.textContainerInset                = UIEdgeInsetsZero;
    _textView.userInteractionEnabled            = YES;
    _textView.text = @"Describe it positive...";
    _textView.textColor = [UIColor lightGrayColor];
    
//    _textView.layer.borderColor = [UIColor purpleColor].CGColor;
//    _textView.layer.borderWidth = 2.0;
    
    [self addSubview:_textView];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([self.textView.text isEqualToString:@"Describe it positive..."]) {
        self.textView.text = @"";
        self.textView.textColor = [UIColor blackColor]; //optional
    }
    
    [self performSelector:@selector(textViewDidChange:) withObject:nil afterDelay:0.1];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([self.textView.text isEqualToString:@""]) {
        self.textView.text = @"Describe it positive...";
        self.textView.textColor = [UIColor lightGrayColor]; //optional
    }
    self.suggesting = NO;
}


- (void)setupURLRegularExpression {

    NSError *regexError = nil;
    self.urlRegex = [NSRegularExpression regularExpressionWithPattern:STURLRegex options:0 error:&regexError];
}

#pragma mark - Responder

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return (action == @selector(copy:));
}

- (void)copy:(id)sender {
    [[UIPasteboard generalPasteboard] setString:[_cleanText substringWithRange:_selectableRange]];
    
    @try {
        [_textStorage removeAttribute:NSBackgroundColorAttributeName range:_selectableRange];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}

#pragma mark - Setup

- (void)setupLabel
{
    [self setBackgroundColor:[UIColor clearColor]];
    [self setClipsToBounds:NO];
    [self setUserInteractionEnabled:YES];
    
    _leftToRight = YES;
    _textSelectable = YES;
    _selectionColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    
//    _attributesText = @{NSForegroundColorAttributeName: [UIColor blackColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:12.0]};
//    _attributesHandle = @{NSForegroundColorAttributeName: ORANGECOLOR, NSFontAttributeName: [UIFont fontWithName:@"Helvetica Bold" size:12.0]};
//    _attributesHashtag = @{NSForegroundColorAttributeName: ORANGECOLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:14.0]};
//    _attributesStartag = @{NSForegroundColorAttributeName: ORANGECOLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:14.0]};
//    _attributesLink = @{NSForegroundColorAttributeName: ORANGECOLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:14.0]};
    
    
    UIColor *customOrangeColor  = [UIColor colorWithRed:237.0/255.0 green:71.0/255.0 blue:22.0/255.0 alpha:0.9];
    _attributesText             = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"HelveticaNeue" size:12.0],NSFontAttributeName,nil];
    _attributesHandle          = [NSDictionary dictionaryWithObjectsAndKeys:customOrangeColor,NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:12.0],NSFontAttributeName,nil];
    _attributesHashtag          = [NSDictionary dictionaryWithObjectsAndKeys:customOrangeColor,NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:12.0],NSFontAttributeName,nil];
    _attributesStartag         = [NSDictionary dictionaryWithObjectsAndKeys:customOrangeColor,NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:12.0],NSFontAttributeName,nil];
    _attributesLink         = [NSDictionary dictionaryWithObjectsAndKeys:customOrangeColor,NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:12.0],NSFontAttributeName,nil];
    
    self.validProtocols = @[@"http", @"https"];
}

//-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    currentInput = text;
//
//    if (range.location == 0 && [text isEqualToString:@" "]) {
//        return NO;
//    }
//    if ([text isEqualToString: @""])
//    {
//        return YES;
//    }
//   
//   
//    
//    else if([text isEqualToString:@"\n"]) {
//        [self.textView resignFirstResponder];
//        return NO;
//    }
//    
//    
//    
//    __block NSString *word = nil;
//    __block NSRange ranges = NSMakeRange(NSNotFound, 0);
//    NSString *textViewTXT = self.textView.text;
//    
//    [self.textCheckingRegularExpression enumerateMatchesInString:textViewTXT
//                                                         options:0
//                                                           range:NSMakeRange(0, textViewTXT.length)
//                                                      usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
//     {
//         NSRange textSelectedRange = self.textView.selectedRange;
//         if (textSelectedRange.location > result.range.location && textSelectedRange.location <= result.range.location + result.range.length) {
//             word = [textViewTXT substringWithRange:result.range];
//             ranges = result.range;
//             *stop = YES;
//         }
//     }];
//    if ([currentInput containsString:@"*"] && _isStarCharBlocked) {
//        return NO;
//    }
//    
//    if ([currentInput isEqualToString:@"@"] || [currentInput isEqualToString:@"#"] || [currentInput isEqualToString:@"*"])
//    {
//        return YES;
//    }
//    else if (word.length >= 1 && ranges.location != NSNotFound)
//    {
//        NSString *first = [word substringToIndex:1];
//        
//        if ([first isEqualToString:@"@"] || [first isEqualToString:@"#"] || [first isEqualToString:@"*"])
//        {
//            return YES;
//        }
//    }
//    
// 
//    else if (self.counterCount >= _nMaxTextLength)
//    {
//        if (text.isIncludingEmoji) {
//            return YES;
//        }
//        return NO;
//    }
//    
//    return YES;
//    
//}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    currentInput = text;
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    if ([text isEqualToString: @""])
    {
        return YES;
    }
    
    
    
    else if([text isEqualToString:@"\n"]) {
        [self.textView resignFirstResponder];
        return NO;
    }
    
    
    
    __block NSString *word = nil;
    __block NSRange ranges = NSMakeRange(NSNotFound, 0);
    NSString *textViewTXT = self.textView.text;
    
    [self.textCheckingRegularExpression enumerateMatchesInString:textViewTXT
                                                         options:0
                                                           range:NSMakeRange(0, textViewTXT.length)
                                                      usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         NSRange textSelectedRange = self.textView.selectedRange;
         if (textSelectedRange.location > result.range.location && textSelectedRange.location <= result.range.location + result.range.length) {
             word = [textViewTXT substringWithRange:result.range];
             ranges = result.range;
             *stop = YES;
         }
     }];
    if ([currentInput containsString:@"*"] && _isStarCharBlocked) {
        return NO;
    }
    
    if ([currentInput isEqualToString:@"@"] || [currentInput isEqualToString:@"#"] || [currentInput isEqualToString:@"*"])
    {
        return YES;
    }
    else if (word.length >= 1 && ranges.location != NSNotFound)
    {
        NSString *first = [word substringToIndex:1];
        
        if ([first isEqualToString:@"@"] || [first isEqualToString:@"#"] || [first isEqualToString:@"*"])
        {
            return YES;
        }
    }
    
    
    //    else if (self.counterCount >= _nMaxTextLength)
    //    {
    //        if (text.isIncludingEmoji) {
    //            return YES;
    //        }
    //        return NO;
    //    }
    
    
    NSUInteger newLength = (self.counterCount - range.length) + [text stringByRemovingEmoji].length;
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    if(newLength <= _nMaxTextLength)
    {
        return YES;
    }
    else
    {
        if (text.isIncludingEmoji)
        {
            if ([text stringByRemovingEmoji].length)
            {
                return NO;
            }
            return YES;
        }
        NSUInteger emptySpace = _nMaxTextLength - (self.counterCount - range.length);
        textView.text = [[[textView.text substringToIndex:range.location] stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        [self determineHotWordsNew];
        return NO;
    }
    
    return YES;
}

- (NSArray *)getAllShowRoomArrayFromTextView
{
    NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictionary in _rangesOfHotWords)  {
        NSRange range = [dictionary[@"range"] rangeValue];
        NSString *strStarTags = [self.textView.text substringWithRange:range];
        STTweetHotViewWord hotWord = (STTweetHotViewWord)[dictionary[@"hotWord"] intValue];
        if (hotWord == STTweetViewStartag)
        {
            [arrTemp addObject:[strStarTags stringByReplacingOccurrencesOfString:@"*" withString:@""]];
        }
    }
    return arrTemp;
}
- (void)textViewDidChange:(UITextView *)textView
{
    [self determineHotWordsNew];
    __block NSString *word = nil;
    __block NSRange range = NSMakeRange(NSNotFound, 0);
//    _cleanText = self.textView.text;
    [self.textDelegate textDidChange:self];
    NSString *textViewTXT = self.textView.text;
    
    [self.textCheckingRegularExpression enumerateMatchesInString:textViewTXT
                                                         options:0
                                                           range:NSMakeRange(0, textViewTXT.length)
                                                      usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         NSRange textSelectedRange = self.textView.selectedRange;
         if (textSelectedRange.location > result.range.location && textSelectedRange.location <= result.range.location + result.range.length) {
             word = [textViewTXT substringWithRange:result.range];
             range = result.range;
             *stop = YES;
         }
     }];
    
    if (word.length >= 1 && range.location != NSNotFound)
    {
        NSString *first = [word substringToIndex:1];
        NSString *rest = [word substringFromIndex:1];
        
        if ([first isEqualToString:@"@"])
        {
            self.suggesting = YES;
            self.suggestionRange = NSMakeRange(range.location + 1, range.length - 1);
            NSLog(@"Making Range =====>  %@",NSStringFromRange(NSMakeRange(range.location + 1, range.length - 1)));
            if (self.textDelegate) {
                self.suggestionType = EntityTypeMention;
                [self.textDelegate beginSuggestingForTextView:EntityTypeMention query:rest  Inrange:self.suggestionRange];
            }
        }
        else if ([first isEqualToString:@"#"])
        {
            self.suggesting = YES;
            self.suggestionRange = NSMakeRange(range.location + 1, range.length - 1);
            if (self.textDelegate)
            {
                self.suggestionType = EntityTypeHashTag;
                [self.textDelegate beginSuggestingForTextView:EntityTypeHashTag query:rest Inrange:self.suggestionRange];
            }
        }
        else if ([first isEqualToString:@"*"])
        {
            self.suggesting = YES;
            self.suggestionRange = NSMakeRange(range.location + 1, range.length - 1);
            if (self.textDelegate) {
                self.suggestionType = EntityTypeshowroom;
                [self.textDelegate beginSuggestingForTextView:EntityTypeshowroom query:rest Inrange:self.suggestionRange];
            }        }else {
                self.suggestionRange = NSMakeRange(NSNotFound, 0);
                self.suggesting = NO;
                [self.textDelegate endSuggesting];
            }
    }
    else
    {
        self.suggestionRange = NSMakeRange(NSNotFound, 0);
        self.suggesting = NO;
        [self.textDelegate endSuggesting];
        
    }
}

- (NSRegularExpression *)textCheckingRegularExpression
{
    if (!_textCheckingRegularExpression)
    {
        _textCheckingRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[@#*]([^\\s/:：@#*]?)+$?" options:NSRegularExpressionCaseInsensitive error:NULL];
    }
    return _textCheckingRegularExpression;
}

- (void)setValidProtocols:(NSArray *)validProtocols {
    _validProtocols = validProtocols;
    if(self.textView.text.length>0)
        [self determineHotWordsNew];
}

- (void)determineHotWordsNew
{
    _cleanText =self.textView.text;
    NSMutableString *tmpText = [[NSMutableString alloc] initWithString:_cleanText];
    
    NSString *hotCharacters = @"@#*";
    NSCharacterSet *hotCharactersSet = [NSCharacterSet characterSetWithCharactersInString:hotCharacters];
    
    // Define a character set for the complete world (determine the end of the hot word)
    NSMutableCharacterSet *validCharactersSet = [NSMutableCharacterSet alphanumericCharacterSet];
        [validCharactersSet removeCharactersInString:@"!@#$%^&*()-={[]}|;:',<>.?/"];
        [validCharactersSet addCharactersInString:@""];
    
    _rangesOfHotWords = [[NSMutableArray alloc] init];
    
//    NSLog(@"%lu And Length %lu",(unsigned long)[tmpText rangeOfCharacterFromSet:hotCharactersSet].location,(unsigned long)tmpText.length);
    while ([tmpText rangeOfCharacterFromSet:hotCharactersSet].location < tmpText.length)
    {
        NSRange range = [tmpText rangeOfCharacterFromSet:hotCharactersSet];
        
        STTweetHotViewWord hotWord;
        
        switch ([tmpText characterAtIndex:range.location])
        {
            case '@':
                hotWord = STTweetViewHandle;
                break;
            case '#':
                hotWord = STTweetViewHashtag;
                break;
            case '*':
                hotWord = STTweetViewStartag;
                break;
                
            default:
                break;
        }

     
        
        [tmpText replaceCharactersInRange:range withString:@"%"];
        
        // Determine the length of the hot word
        int length = (int)range.length;
        
        // Register the hot word and its range
        while (range.location + length < tmpText.length) {
            BOOL charIsMember = [validCharactersSet characterIsMember:[tmpText characterAtIndex:range.location + length]];
            
            if (charIsMember)
                length++;
            else
                break;
        }
        
        if (length > 1)
        {
                        [_rangesOfHotWords addObject:@{@"hotWord": @(hotWord), @"range": [NSValue valueWithRange:NSMakeRange(range.location, length)]}];
        }
        
//        NSLog(@"My range %@",NSStringFromRange(NSMakeRange(range.location, length)));

        // If the hot character is not preceded by a alphanumeric characater, ie email (sebastien@world.com)
        if (range.location > 0 && [validCharactersSet characterIsMember:[tmpText characterAtIndex:range.location - 1]])
            continue;
    }
   // [self determineLinks];

    [self updateText];
    
    int nHotWordLength = 0;
    NSString *strLength = self.textView.text;
   // strLength = [[strLength stringByRemovingEmoji]stringByReplacingOccurrencesOfString:@" " withString:@""];
    strLength = [strLength stringByReplacingOccurrencesOfString:@"#" withString:@""];
    strLength = [strLength stringByReplacingOccurrencesOfString:@"*" withString:@""];
    strLength = [strLength stringByReplacingOccurrencesOfString:@"@" withString:@""];
    
    if (_rangesOfHotWords)
    {
        for (NSDictionary *dictionary in _rangesOfHotWords)
        {
            NSRange range = [dictionary[@"range"] rangeValue];
            nHotWordLength += range.length-1;
        }
    }
    
    
    
    
    self.counterCount= strLength.length-nHotWordLength;
    
    [self.textDelegate textDidChange:self];
}

- (void)determineLinks {
    NSMutableString *tmpText = [[NSMutableString alloc] initWithString:_cleanText];
    
    [self.urlRegex enumerateMatchesInString:tmpText options:0 range:NSMakeRange(0, tmpText.length) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        NSString *protocol     = @"http";
        NSString *link         = [tmpText substringWithRange:result.range];
        NSRange  protocolRange = [link rangeOfString:@":"];
        if (protocolRange.location != NSNotFound) {
            protocol = [link substringToIndex:protocolRange.location];
        }
        
        if ([_validProtocols containsObject:protocol.lowercaseString]) {
            [_rangesOfHotWords addObject:@{ @"hotWord"  : @(STTweetViewLink),
                                            @"protocol" : protocol,
                                            @"range"    : [NSValue valueWithRange:result.range]
                                            }];
        }
    }];
}

#pragma mark - Printing and calculating text


- (void)updateText {
    [_textStorage beginEditing];

    NSAttributedString *attributedString = _cleanAttributedText ?: [[NSMutableAttributedString alloc] initWithString:_cleanText];
    [_textStorage setAttributedString:attributedString];
    [_textStorage setAttributes:_attributesText range:NSMakeRange(0, attributedString.length)];

    for (NSDictionary *dictionary in _rangesOfHotWords)  {
        NSRange range = [dictionary[@"range"] rangeValue];
        STTweetHotViewWord hotWord = (STTweetHotViewWord)[dictionary[@"hotWord"] intValue];
        [_textStorage setAttributes:[self attributesForHotWord:hotWord] range:range];
    }

    [_textStorage endEditing];
}

-(void)removeShowroomNameInTweetTextView:(NSString *)strSelectedShowRoomName
{
    self.textView.text = [self.textView.text stringByReplacingOccurrencesOfString:strSelectedShowRoomName withString:@""];
    _cleanText =self.textView.text;
    
    [self determineHotWordsNew];
    [self updateText];
    
    self.suggesting = NO;
    self.suggestionRange = NSMakeRange(NSNotFound, 0);
}

-(void)appandAStringWithEntityType:(EntityType)textSuggestion AndString:(NSString*)fullWord andID:(NSString*)contactID
{
    if ([self.textView.text isEqualToString:@"Describe it positive..."])
    {
        self.textView.text = @"";
        self.textView.textColor = [UIColor blackColor]; //optional
    }
    
    // Get Curret Cursor Position
    NSInteger pos = [self.textView offsetFromPosition:self.textView.beginningOfDocument toPosition:self.textView.selectedTextRange.start];
    NSLog(@"position of textview %ld",(long)pos);
    
    self.suggestionRange = NSMakeRange(pos, 0);
    
    self.textView.text = [self.textView.text stringByReplacingCharactersInRange:self.suggestionRange withString:fullWord];
    _cleanText =self.textView.text;
    
    [self determineHotWordsNew];
    [self updateText];
    
    self.suggesting = NO;
    self.suggestionRange = NSMakeRange(NSNotFound, 0);
    
    NSLog(@"Total Hot Word Array :%@",_rangesOfHotWords);
}

-(void)wordSelectedForSuggestType:(EntityType)textSuggestion AndString:(NSString*)fullWord andID:(NSString*)contactID
                         forRange:(NSRange)range
{
    NSRange suggestionRange = self.suggestionRange;
    NSString *suggestionString = fullWord;
    NSString *insertString = [suggestionString stringByAppendingString:@""];
    if (self.textView.text.length > suggestionRange.location + suggestionRange.length) {
        if ([[self.textView.text substringWithRange:NSMakeRange(suggestionRange.location, suggestionRange.length + 1)] hasSuffix:@""])
        {
            insertString = suggestionString;
        }
    }
    
    @try
    {
        self.textView.text = [self.textView.text stringByReplacingCharactersInRange:suggestionRange withString:insertString];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception);
    }
    
    _cleanText =self.textView.text;
//    [_rangesOfHotWords addObject:@{@"hotWord": @(STTweetViewHashtag), @"range": [NSValue valueWithRange:NSMakeRange(range.location-1, fullWord.length+1)]}];
    [self determineHotWordsNew];
    [self updateText];
    self.suggesting = NO;
    self.suggestionRange = NSMakeRange(NSNotFound, 0);
}

#pragma mark - Public methods

- (CGSize)suggestedFrameSizeToFitEntireStringConstrainedToWidth:(CGFloat)width {
    if (_cleanText == nil)
        return CGSizeZero;

    return [_textView sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
}

- (CGSize) intrinsicContentSize {
    CGSize size = [self suggestedFrameSizeToFitEntireStringConstrainedToWidth:CGRectGetWidth(self.frame)];
    return CGSizeMake(size.width, size.height + 1);
}

#pragma mark - Private methods

- (NSArray *)hotWordsList {
    return _rangesOfHotWords;
}

#pragma mark - Setters

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    [self invalidateIntrinsicContentSize];
}

- (void)setText:(NSString *)text {
//    [super setText:@""];
    _selectableRange = NSMakeRange(NSNotFound, 0);

    _cleanText = @"hi @inthe begining";
//    [self determineHotWords];
    [self invalidateIntrinsicContentSize];
}

- (void)setAttributes:(NSDictionary *)attributes {
    if (!attributes[NSFontAttributeName]) {
        NSMutableDictionary *copy = [attributes mutableCopy];
        copy[NSFontAttributeName] = [UIFont systemFontOfSize:15];
        attributes = [NSDictionary dictionaryWithDictionary:copy];
    }
    
    if (!attributes[NSForegroundColorAttributeName]) {
        NSMutableDictionary *copy = [attributes mutableCopy];
        copy[NSForegroundColorAttributeName] = [UIColor blackColor];
        attributes = [NSDictionary dictionaryWithDictionary:copy];
    }

    _attributesText = attributes;
    
    [self determineHotWordsNew];
}

- (void)setAttributes:(NSDictionary *)attributes hotWord:(STTweetHotViewWord)hotWord {
    if (!attributes[NSFontAttributeName]) {
        NSMutableDictionary *copy = [attributes mutableCopy];
        copy[NSFontAttributeName] = [UIFont systemFontOfSize:15];
        attributes = [NSDictionary dictionaryWithDictionary:copy];
    }
    
    if (!attributes[NSForegroundColorAttributeName])
    {
        NSMutableDictionary *copy = [attributes mutableCopy];
        copy[NSForegroundColorAttributeName] = [UIColor blackColor];
        attributes = [NSDictionary dictionaryWithDictionary:copy];
    }
    
    switch (hotWord)
    {
        case STTweetViewHandle:
            _attributesHandle = attributes;
            break;
        case STTweetViewHashtag:
            _attributesHashtag = attributes;
            break;
        case STTweetViewStartag:
            _attributesStartag = attributes;
            break;
            
        case STTweetViewLink:
            _attributesLink = attributes;
            break;
            
        default:
            break;
    }
    
    [self determineHotWordsNew];
}

- (void)setLeftToRight:(BOOL)leftToRight {
    _leftToRight = leftToRight;

    [self determineHotWordsNew];
}

//- (void)setTextAlignment:(NSTextAlignment)textAlignment {
//    [super setTextAlignment:textAlignment];
//    _textView.textAlignment = textAlignment;
//}

- (void)setDetectionBlock:(void (^)(STTweetHotViewWord, NSString *, NSString *, NSRange))detectionBlock {
    if (detectionBlock) {
        _detectionBlock = [detectionBlock copy];
        self.userInteractionEnabled = YES;
    } else {
        _detectionBlock = nil;
        self.userInteractionEnabled = NO;
    }
}

- (void)setAttributedText:(NSAttributedString *)attributedText {
    _cleanAttributedText = [attributedText copy];
    self.text = _cleanAttributedText.string;
}

#pragma mark - Getters

- (NSString *)text {
    return _cleanText;
}

- (NSDictionary *)attributes {
    return _attributesText;
}

- (NSDictionary *)attributesForHotWord:(STTweetHotViewWord)hotWord {
    switch (hotWord) {
        case STTweetViewHandle:
            return _attributesHandle;

        case STTweetViewHashtag:
            return _attributesHashtag;
            
        case STTweetViewStartag:
            return _attributesStartag;
            
        case STTweetViewLink:
            return _attributesLink;
            
        default:
            break;
    }
    return nil;
}

- (BOOL)isLeftToRight {
    return _leftToRight;
}

#pragma mark - Retrieve word after touch event

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    _selectableRange = NSMakeRange(0, 0);
    if (![self getTouchedHotword:touches]) {
        [super touchesBegan:touches withEvent:event];
    }
    
    _isTouchesMoved = NO;
    
    @try {
        [_textStorage removeAttribute:NSBackgroundColorAttributeName range:_selectableRange];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    
//    _selectableRange = NSMakeRange(0, 0);
    _firstTouchLocation = [[touches anyObject] locationInView:_textView];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if ([self getTouchedHotword:touches] == nil) {
        [super touchesMoved:touches withEvent:event];
    }
    
    if (!_textSelectable) {
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        [menuController setMenuVisible:NO animated:YES];
        
        return;
    }
    
    _isTouchesMoved = YES;
    
    NSInteger charIndex = [self charIndexAtLocation:[[touches anyObject] locationInView:_textView]];
    if (charIndex == NSNotFound)
        return;

    [_textStorage beginEditing];

    @try {
        [_textStorage removeAttribute:NSBackgroundColorAttributeName range:_selectableRange];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    
    if (_selectableRange.length == 0) {
        _selectableRange = NSMakeRange(charIndex, 1);
        _firstCharIndex = charIndex;
    } else if (charIndex > _firstCharIndex) {
        _selectableRange = NSMakeRange(_firstCharIndex, charIndex - _firstCharIndex + 1);
    } else if (charIndex < _firstCharIndex) {
        _firstTouchLocation = [[touches anyObject] locationInView:_textView];
        
        _selectableRange = NSMakeRange(charIndex, _firstCharIndex - charIndex);
    }

    NSAssert(_selectableRange.location >= 0, @"range < 0");
    NSAssert(NSMaxRange(_selectableRange) < _textStorage.length, @"range > max");

    @try {
        [_textStorage addAttribute:NSBackgroundColorAttributeName value:_selectionColor range:_selectableRange];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }

    [_textStorage endEditing];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    CGPoint touchLocation = [[touches anyObject] locationInView:self];

    if (self.textSelectable && _isTouchesMoved) {
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        [menuController setTargetRect:CGRectMake(_firstTouchLocation.x, _firstTouchLocation.y, 1.0, 1.0) inView:self];
        [menuController setMenuVisible:YES animated:YES];
        
        [self becomeFirstResponder];

        return;
    }
    
    if (!CGRectContainsPoint(_textView.frame, touchLocation))
        return;

    id touchedHotword = [self getTouchedHotword:touches];
    if(touchedHotword != nil && _detectionBlock != NULL) {
        NSRange range = [[touchedHotword objectForKey:@"range"] rangeValue];
        
        _detectionBlock((STTweetHotViewWord)[[touchedHotword objectForKey:@"hotWord"] intValue], [_cleanText substringWithRange:range], [touchedHotword objectForKey:@"protocol"], range);
    } else {
        [super touchesEnded:touches withEvent:event];
    }
}

- (NSInteger)charIndexAtLocation:(CGPoint)touchLocation
{
    NSUInteger glyphIndex = [_layoutManager glyphIndexForPoint:touchLocation inTextContainer:_textView.textContainer];
    CGRect boundingRect = [_layoutManager boundingRectForGlyphRange:NSMakeRange(glyphIndex, 1) inTextContainer:_textView.textContainer];
    
    if (CGRectContainsPoint(boundingRect, touchLocation))
        return [_layoutManager characterIndexForGlyphAtIndex:glyphIndex];
    else
        return NSNotFound;
}

- (id)getTouchedHotword:(NSSet *)touches
{
    NSInteger charIndex = [self charIndexAtLocation:[[touches anyObject] locationInView:_textView]];

    if (charIndex != NSNotFound) {
        for (id obj in _rangesOfHotWords) {
            NSRange range = [[obj objectForKey:@"range"] rangeValue];

            if (charIndex >= range.location && charIndex < range.location + range.length) {
                return obj;
            }
        }
    }

    return nil;
}


#pragma  mark - --

@end
