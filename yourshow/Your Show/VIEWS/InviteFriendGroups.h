//
//  InviteFriendGroups.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 21/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsInfo.h"

@interface InviteFriendGroups : UIView

- (instancetype)initWithFrame:(CGRect)frame withArray : (NSArray *) groupInFo;
@end
