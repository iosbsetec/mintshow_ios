//
//  UIImage+UIImagescaling.h
//  Conciergist
//
//  Created by ivmac on 5/18/13.
//  Copyright (c) 2013 macuser. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImagescaling)

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;

@end
