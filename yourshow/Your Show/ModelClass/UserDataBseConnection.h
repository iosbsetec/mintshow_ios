//
//  UserDataBseConnection.h
//  FormGenerator
//
//  Created by mac mini on 11/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "coachingTipModel.h"

@class Userfeed;

typedef NS_ENUM(NSInteger, YScoachTipType) {
    YScoachTipTypeDiscoverFirstTime = 0,
    YScoachTipTypeShowroomFirstTime,
    YScoachTipTypeMintDetailsFirstTime,
    YScoachTipTypeAddMintFirstTime
};

@interface UserDataBseConnection : NSObject
{
    NSString *databaseName;
    NSString *databasePath;
    NSString *idd;
}


-(BOOL)shouldDisplayPopupforType:(YScoachTipType)coachTipType;
-(void)UpdateDataforType:(YScoachTipType)coachTipType;
-(void)clearAllSession;
-(void)InsertUserDetailsToDataBase:(Userfeed *)value;
-(void)UpdateUserDetailsToDataBase:(Userfeed *)value;
-(void)UpdateRecordToRecordDataBase:(NSString*)value  keyData:(NSString*)key;
-(NSMutableArray*)getAllrecordForUser;


-(void)insertTodb:(NSString *)placeID withReference:(NSString*)reference andDescription:(NSString*)description;
-(NSMutableArray*)getAllrecordForStoredLocation;

-(void)InsertFeedDetailsToDataBase:(NSMutableArray *)feedvalueArray;
-(NSMutableArray*)getAllrecordForMintshowFeedList;
-(void)InsertDiscoverMintsFeedDetailsToDataBase:(NSMutableArray *)feedvalueArray;
-(NSMutableArray*)getAllrecordForDiscoverMintsMintshowFeedList;

-(void)InsertShowroomDetailsToDataBase:(NSMutableArray *)feedvalueArray;
-(NSMutableArray*)getAllrecordForShowroomList;

-(void)InsertActivityListToDataBase:(NSMutableArray *)feedvalueArray;
-(NSMutableArray*)getAllrecordForActivityYouList;

-(void)setAllValuesForcOachingTips:(coachingTipModel *)model;

-(void)InsertRequestActivityListToDataBase:(NSMutableArray *)feedvalueArray;
-(NSMutableArray*)getAllrecordForActivityRequestList;

-(void)InsertMyCircleActivityListToDataBase:(NSMutableArray *)feedvalueArray;
-(NSMutableArray*)getAllrecordForActivityMyCircleList;
@end

