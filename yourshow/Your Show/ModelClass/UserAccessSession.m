

#import "UserAccessSession.h"
#import  "UserDataBseConnection.h"
#import "Keys.h"


@implementation UserAccessSession


+(BOOL)isLoggedIn
{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    NSMutableArray *someArray=[udb getAllrecordForUser];
    Userfeed *userSession;
    if(someArray.count>0)
        userSession =[someArray objectAtIndex:0];
    if(userSession.login_Status!=1)
        return NO;
    return YES;
}

+(void)setAllValuesForcOachingTips:(coachingTipModel *)model {
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb setAllValuesForcOachingTips:model];
}

+(BOOL)shouldDisplayDiscoverFirstTimeVisitPopup{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb shouldDisplayPopupforType:YScoachTipTypeDiscoverFirstTime];
}
+(BOOL)shouldDisplayShowroomFirstTimeVisitPopup{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb shouldDisplayPopupforType:YScoachTipTypeShowroomFirstTime];
}
+(BOOL)shouldDisplayMintdetailsFirstTimeVisitPopup{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb shouldDisplayPopupforType:YScoachTipTypeMintDetailsFirstTime];
}
+(BOOL)shouldDisplayFirstTimePostMintPopup{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb shouldDisplayPopupforType:YScoachTipTypeAddMintFirstTime];
}

+(void)setDiscoverFirstTimeVisitCompleted{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb UpdateDataforType:YScoachTipTypeDiscoverFirstTime];

}
+(void)setShowroomFirstTimeVisitCompleted{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb UpdateDataforType:YScoachTipTypeShowroomFirstTime];
 
}
+(void)setMintdetailsFirstTimeCompleted{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb UpdateDataforType:YScoachTipTypeMintDetailsFirstTime];
 
}
+(void)setFirstTimePostMintCompleted{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb UpdateDataforType:YScoachTipTypeAddMintFirstTime];
  
}


+(void)storeUserSession:(Userfeed*)session {
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb InsertUserDetailsToDataBase:session];
}


+(void)storeUserSessionValue:(NSString *)value forKey:(NSString *)key
{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb UpdateRecordToRecordDataBase:value keyData:key];
}


+(Userfeed*)getUserSession
{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    NSMutableArray *someArray=[udb getAllrecordForUser];
    Userfeed *userSession = [Userfeed new];
    if(someArray.count>0)
         return [someArray objectAtIndex:0];
   
    userSession.login_Status =0;
    return userSession;
}

+(void)clearAllSession
{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb clearAllSession];
    
}



+(void)storeLocationData:(NSDictionary*)adata{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb insertTodb:[adata valueForKey:@"place_id"] withReference:[adata valueForKey:@"reference"] andDescription:[adata valueForKey:@"description"]];
}
+(NSMutableArray*)getStoredLocationHistory{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb getAllrecordForStoredLocation];
    
}

+(void)InsertDiscoverMints:(NSMutableArray *)feedvalueArray{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb InsertDiscoverMintsFeedDetailsToDataBase:feedvalueArray];
}
+(NSMutableArray*)getAllDiscoverMints{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb getAllrecordForDiscoverMintsMintshowFeedList];
  
}

+(void)InsertFeedDetailsToDataBase:(NSMutableArray *)feedvalueArray{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb InsertFeedDetailsToDataBase:feedvalueArray];

}
+(NSMutableArray*)getAllrecordForMintshowFeedList{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb getAllrecordForMintshowFeedList];
}

+(void)InsertShowroomDetailsToDataBase:(NSMutableArray *)feedvalueArray{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb InsertShowroomDetailsToDataBase:feedvalueArray];

}
+(NSMutableArray*)getAllrecordForShowroomList{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb getAllrecordForShowroomList];
}


+(void)InsertActivityYouDataBase:(NSMutableArray *)feedvalueArray{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb InsertActivityListToDataBase:feedvalueArray];
}

+(NSMutableArray*)getAllActivityYouList{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb getAllrecordForActivityYouList];
}
+(void)InsertActivityRequestDataBase:(NSMutableArray *)feedvalueArray{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb InsertRequestActivityListToDataBase:feedvalueArray];
}

+(NSMutableArray*)getAllActivityrequestList
{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb getAllrecordForActivityRequestList];
    
}

+(void)InsertActivityMyCircleDataBase:(NSMutableArray *)feedvalueArray
{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    [udb InsertMyCircleActivityListToDataBase:feedvalueArray];
    
}

+(NSMutableArray*)getAllActivityMyCircleList
{
    UserDataBseConnection *udb=[[UserDataBseConnection alloc]init];
    return [udb getAllrecordForActivityMyCircleList];
    
}


@end


