//
//  NSString+YaacMention.m
//  SearchUser
//
//  Created by Siba Prasad Hota  on 6/1/15.
//  Copyright (c) 2015 Lemonpeak. All rights reserved.
//

#import "NSString+YaacMention.h"



@implementation NSString (YaacMention)


- (NSString *)StringByChangingComplimintStringToNormalString;

{
    NSString *newString = self;
    newString = [newString stringByReplacingOccurrencesOfString:@"\\([^)]+\\)" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@"\\["        withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@"\\]"         withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, newString.length)];
    return newString;
}


- (NSString *)StringByChangingNormalNameToServername:(NSString *)idString entityType:(EntityType)entype
{
    NSString *newString =self;
    NSString *bracketString=@"";
    if (entype == EntityTypeMention)
    {
        newString = [newString stringByReplacingOccurrencesOfString:@"@" withString:@""];
        bracketString=@"@[";
    }
    
    else if (entype == EntityTypeshowroom)
    {
        newString = [newString stringByReplacingOccurrencesOfString:@"*" withString:@""];
        bracketString=@"*[";
    }
    else if (entype == EntityTypeHashTag)
    {
        return newString;
        //        newString = [newString stringByReplacingOccurrencesOfString:@"#" withString:@""];
        //        bracketString=@"#[";
    }
    bracketString = [bracketString stringByAppendingString:newString];
    newString = [bracketString stringByAppendingString:[NSString stringWithFormat:@"](contact:%@)",idString]];
    return newString;
}



-(NSString*)StringByConvertingToHUmanReadable:(NSMutableArray *)entities
{
    NSString *newString =self;
    for (EntityModel *Newentities in entities)
    {
        newString = [newString stringByAppendingString:Newentities.name];
        newString = [newString stringByAppendingString:@" "];
    }
    return newString;
}


- (NSString *)StringByConvertingPartialStringsToFullString:(NSMutableArray*)entities;
{
    NSString *newString =self;
    NSString *bracketString =@"";
    for (EntityModel *Newentities in entities)
    {
        NSString *nameString = Newentities.name;
        if (Newentities.entityType == EntityTypeNormalText||Newentities.entityType == EntityTypeHashTag)
            newString = [newString stringByAppendingString:Newentities.name];
        else
        {
            if (Newentities.entityType == EntityTypeMention)
            {
                nameString = [nameString stringByReplacingOccurrencesOfString:@"@" withString:@""];
                bracketString=@"@[";
            }
     
            else if (Newentities.entityType == EntityTypeshowroom)
            {
                bracketString=@"*[";
                nameString = [nameString stringByReplacingOccurrencesOfString:@"*" withString:@""];
            }
            bracketString = [bracketString stringByAppendingString:nameString];
            NSString *idString = [Newentities.idString stringByReplacingOccurrencesOfString:@")" withString:@""];
            bracketString = [bracketString stringByAppendingString:[NSString stringWithFormat:@"](contact:%@)",idString]];
            newString = [newString stringByAppendingString:bracketString];
        }
        newString = [newString stringByAppendingString:@" "];
    }
    return newString;
}


/*
 else if (Newentities.entityType == EntityTypeHashTag)
 {
 bracketString=@"#[";
 nameString = [nameString stringByReplacingOccurrencesOfString:@"#" withString:@""];
 
 }
 */


@end

