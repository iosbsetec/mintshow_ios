

#import <Foundation/Foundation.h>
#import "Userfeed.h"
#import "coachingTipModel.h"

@interface UserAccessSession : NSObject

+(void)storeUserSession:(Userfeed*)session;

+(void)storeUserSessionValue:(NSString *)value forKey:(NSString *)key;
+(Userfeed*)getUserSession;
+(void)clearAllSession;
+(BOOL)isLoggedIn;

+(void)storeLocationData:(NSDictionary*)adata;
+(NSMutableArray*)getStoredLocationHistory;

+(void)InsertFeedDetailsToDataBase:(NSMutableArray *)feedvalueArray;
+(NSMutableArray*)getAllrecordForMintshowFeedList;
+(void)InsertDiscoverMints:(NSMutableArray *)feedvalueArray;
+(NSMutableArray*)getAllDiscoverMints;

+(void)InsertShowroomDetailsToDataBase:(NSMutableArray *)feedvalueArray;
+(NSMutableArray*)getAllrecordForShowroomList;

+(void)InsertActivityYouDataBase:(NSMutableArray *)feedvalueArray;
+(NSMutableArray*)getAllActivityYouList;

+(BOOL)shouldDisplayDiscoverFirstTimeVisitPopup;
+(BOOL)shouldDisplayShowroomFirstTimeVisitPopup;
+(BOOL)shouldDisplayMintdetailsFirstTimeVisitPopup;
+(BOOL)shouldDisplayFirstTimePostMintPopup;

+(void)setDiscoverFirstTimeVisitCompleted;
+(void)setShowroomFirstTimeVisitCompleted;
+(void)setMintdetailsFirstTimeCompleted;
+(void)setFirstTimePostMintCompleted;

+(void)setAllValuesForcOachingTips:(coachingTipModel *)model;

+(void)InsertActivityRequestDataBase:(NSMutableArray *)feedvalueArray;
+(NSMutableArray*)getAllActivityrequestList;

+(void)InsertActivityMyCircleDataBase:(NSMutableArray *)feedvalueArray;
+(NSMutableArray*)getAllActivityMyCircleList;
@end


/*
 Triggers
 1. discover_all_page_first_time_visit
 2. showroom_page_first_time_visit
 3. mint_detail_page_first_time_visit
 4. is_already_posted_mint
 

*/