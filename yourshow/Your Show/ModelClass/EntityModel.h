//
//  EntityModel.h
//  SearchUser
//
//  Created by Siba Prasad Hota  on 6/1/15.
//  Copyright (c) 2015 Lemonpeak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    EntityTypeNormalText,
    EntityTypeUserName,
    EntityTypeMention,
    EntityTypeHashTag,
    EntityTypeUrl,
    EntityTypeshowroom // No selection indicator
} EntityType;


@interface EntityModel : NSObject

//MentionEntity
@property (nonatomic) NSUInteger  start;
@property (nonatomic) NSUInteger  end;

@property (nonatomic, retain) NSString      *name;
@property (nonatomic, retain) NSString      *screenName;
@property (nonatomic, retain) NSString      *idString;
@property (nonatomic, assign) EntityType    entityType;
@property (nonatomic, retain) NSString      *text;
@property (nonatomic, retain) UIColor       *urlColor;
@property (nonatomic, retain) UIFont        *urlFont;
@property (nonatomic, retain) NSString      *entityTypeTest;
@property (nonatomic, retain) NSString      *is_private;


@end
