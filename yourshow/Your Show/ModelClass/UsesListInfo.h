//
//  UsesListInfo.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 04/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UsesListInfo : NSObject
@property (strong,nonatomic) NSString* follow_status;
@property (strong,nonatomic) NSString *user_id ;
@property (strong,nonatomic) NSString *user_name;
@property (strong,nonatomic) NSString *user_thumb_image;
@property (strong,nonatomic) NSString *time;

-(id)initWithUserInfo:(NSDictionary*)userInfo;
@end
