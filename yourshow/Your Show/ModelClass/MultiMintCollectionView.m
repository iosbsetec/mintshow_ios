//
//  MultiMintCollectionView.m
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MultiMintCollectionView.h"

#import "Feedmodel.h"
#import "PintCollecionCell.h"
#import "GHContextMenuView.h"
#import "STTweetLabel.h"
#import "YSCommentUserLable.h"
#import "NSString+Emojize.h"

#import "MBProgressHUD.h"

@interface MultiMintCollectionView ()<GHContextOverlayViewDataSource, GHContextOverlayViewDelegate>
{
    UILongPressGestureRecognizer* _longPressRecognizer;
    GHContextMenuView* overlay;
}
@property (strong,nonatomic) NSMutableArray* photoList;
@end

#define MultimintCollectionCellBorderTop 0.0
#define MultimintCollectionCellBorderBottom 2.0
#define MultimintCollectionCellBorderLeft 0.0
#define MultimintCollectionCellBorderRight 0.0



@implementation MultiMintCollectionView

@synthesize multiMintdelegate = _multiMintdelegate;
@synthesize multiMintDataSource = _multiMintDataSource;


- (void)initializator
{
    
    // set up delegates
    self.delegate = self;
    self.dataSource = self;
    
    [self registerClass:[PintCollecionCell class] forCellWithReuseIdentifier:@"PintReuse"];
    // set inter-item spacing in the layout
    PintCollectionViewLayout* customLayout = (PintCollectionViewLayout*)self.collectionViewLayout;
   customLayout.interitemSpacing = 4.0;
    
    overlay = [[GHContextMenuView alloc] init];
    overlay.dataSource = self;
    overlay.delegate = self;

   
    
}
- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}





#pragma mark - UICollectionViewDelegateJSPintLayout

- (CGFloat)columnWidthForCollectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout
{
    return ([UIScreen mainScreen].bounds.size.height > 568)?185.0:155.0;
}

- (NSUInteger)maximumNumberOfColumnsForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
{
    return 2;
}

- (CGFloat)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath*)indexPath
{
    Feedmodel *feedData  = [self.multiMintDataSource multiMintCollectionViewView:self DataForItemAtRow:indexPath.row];
    CGFloat widthCell = ([UIScreen mainScreen].bounds.size.height > 568)?185.0:155.0;
    
    feedData.width =(feedData.width>1)?feedData.width:widthCell;
    feedData.height =(feedData.height>1)?feedData.height:65.0;
    
    CGSize rctSizeOriginal = CGSizeMake(feedData.width , feedData.height);
    double scale = (widthCell  - (MultimintCollectionCellBorderLeft + MultimintCollectionCellBorderRight)) / rctSizeOriginal.width;
    CGSize rctSizeFinal = CGSizeMake(rctSizeOriginal.width*scale,rctSizeOriginal.height*scale);
    
    NSString *OriginalFrontendText    =   [feedData.feed_text emojizedString];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGRect txtRect = [OriginalFrontendText boundingRectWithSize:CGSizeMake(widthCell-10, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
   
    CGFloat imageHeight =(![feedData.achievementType isEqualToString:@"0"] && rctSizeFinal.height<135)?135:rctSizeFinal.height;
    
    CGFloat height = 0.0;;
    if (![feedData.achievementType isEqualToString:@""])
        height = imageHeight + txtRect.size.height+75;
    else
        height = 80 + txtRect.size.height;
    
    if ([feedData.isMintReminted isEqualToString:@"YES"])  {
        CGRect recttxtLbl= [[[feedData.remint_message emojizedString] StringByChangingComplimintStringToNormalString] boundingRectWithSize:CGSizeMake(widthCell, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:9.5]} context:nil];
        height = height + 20 +recttxtLbl.size.height;
    }
    return height;
}


- (BOOL)collectionView:(UICollectionView*)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath*)indexPath
{
    return YES;
}




#pragma mark = UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.multiMintDataSource NumberOFrowsForMultiMintView:self];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView
{
//    [self.collectionViewLayout invalidateLayout];
    return 1;
}


// Jiten Code

- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath
{
    PintCollecionCell* cell  = (PintCollecionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"PintReuse" forIndexPath:indexPath];
    cell.layer.cornerRadius  = 3.0f;
    CGRect rectReference     = cell.bounds;
    [cell setClipsToBounds:YES];
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    PNCollectionCellBackgroundView* backgroundView = [[PNCollectionCellBackgroundView alloc] initWithFrame:rectReference];
    backgroundView.backgroundColor                 = [YSSupport randomColor];
    cell.backgroundView                            = backgroundView;
    
    UIView* selectedBackgroundView         = [[UIView alloc] initWithFrame:rectReference];
    selectedBackgroundView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView            = selectedBackgroundView;
    Feedmodel *feedData                    = [self.multiMintDataSource multiMintCollectionViewView:self DataForItemAtRow:indexPath.row];
    
    CGFloat widthCell = ([UIScreen mainScreen].bounds.size.height > 568)?185.0:155.0;
    feedData.width    = (feedData.width>1)?feedData.width:widthCell;
    feedData.height   = (feedData.height>1)?feedData.height:60.0;
    
    
    UIImageView* imageView = [[UIImageView alloc] init];
    [imageView setBackgroundColor:[UIColor whiteColor]];
    [imageView setUserInteractionEnabled:YES];
    
    
    CGSize rctSizeOriginal = CGSizeMake(feedData.width , feedData.height);
    double scale           = (widthCell  - (MultimintCollectionCellBorderLeft + MultimintCollectionCellBorderRight)) / rctSizeOriginal.width;
    CGSize rctSizeFinal    = CGSizeMake(rctSizeOriginal.width * scale,rctSizeOriginal.height * scale);
    UIView *userView       = [[UIView alloc] init];
    
    NSString *OriginalFrontendText    = [feedData.feed_text emojizedString];
    UIFont *font                      = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGRect newRect                    = [OriginalFrontendText boundingRectWithSize:CGSizeMake(widthCell-10, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
    
    UIButton *btnView = [UIButton buttonWithType:UIButtonTypeCustom];
    UILongPressGestureRecognizer*    _longPressRecognizer1 = [[UILongPressGestureRecognizer alloc] initWithTarget:overlay action:@selector(longPressDetected:)];
    CGRect recttxtLbl;
    
    if ([feedData.isMintReminted isEqualToString:@"YES"])
    {
        recttxtLbl                  = [[[feedData.remint_message emojizedString] StringByChangingComplimintStringToNormalString]  boundingRectWithSize:CGSizeMake(155, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:9.5]} context:nil];

                UIView *remintView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rctSizeFinal.width+10, 30+recttxtLbl.size.height)];
                remintView.backgroundColor  = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1];
                
                
                UIImageView *remint_UserImage          = [[UIImageView alloc] initWithFrame:CGRectMake(2,2,24,24)];
                remint_UserImage.backgroundColor       = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1];
                remint_UserImage.layer.cornerRadius    = 12.0f;
                remint_UserImage.mintImageURL              = [NSURL URLWithString:feedData.re_userimg];
        
                UIButton *anButton = [UIButton buttonWithType:UIButtonTypeCustom];
                anButton.frame = remint_UserImage.frame;
                anButton.tag = indexPath.row;
                [anButton addTarget:self action:@selector(clickedRemintedUserImageWithTag:) forControlEvents:UIControlEventTouchUpInside];
        
        
                [remintView addSubview:anButton];

                [remint_UserImage setClipsToBounds:YES];
                [remintView addSubview:remint_UserImage];

        
                UILabel *lblDetails                     = [[UILabel alloc]initWithFrame:CGRectMake(28,-5,rctSizeFinal.width-28,40)];
                lblDetails.numberOfLines                = 2;
                lblDetails.font                = [UIFont fontWithName:@"Helvetica-Bold" size:8.0f];

        
                NSMutableAttributedString * string      = [[NSMutableAttributedString alloc] initWithString:feedData.remint_atriputtedString];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:88.0/255.0 green:172.0/255.0 blue:230.0/255.0 alpha:1] range:NSMakeRange(0,10)];
                
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(11,[feedData.remint_atriputtedString length]-11)];
                [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-Bold" size:9.0f] range:NSMakeRange(0, ([feedData.re_user_id isEqualToString:GETVALUE(CEO_UserId)])?14:[feedData.re_username length]+10)];
                
                
                NSMutableAttributedString *aString = [[NSMutableAttributedString alloc] initWithString:feedData.remint_postedTime];
                [aString setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.5]}
                             range:(NSRange){0,feedData.remint_postedTime.length}];
                
                [string appendAttributedString:aString];
                
               
                
                lblDetails.attributedText = string;
                [remintView addSubview:lblDetails]; // this is username with time
        
        

        
                STTweetLabel *remint_message   = [[STTweetLabel alloc]initWithFrame:CGRectMake(2,30,rctSizeFinal.width-5,recttxtLbl.size.height)];
                remint_message.text              =  [feedData.remint_message emojizedString];
                [remint_message setBackgroundColor:[UIColor clearColor]];
                [remintView addSubview:remint_message];
                remint_message.textAlignment = NSTextAlignmentLeft;
        remint_message.font=[UIFont fontWithName:@"HelveticaNeue" size:9];
         remint_message.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
        {
            if (hotWord == STTweetHashtag)
            {
                [self.multiMintdelegate selectdFeature:string withType:0 isPrivateShowroomAccess:NO];
            }
            
            else if (hotWord == STTweetStartag)
            {
               
                BOOL isAccess = NO;
                for (NSDictionary *dictShowroomInfo in feedData.privateShowroomList) {
                    if ([[dictShowroomInfo valueForKey:@"access_key"]  isEqualToString:@"no"] && [[dictShowroomInfo valueForKey:@"name"]  isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                        isAccess = YES;
                        break;
                    }
                }
            
                
                
                [self.multiMintdelegate selectdFeature:string withType:1 isPrivateShowroomAccess:isAccess];
                
            }
            else if (hotWord == STTweetMention)
            {
                [self.multiMintdelegate selectdFeature:string withType:2 isPrivateShowroomAccess:NO];
                
            }
        };


                if ([feedData.achievementType isEqualToString:@""])
                {
                    STTweetLabel *ycomment    = [[STTweetLabel alloc]initWithFrame:CGRectMake(5,remintView.frame.size.height,rctSizeFinal.width-15, newRect.size.height+8)];
                    ycomment.text               = [feedData.feed_text emojizedString];
                    ycomment.textAlignment = NSTextAlignmentCenter;
                    [cell.contentView addSubview:ycomment];
                    
                    ycomment.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
                    {

                        if (hotWord == STTweetHashtag)
                        {
                            [self.multiMintdelegate selectdFeature:string withType:0 isPrivateShowroomAccess:NO];
                        }
                        
                        else if (hotWord == STTweetStartag)
                        {
                            BOOL isAccess = NO;
                            for (NSDictionary *dictShowroomInfo in feedData.privateShowroomList) {
                                if ([[dictShowroomInfo valueForKey:@"access_key"]  isEqualToString:@"no"] && [[dictShowroomInfo valueForKey:@"name"]  isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                                    isAccess = YES;
                                    break;
                                }
                            }

                            [self.multiMintdelegate selectdFeature:string withType:1 isPrivateShowroomAccess:isAccess];
                            
                        }
                        else if (hotWord == STTweetMention)
                        {
                            [self.multiMintdelegate selectdFeature:string withType:2 isPrivateShowroomAccess:NO];
                            
                        }
                    };
                    

                    
                    [userView setFrame: CGRectMake ( 2,remintView.frame.size.height+ycomment.frame.size.height, 148, 32)];
                    btnView.frame = CGRectMake(MultimintCollectionCellBorderLeft,userView.frame.origin.y,rctSizeFinal.width,newRect.size.height+5);
                }
                else
                {
                    CGFloat imageHeight =(![feedData.achievementType isEqualToString:@"0"] && rctSizeFinal.height<135)?135:rctSizeFinal.height;
                    
                    imageView.frame    = CGRectMake(MultimintCollectionCellBorderLeft,30+recttxtLbl.size.height,rctSizeFinal.width,imageHeight);
                    imageView.mintImageURL = [NSURL URLWithString:feedData.feed_thumb_image_small];
                    imageView.mintImageURL = [NSURL URLWithString:feedData.feed_thumb_image];
                    [imageView setBackgroundColor:[YSSupport randomColor]];
                    [cell.contentView addSubview:imageView];
                    
                    CGRect rctLabel          = CGRectMake(5,imageHeight + remintView.frame.size.height,rctSizeFinal.width-8,newRect.size.height+8);
                    STTweetLabel *ycomment = [[STTweetLabel alloc]initWithFrame:rctLabel];
                    ycomment.text            = [feedData.feed_text emojizedString];
                    ycomment.textAlignment = NSTextAlignmentCenter;
                    [cell.contentView addSubview:ycomment];
                    
                    ycomment.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
                    {

                        
                        if (hotWord == STTweetHashtag)
                        {
                            [self.multiMintdelegate selectdFeature:string withType:0 isPrivateShowroomAccess:NO];
                        }
                        
                        else if (hotWord == STTweetStartag)
                        {
                            BOOL isAccess = NO;
                            for (NSDictionary *dictShowroomInfo in feedData.privateShowroomList) {
                                if ([[dictShowroomInfo valueForKey:@"access_key"]  isEqualToString:@"no"] && [[dictShowroomInfo valueForKey:@"name"]  isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                                    isAccess = YES;
                                    break;
                                }
                            }
                            
                            [self.multiMintdelegate selectdFeature:string withType:1 isPrivateShowroomAccess:isAccess];
                        }
                        else if (hotWord == STTweetMention)
                        {
                            [self.multiMintdelegate selectdFeature:string withType:2 isPrivateShowroomAccess:NO];
                            
                        }
                    };
                    

                    
                    [userView setFrame: CGRectMake ( 2,ycomment.frame.size.height+ycomment.frame.origin.y, 148, 32)];
                    btnView.frame = CGRectMake(MultimintCollectionCellBorderLeft,ycomment.frame.size.height+ycomment.frame.origin.y,rctSizeFinal.width,newRect.size.height+imageHeight);
                }
                [cell.contentView addSubview:remintView];
    }
    else
    {
                    if ([feedData.achievementType isEqualToString:@""])
                    {
                        STTweetLabel *ycomment = [[STTweetLabel alloc]initWithFrame:CGRectMake(5,7,rctSizeFinal.width-8, newRect.size.height+15)];
                        ycomment.text = [feedData.feed_text emojizedString];
                        ycomment.textAlignment = NSTextAlignmentCenter;
                        [cell.contentView addSubview:ycomment];
                        
                        ycomment.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
                        {

                            
                            if (hotWord == STTweetHashtag)
                            {
                                [self.multiMintdelegate selectdFeature:string withType:0 isPrivateShowroomAccess:NO];
                            }
                            
                            else if (hotWord == STTweetStartag)
                            {
                                BOOL isAccess = NO;
                                for (NSDictionary *dictShowroomInfo in feedData.privateShowroomList) {
                                    if ([[dictShowroomInfo valueForKey:@"access_key"]  isEqualToString:@"no"] && [[dictShowroomInfo valueForKey:@"name"]  isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                                        isAccess = YES;
                                        break;
                                    }
                                }
                                
                                [self.multiMintdelegate selectdFeature:string withType:1 isPrivateShowroomAccess:isAccess];
                            }
                            else if (hotWord == STTweetMention)
                            {
                                [self.multiMintdelegate selectdFeature:string withType:2 isPrivateShowroomAccess:NO];
                                
                            }
                        };
                        

                        
                        [userView setFrame: CGRectMake ( 2,ycomment.frame.size.height, 148, 32)];
                        btnView.frame = CGRectMake(MultimintCollectionCellBorderLeft,userView.frame.size.height + newRect.size.height+15,rctSizeFinal.width,newRect.size.height+5);
                    }
                    else
                    {
                        CGFloat imageHeight =(![feedData.achievementType isEqualToString:@"0"] && rctSizeFinal.height<135)?135:rctSizeFinal.height;
                        
                        imageView.frame    = CGRectMake(MultimintCollectionCellBorderLeft,MultimintCollectionCellBorderTop,rctSizeFinal.width,imageHeight);
                        imageView.mintImageURL = [NSURL URLWithString:feedData.feed_thumb_image_small];
                        imageView.mintImageURL = [NSURL URLWithString:feedData.feed_thumb_image];
                        [imageView setBackgroundColor:[YSSupport randomColor]];
                        [cell.contentView addSubview:imageView];
                        
                        // Create the path (with only the top-left corner rounded)
                        UIBezierPath *maskPath  = [UIBezierPath bezierPathWithRoundedRect:imageView.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight                                                         cornerRadii:CGSizeMake(3.0, 3.0)];
                        // Create the shape layer and set its path
                        CAShapeLayer *maskLayer = [CAShapeLayer layer];
                        maskLayer.frame         = imageView.bounds;
                        maskLayer.path          = maskPath.CGPath;
                        // Set the newly created shape layer as the mask for the image view's layer
                        imageView.layer.mask    = maskLayer;
                        
                        CGRect rctLabel          = CGRectMake(5,imageHeight + 5,rctSizeFinal.width-8,newRect.size.height+8);
                        STTweetLabel *ycomment   = [[STTweetLabel alloc]initWithFrame:rctLabel];
                        ycomment.text            = [feedData.feed_text emojizedString];
                        ycomment.textAlignment   = NSTextAlignmentLeft;
                        [cell.contentView addSubview:ycomment];
                        
                        ycomment.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
                        {
                            
                            if (hotWord == STTweetHashtag)
                            {
                                [self.multiMintdelegate selectdFeature:string withType:0 isPrivateShowroomAccess:NO];
                            }
                            
                            else if (hotWord == STTweetStartag)
                            {
                                BOOL isAccess = NO;
                                for (NSDictionary *dictShowroomInfo in feedData.privateShowroomList) {
                                    if ([[dictShowroomInfo valueForKey:@"access_key"]  isEqualToString:@"no"] && [[dictShowroomInfo valueForKey:@"name"]  isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                                        isAccess = YES;
                                        break;
                                    }
                                }
                                
                                [self.multiMintdelegate selectdFeature:string withType:1 isPrivateShowroomAccess:isAccess];
                            }
                            else if (hotWord == STTweetMention)
                            {
                                [self.multiMintdelegate selectdFeature:string withType:2 isPrivateShowroomAccess:NO];
                                
                            }
                        };
                        
                        
                        [userView setFrame: CGRectMake ( 2,imageHeight+newRect.size.height+8, 148, 32)];
                        btnView.frame = CGRectMake(MultimintCollectionCellBorderLeft,imageHeight+newRect.size.height+userView.frame.size.height+8,rctSizeFinal.width,newRect.size.height+imageHeight);
                        
                        
                    }
    }

    
    btnView.tag = indexPath.row;
    [cell.contentView addSubview:btnView];
    [btnView addGestureRecognizer:_longPressRecognizer1];
    
    
    UIImageView *userImage          = [[UIImageView alloc] initWithFrame:CGRectMake(1,2,26,26)];
    userImage.backgroundColor       = [UIColor grayColor];
    userImage.layer.cornerRadius    = 13.0f;
    userImage.mintImageURL              = [NSURL URLWithString:feedData.user_thumb_image];
    YSCommentUserLable *lableUserName   = [[YSCommentUserLable alloc]initWithFrame:CGRectMake(32,4,CGRectGetWidth(userView.frame)- 27,33) withFont:9.0 attributeTextColor:[UIColor darkGrayColor]];
    lableUserName.text              = feedData.attributedName;
    [userImage setClipsToBounds:YES];
    [userView addSubview:lableUserName];
    
    UIButton *anButton = [UIButton buttonWithType:UIButtonTypeCustom];
    anButton.frame = userImage.frame;
    anButton.tag = indexPath.row;
    [anButton addTarget:self action:@selector(clickedProfileUserImageWithTag:) forControlEvents:UIControlEventTouchUpInside];
    [userView addSubview:anButton];
    
    lableUserName.detectionBlock = ^(EntityModel *hotWord, NSRange range)  {
        if (hotWord.entityType == EntityTypeUserName) {
            [self.multiMintdelegate selectdFeature:hotWord.name withType:2 isPrivateShowroomAccess:NO];
        }
    };
    
    [userView addSubview:userImage];
    [cell.contentView addSubview:userView];
    [cell.contentView addSubview:[self createButtonView:btnView.frame index:indexPath.row withDta:feedData withCell:cell]];
    if ([feedData.achievementType isEqualToString:@"1"])
    {
        UIButton *buttonPlay = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonPlay setFrame:imageView.frame];
        UIImageView *imgV    = [[UIImageView alloc]init];
        [imgV setFrame:CGRectMake((imageView.frame.size.width-30)/2, (imageView.frame.size.height-30)/2, 30, 30)];
        [imgV setImage:[UIImage imageNamed:@"media_laundge_play_btn_ios.png"]];
        [imageView addSubview:imgV];
    }
    
    return cell;
}



-(IBAction)clickedProfileUserImageWithTag:(id)sender{
    Feedmodel *fmodel  = [self.multiMintDataSource multiMintCollectionViewView:self DataForItemAtRow:[sender tag]];
    [self.multiMintdelegate buttonActionclickedFortype:4 forRow:[sender tag] feedModelData:fmodel];
}
-(IBAction)clickedRemintedUserImageWithTag:(id)sender{
    Feedmodel *fmodel  = [self.multiMintDataSource multiMintCollectionViewView:self DataForItemAtRow:[sender tag]];
    [self.multiMintdelegate buttonActionclickedFortype:5 forRow:[sender tag] feedModelData:fmodel];
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    Feedmodel *fmodel  = [self.multiMintDataSource multiMintCollectionViewView:self DataForItemAtRow:indexPath.row];
    [self.multiMintdelegate buttonActionclickedFortype:3 forRow:indexPath.row feedModelData:fmodel];
    
}

-(CGFloat)getHeight : (NSString *) feedText widthCell:(CGFloat)width
{
    UIFont *font = [UIFont boldSystemFontOfSize:10];
    CGRect new = [feedText boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
    return new.size.height;
}

//jiten code

-(UIView *)createButtonView : (CGRect)rctSizeFinal index :(NSInteger)index withDta : (Feedmodel *)modelData withCell : (PintCollecionCell *)cell
{
    UIView *polygonView;
    
    if (![modelData.isMintReminted isEqualToString:@"YES"])
    {polygonView = [[UIView alloc] initWithFrame:rctSizeFinal];}
    else
    { polygonView           = [[UIView alloc] initWithFrame: CGRectMake(0, rctSizeFinal.origin.y +30, rctSizeFinal.size.width, rctSizeFinal.size.height)];}
    
    if (self.bounds.size.height<=568) {
        return [self createSmallButtonView:rctSizeFinal index:index withDta:modelData withCell:cell];
    }

    
    [polygonView setTag:5999];
    cell.buttonHighFive = [UIButton buttonWithType:UIButtonTypeCustom];
    [cell.buttonHighFive setFrame:CGRectMake( 8,5,24,20)];
    [cell.buttonHighFive setImage:[UIImage imageNamed:([modelData.is_user_liked integerValue])?@"HighFive_Icon_24x24":@"hive_it_normal"] forState:UIControlStateNormal];
    [cell.buttonHighFive addTarget:self action:@selector(hifiveClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonHighFive setTag:index];
    
    CGRect rctLabel1                    = CGRectMake( 35 ,8,20,14);
    cell.labeHighFiveCount          = [[UILabel alloc] initWithFrame:rctLabel1];
    cell.labeHighFiveCount.textColor         = [UIColor grayColor];
    cell.labeHighFiveCount.numberOfLines     = 0;
    cell.labeHighFiveCount.font              = [UIFont fontWithName:@"Helvetica Bold" size:11.0];
    cell.labeHighFiveCount.text              = [modelData feed_like_count];
    cell.labeHighFiveCount.textAlignment     = NSTextAlignmentLeft;
    cell.labeHighFiveCount.textColor           =      ([modelData.is_user_liked integerValue])?ORANGECOLOR:[UIColor lightGrayColor];

    
    UIButton *buttonComplimint = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonComplimint setFrame:CGRectMake( 59 ,5,24,20)];
    [buttonComplimint setImage:([[modelData isUserCommented] integerValue])?[UIImage imageNamed:@"complimint_Selected"]:[UIImage imageNamed:@"Btn_CompliMint"] forState:UIControlStateNormal];
    [buttonComplimint addTarget:self action:@selector(gotoCommentScreen:) forControlEvents:UIControlEventTouchUpInside];
    [buttonComplimint setTag:index ];
    
    CGRect rctLabel2                       = CGRectMake( 84 ,8,40,14);
    UILabel* labelComplimintCount          = [[UILabel alloc] initWithFrame:rctLabel2];
    labelComplimintCount.textColor         = [UIColor grayColor];
    labelComplimintCount.numberOfLines     = 0;
    labelComplimintCount.font                 = [UIFont fontWithName:@"Helvetica Bold" size:11.0];
    labelComplimintCount.textAlignment = NSTextAlignmentLeft;
    labelComplimintCount.text              = [modelData feed_comment_count];
    labelComplimintCount.textColor           =      ([modelData.isUserCommented integerValue])?ORANGECOLOR:[UIColor lightGrayColor];

    [polygonView addSubview:cell.labeHighFiveCount];
    [polygonView addSubview:cell.buttonHighFive];
    [polygonView addSubview:buttonComplimint];
    [polygonView addSubview:labelComplimintCount];
    
    if (![modelData.user_id isEqualToString:GETVALUE(CEO_UserId)] && ![modelData.re_user_id isEqualToString:GETVALUE(CEO_UserId)]){
        UIButton *buttonRemint = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonRemint setFrame:CGRectMake(102 ,7,34,17)];
        [buttonRemint setImage:[UIImage imageNamed:([modelData.is_user_reminted isEqualToString:@"1"])?@"Icon_ReMint_Selected":@"Icon_ReMint"] forState:UIControlStateNormal];
        [buttonRemint addTarget:self action:@selector(gotoRemintScreen:) forControlEvents:UIControlEventTouchUpInside];
        [buttonRemint setTag:index];
        
        CGRect rctLabelRemint         = CGRectMake(138 ,8,40,14);
        UILabel* labelRemint          = [[UILabel alloc] initWithFrame:rctLabelRemint];
        labelRemint.textColor         = ([modelData.is_user_reminted isEqualToString:@"1"])?[UIColor orangeColor]:[UIColor grayColor];
        labelRemint.numberOfLines     = 0;
        labelRemint.font                 = [UIFont fontWithName:@"Helvetica Bold" size:11.0];
        labelRemint.textAlignment = NSTextAlignmentLeft;
        labelRemint.text              = [modelData remint_count];
        labelRemint.textColor           =      ([modelData.is_user_reminted integerValue])?ORANGECOLOR:[UIColor lightGrayColor];

        [polygonView addSubview:labelRemint];
        [polygonView addSubview:buttonRemint];
    }
    return polygonView;


}

-(UIView *)createSmallButtonView : (CGRect)rctSizeFinal index :(NSInteger)index withDta : (Feedmodel *)modelData withCell : (PintCollecionCell *)cell
{
    UIView *polygonView = [[UIView alloc] initWithFrame: rctSizeFinal];
    
    cell.buttonHighFive = [UIButton buttonWithType:UIButtonTypeCustom];
    [cell.buttonHighFive setFrame:CGRectMake( 8,5,24,20)];
    [cell.buttonHighFive setImage:[UIImage imageNamed:([modelData.is_user_liked integerValue])?@"HighFive_Icon_24x24":@"hive_it_normal"] forState:UIControlStateNormal];
    [cell.buttonHighFive addTarget:self action:@selector(hifiveClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonHighFive setTag:index];
    
    CGRect rctLabel1                    = CGRectMake( 35 ,8,20,14);
    cell.labeHighFiveCount          = [[UILabel alloc] initWithFrame:rctLabel1];
    cell.labeHighFiveCount.textColor         = [UIColor grayColor];
    cell.labeHighFiveCount.numberOfLines     = 0;
    cell.labeHighFiveCount.font              = [UIFont fontWithName:@"Helvetica Bold" size:10.0];
    cell.labeHighFiveCount.text              = [modelData feed_like_count];
    cell.labeHighFiveCount.textAlignment = NSTextAlignmentLeft;
    cell.labeHighFiveCount.textColor           =      ([modelData.is_user_liked integerValue])?ORANGECOLOR:[UIColor lightGrayColor];

    
    
    UIButton *buttonComplimint = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonComplimint setFrame:CGRectMake(59 ,5,24,20)];
    [buttonComplimint setImage:[UIImage imageNamed:@"Btn_CompliMint"] forState:UIControlStateNormal];
    [buttonComplimint addTarget:self action:@selector(gotoCommentScreen:) forControlEvents:UIControlEventTouchUpInside];
    [buttonComplimint setTag:index ];
    
    CGRect rctLabel2                       = CGRectMake( 84 ,8,40,14);
    UILabel* labelComplimintCount          = [[UILabel alloc] initWithFrame:rctLabel2];
    labelComplimintCount.textColor         = [UIColor grayColor];
    labelComplimintCount.numberOfLines     = 0;
    labelComplimintCount.font                 = [UIFont fontWithName:@"Helvetica Bold" size:10.0];
    labelComplimintCount.textAlignment = NSTextAlignmentLeft;
    labelComplimintCount.text              = [modelData feed_comment_count];
    labelComplimintCount.textColor           =      ([modelData.isUserCommented integerValue])?ORANGECOLOR:[UIColor lightGrayColor];

    [polygonView addSubview:cell.labeHighFiveCount];
    [polygonView addSubview:cell.buttonHighFive];
    [polygonView addSubview:buttonComplimint];
    [polygonView addSubview:labelComplimintCount];
    
    //gotoRemintScreen
    if (![modelData.user_id isEqualToString:GETVALUE(CEO_UserId)] && ![modelData.re_user_id isEqualToString:GETVALUE(CEO_UserId)]){
        UIButton *buttonRemint = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonRemint setFrame:CGRectMake( 102 ,7,34,17)];
        [buttonRemint setImage:[UIImage imageNamed:([modelData.is_user_reminted isEqualToString:@"1"])?@"Icon_ReMint_Selected":@"Icon_ReMint"] forState:UIControlStateNormal];
        [buttonRemint addTarget:self action:@selector(gotoRemintScreen:) forControlEvents:UIControlEventTouchUpInside];
        [buttonRemint setTag:index];
        
        CGRect rctLabelRemint         = CGRectMake(138 ,8,40,14);
        UILabel* labelRemint          = [[UILabel alloc] initWithFrame:rctLabelRemint];
        labelRemint.textColor         = ([modelData.is_user_reminted isEqualToString:@"active"])?[UIColor orangeColor]:[UIColor grayColor];
        labelRemint.numberOfLines     = 0;
        labelRemint.font              = [UIFont fontWithName:@"Helvetica Bold" size:10.0];
        labelRemint.textAlignment = NSTextAlignmentLeft;
        labelRemint.text              = [modelData remint_count];

        labelRemint.textColor         =      ([modelData.is_user_reminted integerValue])?ORANGECOLOR:[UIColor lightGrayColor];

        [polygonView addSubview:labelRemint];
        [polygonView addSubview:buttonRemint];
        
        
    }
    return polygonView;
    
    
}


static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.86;

static BOOL ShouldLoadNextPage(UICollectionView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self);
    if (shouldLoadNextPage)
    {
        [self.multiMintdelegate LoadNextPagetriggered];
    }
}





#pragma mark - GHMenu methods

-(BOOL) shouldShowMenuAtPoint:(CGPoint)point
{
    NSIndexPath* indexPath = [self indexPathForItemAtPoint:point];
    UICollectionViewCell* cell = [self cellForItemAtIndexPath:indexPath];
    
    return cell != nil;
}

- (NSInteger) numberOfMenuItems
{
    return 4;
}

-(UIImage*) imageForItemAtIndex:(NSInteger)index
{
    
    NSString* imageName = nil;
    switch (index) {
        case 0:
            imageName = @"inspire_icon_normal";
            break;
        case 1:
            imageName = @"";
            break;
        case 2:
            imageName = @"flag.png";
            break;
        case 3:
            imageName = @"share_profile_icon";
            break;
            
            
        default:
            break;
    }
    return [UIImage imageNamed:imageName];
}

- (void) didSelectItemAtIndex:(NSInteger)selectedIndex forMenuAtPoint:(CGPoint)point selectedMint:(int)MintTag {
    NSString* msg = nil;
    switch (selectedIndex) {
        case 0:
            msg = @"Inspire Selected";
            //  [self InspireClicked:selectedIndex];
            break;
        case 1:
            msg = @"HighFive Selected";
            //  [self hifiveClicked:selectedIndex];
            
            break;
        case 2:
            msg = @"PlanToDo Selected";
            //[self NominateClicked:selectedIndex];
            
            break;
        case 3:
            msg = @"Linkedin Selected";
            //  [self showActionSheetForShare];
            break;
        case 4:
            msg = @"Pinterest Selected";
            break;
            
        default:
            break;
    }
}

-(void)gotoCommentScreen : (UIButton *) sender {
    [self.multiMintdelegate buttonActionclickedFortype:1 forRow:sender.tag feedModelData:nil];
}


-(void)gotoRemintScreen : (UIButton *) sender {
    [self.multiMintdelegate buttonActionclickedFortype:2 forRow:sender.tag feedModelData:nil];
}

-(void)remint : (UIButton *) sender {
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollViewd{
    //[self.multiMintdelegate scrollViewDidScroll:scrollViewd];
}

- (IBAction)hifiveClicked:(UIButton*)sender {
    Feedmodel *fmodel  = [self.multiMintDataSource multiMintCollectionViewView:self DataForItemAtRow:sender.tag];
    [self changeHifiveStatusForRow:sender.tag];

    [YSAPICalls hifiveMint:fmodel withSuccessionBlock:^(id response) {
        Generalmodel *newResponse=response;
         if ([newResponse.status_code intValue] == 1) {
             
             
         }
         else {
             [self changeHifiveStatusForRow:sender.tag];
         }
     } andFailureBlock:^(NSError *error)  {
        [self changeHifiveStatusForRow:sender.tag];
    }];
}


-(void)changeHifiveStatusForRow:(NSInteger)row {
    Feedmodel *fmodel  = [self.multiMintDataSource multiMintCollectionViewView:self DataForItemAtRow:row];
    int hiveCount      = [fmodel.feed_like_count intValue];
    hiveCount          = ([fmodel.is_user_liked intValue]==0)?(hiveCount+1):(hiveCount-1);
    fmodel.feed_like_count = [NSString stringWithFormat:@"%d",hiveCount];
    fmodel.is_user_liked   = ([fmodel.is_user_liked intValue]==0)?@"1":@"0";
    [self.multiMintdelegate buttonActionclickedFortype:0 forRow:row feedModelData:fmodel];
   
    
    PintCollecionCell *cell =  (PintCollecionCell *) [self cellForItemAtIndexPath:[NSIndexPath indexPathForItem:row inSection:0]];
    [cell.buttonHighFive setImage:[UIImage imageNamed:([fmodel.is_user_liked integerValue])?@"HighFive_Icon_24x24":@"hive_it_normal"] forState:UIControlStateNormal];
    cell.labeHighFiveCount.text              = [fmodel feed_like_count];
    cell.labeHighFiveCount.textColor           =      ([fmodel.is_user_liked integerValue])?ORANGECOLOR:[UIColor lightGrayColor];


    
//    BOOL animationsEnabled = [UIView areAnimationsEnabled];
//    [UIView setAnimationsEnabled:NO];
//    [self performBatchUpdates:^{
//        [ self reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]]];
//    } completion:^(BOOL finished) {
//        [UIView setAnimationsEnabled:animationsEnabled];
//    }];
}

@end

/*
 static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.46;
 
 static BOOL ShouldLoadNextPage(UICollectionView *collectionView)
 {
 CGFloat yOffset = collectionView.contentOffset.y;
 CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
 return yOffset / height > kNewPageLoadScrollPercentageThreshold;
 }
 
 -(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
 {
 BOOL shouldLoadNextPage = ShouldLoadNextPage(self);
 if (shouldLoadNextPage)
 {
 [self.multiMintdelegate LoadNextPagetriggered];
 }
 }

 
 
 -(UIView *)createButtonView : (CGRect)rctSizeFinal index :(NSInteger)index withDta : (Feedmodel *)modelData
 {
 UIView *polygonView = [[UIView alloc] initWithFrame: rctSizeFinal];
 
 UIButton *buttonHighFive = [UIButton buttonWithType:UIButtonTypeCustom];
 [buttonHighFive setFrame:CGRectMake(4,1,18,16)];
 [buttonHighFive setImage:[UIImage imageNamed:([modelData.is_user_liked integerValue])?@"HighFive_Icon_24x24":@"hive_it_normal"] forState:UIControlStateNormal];
 [buttonHighFive addTarget:self action:@selector(hifiveClicked:) forControlEvents:UIControlEventTouchUpInside];
 [buttonHighFive setTag:index];
 
 UIButton *buttonComplimint = [UIButton buttonWithType:UIButtonTypeCustom];
 [buttonComplimint setFrame:CGRectMake(45,2,18,16)];
 [buttonComplimint setImage:[UIImage imageNamed:@"Btn_CompliMint"] forState:UIControlStateNormal];
 [buttonComplimint addTarget:self action:@selector(gotoCommentScreen:) forControlEvents:UIControlEventTouchUpInside];
 [buttonComplimint setTag:index ];
 
 if (![modelData.user_id isEqualToString:GETVALUE(CEO_UserId)]){
 UIButton *buttonRemint = [UIButton buttonWithType:UIButtonTypeCustom];
 [buttonRemint setFrame:CGRectMake(85,2,30,15)];
 [buttonRemint setImage:[UIImage imageNamed:@"Icon_ReMint"] forState:UIControlStateNormal];
 [buttonRemint addTarget:self action:@selector(gotoRemintScreen:) forControlEvents:UIControlEventTouchUpInside];
 [buttonRemint setTag:index];
 [polygonView addSubview:buttonRemint];
 
 CGRect rctLabelRemint         = CGRectMake(120,2,18,15);
 UILabel* labelRemint          = [[UILabel alloc] initWithFrame:rctLabelRemint];
 labelRemint.textColor         = [UIColor grayColor];
 labelRemint.numberOfLines     = 0;
 labelRemint.font              = [UIFont systemFontOfSize:10];
 labelRemint.text              = [modelData remint_count];
 [polygonView addSubview:labelRemint];
 [polygonView addSubview:buttonRemint];
 }
 
 [polygonView addSubview:buttonHighFive];
 [polygonView addSubview:buttonComplimint];
 
 CGRect rctLabel1                    = CGRectMake(26,2,18,14);
 UILabel* labeHighFiveCount          = [[UILabel alloc] initWithFrame:rctLabel1];
 labeHighFiveCount.textColor         = [UIColor grayColor];
 labeHighFiveCount.numberOfLines     = 0;
 labeHighFiveCount.font              = [UIFont systemFontOfSize:10];
 labeHighFiveCount.text              = [modelData feed_like_count];
 
 CGRect rctLabel2                      = CGRectMake(67,2,18,14);
 UILabel* labelComplimintCount          = [[UILabel alloc] initWithFrame:rctLabel2];
 labelComplimintCount.textColor         = [UIColor grayColor];
 labelComplimintCount.numberOfLines     = 0;
 labelComplimintCount.font              = [UIFont systemFontOfSize:10];
 labelComplimintCount.text              = [modelData feed_comment_count];
 
 
 [polygonView addSubview:labeHighFiveCount];
 [polygonView addSubview:labelComplimintCount];
 
 return polygonView;
 }
 */
