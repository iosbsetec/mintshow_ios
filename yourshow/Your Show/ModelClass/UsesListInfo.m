//
//  UsesListInfo.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 04/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "UsesListInfo.h"

@implementation UsesListInfo
-(id)initWithUserInfo:(NSDictionary*)userInfo
{

    if (self == [super init]) {
        self.follow_status              =   [userInfo   valueForKey:@"follow_status"];
        self.user_id                    =   [userInfo   valueForKey:@"user_id"];
        self.user_name                  =   [userInfo   valueForKey:@"user_name"];
        self.user_thumb_image           =   [userInfo   valueForKey:@"user_thumb_image"];
        self.time                       =   [userInfo   valueForKey:@"time"];
    }

    return self;
}

@end
