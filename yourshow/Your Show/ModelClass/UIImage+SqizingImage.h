//
//  UIImage+SqizingImage.h
//  Conciergist
//
//  Created by macuser on 28/02/13.
//  Copyright (c) 2013 macuser. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SqizingImage)

+ (UIImage*)imageWithImage:(UIImage *)image
              scaledToSize:(CGSize)newSize;

@end
