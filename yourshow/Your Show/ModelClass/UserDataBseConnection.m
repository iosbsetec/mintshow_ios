

//
//  UserDataBseConnection.m
//  FormGenerator
//
//  Created by Siba Prasad Hota on 06/12/12.
//  Copyright Lemon Peak marketing Private Limited (c) 2012 macuser. All rights reserved.
//

#import "UserDataBseConnection.h"
#import "Userfeed.h"
#import "Keys.h"
#import "BaseFeedModel.h"
#import "ActivityModel.h"
#import "RequestActivityModel.h"

@implementation UserDataBseConnection

-(id)init
{
    self = [super init];
    if(self)
    {
        databaseName = @"MintshowDB.sqlite";
        // Get the path to the documents directory and append the databaseName
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
    }
    return self;
}

-(void)setUp
{
    databaseName = @"MintshowDB.sqlite";
    // Get the path to the documents directory and append the databaseName
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
    
    
}

-(void)checkAndCreateDatabase
{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:databasePath];
    if(success) return;
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
    //   NSLog(@"DATABASE PATH IS : %@",databasePath);
}


-(void)apiCalltodatabase:(NSString*)statement andSecondStateMent:(NSString*)secondStatement{
    [self checkAndCreateDatabase];
    sqlite3 *database;
    sqlite3_stmt *compiledSelectStatement;
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)  {
        NSLog(@"5 THE STATEMENT IS : %@",statement);
        const char *sql = [statement UTF8String];
        if(sqlite3_prepare_v2(database, sql, -1, &compiledSelectStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledSelectStatement) == SQLITE_ROW) {
            }
        }
        sqlite3_finalize(compiledSelectStatement);
        
        if (secondStatement) {
            NSLog(@"5 THE secondStatement IS : %@",secondStatement);

            sqlite3_stmt *compiledSelectStatement2;
            const char *sql2 = [secondStatement UTF8String];
            if(sqlite3_prepare_v2(database, sql2, -1, &compiledSelectStatement2, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledSelectStatement2) == SQLITE_ROW) {
                }
            }
            sqlite3_finalize(compiledSelectStatement2);
        }
    }
    sqlite3_close(database);
}


-(void)apiCalltoLocaldatabasetoFetchExistingData:(NSString*)statement
                             withSuccessionBlock:(void(^)(sqlite3_stmt *response))successBlock
                                 andFailureBlock:(void(^)(NSError *error))failureBlock{
    sqlite3 *database;
    sqlite3_stmt *compiledSelectStatement;
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)  {
        const char *sql = [statement UTF8String];
        if(sqlite3_prepare_v2(database, sql, -1, &compiledSelectStatement, NULL) == SQLITE_OK)  {
            while(sqlite3_step(compiledSelectStatement) == SQLITE_ROW)  {
                successBlock(compiledSelectStatement);
            }
        }
        sqlite3_finalize(compiledSelectStatement);
    }else{
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"something went wrong" forKey:NSLocalizedDescriptionKey];
        failureBlock([NSError errorWithDomain:@"world" code:200 userInfo:details]);
    }
    sqlite3_close(database);
    
}


/*
 CREATE TABLE "showroomData" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "showroom_id" VARCHAR NOT NULL  DEFAULT NA,"showroom_link" VARCHAR,"showroom_name" VARCHAR,"showroom_type" VARCHAR,"showroom_categoryName" VARCHAR,"is_user_joined" VARCHAR,"showroom_img_Url" VARCHAR,"showroom_image" VARCHAR,"showroom_tag_keywords_ids" VARCHAR,"showroom_category_ids" VARCHAR,"follow_icon_status" VARCHAR,"follow_status" VARCHAR,"member_count" VARCHAR,"show_tag" VARCHAR,"mint_count" VARCHAR,"privacy_settings" VARCHAR,"show_option" VARCHAR,"showroom_description" VARCHAR,"user_link" VARCHAR,"user_name" VARCHAR,"user_thumb_image_url" VARCHAR,"selected" VARCHAR)
 */

-(void)InsertShowroomDetailsToDataBase:(NSMutableArray *)feedvalueArray{
    NSMutableString *query = [[NSMutableString alloc]init];
    for(ShowRoomModel *value in feedvalueArray){
        NSString *sqlQuery = nil;
        sqlQuery = [NSString stringWithFormat:@"('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@'),",value.showroom_id,value.showroom_link,[value.showroom_name stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.showroom_type,value.showroom_categoryName,value.is_user_joined,value.showroom_img_Url,value.showroom_image,value.showroom_tag_keywords_ids,value.showroom_category_ids,value.follow_icon_status,value.follow_status,value.member_count,value.show_tag,value.mint_count,value.privacy_settings,value.show_option,[value.showroom_description stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.user_link,value.user_name,value.user_thumb_image_url,value.selected];
        [query appendString:sqlQuery];
    }
    
    if (query.length >0) {
        [query deleteCharactersInRange:NSMakeRange([query length]-1, 1)];
        query = [NSMutableString stringWithFormat:@"INSERT INTO showroomData (showroom_id,showroom_link,showroom_name,showroom_type,showroom_categoryName,is_user_joined,showroom_img_Url,showroom_image,showroom_tag_keywords_ids,showroom_category_ids,follow_icon_status,follow_status,member_count,show_tag,mint_count,privacy_settings,show_option,showroom_description,user_link,user_name,user_thumb_image_url,selected)  values%@",query];
        NSLog(@"query=%@",query);
        [self apiCalltodatabase:@"DELETE FROM showroomData" andSecondStateMent:query];
    }
}

-(NSMutableArray*)getAllrecordForShowroomList{
    [self checkAndCreateDatabase];
    NSMutableArray * employeeArray = [[NSMutableArray alloc] init];
    NSString *statement = @"SELECT * From showroomData";
    [self apiCalltoLocaldatabasetoFetchExistingData:statement withSuccessionBlock:^(sqlite3_stmt *response) {
        [employeeArray addObject:[self createObjectFromrawDataforAllShowroomList:response]];
    } andFailureBlock:^(NSError *error) {
        
    }];
    return employeeArray;
}



-(void)InsertFeedDetailsToDataBase:(NSMutableArray *)feedvalueArray{
    [self insertMints:@"feedData" andData:feedvalueArray];
}


-(NSMutableArray*)getAllrecordForMintshowFeedList{
   return [self getAllrecordForMints:@"feedData"];
}

-(void)InsertDiscoverMintsFeedDetailsToDataBase:(NSMutableArray *)feedvalueArray{
    [self insertMints:@"DiscoverfeedData" andData:feedvalueArray];
}


-(NSMutableArray*)getAllrecordForDiscoverMintsMintshowFeedList{
    return [self getAllrecordForMints:@"DiscoverfeedData"];
}


-(NSMutableArray*)getAllrecordForMints:(NSString *)tableName{
    [self checkAndCreateDatabase];
    NSMutableArray * employeeArray = [[NSMutableArray alloc] init];
    NSString *statement = [NSString stringWithFormat:@"SELECT * From %@",tableName];
    [self apiCalltoLocaldatabasetoFetchExistingData:statement withSuccessionBlock:^(sqlite3_stmt *response) {
        [employeeArray addObject:[self createObjectFromrawDataforAllFeedrecordDetails:response]];
    } andFailureBlock:^(NSError *error) {
        
    }];
    return employeeArray;
}


/*
 CREATE TABLE "feedData" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "feed_id" VARCHAR NOT NULL  DEFAULT NA, "category_type" VARCHAR DEFAULT NA, "feed_comment_count" VARCHAR DEFAULT NA, "feed_created_date" VARCHAR DEFAULT NA, "feed_inspiration_count" VARCHAR, "feed_like_count" VARCHAR, "feed_nomination_count" VARCHAR DEFAULT m,"long" VARCHAR DEFAULT NA, "lat" VARCHAR DEFAULT NA, "feed_posted_location" VARCHAR DEFAULT NA, "feed_posted_time" VARCHAR
 ,"is_user_inspired" INTEGER DEFAULT 0,"is_user_liked" INTEGER DEFAULT 0,"is_video_status" INTEGER DEFAULT 0,"isUserFollowed" INTEGER DEFAULT 0,"is_user_nominated" INTEGER DEFAULT 0,"is_user_reminted" INTEGER DEFAULT 0,"is_user_commented" INTEGER DEFAULT 0,"IsMintReminted" INTEGER DEFAULT 0,"mintType" INTEGER DEFAULT 0, "media_id" VARCHAR, "original_mint_id" VARCHAR, "remint_count" VARCHAR, "showroom_name" VARCHAR, "feed_user_id" VARCHAR, "feed_user_link" VARCHAR, "feed_user_name" VARCHAR, "feed_thumb_image" VARCHAR, "mint_row_id" VARCHAR, "remint_val" VARCHAR,  "mintdetail_url" VARCHAR, "feed_posted_edited_text" VARCHAR, "app_connection_url" VARCHAR, "feed_thumb_height" VARCHAR, "feed_thumb_Width" VARCHAR, "youtube_id" VARCHAR, "feed_title" VARCHAR, "feed_description" VARCHAR, "remint_AtributtedString" VARCHAR, "remint_postedTime" VARCHAR, "remint_username" VARCHAR, "remint_message" VARCHAR, "remint_userimg" VARCHAR, "remint_user_id" VARCHAR,"feed_thumb_image_small" VARCHAR, "feed_user_thumb_image" VARCHAR,"is_mint_private" INTEGER DEFAULT 0)
 */




-(void)insertMints:(NSString *)tableName andData:(NSMutableArray *)data
{
    NSMutableString *query = [[NSMutableString alloc]init];
    for(BaseFeedModel *value in data){
        NSString *sqlQuery = nil;
        sqlQuery = [NSString stringWithFormat:@"('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',%d,%d,%d,%d,%d,%d,%d,%d,%d,'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',%d,'%@','%@',%d),",value.feed_id,value.category_type,value.feed_comment_count,value.feed_created_date,value.feed_inspiration_count,value.feed_like_count,value.feed_nomination_count,value.feed_longitude,value.feed_latitude,value.feed_posted_location,value.feed_posted_time,value.is_user_inspired,value.is_user_liked,value.is_video_status,value.is_user_followed,value.is_user_nominated,value.is_user_reminted,value.is_user_commented,value.IsMintReminted,[self getMintTupeNumberFromType:value.mintType],value.media_id,value.original_mint_id,value.remint_count,value.showroom_name,value.feed_user_id,value.feed_user_link,value.feed_user_name,value.feed_thumb_image,value.mint_row_id,value.remint_val,value.mintdetail_url,value.feed_posted_edited_text,value.app_connection_url,[NSString stringWithFormat:@"%f",value.feed_thumb_height],[NSString stringWithFormat:@"%f",value.feed_thumb_Width],value.youtube_id,[value.feed_title stringByReplacingOccurrencesOfString:@"'" withString:@"''"],[value.feed_description stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.remint_AtributtedString,value.remint_postedTime,value.remint_username,value.remint_message,value.remint_userimg,value.remint_user_id,value.feed_thumb_image_small,value.feed_user_thumb_image,value.is_private_showroom_mint,value.is_mint_streak,value.streak_days,value.minttype_exists];
        [query appendString:sqlQuery];
    }
    
    if (query.length >0) {
        [query deleteCharactersInRange:NSMakeRange([query length]-1, 1)];
        query = [NSMutableString stringWithFormat:@"INSERT INTO %@ (feed_id,category_type,feed_comment_count,feed_created_date,feed_inspiration_count,feed_like_count,feed_nomination_count,long,lat,feed_posted_location,feed_posted_time,is_user_inspired,is_user_liked,is_video_status,isUserFollowed,is_user_nominated,is_user_reminted,is_user_commented,IsMintReminted,mintType,media_id,original_mint_id,remint_count,showroom_name,feed_user_id,feed_user_link,feed_user_name,feed_thumb_image,mint_row_id,remint_val,mintdetail_url,feed_posted_edited_text,app_connection_url,feed_thumb_height,feed_thumb_Width,youtube_id,feed_title,feed_description,remint_AtributtedString,remint_postedTime,remint_username,remint_message,remint_userimg,remint_user_id,feed_thumb_image_small,feed_user_thumb_image,is_mint_private,is_mint_streak,streak_days,minttype_exists)  values%@",tableName,query];
        NSLog(@"query=%@",query);
        [self apiCalltodatabase:[NSString stringWithFormat:@"DELETE FROM %@",tableName] andSecondStateMent:query];
    }
}



/*

-(void)InsertActivityListToDataBase:(NSMutableArray *)feedvalueArray{
    NSMutableString *query = [[NSMutableString alloc]init];
    for(ActivityModel *value in feedvalueArray){
        NSString *sqlQuery = nil;
        sqlQuery = [NSString stringWithFormat:@"('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%d'),",value.activity_id,value.activity_type,[value.activity_text stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.activity,value.activity_thumb_image,value.activity_image,value.activity_user_id,value.activity_user_name,value.activity_showroom_id,[value.activity_showroom_name stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.activity_showroom_tag,value.activity_showroom_thumb_image,value.activity_total_pages,value.activity_feed_thumb_image,value.first_user_name,value.first_user_id,value.second_user_name,value.second_user_id,value.status_commnetEdited,(value.postingStatus)?1:0];
        [query appendString:sqlQuery];
    }
    
    if (query.length >0) {
        [query deleteCharactersInRange:NSMakeRange([query length]-1, 1)];
        query = [NSMutableString stringWithFormat:@"INSERT INTO activityYouData (activity_id,activity_type,activity_text,activity,activity_thumb_image,activity_image,activity_user_id,activity_user_name,activity_showroom_id,activity_showroom_name,activity_showroom_tag,activity_showroom_thumb_image,activity_total_pages,activity_feed_thumb_image,first_user_name,first_user_id,second_user_name,second_user_id,status_commnetEdited,postingStatus)  values%@",query];
        NSLog(@"query=%@",query);
        [self apiCalltodatabase:@"DELETE FROM activityYouData" andSecondStateMent:query];
    }
}
*/
-(NSMutableArray*)getAllrecordForActivityYouList{
    [self checkAndCreateDatabase];
    NSMutableArray * employeeArray = [[NSMutableArray alloc] init];
    NSString *statement = @"SELECT * From activityYouData";
    [self apiCalltoLocaldatabasetoFetchExistingData:statement withSuccessionBlock:^(sqlite3_stmt *response) {
        [employeeArray addObject:[self createObjectFromrawDataforAllActivityList:response]];
    } andFailureBlock:^(NSError *error) {
        
    }];
    return employeeArray;
}

/*
 CREATE TABLE "activityYouData" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "activity_id" VARCHAR NOT NULL  DEFAULT NA,"activity_type" VARCHAR,"activity_text" VARCHAR,"activity_time" VARCHAR,"activity_image" VARCHAR,"activity_isVideo" VARCHAR,"activity" VARCHAR,"activity_thumb_image" VARCHAR,"activity_user_id" VARCHAR,"activity_user_name" VARCHAR,"activity_showroom_id" VARCHAR,"activity_showroom_name" VARCHAR,"activity_showroom_tag" VARCHAR,"activity_showroom_thumb_image" VARCHAR,"activity_total_pages" VARCHAR,"activity_feed_thumb_image" VARCHAR,"first_user_name" VARCHAR,"first_user_id" VARCHAR,"second_user_name" VARCHAR,"second_user_id" VARCHAR,"status_commnetEdited" VARCHAR,"postingStatus" INTEGER DEFAULT 0)
 */
/*-(ActivityModel *)createObjectFromrawDataforAllActivityList:(sqlite3_stmt *)response{
    ActivityModel *list_Param           =[[ActivityModel alloc]init];
    list_Param.activity_id               =  [self gettheValue:(char*)sqlite3_column_text(response,1)];
    list_Param.activity_type             =  [self gettheValue:(char*)sqlite3_column_text(response,2)];
    list_Param.activity_text             =  [self gettheValue:(char*)sqlite3_column_text(response,3)];
    list_Param.activity_time             =  [self gettheValue:(char*)sqlite3_column_text(response,4)];
    list_Param.activity_image     =  [self gettheValue:(char*)sqlite3_column_text(response,5)];
    list_Param.activity_isVideo            =  [self gettheValue:(char*)sqlite3_column_text(response,6)];
    list_Param.activity          =  [self gettheValue:(char*)sqlite3_column_text(response,7)];
    list_Param.activity_thumb_image            =  [self gettheValue:(char*)sqlite3_column_text(response,8)];
    list_Param.activity_user_id =  [self gettheValue:(char*)sqlite3_column_text(response,9)];
    list_Param.activity_user_name     =  [self gettheValue:(char*)sqlite3_column_text(response,10)];
    list_Param.activity_showroom_id        =  [self gettheValue:(char*)sqlite3_column_text(response,11)];
    list_Param.activity_showroom_name             =  [self gettheValue:(char*)sqlite3_column_text(response,12)];
    list_Param.activity_showroom_tag              =  [self gettheValue:(char*)sqlite3_column_text(response,13)];
    list_Param.activity_showroom_thumb_image                  =  [self gettheValue:(char*)sqlite3_column_text(response,14)];
    list_Param.activity_total_pages                =  [self gettheValue:(char*)sqlite3_column_text(response,15)];
    list_Param.activity_feed_thumb_image          =  [self gettheValue:(char*)sqlite3_column_text(response,16)];
    list_Param.first_user_name               =  [self gettheValue:(char*)sqlite3_column_text(response,17)];
    list_Param.first_user_id      =  [self gettheValue:(char*)sqlite3_column_text(response,18)];
    list_Param.second_user_name                 =  [self gettheValue:(char*)sqlite3_column_text(response,19)];
    list_Param.second_user_id                 =  [self gettheValue:(char*)sqlite3_column_text(response,20)];
    list_Param.status_commnetEdited      =  [self gettheValue:(char*)sqlite3_column_text(response,21)];
    list_Param.postingStatus                  =  ([[self gettheValue:(char*)sqlite3_column_text(response,22)] intValue])?YES:NO;
    
    return list_Param;
}

*/


-(BOOL)shouldDisplayPopupforType:(YScoachTipType)coachTipType{
    
    coachingTipModel *coachTip = (coachingTipModel *)[self getrecordForCoachingTip];
    if (!coachTip) {
        return YES;
    }
    
    switch (coachTipType) {
        case YScoachTipTypeAddMintFirstTime:
            return (!coachTip.postmintFirstTime);
            break;
        case YScoachTipTypeDiscoverFirstTime:
            return (!coachTip.discoverFirstTime);
            break;
        case YScoachTipTypeMintDetailsFirstTime:
            return (!coachTip.mintDetailsFirstTime);
            break;
        case YScoachTipTypeShowroomFirstTime:
            return (!coachTip.showroomFirstTime);
            break;
        default:
            return YES;
            break;
    }
    
    return NO;
}

-(void)updateLoginTimeCoachingTipdata:(coachingTipModel *)coachTip{
    NSString *statement =[NSString stringWithFormat:@"UPDATE coachData SET col1 = %d,col2 = %d,col3 = %d,col4 = %d,col5 = 0,col6 = 0,col7 = 0,col8 = 0,col9 = 0,col10 = 0",coachTip.postmintFirstTime,coachTip.discoverFirstTime,coachTip.mintDetailsFirstTime,coachTip.showroomFirstTime];
    [self apiCalltodatabase:statement andSecondStateMent:nil];
}


-(coachingTipModel *)getrecordForCoachingTip{
    [self checkAndCreateDatabase];
    NSMutableArray * employeeArray = [[NSMutableArray alloc] init];
    NSString *statement = @"SELECT * From coachData";
    [self apiCalltoLocaldatabasetoFetchExistingData:statement withSuccessionBlock:^(sqlite3_stmt *response) {
        [employeeArray addObject:[self createObjectFromrawDataforCoachingTiposDetails:response]];
    } andFailureBlock:^(NSError *error) {
        
    }];
    return (employeeArray.count)?employeeArray[0]:nil;
}


-(void)UpdateDataforType:(YScoachTipType)coachTipType{
    
    NSString *key = @"col1";
    switch (coachTipType) {
        case YScoachTipTypeAddMintFirstTime:
            key=@"col1";
            break;
        case YScoachTipTypeDiscoverFirstTime:
            key=@"col2";
            break;
        case YScoachTipTypeMintDetailsFirstTime:
            key=@"col3";
            break;
        case YScoachTipTypeShowroomFirstTime:
            key=@"col4";
            break;
        default:
            key=@"col5";
            break;
    }
    
    [self checkAndCreateDatabase];
    [self apiCalltodatabase:[NSString stringWithFormat:@"UPDATE coachData SET %@ = 1",key] andSecondStateMent:nil];
    NSLog(@"Completed");
    
}


-(void)setAllValuesForcOachingTips : (coachingTipModel *)model{
    [self apiCalltodatabase:[NSString stringWithFormat:@"UPDATE coachData SET col1 = %d,col2 = %d,col3 = %d,col4 = %d,col5 = 0,col6 = 0,col7 = 0,col8 = 0,col9 = 0,col10 = 0",model.postmintFirstTime,model.discoverFirstTime,model.mintDetailsFirstTime,model.showroomFirstTime] andSecondStateMent:nil];
}


-(coachingTipModel*)createObjectFromrawDataforCoachingTiposDetails:(sqlite3_stmt *)response{
    coachingTipModel *list_Param=[[coachingTipModel alloc]init];
    list_Param.postmintFirstTime    = sqlite3_column_int(response,0);
    list_Param.discoverFirstTime    = sqlite3_column_int(response,1);
    list_Param.mintDetailsFirstTime = sqlite3_column_int(response,2);
    list_Param.showroomFirstTime    = sqlite3_column_int(response,3);
    return list_Param;
}
/*
 Are you sure you want to perform the following operation(s):
 Create Table "main"."coachData"
 SQL:
 CREATE  TABLE "main"."coachData" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "col1" INTEGER NOT NULL  DEFAULT 0, "col2" INTEGER NOT NULL  DEFAULT 0, "col3" INTEGER NOT NULL  DEFAULT 0, "col4" INTEGER NOT NULL  DEFAULT 0, "col5" INTEGER NOT NULL  DEFAULT 0, "col6" INTEGER NOT NULL  DEFAULT 0, "col7" INTEGER NOT NULL  DEFAULT 0, "col8" INTEGER NOT NULL  DEFAULT 0, "col9" INTEGER NOT NULL  DEFAULT 0, "col10" INTEGER NOT NULL  DEFAULT 0, "col11" INTEGER NOT NULL  DEFAULT 0)
 */

//DiscoverfeedData

-(void)clearAllSession{
     NSString *statement =@"DELETE FROM userData";
    [self apiCalltodatabase:statement andSecondStateMent:@"UPDATE coachData SET col1 = 0,col2 = 0,col3 = 0,col4 = 0,col5 = 0,col6 = 0,col7 = 0,col8 = 0,col9 = 0,col10 = 0"];
    [self apiCalltodatabase:@"DELETE FROM showroomData" andSecondStateMent:@"DELETE FROM feedData"];
    [self apiCalltodatabase:@"DELETE FROM DiscoverfeedData" andSecondStateMent:@"DELETE FROM activityYouData"];
    [self apiCalltodatabase:@"DELETE FROM activityMyCircleData" andSecondStateMent:@"DELETE FROM activityRequestData"];
}



-(void)InsertUserDetailsToDataBase:(Userfeed *)value{
   NSString *statement = [NSString stringWithFormat:@"INSERT INTO userData(userid,email,password,userName,first_Name,last_Name,gender,login_Status,date_of_birth,device_Token,unique_Device_ID,phone_Number,Profile_image_url,Profile_thumb_url,user_type,location,active,confirmed,website,about,age,Referral_id,twitter_id,date_created,AccessToken,powerMember,followingCount,followerCount,stage,total_achievemints,total_encouragemints,existing_userSocial,version,discover_all_page_first_time_visit,showroom_page_first_time_visit) VALUES ('%@','%@','%@','%@','%@','%@','%@',%d,'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",value.User_id,value.email,value.password,value.userName,value.first_name,value.last_name,value.gender,1,value.date_of_birth,value.User_deviceToken,value.User_DeviceId,value.phone_Number,value.user_image_url,value.avatarImgurl,value.user_type,value.location,value.active,value.confirmed,value.website,value.about,value.age,value.Referral_id,value.twitter_id,value.date_created,value.AccessToken,value.powerMember,value.followingCount,value.followerCount,value.stage,value.total_achievemints,value.total_encouragemints,value.existing_userSocial,value.version,value.discover_all_page_first_time_visit,value.showroom_page_first_time_visit];
    [self apiCalltodatabase:@"DELETE FROM userData " andSecondStateMent:statement];
}

-(NSMutableArray*)getAllrecordForUser{
    [self checkAndCreateDatabase];
    NSMutableArray * employeeArray = [[NSMutableArray alloc] init];
    NSString *statement = @"SELECT * From userData";
    [self apiCalltoLocaldatabasetoFetchExistingData:statement withSuccessionBlock:^(sqlite3_stmt *response) {
        [employeeArray addObject:[self createObjectFromrawDataforAllrecordDetails:response]];
    } andFailureBlock:^(NSError *error) {
        
    }];
    return employeeArray;
}

-(void)UpdateRecordToRecordDataBase:(NSString*)value  keyData:(NSString*)key{
    [self checkAndCreateDatabase];
    [self apiCalltodatabase:[NSString stringWithFormat:@"UPDATE userData SET %@ = '%@'",key,value] andSecondStateMent:nil];
        NSLog(@"Completed");
}

-(void)UpdateUserDetailsToDataBase:(Userfeed *)value{
    
}


-(void)insertTodb:(NSString *)placeID withReference:(NSString*)reference andDescription:(NSString*)description{
    NSString *statement = [NSString stringWithFormat:@"INSERT INTO LocationHistory(placeID,reference,description) VALUES ('%@','%@','%@')",placeID,reference,description];
    [self apiCalltodatabase:statement andSecondStateMent:@"DELETE FROM LocationHistory WHERE id NOT IN (SELECT id FROM LocationHistory ORDER BY id  DESC LIMIT 0,5)"];
}
-(NSDictionary*)createObjectFromrawDataforAllLocationDetails:(sqlite3_stmt *)response{
    
    return  [NSDictionary dictionaryWithObjectsAndKeys:[self gettheValue:(char*)sqlite3_column_text(response,1)],@"place_id",[self gettheValue:(char*)sqlite3_column_text(response,2)],@"reference",[self gettheValue:(char*)sqlite3_column_text(response,3)],@"description", nil];
}


-(NSMutableArray*)getAllrecordForStoredLocation{
    [self checkAndCreateDatabase];
    NSMutableArray * employeeArray = [[NSMutableArray alloc] init];
    NSString *statement = @"SELECT * From LocationHistory ORDER BY id DESC LIMIT 0,5";
    
    [self apiCalltoLocaldatabasetoFetchExistingData:statement withSuccessionBlock:^(sqlite3_stmt *response) {
        [employeeArray addObject:[self  createObjectFromrawDataforAllLocationDetails:response]];
    } andFailureBlock:^(NSError *error) {
        
    }];
    return employeeArray;
}


/*
 CREATE TABLE "userData" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "userid" VARCHAR NOT NULL  DEFAULT NA, "email" VARCHAR DEFAULT NA, "password" VARCHAR DEFAULT NA, "userName" VARCHAR DEFAULT NA, "first_Name" VARCHAR, "last_Name" VARCHAR, "gender" VARCHAR DEFAULT m, "login_Status" INTEGER DEFAULT 0, "date_of_birth" VARCHAR DEFAULT NA, "device_Token" VARCHAR DEFAULT NA, "unique_Device_ID" VARCHAR DEFAULT NA, "phone_Number" VARCHAR, "Profile_image_url" VARCHAR DEFAULT NA, "Profile_thumb_url" VARCHAR, "user_type" VARCHAR, "location" VARCHAR, "active" VARCHAR, "confirmed" VARCHAR, "website" VARCHAR, "about" VARCHAR, "age" VARCHAR, "Referral_id" VARCHAR, "twitter_id" VARCHAR, "date_created" VARCHAR, "AccessToken" VARCHAR, "powerMember" VARCHAR, "followingCount" VARCHAR, "followerCount" VARCHAR, "stage" VARCHAR, "total_achievemints" VARCHAR,  "total_encouragemints" VARCHAR, "existing_userSocial" VARCHAR, "version" VARCHAR, "discover_all_page_first_time_visit" VARCHAR, "showroom_page_first_time_visit" VARCHAR)
 */
-(Userfeed*)createObjectFromrawDataforAllrecordDetails:(sqlite3_stmt *)response{
    Userfeed *list_Param=[[Userfeed alloc]init];
    list_Param.User_id =  [self gettheValue:(char*)sqlite3_column_text(response,1)];
    list_Param.email =  [self gettheValue:(char*)sqlite3_column_text(response,2)];
    list_Param.password =  [self gettheValue:(char*)sqlite3_column_text(response,3)];
    list_Param.userName =  [self gettheValue:(char*)sqlite3_column_text(response,4)];
    list_Param.first_name =  [self gettheValue:(char*)sqlite3_column_text(response,5)];
    list_Param.last_name =  [self gettheValue:(char*)sqlite3_column_text(response,6)];
    list_Param.gender =  [self gettheValue:(char*)sqlite3_column_text(response,7)];
    list_Param.login_Status=[[self gettheValue:(char*)sqlite3_column_text(response,8)] integerValue];
    list_Param.date_of_birth =  [self gettheValue:(char*)sqlite3_column_text(response,9)];
    
//    list_Param.device_Token =  [self gettheValue:(char*)sqlite3_column_text(response,10)];
//    list_Param.unique_Device_ID =  [self gettheValue:(char*)sqlite3_column_text(response,11)];
    
    list_Param.phone_Number =  [self gettheValue:(char*)sqlite3_column_text(response,12)];
    list_Param.user_image_url =  [self gettheValue:(char*)sqlite3_column_text(response,13)];
    list_Param.avatarImgurl = [self gettheValue:(char*)sqlite3_column_text(response,14)];
    list_Param.user_type =  [self gettheValue:(char*)sqlite3_column_text(response,14)];
    list_Param.location =  [self gettheValue:(char*)sqlite3_column_text(response,16)];
    list_Param.active =  [self gettheValue:(char*)sqlite3_column_text(response,17)];
    list_Param.confirmed =  [self gettheValue:(char*)sqlite3_column_text(response,18)];
    list_Param.website =  [self gettheValue:(char*)sqlite3_column_text(response,19)];
    list_Param.about =  [self gettheValue:(char*)sqlite3_column_text(response,20)];
    list_Param.age =  [self gettheValue:(char*)sqlite3_column_text(response,21)];
    list_Param.Referral_id =  [self gettheValue:(char*)sqlite3_column_text(response,22)];
    list_Param.twitter_id =  [self gettheValue:(char*)sqlite3_column_text(response,23)];
    list_Param.date_created =  [self gettheValue:(char*)sqlite3_column_text(response,24)];
    list_Param.AccessToken =  [self gettheValue:(char*)sqlite3_column_text(response,25)];
    list_Param.powerMember =  [self gettheValue:(char*)sqlite3_column_text(response,26)];
    list_Param.followingCount =  [self gettheValue:(char*)sqlite3_column_text(response,27)];
    list_Param.followerCount =  [self gettheValue:(char*)sqlite3_column_text(response,28)];
    list_Param.stage =  [self gettheValue:(char*)sqlite3_column_text(response,29)];
    list_Param.total_achievemints =  [self gettheValue:(char*)sqlite3_column_text(response,30)];
    list_Param.total_encouragemints =  [self gettheValue:(char*)sqlite3_column_text(response,31)];
    list_Param.existing_userSocial =  [self gettheValue:(char*)sqlite3_column_text(response,32)];
    list_Param.version =  [self gettheValue:(char*)sqlite3_column_text(response,33)];
    list_Param.discover_all_page_first_time_visit =  [self gettheValue:(char*)sqlite3_column_text(response,34)];
    list_Param.showroom_page_first_time_visit =  [self gettheValue:(char*)sqlite3_column_text(response,35)];
    
    return list_Param;
}

/*
 CREATE TABLE "showroomData" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "showroom_id" VARCHAR NOT NULL  DEFAULT NA,"showroom_link" VARCHAR,"showroom_name" VARCHAR,"showroom_type" VARCHAR,"showroom_categoryName" VARCHAR,"is_user_joined" VARCHAR,"showroom_img_Url" VARCHAR,"showroom_image" VARCHAR,"showroom_tag_keywords_ids" VARCHAR,"showroom_category_ids" VARCHAR,"follow_icon_status" VARCHAR,"follow_status" VARCHAR,"member_count" VARCHAR,"show_tag" VARCHAR,"mint_count" VARCHAR,"privacy_settings" VARCHAR,"show_option" VARCHAR,"showroom_description" VARCHAR,"user_link" VARCHAR,"user_name" VARCHAR,"user_thumb_image_url" VARCHAR,"selected" VARCHAR)
 */

-(ShowRoomModel*)createObjectFromrawDataforAllShowroomList:(sqlite3_stmt *)response{
    ShowRoomModel *list_Param           =[[ShowRoomModel alloc]init];
    list_Param.showroom_id               =  [self gettheValue:(char*)sqlite3_column_text(response,1)];
    list_Param.showroom_link             =  [self gettheValue:(char*)sqlite3_column_text(response,2)];
    list_Param.showroom_name             =  [self gettheValue:(char*)sqlite3_column_text(response,3)];
    list_Param.showroom_type             =  [self gettheValue:(char*)sqlite3_column_text(response,4)];
    list_Param.showroom_categoryName     =  [self gettheValue:(char*)sqlite3_column_text(response,5)];
    list_Param.is_user_joined            =  [self gettheValue:(char*)sqlite3_column_text(response,6)];
    list_Param.showroom_img_Url          =  [self gettheValue:(char*)sqlite3_column_text(response,7)];
    list_Param.showroom_image            =  [self gettheValue:(char*)sqlite3_column_text(response,8)];
    list_Param.showroom_tag_keywords_ids =  [self gettheValue:(char*)sqlite3_column_text(response,9)];
    list_Param.showroom_category_ids     =  [self gettheValue:(char*)sqlite3_column_text(response,10)];
    list_Param.follow_icon_status        =  [self gettheValue:(char*)sqlite3_column_text(response,11)];
    list_Param.follow_status             =  [self gettheValue:(char*)sqlite3_column_text(response,12)];
    list_Param.member_count              =  [self gettheValue:(char*)sqlite3_column_text(response,13)];
    list_Param.show_tag                  =  [self gettheValue:(char*)sqlite3_column_text(response,14)];
    list_Param.mint_count                =  [self gettheValue:(char*)sqlite3_column_text(response,15)];
    list_Param.privacy_settings          =  [self gettheValue:(char*)sqlite3_column_text(response,16)];
    list_Param.show_option               =  [self gettheValue:(char*)sqlite3_column_text(response,17)];
    list_Param.showroom_description      =  [self gettheValue:(char*)sqlite3_column_text(response,18)];
    list_Param.user_link                 =  [self gettheValue:(char*)sqlite3_column_text(response,19)];
    list_Param.user_name                 =  [self gettheValue:(char*)sqlite3_column_text(response,20)];
    list_Param.user_thumb_image_url      =  [self gettheValue:(char*)sqlite3_column_text(response,21)];
    list_Param.selected                  =  [self gettheValue:(char*)sqlite3_column_text(response,22)];
    return list_Param;
}

/*
 CREATE TABLE "feedData" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "feed_id" VARCHAR NOT NULL  DEFAULT NA, "category_type" VARCHAR DEFAULT NA, "feed_comment_count" VARCHAR DEFAULT NA, "feed_created_date" VARCHAR DEFAULT NA, "feed_inspiration_count" VARCHAR, "feed_like_count" VARCHAR, "feed_nomination_count" VARCHAR DEFAULT m,"long" VARCHAR DEFAULT NA, "lat" VARCHAR DEFAULT NA, "feed_posted_location" VARCHAR DEFAULT NA, "feed_posted_time" VARCHAR
 ,"is_user_inspired" INTEGER DEFAULT 0,"is_user_liked" INTEGER DEFAULT 0,"is_video_status" INTEGER DEFAULT 0,"isUserFollowed" INTEGER DEFAULT 0,"is_user_nominated" INTEGER DEFAULT 0,"is_user_reminted" INTEGER DEFAULT 0,"is_user_commented" INTEGER DEFAULT 0,"IsMintReminted" INTEGER DEFAULT 0,"mintType" INTEGER DEFAULT 0, "media_id" VARCHAR, "original_mint_id" VARCHAR, "remint_count" VARCHAR, "showroom_name" VARCHAR, "feed_user_id" VARCHAR, "feed_user_link" VARCHAR, "feed_user_name" VARCHAR, "feed_thumb_image" VARCHAR, "mint_row_id" VARCHAR, "remint_val" VARCHAR,  "mintdetail_url" VARCHAR, "feed_posted_edited_text" VARCHAR, "app_connection_url" VARCHAR, "feed_thumb_height" VARCHAR, "feed_thumb_Width" VARCHAR, "youtube_id" VARCHAR, "feed_title" VARCHAR, "feed_description" VARCHAR, "remint_AtributtedString" VARCHAR, "remint_postedTime" VARCHAR, "remint_username" VARCHAR, "remint_message" VARCHAR, "remint_userimg" VARCHAR, "remint_user_id" VARCHAR, "feed_thumb_image_small" VARCHAR)
 */

-(BaseFeedModel*)createObjectFromrawDataforAllFeedrecordDetails:(sqlite3_stmt *)response{
    BaseFeedModel *list_Param=[[BaseFeedModel alloc]init];
    list_Param.feed_id                  =  [self gettheValue:(char*)sqlite3_column_text(response,1)];
    list_Param.category_type            =  [self gettheValue:(char*)sqlite3_column_text(response,2)];
    list_Param.feed_comment_count       =  [self gettheValue:(char*)sqlite3_column_text(response,3)];
    list_Param.feed_created_date        =  [self gettheValue:(char*)sqlite3_column_text(response,4)];
    list_Param.feed_inspiration_count   =  [self gettheValue:(char*)sqlite3_column_text(response,5)];
    list_Param.feed_like_count          =  [self gettheValue:(char*)sqlite3_column_text(response,6)];
    list_Param.feed_nomination_count    =  [self gettheValue:(char*)sqlite3_column_text(response,7)];
    list_Param.feed_longitude           =  [self gettheValue:(char*)sqlite3_column_text(response,8)];
    list_Param.feed_latitude            =  [self gettheValue:(char*)sqlite3_column_text(response,9)];
    list_Param.feed_posted_location     =  [self gettheValue:(char*)sqlite3_column_text(response,10)];
    list_Param.feed_posted_time         =  [self gettheValue:(char*)sqlite3_column_text(response,11)] ;
    list_Param.is_user_inspired         =  sqlite3_column_int(response, 47);;
    list_Param.is_user_liked            =  sqlite3_column_int(response, 13);
    list_Param.is_video_status          =  sqlite3_column_int(response, 14);
    list_Param.is_user_followed         =  sqlite3_column_int(response, 15);
    list_Param.is_user_nominated        =  sqlite3_column_int(response, 16);
    list_Param.is_user_reminted         =  sqlite3_column_int(response, 17);
    list_Param.is_user_commented        =  sqlite3_column_int(response, 18);
    list_Param.IsMintReminted           =  sqlite3_column_int(response, 19);
    list_Param.mintType                 =  [self getMintTupeFromNumber:[[self gettheValue:(char*)sqlite3_column_text(response,20)] intValue]];
    list_Param.media_id                 =  [self gettheValue:(char*)sqlite3_column_text(response,21)];
    list_Param.original_mint_id         =  [self gettheValue:(char*)sqlite3_column_text(response,22)];
    list_Param.remint_count             =  [self gettheValue:(char*)sqlite3_column_text(response,23)];
    list_Param.showroom_name            =  [self gettheValue:(char*)sqlite3_column_text(response,24)];
    list_Param.feed_user_id             =  [self gettheValue:(char*)sqlite3_column_text(response,25)];
    list_Param.feed_user_link           =  [self gettheValue:(char*)sqlite3_column_text(response,26)];
    list_Param.feed_user_name           =  [self gettheValue:(char*)sqlite3_column_text(response,27)];
    list_Param.feed_thumb_image         =  [self gettheValue:(char*)sqlite3_column_text(response,28)];
    list_Param.mint_row_id              =  [self gettheValue:(char*)sqlite3_column_text(response,29)];
    list_Param.remint_val               =  [self gettheValue:(char*)sqlite3_column_text(response,30)];
    list_Param.mintdetail_url           =  [self gettheValue:(char*)sqlite3_column_text(response,31)];
    list_Param.feed_posted_edited_text  =  [self gettheValue:(char*)sqlite3_column_text(response,32)];
    list_Param.app_connection_url       =  [self gettheValue:(char*)sqlite3_column_text(response,33)];
    list_Param.feed_thumb_height        =  [[self gettheValue:(char*)sqlite3_column_text(response,34)] floatValue];
    list_Param.feed_thumb_Width         =  [[self gettheValue:(char*)sqlite3_column_text(response,35)] floatValue];
    list_Param.youtube_id               =  [self gettheValue:(char*)sqlite3_column_text(response,36)];
    list_Param.feed_title               =  [self gettheValue:(char*)sqlite3_column_text(response,37)];
    list_Param.feed_description         =  [self gettheValue:(char*)sqlite3_column_text(response,38)];
    list_Param.remint_AtributtedString  =  [self gettheValue:(char*)sqlite3_column_text(response,39)];
    list_Param.remint_postedTime        =  [self gettheValue:(char*)sqlite3_column_text(response,40)];
    list_Param.remint_username          =  [self gettheValue:(char*)sqlite3_column_text(response,41)];
    list_Param.remint_message           =  [self gettheValue:(char*)sqlite3_column_text(response,42)];
    list_Param.remint_userimg           =  [self gettheValue:(char*)sqlite3_column_text(response,43)];
    list_Param.remint_user_id           =  [self gettheValue:(char*)sqlite3_column_text(response,44)];
    list_Param.feed_thumb_image_small   =  [self gettheValue:(char*)sqlite3_column_text(response,45)];
    list_Param.feed_user_thumb_image    =  [self gettheValue:(char*)sqlite3_column_text(response,46)];
    list_Param.is_private_showroom_mint = sqlite3_column_int(response, 47);
    list_Param.streak_days              =  [self gettheValue:(char*)sqlite3_column_text(response,48)];
    list_Param.is_mint_streak           =  [self gettheValue:(char*)sqlite3_column_text(response,49)];
    list_Param.minttype_exists          =  sqlite3_column_int(response, 50);
    return list_Param;
}

/*
 0=text
 1=Image
 2= video
 3=youtube
 4= instagram
 */

-(MintType)getMintTupeFromNumber:(int)minttypenumber{
    switch (minttypenumber) {
        case 0:
            return MSMintTypeText;
            break;
        case 1:
            return MSMintTypeImage;
            break;
        case 2:
            return MSMintTypeVideo;
            break;
        case 3:
            return MSMintTypeYouTube;
            break;
        case 4:
            return MSMintTypeInsthaGram;
            break;
        default:
            break;
    }
    return MSMintTypeText;
}


-(int)getMintTupeNumberFromType:(MintType)minttype{
    switch (minttype) {
        case MSMintTypeText:
            return 0;
            break;
        case MSMintTypeImage:
            return 1;
            break;
        case MSMintTypeVideo:
            return 2;
            break;
        case MSMintTypeYouTube:
            return 3;
            break;
        case MSMintTypeInsthaGram:
            return 4;
            break;
        default:
            break;
    }
    return 0;
}



-(NSString *)gettheValue:(char *)somevalue{
    return (somevalue)?[NSString stringWithUTF8String:somevalue]:@"";
}

-(void)InsertRequestActivityListToDataBase:(NSMutableArray *)feedvalueArray{
    NSMutableString *query = [[NSMutableString alloc]init];
    for(RequestActivityModel  *value in feedvalueArray){
        NSString *sqlQuery = nil;
        sqlQuery = [NSString stringWithFormat:@"('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%d'),",value.activity,value.activity_type,[value.activity_text stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.activity_time,value.activity_image,value.activity_id, value.activity_thumb_image,value.activity_user_id,value.activity_user_name,value.activity_showroom_id,[value.activity_showroom_name stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.activity_showroom_tag,value.activity_showroom_thumb_image,value.activity_total_pages,value.activity_feed_thumb_image,value.is_follow_status, value.status_commnetEdited,(value.postingStatus)?1:0];
        [query appendString:sqlQuery];
    }
    
    if (query.length >0) {
        [query deleteCharactersInRange:NSMakeRange([query length]-1, 1)];
        query = [NSMutableString stringWithFormat:@"INSERT INTO activityRequestData (activity,activity_type,activity_text,activity_time,activity_image,activity_id,activity_thumb_image,activity_user_id,activity_user_name,activity_showroom_id,activity_showroom_name,activity_showroom_tag,activity_showroom_thumb_image,activity_total_pages,activity_feed_thumb_image,is_follow_status,status_commnetEdited,postingStatus)  values%@",query];
        NSLog(@"query=%@",query);
        [self apiCalltodatabase:@"DELETE FROM activityRequestData" andSecondStateMent:query];
    }
}

-(NSMutableArray*)getAllrecordForActivityRequestList{
    [self checkAndCreateDatabase];
    NSMutableArray * employeeArray = [[NSMutableArray alloc] init];
    NSString *statement =  @"SELECT * From activityRequestData";
    [self apiCalltoLocaldatabasetoFetchExistingData:statement withSuccessionBlock:^(sqlite3_stmt *response) {
        [employeeArray addObject:[self createObjectFromrawDataforAllRequestActivityList:response]];
    } andFailureBlock:^(NSError *error) {
        
    }];
    return employeeArray;
}
-(RequestActivityModel *)createObjectFromrawDataforAllRequestActivityList:(sqlite3_stmt *)response{
    RequestActivityModel *list_Param           =  [[RequestActivityModel alloc]init];
    list_Param.activity                        =  [self gettheValue:(char*)sqlite3_column_text(response,1)];
    list_Param.activity_type                   =  [self gettheValue:(char*)sqlite3_column_text(response,2)];
    list_Param.activity_text                   =  [self gettheValue:(char*)sqlite3_column_text(response,3)];
    list_Param.activity_time                   =  [self gettheValue:(char*)sqlite3_column_text(response,4)];
    list_Param.activity_image                  =  [self gettheValue:(char*)sqlite3_column_text(response,5)];
    list_Param.activity_id                     =  [self gettheValue:(char*)sqlite3_column_text(response,6)];
    list_Param.activity_thumb_image            =  [self gettheValue:(char*)sqlite3_column_text(response,7)];
    list_Param.activity_user_id                =  [self gettheValue:(char*)sqlite3_column_text(response,8)];
    list_Param.activity_user_name              =  [self gettheValue:(char*)sqlite3_column_text(response,9)];
    list_Param.activity_showroom_id            =  [self gettheValue:(char*)sqlite3_column_text(response,10)];
    list_Param.activity_showroom_name          =  [self gettheValue:(char*)sqlite3_column_text(response,11)];
    list_Param.activity_showroom_tag           =  [self gettheValue:(char*)sqlite3_column_text(response,12)];
    list_Param.activity_showroom_thumb_image   =  [self gettheValue:(char*)sqlite3_column_text(response,13)];
    list_Param.activity_total_pages            =  [self gettheValue:(char*)sqlite3_column_text(response,14)];
    list_Param.activity_feed_thumb_image       =  [self gettheValue:(char*)sqlite3_column_text(response,15)];
    list_Param.is_follow_status                =  [self gettheValue:(char*)sqlite3_column_text(response,16)];
    list_Param.status_commnetEdited            =  [self gettheValue:(char*)sqlite3_column_text(response,17)];
    list_Param.postingStatus                   =  ([[self gettheValue:(char*)sqlite3_column_text(response,18)] intValue])?YES:NO;
    
    return list_Param;
}
-(void)InsertMyCircleActivityListToDataBase:(NSMutableArray *)feedvalueArray
{
    NSLog(@"asd database : %@",databasePath);
    NSMutableString *query = [[NSMutableString alloc]init];
    //    NSMutableString *queryDetails = [[NSMutableString alloc]init];
    
    for(ActivityModel *value in feedvalueArray){
        NSString *sqlQuery = nil;
        NSString *strFeedDetails = (value.arrFeedDetails.count>0)?[self getFeedDetails:value]:@"";
        NSString *strShowroomDetails = (value.arrShowroomDetails.count>0)?[self getShowRoomDetails:value]:@"";
        NSString *strMintDetails = (value.arrMintListDetails.count>0)?[self getMintDetails:value]:@"";
        NSString *strFollowerListDetails = (value.arrFollowerDetails.count>0)?[self getFollowersDetails:value]:@"";
        
        sqlQuery = [NSString stringWithFormat:@"('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%d','%@','%@','%@','%@','%@','%@'),",value.activity_id,value.activity_type,[value.activity_text stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.activity_time,value.activity_image,value.activity_isVideo,value.activity,value.activity_thumb_image,value.activity_user_id,value.activity_user_name,value.activity_showroom_id,[value.activity_showroom_name stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.activity_showroom_tag,value.activity_showroom_thumb_image,value.activity_total_pages,value.activity_feed_thumb_image,value.first_user_name,value.first_user_id,value.second_user_name,value.second_user_id,value.status_commnetEdited,(value.postingStatus)?1:0,strFeedDetails,strMintDetails,strShowroomDetails,value.feed_id,value.is_follow_status,strFollowerListDetails];
        [query appendString:sqlQuery];
        
        //        queryDetails = [self storeGroupingShowroomAndMintDetails:value queryString:queryDetails];
    }
    
    if (query.length >0)
    {
        [query deleteCharactersInRange:NSMakeRange([query length]-1, 1)];
        query = [NSMutableString stringWithFormat:@"INSERT INTO activityMyCircleData (activity_id,activity_type,activity_text,activity_time,activity_image,activity_isVideo,activity,activity_thumb_image,activity_user_id,activity_user_name,activity_showroom_id,activity_showroom_name,activity_showroom_tag,activity_showroom_thumb_image,activity_total_pages,activity_feed_thumb_image,first_user_name,first_user_id,second_user_name,second_user_id,status_commnetEdited,postingStatus,arrFeedDetails,arrMintListDetails,arrShowroomDetails,activity_feed_id,activity_is_follow_status,arrFollowerDetails)  values%@",query];
        NSLog(@"query=%@",query);
        [self apiCalltodatabase:@"DELETE FROM activityMyCircleData" andSecondStateMent:query];
    }
}

-(NSMutableString *)getFeedDetails:(ActivityModel *)value
{
    NSMutableString *queryDetails = [[NSMutableString alloc]init];
    NSString *sqlQueryDetails = nil;
    if (value.arrFeedDetails.count>0)
    {
        NSString *strFeedId = @"";
        NSString *strFeedThumb = @"";
        NSString *strIsVideo = @"";
        
        for (int i=0; i<[value.arrFeedDetails count]; i++)
        {
            if (i==0)
            {
                strFeedId = [[value.arrFeedDetails objectAtIndex:0] valueForKey:@"feed_id"];
                strFeedThumb = [[value.arrFeedDetails objectAtIndex:0] valueForKey:@"feed_thumb_image"];
                strIsVideo = [[value.arrFeedDetails objectAtIndex:0] valueForKey:@"is_video"];
                sqlQueryDetails = [NSString stringWithFormat:@"%@||%@||%@",strFeedId,strFeedThumb,strIsVideo];
            }
            else
            {
                strFeedId        = [[value.arrFeedDetails objectAtIndex:i] valueForKey:@"feed_id"];
                strFeedThumb     = [[value.arrFeedDetails objectAtIndex:i] valueForKey:@"feed_thumb_image"];
                strIsVideo = [[value.arrFeedDetails objectAtIndex:i] valueForKey:@"is_video"];
                sqlQueryDetails = [sqlQueryDetails stringByAppendingFormat:@"~~~%@",[NSString stringWithFormat:@"%@||%@||%@",strFeedId,strFeedThumb,strIsVideo]];
            }
        }
        
        [queryDetails appendString:sqlQueryDetails];
        
        return queryDetails;
        
    }
    
    return nil;
}

-(NSMutableString *)getFollowersDetails:(ActivityModel *)value
{
    NSMutableString *queryDetails = [[NSMutableString alloc]init];
    NSString *sqlQueryDetails = nil;
//    if (value.arrFollowerDetails.count)
//    {
//        NSString *strFeedId = [[value.arrFollowerDetails objectAtIndex:0] valueForKey:@"user_name"];
//        NSString *strFeedThumb = [[value.arrFollowerDetails objectAtIndex:0] valueForKey:@"user_image"];
//        NSString *strIsVideo = [[value.arrFollowerDetails objectAtIndex:0] valueForKey:@"user_id"];
//        
//        sqlQueryDetails = [NSString stringWithFormat:@"%@||%@||%@",strFeedId,strFeedThumb,strIsVideo];
//        
//        [queryDetails appendString:sqlQueryDetails];
//        return queryDetails;
//    }
    
    
    
    
    
    if (value.arrFollowerDetails.count>0)
    {
        NSString *strUserName = @"";
        NSString *strUserImg = @"";
        NSString *strUserId = @"";
        
        for (int i=0; i<[value.arrFollowerDetails count]; i++)
        {
            if (i==0)
            {
                strUserName = [[value.arrFollowerDetails objectAtIndex:i] valueForKey:@"user_name"];
                strUserImg = [[value.arrFollowerDetails objectAtIndex:i] valueForKey:@"user_image"];
                strUserId = [[value.arrFollowerDetails objectAtIndex:i] valueForKey:@"user_id"];
                sqlQueryDetails = [NSString stringWithFormat:@"%@||%@||%@",strUserName,strUserImg,strUserId];
            }
            else
            {
                strUserName        = [[value.arrFollowerDetails objectAtIndex:i] valueForKey:@"user_image"];
                strUserImg     = [[value.arrFollowerDetails objectAtIndex:i] valueForKey:@"user_image"];
                strUserId = [[value.arrFollowerDetails objectAtIndex:i] valueForKey:@"user_id"];
                
                sqlQueryDetails = [sqlQueryDetails stringByAppendingFormat:@"~~~%@",[NSString stringWithFormat:@"%@||%@||%@",strUserName,strUserImg,strUserId]];
            }
        }
        
        [queryDetails appendString:sqlQueryDetails];
        
        return queryDetails;
    }
    
    
    return nil;
}


-(NSMutableString *)getMintDetails:(ActivityModel *)value
{
    NSMutableString *queryDetails = [[NSMutableString alloc]init];
    NSString *sqlQueryDetails = nil;
    if ([value.activity isEqualToString:@"single_posted_mint_in_showroom"] && value.arrMintListDetails.count)
    {
        NSString *strFeedId = [[value.arrMintListDetails objectAtIndex:0] valueForKey:@"feed_id"];
        NSString *strFeedThumb = [[value.arrMintListDetails objectAtIndex:0] valueForKey:@"feed_thumb_image"];
        NSString *strIsVideo = [[value.arrMintListDetails objectAtIndex:0] valueForKey:@"is_video"];
        
        sqlQueryDetails = [NSString stringWithFormat:@"%@||%@||%@",strFeedId,strFeedThumb,strIsVideo];
        
        [queryDetails appendString:sqlQueryDetails];
        return queryDetails;
    }
    else
    {
        return [self getOtherGroupingMintDetails:value stringQuery:queryDetails];
    }
    
    return nil;
}

-(NSMutableString *)getShowRoomDetails:(ActivityModel *)value
{
    NSMutableString *queryDetails = [[NSMutableString alloc]init];
    NSString *sqlQueryDetails = nil;
    if ([value.activity isEqualToString:@"single_posted_mint_in_showroom"])
    {
        NSString *strShowId = [[value.arrShowroomDetails objectAtIndex:0] valueForKey:@"showroom_id"];
        NSString *strShowName = [[value.arrShowroomDetails objectAtIndex:0] valueForKey:@"showroom_name"];
        NSString *strShowTag = [[value.arrShowroomDetails objectAtIndex:0] valueForKey:@"showroom_tag"];
        NSString *strShowroomThumb = [[value.arrShowroomDetails objectAtIndex:0] valueForKey:@"showroom_thumb_image"];
        
        sqlQueryDetails = [NSString stringWithFormat:@"%@||%@||%@||%@",strShowId,strShowName,strShowTag,strShowroomThumb];
        
        [queryDetails appendString:sqlQueryDetails];
        return queryDetails;
    }
    else
    {
        return [self getOtherGroupingMintDetails:value stringQuery:queryDetails];
    }
    return nil;
}

-(NSMutableString *)getOtherGroupingMintDetails:(ActivityModel *)value stringQuery:(NSMutableString *)queryDetails
{
    NSString *sqlQueryDetails = nil;
    
    if (value.arrShowroomDetails.count>0)
    {
        NSString *strShowId = @"";
        NSString *strShowroomThumb = @"";
        NSString *strShowTag = @"";
        NSString *strShowName = @"";
        
        for (int i=0; i<[value.arrShowroomDetails count]; i++)
        {
            if (i==0)
            {
                strShowId = [[value.arrShowroomDetails objectAtIndex:i] valueForKey:@"showroom_id"];
                strShowName = [[value.arrShowroomDetails objectAtIndex:i] valueForKey:@"showroom_name"];
                strShowTag = [[value.arrShowroomDetails objectAtIndex:i] valueForKey:@"showroom_tag"];
                strShowroomThumb = [[value.arrShowroomDetails objectAtIndex:i] valueForKey:@"showroom_thumb_image"];
                sqlQueryDetails = [NSString stringWithFormat:@"%@||%@||%@||%@",strShowId,strShowName,strShowTag,strShowroomThumb];
            }
            else
            {
                strShowId        = [[value.arrShowroomDetails objectAtIndex:i] valueForKey:@"showroom_id"];
                strShowName     = [[value.arrShowroomDetails objectAtIndex:i] valueForKey:@"showroom_name"];
                strShowTag   = [[value.arrShowroomDetails objectAtIndex:i] valueForKey:@"showroom_tag"];
                strShowroomThumb = [[value.arrShowroomDetails objectAtIndex:i] valueForKey:@"showroom_thumb_image"];
                
                sqlQueryDetails = [sqlQueryDetails stringByAppendingFormat:@"~~~%@",[NSString stringWithFormat:@"%@||%@||%@||%@",strShowId,strShowName,strShowTag,strShowroomThumb]];
            }
        }
        
        [queryDetails appendString:sqlQueryDetails];
        
        return queryDetails;
    }
    if (value.arrMintListDetails.count>0)
    {
        NSString *strFeedId = @"";
        NSString *strFeedThumb = @"";
        NSString *strIsVideo = @"";
        
        for (int i=0; i<[value.arrMintListDetails count]; i++)
        {
            if (i==0)
            {
                strFeedId = [[value.arrMintListDetails objectAtIndex:i] valueForKey:@"feed_id"];
                strFeedThumb = [[value.arrMintListDetails objectAtIndex:i] valueForKey:@"feed_thumb_image"];
                strIsVideo = [[value.arrMintListDetails objectAtIndex:i] valueForKey:@"is_video"];
                sqlQueryDetails = [NSString stringWithFormat:@"%@||%@||%@",strFeedId,strFeedThumb,strIsVideo];
            }
            else
            {
                strFeedId = [[value.arrMintListDetails objectAtIndex:i] valueForKey:@"feed_id"];
                strFeedThumb = [[value.arrMintListDetails objectAtIndex:i] valueForKey:@"feed_thumb_image"];
                strIsVideo = [[value.arrMintListDetails objectAtIndex:i] valueForKey:@"is_video"];
                sqlQueryDetails = [sqlQueryDetails stringByAppendingFormat:@"~~~%@",[NSString stringWithFormat:@"%@||%@||%@",strFeedId,strFeedThumb,strIsVideo]];
            }
        }
        
        [queryDetails appendString:sqlQueryDetails];
        return queryDetails;
    }
    return nil;
}

-(ActivityModel *)createObjectFromrawDataforAllMyCircleActivityList:(sqlite3_stmt *)response
{
    ActivityModel *list_Param                       =  [[ActivityModel alloc]init];
    list_Param.activity_id                          =  [self gettheValue:(char*)sqlite3_column_text(response,1)];
    list_Param.activity_type                        =  [self gettheValue:(char*)sqlite3_column_text(response,2)];
    list_Param.activity_text                        =  [self gettheValue:(char*)sqlite3_column_text(response,3)];
    list_Param.activity_time                        =  [self gettheValue:(char*)sqlite3_column_text(response,4)];
    list_Param.activity_image                       =  [self gettheValue:(char*)sqlite3_column_text(response,5)];
    list_Param.activity_isVideo                     =  [self gettheValue:(char*)sqlite3_column_text(response,6)];
    list_Param.activity                             =  [self gettheValue:(char*)sqlite3_column_text(response,7)];
    list_Param.activity_thumb_image                 =  [self gettheValue:(char*)sqlite3_column_text(response,8)];
    list_Param.activity_user_id                     =  [self gettheValue:(char*)sqlite3_column_text(response,9)];
    list_Param.activity_user_name                   =  [self gettheValue:(char*)sqlite3_column_text(response,10)];
    list_Param.activity_showroom_id                 =  [self gettheValue:(char*)sqlite3_column_text(response,11)];
    list_Param.activity_showroom_name               =  [self gettheValue:(char*)sqlite3_column_text(response,12)];
    list_Param.activity_showroom_tag                =  [self gettheValue:(char*)sqlite3_column_text(response,13)];
    list_Param.activity_showroom_thumb_image        =  [self gettheValue:(char*)sqlite3_column_text(response,14)];
    list_Param.activity_total_pages                 =  [self gettheValue:(char*)sqlite3_column_text(response,15)];
    list_Param.activity_feed_thumb_image            =  [self gettheValue:(char*)sqlite3_column_text(response,16)];
    list_Param.first_user_name                      =  [self gettheValue:(char*)sqlite3_column_text(response,17)];
    list_Param.first_user_id                        =  [self gettheValue:(char*)sqlite3_column_text(response,18)];
    list_Param.second_user_name                     =  [self gettheValue:(char*)sqlite3_column_text(response,19)];
    list_Param.second_user_id                       =  [self gettheValue:(char*)sqlite3_column_text(response,20)];
    list_Param.status_commnetEdited                 =  [self gettheValue:(char*)sqlite3_column_text(response,21)];
    list_Param.postingStatus                        =  ([[self gettheValue:(char*)sqlite3_column_text(response,22)] intValue])?YES:NO;
    NSString *strFeedDetails                        =  [self gettheValue:(char*)sqlite3_column_text(response,23)];
    NSString *strMintDetails                        =  [self gettheValue:(char*)sqlite3_column_text(response,24)];
    NSString *strShowroomDetails                    =  [self gettheValue:(char*)sqlite3_column_text(response,25)];
    list_Param.feed_id                              =  [self gettheValue:(char*)sqlite3_column_text(response,26)];
    list_Param.is_follow_status                     =  [self gettheValue:(char*)sqlite3_column_text(response,27)];
    NSString *strFollowerDetails                    =  [self gettheValue:(char*)sqlite3_column_text(response,28)];
    
    list_Param.arrFeedDetails = [[NSMutableArray alloc]init];
    list_Param.arrMintListDetails = [[NSMutableArray alloc]init];
    list_Param.arrShowroomDetails = [[NSMutableArray alloc]init];
    list_Param.arrFollowerDetails = [[NSMutableArray alloc]init];
    [list_Param.arrFeedDetails addObjectsFromArray:[self makeFeedAndMintArray:strFeedDetails]];
    [list_Param.arrShowroomDetails addObjectsFromArray:[self makeShowroomArray:strShowroomDetails]];
    [list_Param.arrMintListDetails addObjectsFromArray:[self makeFeedAndMintArray:strMintDetails]];
    [list_Param.arrFollowerDetails addObjectsFromArray:[self makeFollowerListArray:strFollowerDetails]];
    return list_Param;
}


-(NSMutableArray *)makeFollowerListArray:(NSString *)strFeedDetails
{
    NSLog(@"%@",strFeedDetails);
    NSMutableArray *arrFeeds = [[NSMutableArray alloc]init];
    
    if ([strFeedDetails length])
    {
        if ([strFeedDetails rangeOfString:@"~~~"].location != NSNotFound)
        {
            NSArray *arrData = [strFeedDetails componentsSeparatedByString:@"~~~"];
            for (int i=0; i<arrData.count;i++)
            {
                NSMutableDictionary *dictMintDetails = [[NSMutableDictionary alloc]init];
                [dictMintDetails setObject:[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:0] forKey:@"user_name"];
                [dictMintDetails setObject:([[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:1] length])?[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:1]:@"" forKey:@"user_image"];
                [dictMintDetails setObject:[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:2] forKey:@"user_id"];
                [arrFeeds addObject:dictMintDetails];
            }
        }
        else if([strFeedDetails rangeOfString:@"||"].location != NSNotFound)
        {
            NSMutableDictionary *dictMintDetails = [[NSMutableDictionary alloc]init];
            [dictMintDetails setObject:[[strFeedDetails componentsSeparatedByString:@"||"] objectAtIndex:0] forKey:@"user_name"];
            [dictMintDetails setObject:([[[strFeedDetails componentsSeparatedByString:@"||"] objectAtIndex:1] length])?[[strFeedDetails componentsSeparatedByString:@"||"] objectAtIndex:1]:@"" forKey:@"user_image"];
            [dictMintDetails setObject:[[strFeedDetails componentsSeparatedByString:@"||"] objectAtIndex:2] forKey:@"user_id"];
            
            [arrFeeds addObject:dictMintDetails];
        }
    }
    return arrFeeds;
}

-(NSMutableArray *)makeFeedAndMintArray:(NSString *)strFeedDetails
{
    NSLog(@"%@",strFeedDetails);
    NSMutableArray *arrFeeds = [[NSMutableArray alloc]init];
    
    if ([strFeedDetails length])
    {
        if ([strFeedDetails rangeOfString:@"~~~"].location != NSNotFound)
        {
            NSArray *arrData = [strFeedDetails componentsSeparatedByString:@"~~~"];
            for (int i=0; i<arrData.count;i++)
            {
           
                NSMutableDictionary *dictMintDetails = [[NSMutableDictionary alloc]init];
                [dictMintDetails setObject:[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:0] forKey:@"feed_id"];
                [dictMintDetails setObject:([[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:1] length])?[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:1]:@"" forKey:@"feed_thumb_image"];
                [dictMintDetails setObject:[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:2] forKey:@"is_video"];
                [arrFeeds addObject:dictMintDetails];
            }
        }
        else if([strFeedDetails rangeOfString:@"||"].location != NSNotFound)
        {
            NSMutableDictionary *dictMintDetails = [[NSMutableDictionary alloc]init];
            [dictMintDetails setObject:[[strFeedDetails componentsSeparatedByString:@"||"] objectAtIndex:0] forKey:@"feed_id"];
            [dictMintDetails setObject:([[[strFeedDetails componentsSeparatedByString:@"||"] objectAtIndex:1] length])?[[strFeedDetails componentsSeparatedByString:@"||"] objectAtIndex:1]:@"" forKey:@"feed_thumb_image"];
            [dictMintDetails setObject:[[strFeedDetails componentsSeparatedByString:@"||"] objectAtIndex:2] forKey:@"is_video"];
            
            [arrFeeds addObject:dictMintDetails];
        }
    }
    return arrFeeds;
}

-(NSMutableArray *)makeShowroomArray:(NSString *)strShowrromDetails
{
    NSLog(@"%@",strShowrromDetails);
    NSMutableArray *arrFeeds = [[NSMutableArray alloc]init];
    
    if ([strShowrromDetails length])
    {
        if ([strShowrromDetails rangeOfString:@"~~~"].location != NSNotFound)
        {
            NSArray *arrData = [strShowrromDetails componentsSeparatedByString:@"~~~"];
            for (int i=0; i<arrData.count;i++)
            {
                NSMutableDictionary *dictMintDetails = [[NSMutableDictionary alloc]init];
                [dictMintDetails setObject:[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:0] forKey:@"showroom_id"];
                [dictMintDetails setObject:[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:1] forKey:@"showroom_name"];
                [dictMintDetails setObject:[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:2] forKey:@"showroom_tag"];
                [dictMintDetails setObject:([[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:3] length])?[[[arrData objectAtIndex:i] componentsSeparatedByString:@"||"] objectAtIndex:3]:@"" forKey:@"showroom_thumb_image"];
                [arrFeeds addObject:dictMintDetails];
            }
        }
        else if([strShowrromDetails rangeOfString:@"||"].location != NSNotFound)
        {
            NSMutableDictionary *dictMintDetails = [[NSMutableDictionary alloc]init];
            [dictMintDetails setObject:[[strShowrromDetails componentsSeparatedByString:@"||"] objectAtIndex:0] forKey:@"showroom_id"];
            [dictMintDetails setObject:[[strShowrromDetails componentsSeparatedByString:@"||"] objectAtIndex:1] forKey:@"showroom_name"];
            [dictMintDetails setObject:[[strShowrromDetails componentsSeparatedByString:@"||"] objectAtIndex:2] forKey:@"showroom_tag"];
            [dictMintDetails setObject:([[[strShowrromDetails componentsSeparatedByString:@"||"] objectAtIndex:3] length])?[[strShowrromDetails componentsSeparatedByString:@"||"] objectAtIndex:3]:@"" forKey:@"showroom_thumb_image"];
            [arrFeeds addObject:dictMintDetails];
        }
    }
    return arrFeeds;
}

-(NSMutableArray*)getAllrecordForActivityMyCircleList
{
    [self checkAndCreateDatabase];
    NSMutableArray * employeeArray = [[NSMutableArray alloc] init];
    NSString *statement = @"SELECT * From activityMyCircleData";
    [self apiCalltoLocaldatabasetoFetchExistingData:statement withSuccessionBlock:^(sqlite3_stmt *response) {
        [employeeArray addObject:[self createObjectFromrawDataforAllMyCircleActivityList:response]];
    } andFailureBlock:^(NSError *error) {
        
    }];
    return employeeArray;
}


-(ActivityModel *)createObjectFromrawDataforAllActivityList:(sqlite3_stmt *)response{
    ActivityModel *list_Param           =[[ActivityModel alloc]init];
    list_Param.activity_id               =  [self gettheValue:(char*)sqlite3_column_text(response,1)];
    list_Param.activity_type             =  [self gettheValue:(char*)sqlite3_column_text(response,2)];
    list_Param.activity_text             =  [self gettheValue:(char*)sqlite3_column_text(response,3)];
    list_Param.activity_time             =  [self gettheValue:(char*)sqlite3_column_text(response,4)];
    list_Param.activity_image     =  [self gettheValue:(char*)sqlite3_column_text(response,5)];
    list_Param.activity_isVideo            =  [self gettheValue:(char*)sqlite3_column_text(response,6)];
    list_Param.activity          =  [self gettheValue:(char*)sqlite3_column_text(response,7)];
    list_Param.activity_thumb_image            =  [self gettheValue:(char*)sqlite3_column_text(response,8)];
    list_Param.activity_user_id =  [self gettheValue:(char*)sqlite3_column_text(response,9)];
    list_Param.activity_user_name     =  [self gettheValue:(char*)sqlite3_column_text(response,10)];
    list_Param.activity_showroom_id        =  [self gettheValue:(char*)sqlite3_column_text(response,11)];
    list_Param.activity_showroom_name             =  [self gettheValue:(char*)sqlite3_column_text(response,12)];
    list_Param.activity_showroom_tag              =  [self gettheValue:(char*)sqlite3_column_text(response,13)];
    list_Param.activity_showroom_thumb_image                  =  [self gettheValue:(char*)sqlite3_column_text(response,14)];
    list_Param.activity_total_pages                =  [self gettheValue:(char*)sqlite3_column_text(response,15)];
    list_Param.activity_feed_thumb_image          =  [self gettheValue:(char*)sqlite3_column_text(response,16)];
    list_Param.first_user_name               =  [self gettheValue:(char*)sqlite3_column_text(response,17)];
    list_Param.first_user_id      =  [self gettheValue:(char*)sqlite3_column_text(response,18)];
    list_Param.second_user_name                 =  [self gettheValue:(char*)sqlite3_column_text(response,19)];
    list_Param.second_user_id                 =  [self gettheValue:(char*)sqlite3_column_text(response,20)];
    list_Param.status_commnetEdited      =  [self gettheValue:(char*)sqlite3_column_text(response,21)];
    list_Param.postingStatus                  =  ([[self gettheValue:(char*)sqlite3_column_text(response,22)] intValue])?YES:NO;
    list_Param.is_userjoined      =  [self gettheValue:(char*)sqlite3_column_text(response,23)];
    list_Param.activity_mint_desc      =  [self gettheValue:(char*)sqlite3_column_text(response,24)];

    
    return list_Param;
}

-(void)InsertActivityListToDataBase:(NSMutableArray *)feedvalueArray   // You Activity
{
    NSLog(@"%@",databasePath);
    NSMutableString *query = [[NSMutableString alloc]init];
    for(ActivityModel *value in feedvalueArray){
        NSString *sqlQuery = nil;
        sqlQuery = [NSString stringWithFormat:@"('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%d','%@','%@'),",value.activity_id,value.activity_type,[value.activity_text stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.activity,value.activity_thumb_image,value.activity_image,value.activity_user_id,value.activity_user_name,value.activity_showroom_id,[value.activity_showroom_name stringByReplacingOccurrencesOfString:@"'" withString:@"''"],value.activity_showroom_tag,value.activity_showroom_thumb_image,value.activity_total_pages,value.activity_feed_thumb_image,value.first_user_name,value.first_user_id,value.second_user_name,value.second_user_id,value.status_commnetEdited,(value.postingStatus)?1:0,value.is_userjoined,value.activity_mint_desc];
        [query appendString:sqlQuery];
    }
    
    if (query.length >0) {
        [query deleteCharactersInRange:NSMakeRange([query length]-1, 1)];
        query = [NSMutableString stringWithFormat:@"INSERT INTO activityYouData (activity_id,activity_type,activity_text,activity,activity_thumb_image,activity_image,activity_user_id,activity_user_name,activity_showroom_id,activity_showroom_name,activity_showroom_tag,activity_showroom_thumb_image,activity_total_pages,activity_feed_thumb_image,first_user_name,first_user_id,second_user_name,second_user_id,status_commnetEdited,postingStatus,is_userjoined,activity_mint_desc)  values%@",query];
        NSLog(@"query=%@",query);
        [self apiCalltodatabase:@"DELETE FROM activityYouData" andSecondStateMent:query];
    }
}



@end



              