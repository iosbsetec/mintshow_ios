//
//  coachingTipModel.h
//  Your Show
//
//  Created by Siba Prasad Hota on 08/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface coachingTipModel : NSObject

@property(nonatomic,assign)  int discoverFirstTime;
@property(nonatomic,assign)  int showroomFirstTime;
@property(nonatomic,assign)  int mintDetailsFirstTime;
@property(nonatomic,assign)  int postmintFirstTime;

@end
