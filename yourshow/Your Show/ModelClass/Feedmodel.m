//
//  Feedmodel.m
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 18/09/14.
//
//

#import "Feedmodel.h"
#import "CommentModel.h"
#import  "HCYoutubeParser.h"



@interface NSString (Replacecrash)

- (NSString *)stringValue;

@end


@implementation NSString (Replacecrash)

- (NSString *)stringValue
{
    return self;
}

@end



@implementation Feedmodel

-(Feedmodel *)initWithShowRoomDict:(NSDictionary*)showRoomDict
{
    self.isMintReminted             =[showRoomDict   valueForKey:@"IsMintReminted"];
    self.achievementType            =[showRoomDict   valueForKey:@"achievement_type"];
    self.feed_alert_action          =[showRoomDict   valueForKey:@"feed_alert_action"];
    self.category_type              =[showRoomDict   valueForKey:@"category_type"];
    self.feed_alert_action          =[showRoomDict   valueForKey:@"feed_alert_action"];
    self.feed_comment_count         =[[showRoomDict  valueForKey:@"feed_comment_count"] stringValue];
    self.feed_created_date          =[[showRoomDict  valueForKey:@"feed_created_date"] stringValue];
    self.feed_id                    =[[showRoomDict  valueForKey:@"feed_id"] stringValue];
    self.feed_inspiration_number    =[[showRoomDict  valueForKey:@"feed_inspiration_number"] stringValue];
    self.feed_like_count            =[[showRoomDict  valueForKey:@"feed_like_count"] stringValue];
    self.feed_mint_type             =[[showRoomDict  valueForKey:@"feed_mint_type"] stringValue];
    self.feed_nomination_number     =[[showRoomDict  valueForKey:@"feed_nomination_number"] stringValue];
    self.feed_posted_location       =[showRoomDict   valueForKey:@"feed_posted_location"];
    self.feed_posted_time           =[showRoomDict   valueForKey:@"feed_posted_time"];
    self.feed_type                  =[showRoomDict   valueForKey:@"feed_type"];
    self.is_user_inspired           =[[showRoomDict  valueForKey:@"is_user_inspired"] stringValue];
    self.is_user_liked              =[[showRoomDict  valueForKey:@"is_user_liked"] stringValue];
    self.is_video_status            =[[showRoomDict  valueForKey:@"is_video_status"] stringValue];
    self.media_id                   =[[showRoomDict  valueForKey:@"media_id"] stringValue];
    self.original_mint_id           =[[showRoomDict  valueForKey:@"original_mint_id"] stringValue];
    self.remint_count               =[NSString stringWithFormat:@"%d",[[showRoomDict   valueForKey:@"remint_count"] intValue]];
    self.showroom_name              =[showRoomDict   valueForKey:@"showroom_name"];
    self.user_id                    =[showRoomDict   valueForKey:@"user_id"];
    self.user_link                  =[showRoomDict   valueForKey:@"user_link"];
    self.feed_thumb_image           =[showRoomDict   valueForKey:@"feed_thumb_image"];
    self.isUserFollowed             =[showRoomDict   valueForKey:@"is_follow_status"];
    self.is_user_nominated          =[showRoomDict   valueForKey:@"is_user_nominated"];
    self.mint_row_id                =[showRoomDict   valueForKey:@"mint_row_id"];
    self.remint_val                 =[showRoomDict   valueForKey:@"remint_val"];
    self.is_user_reminted           =[showRoomDict   valueForKey:@"is_user_reminted"];
    self.mintdetail_url             =[showRoomDict   valueForKey:@"mintdetail_url"];
    self.feed_posted_edited_text    =[showRoomDict   valueForKey:@"feed_posted_edited_text"];
    self.app_connection_url         =[showRoomDict   valueForKey:@"app_connection_url"];
    self.user_name                  =([self.user_id isEqualToString:GETVALUE(CEO_UserId)])?@"You":[showRoomDict   valueForKey:@"user_name"];
    self.is_private_showroom_mint   =[showRoomDict   valueForKey:@"is_private_showroom_mint"];
    
    
    if ([[showRoomDict   valueForKey:@"star_mentioned_showrooms"] count]) {
        self.privateShowroomList  = [NSMutableArray array];
        NSArray *arra = [self getPrivateShowroomList:[showRoomDict   valueForKey:@"star_mentioned_showrooms"]];
        if ([arra count]) {
            self.arraPrivateShowroomList  = [NSMutableArray arrayWithArray:arra];
        }
    }
    
    self.isUserCommented          =    ( [[showRoomDict   valueForKey:@"is_user_commented"] intValue])?@"1":@"0";
    _commentTextHeight = 0.0;
    if ([_feed_comment_count integerValue]) {
        self.CommentsArray              =  [self getComplimineObject:[showRoomDict valueForKey:@"comment"]];
        _commentTextHeight             =  [self getCommentScreenHeight:self.CommentsArray];
    }
    
    
    
    
    
    self.feed_text                      =   [[showRoomDict valueForKey:@"feed_text"] stringByReplacingOccurrencesOfString:@"(null)" withString:@"10987"];
    self.feed_title                     =   [[showRoomDict valueForKey:@"feed_text"] stringByReplacingOccurrencesOfString:@"(null)" withString:@"10987"];
    
    
    self.width                      = ([[showRoomDict valueForKey:@"width_small"] floatValue]>0.0)? [[showRoomDict valueForKey:@"width_small"] floatValue]:70.0;
    self.height                     = ([[showRoomDict valueForKey:@"height_small"]floatValue]>0.0)? [[showRoomDict valueForKey:@"height_small"] floatValue]:70.0;
    self.bigHeight                  = ([[showRoomDict valueForKey:@"height"] floatValue]>0.0)? [[showRoomDict valueForKey:@"height"] floatValue]:70.0;
    self.bigWidth                   = ([[showRoomDict valueForKey:@"width"] floatValue]>0.0)? [[showRoomDict valueForKey:@"width"] floatValue]:70.0;
    
    
    if ([self.achievementType isEqualToString:@"4"]) {
        self.achievementType            =   @"1";
    }
    if (![[showRoomDict valueForKey:@"achievement_type"] isEqualToString:@""] )
    {
        self.feed_thumb_image_small     =  self.feed_thumb_image;
        if(self.height<150) {
            self.height = 150;
            self.width = 150;
            self.bigHeight = 250.0;
            self.bigWidth = 250.0;
        }
    }
    self.feed_thumb_image_small     = ([[showRoomDict valueForKey:@"achievement_type"] isEqualToString:@"0"] )? [showRoomDict valueForKey:@"feed_thumb_image_small"]:@"";
    self.user_thumb_image           =  [showRoomDict valueForKey:@"user_thumb_image"];
    NSString *userName = ([self.user_id isEqualToString:GETVALUE(CEO_UserId)])?@"You":self.user_name;
    NSString *showroom_id = [showRoomDict   valueForKey:@"showroom_id"];
    showroom_id = ([showroom_id length])?showroom_id:@"a1234z";
    
    self.attributedName = [NSString stringWithFormat:@"√[%@](contact:%@)\n %@",userName ,self.user_id, self.feed_posted_time];
    
    
    if([[showRoomDict valueForKey:@"youtube_id"] length]>1)
    {
        self.youtube_id                 =  [showRoomDict valueForKey:@"youtube_id"];
        self.height     = 120.0;
        // self.bigHeight  = 150.0;

    }
    
    
    
    if ([[showRoomDict   valueForKey:@"IsMintReminted"] isEqualToString:@"YES"]) {
        NSString *name_time_str = @"ReMint by ";
        @try {
            name_time_str         =[name_time_str stringByAppendingString:([[[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_userid"] isEqualToString:GETVALUE(CEO_UserId)])?@"You ":[[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_name"]];
            name_time_str                   = [name_time_str stringByAppendingString:@"\n"];
        }
        @catch (NSException *exception) {
            NSLog(@"my exp 1 = %@",exception);
        }
        @finally {
        }
        
        self.remint_count               = [NSString stringWithFormat:@"%d",[[showRoomDict   valueForKey:@"remint_count"] intValue]];
        self.remint_atriputtedString    = name_time_str;
        self.remint_postedTime          = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_posted_time"];
        self.re_username                = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_name"];
        self.remint_message             = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_message"];
        self.re_userimg                 = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_userimg"];
        self.re_user_id                 = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_userid"];
    }
    
    return self;
    
}
-(NSMutableArray *)getComplimineObject : (NSArray *) commentArr
{
    NSMutableArray *arrayComplimint = [NSMutableArray array];
    
    for (NSDictionary *dict in commentArr) {
        
        CommentModel *obj       = [[CommentModel alloc]init];
        obj.achieveCommentText  = [dict valueForKey:@"comment_text"];
        obj.achieveCommentUname = [dict valueForKey:@"user_name"];
        obj.achieveCommentId    = [dict valueForKey:@"comment_id"];
        obj.achieveCommentUserId    = [dict valueForKey:@"user_id"];
        
        obj.commentTextHeight   = [YSSupport getCommentTextHeight:[NSString stringWithFormat:@"%@ %@",obj.achieveCommentUname, obj.achieveCommentText] font:12];
        [arrayComplimint addObject:obj];
        obj = nil;
    }
    return arrayComplimint;
}

-(CGFloat)getCommentScreenHeight : (NSArray *) commentArr {
    CGFloat complimintHeight = 0.0;
    for (CommentModel *obj in commentArr) {
        complimintHeight = complimintHeight +obj.commentTextHeight +5;
    }
    return complimintHeight -5;
    
}



-(Feedmodel *)initWithMintDetail:(NSDictionary*)showRoomDict {
    self.isMintReminted             =   [showRoomDict   valueForKey:@"IsMintReminted"];
    self.achievementType            =   [showRoomDict   valueForKey:@"achievement_type"];
    self.feed_alert_action          =   [showRoomDict   valueForKey:@"feed_alert_action"];
    self.category_type              =   [showRoomDict   valueForKey:@"category_type"];
    self.feed_alert_action          =   [showRoomDict   valueForKey:@"feed_alert_action"];
    self.feed_comment_count         =   [[showRoomDict  valueForKey:@"feed_comment_count"] stringValue];
    self.feed_created_date          =   [[showRoomDict  valueForKey:@"feed_created_date"] stringValue];
    self.feed_id                    =   [[showRoomDict  valueForKey:@"feed_id"] stringValue];
    self.feed_inspiration_number    =   [[showRoomDict  valueForKey:@"feed_inspiration_number"] stringValue];
    self.feed_like_count            =   [[showRoomDict  valueForKey:@"feed_like_count"] stringValue];
    self.feed_mint_type             =   [[showRoomDict  valueForKey:@"feed_mint_type"] stringValue];
    self.feed_nomination_number     =   [[showRoomDict  valueForKey:@"feed_nomination_number"] stringValue];
    self.feed_posted_location       =   [showRoomDict   valueForKey:@"feed_posted_location"];
    self.feed_posted_time           =   [showRoomDict   valueForKey:@"feed_posted_time"];
    self.feed_test_check            =   [showRoomDict   valueForKey:@"feed_test_check"];
    self.feed_type                  =   [showRoomDict   valueForKey:@"feed_type"];
    self.is_user_inspired           =   [[showRoomDict  valueForKey:@"is_user_inspired"] stringValue];
    self.is_user_liked              =   [[showRoomDict  valueForKey:@"is_user_liked"] stringValue];
    self.is_video_status            =   [[showRoomDict  valueForKey:@"is_video_status"] stringValue];
    self.media_id                   =   [[showRoomDict  valueForKey:@"media_id"] stringValue];
    self.original_mint_id           =   [[showRoomDict  valueForKey:@"original_mint_id"] stringValue];
    self.showroom_name              =   [showRoomDict   valueForKey:@"showroom_name"];
    self.remint                     =   [showRoomDict   valueForKey:@"remint"];
    self.user_id                    =   [showRoomDict   valueForKey:@"user_id"];
    self.user_link                  =   [showRoomDict   valueForKey:@"user_link"];
    self.user_name                  =   ([self.user_id isEqualToString:GETVALUE(CEO_UserId)])?@"You":[showRoomDict   valueForKey:@"user_name"];
    self.feed_thumb_image           =   [showRoomDict   valueForKey:@"feed_thumb_image"];
    self.remint_count               =   [NSString stringWithFormat:@"%d",[[showRoomDict   valueForKey:@"remint_count"] intValue]];
    self.isUserFollowed             =   [showRoomDict   valueForKey:@"user_follow_status"];
    self.is_user_nominated          =   [showRoomDict   valueForKey:@"is_user_nominated"];
    self.mint_row_id                =   [showRoomDict   valueForKey:@"mint_row_id"];
    self.isUserCommented            =    ( [[showRoomDict   valueForKey:@"is_user_commented"] intValue])?@"1":@"0";
    self.CommentsArray              =   [NSMutableArray arrayWithArray:[showRoomDict valueForKey:@"comment"]];
    self.remint_val                 =   [showRoomDict   valueForKey:@"remint_val"];
    self.is_user_reminted           =   [showRoomDict   valueForKey:@"is_user_reminted"];
    self.mintdetail_url             =   [showRoomDict   valueForKey:@"mintdetail_url"];
    self.feed_posted_edited_text    =   [showRoomDict   valueForKey:@"feed_posted_edited_text"];
    self.app_connection_url         =   [showRoomDict   valueForKey:@"app_connection_url"];
    self.is_private_showroom_mint   =[showRoomDict   valueForKey:@"is_private_showroom_mint"];

    if ([[showRoomDict   valueForKey:@"star_mentioned_showrooms"] isKindOfClass:[NSArray class]]) {
        NSArray *arra = [self getPrivateShowroomList:[showRoomDict   valueForKey:@"star_mentioned_showrooms"]];
        if ([arra count]) {
            self.arraPrivateShowroomList  = [NSMutableArray arrayWithArray:arra];
        }
    }
    
    _commentTextHeight = 0.0;
    
    if ([_feed_comment_count integerValue]) {
        self.CommentsArray              =  [self getComplimineObject:[showRoomDict valueForKey:@"comment"]];
        _commentTextHeight              =  [self getCommentScreenHeight:self.CommentsArray];
    }
    
    
    self.feed_text                  =   [[showRoomDict valueForKey:@"feed_text"] stringByReplacingOccurrencesOfString:@"(null)" withString:@"10987"];
    self.feed_title                  =   [[showRoomDict valueForKey:@"feed_text"] stringByReplacingOccurrencesOfString:@"(null)" withString:@"10987"];
    self.width                      = ([[showRoomDict valueForKey:@"width_small"] floatValue]>0.0)? [[showRoomDict valueForKey:@"width_small"] floatValue]:70.0;
    self.height                     = ([[showRoomDict valueForKey:@"height_small"]floatValue]>0.0)? [[showRoomDict valueForKey:@"height_small"] floatValue]:70.0;
    self.bigHeight                  = ([[showRoomDict valueForKey:@"height"] floatValue]>0.0)? [[showRoomDict valueForKey:@"height"] floatValue]:70.0;
    self.bigWidth                   = ([[showRoomDict valueForKey:@"width"] floatValue]>0.0)? [[showRoomDict valueForKey:@"width"] floatValue]:70.0;
    
    if ([self.achievementType isEqualToString:@"4"]) {
        self.achievementType            =   @"1";
    }
    
    
    if (![[showRoomDict valueForKey:@"achievement_type"] isEqualToString:@""] )
    {
        self.feed_thumb_image_small     =  self.feed_thumb_image;
        if(self.height<150) {
            self.height = 150;
            self.width = 150;
            self.bigHeight = 250.0;
            self.bigWidth = 250.0;
            
        }
    }
    if ([[showRoomDict valueForKey:@"achievement_type"] isEqualToString:@"0"] ) {
        
        self.feed_thumb_image_small     =  [showRoomDict valueForKey:@"feed_thumb_image_small"];
    }
    
    
    self.user_thumb_image           =  [showRoomDict valueForKey:@"user_thumb_image"];
    NSString *userName = ([self.user_id isEqualToString:GETVALUE(CEO_UserId)])?@"You":self.user_name;
    
    self.attributedName = [NSString stringWithFormat:@"√[%@](contact:%@) posted  %@",userName ,self.user_id, self.feed_posted_time];
    
    if([[showRoomDict valueForKey:@"youtube_id"] length]>1)
    {
        self.youtube_id                 =  [showRoomDict valueForKey:@"youtube_id"];
        self.height     = 120.0;
        //  self.bigHeight  = 150.0;
    }
    
    if([[showRoomDict valueForKey:@"achievement_type"] isEqualToString:@"1"])
    {
        // self.height     = 185.0;
        // self.bigHeight  = 185.0;
    }
    
    // Remint Comment
    if ([[showRoomDict   valueForKey:@"IsMintReminted"] isEqualToString:@"YES"]) {
        NSString *name_time_str = @"ReMint by ";
        
        @try {
            name_time_str         =[name_time_str stringByAppendingString:([[[showRoomDict   valueForKey:@"remintDetails"] valueForKey:@"userId"] isEqualToString:GETVALUE(CEO_UserId)])?@"You ":[[showRoomDict   valueForKey:@"remintDetails"] valueForKey:@"userName"]];
            name_time_str                   = [name_time_str stringByAppendingString:@"\n"];
        }
        @catch (NSException *exception) {
            NSLog(@"my exp 2 = %@",exception);
        }  @finally {  }
        
        
        self.remint_postedTime          = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_posted_time"];
        self.re_username                = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_name"];
        self.remint_message             = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_message"];
        self.re_userimg                 = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_userimg"];
        self.re_user_id                 = [[showRoomDict   valueForKey:@"remint"] valueForKey:@"re_userid"];
    }
    return self;
}

- (NSString *) YoutubeVCDelegateMethod:(NSString *) mediaId
{
    __block NSString *URLString = nil;
    NSURL *youtubeUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",mediaId]];
    [HCYoutubeParser thumbnailForYoutubeURL:youtubeUrl thumbnailSize:YouTubeThumbnailDefaultMedium completeBlock:^(UIImage *image, NSError *error) {
        if (!error) {
            [HCYoutubeParser h264videosWithYoutubeURL:youtubeUrl completeBlock:^(NSDictionary *videoDictionary, NSError *error){
                NSDictionary *qualities = videoDictionary;
                if ([qualities objectForKey:@"small"] != nil) {
                    URLString = [qualities objectForKey:@"small"];
                } else if ([qualities objectForKey:@"live"] != nil) {
                    URLString = [qualities objectForKey:@"live"];
                }  else {
                    [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"This YouTube video is restricted to watch out side you tube." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
                    return;
                }
            }];
        }  else {
            NSLog(@"error %@",error);
        }
    }];
    
    return  URLString;
    
}

-(NSArray *)getPrivateShowroomList : (NSArray *)star_mentioned_showrooms {
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dict in star_mentioned_showrooms) {
        if ([[dict valueForKey:@"is_private"] isEqualToString:@"yes"]) {
            [arr addObject:[NSString stringWithFormat:@"%@-1",[dict valueForKey:@"name"]]];
            [self.privateShowroomList addObject:dict];
        } else {
            [arr addObject:[NSString stringWithFormat:@"%@-0",[dict valueForKey:@"name"]]];
        }
    }
    return arr;
}


@end





