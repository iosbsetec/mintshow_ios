//
//  Feedmodel.h
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 18/09/14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface Feedmodel : NSObject





//NEEDED
@property(nonatomic,retain)     NSMutableArray *CommentsArray;
@property(nonatomic,retain)     NSMutableArray *arraPrivateShowroomList;
@property(nonatomic,retain)     NSMutableArray *privateShowroomList;

@property(nonatomic,assign)     CGFloat         width,height,bigHeight,bigWidth ;
@property(nonatomic,strong)     NSString        *isMintReminted;
@property(nonatomic,strong)     NSString        *achievementType,*feed_created_date,*feed_like_count;
@property(nonatomic,strong)     NSString        *status,*feed_alert_action,*category_type,*feed_comment_count;
@property(nonatomic,strong)     NSString        *userId,*feed_id,*feed_inspiration_number;
@property(nonatomic,strong)     NSString        *feed_category_type,*feed_mint_type,*feed_nomination_number;
@property(nonatomic,strong)     NSString        *feed_posted_location;
@property(nonatomic,strong)     NSString        *feed_posted_time;
@property(nonatomic,retain)     NSString        *next_mint_id,*next_mint_type,*prev_mint_id,*prev_mint_type;
@property(nonatomic,retain)     NSString        *feed_test_check,*feed_text,*feed_thumb_image,*feed_thumb_image_small,*feed_title;
@property(nonatomic,retain)     NSString        *feed_type,*is_user_inspired,*is_user_liked,*is_video_status,*is_user_reminted,*media_id,*is_user_nominated;
@property(nonatomic,retain)     NSString        *original_mint_id,*showroom_name,*isUserCommented;
@property(nonatomic,retain)     NSString        *remint,*user_id,*user_link,*user_name,*user_thumb_image,*youtube_id,*remint_count,*remint_val;
@property(nonatomic,strong)     NSString        *attributedName,*isUserFollowed,*mint_row_id,*mintdetail_url,*is_private_showroom_mint;


@property(nonatomic,strong)     NSString        *remint_atriputtedString;
@property(nonatomic,strong)     NSString        *remint_postedTime;
@property(nonatomic,strong)     NSString        *youtubeParsedUrl;


@property(nonatomic,strong)     NSString        *remint_message,*feed_posted_edited_text;
@property(nonatomic,strong)     NSString        *re_userimg;
@property(nonatomic,strong)     NSString        *re_username;
@property(nonatomic,strong)     NSString        *re_user_id;
@property(nonatomic,strong)     NSString        *app_connection_url;

@property(nonatomic,assign)     CGFloat         commentTextHeight;


-(Feedmodel *)initWithShowRoomDict:(NSDictionary*)showRoomDict;
-(Feedmodel *)initWithMintDetail  :(NSDictionary*)mintDetailsData;

@end


