//
//  MultiMintCollectionView.h
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultimintViewDataSource.h"
#import "PintCollectionViewLayout.h"
#import "PNCollectionCellBackgroundView.h"
#import "Feedmodel.h"

@class MultiMintCollectionView,EntityModel;

@protocol MultiMintCollectionTableDelegate
- (void)hashTagClicked :(EntityModel *)selectedEnitity;
- (void)hashTagFearureSelected :(NSString *)selectedEnitity;
- (void)selectdFeature :(NSString *)selectedEnitity withType :(int) type isPrivateShowroomAccess : (BOOL) access;
- (void)scrollViewDidScroll:(UIScrollView *)scrollViewd;
- (void)LoadNextPagetriggered;



- (void)buttonActionclickedFortype :(int )model forRow : (NSInteger) row feedModelData : (Feedmodel *)data;//buttons are for highfive = 0, complimint = 1 , remint = 2 , mintdetail = 3
- (void)multiMintCollectionViewClicked:(MultiMintCollectionView *)sender AtRow:(NSInteger)row;
@end

@interface MultiMintCollectionView : UICollectionView <UICollectionViewDataSource,UICollectionViewDelegateJSPintLayout>
{
    NSArray *arrimages;
    
}
@property (nonatomic, retain) id <MultiMintCollectionTableDelegate> multiMintdelegate;
@property (nonatomic, retain) id <MultimintViewDataSource> multiMintDataSource;


@end

