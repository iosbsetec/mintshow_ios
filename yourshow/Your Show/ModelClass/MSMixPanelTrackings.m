//
//  MSMixPanelTrackings.m
//  Your Show
//
//  Created by Siba Prasad Hota on 11/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "MSMixPanelTrackings.h"

@implementation MSMixPanelTrackings


+(void)postMint:(NSDictionary *)infoPreoperties
{
    // On Hold now
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Post Mint" properties:infoPreoperties];
}

+(void)trackAppInstallWithData
{
    // On Hold now
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"App Install" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"iOS",@"Attribution Source",[NSDate date],@"Install Date",nil]];
}
+(void)trackAppOpenWithCount:(int)count WithAccessToken:(NSString*)accessToken{
    [Mixpanel sharedInstanceWithToken:MixPanelToken];
    // Later, you can get your instance with
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"App opened"];
}

+(void)trackAppOpen:(NSString *)totalAppOpens lastAppOpenDate:(NSString *)lastOpenDate
{

    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"App Open" properties:[NSDictionary dictionaryWithObjectsAndKeys:totalAppOpens,@"Total App Opens",[NSDate date],@"Last App Open Date",nil]];

}
+(void)trackSignupWithUserDetails:(Userfeed *)usermodel
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Sign Up" properties:[NSDictionary dictionaryWithObjectsAndKeys:[NSDate date],@"Account Created Date",[NSNumber numberWithBool:NO],@"Invited?",@"Email",@"Signup Method",[NSNumber numberWithInteger:6],@"Age Month",[NSNumber numberWithInteger:27],@"Age Year",@"male",@"Gender",usermodel.email,@"Email",usermodel.userName,@"Username",nil]];
}


+(void)trackLoginWithType:(NSString *)loginMethodType
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Log In" properties:[NSDictionary dictionaryWithObjectsAndKeys:loginMethodType,@"Login method",nil]];
    
}
+(void)trackSelectInterestForUserDetails:(NSInteger)selectedFriendsCount listOfPeoples:(NSArray *)list SelectionDuration:(NSInteger)duration scrolled:(BOOL)isScrolled {
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Select Interests" properties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:selectedFriendsCount],@"Total Number Interests Selected",list,@"Interests selected",nil]];
}
+(void)trackInviteFriendForUser:(NSInteger)NumberOfInvites{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Invite Friends" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Email",@"Invite Method",[NSNumber numberWithInteger:NumberOfInvites],@"Number of Invites",nil]];

}


+(void)trackForATaskForUser:(Userfeed *)usermodel withTaskType:(MSTaskType)taskType{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    switch (taskType) {
        case MSTaskTypeOpenedMintshowScreen:
            [mixpanel track:@"Mints list opened"];
            break;
        case MSTaskTypeShowroomOpened:
            [mixpanel track:@"Showroom List opened"];
            break;
        case MSTaskTypeOpenedDiscoverScreen:
            [mixpanel track:@"Discover opened"];
            break;
        case MSTaskTypeOpenedProfileScreen:
            [mixpanel track:@"Profile opened"];
            break;
        case MSTaskTypeOpenedActivityScreen:
            [mixpanel track:@"Activity opened"];
            break;
        case MSTaskTypeOpenedParticularShowroom:
            [mixpanel track:@"Showroom Details opened"];
            break;
        case MSTaskTypeOpenedMintDetails:
            [mixpanel track:@"Mint details opened"];
            break;
        case MSTaskTypeOpenedAddMint:
            [mixpanel track:@"Addmint opened"];
            break;
        case MSTaskTypeOpenedAddShowroom:
            [mixpanel track:@"Add Showroom opened"];
            break;
        case MSTaskTypeHifivedMint:
            [mixpanel track:@"User hifived Mint"];
            break;
        case MSTaskTypeComplimintedMint:
            [mixpanel track:@"User Compliminted Mint"];
            break;
        case MSTaskTypeFirstMintPosted:
            [mixpanel track:@"User Posted First Mint"];
            break;
        case MSTaskTypeSelectInterest:
            [mixpanel track:@"Select Interests"];
            break;
            
        case MSTaskTypeInviteFriend:
            [mixpanel track:@"MSTaskTypeInviteFriend"];
            break;
            
        default:
            break;
    }
}


@end
