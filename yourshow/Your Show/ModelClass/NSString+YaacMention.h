//
//  NSString+YaacMention.h
//  SearchUser
//
//  Created by Siba Prasad Hota  on 6/1/15.
//  Copyright (c) 2015 Lemonpeak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EntityModel.h"

@interface NSString (YaacMention)

- (NSString *)StringByChangingComplimintStringToNormalString;
- (NSString *)StringByChangingNormalNameToServername:(NSString *)idString entityType:(EntityType)entype;
- (NSString *)StringByConvertingPartialStringsToFullString:(NSMutableArray*)entities;
- (NSString *)StringByConvertingToHUmanReadable:(NSMutableArray *)entities;

@end
