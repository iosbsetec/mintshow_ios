//
//  BaseFeedModel.m
//  Your Show
//
//  Created by Siba Prasad Hota on 22/02/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "BaseFeedModel.h"
#import "CommentModel.h"
#import "YSSupport.h"
#import "HCYoutubeParser.h"

@implementation BaseFeedModel


-(BaseFeedModel*)initWithMintDetails:(NSDictionary *)mintDictioNary{
    if (!self) {
        self = [super init];
    }
    self.category_type              =[mintDictioNary   valueForKey:@"category_type"];
    self.feed_comment_count         =[[mintDictioNary  valueForKey:@"feed_comment_count"] stringValue];
    self.feed_created_date          =[[mintDictioNary  valueForKey:@"feed_created_date"] stringValue];
    self.feed_id                    =[[mintDictioNary  valueForKey:@"feed_id"] stringValue];
    self.feed_inspiration_count     =[[mintDictioNary  valueForKey:@"feed_inspiration_count"] stringValue];
    self.feed_like_count            =[[mintDictioNary  valueForKey:@"feed_like_count"] stringValue];
    self.feed_nomination_count      =[[mintDictioNary  valueForKey:@"feed_nomination_number"] stringValue];
    self.feed_posted_location       =[mintDictioNary   valueForKey:@"feed_posted_location"];
    self.feed_posted_time           =[[mintDictioNary   valueForKey:@"feed_posted_time"] stringByReplacingOccurrencesOfString:@"M" withString:@"mo"];
    
    
    self.minttype_exists            =[[mintDictioNary  valueForKey:@"minttype_exists"] intValue];
    self.streak_days                =[[mintDictioNary  valueForKey:@"streak_days"] stringValue];
    self.is_mint_streak             = [mintDictioNary  valueForKey:@"is_mint_streak"];
    
    
    
    self.is_user_inspired           =[[mintDictioNary  valueForKey:@"is_user_inspired"] intValue];
    self.is_user_liked              =[[mintDictioNary  valueForKey:@"is_user_liked"] intValue];
    self.is_video_status            =[[mintDictioNary  valueForKey:@"is_video_status"] intValue];
    self.is_user_followed           =[[mintDictioNary   valueForKey:@"is_follow_status"] intValue];
    self.is_user_nominated          =[[mintDictioNary   valueForKey:@"is_user_nominated"] intValue];
    self.is_user_reminted           =[[mintDictioNary   valueForKey:@"is_user_reminted"] intValue];
    self.is_private_showroom_mint   =([[mintDictioNary   valueForKey:@"is_private_showroom_mint"] isEqualToString:@"No"])?0:1;
    self.media_id                   =[[mintDictioNary  valueForKey:@"media_id"] stringValue];
    self.original_mint_id           =[[mintDictioNary  valueForKey:@"original_mint_id"] stringValue];
    self.remint_count               =[NSString stringWithFormat:@"%d",[[mintDictioNary   valueForKey:@"remint_count"] intValue]];
    self.showroom_name              =[mintDictioNary   valueForKey:@"showroom_name"];
    self.feed_user_id               =[mintDictioNary   valueForKey:@"user_id"];
    self.feed_user_link             =[mintDictioNary   valueForKey:@"user_link"];
    self.feed_user_name             = ([self.feed_user_id isEqualToString:GETVALUE(CEO_UserId)])?@"You":[mintDictioNary   valueForKey:@"user_name"];
    self.feedUsernameWithTime       = [NSString stringWithFormat:@"√[%@](contact:%@)\n %@",self.feed_user_name ,self.feed_user_id, self.feed_posted_time];
    self.feed_thumb_image           =[mintDictioNary   valueForKey:@"feed_thumb_image"];
    self.feed_thumb_image_small     =[mintDictioNary   valueForKey:@"feed_thumb_image_small"];
    self.feed_user_thumb_image      =[mintDictioNary   valueForKey:@"user_thumb_image"];
    self.mint_row_id                =[mintDictioNary   valueForKey:@"mint_row_id"];
    self.remint_val                 =[mintDictioNary   valueForKey:@"remint_val"];
    self.mintdetail_url             =[mintDictioNary   valueForKey:@"mintdetail_url"];
    self.feed_posted_edited_text    =[mintDictioNary   valueForKey:@"feed_posted_edited_text"];
    self.app_connection_url         =[mintDictioNary   valueForKey:@"app_connection_url"];

    self.feed_thumb_height = ([[mintDictioNary valueForKey:@"height"] floatValue]>0.0)?[[mintDictioNary valueForKey:@"height"] floatValue]:250;
    self.feed_thumb_Width  = ([[mintDictioNary valueForKey:@"width"] floatValue]>0.0)?[[mintDictioNary valueForKey:@"width"] floatValue]:350;
       
    
    

    if ([[mintDictioNary valueForKey:@"achievement_type"] isEqualToString:@""]) {
        self.mintType = MSMintTypeText;
    }else if ([[mintDictioNary valueForKey:@"achievement_type"] isEqualToString:@"0"]) {
        self.mintType = MSMintTypeImage;
    }else if ([[mintDictioNary valueForKey:@"achievement_type"] isEqualToString:@"1"]) {
        self.mintType = MSMintTypeVideo;
        if([[mintDictioNary valueForKey:@"youtube_id"] length]>1){
        self.youtube_id                 =  [mintDictioNary valueForKey:@"youtube_id"];
        self.mintType = MSMintTypeYouTube;
        }else  if([self.app_connection_url length]>1){
            self.mintType = MSMintTypeInsthaGram;
        }
    }else if ([[mintDictioNary valueForKey:@"achievement_type"] isEqualToString:@"4"]) {
        self.mintType = MSMintTypeInsthaGram;
    }
    
    
    if ([mintDictioNary valueForKey:@"signature_shlink"] != 0) {
        self.feed_title                  =   [[mintDictioNary valueForKey:@"signature_shlink"] stringByReplacingOccurrencesOfString:@"(null)" withString:@"10987"];

    }
     self.feed_description                  =   [[mintDictioNary valueForKey:@"feed_text"] stringByReplacingOccurrencesOfString:@"(null)" withString:@"10987"];
    
    if ([[mintDictioNary   valueForKey:@"star_mentioned_showrooms"] count]) {
        self.privateShowroomListArray  = [NSMutableArray array];
        NSArray *arra = [self getPrivateShowroomList:[mintDictioNary   valueForKey:@"star_mentioned_showrooms"]];
        if ([arra count]) {
            self.privateShowroomListArray  = [NSMutableArray arrayWithArray:arra];
        }
    }
    self.commentsArray = [NSMutableArray array];
    self.is_user_commented          =    ( [[mintDictioNary   valueForKey:@"is_user_commented"] intValue])?1:0;
    self.feed_comment_height = 0.0;
    if ([_feed_comment_count intValue] && [[mintDictioNary valueForKey:@"comment"] count]) {
        self.commentsArray              =  [self getComplimineObject:[mintDictioNary valueForKey:@"comment"]];
        self.feed_comment_height        =  [self getCommentScreenHeight:self.commentsArray];
        
        // need to add
        if ([[[[mintDictioNary valueForKey:@"comment"] valueForKey:@"json_cmt"] objectAtIndex:0] count])
        {
            self.commentedUserArray  = [NSMutableArray array];
            NSArray *arra = [self getCompliMintedUserObject:[[[mintDictioNary valueForKey:@"comment"] valueForKey:@"json_cmt"] objectAtIndex:0]];
            if ([arra count])
            {
                self.commentedUserArray  = [NSMutableArray arrayWithArray:arra];
            }
        }
        
        
    }
    
    self.IsMintReminted =([[mintDictioNary   valueForKey:@"IsMintReminted"] isEqualToString:@"YES"])?1:0;

    if ([[mintDictioNary valueForKey:@"at_mentioned_users"] count]) {
        self.atMentionedArray  = [NSMutableArray array];
        self.atMentionedArray  =  [mintDictioNary valueForKey:@"at_mentioned_users"];
    }
    if (self.IsMintReminted) {
        NSString *name_time_str = @"ReMint by ";
        @try {
            
            name_time_str         = [name_time_str stringByAppendingString:([[[mintDictioNary   valueForKey:@"remint"] valueForKey:@"re_userid"] isEqualToString:GETVALUE(CEO_UserId)])?@"You ":[[mintDictioNary   valueForKey:@"remint"] valueForKey:@"re_name"]];
            name_time_str         = [name_time_str stringByAppendingString:@"\n"];
            if ([[[mintDictioNary valueForKey:@"remint"] valueForKey:@"re_mention_users"] count]>0) {
                self.remintAtMentionedArray  = [NSMutableArray array];
                self.remintAtMentionedArray  =  [[mintDictioNary valueForKey:@"remint"] valueForKey:@"re_mention_users"];
            }
            
        }
        @catch (NSException *exception) {
            
            NSLog(@"my exp 1 = %@",exception);
        }
        @finally {
            
        }
        
        self.remint_count               = [[mintDictioNary   valueForKey:@"remint_count"] stringValue];
        self.remint_AtributtedString    =  name_time_str;
        self.remint_postedTime          = [[mintDictioNary   valueForKey:@"remint"] valueForKey:@"re_posted_time"];
        self.remint_username            = [[mintDictioNary   valueForKey:@"remint"] valueForKey:@"re_name"];
        self.remint_message             = [[mintDictioNary   valueForKey:@"remint"] valueForKey:@"re_message"];
        self.remint_userimg             = [[mintDictioNary   valueForKey:@"remint"] valueForKey:@"re_userimg"];
        self.remint_user_id             = [[mintDictioNary   valueForKey:@"remint"] valueForKey:@"re_userid"];
    }

    return self;
}

-(NSMutableArray *)getComplimineObject : (NSArray *) commentArr
{
    NSMutableArray *arrayComplimint = [NSMutableArray array];
    
    for (NSDictionary *dict in commentArr) {
        
        CommentModel *obj       = [[CommentModel alloc]init];
        obj.achieveCommentText  = [dict valueForKey:@"comment_text"];
        obj.achieveCommentUname = [dict valueForKey:@"user_name"];
        obj.achieveCommentId    = [dict valueForKey:@"comment_id"];
        obj.achieveCommentUserId    = [dict valueForKey:@"user_id"];
        obj.commentTextHeight   = [YSSupport getCommentTextHeight:[NSString stringWithFormat:@"%@ %@",obj.achieveCommentUname, obj.achieveCommentText] font:12];     // Here Changed
        
        
        [arrayComplimint addObject:obj];
        obj = nil;
        
    }
    
    
    return arrayComplimint;
}
//-(CGFloat)getCommentScreenHeight : (NSArray *) commentArr {
//    CGFloat complimintHeight = 0.0;
//    for (CommentModel *obj in commentArr) {
//        complimintHeight = complimintHeight +obj.commentTextHeight +5;
//    }
//    return complimintHeight -5;
//}
-(CGFloat)getCommentScreenHeight : (NSArray *) commentArr {
    CGFloat complimintHeight = 0.0;
    //    for (CommentModel *obj in commentArr) {
    //        complimintHeight = complimintHeight +obj.commentTextHeight +5;
    //    }
    int i=0;
    for (CommentModel *obj in commentArr) {
        i++;
        complimintHeight = complimintHeight +obj.commentTextHeight +5;
        
        if (i==3) {
            break;
        }
    }
    return complimintHeight -5;
    
}

- (NSString *) YoutubeVCDelegateMethod:(NSString *) mediaId
{
    __block NSString *URLString = nil;
    NSURL *youtubeUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",mediaId]];
    [HCYoutubeParser thumbnailForYoutubeURL:youtubeUrl thumbnailSize:YouTubeThumbnailDefaultMedium completeBlock:^(UIImage *image, NSError *error) {
        if (!error) {
            [HCYoutubeParser h264videosWithYoutubeURL:youtubeUrl completeBlock:^(NSDictionary *videoDictionary, NSError *error){
                NSDictionary *qualities = videoDictionary;
                if ([qualities objectForKey:@"small"] != nil) {
                    URLString = [qualities objectForKey:@"small"];
                } else if ([qualities objectForKey:@"live"] != nil) {
                    URLString = [qualities objectForKey:@"live"];
                }  else {
                    [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"This YouTube video is restricted to watch out side you tube." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
                    return;
                }
            }];
        }  else {
            NSLog(@"error %@",error);
        }
    }];
    
    return  URLString;
    
}
-(NSArray *)getPrivateShowroomList : (NSArray *)star_mentioned_showrooms {
    NSMutableArray *arr = [NSMutableArray array];
    self.privateShowroomListArray = [NSMutableArray new];
    for (NSDictionary *dict in star_mentioned_showrooms) {
        if ([[dict valueForKey:@"is_private"] isEqualToString:@"yes"]) {
            [arr addObject:[NSString stringWithFormat:@"%@-1",[dict valueForKey:@"name"]]];
            [self.privateShowroomListArray addObject:dict];
        } else {
            [arr addObject:[NSString stringWithFormat:@"%@-0",[dict valueForKey:@"name"]]];
        }
    }
    return arr;
}


-(NSMutableArray *)getCompliMintedUserObject : (NSArray *) commentedArr
{
    NSMutableArray *arrayComplimint = [NSMutableArray array];
    
    for (NSDictionary *dict in commentedArr) {
        
        CommentedUserModel *obj       = [[CommentedUserModel alloc]init];
        obj.achieveCommentedUserId = [dict valueForKey:@"id"];
        obj.achieveCommentedUname    = [dict valueForKey:@"name"];
        [arrayComplimint addObject:obj];
        obj = nil;
    }
    return arrayComplimint;
}


@end
