//
//  MSMixPanelTrackings.h
//  Your Show
//
//  Created by Siba Prasad Hota on 11/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Userfeed.h"


typedef NS_ENUM(NSInteger, MSTaskType) {
    MSTaskTypeOpenedMintshowScreen = 0,
    MSTaskTypeShowroomOpened,
    MSTaskTypeOpenedDiscoverScreen,
    MSTaskTypeOpenedProfileScreen,
    MSTaskTypeOpenedActivityScreen,
    MSTaskTypeOpenedParticularShowroom,
    MSTaskTypeOpenedMintDetails,
    MSTaskTypeOpenedAddMint,
    MSTaskTypeOpenedAddShowroom,
    MSTaskTypeHifivedMint,
    MSTaskTypeComplimintedMint,
    MSTaskTypeFirstMintPosted,
    MSTaskTypeSelectInterest,
    MSTaskTypeInviteFriend,
    MSTaskTypeSignUp

};

@interface MSMixPanelTrackings : NSObject

+(void)trackAppInstallWithData;
+(void)trackAppOpenWithCount:(int)count WithAccessToken:(NSString*)accessToken;
+(void)trackSignupWithUserDetails:(Userfeed *)usermodel;
+(void)trackForATaskForUser:(Userfeed *)usermodel withTaskType:(MSTaskType)taskType;
+(void)trackInviteFriendForUser:(NSInteger)NumberOfInvites;
+(void)trackLoginWithType:(NSString *)loginMethodType;
+(void)trackAppOpen:(NSString *)totalAppOpens lastAppOpenDate:(NSString *)lastOpenDate;
+(void)trackSelectInterestForUserDetails:(NSInteger)selectedFriendsCount listOfPeoples:(NSArray *)list SelectionDuration:(NSInteger)duration scrolled:(BOOL)isScrolled;
+(void)postMint:(NSDictionary *)infoPreoperties;
@end
