//
//  BaseFeedModel.h
//  Your Show
//
//  Created by Siba Prasad Hota on 22/02/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CommentedUserModel.h"

typedef enum {
    MSMintTypeText,
    MSMintTypeImage,
    MSMintTypeVideo,
    MSMintTypeYouTube,
    MSMintTypeInsthaGram
} MintType;

/*
 0=text
 1=Image
 2= video
 3=youtube
 4= instagram
 */


@interface BaseFeedModel : NSObject

@property(nonatomic,assign) MintType mintType;

@property(nonatomic,assign)  int IsMintReminted;
@property(nonatomic,assign)  int is_follow_status;
@property(nonatomic,assign)  int is_mint_nominated;
@property(nonatomic,assign)  int is_mint_shared;
@property(nonatomic,assign)  int is_user_commented;
@property(nonatomic,assign)  int is_user_liked;
@property(nonatomic,assign)  int is_user_nominated;
@property(nonatomic,assign)  int is_user_reminted;
@property(nonatomic,assign)  int is_video_status;
@property(nonatomic,assign)  int is_user_inspired;
@property(nonatomic,assign)  int is_user_followed;
@property(nonatomic,assign)  int mintTupeNumber,minttype_exists;
@property(nonatomic,assign)  int is_private_showroom_mint;

@property(nonatomic,strong)  NSMutableArray *remintAtMentionedArray;

@property(nonatomic,strong)  NSString *is_mint_streak,*streak_days;


@property(nonatomic,strong)  NSString *app_connection_url;
@property(nonatomic,strong)  NSString *youtube_id;
@property(nonatomic,strong)  NSString *category_id;
@property(nonatomic,strong)  NSString *category_type;
@property(nonatomic,strong)  NSString *feed_comment_count;
@property(nonatomic,strong)  NSString *feed_created_date;
@property(nonatomic,strong)  NSString *feed_id;
@property(nonatomic,strong)  NSString *feed_like_count;
@property(nonatomic,strong)  NSString *feed_nomination_count;
@property(nonatomic,strong)  NSString *feed_inspiration_count;
@property(nonatomic,strong)  NSString *feed_posted_edited_text;
@property(nonatomic,strong)  NSString *feed_posted_location;
@property(nonatomic,strong)  NSString *feed_posted_time;
@property(nonatomic,strong)  NSString *feed_description;
@property(nonatomic,strong)  NSString *feed_title;
@property(nonatomic,strong)  NSString *feed_longitude;
@property(nonatomic,strong)  NSString *feed_latitude;

// Image url and sizes respectively.
@property(nonatomic,strong)  NSString *feed_thumb_image;
@property(nonatomic,strong)  NSString *feed_thumb_image_small;

@property(nonatomic,assign)  CGFloat  feed_thumb_Width;
@property(nonatomic,assign)  CGFloat  feed_thumb_height;
@property(nonatomic,assign)  CGFloat  feed_comment_height;

@property(nonatomic,strong)  NSString *media_id;
@property(nonatomic,strong)  NSString *media_type;
@property(nonatomic,strong)  NSString *mint_row_id;
@property(nonatomic,strong)  NSString *mintdetail_url;
@property(nonatomic,strong)  NSString *original_mint_id;

@property(nonatomic,strong)  NSString *remint_count;
@property(nonatomic,strong)  NSString *remint_val;
@property(nonatomic,strong)  NSString *remint_AtributtedString;
@property(nonatomic,strong)  NSString *remint_postedTime;
@property(nonatomic,strong)  NSString *remint_message;
@property(nonatomic,strong)  NSString *remint_userimg;
@property(nonatomic,strong)  NSString *remint_username;
@property(nonatomic,strong)  NSString *remint_user_id;

@property(nonatomic,strong)  NSString *showroom_count;
@property(nonatomic,strong)  NSString *showroom_id;
@property(nonatomic,strong)  NSString *showroom_name;

@property(nonatomic,strong)  NSString *feed_user_id;
@property(nonatomic,strong)  NSString *feed_user_name;
@property(nonatomic,strong)  NSString *attributedName;
@property(nonatomic,strong)  NSString *feed_user_link;
@property(nonatomic,strong)  NSString *feed_user_thumb_image;
@property(nonatomic,strong)  NSString *feed_user_thumb_rotate;
@property(nonatomic,strong)  NSString *feedUsernameWithTime;


@property(nonatomic,strong)  NSMutableArray *commentsArray;
@property(nonatomic,strong)  NSMutableArray *starMentionedArray;
@property(nonatomic,strong)  NSMutableArray *atMentionedArray;
@property(nonatomic,strong)  NSMutableArray *privateShowroomListArray;

@property(nonatomic,strong)  NSMutableArray *commentedUserArray;

-(BaseFeedModel*)initWithMintDetails:(NSDictionary *)mintDictioNary;

@end



/*
  
 
 CREATE TABLE "feedData" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "userid" VARCHAR NOT NULL  DEFAULT NA, "email" VARCHAR DEFAULT NA, "password" VARCHAR DEFAULT NA, "userName" VARCHAR DEFAULT NA, "first_Name" VARCHAR, "last_Name" VARCHAR, "gender" VARCHAR DEFAULT m, "login_Status" INTEGER DEFAULT 0, "date_of_birth" VARCHAR DEFAULT NA, "device_Token" VARCHAR DEFAULT NA, "unique_Device_ID" VARCHAR DEFAULT NA, "phone_Number" VARCHAR, "Profile_image_url" VARCHAR DEFAULT NA, "Profile_thumb_url" VARCHAR, "user_type" VARCHAR, "location" VARCHAR, "active" VARCHAR, "confirmed" VARCHAR, "website" VARCHAR, "about" VARCHAR, "age" VARCHAR, "Referral_id" VARCHAR, "twitter_id" VARCHAR, "date_created" VARCHAR, "AccessToken" VARCHAR, "powerMember" VARCHAR, "followingCount" VARCHAR, "followerCount" VARCHAR, "stage" VARCHAR, "total_achievemints" VARCHAR,  "total_encouragemints" VARCHAR, "existing_userSocial" VARCHAR, "version" VARCHAR, "discover_all_page_first_time_visit" VARCHAR, "showroom_page_first_time_visit" VARCHAR)
 
 
 comment =             (
 {
 "comment_id" = 1114;
 "comment_text" = as;
 "comment_time" = "3h ago";
 "is_edited" = 0;
 "json_cmt" =                     (
 );
 "user_id" = 10;
 "user_link" = "http://mintshowapp.com/boRXd1454125921";
 "user_name" = "Mani Kandan";
 "user_thumb_image" = "http://4ddfbe9a7f0ae00c0fe7-fa77fb2cde31107cbcfc98a033e304c0.r96.cf1.rackcdn.com/avatar/121.jpg";
 "user_thumb_rotate" = "";
 }
 );

 "at_mentioned_users" =             (
 );
 
 "star_mentioned_showrooms" =             (
 {
 "access_key" = yes;
 id = 308;
 "is_private" = yes;
 name = cameraprivate;
 "privacy_type" = "";
 "showroom_exist" = yes;
 }
 );

 
 IsMintReminted = NO;

 }
 
 
 
 
 
 */