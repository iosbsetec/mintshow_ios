
#import <Foundation/Foundation.h>

@interface Userfeed : NSObject

@property(nonatomic,assign)  NSInteger login_Status;

@property(nonatomic,strong)  NSString *status;
@property(nonatomic,strong)  NSString *status_Msg;
@property(nonatomic,strong)  NSString *status_code;
@property(nonatomic,strong)  NSString *User_deviceToken;
@property(nonatomic,strong)  NSString *User_DeviceId;

@property(nonatomic,strong)  NSString *User_id;
@property(nonatomic,strong)  NSString *email;
@property(nonatomic,strong)  NSString *password;
@property(nonatomic,strong)  NSString *userName;
@property(nonatomic,strong)  NSString *fullname;
@property(nonatomic,strong)  NSString *first_name;
@property(nonatomic,strong)  NSString *last_name;
@property(nonatomic,strong)  NSString *user_type;
@property(nonatomic,strong)  NSString *phone_Number;
@property(nonatomic,strong)  NSString *gender;
@property(nonatomic,strong)  NSString *avatarImgurl;
@property(nonatomic,strong)  NSString *location;
@property(nonatomic,strong)  NSString *active;
@property(nonatomic,strong)  NSString *confirmed;
@property(nonatomic,strong)  NSString *website;
@property(nonatomic,strong)  NSString *date_of_birth;
@property(nonatomic,strong)  NSString *about;
@property(nonatomic,strong)  NSString *age;
@property(nonatomic,strong)  NSString *Referral_id;
@property(nonatomic,strong)  NSString *twitter_id;
@property(nonatomic,strong)  NSString *date_created;
@property(nonatomic,strong)  NSString *AccessToken;
@property(nonatomic,strong)  NSString *powerMember;
@property(nonatomic,strong)  NSString *followingCount;
@property(nonatomic,strong)  NSString *followerCount;

@property(nonatomic,strong)  NSString *assistantid;
@property(nonatomic,strong)  NSString *assistantname;
@property(nonatomic,strong)  NSString *user_image_url;
@property(nonatomic,strong)  NSString *stage;
@property(nonatomic,strong)  NSString *total_achievemints;
@property(nonatomic,strong)  NSString *total_encouragemints;
@property(nonatomic,strong)  NSString *success_message,*addmint_culture_first_time_visit;

@property(nonatomic,strong)  NSString *is_Account_deactivated;
@property(nonatomic,strong)  NSString *notify_val;
@property(nonatomic,assign)  NSInteger *totalActivityCount;

@property(nonatomic,strong)  NSString *notify_text;
@property(nonatomic,strong)  NSString *existing_userSocial,*version,*discover_all_page_first_time_visit,*showroom_page_first_time_visit,*is_already_posted_mint,*mint_detail_page_first_time_visit,*is_joined_showrooms;


@end













