//
//  ViewController.h
//  OnBoardDemo
//
//  Created by Bsetec on 14/01/16.
//  Copyright (c) 2016 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopUpList.h"

@interface SignUpOnBoard : UIViewController<PopUpListDelegate,UITextFieldDelegate>

-(IBAction)btnBackTapped:(id)sender;
-(IBAction)btnMonthTapped:(id)sender;
-(IBAction)btnYearTapped:(id)sender;
-(IBAction)btnGenderTapped:(id)sender;
-(IBAction)btnDoneTapped:(id)sender;

@end

