//
//  Your Show
//
//  Created by Siba Prasad Hota on 05/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum
{
    YSLoginTypeFacebook,
    YSLoginTypeTwitter,
    YSLoginTypeEmail,
    YSLoginTypeGooglePlus,
}
YSLoginType;

@interface MSLandingPage : UIViewController



@end
