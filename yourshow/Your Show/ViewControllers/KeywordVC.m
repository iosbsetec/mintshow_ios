//
//  KeywordVC.m
//  ZFTokenFieldDemo
//
//  Created by BseTec on 12/16/15.
//  Copyright © 2015 Amornchai Kanokpullwad. All rights reserved.
//

#import "KeywordVC.h"
#import "ZFTokenField.h"
#import "POPupTableView.h"
#import "TSPopoverController.h"
#import "FeatureModel.h"

@interface KeywordVC () <ZFTokenFieldDataSource, ZFTokenFieldDelegate,POPUpTableViewDataSource,POPUpTableDelegate>
{
    NSMutableArray *dataArray;
    TSPopoverController *popoverController;
    POPupTableView *apopupTable;
    NSMutableArray *arrayFriendList,*userDataArray;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet ZFTokenField *tokenField;
@property (nonatomic, strong) NSMutableArray *tokens;
@end

@implementation KeywordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createTableAndPopupView];
    [self createTokenField];
    [self getAllKeyWords];
    
    if ([_arrayKeywords count])
    {
        [_tokens addObjectsFromArray:_arrayKeywords];
        [self.tokenField reloadData];
    }

}

- (IBAction)saveButtonPressed:(id)sender
{
    if([self.keyDelegate respondsToSelector:@selector(updateAddShowRoomKeyword:)])
        [self.keyDelegate updateAddShowRoomKeyword:self.tokens];
    self.tokens = nil;
    [self.tokenField endEditing:YES];
    [self.navigationController popViewControllerAnimated:NO];
  
}
- (IBAction)cancelButtonPressed:(id)sender
{
    self.tokens = nil;
    [self.tokenField endEditing:YES];
    [self.navigationController popViewControllerAnimated:NO];
    
}

-(void)createTableAndPopupView
{
    apopupTable = [[POPupTableView alloc]initWithFrame:CGRectMake(0,0, self.view.frame.size.width - 20, 120)];
    apopupTable.isFromKeyword = YES;
    apopupTable.popupDataSource=self;
    apopupTable.popupDelegate = self;
    
    popoverController = [[TSPopoverController alloc] initWithView:apopupTable];
    popoverController.cornerRadius = 5;
    popoverController.popoverBaseColor = [UIColor lightGrayColor];
    popoverController.popoverGradient= NO;
    
}
-(void)cancel
{
    _tokens = nil;
    dataArray = nil;
    arrayFriendList = nil;
    apopupTable = nil;
    
    [self.navigationController popViewControllerAnimated:NO];

}
-(void)createTableviewAndShowPopup
{
    [popoverController showPopoverWithRect:CGRectMake(0,60,320,100)];
    [apopupTable reloadData];
}

- (NSInteger)rowsForBubbleTable:(POPupTableView *)tableView
{
    tableView.separatorStyle= (dataArray.count>0)?UITableViewCellSeparatorStyleSingleLine:UITableViewCellSeparatorStyleNone;
    return dataArray.count;
}

- (FeatureModel*)bubbleTableView:(POPupTableView *)tableView dataForRow:(NSInteger)row;
{
    if (dataArray.count>0) {
        return [dataArray objectAtIndex:row];
    }
    return nil;
}

-(void)createTokenField
{
    self.tokens = [NSMutableArray array];
    self.tokenField.dataSource            = self;
    self.tokenField.delegate    = self;
    self.tokenField.textField.placeholder = @"Enter Keywords";
    [self.tokenField.textField becomeFirstResponder];
    [self.tokenField reloadData];

    [self.scrollView setContentOffset:CGPointZero animated:YES];
    [self.scrollView setContentSize:CGSizeMake(self.tokenField.frame.size.width, 500)];
}

- (void)updateScrollHeight:(int)height
{
    [self.scrollView setContentSize:CGSizeMake(self.tokenField.frame.size.width, 500)];
    
    CGRect rectToken = self.tokenField.frame;
    rectToken.size.height = self.scrollView.contentSize.height;
    [self.tokenField setFrame:rectToken];
}



- (void)tokenDeleteButtonPressed:(UIButton *)tokenButton
{
    NSUInteger index = [self.tokenField indexOfTokenView:tokenButton.superview];
    if (index != NSNotFound) {
        [self.tokens removeObjectAtIndex:index];
        [self.tokenField reloadData];
    }
}

#pragma mark - ZFTokenField DataSource

- (CGFloat)lineHeightForTokenInField:(ZFTokenField *)tokenField
{
    return 25;
}

- (NSUInteger)numberOfTokenInField:(ZFTokenField *)tokenField
{
    return self.tokens.count;
}

- (UIView *)tokenField:(ZFTokenField *)tokenField viewForTokenAtIndex:(NSUInteger)index
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TokenView" owner:nil options:nil];
    UIView *view = nibContents[0];
    UILabel *label = (UILabel *)[view viewWithTag:2];
    UIButton *button = (UIButton *)[view viewWithTag:3];
    
    [button addTarget:self action:@selector(tokenDeleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    label.text = self.tokens[index];
    CGSize size = [label sizeThatFits:CGSizeMake(1000, 25)];
    view.frame = CGRectMake(0, 2, size.width + 30, 25);
    view.layer.cornerRadius = 4;
    [view setClipsToBounds:YES];
    return view;
}

#pragma mark - ZFTokenField Delegate

- (CGFloat)tokenMarginInTokenInField:(ZFTokenField *)tokenField
{
    return 5;
}

- (void)tokenField:(ZFTokenField *)tokenField didReturnWithText:(NSString *)text
{
    if (text.length>0) {
        [self.tokens addObject:text];
        [tokenField reloadData];
    }
    
    [popoverController dismissPopoverAnimatd:YES];
    
}

- (void)tokenField:(ZFTokenField *)tokenField didRemoveTokenAtIndex:(NSUInteger)index
{
    [self.tokens removeObjectAtIndex:index];
}

- (BOOL)tokenFieldShouldEndEditing:(ZFTokenField *)textField
{
    return YES;
}

- (void)tokenField:(ZFTokenField *)tokenField didTextChanged:(NSString *)text
{
    if (text.length>0)
        [self searchKeywords:text];
    else if (text.length==0)
    {
        [popoverController dismissPopoverAnimatd:YES];
    }
}

- (void)getAllKeyWords
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@",GETVALUE(CEO_AccessToken)] ;
    [self CallAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"customshowroom/get_keywords",submitData]]];
}

-(void)CallAPIUrl:(NSURL *)myUrl
{
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];

    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dataArray =[self UserlistDataWithDictioNary:[self dictionaryFromResponseData:data]];
     }];
}
-(void)searchKeywords : (NSString *) txtField
{
    
    if([[txtField stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]>0){
        
        NSString *searchString = txtField;
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"name BEGINSWITH[cd] %@", searchString];
        //        NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", searchString];
        NSArray*  resultArray = [[arrayFriendList filteredArrayUsingPredicate:filter] mutableCopy];
        
        
        if (resultArray.count == 0) {
            NSLog(@"No data From Search");
        }
        else
        {
            [dataArray removeAllObjects];
            [dataArray addObjectsFromArray:resultArray];
            [self createTableviewAndShowPopup];
        }
        
    } else {
        if ([arrayFriendList count] != [dataArray count]) {
            [dataArray removeAllObjects];
            [dataArray addObjectsFromArray:arrayFriendList];
            [self createTableviewAndShowPopup];
        }
    }
    
}
-(NSMutableArray*)UserlistDataWithDictioNary:(NSDictionary*)dictionary
{
    userDataArray   = [NSMutableArray new];
    arrayFriendList = [NSMutableArray new];

    for (NSDictionary *userData in [dictionary valueForKey:@"msg"])
    {
        FeatureModel *feature = [[FeatureModel alloc]init];
        feature.name          = [userData valueForKey:@"text"];
        feature.ct_id         = [userData valueForKey:@"id"];
        feature.type          = @"";
        [userDataArray addObject:feature];
        [arrayFriendList addObject:feature];
    }
    return userDataArray;
}

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}

-(void)deleteExistingRecordWithModelData
{
    NSMutableArray *anArray = [dataArray mutableCopy];
    
    for (NSString *c_id in dataArray)
    {
        for (FeatureModel *fmodel in anArray )
        {
            if ([c_id isEqualToString:fmodel.ct_id])
            {
                [dataArray removeObject:fmodel];
            }
        }
    }
}
- (void) PopupTableSelectedIndex :(NSInteger)selectedRowNo
{
    FeatureModel *fdata = [dataArray objectAtIndex:selectedRowNo];
    [self.tokens addObject:fdata.name];
    [_tokenField reloadData];
    [popoverController dismissPopoverAnimatd:YES];
}



@end

