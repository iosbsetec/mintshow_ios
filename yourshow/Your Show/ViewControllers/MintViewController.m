//
//  MintViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota on 16/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MintViewController.h"
#import "AppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "CellMyCircle.h"
#import "PhotoCell.h"
#import "MyShowViewController.h"
#import "JitenCollectionView.h"
#import "NearLocation.h"
#import "LocatioDetails.h"
#import "FilterListTable.h"
@interface MintViewController ()<JitenCollectionTableDelegate,JitenCollectionViewDataSource,NearLocationTableDelegate,FilterListTableDelegate>
{

    __weak IBOutlet UITableView *tblMint;

    __weak IBOutlet UILabel *lblCtegoryName;
    __weak IBOutlet UIImageView *imgVLatest;
    NearLocation *nearLocationTableView;
    FilterListTable *filterTableView;

    NSMutableArray *allVideos;
    NSMutableDictionary *dic;
    IBOutlet UICollectionView *ceoconnectCollectionVw;
    IBOutlet UIView *viewUploadMint;
    
    __weak IBOutlet UILabel *lblNameLocation;
    __weak IBOutlet UIImageView *imgVUpload;
    IBOutlet UIView *viewPhoto;
    IBOutlet UIView *viewCamera;
}
@property (weak, nonatomic) IBOutlet JitenCollectionView *discovershowroomcollectionview;

- (IBAction)photoTapped:(UIButton *)sender;
- (IBAction)crossTapped:(UIButton *)sender;
- (IBAction)locationTapped:(UIButton *)sender;

@end

@implementation MintViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /* uncomment this block to use subclassed cells */
    [ceoconnectCollectionVw registerClass:[PhotoCell class] forCellWithReuseIdentifier:@"photoCell"];
    /* end of subclass-based cells block */

    // Configure layout
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(90, 90)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [ceoconnectCollectionVw setCollectionViewLayout:flowLayout];
    
    
    self.discovershowroomcollectionview.CollectionDataSource = self;
    self.discovershowroomcollectionview.collectiondelegate = self;
    self.discovershowroomcollectionview.Addshowroom = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   }


-(void)getCurrentPhotoFromPhotoLibrary
{
    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
    allVideos = [[NSMutableArray alloc] init];
    NSLog(@"count %@",[ALAssetsFilter allPhotos]);
   
    
    [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop)
     {
         if (group)
         {
             [group setAssetsFilter:[ALAssetsFilter allPhotos]];
             [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop)
              {
                  if (asset)
                  {
                      dic = [[NSMutableDictionary alloc] init];
                      ALAssetRepresentation *defaultRepresentation = [asset defaultRepresentation];
                      UIImage *latestPhoto= [UIImage imageWithCGImage:[defaultRepresentation fullScreenImage]];

                     [allVideos addObject:latestPhoto];

                  }
                  else
                  {
                  }
              }
              
              ];
             
             NSLog(@"allVideos of accets %@",[allVideos description]);
             [ceoconnectCollectionVw reloadData];
             
         }
     }
     failureBlock:^(NSError *error)
         {
             NSLog(@"error enumerating AssetLibrary groups %@\n", error);
         }];
    
    
}
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize withImage : (UIImage *)image {
    
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        
        if (widthFactor < heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        } else if (widthFactor > heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    
    // this is actually the interesting part:
    
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage ;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [allVideos count]+1;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Setup cell identifier
    static NSString *cellIdentifier = @"photoCell";
    
    /*  Uncomment this block to use nib-based cells */
    // UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    // UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
    // [titleLabel setText:cellData];
    /* end of nib-based cell block */
    
    /* Uncomment this block to use subclass-based cells */
    PhotoCell *cell = (PhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell.btnUser addTarget:self action:@selector(galleryImageTapped:) forControlEvents:UIControlEventTouchUpInside];

    if (indexPath.row == 0) {
        cell.imgUser.image = [UIImage imageNamed:@"nature"];
        [cell.btnUser setImage:[UIImage imageNamed:@"CameraPhoto"] forState:UIControlStateNormal];
        [cell.btnUser addTarget:self action:@selector(cameraTapped) forControlEvents:UIControlEventTouchUpInside];

    }
    else
    {
    cell.imgUser.image = [allVideos objectAtIndex:indexPath.row-1];
        cell.btnUser.tag =indexPath.row-1;
        [cell.btnUser addTarget:self action:@selector(galleryImageTapped:) forControlEvents:UIControlEventTouchUpInside];

    }
    
       // Return the cell
    cell.contentView.backgroundColor = [UIColor darkGrayColor];
    cell.layer.cornerRadius = 6.0;
    return cell;
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(6,4,4,6);
    
    
}

- (IBAction)postMint:(UIButton *)sender {
    [self crossTapped:nil];
       AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegateApp.ysTabBarController setSelectedIndex:0];
   // [delegateApp tabBarController:delegateApp.ysTabBarController didSelectViewController:[[delegateApp.ysTabBarController viewControllers] objectAtIndex:0]];
    
}
-(IBAction)galleryImageTapped: (UIButton *) sender{

    imgVUpload.image = [allVideos objectAtIndex:sender.tag];
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [viewUploadMint setFrame:[UIScreen mainScreen].bounds];
    [delegateApp.window addSubview:viewUploadMint];


}
- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row
{
    return  nil;
}
- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView
{
    return 6;
}

-(IBAction)cameraTapped
{

    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [viewCamera setFrame:[UIScreen mainScreen].bounds];
    [delegateApp.window addSubview:viewCamera];

}
- (IBAction)photoTapped:(UIButton *)sender {
    [self getCurrentPhotoFromPhotoLibrary];
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [viewPhoto setFrame:[UIScreen mainScreen].bounds];
    [delegateApp.window addSubview:viewPhoto];
    
}

- (IBAction)crossTapped:(UIButton *)sender {
    if ([viewUploadMint superview]) {
        [viewUploadMint removeFromSuperview];
    }
    if ([viewCamera superview]) {
        [viewCamera removeFromSuperview];
    }
    [viewPhoto removeFromSuperview];
}

- (void) NearLocationTableDelegateMethod: (LocatioDetails *) sender stringWithCustomText :(NSString *) customLocation;
{
    if ([customLocation length]) {
        lblNameLocation.text = customLocation;
    }
    else
    lblNameLocation.text = sender.yaacPlaceName;
    
    [nearLocationTableView hideDropDown];
}


-(IBAction)locationTapped:(UIButton *)sender
{
    nearLocationTableView = [[NearLocation alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds];
    nearLocationTableView.delegate = self;
}


- (void) FilterListTableDelegateMethod: (NSString *) textData selectedId :(NSString *) iddee;
{
    lblCtegoryName.text = textData;
    [filterTableView hideDropDown];
}


-(IBAction)cateGoryTapped
{
    filterTableView = [[FilterListTable alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds withTitle:@"Filters" isCateGoryView:NO];
    filterTableView.delegate = self;
}

@end
