//
//  MyShowViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota on 16/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MyShowViewController.h"
#import "CommentsViewController.h"
#import "FilterListTable.h"
#import "UIViewController+CWPopup.h"
#import "UploadStatusView.h"
#import "EntityModel.h"
#import "MSRemintVC.h"
#import "MSMintDetails.h"
#import "PintCollecionCell.h"
#import "MSProfileVC.h"
#import "SocialVideoHelper.h"
#import "MintShowroomViewController.h"
#import "UIViewController+CWPopup.h"
#import "CarbonKit.h"
#import "YSCollectionView.h"
#import "BaseFeedModel.h"
#import "UserAccessSession.h"
#import "UsersListView.h"
#import "MSSelectInterests.h"
#import  <MediaPlayer/MediaPlayer.h>
#import "MSGlobalSearchVCViewController.h"
#import "CommentModel.h"
#import "MSPSignatureShowroomVC.h"

@interface MyShowViewController()<YSCollectionViewDataSource,YSCollectionViewDelegate,FilterListTableDelegate,uploadDelegate,MSMintDetailsDelegate,RemintDelegate,MSGlobalSearchDelegate,UsersListDelegate>{
    
           IBOutlet UIButton *btnFilter;
           IBOutlet UITextField *txtFldSearch;
    __weak IBOutlet UIButton *btnMint;

    __weak IBOutlet UIView  *viewFilter;
    __weak IBOutlet UIView  *viewSegment;
           IBOutlet UIView  *alertDeleteShowroom;
           IBOutlet UILabel *lblMint;
           IBOutlet UILabel *lblShowrrom;

    NSMutableArray          *allMintsdata;
    NSMutableArray          *arrayFilterSearchParameters;

    BOOL                    isLoading,isPullToreFresh;
    BOOL                    isfilterSearchApply;
    BOOL                    isDataFinishedInServer;
    
    int                     pagenumber;
    int                     pagenumberShowroom;
    NSInteger               selectedIndex;

    
    
    
    FilterListTable         *filterTableView;
    CarbonSwipeRefresh      *refreshCarbon;

    MPMoviePlayerController *movieController;
}

@property (retain,nonatomic)  NSMutableArray *allMintsdata;

@property (strong, nonatomic) IBOutlet YSCollectionView *aView;

- (IBAction)closeShowroomDeletePopup:(UIButton *)sender;
- (IBAction)logOutClicked:(id)sender;


@end


@implementation MyShowViewController

@synthesize allMintsdata;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [MSMixPanelTrackings trackForATaskForUser:nil withTaskType:MSTaskTypeOpenedMintshowScreen];
    [self initializer];
    
    btnFilter.layer.cornerRadius = 5.;
    viewFilter.layer.cornerRadius = 5.;
    isPullToreFresh = NO;
    
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:self.aView withYPos:100];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playbDidFinish)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playbDidFinish)
                                                 name:MPMoviePlayerDidExitFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadMintsSelectThreeInterestComplete)
                                                 name:@"SELECTINTERESTCOMPLTE" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(remintedSucssesfully)
                                                 name:@"REMINTCOMPLETE" object:nil];
  
}

- (void) FilterListTableDelegateMethodCategoryId:(NSString *)text sort_by_option:(NSInteger )sort_by_option sort_by_feature:(NSInteger)sort_by_feature
{
     pagenumber =1;
    [filterTableView hideDropDown];
    NSLog(@"category_id %@ \n sort_by_option %d \n sort_by_feature= %d",text,(int)sort_by_option,(int)sort_by_feature);
    arrayFilterSearchParameters = nil;
    arrayFilterSearchParameters = [NSMutableArray arrayWithObjects:text,[NSNumber numberWithInteger:sort_by_option],[NSNumber numberWithInteger:sort_by_feature],nil];
    [self filterSearchApplyForMints:YES];
    isfilterSearchApply = YES;
    
}
-(void)filterSearchApplyForMints:(BOOL)deletePreviousData
{
    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&sort_by_option=%d&category_id=%@&sort_by_feature=%d&page_number=%d",GETVALUE(CEO_AccessToken),[[arrayFilterSearchParameters objectAtIndex:1] intValue],[arrayFilterSearchParameters objectAtIndex:0],[[arrayFilterSearchParameters objectAtIndex:2] intValue],pagenumber] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"result is %@",str);
    
    [YSAPICalls filterSearch:submitData ForSuccessionBlock:^(id newResponse) {
        Generalmodel *gmodel = newResponse;
        if ([gmodel.status_code integerValue]){
            if([gmodel.tempArray count]>0){
                isDataFinishedInServer = NO;
                if (pagenumber  ==  1) {
                    SETVALUE(gmodel.pull_request_time, CEO_PULLREQUESTTIME);
                    SYNCHRONISE;
                }
                if (deletePreviousData) {
                    [ self.allMintsdata removeAllObjects];
                }
                [self.allMintsdata addObjectsFromArray: gmodel.tempArray];
                [self batchreloadCollectionView];
                SPHSingletonClass *objectShareClass = [SPHSingletonClass sharedSingletonClass];
                objectShareClass.arrayMyShowMintData = nil;
                [objectShareClass setArrayMyShowMintData:self.allMintsdata];
            }
            else{
                isDataFinishedInServer = YES;
            }
            
        } }andFailureBlock:^(NSError *error) {
        }];
}


#pragma mARK - nO cONTENT aVAILABLE

-(void)showNoContentPopUp
{
    UIView *rootView = [[[NSBundle mainBundle] loadNibNamed:@"NoContent" owner:self options:nil] objectAtIndex:0];
    UIView *containerView = [[[NSBundle mainBundle] loadNibNamed:@"NoContent" owner:self options:nil] lastObject];
    [rootView addSubview:containerView];
    rootView.tag = 2233;
    
    //For Return Myshow button
    UIButton *btnRetMyShow =(UIButton *)[containerView viewWithTag:35];
    [btnRetMyShow addTarget:self action:@selector(onReturnMyShow) forControlEvents:UIControlEventTouchUpInside];
    
    //For Close button
    UIButton *btnClose =(UIButton *)[containerView viewWithTag:45];
    [btnClose addTarget:self action:@selector(onClosePopup) forControlEvents:UIControlEventTouchUpInside];
    
    [containerView viewWithTag:25].layer.cornerRadius = 5.0;
    [containerView viewWithTag:25].layer.borderColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.6].CGColor;
    [containerView viewWithTag:25].layer.borderWidth = 1.0;
    [self.view addSubview:rootView];
}

-(void)serverCallForTheSearch :(NSString *)textToSearch GlobalsearchApi : (BOOL)GlobalSearch
{
    [self gotoProfileView:textToSearch];
    
}
-(void)onClosePopup{
    [[self.view viewWithTag:2233]removeFromSuperview];
    NSLog(@"Close Tapped");
}

-(void)onReturnMyShow{
    [[self.view viewWithTag:2233]removeFromSuperview];
    NSLog(@"Close Tapped");
}




- (void)refresh:(id)sender {
    pagenumber = 1;
    isPullToreFresh = YES;
    [self getMintsMyshow:pagenumber isDelete:YES pull_request_time:NO];

}


- (void)endRefreshing {
    [refreshCarbon endRefreshing];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [MSSelectInterests hideSelectInterestView:self.view animated:YES];
    


}
-(void)viewDidAppear:(BOOL)animated{
    
     AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if ([GETVALUE(CEO_ISJOINEDSHOWROOM) isEqualToString:@"no"]) {
        CGRect totalframe      =  [[UIScreen mainScreen] bounds];
        totalframe.origin.y    = 90;
        totalframe.size.height = totalframe.size.height -140;
        [MSSelectInterests showSelectInterestViewTo:self.view withViewcontainerFrame:totalframe];
    }
    else if (delegateApp.isAnyShowroomJoinUnjoin)
    {
        delegateApp.isAnyShowroomJoinUnjoin = NO;
        pagenumber = 1;
        [self getMintsMyshow:pagenumber isDelete:YES pull_request_time:NO];
    }
    [super viewDidAppear:animated];
}
-(void)loadMintsSelectThreeInterestComplete
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SELECTINTERESTCOMPLTE" object:nil];
    pagenumber = 1;
    [self getMintsMyshow:pagenumber isDelete:YES pull_request_time:NO];

}
-(void)initializer{
    // MULTI MINT COLLECTION VIEW
    self.aView.backgroundColor                  = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.view.backgroundColor                   = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    
    self.aView.showsHorizontalScrollIndicator   = NO;
    self.aView.scrollEnabled                    = YES;
    self.aView.ysDataSource                     = self;
    self.aView.ysDelegate                       = self;
    self.aView.bounces                          = YES;
    
    self.allMintsdata = [[NSMutableArray alloc]init];
    
    
    isLoading = NO;
    isDataFinishedInServer = NO;

    pagenumber = 1;
    self.allMintsdata = [UserAccessSession getAllrecordForMintshowFeedList];
    if(self.allMintsdata.count){
        SPHSingletonClass *objectShareClass = [SPHSingletonClass sharedSingletonClass];
        objectShareClass.arrayMyShowMintData = nil;
        [objectShareClass setArrayMyShowMintData:self.allMintsdata];
    }
    
    [self getMintsMyshow:pagenumber isDelete:(self.allMintsdata.count)?YES:NO pull_request_time:NO];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissPopup) name:@"CLosePopupNow" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(mintAddedSuccesFully:) name:KMINTADDED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(commentUpdatedSuccesFully:) name:KCOMMNETUPDATED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showMintdetailsWithSingleRecord) name:KSHOWSINGLEMINTDETAILS object:nil];
}




-(void)gotoOldShowroom:(NSString *)selectedEnitity{
   // AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
   // delegateApp.lbl.hidden   = YES;
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [selectedEnitity substringFromIndex:1];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}


-(void)showMintdetailsWithSingleRecord{
    if ([GETVALUE(CEO_DEEPLINKINGINFO) integerValue]) {
        MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
        mintDetailsVC.hidesBottomBarWhenPushed = YES;
        mintDetailsVC.isToshowSingleRecord = YES;
        mintDetailsVC.isMintOwner = NO;
        mintDetailsVC.mint_id =     GETVALUE(CEO_DEEPLINKINGINFO);
        mintDetailsVC.delegate = self;
        [self.navigationController pushViewController:mintDetailsVC animated:YES];
    }  else {
      
//        AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
//        delegateApp.lbl.hidden   = YES;
        
        MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
        vcShowRoom.sendersName = GETVALUE(CEO_DEEPLINKINGUSERNAME);
        vcShowRoom.selectedShowroom = GETVALUE(CEO_DEEPLINKINGINFO);

        vcShowRoom.isFromDeepLinking = YES;
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vcShowRoom];
        nc.navigationBar.hidden = YES;
        [self presentViewController:nc animated:YES completion:^{
        }];
    }
}


-(void)gotoHashTagView:(NSString *)selectedEnitity{
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.delegate = self;
    mintDetailsVC.isToshowSingleRecord = NO;
    mintDetailsVC.isFromTrendingTags = YES;
    mintDetailsVC.isMintOwner = NO;
    mintDetailsVC.navigationHeaderText = selectedEnitity;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}
- (IBAction)searchTapped:(UIButton *)sender {
    MSGlobalSearchVCViewController  *mintDetailsVC =  [[MSGlobalSearchVCViewController alloc] initWithNibName:@"MSGlobalSearchVCViewController" bundle:nil];
    mintDetailsVC.delegate = self;
    [self.navigationController pushViewController:mintDetailsVC animated:NO];
}



-(void)gotoProfileView:(NSString *)selectedEnitity{
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    // prof.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:prof animated:YES];
}



- (void)selectedFeatureType:(int )featureType withString:(NSString *)selectedEnitity isPrivateShowroomAccess:(BOOL)access{
    switch (featureType) {
        case 0:
            [self gotoHashTagView:selectedEnitity];
            break;
        case 1:
        {
            if(access) {
                showAlert(@"It's a private showroom", nil, @"Ok", nil);
            }
            else
               [self gotoOldShowroom:selectedEnitity];
        }
            break;
        case 2:case 44:
            [self gotoProfileView:selectedEnitity];
            break;
            
        case 88:
            [self gotoSignatureShowroom:selectedEnitity];
            break;
            
        default:
            break;
    }

}

-(void)gotoSignatureShowroom:(NSString *)mintType
{

    MSPSignatureShowroomVC *vcShowRoom = [[MSPSignatureShowroomVC alloc]initWithNibName:@"MSPSignatureShowroomVC" bundle:nil];
    vcShowRoom.isFromDiscoverShowroom = NO;
    vcShowRoom.selectedShowroom = mintType;
    [self.navigationController pushViewController:vcShowRoom animated:YES];


}
- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell{
    BaseFeedModel *existingFeedModeldata =              [ self.allMintsdata objectAtIndex:row];
  
    if(buttonType == YSButtonTypeComplimint)  {
        [self commentScreen:existingFeedModeldata withSelectedMintTag:row];
    }else if (buttonType == YSButtonTypeHifiveCount)  {
        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeHighFive numberofPeoples:[existingFeedModeldata.feed_like_count intValue] feedId:existingFeedModeldata.feed_id delegate:self];
    }
    else if (buttonType == YSButtonTypeInspire)  {
        [self remintClicked:existingFeedModeldata];
    }
    else if (buttonType == YSButtonTypedetails){
        [self gotoMintDetailsScreen:existingFeedModeldata forRow:row];
    } else if (buttonType == YSButtonTypeUserImage) {
        [self showPopupWithModeldata:data withrow:row isRemint:NO];
    } else if (buttonType == YSButtonTypeRemintUserImage) {
        [self showPopupWithModeldata:data withrow:row isRemint:YES];
    }else if (buttonType == YSButtonTypeHifive){
        [ self.allMintsdata replaceObjectAtIndex:row withObject:data];
    }
}
-(void)selectedUserIdFromHighfiveList:(NSString *)selectdUserId
{
    
    [self gotoProfileView:selectdUserId];
    
}

//
//
//
//-(void)commentUpdatedSuccesFully:(NSNotification *) notification
//{
//    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
//    if ([aDelegate.selectedIndex integerValue]>=self.allMintsdata.count) {
//        return;
//    }
//    CommentModel *commentObject = (CommentModel *) [[notification valueForKey:@"userInfo"] valueForKey:@"CommentData"];
//    BaseFeedModel *model = [self.allMintsdata objectAtIndex:[aDelegate.selectedIndex integerValue]];
//    if ([[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:DELETECOMMENT]) {
//        if ([model.feed_comment_count integerValue] == 1){
//            [model setFeed_comment_count:@"0"];
//            [model setIs_user_commented:0];
//        }else{
//            [model setFeed_comment_count:[NSString stringWithFormat:@"%d", [model.feed_comment_count intValue]-1]];
//            [model setIs_user_commented:1];
//        }
//    }
//    
//    else {
//        if (![[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:EDITCOMMENT])  {
//            [model setFeed_comment_count:[NSString stringWithFormat:@"%d", [model.feed_comment_count intValue]+1]];
//            [model setIs_user_commented:1];
//            if (model.commentsArray.count){
//                [model.commentsArray insertObject:commentObject atIndex:0];
//            }else{
//                model.commentsArray = [[NSMutableArray alloc] init];
//                [model.commentsArray addObject:commentObject];
//            }
//            
//        }
//    }
//    NSInteger commentCount = (model.commentsArray.count>3)?3:model.commentsArray.count;
//    float CommentHeight = 0.0;
//    for (int i = 0; i<commentCount; i++)
//    {
//        NSString *strText = [NSString stringWithFormat:@"%@ %@",[[model.commentsArray objectAtIndex:i] achieveCommentUname],[[model.commentsArray objectAtIndex:i] achieveCommentText]];
//        
//        CommentHeight += [YSSupport getCommentTextHeight:strText font:12.0];
//    }
//    model.feed_comment_height       = CommentHeight+5;
//    
//    
//    [ self.allMintsdata replaceObjectAtIndex:[aDelegate.selectedIndex integerValue] withObject:model];
//    [_aView performBatchUpdates:^{
//        [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:[aDelegate.selectedIndex integerValue] inSection:0]]];
//    } completion:^(BOOL finished) {
//    }];
//}

-(void)commentUpdatedSuccesFully:(NSNotification *) notification
{
    if (self.allMintsdata.count == 0) {
        return;
    }
    CommentModel *commentObject = (CommentModel *) [[notification valueForKey:@"userInfo"] valueForKey:@"CommentData"];
    
    //    NSSet *NScolors = [NSSet setWithArray:[self.allMintsdata valueForKey:@"feed_id"]];
    //    NSArray *colors = [NScolors allObjects];
    
    for (int i = 0; i < self.allMintsdata.count; i++) {
        BaseFeedModel *model = [self.allMintsdata objectAtIndex:i];
        
        
        if ([model.feed_id isEqualToString:[[notification valueForKey:@"userInfo"] valueForKey:@"FEEDID"]]) {
            
            if ([[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:DELETECOMMENT]) {
                if ([model.feed_comment_count integerValue] == 1){
                    [model setFeed_comment_count:@"0"];
                    [model setIs_user_commented:0];
                }else{
                    [model setFeed_comment_count:[NSString stringWithFormat:@"%d", [model.feed_comment_count intValue]-1]];
                    [model setIs_user_commented:1];
                }
                
                for (int i=0; i<[model.commentsArray count]; i++) {
                    if ([[[model.commentsArray objectAtIndex:i] achieveCommentId]isEqualToString:commentObject.achieveCommentId])  {
                        [model.commentsArray removeObjectAtIndex:i];
                        break;
                    }
                }
            }
            else {
                if (![[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:EDITCOMMENT])  {
                    [model setFeed_comment_count:[NSString stringWithFormat:@"%d", [model.feed_comment_count intValue]+1]];
                    [model setIs_user_commented:1];
                    if (model.commentsArray.count){
                        [model.commentsArray insertObject:commentObject atIndex:0];
                    }else{
                        model.commentsArray = [[NSMutableArray alloc] init];
                        [model.commentsArray addObject:commentObject];
                    }
                    
                }
            }
            NSInteger commentCount = (model.commentsArray.count>3)?3:model.commentsArray.count;
            float CommentHeight = 0.0;
            for (int i = 0; i<commentCount; i++)
            {
                NSString *strText = [NSString stringWithFormat:@"%@ %@",[[model.commentsArray objectAtIndex:i] achieveCommentUname],[[model.commentsArray objectAtIndex:i] achieveCommentText]];
                
                CommentHeight += [YSSupport getCommentTextHeight:strText font:12.0];
            }
            model.feed_comment_height       = CommentHeight+5;
            
            
            [self.allMintsdata replaceObjectAtIndex:i withObject:model];
            
            [_aView performBatchUpdates:^{
                [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]]];
            } completion:^(BOOL finished) {
            }];
            
            break;
        }
        
    }
    
    
}


#pragma mark  POST MINT DELEGATE

-(void)postMintDidClicked:(VideoModel *)someModel{
    // Show the Upload view Here
    for (UIViewController*vc in [self.navigationController viewControllers]) {
        if ([vc isKindOfClass: [MSMintDetails class]] || [vc isKindOfClass: [MSGlobalSearchVCViewController class]]){
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
    [self performSelector:@selector(initiateUploadStatusView:) withObject:someModel afterDelay:0.5];
    });
}




-(void)mintAddedSuccesFully : (NSNotification *) notification{
    for (UIViewController*vc in [self.navigationController viewControllers]) {
        if ([vc isKindOfClass: [MSMintDetails class]]   || [vc isKindOfClass: [MintShowroomViewController class]] || [vc isKindOfClass: [MSGlobalSearchVCViewController class]]){
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
    
    
    SPHSingletonClass *sharedClass = [SPHSingletonClass sharedSingletonClass];
    NSString *linkUrl = [[notification valueForKey:@"userInfo"] valueForKey:@"mintdetail_url"];
    NSString *mintType = [[notification valueForKey:@"userInfo"] valueForKey:@"type"];
    
    if (sharedClass.isFacebookSelected) {
        [self shareFacebook:linkUrl mintType:mintType];
    }
    if (sharedClass.isTwitterSelected) {
        [self shareViaTwitter:linkUrl mintType:mintType withImage:sharedClass.selectedPostMintImage];
    }
    pagenumber = 1;
    [self getMintsMyshow:pagenumber isDelete :YES pull_request_time:NO];
}



-(void)initiateUploadStatusView:(VideoModel*)someModel{
    UploadStatusView *uploadview = [UploadStatusView UploadStatusViewInit];
    uploadview.mintDataStr = someModel.mintDataStr;
    uploadview.upDelegate = self;
    [uploadview setUploadStatusData:nil];
    if(someModel.mintType==1){
        [uploadview uploadVideoWithData:someModel.dataGaleryVideo withImage:someModel.thumbImage];
    }else{
        [uploadview uploadImageWithData:someModel.dataGaleryVideo withImage:someModel.thumbImage];
    }
    CGRect PromoFrame = uploadview.frame;
    PromoFrame.size.width = self.view.frame.size.width;
    PromoFrame.size.height = 50;
    uploadview.frame = PromoFrame;
    [self performSelector:@selector(showHeaderViewOfTable:) withObject:uploadview afterDelay:0.3];
}


-(void)uploadDidfinish:(UploadStatusView*)uploadview{
    SPHSingletonClass *sharedClass = [SPHSingletonClass sharedSingletonClass];
    if (sharedClass.isFacebookSelected) {
        sharedClass.isFacebookSelected = NO;
        if(sharedClass.mintdetail_url != nil)
        [self shareFacebook:sharedClass.mintdetail_url mintType:@"Media"];
        sharedClass.mintdetail_url = nil;
    }
    if (sharedClass.isTwitterSelected) {
        sharedClass.isTwitterSelected = NO;
        if(sharedClass.mintdetail_url != nil)
        [self shareViaTwitter:sharedClass.mintdetail_url mintType:@"Media" withImage:sharedClass.selectedPostMintImage];
        sharedClass.mintdetail_url = nil;
    }
    SETVALUE(@"yes", CEO_ISMINTALREADYPOSTED);
    SYNCHRONISE;
    
    NSString *mintCount = [NSString stringWithFormat:@"%d",[(([GETVALUE(PUSHMINTCOUNT) intValue] == -1)?@"0":GETVALUE(PUSHMINTCOUNT)) intValue]+1];
    SETVALUE(mintCount,PUSHMINTCOUNT);
    SYNCHRONISE;

    
    
    pagenumber = 1;
    [self performSelector:@selector(hideHeaderViewOfTable:) withObject:uploadview afterDelay:2.0];
    [self getMintsMyshow:pagenumber isDelete : YES pull_request_time:NO];
}

-(void)shareViaTwitter : (NSString *)mintdetail_url mintType : (NSString *)mintType withImage:(UIImage *)selectedImage{
    NSLog(@"link url is %@",mintdetail_url);
    [SocialVideoHelper PostimageOntwitterAccount:mintdetail_url withImage:selectedImage];
}


-(void)removeAlert:(UIAlertView *) alert{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}


-(void)shareFacebook : (NSString *)mintdetail_url mintType : (NSString *)mintType
{
    NSString *url = mintdetail_url;
    NSDictionary *params = @{@"link":url,@"name":@"MintShow App", @"description":[NSString stringWithFormat:@"%@ Posted a %@ Mint!",GETVALUE(CEO_UserName),mintType]};
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/feed" parameters:params HTTPMethod:@"POST"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (!error) {
        } else {
            NSLog(@"Graph API Error: %@", [error description]);
        }
    }];
}


-(void)playUploadClicked:(NSURL*)videoUrl{
    movieController = [[MPMoviePlayerController alloc] initWithContentURL:videoUrl];
    [self.view addSubview:movieController.view];
    [movieController setFullscreen:YES animated:YES];
    [movieController prepareToPlay];
    [movieController play];
    NSLog(@"video play preview");
}

-(void)playbDidFinish{
    [movieController stop];
    [movieController.view removeFromSuperview];
    movieController = nil;
}


-(void)showHeaderViewOfTable:(UploadStatusView*)uploadview{
    CGRect uploadFrame = uploadview.frame;
    uploadFrame.origin.x =0.0;
    uploadFrame.origin.y = 0.0;
    uploadFrame.size.width = CGRectGetWidth([[UIScreen mainScreen] bounds]);
    [self.view addSubview:uploadview];
    CGRect collectionFrame = self.aView.frame;
   
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    collectionFrame.origin.y =100;
    collectionFrame.size.height = CGRectGetHeight([[UIScreen mainScreen] bounds])-100;
    uploadFrame.origin.y = 50.0;
    uploadview.frame = uploadFrame;
    self.aView.frame = collectionFrame;
    
    [UIView commitAnimations];

}

-(void)hideHeaderViewOfTable:(UploadStatusView*)uploadview{
    CGRect collectionFrame = self.aView.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [uploadview removeFromSuperview];
    uploadview = nil;
    collectionFrame.origin.y = 92;
    collectionFrame.size.height = CGRectGetHeight([[UIScreen mainScreen] bounds])- 92;
    self.aView.frame = collectionFrame;
    [UIView commitAnimations];
}


-(void)CanceluploadDidclicked:(UploadStatusView*)uploadview{
    if(uploadview.isCancelable){
        [self hideHeaderViewOfTable:uploadview];
       // showAlert(@"Success!", @"You have cancelled the upload!", @"Ok", nil);
    }else{
       // showAlert(@"Warning!", @"You can't Cancel the upload now!", @"Ok", nil);
    }
}




#pragma THIS CODE FOR FILETR VIEW

- (void) FilterListTableDelegateMethod:(NSString *)sender selectedIndex:(NSInteger) selectedRowNo{
}


- (void) FilterListTableDelegateMethod: (NSString *) text selectedId :(NSString *) iddee;{
    [filterTableView hideDropDown];
}

-(IBAction)showSubclassedView:(id)sender{
    filterTableView = [[FilterListTable alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds withTitle:@"Filter" isCateGoryView:NO withPreviousSelection:nil cateGoryId:nil showcategoryList:YES];
    filterTableView.delegate = self;
}





#pragma THIS CODE FOR DISCOVER TABLE

- (void)didSelectSelectRow:(NSInteger)row inSection:(NSInteger)section{
    if (selectedIndex==0) {
        [self gotoMintDetailsScreen:nil forRow:row];
    }else {
        MyShowViewController *allShow = [[MyShowViewController alloc]initWithNibName:@"MyShowViewController" bundle:nil];
        [self.navigationController pushViewController:allShow animated:YES];
    }
}


- (void)dismissPopup{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
    delegateApp.lbl.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}




#pragma mark-  THIS CODE FOR MULTI MINT

- (BOOL)HorizontalScrollerSection:(NSInteger)section{
    if (section==0 || section==1) {
        return YES;
    }
    return NO;
}

-(void)multiMintCollectionViewClicked:(YSCollectionView *)sender AtRow:(NSInteger)row {
    
}

- (BaseFeedModel *)multiMintCollectionViewView:(YSCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    return [allMintsdata objectAtIndex:row];
}

- (NSInteger)NumberOFSectionsMultiMintView:(YSCollectionView *)collectionView{
    return 1;
}

- (NSInteger)NumberOFItemsForMultiMintView:(YSCollectionView *)collectionView forSection:(NSInteger)section{    return  allMintsdata.count;
}

- (void)didLoadNextTriggered{
    if(!isLoading && !isDataFinishedInServer){
        pagenumber = pagenumber+1;
        
        if (isfilterSearchApply) {
            [self filterSearchApplyForMints:NO];
        }
        else
        [self getMintsMyshow:pagenumber isDelete:NO pull_request_time:NO];
    }
}

- (IBAction)logOutClicked:(id)sender{
    AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appDell logOutDidFinish:appDell.ysTabBarController];
}


- (void) displayContentController: (UIViewController*) content{
    [self addChildViewController:content];
    [content willMoveToParentViewController:self];
    [content didMoveToParentViewController:self];
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
}


-(IBAction)showSubclassedView{
    filterTableView = [[FilterListTable alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds withTitle:@"Filters" isCateGoryView:NO withPreviousSelection:nil cateGoryId:nil showcategoryList:YES];
    filterTableView.delegate = self;
}

-(void)gotoComentWithData : (BaseFeedModel *)modelData{
    //    AppDelegate *appdelagate = YACCAppDelegate;
    CommentsViewController *comentVc = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.achievementdetails = modelData ;
    //[self.navigationController pushViewController:comentVc animated:YES];
   // comentVc.hidesBottomBarWhenPushed = YES;
    [self displayContentController:comentVc];
}

-(NSMutableArray *)checkTheDataExistOrNot : (NSString *)mintId andServerArray : (NSArray *)serverArray{
    NSMutableArray *arrayNew = [NSMutableArray array];
    for (BaseFeedModel *obj  in serverArray) {
        if ([[obj feed_id] isEqualToString:mintId]) {
            return arrayNew;
        }
        else
            [arrayNew addObject:obj];
    }
    return arrayNew;
}

-(void)checkAndReloadCollectionview:(Generalmodel *)gmodel{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSMutableArray *resultArray;
        if ([ self.allMintsdata count]) {
            resultArray =[self checkTheDataExistOrNot:[[ self.allMintsdata objectAtIndex:0] feed_id] andServerArray:gmodel.tempArray];
            if ([resultArray count])
            {
                NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                                       NSMakeRange(0,[resultArray count])];
                [ self.allMintsdata insertObjects:resultArray atIndexes:indexes ];
            }
        }
        else
            [ self.allMintsdata addObjectsFromArray: gmodel.tempArray];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if ([gmodel.tempArray count])
                [self batchreloadCollectionView];
        });
    });
    
}

-(void)getMintsMyshow:(int)pageNumber isDelete : (BOOL) isDelete pull_request_time:(BOOL)pullRequestNeedToAdd
{
    isLoading = YES;
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:@"0" forKey:@"mint_list_type"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page_number"];
    [YSAPICalls getMintsForMyShowTabwithDict:dicts SuccessionBlock:^(id newResponse){
        isLoading = NO;
        Generalmodel *gmodel = newResponse;
        if (isPullToreFresh) {
            [refreshCarbon endRefreshing];
            isPullToreFresh = NO;
        }
        if ([gmodel.status_code integerValue]){
            if([gmodel.tempArray count]>0){
                
                if ([[SPHSingletonClass sharedSingletonClass] isUser_mint_streak]) {
                    
                    SPHSingletonClass *obj = [SPHSingletonClass sharedSingletonClass];
                    
                   // [AppDelegate createStreakPopup:[NSString stringWithFormat:@"%d",(int)[obj login_user_streak]]];
                    
                    AppDelegate *obj1 = MINTSHOWAPPDELEGATE;
                    [obj1 createStreakPopup:[NSString stringWithFormat:@"%d",(int)[obj login_user_streak]]];

                    [obj setIsUser_mint_streak:NO];

                    //[obj setValue:[NSString stringWithFormat:@"%d",(int)[obj login_user_streak]] forKey:@"login_user_streak"];

                    

                }
                isDataFinishedInServer = NO;
                if (pageNumber  ==  1) {
                    [UserAccessSession InsertFeedDetailsToDataBase:gmodel.tempArray];
                    SETVALUE(gmodel.pull_request_time, CEO_PULLREQUESTTIME);
                    SYNCHRONISE;
                }
                if (isDelete) {
                    [ self.allMintsdata removeAllObjects];
                }
                [self.allMintsdata addObjectsFromArray: gmodel.tempArray];
                [self batchreloadCollectionView];
                SPHSingletonClass *objectShareClass = [SPHSingletonClass sharedSingletonClass];
                objectShareClass.arrayMyShowMintData = nil;
                [objectShareClass setArrayMyShowMintData:self.allMintsdata];
            }
            else{
                if (pageNumber  ==  1) {
                    [ self.allMintsdata removeAllObjects];
                    [self batchreloadCollectionView];
                }
                isDataFinishedInServer = YES;
            }
        }
        
        else{
        if ([gmodel.status_Msg isEqualToString:@"Authentication failed"]) {
            
            [YSSupport resetDefaultsLogout:NO];
            [UserAccessSession clearAllSession];
            AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            [appDell logOutDidFinish:appDell.ysTabBarController];
        }
        }
    } andFailureBlock:^(NSError *error){
        
        NSLog(@"Failed from the server %@",[error description]);
    }];
}


-(void)batchreloadCollectionView{
    
    BOOL issuesCame = NO;
    @try {
        BOOL animationsEnabled = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        [self.aView performBatchUpdates:^{
            [self.aView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        } completion:^(BOOL finished) {
            if (finished) {
                [UIView setAnimationsEnabled:animationsEnabled];
            }
        }];
        issuesCame = NO;
    }
    @catch (NSException *theException) {
        issuesCame = YES;
        //  showAlert(@"try", @"load tableview again", @"OKAY", nil);
    }
    @finally  {
        issuesCame = NO;
        NSLog(@"Executing finally block");
    }
}




- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }  else {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}

-(void)reloadTableviewWithsection:(NSInteger)section
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [allMintsdata removeAllObjects];
    [self.aView reloadData];
    // Dispose of any resources that can be recreated.
}


- (void)complimintClicked :(BaseFeedModel *)model {
    BaseFeedModel *someModel    = model;
    ///achievements/get_single_achievements
    NSString *feedId        = someModel.feed_id;
    SPHServiceRequest *req  = [[SPHServiceRequest alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@%@?access_token=%@&%@=%d",ServerUrl,API_GETALLCOMPLIMENTS,
                     GETVALUE(CEO_AccessToken),@"mint_id",[feedId intValue]];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //NSLog(@"%@",url);
    [req GETRequestWithUrlAndData:nil mathod:METHOD_GET_SINGLEMINT link:url withSuccessionBlock:^(id response)
     {
         Generalmodel *list_param  = response;
         if ([list_param.status isEqualToString:@"true"]||[list_param.status isEqualToString:@"True"]) {
             BaseFeedModel *model                   = [list_param.tempArray lastObject];
            CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
             comentVc.achievementdetails = model ;
//             comentVc.hidesBottomBarWhenPushed = YES;

             [self.navigationController pushViewController:comentVc animated:YES];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
         }else {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     } andFailureBlock:^(NSError *error) {
         //NSLog(@"%@",error);
     }];
}


- (void)remintClicked :(BaseFeedModel *)feedData{
    NSLog(@"Remint screen ");
    MSRemintVC *reminytVW               = [[MSRemintVC alloc]initWithNibName:@"MSRemintVC" bundle:nil];
    reminytVW.aDelegate = self;
    reminytVW.feedData                  = feedData;
    reminytVW.isFromInstagramTableView  =   @"0";
    [self presentViewController:reminytVW animated:NO completion:^{
        
    }];
}


-(void)showPopupWithModeldata:(BaseFeedModel *)feeddetails withrow:(NSInteger)row isRemint:(BOOL)remint {
    [self gotoProfileView:[NSString stringWithFormat:@"%@",(remint)?feeddetails.remint_user_id:feeddetails.feed_user_id]];

}


-(void)gotoRemintScreen {
   
}


-(void)gotoMintDetailsScreen: (BaseFeedModel *)model forRow:(NSInteger)row{
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mint_row_id = model.mint_row_id;
    mintDetailsVC.Selected_row_value = row;
    mintDetailsVC.delegate    = self;
    mintDetailsVC.isFromMyshow = YES;
    mintDetailsVC.isMintOwner = ([model.feed_user_id isEqualToString:GETVALUE(CEO_UserId)])?YES:NO;

    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}

#pragma  mark --
#pragma mark  -  MSMINTDETAILS  delegate methods
#pragma mark ---

// Called after the user highfive in Mintdetails screen for a mint.

-(void)highFiveMintOfmintDetails :(NSString *)mintId andCurerntValue : (NSString *)activityValue countValue: (NSString *)value{
    int i = 0;
    for (BaseFeedModel *model in self.allMintsdata) {
        if ([model.feed_id isEqualToString:mintId]) {
            [model setIs_user_liked:[activityValue intValue]];
            [model setFeed_like_count:value];
            [ self.allMintsdata replaceObjectAtIndex:i withObject:model];
            YSMintCollectionViewCell *cell =  (YSMintCollectionViewCell *) [self.aView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
                [cell setBackgroundImageForButton:YSButtonTypeHifive AndValue:[NSString stringWithFormat:@"%d",model.is_user_liked] withLabelValue:[NSString stringWithFormat:@"%@",model.feed_like_count]];
            
            
            break;
        }
        i++;
    }
}


-(void)nominateMintOfmintDetails{
}

-(void)remintedSucssesfully {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self getMintsMyshow:pagenumber isDelete:YES pull_request_time:NO];
}

-(void)editRemintDetails : (NSString *)strMessage rowId:(NSInteger)rowId{
    if (rowId<self.allMintsdata.count) {
        BaseFeedModel *model = [ self.allMintsdata objectAtIndex:rowId];
        model.remint_message = strMessage;
        [ self.allMintsdata replaceObjectAtIndex:rowId withObject:model];
        [_aView performBatchUpdates:^{
            [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowId inSection:0]]];
        } completion:^(BOOL finished) {
        }];
    }
}


-(void)addCommentToMintOfmintDetails : (NSInteger)recordNumber CounterValue : (NSInteger)counter
{

    if ([self.allMintsdata count]) {
        
        if ([self.allMintsdata count]>recordNumber) {
            
            BaseFeedModel *model = [ self.allMintsdata objectAtIndex:recordNumber];
            model.feed_comment_count = [NSString stringWithFormat:@"%d",(int)counter];
            [self.allMintsdata replaceObjectAtIndex:recordNumber withObject:model];
            [_aView performBatchUpdates:^{
                [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:recordNumber inSection:0]]];
            } completion:^(BOOL finished) {
            }];
        }
    }
}



-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId {
    [self getMintsMyshow:pagenumber isDelete:NO pull_request_time:NO];
    
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[allMintsdata count]; currentIndex++){
        //do stuff with obj
        BaseFeedModel *feedlist = [allMintsdata objectAtIndex:currentIndex];
        if([mintId isEqualToString:feedlist.feed_id]){
            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [allMintsdata removeObjectsAtIndexes:indexesToDelete];
        [self batchreloadCollectionView];
    }

    
}

-(void)muteMintOfmintDetails : (NSString *)mintId{
    
    
}


-(void)muteUserOfmintDetails:(NSString *)muteUser{
    
        NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
        
        for (int currentIndex = 0;currentIndex<[allMintsdata count]; currentIndex++){
            //do stuff with obj
            BaseFeedModel *feedlist = [allMintsdata objectAtIndex:currentIndex];
            if([muteUser isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){            [indexesToDelete addIndex:currentIndex];
            }
        }
        
        if ([indexesToDelete count]) {
            [allMintsdata removeObjectsAtIndexes:indexesToDelete];
            [self batchreloadCollectionView];
        }

    
}


-(void)gotoCommentScreen : (BaseFeedModel *)model{
    CommentsViewController *comentVc = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.achievementdetails = model ;
  //  comentVc.hidesBottomBarWhenPushed = YES;

    [self.navigationController pushViewController:comentVc animated:YES];
}

-(void)commentScreen : (BaseFeedModel *)model withSelectedMintTag :(NSInteger) tag{
    AppDelegate *aDelegate  =  (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndex =  [NSString stringWithFormat:@"%d",(int)tag];
    CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.commentCount              = [model.feed_comment_count intValue];
    comentVc.achievementdetails        = model ;
    comentVc.feedId                    = model.feed_id ;
    //comentVc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:comentVc animated:YES];
}




@end




