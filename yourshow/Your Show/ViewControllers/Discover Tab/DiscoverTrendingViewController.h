//
//  DiscoverTrendingViewController.h
//  Your Show
//
//  Created by Siba Prasad Hota on 05/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YSCollectionView.h"
#import "DiscoverDataModel.h"

@protocol DiscoverTrendingDelegate

@optional

-(void)gotoProfileView:(NSString *)selectedEnitity;
- (void)selectdFeature :(NSString *)selectedEnitity withType:(int)type isPrivateShowroomAccess:(BOOL)access;
- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell;
- (void)scrollingTableViewDidSelectImageAtIndexPath:(NSIndexPath*)indexPathOfImage Withdata:(DiscoverDataModel*)data atCategoryRowIndex:(NSInteger)categoryRowIndex;
- (void)updateDiscoverTrendingMints:(NSInteger)commentCount mintIndex:(NSInteger)cellIndex;
- (void)showAppdefaultDesignedPushNotificationPopup;

@end



@interface DiscoverTrendingViewController : UIViewController

@property (assign, nonatomic) CGFloat discoverMintHeight;

@property (strong, nonatomic) NSMutableArray *TrendingShowRoomdata;
@property (strong, nonatomic) NSMutableArray *TrendingTagsData;
@property (strong, nonatomic) NSMutableArray *DiscoverMintsdata;

@property (nonatomic, assign)  id <DiscoverTrendingDelegate> delegate;

@end



