//
//  DiscoverViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota on 05/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//


#import "DiscoverViewController.h"

#import "AppDelegate.h"
#import "FilterListTable.h"
#import "Keys.h"
#import "DiscoverTrendingViewController.h"
#import "DiscoverShowroomViewController.h"
#import "CoachingTip.h"
#import "MintShowroomViewController.h"
#import "MSProfileVC.h"
#import "CommentsViewController.h"
#import "MSRemintVC.h"
#import "MSMintDetails.h"
#import "DiscoverDataModel.h"
#import "TrendingTagVC.h"
#import "UserAccessSession.h"
#import "MSGlobalSearchVCViewController.h"
#import "MSPSignatureShowroomVC.h"

@interface DiscoverViewController () <FilterListTableDelegate,ViewPagerDataSource,ViewPagerDelegate,RemintDelegate,MSMintDetailsDelegate,DiscoverTrendingDelegate,MSGlobalSearchDelegate>{
    DiscoverTrendingViewController *trendingVc;
    DiscoverShowroomViewController *showroomVc;
    FilterListTable *filterTableView;
    IBOutlet UIButton *btnFilter;
    __weak IBOutlet UILabel *lblDeletedShowroom;
    IBOutlet UIView  *alertDeleteShowroom;

    IBOutlet UIView *viewPushNotififcations;

}

@property (nonatomic) NSUInteger numberOfTabs;


@end


@implementation DiscoverViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataSource = self;
    self.delegate = self;
    
    [MSMixPanelTrackings trackForATaskForUser:nil withTaskType:MSTaskTypeOpenedDiscoverScreen];
    if ([GETVALUE(CEO_DISCOVERALLFIRSTTIMEVISIT) isEqualToString:@"yes"])  {
//        SETVALUE(@"no", CEO_DISCOVERALLFIRSTTIMEVISIT);
//        SYNCHRONISE;
        CoachingTip *viewCoach = [[CoachingTip alloc]initWithFrame:[UIScreen mainScreen].bounds screenName:@"Discover"headerPresent:NO];
        [self.view addSubview:viewCoach];
    }
    [self performSelector:@selector(loadContent) withObject:nil afterDelay:0.0];
    // Do any additional setup after loading the view from its nib.
    btnFilter.layer.cornerRadius = 5.;
    self.toplayoutVal = 53.0;
    NSLog(@"LaunchFirstTime IS %@",GETVALUE(@"LaunchFirstTime"));



}
- (void)showAppdefaultDesignedPushNotificationPopup
{
    
    UIApplication *application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        if ([application isRegisteredForRemoteNotifications]) {
            NSLog(@"Push allowed");
        }
        
        else
        {
            if (![GETVALUE(APPPUSHALLOWED) isEqualToString:@"yes"]) {
                NSLog(@"Counter %d",(int)[GETVALUE(PUSHMINTCOUNT) integerValue]);
                if ([GETVALUE(PUSHMINTCOUNT) integerValue] >= 5 || [GETVALUE(PUSHMINTCOUNT) integerValue] == 0) {
                    NSLog(@"Apple Push notification not yet shown");
                    AppDelegate *delegate =  MINTSHOWAPPDELEGATE
                    viewPushNotififcations.frame = CGRectMake(0, 0, 320, 568);
                    [delegate.window addSubview:viewPushNotififcations];
                }
            }
        }
    }
}
- (IBAction)getPushNotification:(UIButton *)sender {
    [viewPushNotififcations removeFromSuperview];
    AppDelegate *delegate =  MINTSHOWAPPDELEGATE
    [delegate registerForRemoteNotification];
    SETVALUE(@"yes",APPPUSHALLOWED);
    SYNCHRONISE;
  
}
- (IBAction)DontAskAgain:(UIButton *)sender {
    SETVALUE(@"no", APPPUSHALLOWED);
    SYNCHRONISE;
    NSString *mintCount = [NSString stringWithFormat:@"%@", @"-1"];
    SETVALUE(mintCount,PUSHMINTCOUNT);
    SYNCHRONISE;
    
    
    [viewPushNotififcations removeFromSuperview];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self showAppdefaultDesignedPushNotificationPopup];
    
    
}
-(void)mintAddedToShowroom:(NSString *)notify
{
    if ([notify length])
    {
        NSLog (@"Successfully received test notification for showroom add! %@", notify);
        MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
        vcShowRoom.selectedShowroom =  ([notify rangeOfString:@"*"].location!=NSNotFound)?[notify substringWithRange:NSMakeRange(1, notify.length-1)]:notify;
        [self.navigationController pushViewController:vcShowRoom animated:NO];
     }
}

#pragma THIS CODE FOR FILETR VIEW
-(void)FilterListTableDelegateMethodCategoryId:(NSString *)text sort_by_option:(NSInteger)sort_by_option sort_by_feature:(NSInteger)sort_by_feature{
    
}

- (void) FilterListTableDelegateMethod:(NSString *)sender selectedIndex:(NSInteger) selectedRowNo
{
    
}
- (void) FilterListTableDelegateMethod: (NSString *) text selectedId :(NSString *) iddee;
{
    [filterTableView hideDropDown];
}

-(IBAction)showSubclassedView:(id)sender
{
 
    filterTableView = [[FilterListTable alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds withTitle:@"Filter" isCateGoryView:NO withPreviousSelection:nil cateGoryId:nil showcategoryList:YES];

    filterTableView.delegate = self;
}


- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell{
    if(buttonType == YSButtonTypeComplimint)  {
        [self commentScreen:data withSelectedMintTag:row];
    }else if (buttonType == YSButtonTypeInspire)  {
        [self remintClicked:data];
    } else if (buttonType == YSButtonTypedetails){
        [self gotoMintDetailsScreen:data];
    }
}




-(void)editRemintDetails:(NSString *)strMessage rowId:(NSInteger)rowId{
    
}
-(void)editRemintedSucssesfully:(NSString *)strRemintText{
 
}

-(void)gotoMintDetailsScreen: (BaseFeedModel *)model {
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mint_id         = model.feed_id;
    mintDetailsVC.delegate        = self;
    mintDetailsVC.isMintOwner     = NO;
    mintDetailsVC.isToshowSingleRecord = YES;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}

-(void)commentScreen : (BaseFeedModel *)model withSelectedMintTag :(NSInteger) tag {
    AppDelegate *aDelegate  =  (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndex =  [NSString stringWithFormat:@"%d",(int)tag];
    BaseFeedModel *someModel    =  model;
    
    CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.commentCount              = [someModel.feed_comment_count intValue];
    comentVc.achievementdetails        = model ;
    comentVc.feedId                    = someModel.feed_id ;
   // comentVc.hidesBottomBarWhenPushed = YES;

    [self.navigationController pushViewController:comentVc animated:YES];
}

- (void)remintClicked :(BaseFeedModel *)feedData {
    //POSTHi Five/UnHifive a post from newsfeed/newsfeed/high_five/unlike
    NSLog(@"Remint screen ");
    
    MSRemintVC *reminytVW               = [[MSRemintVC alloc]initWithNibName:@"MSRemintVC" bundle:nil];
    reminytVW.aDelegate = self;
    reminytVW.feedData                  = feedData;
    reminytVW.isFromInstagramTableView  =   @"0";
    [self presentViewController:reminytVW animated:NO completion:^{
        
    }];
}

#pragma THIS CODE FOR MINT SCROLLER

- (void)scrollingTableViewDidSelectImageAtIndexPath:(NSIndexPath*)indexPathOfImage Withdata:(DiscoverDataModel*)data atCategoryRowIndex:(NSInteger)categoryRowIndex
{
    if(categoryRowIndex == 0)
    {

        MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
        vcShowRoom.selectedShowroom = data.discoverName;
        [self.navigationController pushViewController:vcShowRoom animated:YES];
        
        
//        MSPSignatureShowroomVC *vcShowRoom = [[MSPSignatureShowroomVC alloc]initWithNibName:@"MSPSignatureShowroomVC" bundle:nil];
//        vcShowRoom.modelShow = data;
//        vcShowRoom.isFromDiscoverShowroom = YES;
//        [self.navigationController pushViewController:vcShowRoom animated:YES];
        
    }
    else{
        MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
        mintDetailsVC.delegate = self;
        mintDetailsVC.isToshowSingleRecord = NO;
        mintDetailsVC.isFromTrendingTags = YES;
        mintDetailsVC.isMintOwner = NO;
        mintDetailsVC.navigationHeaderText = data.discoverName;
        [self.navigationController pushViewController:mintDetailsVC animated:YES];
       
    }
}




#pragma mark - Setters
- (void)setNumberOfTabs:(NSUInteger)numberOfTabs {
    // Set numberOfTabs
    _numberOfTabs = numberOfTabs;
    // Reload data
    [self reloadData];
    
}

#pragma mark - Helpers
- (void)selectTabWithNumberFive:(NSInteger)selectedTab{
    [self selectTabAtIndex:0];
}
- (void)loadContent {
    self.numberOfTabs = 2;
//    [self performSelector:@selector(selectTabWithNumberFive) withObject:nil];

}

#pragma mark - Interface Orientation Changes
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    // Update changes after screen rotates
    [self performSelector:@selector(setNeedsReloadOptions) withObject:nil afterDelay:duration];
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.numberOfTabs;
}
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    UIFont *fontVal = [UIFont fontWithName:@"Helvetica Neue Medium" size:8];
    label.font = fontVal;
    if (index == 0) {
        label.text = @"TRENDING";
    }
    else if (index == 1) {
        label.text = @"SHOWROOMS";
    }
    
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    [label sizeToFit];
    
    return label;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    
    if (index == 0) {
        trendingVc = [[DiscoverTrendingViewController alloc]initWithNibName:@"DiscoverTrendingViewController" bundle:nil];
        trendingVc.delegate =self;
        return trendingVc;
    } else {
        showroomVc = [[DiscoverShowroomViewController alloc]initWithNibName:@"DiscoverShowroomViewController" bundle:nil];
        return showroomVc;
    }
}

#pragma mark - ViewPagerDelegate
- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            return 40.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            return CGRectGetWidth([UIScreen mainScreen].bounds)/_numberOfTabs;
        case ViewPagerOptionFixFormerTabsPositions:
            return 0.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 0.0;
        default:
            return value;
    }
}
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return ORANGECOLOR;
        case ViewPagerTabsView:
            return [[UIColor whiteColor] colorWithAlphaComponent:0.32];
        case ViewPagerContent:
            return [[UIColor darkGrayColor] colorWithAlphaComponent:0.32];
        default:
            return color;
    }
}
-(void)muteUserOfmintDetails : (NSString *)muteUser{

}

//For Mute mint, Delete Mint. FlagInappropriate
-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId
{
}
- (IBAction)searchTapped:(UIButton *)sender {
    MSGlobalSearchVCViewController  *searchVC =  [[MSGlobalSearchVCViewController alloc] initWithNibName:@"MSGlobalSearchVCViewController" bundle:nil];
    searchVC.delegate = self;
    [self.navigationController pushViewController:searchVC animated:NO];
}

-(void)serverCallForTheSearch :(NSString *)textToSearch GlobalsearchApi : (BOOL)GlobalSearch
{
    [self gotoProfileViewFromSearch:@"jiten"];
    
}

-(void)gotoProfileViewFromSearch:(NSString *)selectedEnitity{
    NSString *newStr = [selectedEnitity substringWithRange:NSMakeRange(1, [selectedEnitity length]-1)];
    
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = newStr;
    [self.navigationController pushViewController:prof animated:YES];
}
@end



/*
 
 - (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
 UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"TrendingHeaderView" forIndexPath:indexPath];
 if (reusableview==nil) {
 reusableview=[[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 200)];
 }
 return reusableview;
 }
 return nil;
 }
 
 
 - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
 return CGSizeMake(0, 200);
 }
*/