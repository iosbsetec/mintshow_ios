//
//  DiscoverTrendingViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota on 05/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "DiscoverTrendingViewController.h"
#import "DiscoverDataModel.h"
#import "UIViewController+CWPopup.h"
#import "UserAccessSession.h"
#import "MintShowroomViewController.h"
#import "MSProfileVC.h"
#import "UsersListView.h"
#import "MSMintDetails.h"
#import "CarbonKit.h"
#import "CommentModel.h"
#import "CommentsViewController.h"
#import "MSPSignatureShowroomVC.h"


@interface DiscoverTrendingViewController ()<DiscoverTableViewDataSource,DiscoverTableDelegate,YSCollectionViewDataSource,YSCollectionViewDelegate,MSMintDetailsDelegate,CommentsViewDelegate,UsersListDelegate> {
    
    BOOL isLoading;
    BOOL isDataFinishedInServer;
    int  pagenumber;
    CarbonSwipeRefresh *refreshCarbon;

}

@property (strong, nonatomic) IBOutlet DiscoverTableview *DiscoverAllTable;
@property (strong, nonatomic) IBOutlet YSCollectionView *aView;

@end

@implementation DiscoverTrendingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissPopup) name:@"CLosePopupNow" object:nil];
   // [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushRelevant) name:@"AppPushEnabled" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updatedComponent:) name:@"SINGLEMINTUPDATE" object:nil];


    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(mintEditedSuccesFullyInTrending:) name:KMINTEDITED object:nil];
    
    [self initializer:0];

    self.TrendingShowRoomdata = [[NSMutableArray alloc]init];
    self.TrendingTagsData = [[NSMutableArray alloc]init];
    self.DiscoverMintsdata = [[NSMutableArray alloc]init];
    self.DiscoverMintsdata = [UserAccessSession getAllDiscoverMints];
    self.DiscoverAllTable.showArrowButton = NO;
    self.DiscoverAllTable.DiscoverDataSource = self;
    self.DiscoverAllTable.Discoverdelegate = self;
    
    isLoading = NO;
    isDataFinishedInServer = NO;
    pagenumber = 1;
    
    if(self.DiscoverMintsdata.count)
        [self batchreloadCollectionView];

    [self getAllDataForDiscoverTab];
    //[self createRefreshView];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)createRefreshView
{
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:self.DiscoverAllTable withYPos:10];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
}
#pragma mark - QBRefreshControlDelegate


- (void)refresh:(id)sender {
    
    [self getAllDataForDiscoverTab];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}


- (void)endRefreshing {
    [refreshCarbon endRefreshing];
}

-(void)initializer : (int)type
{
    UIColor *backgroundCol     =  [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.aView.backgroundColor = backgroundCol;
    self.DiscoverAllTable.backgroundColor     = backgroundCol;
    self.aView.showsHorizontalScrollIndicator = NO;
    self.aView.scrollEnabled   = NO;
    self.aView.ysDataSource    = self;
    self.aView.ysDelegate      = self;
    self.aView.bounces         = NO;
    self.aView.frame           = CGRectMake(3, 5, self.view.frame.size.width, self.discoverMintHeight);
}
-(void)pushRelevant
{
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if (types == UIRemoteNotificationTypeNone)
    {
        [self getfive_mints_posted];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    NSLog(@"LaunchFirstTime IS %@",GETVALUE(@"LaunchFirstTime"));
//
//    
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
//        UIUserNotificationType type = [[[UIApplication sharedApplication] currentUserNotificationSettings] types];
//        if (type == UIUserNotificationTypeNone){
//            [self getfive_mints_posted];
//
//        }
//        
//    }
//    else {
//        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
//        if (types == UIRemoteNotificationTypeNone) {
//            [self getfive_mints_posted];
//
//        }
//        
//    }
//    
    
    AppDelegate *delegateApp = MINTSHOWAPPDELEGATE
    
    if (delegateApp.isMintDetailsChanged)
    {
        delegateApp.isMintDetailsChanged = NO;
        pagenumber = 1;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)addCommentToMintOfmintDetails : (NSInteger)recordNumber CounterValue : (NSInteger)counter
{
    if ([self.DiscoverMintsdata count]) {
        if ([self.DiscoverMintsdata count]>recordNumber) {
            BaseFeedModel *model     = [self.DiscoverMintsdata objectAtIndex:recordNumber];
            model.feed_comment_count = [NSString stringWithFormat:@"%d",(int)counter];
            model.is_user_commented  = 1;
            [self.DiscoverMintsdata replaceObjectAtIndex:recordNumber withObject:model];
            [_aView performBatchUpdates:^{
                [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:recordNumber inSection:0]]];
            } completion:^(BOOL finished) {
            }];
        }
    }
}
-(void)getfive_mints_posted
{
    AppDelegate *delegateApp = MINTSHOWAPPDELEGATE
    NSString*  submitData    = [NSString stringWithFormat:@"accestoken=%@&deviceToken=%@",GETVALUE(CEO_AccessToken),[delegateApp getDeviceTkenFromKeyChain]];
    [self CallAPIForMintPosted:[NSURL URLWithString:[NSString stringWithFormat:@"%@users/five_mints_posted?%@",ServerUrl,submitData]]];
    
}
-(void)CallAPIForMintPosted:(NSURL *)myUrl
{
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSDictionary *activity = [YSSupport dictionaryFromResponseData:data];
        NSLog(@"Activity ****** = %@",activity);
        if ([[activity valueForKey:@"Status"]isEqualToString:@"Success"])  {
            if ([[activity valueForKey:@"5mintPosted"]isEqualToString:@"yes"]) {
                [self.delegate showAppdefaultDesignedPushNotificationPopup];
            }
         
        }
    }];
    
}


-(void)updatedComponent:(NSNotification *) notification
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    if (aDelegate.selectedIndexForTrendingMint >=self.DiscoverMintsdata.count) {
        return;
    }
    BaseFeedModel *model = (BaseFeedModel*)[[notification valueForKey:@"userInfo"] valueForKey:@"model"];
    [self.DiscoverMintsdata replaceObjectAtIndex:aDelegate.selectedIndexForTrendingMint withObject:model];
    [_aView performBatchUpdates:^{
        [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:aDelegate.selectedIndexForTrendingMint  inSection:0]]];
    } completion:^(BOOL finished) {
        
    }];
    
}
-(void)updateCommentCount:(NSInteger)commentCount alreadyCommented:(BOOL)commented
{

    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    if (aDelegate.selectedIndexForTrendingMint >=self.DiscoverMintsdata.count) {
        return;
    }
    BaseFeedModel *model = (BaseFeedModel*)[self.DiscoverMintsdata objectAtIndex:aDelegate.selectedIndexForTrendingMint];
    model.feed_comment_count = [NSString stringWithFormat:@"%d",(int)commentCount];
    model.is_user_commented = (commented)?1:0;
    [self.DiscoverMintsdata replaceObjectAtIndex:aDelegate.selectedIndexForTrendingMint withObject:model];
    [_aView performBatchUpdates:^{
        [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:aDelegate.selectedIndexForTrendingMint  inSection:0]]];
    } completion:^(BOOL finished) {
        
    }];
    

}


-(void)getAllDataForDiscoverTab{
    isLoading = YES;
    NSData *submitData =  [[NSString stringWithFormat:@"access_token=%@&disc_type=0&page_number=%d",
                            GETVALUE(CEO_AccessToken),pagenumber]dataUsingEncoding:NSUTF8StringEncoding];
    [YSAPICalls getForDiscoverTab:submitData withSuccessionBlock:^(id newResponse) {
        Generalmodel *gmodel = newResponse;
        isLoading = NO;
       
        if ([gmodel.status_code integerValue] && [gmodel.status isEqualToString:@"Success"]) {
            
            if ([[SPHSingletonClass sharedSingletonClass] isUser_mint_streak]) {
                
                AppDelegate *obj = MINTSHOWAPPDELEGATE;
                [obj createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];

                
               // [AppDelegate createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];
                [[SPHSingletonClass sharedSingletonClass]
                 setIsUser_mint_streak:NO];
                
            }
            
            
           
            if (pagenumber==1) {
                [self.DiscoverMintsdata    removeAllObjects];
                [self.TrendingShowRoomdata removeAllObjects];
                [self.TrendingTagsData     removeAllObjects];
            }
            
            self.TrendingShowRoomdata  = [gmodel.tempArray mutableCopy];
            self.TrendingTagsData      = [gmodel.tempArray2 mutableCopy];
            self.DiscoverMintsdata     = [gmodel.tempArray3 mutableCopy];
            
            if (pagenumber==1) {
                [UserAccessSession InsertDiscoverMints:self.DiscoverMintsdata];
            }
            [self batchreloadCollectionView];
        }
        
        else{
        
            if ([gmodel.status_Msg isEqualToString:@"Authentication Failed"]) {
                
                [YSSupport resetDefaultsLogout:NO];
                [UserAccessSession clearAllSession];
                AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                [appDell logOutDidFinish:appDell.ysTabBarController];
            }
        }
    } andFailureBlock:^(NSError *error) {
    }];
}

-(void)loadNextDataForMints{
    isLoading = YES;
    NSData *submitData =  [[NSString stringWithFormat:@"access_token=%@&disc_type=0&page_number=%d",
                            GETVALUE(CEO_AccessToken),pagenumber]dataUsingEncoding:NSUTF8StringEncoding];
    [YSAPICalls getForDiscoverTab:submitData withSuccessionBlock:^(id newResponse) {
        Generalmodel *gmodel = newResponse;
        isLoading = NO;
        if ([gmodel.status_code integerValue] && [gmodel.status isEqualToString:@"Success"]) {
            if ([gmodel.tempArray3 count]) {
                isDataFinishedInServer = NO;
            [self.DiscoverMintsdata  addObjectsFromArray:gmodel.tempArray3];
            [self batchreloadCollectionView];
            }  else {
                isDataFinishedInServer = YES;
            }
        }
    } andFailureBlock:^(NSError *error) {
    }];
}

- (void)dismissPopup
{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
   // delegateApp.lbl.hidden = NO;
    
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
        }];
    }
}


#pragma THIS CODE FOR MULTI MINT

-(YSCollectionView *)discoverCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.aView;
}

-(void)multiMintCollectionViewClicked:(YSCollectionView *)sender AtRow:(NSInteger)row
{
    
}

- (BaseFeedModel *)multiMintCollectionViewView:(YSCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    return [self.DiscoverMintsdata objectAtIndex:row];
}

- (NSInteger)NumberOFSectionsMultiMintView:(YSCollectionView *)collectionView{
    return 1;
}

- (NSInteger)NumberOFItemsForMultiMintView:(YSCollectionView *)collectionView forSection:(NSInteger)section{
    return self.DiscoverMintsdata.count;
}

- (void)LoadNextPagetriggeredForSection:(NSInteger)section{
    if(!isLoading && !isDataFinishedInServer){
        pagenumber = pagenumber+1;
        [self loadNextDataForMints];
    }
}

- (void)selectdFeature :(NSString *)selectedEnitity withType:(int)type isPrivateShowroomAccess:(BOOL)access{
    [self.delegate selectdFeature:selectedEnitity withType:type isPrivateShowroomAccess:access];
}

-(void)selectedFeatureType:(int)featureType withString:(NSString *)selectedEnitity isPrivateShowroomAccess:(BOOL)access{
    
    switch (featureType) {
        case 0:
            [self gotoHashTagView:selectedEnitity];
            break;
        case 1:
        {
            if(access) {
                showAlert(@"It's a private showroom", nil, @"Ok", nil);
            }
            else
                [self gotoOldShowroom:selectedEnitity];
        }
            break;
        case 2:case 44:
            [self gotoProfileView:selectedEnitity];
            break;
        case 88:
            [self gotoSignatureShowroom:selectedEnitity];
            break;
        default:
            break;
    }
    
    
    
}

-(void)mintEditedSuccesFullyInTrending:(NSNotification *) notification
{
    AppDelegate  * aDelegate = (AppDelegate  *) [UIApplication sharedApplication].delegate;
    
    
    if (self.DiscoverMintsdata.count>0 && aDelegate.selectedIndexForTrendingMint<self.DiscoverMintsdata.count)
    {
        BaseFeedModel *model = [self.DiscoverMintsdata objectAtIndex:aDelegate.selectedIndexForTrendingMint];
        model.feed_description = [[notification valueForKey:@"userInfo"] valueForKey:@"description"];
        [self.DiscoverMintsdata replaceObjectAtIndex:aDelegate.selectedIndexForTrendingMint withObject:model];
        [_aView performBatchUpdates:^{
            [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:aDelegate.selectedIndexForTrendingMint  inSection:0]]];
        } completion:^(BOOL finished) {
            
        }];
    }
}
-(void)gotoSignatureShowroom:(NSString *)mintType
{
    
    MSPSignatureShowroomVC *vcShowRoom = [[MSPSignatureShowroomVC alloc]initWithNibName:@"MSPSignatureShowroomVC" bundle:nil];
    vcShowRoom.isFromDiscoverShowroom = NO;
    vcShowRoom.selectedShowroom = mintType;
    [self.navigationController pushViewController:vcShowRoom animated:YES];
    
    
}
-(void)gotoShowroomPageFromDiscoverTrendingTags:(NSString *)selectedEnitity
{
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [selectedEnitity substringFromIndex:1];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}

-(void)gotoProfilePageFromDiscoverTrendingTags:(NSString *)selectedEnitity;
{
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    // prof.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:prof animated:YES];
}


-(void)gotoHashTagView:(NSString *)selectedEnitity{
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.delegate = self;
    mintDetailsVC.isToshowSingleRecord = NO;
    mintDetailsVC.isFromTrendingTags = YES;
    mintDetailsVC.isMintOwner = NO;
    mintDetailsVC.navigationHeaderText = selectedEnitity;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}

-(void)gotoProfileView:(NSString *)selectedEnitity{

    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    // prof.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:prof animated:YES];
}

-(void)gotoOldShowroom:(NSString *)selectedEnitity{
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [selectedEnitity substringFromIndex:1];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}



-(void)commentScreen : (BaseFeedModel *)model withSelectedMintTag :(NSInteger) tag {
    AppDelegate *aDelegate  =  (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndexForTrendingMint =  tag;
    BaseFeedModel *someModel    =  model;
    
    CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.commentCount              = [someModel.feed_comment_count intValue];
    comentVc.achievementdetails        = model ;
    comentVc.feedId                    = someModel.feed_id ;
    comentVc.isFromTrendingMints = YES;
    comentVc.delegate = self;
    [self.navigationController pushViewController:comentVc animated:YES];
}
- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell{
    BaseFeedModel *existingFeedModeldata = [self.DiscoverMintsdata objectAtIndex:row];
    if(buttonType == YSButtonTypeComplimint)  {
        [self commentScreen:existingFeedModeldata withSelectedMintTag:row];
    }
    else if (buttonType == YSButtonTypeHifiveCount)  {
        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeHighFive numberofPeoples:[existingFeedModeldata.feed_like_count intValue] feedId:existingFeedModeldata.feed_id delegate:self];
    }
    else if (buttonType == YSButtonTypeInspire)  {
        [self.delegate buttonActionclickedFortype:buttonType forRow:row feedModelData:existingFeedModeldata forCell:cell];
    } else if (buttonType == YSButtonTypedetails){
        [self gotoMintDetailsScreen:existingFeedModeldata rowNumber:row];
    } else if (buttonType == YSButtonTypeUserImage) {
        [self showPopupWithModeldata:data withrow:row isRemint:NO];
    } else if (buttonType == YSButtonTypeRemintUserImage) {
        [self showPopupWithModeldata:data withrow:row isRemint:YES];
    }else if (buttonType == YSButtonTypeHifive){
        [self.DiscoverMintsdata replaceObjectAtIndex:row withObject:data];
    }
}
-(void)selectedUserIdFromHighfiveList:(NSString *)selectdUserId
{
    
    [self gotoProfileView:selectdUserId];
    
}
-(void)gotoMintDetailsScreen: (BaseFeedModel *)model rowNumber:(NSInteger)rowNumber {
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mint_id         = model.feed_id;
    mintDetailsVC.delegate        = self;
    mintDetailsVC.isMintOwner     = NO;
    mintDetailsVC.isToshowSingleRecord = YES;
    mintDetailsVC.isFromTrendingTags = NO;
    mintDetailsVC.isFromDiscoverTrending = YES;
    mintDetailsVC.Selected_row_valueTrendingMints = rowNumber;
    AppDelegate *delegateApp = MINTSHOWAPPDELEGATE
    delegateApp.selectedIndexForTrendingMint = rowNumber;

    [self.navigationController pushViewController:mintDetailsVC animated:YES];

}

- (void)didLoadNextTriggered{
    if(!isLoading && !isDataFinishedInServer){
        pagenumber = pagenumber+1;
        [self loadNextDataForMints];
    }
}

- (void)scrollingTableViewDidSelectImageAtIndexPath:(NSIndexPath*)indexPathOfImage Withdata:(DiscoverDataModel*)data atCategoryRowIndex:(NSInteger)categoryRowIndex{
    if(categoryRowIndex == 1)
    {
        MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
        mintDetailsVC.delegate = self;
        mintDetailsVC.isToshowSingleRecord = NO;
        mintDetailsVC.isFromTrendingTags = YES;
        mintDetailsVC.isMintOwner = NO;
        mintDetailsVC.navigationHeaderText = data.discoverName;
        [self.navigationController pushViewController:mintDetailsVC animated:YES];
    }
    else
    [self.delegate scrollingTableViewDidSelectImageAtIndexPath:indexPathOfImage Withdata:data atCategoryRowIndex:categoryRowIndex];
}

-(void)gotoShowroomPageFromPPImageScrollingTableViewCell:(NSString *)selectedEnitity
{
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [selectedEnitity substringFromIndex:1];
    [self.navigationController pushViewController:vcShowRoom animated:YES];


}
-(void)gotoProfilePageFromPPImageScrollingTableViewCell:(NSString *)selectedEnitity
{

    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    [self.navigationController pushViewController:prof animated:YES];


}
-(void)batchreloadCollectionView{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.aView performBatchUpdates:^{
        [self.aView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
            self.discoverMintHeight = self.aView.collectionViewLayout.collectionViewContentSize.height;
            self.aView.frame = CGRectMake(3, 5, self.view.frame.size.width, self.discoverMintHeight);
            if (pagenumber<3) {
                [self.DiscoverAllTable reloadData];
            }else{
            [self.DiscoverAllTable reloadData];
            }
        }
    }];
}
/*
 reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationNone
 */
-(void)reloadTableviewWithsection:(NSInteger)section {
    [self.DiscoverAllTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:section], nil]  withRowAnimation:UITableViewRowAnimationNone];
}


- ( NSInteger)DiscoverTableViewNumberOfsections:(DiscoverTableview *)tableView {
    return (self.DiscoverMintsdata.count)?3:2;
}

-(CGFloat)DiscoverTableView:(DiscoverTableview *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0||indexPath.section ==1){
        return 130;
    }
    return self.discoverMintHeight;
}

-(CGFloat)DiscovertableView:(DiscoverTableview *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.0;
}


- (DiscoverViewCellType)DiscoverTableView:(DiscoverTableview *)tableView cellTypeForSection:(NSInteger)section
{
    if (section == 0 || section ==1)
        return DiscoverViewCellTypeImageScroller;
    else
        return DiscoverViewCellTypeMultiMint;
}


- (NSString *)DiscoverTableView:(DiscoverTableview *)tableView TitleOfHeaderForSection:(NSInteger)section
{
    if (section == 0)
        return @"TRENDING SHOWROOMS";
    if (section == 1)
        return @"TRENDING TAGS";
    else if (section == 2)
        return  @"TRENDING MINTS";
    else return @"";
}

- (NSDictionary *)DiscoverTableView:(DiscoverTableview *)tableView DataForSection:(NSInteger)section
{
    if (section == 0) {
        return [NSDictionary dictionaryWithObjectsAndKeys:self.TrendingShowRoomdata ,@"images", nil];
    }else if (section == 1) {
        return [NSDictionary dictionaryWithObjectsAndKeys:self.TrendingTagsData ,@"images", nil];
    }
    return [NSDictionary dictionaryWithObjectsAndKeys:self.TrendingTagsData ,@"images", nil];
}

- (void)didSelectSelectRow:(NSInteger)row inSection:(NSInteger)section
{

}
- (void)didSelectSelectAllButtonClickedforSection:(NSInteger) section
{

}


-(void)showPopupWithModeldata:(BaseFeedModel*)feeddetails withrow:(NSInteger)row isRemint:(BOOL)remint {
    if (remint) {
        [self gotoProfileView:[NSString stringWithFormat:@"%@",feeddetails.remint_user_id]];
    }
    else
        [self gotoProfileView:[NSString stringWithFormat:@"%@",feeddetails.feed_user_id]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)muteUserOfmintDetails : (NSString *)muteUser{
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[self.DiscoverMintsdata count]; currentIndex++){
        //do stuff with obj
        BaseFeedModel *feedlist = [self.DiscoverMintsdata objectAtIndex:currentIndex];
        if([muteUser isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [self.DiscoverMintsdata removeObjectsAtIndexes:indexesToDelete];
        [self batchreloadCollectionView];
    }
    
}
//For Mute mint, Delete Mint. FlagInappropriate
-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId
{
    int mintIndex = 0;
    BOOL isMintFoundFromList = NO;
    
    for (int i = 0; i<self.DiscoverMintsdata.count; i++) {
        
        
        if ([[[self.DiscoverMintsdata objectAtIndex:i]feed_id] isEqualToString:mintId]) {
            mintIndex = i;
            isMintFoundFromList = YES;
            break;
        }
        
    }
    
    if (isMintFoundFromList) {
        [self.DiscoverMintsdata removeObjectAtIndex:mintIndex];
        [self batchreloadCollectionView];
    }
}
- (void)loadNextShowroomPages{
    NSLog(@"Need not to load next showrooms/Hash tag");
}

@end
