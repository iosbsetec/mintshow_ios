//
//  DiscoverViewController.h
//  Your Show
//
//  Created by Siba Prasad Hota on 05/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ViewPagerController.h"


@protocol DiscoverViewControllerDelegate
-(void)showAppdefaultDesignedPushNotificationPopup;
@end;


@interface DiscoverViewController : ViewPagerController

@property (assign,nonatomic) id  <DiscoverViewControllerDelegate> aDelegate;

@end