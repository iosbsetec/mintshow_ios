//
//  DiscoverShowroomViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota on 05/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "DiscoverShowroomViewController.h"
#import "MintShowroomViewController.h"
#import "CarbonKit.h"
#import "JitenCollectionView.h"
#import "AddEditShowroomVC.h"
#import "UserAccessSession.h"
#import "DiscoverDataModel.h"
#import "PPImageScrollingCellView.h"
#import "DiscoverTableview.h"
#import "MSPSignatureShowroomVC.h"


@interface DiscoverShowroomViewController () <CarbonTabSwipeNavigationDelegate,JitenCollectionTableDelegate,JitenCollectionViewDataSource,AddEditShowroomVCDelegate,ParticularShowroomDelegate,DiscoverTableViewDataSource,DiscoverTableDelegate>
{


    CarbonSwipeRefresh *refreshCarbon;
    NSMutableArray *allShowRoomdata;
    int pagenumberShowroom;
    int pagenumberSignShowroom;

    BOOL isLoading,isSignApiLoading;
 
    NSMutableArray *signatureShowRoomdata;
    CGFloat multiShowroomHeight;
    BOOL isfirstTime;

}
@property (strong, nonatomic) IBOutlet DiscoverTableview *DiscoverAllTable;
 @property (strong, nonatomic) IBOutlet JitenCollectionView *showRoomCollectionview;
@end

@implementation DiscoverShowroomViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    signatureShowRoomdata = [[NSMutableArray alloc]init];

    allShowRoomdata = [[NSMutableArray alloc]init];
    allShowRoomdata = [UserAccessSession getAllrecordForShowroomList];
    pagenumberSignShowroom = 1;
    if (allShowRoomdata.count) {
        [self reloadTableviewWithsection:1];
    }
    multiShowroomHeight = 400;
//    self.showRoomCollectionview.frame = CGRectMake(0, 0, self.view.frame.size.width, 400);
    isfirstTime = YES;
   
    
    //[self getSignatureShowroom:pagenumberSignShowroom delete:NO];

    pagenumberShowroom = 1;
    UIColor *backgroundCol =  [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.view.backgroundColor = backgroundCol;
    self.DiscoverAllTable.DiscoverDataSource = self;
    self.DiscoverAllTable.Discoverdelegate   = self;

    [self getShowroomListwithPagenum:pagenumberShowroom];
    self.showRoomCollectionview.collectiondelegate = self;
    self.showRoomCollectionview.CollectionDataSource = self;
    [self createRefreshView];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(deleteShowroomFromList)  name:@"DeleteShowRoom" object:nil];
    // Do any additional setup after loading the view from its nib.
    
}



-(void)getSignatureShowroom:(int)pageNo delete:(BOOL)deleteShowroom
{
    isSignApiLoading = YES;
    
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&page_number=%d",GETVALUE(CEO_AccessToken),pageNo] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"customshowroom/trending_showroom_landing",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    NSLog(@"submitDta123 data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isSignApiLoading = NO;
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"Status"]);
         if ([[showroomDict valueForKey:@"Status"]isEqualToString:@"Success"])
         {
            NSLog(@"SHOWROOMS ****** = %@",showroomDict);
             
             if([[showroomDict valueForKey:@"list"] count]>0)
             {
                 if ([signatureShowRoomdata count] && deleteShowroom) {
                     [signatureShowRoomdata removeAllObjects];
                 }
                 
                 for(NSDictionary *showRoom in [showroomDict valueForKey:@"list"])
                 {
                     [signatureShowRoomdata addObject:[[DiscoverDataModel alloc]initWithDiscoverInfo:showRoom ForTrendingShowroom:YES ]];
                 }
                 
                 if ([signatureShowRoomdata count]) {
                     SPHSingletonClass *objectShareClass = [SPHSingletonClass sharedSingletonClass];
                     objectShareClass.arrGlobalSignatureShowroomList = nil;
                     [objectShareClass setArrGlobalSignatureShowroomList:signatureShowRoomdata];
                 }
                 

                 if (pagenumberSignShowroom==1) {
                     [self.DiscoverAllTable reloadData];
                 }
                 else
                     [self reloadTableviewWithsection:0];
             }
             else
             {
//                 isDataFinishedInServer = YES;
             }
         }}];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
        pagenumberSignShowroom = 1;
        [self getSignatureShowroom:pagenumberSignShowroom delete:YES];
}

-(void)deleteShowroomFromList
{
    pagenumberShowroom = 1;
    [ self getShowroomListwithPagenum:pagenumberShowroom];
}

- (void)createRefreshView
{

    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:self.DiscoverAllTable withYPos:10];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - QBRefreshControlDelegate
- (void)refresh:(id)sender
{
    pagenumberShowroom = 1;
    [self getSignatureShowroom:pagenumberSignShowroom delete:YES];
    [ self getShowroomListwithPagenum:pagenumberShowroom];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}


- (void)endRefreshing {
    [refreshCarbon endRefreshing];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)showroomJoinUnjoin:(NSString *)showTagName memberCount:(NSString *)count join:(BOOL)isJoiend
{

    if (allShowRoomdata.count) {
              for (int start = 0; start <allShowRoomdata.count; start++) {
            if ([[[allShowRoomdata objectAtIndex:start]show_tag] isEqualToString:showTagName]) {
                
                ShowRoomModel *model  = [allShowRoomdata objectAtIndex:start];
                model.show_option     = ([model.show_option isEqualToString:@""])?@"FOLLOWER":@"";
                model.member_count     = count;

                model.is_user_joined     = ([model.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
                [allShowRoomdata replaceObjectAtIndex:start withObject:model];
                [self ReloadShowroomCollectionView];
                break;
            }
        }
    }

}

#pragma mark - Server Call
- (void)getShowroomListwithPagenum:(int)pagenum
{
    isLoading = YES;
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&showroom_list_type=4&page_number=%d",GETVALUE(CEO_AccessToken),pagenum];
    [self CallAPIForShowroomsUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@customshowroom/get_all_showrooms_list?%@",ServerUrl,submitData]]];
}
-(void)CallAPIForShowroomsUrl:(NSURL *)myUrl
{
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"Status"]);
         if ([[showroomDict valueForKey:@"Status"]isEqualToString:@"Success"])  {
             isLoading = NO;
             if (pagenumberShowroom == 1) {
                 [allShowRoomdata removeAllObjects];
             }
             
             for(NSDictionary *showRoom in [showroomDict valueForKey:@"list"])
             {
                 [allShowRoomdata addObject:[[ShowRoomModel alloc]initWithShowRoomDict:showRoom ]];
             }
             
             
             if ([[showroomDict valueForKey:@"list"] count]) {
                 [self ReloadShowroomCollectionView];
             }
             if (pagenumberShowroom == 1) {
                 [UserAccessSession InsertShowroomDetailsToDataBase:allShowRoomdata];
             }
             
         }
     }];
}

-(void)ReloadShowroomCollectionView
{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.showRoomCollectionview performBatchUpdates:^{
        [self.showRoomCollectionview reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
            multiShowroomHeight = self.showRoomCollectionview.collectionViewLayout.collectionViewContentSize.height;
            self.showRoomCollectionview.frame = CGRectMake(0, 0, self.view.frame.size.width, multiShowroomHeight);
            
//            [self reloadTableviewWithsection:1];
            [self.DiscoverAllTable reloadData];
        }
    }];
}

-(void)reloadTableviewWithsection:(NSInteger)section
{
    [self.DiscoverAllTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:section], nil]  withRowAnimation:UITableViewRowAnimationNone];
}

//  Outside the implementation block:
- (void)loadNextShowrooms{
    if(!isLoading){
            pagenumberShowroom = pagenumberShowroom+1;
            [self getShowroomListwithPagenum:pagenumberShowroom];
        }
}

- (void)loadNextShowroomPages
{
//    if(!isSignApiLoading){
//    pagenumberSignShowroom = pagenumberSignShowroom+1;
//    [self getSignatureShowroom:pagenumberSignShowroom];
//    }
}

#pragma mark - THIS CODE FOR SHOWROOM COLLECTION VIEW


-(JitenCollectionView *)showroomCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.showRoomCollectionview;
}
- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    return  (ShowRoomModel *)[allShowRoomdata objectAtIndex:row];
}
- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView{
    return allShowRoomdata.count;
}

- (void) JitenCollectionViewDelegateMethod: (ShowRoomModel *) model selectedIndex :(NSInteger) selectedRowNo{
    
   // AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
  //  delegateApp.lbl.hidden = YES;
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.modelShow = model;
    vcShowRoom.delegate = self;
    vcShowRoom.isFromDiscoverShowroom = YES;
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}

-(void)JitenCollectionViewAddClicked:(JitenCollectionView *)sender{
    AppDelegate* delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = YES;
   // delegateApp.lbl.hidden = YES;
    AddEditShowroomVC *samplePopupViewController = [[AddEditShowroomVC alloc] initWithNibName:@"AddEditShowroomVC" bundle:nil];
    samplePopupViewController.aDelegate = self;
    samplePopupViewController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:samplePopupViewController animated:YES];
}

- (void)showroomAddedOrEditedSuccesFully:(NSString *)showroomTag
{
    pagenumberShowroom = 1;
    [self getShowroomListwithPagenum:pagenumberShowroom];
    [self gotoParticularShowroom:showroomTag];
}

-(void)gotoParticularShowroom:(NSString *)selectedEnitity{
    
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = selectedEnitity;
    vcShowRoom.isFromDiscoverShowroom = YES;
    vcShowRoom.delegate = self;

    [self.navigationController pushViewController:vcShowRoom animated:YES];
}

- (void) JitenCollectionViewJoinClicked: (NSInteger) selectedRow
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.isAnyShowroomJoinUnjoin = YES;
    ShowRoomModel *model  = [allShowRoomdata objectAtIndex:selectedRow];
    model.show_option     = ([model.show_option isEqualToString:@""])?@"FOLLOWER":@"";
    model.is_user_joined     = ([model.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
    model.member_count     = [NSString stringWithFormat:@"%d",([model.is_user_joined isEqualToString:@"YES"])?([model.member_count intValue]+1):([model.member_count intValue]-1)];

    [allShowRoomdata replaceObjectAtIndex:selectedRow withObject:model];
    [self ReloadShowroomCollectionView];
}

- (DiscoverViewCellType)DiscoverTableView:(DiscoverTableview *)tableView cellTypeForSection:(NSInteger)section
{
    if (section == 0)
        return DiscoverViewCellTypeSignatureShowroom;
    else
        return DiscoverViewCellTypeMultiShowroom;
}

- ( NSInteger)DiscoverTableViewNumberOfsections:(DiscoverTableview *)tableView
{
    return 2;
}

-(CGFloat)DiscoverTableView:(DiscoverTableview *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0)
    {
        return 150;
    }

    return multiShowroomHeight;
}

-(CGFloat)DiscovertableView:(DiscoverTableview *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section ==0)
        return 25.0;
    else
        return 25.0;
}

- (NSString *)DiscoverTableView:(DiscoverTableview *)tableView TitleOfHeaderForSection:(NSInteger)section
{
    if (section == 0)
        return @"SIGNATURE SHOWROOMS";
    else
        return @"ALL SHOWROOMS";
}

- (NSDictionary *)DiscoverTableView:(DiscoverTableview *)tableView DataForSection:(NSInteger)section
{
    if (section == 0) {
        return [NSDictionary dictionaryWithObjectsAndKeys:signatureShowRoomdata ,@"images", nil];
    }
    return  nil;
}


- (void)LoadNextPagetriggeredForSection:(NSInteger)section{
    [self loadNextShowrooms];
}


- (void)scrollingTableViewDidSelectImageAtIndexPath:(NSIndexPath*)indexPathOfImage Withdata:(DiscoverDataModel*)data atCategoryRowIndex:(NSInteger)categoryRowIndex
{
    MSPSignatureShowroomVC *vcShowRoom = [[MSPSignatureShowroomVC alloc]initWithNibName:@"MSPSignatureShowroomVC" bundle:nil];
    vcShowRoom.modelShow = data;
    vcShowRoom.isFromDiscoverShowroom = YES;
    [self.navigationController pushViewController:vcShowRoom animated:YES];

//    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
//    vcShowRoom.isFromDiscoverShowroom = YES;
//    vcShowRoom.selectedShowroom = data.discoverName;
//    [self.navigationController pushViewController:vcShowRoom animated:YES];
}


- (void)dismissPopup{
    
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
  //  delegateApp.lbl.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
    //    if (self.popupViewController != nil) {
    //        [self dismissPopupViewControllerAnimated:YES completion:^{
    //            NSLog(@"popup view dismissed");
    //        }];
    //    }
}

@end
