

#import "UITextField+Padding.h"

@implementation UITextField (Padding)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

- (CGRect)textRectForBounds:(CGRect)bounds {
    AppDelegate *del = MINTSHOWAPPDELEGATE;
    return CGRectMake(bounds.origin.x + 8, bounds.origin.y + 8,
                      bounds.size.width - ((del.isGlobalSearch)?30:20), bounds.size.height - 16);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
//    self.autocorrectionType = UITextAutocorrectionTypeNo;
    return [self textRectForBounds:bounds];
}


///*< Place holder position >*/
//- (CGRect)textRectForBounds:(CGRect)bounds {
//    
//    bounds.size.width = bounds.size.width - 20.0f;
//    return bounds;
//}
//
///*< Text Posiotion >*/
//- (CGRect)editingRectForBounds:(CGRect)bounds {
//    
//    bounds.size.width = bounds.size.width - 20.0f;
//    return bounds;
//}


// do your override
#pragma clang diagnostic pop

@end

