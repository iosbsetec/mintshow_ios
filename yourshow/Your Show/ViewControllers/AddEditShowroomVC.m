//
//  AddEditShowroomVC.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 04/01/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS      @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
#define kPlaceHolderText           @"Describe your Showroom and the type of Mints you want posted."
#define KprivacyOFF                @"Currently, this showroom is viewable to anyone."
#define KprivacyON                 @"This showroom is hidden and Mints are only posted and viewed by approved members."
#define KprivacyOFFAnyone          @"Currently, this showroom is viewable to anyone."
#define KprivacyOFFMemeberOnly     @"Currently, this showroom is viewable to members only."
#define KprivacyOFFNoOne           @"Currently, this showroom is viewable to noone."


#import "AddEditShowroomVC.h"
#import "FilterListTable.h"
#import "InviteFriendsV.h"
#import "KeywordVC.h"
#import "PhotoGallery.h"
#import "InviteFriendGroups.h"
#import "RoundedRect.h"
#import "MintShowroomViewController.h"

#import "ImageListViewController.h"
#import "CropImageVC.h"


@interface AddEditShowroomVC () <FilterListTableDelegate,InviteFriendsVTableDelegate,KeyWordDelegate,PhotoGalleryDelegate,CropImageDelegate,UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,NewPhotoGalleryDelegate>

{
    
    InviteFriendGroups *obj;

    
    
    IBOutlet UILabel *btnCategoryCancel;
    __weak IBOutlet UIImageView *imgVSugOne;
    __weak IBOutlet UIImageView *imgVSugTwo;
    __weak IBOutlet UIImageView *imgVSugThree;
    __weak IBOutlet UILabel *lblSugOne;
    __weak IBOutlet UILabel *lblSugTwo;
    __weak IBOutlet UILabel *lblSugThree;
    __weak IBOutlet UILabel *lblPrivacyInfo;
    __weak IBOutlet RoundedRect *SUGGESTIONlIST;
    
    __weak IBOutlet UILabel        *lblPrivacySeparator;
    
    IBOutlet UITableViewCell *cellShowroomImage;
    IBOutlet UITableViewCell *cellShowroomName;
    IBOutlet UITableViewCell *cellShowTagInfo;
    IBOutlet UITableViewCell *cellShowroomDescription;
    IBOutlet UITableViewCell *cellCategory;
    IBOutlet UITableViewCell *cellKeyword;
    IBOutlet UITableViewCell *cellColaborator;
    IBOutlet UITableViewCell *cellPrivacySwitch;
    IBOutlet UITableViewCell *cellFriendInvite;
    IBOutlet UITableViewCell *cellDeleteShowroom;

    
    IBOutlet UITableView *tblVwShowroomInfo;

    __weak IBOutlet UIButton *btnEditCoverImage;
    IBOutlet UIImageView *imgVShowroomBg;

    IBOutlet UITextField *txtFldShowroomName,*txtFldShowTag;
    IBOutlet UITextView  *txtVWShowroomDescription;
  
    __weak IBOutlet UIView *viewCategoryInfo;
    IBOutlet UIView          *viewRadioButtons;
    IBOutlet UIView *viewTokenHolder;
    __weak IBOutlet UIView *viewAddimage;

    __weak IBOutlet UIButton *btnPrivacy;
    __weak IBOutlet UIButton *btnAnyOne;
    __weak IBOutlet UIButton *btnMembersOnly;
    __weak IBOutlet UIButton *btnNoOne;

    UIImage *finalImage ;


    IBOutlet UILabel *lblToken1;
    IBOutlet UILabel *lblToken2;
    IBOutlet UILabel *lblVCTitle;
    IBOutlet UILabel     *lblCategory;
    IBOutlet UILabel     *lblEditCategory;

    IBOutlet UILabel     *lblDescTxtCount,*lblShworoomnameCount,*lblShowtagCount;

    
    NSString *memberList;
    NSString *cateGoryId;
    NSString *Keywords;
    NSString *stringImage;


    BOOL isCellOpen,isSujestNeedToshow;
    BOOL privacySettings;
    NSInteger  PrivacyType;
    
    PhotoGallery    *photoGallery;
    FilterListTable *filterTableView;
    InviteFriendsV  *inviteFriends;
    IBOutlet UIImageView *imgDots;

    NSString *strServerImagePath;
    NSMutableArray *arrayExistingKeywords;
    
    
    IBOutlet UILabel *lblShowRoomTitle;
    IBOutlet UILabel *lblShowRoomName;
    
    IBOutlet UIImageView    *imgStarTag1;
    IBOutlet UIImageView    *imgStarTag2;
    IBOutlet UILabel        *lblShowTagName;
    IBOutlet UILabel        *lblShowTagTitle;
    BOOL isShowTagEditStart;
    
    UITapGestureRecognizer *aTapGuesture;
    
    NSInteger inviteFriendsCount;

}
- (IBAction)editCoverImageTapped:(UIButton *)sender;
- (IBAction)privacyOnOff:(UIButton *)sender;
- (IBAction)radioButtonTapped:(UIButton *)sender;
- (IBAction)suggestionClicked:(UIButton *)sender;

@end

@implementation AddEditShowroomVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [tblVwShowroomInfo reloadData];
    // Do any additional setup after loading the view from its nib.
    isSujestNeedToshow = NO;
    //viewRadioButtons.hidden  =YES;
    isCellOpen = YES;
    [btnAnyOne setSelected:YES];
    [self radioButtonTapped:btnAnyOne];
    SUGGESTIONlIST.layer.borderWidth = 1.5;
    SUGGESTIONlIST.layer.borderColor = [UIColor lightGrayColor].CGColor;
    arrayExistingKeywords = [NSMutableArray array];

    [imgStarTag1 setHidden:YES];
    [imgStarTag2 setHidden:YES];
    lblEditCategory.hidden = YES;
    btnEditCoverImage.alpha = 0.0;

    if (_isFromShowRoomVC)
    {
        btnEditCoverImage.alpha = 1.0;
        [self setEditShowroomDatas:_modelShow];
    }
      tblVwShowroomInfo.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

}


- (void)setEditShowroomDatas:(ShowRoomModel *)models
{
    txtVWShowroomDescription.textColor = [UIColor blackColor];
    lblVCTitle.text         =  @"EDIT SHOWROOM";
    viewAddimage.hidden     = YES;
    imgVShowroomBg.mintImageURL =  [NSURL URLWithString:models.showroom_img_Url];
    txtVWShowroomDescription.text     =  models.showroom_description;
    lblDescTxtCount.hidden = YES;
    lblShowRoomName.text =  models.showroom_name;
    lblShowTagName.text  =  models.show_tag;
    lblEditCategory.hidden = NO;

    [self tagShowRoomText:models.show_tag];
    
    lblShowTagName.textColor = [UIColor lightGrayColor];
    viewCategoryInfo.hidden = YES;
    [lblShworoomnameCount setHidden:YES];
    
    lblShowtagCount.text    =  [NSString stringWithFormat:@"%d",50- (int)txtFldShowTag.text.length];
    lblShworoomnameCount.text    =  [NSString stringWithFormat:@"%d",50- (int)txtFldShowroomName.text.length];
    lblDescTxtCount.text    =  [NSString stringWithFormat:@"%d",200- (int)txtVWShowroomDescription.text.length];
    lblEditCategory.text        =  models.showroom_categoryName;
    if ([models.showroom_tag_keywords_ids length]) {
        NSArray *listItems = [models.showroom_tag_keywords_ids componentsSeparatedByString:@","];
        [self updateAddShowRoomKeyword:listItems];
    }
    cateGoryId = models.showroom_category_ids;

    [txtFldShowTag setUserInteractionEnabled:NO];
   
    if ([models.privacy_settings isEqualToString:@"off"]) {
        [btnPrivacy setSelected:YES];
        isCellOpen = NO;
        
        if ([models.showroom_type isEqualToString:@"noone"]) {
            [self radioButtonTapped:btnNoOne];

        }
        if ([models.showroom_type isEqualToString:@"memb_only"]) {
            [self radioButtonTapped:btnMembersOnly];
        }
    }
    else
    {
        [btnPrivacy setSelected:NO];
        isCellOpen = YES;

    }
    [self privacyOnOff:btnPrivacy];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)imageCropperDidCancel:(CropImageVC *)cropperViewController btnTag:(UIButton *)btnTag
{
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        if(btnTag.tag==111)
        {
            [self addCoverImage:nil];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 8;
}
-(UIView *)creatorDevider
{
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 310, 1)];/// change size as you need.
    separatorLineView.backgroundColor = [UIColor lightGrayColor];// you can also put image here
    return separatorLineView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    switch (indexPath.row)
    {
        case 0:
            return 120;
            break;
        case 1: case 4: case 5:
            return 35;
            break;
        case 2:
            return (isSujestNeedToshow)?135:35;
            break;
        case 3:
            return 80;
            break;
        case 6:
            return (isCellOpen)?105:55;
            break;
        case 7:
            return 40;
            break;
        default:
            break;
    }

    return 0.0;

}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
            return cellShowroomImage;
            break;
 
        case 1:
            //[cellShowroomName addSubview:[self creatorDevider]];
            return cellShowroomName;
            break;
        case 2:
            [cellShowTagInfo addSubview:[self creatorDevider]];
            return cellShowTagInfo;
            break;
        case 3:
            [cellShowroomDescription addSubview:[self creatorDevider]];
            return cellShowroomDescription;
            break;
        case 4:
            [cellCategory addSubview:[self creatorDevider]];
            return cellCategory;
            break;
        case 5:
            [cellKeyword addSubview:[self creatorDevider]];
            return cellKeyword;
            break;
        case 6:
        {
            [cellPrivacySwitch addSubview:[self creatorDevider]];
            return cellPrivacySwitch;
            break;
        }
        
        case 7:
            if ([lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"]) {
                [cellDeleteShowroom addSubview:[self creatorDevider]];
                return cellDeleteShowroom;
            }
            [cellFriendInvite addSubview:[self creatorDevider]];
            return cellFriendInvite;
            break;
            
        default:
            break;
    }
    
    return nil;

}
/*        case 6:
 [cellColaborator addSubview:[self creatorDevider]];
 return cellColaborator;
 break;
 */
// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==  0) {
        [self.view endEditing:YES];
        [self addCoverImage:nil];
    }
    
    if (indexPath.row == 4) {
        if (![lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"]) {
        [self btnCategoryTapped:nil];
        }
    }
    if (indexPath.row == 7) {
            [self inviteFriendTapped:nil];
    }
    if (indexPath.row == 5) {
        [self btnAddKeyWordTapped:nil];
        [tblVwShowroomInfo reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:5 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
   
}
- (IBAction)cancelShowroom:(id)sender
{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
    //delegateApp.lbl.hidden = NO;
    [self.navigationController popViewControllerAnimated:NO];
    
}
-(void)createKeywordFromShowroomName
{

    NSMutableArray *arrayTemp   = [NSMutableArray array];
    if (lblShowRoomName.text.length) {
        NSString *showroom =    [lblShowRoomName.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        NSArray *arr = [showroom componentsSeparatedByString:@" "];

        for (NSString *keyWord in arr) {
              if (![arrayExistingKeywords containsObject:keyWord]) {
                // ...
                  NSLog(@"The array contain thsi object %@",keyWord);
                  [arrayTemp addObject:keyWord];
            }
        }
    }
    
    
    if ([arrayTemp count]) {
        NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                               NSMakeRange(0,[arrayTemp count])];
        [arrayExistingKeywords insertObjects:arrayTemp atIndexes:indexes];
        [self updateAddShowRoomKeyword:[NSArray arrayWithArray:arrayExistingKeywords]];
    }
   
    

}
- (IBAction)editCoverImageTapped:(UIButton *)sender {
    [self addCoverImage:nil];

}

- (IBAction)privacyOnOff:(UIButton *)sender
{
    [self tagFieldInNormalMode];
    if (![btnPrivacy isSelected]) {
        [self radioButtonTapped:btnAnyOne];
        [btnPrivacy setSelected:YES];
        privacySettings = YES;
        lblPrivacyInfo.text = KprivacyON;
        [lblPrivacySeparator setAlpha:0.0];
    }
    else
    {
        [btnPrivacy setSelected:NO];
        privacySettings = NO;
        lblPrivacyInfo.text = KprivacyOFF;
        [lblPrivacySeparator setAlpha:1.0];
    }
    isCellOpen = (isCellOpen)?NO:YES;
    [tblVwShowroomInfo reloadData];
}

-(NSInteger)noOfRows
{
    if (privacySettings == YES)
        return 0;
    else
        return inviteFriends.arrayFriendList.count;
}


- (IBAction)radioButtonTapped:(UIButton *)sender {
    [self performSelector:@selector(deselectAll)];
    [((UIButton *) sender) setSelected:YES];
    PrivacyType = sender.tag;
   }

- (IBAction)suggestionClicked:(UIButton *)sender {
    
    switch (sender.tag) {
        case 1:
            imgVSugOne.image = [UIImage imageNamed:@"RadiobButton_On"];
            [self tagShowRoomText:lblSugOne.text];
        
            break;
        case 2:
            imgVSugTwo.image = [UIImage imageNamed:@"RadiobButton_On"];
            [self tagShowRoomText:lblSugTwo.text];
            break;
        case 3:
            imgVSugThree.image = [UIImage imageNamed:@"RadiobButton_On"];
    [self tagShowRoomText:lblSugThree.text];
            break;
            
        default:
            break;
    }
    lblShowtagCount.text = [NSString stringWithFormat:@"%d",50- (int)txtFldShowTag.text.length];
    [self performSelector:@selector(tableRefresh) withObject:self afterDelay:0.7];
    [self hideSuggestionList];

}
-(void)tableRefresh
{
    imgVSugOne.image   = [UIImage imageNamed:@"RadiobButton_Off"];
    imgVSugTwo.image   = [UIImage imageNamed:@"RadiobButton_Off"];
    imgVSugThree.image = [UIImage imageNamed:@"RadiobButton_Off"];
}
- (void) deselectAll
{
    NSArray *views = [viewRadioButtons subviews];
    for (UIView *v in views)
    {
        if ([v isKindOfClass:[UIButton class]])
        {
            [((UIButton *)v) setSelected:NO];
 
        }
    }
}

#pragma mark - ******* Description Handler **********
-(void)textViewDidBeginEditing:(UITextView *)textView
{

    
    [lblDescTxtCount setHidden:NO];
    if ([txtVWShowroomDescription.text isEqualToString:kPlaceHolderText]) {
        txtVWShowroomDescription.text = @"";
        txtVWShowroomDescription.textColor = [UIColor darkGrayColor]; //optional
    }

    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [lblDescTxtCount setHidden:YES];
    if ([txtVWShowroomDescription.text isEqualToString:@""]) {
        txtVWShowroomDescription.text = kPlaceHolderText;
        txtVWShowroomDescription.textColor = [UIColor lightGrayColor]; //optional
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    [self tagFieldInNormalMode];

    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [tblVwShowroomInfo scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    if (textView == txtVWShowroomDescription)
    {
        isShowTagEditStart = NO;
        [self tagFieldInNormalMode];

        txtVWShowroomDescription.backgroundColor = SHOWROOMBACKGROUNDCOLOR;
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if (textView == txtVWShowroomDescription)
    {
        txtVWShowroomDescription.backgroundColor = [UIColor clearColor];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (range.location == 0 && [string isEqualToString:@" "])
    {
        return NO;
    }

    if ([string isEqualToString:@""])
    {
        return YES;
    }
    if ([string isEqualToString:@" "] && textField == txtFldShowTag)
    {
        return NO;
    }
    if (textField.text.length>=50)
    {
        [textField resignFirstResponder];
        return NO;
    }
    if (textField == txtFldShowTag) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }

    return YES;
}

-(BOOL)isHaveSpecialChar:(NSString*)str{
    NSString *customStr = @"~`.|!@#$%^&*()+=-/;:\"\'{}[]<>^?,";
    NSCharacterSet *alphaSet = [NSCharacterSet characterSetWithCharactersInString:customStr];
    BOOL isHaveSpecialChar = [[str stringByTrimmingCharactersInSet:alphaSet] isEqualToString:@""];
    return isHaveSpecialChar;
}

-(IBAction)searchFriends : (UITextField *) txtField
{
    NSLog(@"Final text %@",[txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]);
//    txtFldShowTag.text =  [txtField.text stringByReplacingOccurrencesOfString:@" " withString:@""];

    if (txtField == txtFldShowTag)
    {
        lblShowTagName.text = (!_isFromShowRoomVC)?txtFldShowTag.text:lblShowTagName.text;
    }
    
    if (txtField == txtFldShowroomName) {
        lblShworoomnameCount.text = [NSString stringWithFormat:@"%d",50- (int)txtFldShowroomName.text.length];
        if (![lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"])
            lblShowTagName.text = [self formatIdentificationNumber:[txtField.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    if (![lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"]) {
        txtFldShowTag.text =  [self formatIdentificationNumber:[txtField.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
        lblShowtagCount.text = [NSString stringWithFormat:@"%d",50- (int)txtFldShowTag.text.length];
    }

    if (isSujestNeedToshow) {
        isSujestNeedToshow = NO;
        [tblVwShowroomInfo reloadData];
    }
}

-(void)tagShowRoomText:(NSString *)tagText
{
    if (tagText.length)
    {
        NSAttributedString *labelAttributes = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[self formatIdentificationNumber:[tagText stringByReplacingOccurrencesOfString:@" " withString:@""]]]];
        
        UIImage *img = [UIImage imageNamed:(!_isFromShowRoomVC)?@"star_orange_new.png":@"star_grey_new"];
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
        textAttachment.image = img;
        NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
        [attrStr appendAttributedString:attrStringWithImage];
        [attrStr appendAttributedString:labelAttributes];
        lblShowTagName.attributedText = attrStr;
    }
}

-(void)tagFieldInEditMode
{
    [lblShworoomnameCount setHidden:NO];
    txtFldShowroomName.text = lblShowRoomName.text;
    [lblShowRoomTitle setHidden:YES];
    [lblShowRoomName setHidden:YES];
    
    if (!_isFromShowRoomVC) {
        txtFldShowTag.text = lblShowTagName.text;
        [lblShowTagName setHidden:YES];
        [lblShowTagTitle setHidden:YES];
        [lblShowtagCount setHidden:NO];
    }
}

-(void)onTagFieldEditMode
{
    if (lblShowTagName.text.length)
    {
        txtFldShowTag.text = lblShowTagName.text;
    }

    [lblShowtagCount setHidden:NO];
    [lblShowTagName setHidden:YES];
    [lblShowTagTitle setHidden:YES];
}

-(void)tagFieldInNormalMode
{
    if (!isShowTagEditStart)
    {
        if (txtFldShowTag.text.length)
        {
            [self tagShowRoomText:txtFldShowTag.text];
        }
        
        [lblShowtagCount setHidden:(lblShowTagName.text.length)?YES:NO];
        [lblShowTagName setHidden:NO];
        [lblShowTagTitle setHidden:NO];
        
        txtFldShowTag.text= @"";
        if (lblShowRoomName.text.length) {
            txtFldShowroomName.placeholder = @"";
        }
        txtFldShowTag.placeholder = @"";
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (!_isFromShowRoomVC)
        txtFldShowTag.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Showtag" attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];

    if (textField == txtFldShowroomName)
    {
        [textField setTextColor:[UIColor lightGrayColor]];
        
        txtFldShowroomName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Showroom Name" attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
        

        if (isSujestNeedToshow)
            [self hideSuggestionList];
        
        [self tagFieldInEditMode];
    }
    else if(textField == txtFldShowTag)
    {
        [textField setTextColor:ORANGECOLOR];
 //       txtFldShowroomName.placeholder = @"";

        [self onTagFieldEditMode];
    }
    isShowTagEditStart = YES;

    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField == txtFldShowroomName)
    {

        lblShowRoomName.text = txtFldShowroomName.text;
        if (![lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"])
            [self tagShowRoomText:txtFldShowroomName.text];
        [lblShowRoomTitle setHidden:NO];
        [lblShowRoomName setHidden:NO];
        txtFldShowroomName.text= @"";
        txtFldShowroomName.placeholder = @"";
        if (lblShowRoomName.text.length>0)
        {
            [lblShworoomnameCount setHidden:YES];
        }
        [self tagFieldInNormalMode];
    }
    else if(textField == txtFldShowTag)
    {
        isShowTagEditStart = NO;
        
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self createKeywordFromShowroomName];
    if (lblShowTagName.text.length>2 && ![lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"])
    {
        [self checkShowTag:lblShowTagName.text];
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    lblDescTxtCount.text = [NSString stringWithFormat:@"%d",200-(int)textView.text.length];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    if ([text isEqualToString: @""])
    {
        return YES;
    }
    if (textView.text.length>=200)
    {
        [textView resignFirstResponder];
        return NO;
    }
    if(newLength <= 200)
    {
        return YES;
    } else {
        
        NSUInteger emptySpace = 200 - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
    
    return YES;
}
#pragma mark - *******          End        **********


- (IBAction)btnCategoryTapped:(id)sender;
{
    if (isSujestNeedToshow) {
        [self hideSuggestionList];
    }
    [self.view endEditing:YES];
    [self tagFieldInNormalMode];

    if (txtFldShowTag.text.length>2) {
        [self checkShowTag:txtFldShowTag.text];
    }

    filterTableView = [[FilterListTable alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds withTitle:@"Add Category" isCateGoryView:YES withPreviousSelection:lblCategory.text cateGoryId:cateGoryId showcategoryList:YES];
    filterTableView.delegate = self;
}
- (void) FilterListTableDelegateMethod: (NSString *) textData selectedId :(NSString *) iddee
{
    CGRect frameCategory   = viewCategoryInfo.frame;
    float width            = [self getDescriptionHeight:textData];
    frameCategory.origin.x = self.view.frame.size.width- width-25;
    frameCategory.size.width = width+20;
    viewCategoryInfo.frame = frameCategory;
    
    CGRect frameLblCategory   = lblCategory.frame;
    frameLblCategory.origin.x = 5;
    frameLblCategory.size.width = width;
    [lblCategory setFrame:frameLblCategory];
    
    CGRect frameBtnClose   = btnCategoryCancel.frame;
    frameBtnClose.origin.x = viewCategoryInfo.frame.size.width-13;
    [btnCategoryCancel setFrame:frameBtnClose];
    
    viewCategoryInfo.hidden = NO;
    cateGoryId              = iddee;
    lblCategory.text        = textData;
    
    [filterTableView hideDropDown];
    filterTableView =nil;


}
-(CGFloat)getDescriptionHeight:(NSString *) descriptiontext
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    CGSize textSize = [descriptiontext sizeWithAttributes: userAttributes];
    
    return  textSize.width;
}


- (IBAction)inviteFriendTapped:(UIButton *)sender {
    if (isSujestNeedToshow) {
        [self hideSuggestionList];
    }
    [self.view endEditing:YES];
    [self tagFieldInNormalMode];

    if (inviteFriends != nil)
    {
        inviteFriends = nil;
        
    }
    inviteFriends = [[InviteFriendsV alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds withTitle:@"InviteFriends" isCateGoryView:YES selectdList:nil];
    inviteFriends.delegate = self;
    
    
}
- (void) friendsSelectdForInvite:(NSArray *)memberListVal
{
    [inviteFriends removeFromSuperview];
    inviteFriends = nil;
    if ([obj superview]) {
        [obj removeFromSuperview];
    }
    obj = [[InviteFriendGroups alloc]initWithFrame:CGRectMake(([memberListVal count] >5)?140:180,4, ([memberListVal count] >5)?145:110, CGRectGetHeight(cellFriendInvite.frame)-5) withArray:memberListVal];
    obj.backgroundColor = [UIColor whiteColor];
   [cellFriendInvite addSubview:obj];
    
    NSMutableArray *arrFriends = [[NSMutableArray alloc]init];
    
    for (FriendsInfo *obj1 in memberListVal) {
        [arrFriends addObject:obj1.id_str];
        memberList  = [YSSupport convertToCommaSeparatedFromArray:arrFriends];

    }
    inviteFriendsCount = [arrFriends count];
    NSLog(@"selected Friends %@",memberList);
    
}



#pragma mark _______________________________
#pragma mark  - ********** KeyWords Implementation Start **********
#pragma mark _______________________________

- (IBAction)btnAddKeyWordTapped:(UIButton *)sender
{
    if (isSujestNeedToshow) {
        [self hideSuggestionList];
    }
    [self.view endEditing:YES];
    [self tagFieldInNormalMode];

    KeywordVC *viewKW = [[KeywordVC alloc]initWithNibName:@"KeywordVC" bundle:nil];
    viewKW.keyDelegate = self;
    viewKW.arrayKeywords  = [NSArray arrayWithArray:arrayExistingKeywords];
    [self.navigationController pushViewController:viewKW animated:YES];
    
}

-(void)updateAddShowRoomKeyword:(NSArray *)arrKeywords
{
    viewTokenHolder.hidden = YES;
    [arrayExistingKeywords removeAllObjects];
    [arrayExistingKeywords addObjectsFromArray:arrKeywords];
    imgDots.hidden = (arrayExistingKeywords.count>2)?NO:YES;

    if (arrayExistingKeywords.count>0)// && arrKeywords.count>2)
    {
        viewTokenHolder.hidden = NO;
        
        if ([arrKeywords count] == 1) {
            [self displayViewsWithTokens:[arrayExistingKeywords objectAtIndex:0] token2:nil count:1];
        }
        else
            [self displayViewsWithTokens:[arrayExistingKeywords objectAtIndex:0] token2:[arrayExistingKeywords objectAtIndex:1] count:2];
        
        Keywords =  [YSSupport convertToCommaSeparatedFromArray:arrayExistingKeywords];
    }
    
}

- (void) displayViewsWithTokens : (NSString *)textval token2:(NSString *)textval2 count:(int)num
{

    switch (num) {
        case 1:
            lblToken1.text = textval;
            [[viewTokenHolder viewWithTag:2] setHidden:NO];
            [[viewTokenHolder viewWithTag:2] setHidden:YES];

            break;
        case 2:
        {
            [[viewTokenHolder viewWithTag:2] setHidden:NO];
            [[viewTokenHolder viewWithTag:2] setHidden:NO];
            lblToken1.text = textval;
            lblToken2.text = textval2;
        }
            break;
            
        default:
            break;
    }
    
}


-(NSString *) formatIdentificationNumber:(NSString *)string
{
   // NSCharacterSet * invalidNumberSet = [NSCharacterSet characterSetWithCharactersInString:@"\n_!@#₹$%^&*€£¥()[]{}'\".,<>:;|\\/?+=\t~-` "];
    NSCharacterSet * invalidNumberSet = [NSCharacterSet characterSetWithCharactersInString:@"\n_!@#₹$%^&*€£¥•()[]{}'\".,<>:;|\\/?+=\t~-` "];
                                         
                                         NSString * result = @"";
                                         NSScanner * scanner = [NSScanner scannerWithString:string];
                                         NSString * scannerResult;
                                         
                                         [scanner setCharactersToBeSkipped:nil];
                                         
                                         while (![scanner isAtEnd])
    {
        if([scanner scanUpToCharactersFromSet:invalidNumberSet intoString:&scannerResult])
        {
            result = [result stringByAppendingString:scannerResult];
        }
        else
        {
            if(![scanner isAtEnd])
            {
                [scanner setScanLocation:[scanner scanLocation]+1];
            }
        }
    }
                                         
                                         return result;
    
}
- (IBAction)dismissThisPopup:(id)sender
{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
   // delegateApp.lbl.hidden = NO;
    [self.navigationController popViewControllerAnimated:NO];
    
}


- (void) displayContentController: (UIViewController*) content;
{
    [self addChildViewController:content];
    [content willMoveToParentViewController:self];
    [content didMoveToParentViewController:self];
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
}
- (IBAction)addCoverImage:(id)sender {
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
    ImageListViewController *ilist = [[ImageListViewController alloc]initWithNibName:@"ImageListViewController" bundle:nil];
    
    ilist.isShowCameraButton = NO;
    ilist.isMediaTypeImage = YES;
    ilist.delegate = self;
    [self presentViewController:ilist animated:YES completion:^{
        // Do some stuffs
    }];
    }
    else if (status == PHAuthorizationStatusDenied) {
        // Access has been denied.
        
        //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"app-settings:"]];
        [self cancelPhotoGallery];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Allow Access to your Photos"
                                      message:@"This allows MintShow to access photos from your library."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [alert.view setTintColor:[UIColor blackColor]];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Access"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"app-settings:"]];
                                 AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                                 [appDell authenticationDidFinish:self selectedInde:0];
                                 [alert dismissViewControllerAnimated:YES completion:nil];

                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Denie"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }

    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
            }
            
            else {
                // Access has been denied.
            }
        }];
    }
    
}
-(void)selectedImageFromGallery:(UIImage *)selectedImage;
{
    CropImageVC *imgCropperVC = [[CropImageVC alloc] initWithImage:selectedImage cropFrame:CGRectMake(0, (CGRectGetHeight(self.view.frame)/2)-(self.view.frame.size.width/3.0)/2, self.view.frame.size.width, self.view.frame.size.width/3.0) limitScaleRatio:3.0];
    
    imgCropperVC.delegate = self;
    [[self navigationController] presentViewController:imgCropperVC animated:YES completion:^{
        // TO DO
        
    }];
}


#pragma mark CropImageDelegate
- (void)imageCropper:(CropImageVC *)cropperViewController didFinished:(UIImage *)editedImage {
    [self onSetImageFromImgPicker:editedImage];
    [self cancelPhotoGallery];
}

//- (void)imageCropperDidCancel:(CropImageVC *)cropperViewController {
//    [cropperViewController dismissViewControllerAnimated:YES completion:^{
//    }];
//}

-(void)cancelPhotoGallery{
    if (photoGallery != nil)
    {
        [photoGallery.view removeFromSuperview];
        photoGallery = nil;
    }
}

-(void)onSetImageFromImgPicker:(UIImage *)pickerImg
{
    viewAddimage.hidden = YES;
    finalImage = pickerImg;
    imgVShowroomBg.image  = pickerImg;
    [self uploadSelectedImage];
    btnEditCoverImage.alpha = 1.0;
}

-(void)postImage:(NSData*)imagedata
{
    [self postDataToURl:imagedata postUrl:ImagePostURL ContentDisposition:ImageFilePath];
}

-(void)postDataToURl:(NSData *)Data postUrl:(NSString *)postUrl ContentDisposition:(NSString *)ContentDisposition
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURL *myURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/successprofile/ajax_successprofile.php?page=showroom",ServerWebUrl]];
    NSMutableURLRequest *uploadRequest = [[NSMutableURLRequest alloc] initWithURL:myURL cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    [uploadRequest setHTTPMethod:@"POST"];
    NSString *boundary = [NSString stringWithFormat:@"---------------------------14737809831466499882746641449"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [uploadRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",ContentDisposition] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:Data]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    
    [uploadRequest setHTTPBody:body];
    
    
    [NSURLConnection sendAsynchronousRequest:uploadRequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];

         if (data != nil) {
       
         id jsonObjectData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
         
         NSLog(@"Upload status=%@",jsonObjectData);
    
         if([[jsonObjectData objectForKey:@"status"] boolValue])
         {
             [self.view endEditing:YES];
             [self tagFieldInNormalMode];

             // true Fals
             stringImage = [jsonObjectData objectForKey:@"image_url"];
             //[self createShowrroomSuccses:YES];
             
         }
         }
     }];
}
-(void)removeAlert:(UIAlertController *) alert{
    [alert dismissViewControllerAnimated:YES
                              completion:^{
                                  
                              }];
}
-(void)createAlertView : (NSString *) string
{
//    UIAlertView *av = [[UIAlertView alloc]initWithTitle:nil message:string delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
//    [av show];
//    
//    [self performSelector:@selector(removeAlert:) withObject:av afterDelay:0.9];
//    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, alert.view.frame.size.width-50, 40)];
    fromLabel.text = string;
    fromLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12]; //custom font
    fromLabel.numberOfLines = 1;
    fromLabel.baselineAdjustment = YES;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = ORANGECOLOR;
    fromLabel.textAlignment = NSTextAlignmentCenter;
        [alert.view addSubview:fromLabel];
    
    [self presentViewController:alert animated:YES completion:nil];
    [self performSelector:@selector(removeAlert:) withObject:alert afterDelay:0.9];
}

- (IBAction)btncreateaction:(id)sender {

    [self.view endEditing:YES];
    if (imgVShowroomBg.image == nil)
    {
        [self createAlertView:@"Please add Showroom Image"];
    }

    else if (lblShowRoomName.text.length <3) {
        [self createAlertView:@"Showroom name should be minimum 3 letters"];
    }
    else if (lblShowTagName.text.length <3) {
        [self createAlertView:@"ShowTag name should be minimum 3 letters"];
    }
    else if ([txtVWShowroomDescription.text length] < 25 || [txtVWShowroomDescription.text isEqualToString:@"Describe your Showroom and the type of Mints you want posted."]) {
        [self createAlertView:@"Description should atleast have 25 letters"];
    }
    
    else if (![cateGoryId length] )  {
        [self createAlertView:@"Please enter a category."];
    }

    
    else{
        [self.view endEditing:YES];
        [self tagFieldInNormalMode];

        if (finalImage == nil && [lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"]) {
            stringImage = nil;
            if ([txtFldShowroomName.text length]) {
                lblShowRoomName.text =txtFldShowroomName.text;
            }
            [self createShowrroomSuccses:NO];

        }
        else
        [self createShowrroomSuccses:YES];

    }
}
-(void)uploadSelectedImage
{

    CGSize imageSize = finalImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    NSData *imageData = nil;
    if (width<300 || height < 100) {
        UIImage *img = [self imageResize:finalImage andResizeToWidth:300];
        imageData = UIImagePNGRepresentation(img);
    }
    else
        imageData = UIImagePNGRepresentation(finalImage);
    
    
    [self postImage:imageData];

}
-(UIImage *)imageResize :(UIImage*)img andResizeToWidth:(CGFloat)newWidth{
    CGFloat newheight=(img.size.height/img.size.width)*newWidth;
    CGFloat scale = [[UIScreen mainScreen]scale];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, newheight), NO, scale);
    [img drawInRect:CGRectMake(0,0,newWidth,newheight)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(void)getAttributedString
{
    NSMutableAttributedString *res = [lblShowTagName.attributedText mutableCopy];
    
    [res beginEditing];
    __block BOOL found = NO;
    [res enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, res.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
       
        if (value) {
            UIFont *oldFont = (UIFont *)value;
            UIFont *newFont = [oldFont fontWithSize:oldFont.pointSize * 2];
            [res removeAttribute:NSFontAttributeName range:range];
            [res addAttribute:NSFontAttributeName value:newFont range:range];
            found = YES;
        }
        
    }];
    if (!found) {
        // No font was found - do something else?
    }
    [res endEditing];
    lblShowTagName.attributedText = res;
}
- (void)createShowrroomSuccses : (BOOL)isAddShowroom
{
    /*
     * Privacy - on/off (0 => off and 1 => on) !!
     * Privacy type (0 => Any one and 1 => Member only and 2 => No one)
     * Showroom Id (0 => Add showroom) By default and Edit showrorm Means have to pass ShowroomId
     */
//    [self getAttributedString];
 NSString *showTagText =[lblShowTagName.text stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
    
    NSData *submitData = nil;
    if (!isAddShowroom && [lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"]) {
        submitData = [[NSString stringWithFormat:@"access_token=%@&showroom_id=%@&name=%@&shtag=%@&description=%@&category_ids=%@&tag_keywords_ids=%@&video_path=%@&privacy=%@&privacy_type=%@&membersList=%@",
                       GETVALUE(CEO_AccessToken),(![lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"])?@"0":_modelShow.showroom_id,lblShowRoomName.text,showTagText,txtVWShowroomDescription.text,cateGoryId,Keywords,@"",(privacySettings)?@"1":@"0",[NSString stringWithFormat:@"%ld",(long)PrivacyType],(memberList.length)?memberList:@""]dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    else
    {
        submitData = [[NSString stringWithFormat:@"access_token=%@&showroom_id=%@&name=%@&shtag=%@&description=%@&category_ids=%@&tag_keywords_ids=%@&image_path=%@&is_image_video=%@&video_path=%@&privacy=%@&privacy_type=%@&membersList=%@",
                       GETVALUE(CEO_AccessToken),(![lblVCTitle.text isEqualToString:@"EDIT SHOWROOM"])?@"0":_modelShow.showroom_id,lblShowRoomName.text,showTagText,txtVWShowroomDescription.text,cateGoryId,Keywords,stringImage,@"1",@"",(privacySettings)?@"1":@"0",[NSString stringWithFormat:@"%ld",(long)PrivacyType] ,(memberList.length)?memberList:@""]dataUsingEncoding:NSUTF8StringEncoding];
    }
    NSString *str = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"str = %@",str);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    
    [YSAPICalls AddShowroomWithData:submitData ForSuccessionBlock:^(id newResponse) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MSMixPanelTrackings trackInviteFriendForUser:inviteFriendsCount];
        Generalmodel *model = newResponse;
        NSLog(@"response = %@",model.status_code);
        
        if ([model.status_code integerValue]) {
            [self dismissThisPopup:nil];

            if (_isFromShowRoomVC) {
                        NSString *type = nil;
                    if (PrivacyType == 0)
                        type = @"anyone";
                    
                    if (PrivacyType == 1)
                        type = @"memb_only";
                
                    if (PrivacyType == 2)
                        type = @"noone";
       
                [self.aDelegate showroomAddedOrEditedSuccesFullyShowRoomVc:txtVWShowroomDescription.text showroomImage:imgVShowroomBg.image showroomImageUrl:(!isAddShowroom)?_modelShow.showroom_img_Url:model.mintdetail_url showroomName:lblShowRoomName.text showroomPrivacy:(privacySettings)?@"on":@"off" privacyType:type];
            }
            else
            {
                [self.aDelegate showroomAddedOrEditedSuccesFully:showTagText];
            }
        }
        else{
            
            [self createAlertView:model.status_Msg];
        }
        
        
    } andFailureBlock:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)checkShowTag:(NSString *)showTagVal // Getting the information about the showroom like showroomname,showroomimage,MInt/members count
{
    NSString*  submitData    = [NSString stringWithFormat:@"%@%@?access_token=%@&showroom_tag=%@",ServerUrl,API_SHOWTAGCHECK,GETVALUE(CEO_AccessToken),[showTagVal stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""]] ;
    NSURL *urlLink          =  [[NSURL alloc]initWithString:[NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_SHOWTAGCHECK,submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:submitData]];
    NSLog(@"UrlLink %@, submitData=%@",urlLink,submitData);

    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (!data) {
             return;
         }
            id info = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             NSLog(@"Showroom Info %@",(NSDictionary *)info);
         
             aTapGuesture       = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideSuggestionList)];
             aTapGuesture.delegate = self;
             [tblVwShowroomInfo    addGestureRecognizer:aTapGuesture];
             lblSugOne.text     = [[[info valueForKey:@"suggestion"] objectAtIndex:0] valueForKey:@"name"];
             lblSugTwo.text     = [[[info valueForKey:@"suggestion"] objectAtIndex:1] valueForKey:@"name"];
             lblSugThree.text   = [[[info valueForKey:@"suggestion"] objectAtIndex:2] valueForKey:@"name"];
             isSujestNeedToshow = YES;
             [tblVwShowroomInfo reloadData];
             NSLog(@"%@",[info valueForKey:@"suggestion"]);
         
     }];
}
#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:tblVwShowroomInfo]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        NSLog(@"Guestor recogniser !!!!!!!!");
        return NO;
    }
    
    return YES;
}
-(void)hideSuggestionList
{
    [tblVwShowroomInfo removeGestureRecognizer:aTapGuesture];
    isSujestNeedToshow = NO;
    [tblVwShowroomInfo reloadData];
}
- (IBAction)deleteShowroom
{
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:@"This Showroom will be permanently deleted and there will be no way to recover it."];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont systemFontOfSize:11.0]
                  range:NSMakeRange(0, [hogan  length])];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Delete this Showroom?"
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert.view setTintColor:[UIColor blackColor]];
    [alert setValue:hogan forKey:@"attributedMessage"];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Yes"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&showroom_tag=%@",GETVALUE(CEO_AccessToken),lblShowTagName.text] dataUsingEncoding:NSUTF8StringEncoding];
                             
                             [self CallAPIwithData:submitData andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@customshowroom/delete_showroom_details",ServerUrl]]];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Keep"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
-(void)CallAPIwithData:(NSData*)submitData andAPIUrl:(NSURL *)myUrl
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
     NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
     NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];

         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         NSString *strMsg = [showroomDict valueForKey:@"StatusMsg"];
        if ([[showroomDict valueForKey:@"Status"] isEqualToString:@"Success"] && [[showroomDict valueForKey:@"StatusCode"] boolValue] )
         {
            // [self.aDelegate deleteShowroom:lblShowTagName.text];
             
             [self.navigationController popToRootViewControllerAnimated:YES];
             AppDelegate *delegateApp = (AppDelegate *) [UIApplication sharedApplication].delegate;
             delegateApp.isShowroomDeleted = YES;
 

         }
         else
         {
             
             NSLog(@"%@",strMsg);
         }
     }];
}

@end
