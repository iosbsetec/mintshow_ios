//
//  ViewController.m
//  OnBoardDemo
//
//  Created by Bsetec on 14/01/16.
//  Copyright (c) 2016 bsetec. All rights reserved.
//

#import "LoginOnBoard.h"
#import "PopUpList.h"
#import "AppDelegate.h"
#import "RoundedRect.h"
#import "Reachability.h"
#import "Userfeed.h"
#import "MSForgotPasswordVC.h"
#import "UserAccessSession.h"

#define kVALIDEMAILID @"Invalid Email"
#define kVALIDPASSWORDLENGTH @"Invalid Password"
#define ERRORTAG 404
@implementation LoginOnBoard
{
    IBOutlet UITextField *txtFldEmail;
    IBOutlet UITextField *txtFldPassword;
    IBOutlet UIButton *btnRememberMe;
   
    IBOutlet UILabel *lblSuccessMsg;
    IBOutlet UIImageView *imgSuccess;
    
    CGRect doneRect;
    IBOutlet RoundedRect *btnNextView;
    
    IBOutlet UIButton *btnEmail;
    IBOutlet UIButton *btnPassword;
    
    IBOutlet RoundedRect *viewEmail;
    IBOutlet RoundedRect *viewPassword;
    
    
    NSString *email;
    NSString *firstName;
    NSString *lastname;
    NSString *gender;
    NSString *fbid;
    NSString *fbUserImage;
    
    UILabel *lblTitle;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [lblSuccessMsg setHidden:YES];
    [imgSuccess setHidden:YES];
    
//    lblYear.layer.borderColor = [UIColor redColor].CGColor;
//    lblYear.layer.borderWidth = 2.0;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardShowing:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardHiding:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    doneRect = btnNextView.frame;
    [btnRememberMe setSelected:NO];
    
    [self makeValidationButtonAlpha:0.0];
    
    NSLog(@"Email %@ and password %@",GETVALUE(CEO_Email) ,GETVALUE(CEO_Password));

    if ([GETVALUE(CEO_RememberMe) isEqualToString:@"1"]) {
        NSLog(@"Email %@ and password %@",GETVALUE(CEO_Email) ,GETVALUE(CEO_Password));
        txtFldEmail.text = GETVALUE(CEO_Email) ;
        txtFldPassword.text = GETVALUE(CEO_Password) ;
        [btnRememberMe setSelected:YES];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardShowing:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardHiding:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)makeValidationButtonAlpha:(CGFloat)alpha
{
    btnEmail.alpha = alpha;
    btnPassword.alpha = alpha;
}

-(IBAction)onEmailValidationTapped:(id)sender
{
    [self onCreateErrorMsg:viewEmail title:kVALIDEMAILID xOrigin:173 yOrigin:2 width:90 tipHeight:24];
}
-(IBAction)onPasswordValidationTapped:(id)sender
{
    [self onCreateErrorMsg:viewPassword title:kVALIDPASSWORDLENGTH xOrigin:143 yOrigin:2 width:120 tipHeight:24];
}

-(void)onCreateErrorMsg:(UIView *)view title:(NSString *)strTitle xOrigin:(CGFloat)xPos yOrigin:(CGFloat)yPos width:(CGFloat)width  tipHeight:(CGFloat)height
{
    [[self.view viewWithTag:ERRORTAG]removeFromSuperview];
    
    UIView *viewTips = [[UIView alloc]initWithFrame:CGRectMake(xPos, yPos, width, 36)];
    viewTips.backgroundColor = [UIColor clearColor];
    viewTips.layer.cornerRadius = 4.0;
    [view addSubview:viewTips];
    
    lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, (height==36)?0:6, width-10, height)];
    lblTitle.text = strTitle;
    lblTitle.backgroundColor = [UIColor whiteColor];
    lblTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [[UIColor blackColor]colorWithAlphaComponent:0.7];
    lblTitle.layer.borderColor = [UIColor lightGrayColor].CGColor;
    lblTitle.layer.borderWidth = 0.4;
    lblTitle.numberOfLines = 0;
    lblTitle.layer.cornerRadius = 4.0;
    [lblTitle setClipsToBounds:YES];
    [viewTips addSubview:lblTitle];
    
    UIImageView *imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(lblTitle.frame.size.width-3, 10, 14,16)];
    imgArrow.image = [UIImage imageNamed:@"rightarrow.png"];
    [imgArrow setTag:123];
    [viewTips addSubview:imgArrow];
    
    [viewTips setTag:ERRORTAG];
    [self makeLabelAnimation:viewTips];
}

-(void)makeLabelAnimation:(UIView *)viewTips
{
    CGRect rectLbl = lblTitle.frame;
    rectLbl.size.width = 0;
    rectLbl.origin.x = lblTitle.frame.size.width;
    lblTitle.frame = rectLbl;
    
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect rectLbl2 = lblTitle.frame;
                         rectLbl2.size.width = viewTips.frame.size.width-10;
                         rectLbl2.origin.x = 0;
                         lblTitle.frame = rectLbl2;
                     } completion:^(BOOL finished) {
                     }];
}

-(void)removeAllErrorValidation
{
    [[self.view viewWithTag:ERRORTAG]removeFromSuperview];
}

-(void)KeyboardShowing:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int height = MIN(keyboardSize.height,keyboardSize.width);
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if (isSmallPad) // Changes for iPad done by mani
        btnNextView.frame = CGRectMake(0, appDel.window.frame.size.height-height-40-self.view.frame.origin.y, self.view.window.frame.size.width, 40);
    else
        btnNextView.frame = CGRectMake(0, self.view.window.frame.size.height-height-40, self.view.window.frame.size.width, 40);
}

-(void)KeyboardHiding:(NSNotification *)notification
{
    btnNextView.frame =  CGRectMake(20, doneRect.origin.y, self.view.window.frame.size.width-40, 40);
    
    if (isSmallPad)
        [self changeYoriginForiPadScreen:0];
}
-(void)changeYoriginForiPadScreen:(int)yPos
{
    [UIView animateWithDuration:0.5 delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect rectView = self.view.frame;
        rectView.origin.y = yPos;
        [self.view setFrame:rectView];
    }
                     completion:^(BOOL finished){
                     }];
}


-(IBAction)btnBackTapped:(id)sender
{
    [self.view endEditing:YES];

    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnRememberMeTapped:(id)sender
{
    if ([btnRememberMe isSelected]) {
        [btnRememberMe setSelected:NO];
    }
    else
    {
        [btnRememberMe setSelected:YES];
    }
}

-(IBAction)btnNextTapped:(id)sender
{
    [self.view endEditing:YES]; 
    if ([self isnewWorkAvailable] && [self checkUserGivenDetails]) {
        [self Passparametre];
    }

}

-(BOOL)isnewWorkAvailable{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable)  {
        return YES;
    }  else {
        showAlert(@"Alert", @"We are unable to make a internet connection at this time. Please try again later", @"Ok", nil);
        return NO;
    }
}


-(void)Passparametre
{
    [btnPassword setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
    btnPassword.alpha = 1.0;
    [btnPassword setUserInteractionEnabled:NO];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *querryString=[NSString stringWithFormat:@"email=%@&password=%@&device_token=%@&device_id=%@&app_version=%@&device_type=%@",txtFldEmail.text,txtFldPassword.text,[(AppDelegate *)[UIApplication sharedApplication].delegate getDeviceTkenFromKeyChain],[(AppDelegate *)[UIApplication sharedApplication].delegate getDeviceID],@"1.0",@"iphone"];

    SPHServiceRequest *req= [[SPHServiceRequest alloc]init];
        [req postBlockServerResponseForData:[querryString dataUsingEncoding:NSUTF8StringEncoding] paramDict:nil mathod:METHOD_LOGIN withSuccessionBlock:^(id response)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             Userfeed *model = response;
             if ([model.status_code isEqualToString:@"1"] && [model.status isEqualToString:@"Success"]) {
                 model.User_deviceToken = GETVALUE(DEVICE_TOKEN);
                 model.User_DeviceId    =   [(AppDelegate *)[UIApplication sharedApplication].delegate getDeviceID];

                [MSMixPanelTrackings trackLoginWithType:@"Email"];
                 NSLog(@"Login Success .......");
                 if ([GETVALUE(CEO_RememberMe) isEqualToString:@"1"]) {
                     [[NSUserDefaults standardUserDefaults] removeObjectForKey:CEO_Email];
                     [[NSUserDefaults standardUserDefaults] removeObjectForKey:CEO_Password];
                     [[NSUserDefaults standardUserDefaults] removeObjectForKey:CEO_RememberMe];
                     SYNCHRONISE;
                 }
             
          
                 [self SaveUserDetail:model];
                 txtFldPassword.text = nil;
                 txtFldEmail.text = nil;
                 [lblSuccessMsg setHidden:NO];
                 [imgSuccess setHidden:NO];
                 
                 
                 
                 if ([GETVALUE(CEO_ISACCOUNTDEACTIVATED) isEqualToString:@"yes"])
                 {
                     showAlert(@"Account Already Deactivated!", @"Please login with another Account", @"Ok", nil);
                 }else  {
                     AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                     [appDell authenticationDidFinish:self selectedInde:0];
                     [appDell updateCounterValue:[model.notify_val integerValue]];
                 }

             }
             else {
                 btnPassword.alpha = 1.0;
                 [btnPassword setUserInteractionEnabled:YES];
                 [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
                 AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                 [appDell onCreateToastMsgInWindow:model.status_Msg];

             }
         }
                            andFailureBlock:^(NSError *error) {
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                NSLog(@"Login Fail .......");
                                
                            }];
    }




- (IBAction)rememberPassword:(UIButton *)sender {
    if ([btnRememberMe isSelected]) {
        [btnRememberMe setSelected:NO];
    }
    else
    {
        [btnRememberMe setSelected:YES];
    }
 }
- (void)SaveUserDetail:(Userfeed*)response
{
    response.email = txtFldEmail.text;
    response.password = txtFldPassword.text;
    
    [UserAccessSession storeUserSession:response];


    SETVALUE(response.AccessToken,CEO_AccessToken);
    SETVALUE(response.User_id,CEO_UserId);
    SETVALUE(response.Referral_id,CEO_ReferalID);
    SETVALUE(response.discover_all_page_first_time_visit,CEO_DISCOVERALLFIRSTTIMEVISIT);
    SETVALUE(response.User_id,CEO_UserId);
    SETVALUE(response.userName,CEO_UserName);
    SETVALUE(response.user_image_url,CEO_UserImage);
    SETVALUE(response.first_name,CEO_UserFirstName);
    SETVALUE(response.last_name,CEO_UserLastName);
    SETVALUE(response.showroom_page_first_time_visit,CEO_PARTCULARSHOWROOMFIRSTTIMEVISIT);
    SETVALUE(response.is_already_posted_mint,CEO_ISMINTALREADYPOSTED);
    SETVALUE(response.mint_detail_page_first_time_visit,CEO_ISMINTDETAILFIRSTTIMEVISIT);
    SETVALUE(response.is_joined_showrooms,CEO_ISJOINEDSHOWROOM);
    SETVALUE(response.is_Account_deactivated,CEO_ISACCOUNTDEACTIVATED);
    SETVALUE(response.addmint_culture_first_time_visit,CEO_ISADDMINTFIRSTTIMEVISIT);


    if ([btnRememberMe isSelected])
    {
        SETVALUE(txtFldEmail.text,CEO_Email);
        SETVALUE(txtFldPassword.text,CEO_Password);
        SETVALUE(@"1",CEO_RememberMe);
        NSLog(@"Email %@ and password %@",txtFldEmail.text ,txtFldPassword.text);
    }
    
    SYNCHRONISE;
}


-(BOOL)checkUserGivenDetails
{
    [self removeAllErrorValidation];
    if (txtFldEmail.text.length==0 && txtFldPassword.text.length==0)
    {
        [self makeValidationButtonAlpha:1.0];
        [btnEmail setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        return NO;
    }
    BOOL isAllOk = YES;
    if (![YSSupport validateEmail:txtFldEmail.text])
    {
        btnEmail.alpha = 1.0;
        [btnEmail setUserInteractionEnabled:YES];
        [btnEmail setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        isAllOk = NO;
        return NO;
    }
    if ([YSSupport validateEmail:txtFldEmail.text] )
    {
        btnEmail.alpha = 1.0;
        [btnEmail setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
        
    }
    
    if ([txtFldPassword.text length] < 1 )
    {
        btnPassword.alpha = 1.0;
        [btnPassword setUserInteractionEnabled:YES];
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        isAllOk = NO;
        return NO;
    }

    
    return isAllOk;
}

#pragma mark -Textfield Delegate

-(IBAction)onTxtFldDidChange:(UITextField *)textField
{
    if (textField == txtFldEmail && ![YSSupport validateEmail:txtFldEmail.text])
    {
        btnEmail.alpha = 0.0;
    }
    else if(textField == txtFldEmail && [YSSupport validateEmail:txtFldEmail.text])
    {
        [btnEmail setUserInteractionEnabled:NO];
        [btnEmail setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
        btnEmail.alpha = 1.0;
    }
    
    if (textField == txtFldPassword && ([txtFldPassword.text length] < 6))
    {
        btnPassword.alpha = 0.0;
    }
    else if(textField == txtFldPassword && [txtFldPassword.text length] >=20)
    {
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        btnPassword.alpha = 1.0;
        [btnPassword setUserInteractionEnabled:YES];
    }
    else if(textField == txtFldPassword && [txtFldPassword.text length] >= 6)
    {
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
        btnPassword.alpha = 1.0;
        [btnPassword setUserInteractionEnabled:NO];
    }
}
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    [self removeAllErrorValidation];
//    return YES;
//}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [self removeAllErrorValidation];

    if (textField == txtFldPassword) {
        NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        textField.text = updatedString;
        return NO;
    }
    
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self removeAllErrorValidation];
    if (textField == txtFldEmail)
    {
        
        if (isSmallPad)
            [self changeYoriginForiPadScreen:-50];

        return YES;
    }
    else if (textField == txtFldPassword && [YSSupport validateEmail:txtFldEmail.text])
    {
        if (isSmallPad)
            [self changeYoriginForiPadScreen:-70];
        
        return YES;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)forGotPasswordTapped:(UIButton *)sender
{
    [self.view endEditing:YES];
    MSForgotPasswordVC *vcShowRoom = [[MSForgotPasswordVC alloc]initWithNibName:@"MSForgotPasswordVC" bundle:nil];
    [self presentViewController:vcShowRoom animated:YES completion:^{
    }];
}



#pragma mark - Facebook Functionalities

- (IBAction)facebookInfoDisplayForRegistration:(UIButton *)sender  {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email",@"user_location",@"user_birthday",@"user_hometown"]
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    if ([FBSDKAccessToken currentAccessToken]) {
                                        NSString *access_token=[FBSDKAccessToken currentAccessToken].tokenString;
                                        SETVALUE(access_token, CEO_FacebookAccessToken);
                                        SYNCHRONISE;
                                        
                                        NSLog(@"Token is available : %@",GETVALUE(CEO_FacebookAccessToken));
                                        
                                        
                                        
                                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields": @"id,picture,first_name, last_name, email, gender"} tokenString:GETVALUE(CEO_FacebookAccessToken) version:nil HTTPMethod:@"GET"]
                                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                                         
                                         
                                         {
                                             if (!error) {
                                                 NSLog(@"fetched user:%@", result);
                                                 email = [result objectForKey:@"email"];
                                                 firstName = [result objectForKey:@"first_name"];
                                                 lastname = [result objectForKey:@"last_name"];
                                                 gender = [result objectForKey:@"gender"];
                                                 fbid = [result objectForKey:@"id"];
                                                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal",[result valueForKey:@"id"]] forKey:CEO_FBUSERIMAGE];
                                                 SYNCHRONISE;
                                                 [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                                 
                                                 [self createFBAccount:YES];
                                                 
                                             }
                                             
                                             else
                                             {
                                                 NSLog(@"error is %@",error.description);
                                             }
                                         }];
                                    }
                                    
                                }
                            }];
}

-(void)openFacebookPage
{
    
    
    FacebookInfoVC *facebookVC = [[FacebookInfoVC alloc]initWithNibName:@"FacebookInfoVC" bundle:nil];
    facebookVC.delegate = self;
    facebookVC.strFBUserEmailId = email;
    facebookVC.strFBUserFirstName = firstName;
    facebookVC.strFBUserLastName = lastname;
    facebookVC.strFBUserId = fbid;
    facebookVC.strFBUserImage = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal",fbid];
    
    
    [self.navigationController presentViewController:facebookVC animated:YES completion:^{
        
    }];
}
-(void)signUpwith:(NSString *)fName lastName:(NSString *)lName emailId:(NSString *)emailId;
{
    email = nil;
    email = emailId;
    firstName = nil;
    firstName = fName;
    lastname = lName;
    
    [self createFBAccount:NO];
    
}

-(void)continueWithEmail
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)continueWithLogin
{

}
-(IBAction)createFBAccount :(BOOL) checkAccountExist
{
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSData*  submitData = nil;
    if (checkAccountExist) {
        submitData    = [[NSString stringWithFormat:@"social_id=%@&social_type=%@",fbid,@"FB"]dataUsingEncoding:NSUTF8StringEncoding];
    }
    else
        submitData    = [[NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&social_id=%@&social_type=%@&gender=%@&referral_id=%@&lat=%@&lang=%@&device_token=%@&device_id=%@&device_type=iOS&app_version=%@&dob=%@&user_image_url=%@",firstName,lastname,email,email,fbid,@"FB",gender,@"",@"",@"",GETVALUE(DEVICE_TOKEN),[appDelegate getDeviceID],@"",@"",GETVALUE(CEO_FBUSERIMAGE)]dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString*req = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"request is %@",req);
    [YSAPICalls signUp:submitData ForSuccessionBlock:^(id newResponse) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        Userfeed *model = newResponse;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if ([model.status_code isEqualToString:@"1"]) {
            if ( checkAccountExist && ([model.status_Msg isEqualToString:@"Account does not exist"]|| [model.status_Msg isEqualToString:@"Invalid email"])) {
                [self openFacebookPage];
            }
            else{
                [self SaveUserDetail:model];
                NSLog(@"Login Success .......");
                AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                [appDell updateCounterValue:[model.notify_val integerValue]];

                [MSMixPanelTrackings trackLoginWithType:@"Facebook"];
                if ([model.discover_all_page_first_time_visit isEqualToString:@"yes"]) {
                    [appDell authenticationDidFinish:self selectedInde:1];
                    
                }
                else
                    [appDell authenticationDidFinish:self selectedInde:0];
            }
        }
        
    }
       andFailureBlock:^(NSError *error) {
           [MBProgressHUD hideHUDForView:self.view animated:YES];
           
           NSLog(@"Login Fail .......");
           
       }];
    
}

@end
