//
//  ListViewCell.m
//  SampleDemo
//
//  Created by bsetec on 11/30/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//



#import "PopUpList.h"
#import "SignUpCells.h"
#import "AppDelegate.h"



@interface PopUpList ()<UITableViewDataSource,UITableViewDelegate> {
    NSMutableArray  *selectedIndexes;
    UILabel         *labelFilters;
    NSMutableArray  *arrayCategory;
    NSMutableArray  *selectedRows;NSInteger selectedrow ;
    UIButton        *searchButton;
    NSString        *selectedType;
    NSInteger       nMaxYear;
    NSInteger       lastSelectdData;
}

@property(nonatomic, strong) UITableView *tblPopUpList;

@end

@implementation PopUpList{
    NSString        *strViewTitle ;
    UIView          *viewTable;
    NSMutableArray  *arrUserList;
}

@synthesize tblPopUpList;

- (id)setDatasToTableView:(NSArray *)arrDatas selectedType:(NSString *)strType frame:(CGRect)frame lastSelectdInfo:(NSInteger)selectedInfo
{
    [self setBackgroundColor:[[UIColor blackColor]colorWithAlphaComponent:0.3]];
    
    tblPopUpList = (UITableView *)[super init];
    if (self)
    {
        UIView *viewBG = [[UIView alloc]initWithFrame:frame];
        viewBG.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(handleSingleTap:)];
        [viewBG addGestureRecognizer:singleFingerTap];
        [self addSubview:viewBG];
        
        UIView *viewTop = [[UIView alloc]init];
        viewTop.backgroundColor = [UIColor whiteColor];
        viewTop.layer.cornerRadius = 5;
        tblPopUpList = [[UITableView alloc] init];

        CGRect rectView = viewTop.frame;
        rectView.origin.x = (frame.size.width-(CGRectGetWidth(frame)-150))/2;
        rectView.size.width =CGRectGetWidth(frame)-150;
        rectView.origin.y = (arrDatas.count==3)?((frame.size.height-128)/2):((frame.size.height-(20+CGRectGetHeight(frame)-235))/2);
        rectView.size.height = (arrDatas.count==3)?108:CGRectGetHeight(frame)-235;
        [viewTop setFrame:rectView];
        
        if (selectedInfo != -1) {
            selectedrow = selectedInfo;
        }
        tblPopUpList.frame = CGRectMake(10,0, viewTop.frame.size.width-20, viewTop.frame.size.height);
        tblPopUpList.delegate           = self;
        tblPopUpList.dataSource         = self;
        tblPopUpList.layer.cornerRadius = 5;
        tblPopUpList.backgroundColor    = [UIColor whiteColor];
        tblPopUpList.separatorStyle     = UITableViewCellSeparatorStyleNone;
        
        AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        [appDell.window addSubview:self];
        [self setFrame:frame];
        
        [viewTop addSubview:tblPopUpList];

        [self addSubview:viewTop];

    }
    
    selectedType = strType;
   if ([selectedType isEqualToString:@"Year"])
    {
        NSCalendar *gregorian = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
        NSInteger year = [gregorian component:NSCalendarUnitYear fromDate:NSDate.date];
        nMaxYear = year-13 - 1950;
    }
    if (arrDatas.count)
        arrayCategory = [[NSMutableArray alloc]initWithArray:arrDatas];
    
    [tblPopUpList reloadData];
    
    return self;

}
 -(void)checkTapped : (UIButton *)sender
{
    [sender setImage:[UIImage imageNamed:@"RadiobButton_On"] forState:UIControlStateNormal];
    
    if ([self.delegate respondsToSelector:@selector(setDataFromPopUpList:type:)])
    {
        if ([selectedType isEqualToString:@"Year"])
        {
            [self.delegate setDataFromPopUpList:[NSString stringWithFormat:@"%ld",1951+sender.tag]  type:selectedType];
        }
        else
            [self.delegate setDataFromPopUpList:[arrayCategory objectAtIndex:sender.tag]  type:selectedType];
        
    }
    
    [self removeFromSuperview];
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self removeFromSuperview];
}

-(void)createTableviewWithFrame:(CGFloat)frame
{
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([selectedType isEqualToString:@"Year"])
        return nMaxYear;
    
    return   [arrayCategory count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 35, CGRectGetWidth([UIScreen mainScreen].bounds), 1)];
    separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    separatorView.alpha = 0.4;
    separatorView.layer.borderWidth = 0.5;
    
    static NSString *CellIdentifier = @"SignUpCells";
    SignUpCells *cell = (SignUpCells*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    }
    
    if ([selectedType isEqualToString:@"Year"])
    {
        cell.titleLabel.text = [NSString stringWithFormat:@"%d",1951+indexPath.row];
    }
    else
    {
        
        cell.titleLabel.text     = [arrayCategory objectAtIndex:indexPath.row];
    }
    cell.btnCheckMarck.tag   = indexPath.row;
    cell.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    cell.titleLabel.textColor=[UIColor blackColor];
    [cell.contentView addSubview:separatorView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (selectedrow == indexPath.row) {
        
        [cell.btnCheckMarck setImage:[UIImage imageNamed:@"RadiobButton_On"] forState:UIControlStateNormal];
    }
    else
        [cell.btnCheckMarck setImage:[UIImage imageNamed:@"RadiobButton_Off"] forState:UIControlStateNormal];

    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell.btnCheckMarck addTarget:self action:@selector(checkTapped:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(setDataFromPopUpList:type:)])
    {
        if ([selectedType isEqualToString:@"Year"])
        {
            SignUpCells *cell = (SignUpCells*)[tableView cellForRowAtIndexPath:indexPath];
            [self.delegate setDataFromPopUpList:cell.titleLabel.text  type:selectedType];
        }
        else
            [self.delegate setDataFromPopUpList:[arrayCategory objectAtIndex:indexPath.row]  type:selectedType];

    }
    
    [self removeFromSuperview];
}

-(IBAction)onBGTapped:(id)sender
{
    [self removeFromSuperview];
}

@end





