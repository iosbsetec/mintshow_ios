

#import "MSPSignatureShowroomVC.h"
#import "AsyncImageView.h"
#import "YSCollectionView.h"
#import "YSCollectionViewDataSource.h"
#import "EmptyTableViewCell.h"
#import "UIViewController+CWPopup.h"
#import "CommentsViewController.h"
#import "MSRemintVC.h"
#import "MSMintDetails.h"
#import "YSSupport.h"
#import "MSProfileVC.h"
#import "JitenCollectionView.h"
#import "UsersListView.h"
#import "DiscoverDataModel.h"
#import "MintShowroomViewController.h"

#define kDefaultImagePagerHeight 375.0f
#define kDefaultTableViewHeaderMargin 95.0f
#define kDefaultImageAlpha 1.0f
#define kDefaultImageScalingFactor 450.0f

#define NAVBAR_CHANGE_POINT 15

@interface MSPSignatureShowroomVC ()<YSCollectionViewDataSource,YSCollectionViewDelegate,RemintDelegate,MSMintDetailsDelegate,JitenCollectionTableDelegate,JitenCollectionViewDataSource,DiscoverTableViewDataSource,DiscoverTableDelegate,UsersListDelegate>
{
    IBOutlet UILabel *transperentviewLabel;
    CGFloat currentAlphaValue;
    
    NSMutableArray *allMintsdata;
    
    // For related showroom List
    
    NSMutableArray *allShowRoomdata;
    NSMutableArray *arrSignatureShowroomList;

    BOOL isShowRoomLoaded;
    int pagenumberShowroom;
    CGFloat multiShowroomHeight;
    NSInteger nTotalShowRoomCount;
    
    
    BOOL isLoading,isDataFinishedInServer;
    int pagenumber;
    CGFloat MultiMintHeight;
    
    CGRect cachedImageViewSize;
    
    IBOutlet UIView *viewNoMoreMints;
    IBOutlet UIView *viewHeader;
    IBOutlet UIImageView *imgHeader;
    IBOutlet UILabel *showRoomDescriptionLabel;
    IBOutlet UILabel *lblshowTag;
    
    IBOutlet UIImageView *imgStar;
    IBOutlet UILabel *lblMintsTitle;
    IBOutlet UILabel *lblFollowerTitle;
    
    // Private Showroom Page
    
    IBOutlet UILabel *lblInviterName;
    IBOutlet UILabel *lblPrivateShowroomName;
    IBOutlet UIView *viewPrivateHolder;
    
    
    NSString *showroom_link;
    
    ShowRoomModel *shareModel;
    NSString *selectdSignatureShowroom;

    
    CGFloat discoverMintHeight;
    
    
}

@property (strong, nonatomic) IBOutlet JitenCollectionView *showRoomCollectionview;
@property (strong, nonatomic) IBOutlet YSCollectionView *aView;
//@property (weak, nonatomic)   IBOutlet UITableView *multiMintTableview;
@property (strong, nonatomic) IBOutlet DiscoverTableview *multiMintTableview;

@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIView *detailsView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *showroomNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *showroomNameLabel1;

@property (weak, nonatomic) IBOutlet UIView *customNavBarView;

@property (nonatomic,assign) CGRect initialTableRect;
@property (nonatomic,assign) CGRect initialHeaderRect;
@property (nonatomic,assign) NSInteger kHiddenPixels;

-(IBAction)gobacktoPrevieousScreen:(id)sender;
-(IBAction)gotoShowroomOwnerProfileScreen;

@end

@implementation MSPSignatureShowroomVC

- (void)viewDidLoad {
    
    shareModel = [[ShowRoomModel alloc]init];
    [super viewDidLoad];
    [self.customNavBarView setAlpha:0];
    [_customNavBarView setBackgroundColor:[UIColor colorWithRed:9.0/255.0 green:26.0/255.0  blue:53.0/255.0  alpha:1.0]];
    currentAlphaValue = 0.37;
    transperentviewLabel.alpha = currentAlphaValue;
    
    self.initialTableRect = self.multiMintTableview.frame;
    self.initialHeaderRect = self.customNavBarView.frame;
    self.kHiddenPixels = self.customNavBarView.frame.size.height;
    

  
    
    pagenumber          = 1;
    nTotalShowRoomCount = 0;
    isShowRoomLoaded    = NO;
    pagenumberShowroom  = 1;
    isDataFinishedInServer = NO;
    
    MultiMintHeight     = 500;
    discoverMintHeight  = 0;
    if (_isFromDiscoverShowroom) {
        selectdSignatureShowroom = _modelShow.discoverName;
        [self setScreenDetails:_modelShow];
        
    }
    else
        selectdSignatureShowroom = _selectedShowroom;
    
    [self initializetableheaderView];
    [self initializer];
    isLoading = NO;

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(mintEditedSuccesFullyInSignature:) name:KMINTEDITED object:nil];
   
 //   [[SPHSingletonClass sharedSingletonClass] addObserver:self forKeyPath:@"login_user_streak" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
  }

/*-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"login_user_streak"]) {
        NSLog(@"The value of the login_user_streak was changed.");
        NSLog(@"%@", change);
    }
    
//    if ([keyPath isEqualToString:@"age"]) {
//        NSLog(@"The age of the child was changed.");
//        NSLog(@"%@", change);
//    }
    
}*/
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}
/*-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
   // [[SPHSingletonClass sharedSingletonClass] removeObserver:self forKeyPath:@"login_user_streak"];
}*/
-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    if (aDelegate.isCommentActionUpdate) {
        pagenumber          = 1;
       [self getSignatureShowroomMints:pagenumber deletePreviousList:YES];
        aDelegate.isCommentActionUpdate = NO;
    }
}
-(void)mintEditedSuccesFullyInSignature:(NSNotification *)notification
{
    AppDelegate  * aDelegate = (AppDelegate  *) [UIApplication sharedApplication].delegate;
    
     if (allMintsdata.count>0 && [aDelegate.selectedIndex integerValue]<allMintsdata.count)
        {
            BaseFeedModel *model = [allMintsdata objectAtIndex:[aDelegate.selectedIndex integerValue]];
            model.feed_description = [[notification valueForKey:@"userInfo"] valueForKey:@"description"];
            model.feed_posted_edited_text = ([model.feed_description length])?@"Edited":@"";
            [allMintsdata replaceObjectAtIndex:[aDelegate.selectedIndex integerValue] withObject:model];
            [self batchreloadCollectionView];
   }
}


-(void)setSignatureImage:(NSString *)showTagName view:(UILabel *)lblView
{
    showTagName = [NSString stringWithFormat:@"%@  ",showTagName];
    NSAttributedString *labelAttributes = [[NSAttributedString alloc] initWithString:showTagName];
    
    UIImage *img = [UIImage imageNamed:@"Signature_Icon.png"];
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = img;
    textAttachment.bounds = CGRectMake(0, -4.0, 20, 20);
    
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
    [attrStr appendAttributedString:labelAttributes];
    [attrStr replaceCharactersInRange:NSMakeRange(showTagName.length-1, 1) withAttributedString:attrStringWithImage];
    lblView.attributedText = attrStr;
}

-(void)hideStaticDatas:(BOOL)isHidden
{
    [_imageView setHidden:isHidden];
    [imgStar setHidden:isHidden];
    [_userImageView setHidden:isHidden];
    [lblshowTag setHidden:isHidden];
    [_showroomNameLabel setHidden:isHidden];
}

- (void)dismissMemberList : (NSNotification *) notify{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
        }];
    }
}

-(void)setScreenDetails:(DiscoverDataModel *)model
{
    _selectedShowroom = model.discoverName;
    _modelShow = model;
    showRoomDescriptionLabel.text     = model.discoverText;
    [self resizeDescriptionLblHeighWithDesc:model.discoverText];
    
    [self setSignatureImage:[NSString stringWithFormat:@"%@",model.discoverName] view:_showroomNameLabel];
    [self setSignatureImage:[NSString stringWithFormat:@"%@",model.discoverName] view:lblshowTag];

    imgHeader.mintImageURL = [NSURL URLWithString:model.discoverImg];
    _imageView.mintImageURL = [NSURL URLWithString:model.discoverImg];
}



-(void)resizeDescriptionLblHeighWithDesc:(NSString *)strDesc
{
    CGRect recttxtLbl  = [strDesc boundingRectWithSize:CGSizeMake(showRoomDescriptionLabel.frame.size.width-8, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica Neue" size:13.f]} context:nil];
    
    CGRect rectLbl = showRoomDescriptionLabel.frame;
    rectLbl.size.height = (recttxtLbl.size.height>60)?60:recttxtLbl.size.height;
    [showRoomDescriptionLabel setFrame:rectLbl];
}

-(void)initializetableheaderView
{
    [self.imageView setUserInteractionEnabled:YES];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    
    CGRect rctImage = self.imageView.frame;
  //  rctImage.size.height = viewHeader.frame.size.height-16;
    [self.imageView setFrame:rctImage];
    cachedImageViewSize = self.imageView.frame;
    
    // Need to add
    CGRect rctTransparent = transperentviewLabel.frame;
    rctTransparent.size.height = viewHeader.frame.size.height+((isSmallPad)?20:2);
    [transperentviewLabel setFrame:rctTransparent];
    
}
-(void)initializer
{
    // MULTI MINT COLLECTION VIEW
    self.aView.backgroundColor                  = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.multiMintTableview.backgroundColor     = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    
    self.aView.showsHorizontalScrollIndicator   = NO;
    //self.aView.scrollEnabled                    = NO;
    self.aView.ysDataSource              = self;
    self.aView.ysDelegate                = self;
    self.aView.scrollEnabled = NO;

//    self.aView.layer.borderColor = [UIColor redColor].CGColor;
//    self.aView.layer.borderWidth = 2.0;
    
    // ALL TABLE VIEW
    self.multiMintTableview.DiscoverDataSource = self;
    self.multiMintTableview.Discoverdelegate   = self;
    [MBProgressHUD showHUDAddedTo:_multiMintTableview animated:YES];
    
    arrSignatureShowroomList = [[NSMutableArray alloc]init];
    allMintsdata = [[NSMutableArray alloc]init];
    [self UpdateTheMinttype];
}
-(void)getShowRoomObject
{
    SPHSingletonClass *singleTone = [SPHSingletonClass sharedSingletonClass];

    for (DiscoverDataModel *obj in singleTone.arrGlobalSignatureShowroomList) {
        
        if ([obj.discover_showroom_name isEqualToString:_selectedShowroom]) {
            [self setScreenDetails:obj];
            selectdSignatureShowroom = obj.discoverName;
            [self getSignatureShowroomMints:pagenumber deletePreviousList:NO];
            break;
        }
    }
}
-(void)UpdateTheMinttype{

    SPHSingletonClass *singleTone = [SPHSingletonClass sharedSingletonClass];
    if ([singleTone.arrGlobalSignatureShowroomList count]) {
        if (!_isFromDiscoverShowroom) {
            [self getShowRoomObject];
        }
        else
        {
            [self getSignatureShowroomMints:pagenumber deletePreviousList:NO];
        }
        NSMutableArray *arr = [NSMutableArray array];
        [arr addObjectsFromArray:singleTone.arrGlobalSignatureShowroomList];
        [arr removeObject:_modelShow];
        [arrSignatureShowroomList removeAllObjects];
        [arrSignatureShowroomList addObjectsFromArray:arr];
        [arr removeAllObjects];
        arr = nil;
        
        [self.multiMintTableview reloadData];
        return;
    }
    else{
        [self getSignatureShowroomsList:pagenumberShowroom];
     }


}
-(void)getSignatureShowroomsList:(int)pageNum
{
    isLoading = YES;
    
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&page_number=%d",GETVALUE(CEO_AccessToken),pageNum] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"customshowroom/trending_showroom_landing",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    NSLog(@"submitDta123 data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isLoading = NO;
         
         
         // [allShowRoomdata removeAllObjects];
         NSDictionary *showroomDict = [self dictionaryFromResponseData:data];
         if ([[showroomDict valueForKey:@"Status"]isEqualToString:@"Success"])
         {
             
             NSDictionary *showroomDict = [self dictionaryFromResponseData:data];
             NSLog(@"SHOWROOMS ****** = %@",showroomDict);
             
             if([[showroomDict valueForKey:@"list"] count]>0)
             {
                 SPHSingletonClass *singleTone = [SPHSingletonClass sharedSingletonClass];

                 isDataFinishedInServer = NO;
                 [arrSignatureShowroomList removeAllObjects];
                 [singleTone.arrGlobalSignatureShowroomList removeAllObjects];

                 for(NSDictionary *showRoom in [showroomDict valueForKey:@"list"])
                 {
                     [singleTone.arrGlobalSignatureShowroomList addObject:[[DiscoverDataModel alloc]initWithDiscoverInfo:showRoom ForTrendingShowroom:YES]];
                     
                 }
                 [self UpdateTheMinttype];
                 
                 [self.multiMintTableview reloadData];
             }
             else
             {
                 isDataFinishedInServer = YES;
             }
             
             
         }}];
}
- (void)loadNextShowroomPages{
    if(!isLoading && !isDataFinishedInServer){
//        pagenumberShowroom = pagenumberShowroom+1;
//        [self getSignatureShowroomsList:pagenumberShowroom];
    }
}

-(void)getSignatureShowroomMints:(int)pageNumber deletePreviousList:(BOOL)deleteLst
{
    isLoading = YES;
    
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&showroom_tag=%@&page_number=%d",GETVALUE(CEO_AccessToken),selectdSignatureShowroom,pagenumber] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"customshowroom/get_particular_showroom",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    
    NSLog(@"submitDta123 data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isLoading = NO;
         
         [MBProgressHUD hideHUDForView:_multiMintTableview animated:YES];
         
         Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[YSSupport dictionaryFromResponseData:data] methodName:METHOD_MYSHOW_MINT];
         
         if ([gmodel.status_code integerValue])
         {
             
             if ([[SPHSingletonClass sharedSingletonClass] isUser_mint_streak]) {
                 
               //  [AppDelegate createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];
                 AppDelegate *obj = MINTSHOWAPPDELEGATE;
                 [obj createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];

                 [[SPHSingletonClass sharedSingletonClass]
                  setIsUser_mint_streak:NO];
                 
             }
             
             if([gmodel.tempArray count]>0)
             {
                 if (deleteLst) {
                     [allMintsdata removeAllObjects];
                 }
                 isDataFinishedInServer = NO;
                 [allMintsdata addObjectsFromArray: gmodel.tempArray];
                 [self batchreloadCollectionView];
             }
             else
             {
                 isDataFinishedInServer = YES;
             }
         }
         
     }];
    
}


#pragma mark - THIS CODE FOR RELATED SHOWROOM COLLECTION VIEW

-(void)ReloadShowroomCollectionView
{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.showRoomCollectionview performBatchUpdates:^{
        [self.showRoomCollectionview reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
            multiShowroomHeight = self.showRoomCollectionview.collectionViewLayout.collectionViewContentSize.height;
            self.showRoomCollectionview.frame = CGRectMake(0,10, self.view.frame.size.width,self.view.frame.size.height);
            [self reloadTableviewWithsection:0];
        }
    }];
}

-(void)reloadTableviewWithsection:(NSInteger)section
{
    [self.multiMintTableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:section], nil]  withRowAnimation:UITableViewRowAnimationNone];
}

-(JitenCollectionView *)showroomCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.showRoomCollectionview;
}
- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    
    return  (ShowRoomModel *)[allShowRoomdata objectAtIndex:row];
    // return  nil;
}
- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView{
    NSLog(@"NUMBER OF SHOWROOMS %ld",(unsigned long)allShowRoomdata.count);
    return allShowRoomdata.count;
    //return 6;
}

#pragma mark JITEN COLLECTION DID SELECT CELL METHOD

- (void) JitenCollectionViewDelegateMethod: (ShowRoomModel *) modeldata selectedIndex :(NSInteger) selectedRowNo
{
    shareModel = modeldata;
    self.showRoomCollectionview.alpha = 0.0;
    [self hideStaticDatas:NO];
   // [self setScreenDetails:modeldata];
    isShowRoomLoaded = NO;
    [self.multiMintTableview reloadData];
}


- ( NSInteger)DiscoverTableViewNumberOfsections:(DiscoverTableview *)tableView
{
    return 2;
}

-(CGFloat)DiscoverTableView:(DiscoverTableview *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0){
        return 150;
    }
    
    return MultiMintHeight;
}

-(CGFloat)DiscovertableView:(DiscoverTableview *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 20.0;
        else
            return 0.0;
}


- (DiscoverViewCellType)DiscoverTableView:(DiscoverTableview *)tableView cellTypeForSection:(NSInteger)section
{
    if (section == 0)
        return DiscoverViewCellTypeSignatureShowroom;
    else
        return DiscoverViewCellTypeMultiMint;
}


- (NSString *)DiscoverTableView:(DiscoverTableview *)tableView TitleOfHeaderForSection:(NSInteger)section
{
    if (section == 0)
        return @"SIGNATURE SHOWROOMS";
    else
        return  @"";
    
}

- (NSDictionary *)DiscoverTableView:(DiscoverTableview *)tableView DataForSection:(NSInteger)section
{
        return [NSDictionary dictionaryWithObjectsAndKeys:arrSignatureShowroomList ,@"images", nil];
}



#pragma mark - **** SHOWROOM COLLECTION VIEW END *****

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return   1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (isShowRoomLoaded)?35:0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (isShowRoomLoaded)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
        [view setBackgroundColor:[UIColor whiteColor]];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        label.textColor=[UIColor blackColor];
        [label setText:@"  RELATED SHOWROOM"];
        [label setBackgroundColor:[UIColor whiteColor]];
        [view addSubview:label];
        return view;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isShowRoomLoaded == 0 )
        return MultiMintHeight+200;
    else
        return multiShowroomHeight+20;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MultiMintCollectionTableViewCell";
    
    EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    for(id aview in [cell.contentView subviews])
    {
        [aview removeFromSuperview];
    }
    
    
    if (isShowRoomLoaded) {
        [cell.contentView addSubview:self.showRoomCollectionview];
    }
    else
    {
        [cell.contentView addSubview:self.aView];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

- (void)signatureCollectionViewClickedAtRow:(NSInteger)row
{
    pagenumber = 1;
//    selectdSignatureShowroom = data.discoverName;
//    [self setScreenDetails:data];
//    [MBProgressHUD showHUDAddedTo:_multiMintTableview animated:YES];
//    
//    [self getSignatureShowroomMints:pagenumber deletePreviousList:YES];
}

#pragma mark-  THIS CODE FOR MULTI MINT

-(YSCollectionView *)discoverCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.aView;
}

-(void)multiMintCollectionViewClicked:(YSCollectionView *)sender AtRow:(NSInteger)row
{
    
}

- (BaseFeedModel *)multiMintCollectionViewView:(YSCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    return [allMintsdata objectAtIndex:row];
}



- (NSInteger)NumberOFSectionsMultiMintView:(YSCollectionView *)collectionView
{
    return 1;
}


- (NSInteger)NumberOFItemsForMultiMintView:(YSCollectionView *)collectionView forSection:(NSInteger)section {
    return  allMintsdata.count;
}

- (void)scrollViewSignatureShowroomDidScroll:(UIScrollView *)scrollView
{
    CGFloat y = -scrollView.contentOffset.y;
    
    if (y > 0){
        self.imageView.frame = CGRectMake(0, scrollView.contentOffset.y, cachedImageViewSize.size.width+y, cachedImageViewSize.size.height+y);
        self.imageView.center = CGPointMake(self.view.center.x, self.imageView.center.y);
        transperentviewLabel.frame = self.imageView.frame;
        if(y<2){
            currentAlphaValue=0.37;
        }else{
            currentAlphaValue = 0.45+ (y/500.0);
        }
        transperentviewLabel.alpha= currentAlphaValue;
    }else {
        CGFloat offsetY = scrollView.contentOffset.y;
        CGFloat alpha = MIN(1, 1 - ((NAVBAR_CHANGE_POINT + 110 - offsetY) / 84));
        if (offsetY > NAVBAR_CHANGE_POINT)  {
            [self.customNavBarView setAlpha:alpha];
        } else  {
            [self.customNavBarView setAlpha:alpha];
        }
    }
}





- (UIView*)viewForZoomingInScrollView:(UIScrollView *)aScrollView {
    return self.imageView;
}

//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self.multiMintTableview);
    if (shouldLoadNextPage)
    {
        
        if ([_modelShow.mint_count integerValue]>0)
        {
            if(!isLoading && !isDataFinishedInServer)
            {
                isLoading = YES;
                pagenumber = pagenumber+1;
                [self getParticularShowrromMintswithPageNumber:pagenumber deleted:NO];
            }
        }
    }
}

#pragma mark - Star Feature
- (void)selectedFeatureType:(int )featureType withString:(NSString *)tagString
{
    
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [tagString substringFromIndex:1];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}



- (void)selectedFeatureType:(int )featureType withString:(NSString *)selectedEnitity isPrivateShowroomAccess:(BOOL)access{
    switch (featureType) {
        case 0:
            [self goToMintdetails:NO mintId:nil navigationHeaderText:selectedEnitity];
            break;
        case 1:
        {
            if(access) {
                showAlert(@"It's a private showroom", nil, @"Ok", nil);
            }
            else
                [self selectedFeatureType:1 withString:selectedEnitity];
        }
            break;
        case 2:case 44:
            [self gotoProfileView:selectedEnitity];
            break;
            
//        case 88:
//            if (![selectedEnitity isEqualToString:selectdSignatureShowroom]) {
//                
//                [self UpdateTheScreenWithMinttypeSelectionFromMints:selectedEnitity];
//            }
//            
//            break;
            
            
        default:
            break;
    }
    
}


-(void)buttonActionclickedFortype:(int)typeVal forRow:(NSInteger)row feedModelData:(BaseFeedModel *)data {
    switch (typeVal)
    {
            
    }
}


- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndex = [NSString stringWithFormat:@"%ld",(long)row];
    if(buttonType == YSButtonTypeComplimint)
    {
        CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
        comentVc.commentCount              = [data.feed_comment_count intValue];
        comentVc.achievementdetails        = data ;
        comentVc.feedId                    = data.feed_id ;
        [self.navigationController pushViewController:comentVc animated:YES];
    }
    else if (buttonType == YSButtonTypeHifiveCount)  {
        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeHighFive numberofPeoples:[data.feed_like_count intValue] feedId:data.feed_id delegate:self];
    }
    
    else if (buttonType == YSButtonTypeInspire)
    {
        MSRemintVC *reminytVW               = [[MSRemintVC alloc]initWithNibName:@"MSRemintVC" bundle:nil];
        reminytVW.aDelegate                 = self;
        reminytVW.feedData                  = data;
        reminytVW.isFromInstagramTableView  = @"0";
        [self presentViewController:reminytVW animated:NO completion:^{
            
        }];
    }
    else if (buttonType == YSButtonTypedetails)
    {
        [self goToMintdetails:YES mintId:[NSString stringWithFormat:@"%ld",(long)row]  navigationHeaderText:_showroomNameLabel.text];
    }
    else if (buttonType == YSButtonTypeUserImage)
    {
        [self showPopupWithModeldata:data withrow:row isRemint:NO];
    }
    else if (buttonType == YSButtonTypeRemintUserImage)
    {
        [self showPopupWithModeldata:data withrow:row isRemint:YES];
    }
}

-(void)selectedUserIdFromHighfiveList:(NSString *)selectdUserId
{
    [self gotoProfileView:selectdUserId];
}

-(void)goToMintdetails: (BOOL)isforShowroomMintDetails mintId:(NSString *)mintIdVal navigationHeaderText:(NSString *)textVal
{
    
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mintArrayDetails = allMintsdata;
    mintDetailsVC.mint_row_id = mintIdVal;
    
    //BOOL isOwner =     (remint)?feeddetails.remint_user_id:feeddetails.feed_user_id]];
    mintDetailsVC.isMintOwner = NO;
    mintDetailsVC.Selected_row_value = [mintIdVal integerValue];
    if (isforShowroomMintDetails) {
        mintDetailsVC.isFromSignatureShowroom = YES;
    }
    else
        mintDetailsVC.isFromTrendingTags = YES;
    
    mintDetailsVC.delegate = self;
    mintDetailsVC.showtagText =  [selectdSignatureShowroom stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    mintDetailsVC.navigationHeaderText = [textVal stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    mintDetailsVC.strShowroomImageUrl = _modelShow.discoverImg;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [[self.view.window viewWithTag:2233]removeFromSuperview];
}

-(void)showPopupWithModeldata:(BaseFeedModel *)feeddetails withrow:(NSInteger)row isRemint:(BOOL)remint {
    
    [self gotoProfileView:[NSString stringWithFormat:@"%@",(remint)?feeddetails.remint_user_id:feeddetails.feed_user_id]];
    
}

-(void)gotoProfileView:(NSString *)selectedEnitity{
    MSProfileVC *prof  = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    [self.navigationController pushViewController:prof animated:YES];
    
}

-(IBAction)gotoShowroomOwnerProfileScreen{
    MSProfileVC *prof  = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = GETVALUE(CEO_UserId);
    [self.navigationController pushViewController:prof animated:YES];
    
}







-(void)batchreloadCollectionView{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.aView performBatchUpdates:^{
        [self.aView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            MultiMintHeight = self.aView.collectionViewLayout.collectionViewContentSize.height;
            self.aView.frame = CGRectMake(0, 5, self.view.frame.size.width,MultiMintHeight+400);
            [UIView setAnimationsEnabled:animationsEnabled];
//            [self.multiMintTableview beginUpdates];
//            [self.multiMintTableview endUpdates];
            [self.multiMintTableview reloadData];
            isLoading = NO;
        }
    }];
    
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gobacktoPrevieousScreen:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES ];
}




#pragma mark - Server API Calls

-(void)getParticularShowrromMintswithPageNumber: (int)pageNumber deleted:(BOOL)isDeleted
{
    if (![YSSupport isNetworkRechable]) {
        return;
    }
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    _selectedShowroom = [_selectedShowroom stringByReplacingOccurrencesOfString:@"*" withString:@""];
    [dicts setValue:_selectedShowroom forKey:@"showroom_tag"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page_number"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [YSAPICalls getParticularShowroom:dicts ForSuccessionBlock:^(id newResponse)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         Generalmodel *gmodel = newResponse;
         isLoading = NO;
         
         if ([gmodel.status_code isEqualToString:@"1"])    // GET RELATED Mints
         {
             isShowRoomLoaded = NO;
             self.aView.alpha = 1.0;
             self.showRoomCollectionview.alpha = 0.0;
             if (isDeleted) {
                 [allMintsdata removeAllObjects];
                 [self batchreloadCollectionView];
             }
             
             if([gmodel.tempArray count]>0)
             {
                 isDataFinishedInServer = NO;
                 [allMintsdata addObjectsFromArray: gmodel.tempArray];
                 [self batchreloadCollectionView];
             }
             else
             {
                 isDataFinishedInServer = YES;
             }
         }

     }
                      andFailureBlock:^(NSError *error) {
                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                          NSLog(@"Failed from the server %@",[error description]);
                          isLoading = NO;
                          
                          
                      }];
}




-(void)onClosePopup
{
    [[self.view viewWithTag:2233]removeFromSuperview];
    NSLog(@"Close Tapped");
}

-(void)onReturnMyShow
{
    [[self.view viewWithTag:2233]removeFromSuperview];
    [self gobacktoPrevieousScreen:nil];
    NSLog(@"Return Tapped");
}

-(void)showshare :(NSString *)titleMsg
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:titleMsg
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    [alert show];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}

- (void)getShowroomDetails:(NSString *)showroomName // Getting the information about the showroom like showroomname,showroomimage,MInt/members count
{
    
    if (![YSSupport isNetworkRechable]) {
        return;
    }
    }



-(void)highFiveMintOfmintDetails :(NSString *)mintId andCurerntValue : (NSString *)activityValue countValue: (NSString *)value
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    int i = 0;
    
    for (BaseFeedModel *model in allMintsdata)
    {
        if ([model.feed_id isEqualToString:mintId])
        {
            [model setIs_user_liked:[activityValue intValue]];
            [model setFeed_like_count:value];
            //            [allMintsdata replaceObjectAtIndex:i withObject:model];
            
            [allMintsdata replaceObjectAtIndex:[aDelegate.selectedIndex integerValue] withObject:model];
            [_aView performBatchUpdates:^{
                [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:[aDelegate.selectedIndex integerValue] inSection:0]]];
            } completion:^(BOOL finished) {
            }];
            
            break;
        }
        i++;
    }
}


-(void)commentAddedSuccesFully:(NSInteger) row
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    if ([aDelegate.selectedIndex integerValue]>=allMintsdata.count) {
        return;
    }
    
    @try {
        //        [_aView reloadData];
        [_aView performBatchUpdates:^{
            [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]]];
        } completion:^(BOOL finished) {
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    @finally {
        
    }
    
}

-(void)remintedSucssesfully
{
    [self getParticularShowrromMintswithPageNumber:1 deleted:YES];
}

//For Mute mint, Delete Mint. FlagInappropriate
-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId
{
    int mintIndex = 0;
    BOOL isMintFoundFromList = NO;
    
    for (int i = 0; i<allMintsdata.count; i++) {
        
        
        if ([[[allMintsdata objectAtIndex:i]feed_id] isEqualToString:mintId]) {
            mintIndex = i;
            isMintFoundFromList = YES;
            break;
        }
        
    }
    
    if (isMintFoundFromList) {
        
        [allMintsdata removeObjectAtIndex:mintIndex];
        [self batchreloadCollectionView];
    }
    
    
}
//Update the tableview if the MintUser Mint exist
-(void)muteUserOfmintDetails:(NSString *)muteUser{
    
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[allMintsdata count]; currentIndex++){
        //do stuff with obj
        BaseFeedModel *feedlist = [allMintsdata objectAtIndex:currentIndex];
        if([muteUser isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [allMintsdata removeObjectsAtIndexes:indexesToDelete];
        [self batchreloadCollectionView];
    }
    
    
}
-(void)updateMintCountForShowroom:(NSString *)mintId{
    int mintIndex = 0;
    BOOL isMintFoundFromList = NO;
    
    for (int i = 0; i<allMintsdata.count; i++) {
        
        
        if ([[[allMintsdata objectAtIndex:i]feed_id] isEqualToString:mintId]) {
            mintIndex = i;
            isMintFoundFromList = YES;
            break;
        }
        
    }
    
    if (isMintFoundFromList) {
        [self updateSignatureShowroomDataCount];
        [allMintsdata removeObjectAtIndex:mintIndex];
        [self batchreloadCollectionView];
    }


}

-(void)updateSignatureShowroomDataCount{
    
    if ([arrSignatureShowroomList count])
    {
                _modelShow.mint_count = [NSString stringWithFormat:@"%d",(int)[_modelShow.mint_count integerValue]-1];
    }
}

- (void)LoadNextPagetriggeredForSection:(NSInteger)section;
{
 
    if(!isLoading)
    {
        pagenumber = pagenumber+1;
        [self getSignatureShowroomMints:pagenumber deletePreviousList:NO];
    }
}
- (void)scrollingTableViewDidSelectImageAtIndexPath:(NSIndexPath*)indexPathOfImage Withdata:(DiscoverDataModel*)data atCategoryRowIndex:(NSInteger)categoryRowIndex
{
    pagenumber = 1;
    selectdSignatureShowroom = data.discoverName;
    SPHSingletonClass *singleTone = [SPHSingletonClass sharedSingletonClass];
   
    if ([singleTone.arrGlobalSignatureShowroomList count] && (indexPathOfImage.row <= [singleTone.arrGlobalSignatureShowroomList count])  ) {
        NSMutableArray *arr = [NSMutableArray array];
        [arr addObjectsFromArray:singleTone.arrGlobalSignatureShowroomList];
        [arr removeObject:data];

        [arrSignatureShowroomList removeAllObjects];
        [arrSignatureShowroomList addObjectsFromArray:arr];
        [arr removeAllObjects];
         arr = nil;
         [self.multiMintTableview reloadData];
        
    }

    [self setScreenDetails:data];
    [MBProgressHUD showHUDAddedTo:_multiMintTableview animated:YES];
    [self getSignatureShowroomMints:pagenumber deletePreviousList:YES];
    
}



//-(void)UpdateTheScreenWithMinttypeSelectionFromMints:(NSString *)mintType
//{
//    SPHSingletonClass *singleTone = [SPHSingletonClass sharedSingletonClass];
//    if ([singleTone.arrGlobalSignatureShowroomList count]) {
//        [self getShowRoomObject];
//        NSMutableArray *arr = [NSMutableArray array];
//        [arr addObjectsFromArray:singleTone.arrGlobalSignatureShowroomList];
//        [arr removeObject:_modelShow];
//        [arrSignatureShowroomList removeAllObjects];
//        [arrSignatureShowroomList addObjectsFromArray:arr];
//        [arr removeAllObjects];
//        arr = nil;
//        
//        [self.multiMintTableview reloadData];
//        return;
//    }
//
//
//}
@end
