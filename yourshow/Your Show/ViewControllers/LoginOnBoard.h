//
//  ViewController.h
//  OnBoardDemo
//
//  Created by Bsetec on 14/01/16.
//  Copyright (c) 2016 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopUpList.h"
#import "FacebookInfoVC.h"


@interface LoginOnBoard : UIViewController<UITextFieldDelegate,MSFacebookDelegate>

-(IBAction)btnBackTapped:(id)sender;
-(IBAction)btnNextTapped:(id)sender;
-(IBAction)btnRememberMeTapped:(id)sender;
@end

