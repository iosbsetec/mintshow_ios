//
//  AddMintViewController.h
//  Your Show
//
//  Created by Siba Prasad Hota  on 8/12/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"
#import "BaseFeedModel.h"
@protocol addMintDelegate;


@interface AddMintViewController : UIViewController

@property (nonatomic, assign) id <addMintDelegate> delegate;
@property (nonatomic, assign) BOOL            isFromMintDetail;

@property (nonatomic, strong) VideoModel      *anewModel;
@property (nonatomic, strong) BaseFeedModel   *feedModelData;
@property (nonatomic, weak)   IBOutlet UIView *viewCulture;
@end


@protocol addMintDelegate
@required
-(void)postMintDidClicked:(VideoModel *)someModel;
@end