//
//  GalleryOptionView.h
//  Awesome Media Gallery
//
//  Created by Siba Prasad Hota on 29/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <UIKit/UIKit.h>




@class GalleryOptionView;

@protocol OptionViewDataSource <NSObject>

@optional

@required

- (NSString *)OptionView:(GalleryOptionView *)optionView TitleForItemAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)numberOfSectionsInoptionView:(GalleryOptionView *)optionView;
- (NSInteger)optionView:(GalleryOptionView *)optionView numberOfRowsInSection:(NSInteger)section;

- (NSInteger)optionView:(GalleryOptionView *)optionView numberOfItemsInindexPath:(NSIndexPath *)indexPath;

@end





@protocol OptionViewDelegate <NSObject>

-(void)galleryOptionViewDidclickedAtIndexPath:(NSIndexPath*)indexPath;

@end


@interface GalleryOptionView : UIView <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,assign)  id <OptionViewDelegate> aDelegate;
@property (nonatomic,assign)  id <OptionViewDataSource> aDatasource;

@property (nonatomic,assign)  BOOL mediaTypeImage;
-(void)reloadOptionView;
- (id)initWithFrame:(CGRect)frame mediaType : (BOOL)mediaType;

@end
