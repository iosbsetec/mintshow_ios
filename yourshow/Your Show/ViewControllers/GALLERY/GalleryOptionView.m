//
//  GalleryOptionView.m
//  Awesome Media Gallery
//
//  Created by Siba Prasad Hota on 29/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import "GalleryOptionView.h"

@implementation GalleryOptionView
{
    NSString *title;
    UITableView *cameraDDTbl;
}




- (id)initWithFrame:(CGRect)frame mediaType : (BOOL)mediaType{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _mediaTypeImage = mediaType;
        [self setupTableViewView];
    }
    
    return self;
}



- (void)setupTableViewView
{
    cameraDDTbl                = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 200, 0)];
    cameraDDTbl.delegate                    = self;
    cameraDDTbl.dataSource                  = self;
    cameraDDTbl.separatorColor              = [UIColor blackColor];
    cameraDDTbl.backgroundColor             = [UIColor colorWithRed:(10.0/255.0) green:(19.0/255.0) blue:(40.0/255.0) alpha:1.0];
    cameraDDTbl.layer.cornerRadius          = 3.0;
    cameraDDTbl.layer.masksToBounds = YES;
//    cameraDDTbl.layer.shadowOffset = CGSizeMake(-15, 20);
//    cameraDDTbl.layer.shadowRadius = 5;
//    cameraDDTbl.layer.shadowOpacity = 0.5;
    cameraDDTbl.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self addSubview:cameraDDTbl];
}

-(void)reloadOptionView{
    [cameraDDTbl reloadData];
}

#pragma mark ---  TableView DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.aDatasource numberOfSectionsInoptionView:self];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    NSLog(@"Count =%lu",(unsigned long)self.cameraItemsArray.count);
    return [self.aDatasource optionView:self numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cameraRollTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cameraRollTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cameraRollTableIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    cell.selectionStyle         = UITableViewCellSelectionStyleNone;
    cell.textLabel.font         = [UIFont fontWithName:@"Helvetica" size:17];
    cell.textLabel.textColor    = [UIColor whiteColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%d)",[self.aDatasource OptionView:self TitleForItemAtIndexPath:indexPath],(int)[self.aDatasource optionView:self numberOfItemsInindexPath:indexPath]];
    return cell;
}


#pragma mark ---  TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Did Select");
    [self.aDelegate galleryOptionViewDidclickedAtIndexPath:indexPath];

}

@end
