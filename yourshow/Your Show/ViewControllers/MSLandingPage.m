//
//  Your Show
//
//  Created by Siba Prasad Hota on 05/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MSLandingPage.h"
#import "JitenCollectionView.h"
#import "MultiMintCollectionView.h"
#import "ShowRoomModel.h"
#import "LoginOnBoard.h"
#import "SignUpOnBoard.h"
#import "TermsAndAgree.h"
#import "MyShowViewController.h"
#import "DiscoverDataModel.h"
#import "Userfeed.h"
#import "UserAccessSession.h"
#import  "FBFriendsListVC.h"
#import "YSCollectionView.h"
#import "FacebookInfoVC.h"
#import "PPImageScrollingCellView.h"

@interface MSLandingPage ()<DiscoverTableViewDataSource,DiscoverTableDelegate,JitenCollectionTableDelegate,JitenCollectionViewDataSource,YSCollectionViewDataSource,YSCollectionViewDelegate,MSFacebookDelegate>
{
    NSMutableArray *allShowRoomdata;
    NSMutableArray *TrendingShowRoomdata;
    NSMutableArray *TrendingTagsData;
    NSMutableArray *DiscoverMintsdata;
    
    BOOL isLoading;
    BOOL isloggedIN;

    BOOL isDataFinishedInServer;
    int  pagenumber,pagenumberShowroom;
    
    BOOL LoadingDataSourceforDiscoverMint;
    BOOL LoadingDataSourceforMultiMint;
    BOOL LoadingDataSourceforMultishowroom;


    
    CGFloat discoverMintHeight;
    CGFloat MultiMintHeight;
    CGFloat multiShowroomHeight;
    
    NSString *email;
    NSString *firstName;
    NSString *lastname;
    NSString *gender;
    NSString *fbid;
    NSString *fbUserImage;

    
    IBOutlet UIView *viewSubPopUpHolder;
    IBOutlet UIView *viewPopUpHolderBG;
    IBOutlet UIView *viewPopUp;
    
    NSMutableArray *friendslist;
    FBFriends *fbProfileImages;
    
    __weak IBOutlet UIButton *btnSignUp;
    __weak IBOutlet UIButton *btnLogin;
}
@property (strong, nonatomic) NSArray *images;
//@property(nonatomic,strong)     IBOutlet UISegmentedControl *selectedSegment;

@property (strong, nonatomic) IBOutlet DiscoverTableview *DiscoverAllTable;
@property (strong, nonatomic) IBOutlet JitenCollectionView *showRoomCollectionview;
@property (strong, nonatomic) IBOutlet YSCollectionView *aView;
@property(nonatomic,assign)YSLoginType *logintype;


@end

@implementation MSLandingPage

- (void)viewDidLoad
{
    [super viewDidLoad];
    isloggedIN = [self isLoggedIn];
    if (!isloggedIN){
        [self initializer];
    }
    self.DiscoverAllTable.showArrowButton = YES;
    friendslist = [[NSMutableArray alloc]init];
    
    viewSubPopUpHolder.layer.cornerRadius = 8.0;
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapinPopUp:)];
    [viewPopUpHolderBG addGestureRecognizer:singleFingerTap];

    // Do any additional setup after loading the view from its nib.
    
    btnLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    btnLogin.layer.borderWidth = 1.5;
    btnLogin.layer.cornerRadius = 2.0;
    btnSignUp.layer.cornerRadius = 2.0;
    [btnLogin setClipsToBounds:YES];
    btnSignUp.layer.borderColor = [UIColor whiteColor].CGColor;
    btnSignUp.layer.borderWidth = 1.5;
    [btnSignUp setClipsToBounds:YES];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [viewPopUp setFrame:appDel.window.frame];
}

- (void)handleSingleTapinPopUp:(UITapGestureRecognizer *)recognizer {
    [viewPopUp removeFromSuperview];
}

- (IBAction)headerLoginTapped:(UIButton *)sender {
    

        [self btnLoginTapped:btnLogin];


}
- (IBAction)headerSignTapped:(UIButton *)sender
    {
        [self.view addSubview:viewPopUp];

    }
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (isloggedIN)  {
        [self gotoTabbar:nil];
    }
}


-(BOOL)isLoggedIn {
  Userfeed *session = [UserAccessSession getUserSession];
    if (session.login_Status==1) {
        return YES;
    }
    return NO;
}



- (IBAction)gotoTabbar:(id)sender{
    if(![self isLoggedIn]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"oops!" message:@"Please Login" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appDell authenticationDidFinish:self selectedInde:0];

}


-(void)initializer
{
    UIColor *backgroundCol =  [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];

    // MULTI MINT COLLECTION VIEW
    self.aView.backgroundColor = backgroundCol;
    self.aView.showsHorizontalScrollIndicator = NO;
    self.aView.scrollEnabled    = NO;
    self.aView.ysDataSource     = self;
    self.aView.ysDelegate       = self;
    self.aView.bounces          = NO;
    self.aView.isForLandingPage = YES;
    
    
       // ALL TABLE VIEW
    self.DiscoverAllTable.DiscoverDataSource = self;
    self.DiscoverAllTable.Discoverdelegate   = self;
    
    allShowRoomdata      = [[NSMutableArray alloc]init];
    TrendingShowRoomdata = [[NSMutableArray alloc]init];
    TrendingTagsData     = [[NSMutableArray alloc]init];
    DiscoverMintsdata    = [[NSMutableArray alloc]init];
    isLoading = NO;
    
    LoadingDataSourceforDiscoverMint  = YES;
    LoadingDataSourceforMultiMint     = YES;
    LoadingDataSourceforMultishowroom = YES;
    
    isDataFinishedInServer = NO;
    
    pagenumber = 1;
    pagenumberShowroom =1;
    discoverMintHeight  = 0;
    MultiMintHeight     = 0;
    multiShowroomHeight = 400;

    self.aView.frame = CGRectMake(3, 5, self.view.frame.size.width, discoverMintHeight);
    [MBProgressHUD showHUDAddedTo:_DiscoverAllTable animated:YES];

    [self getTrendingsShowroom:pagenumberShowroom];
    [self getMintsMyshow:pagenumber];
    
    
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissPopup) name:@"CLosePopupNow" object:nil];
}

- ( NSInteger)DiscoverTableViewNumberOfsections:(DiscoverTableview *)tableView
{
    return 2;
}

-(CGFloat)DiscoverTableView:(DiscoverTableview *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0){
        return 130;
    }
    
    return discoverMintHeight;
}

-(CGFloat)DiscovertableView:(DiscoverTableview *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.0;
}


- (DiscoverViewCellType)DiscoverTableView:(DiscoverTableview *)tableView cellTypeForSection:(NSInteger)section
{
    if (section == 0)
        return DiscoverViewCellTypeImageScroller;
    else
        return DiscoverViewCellTypeMultiMint;
}


- (NSString *)DiscoverTableView:(DiscoverTableview *)tableView TitleOfHeaderForSection:(NSInteger)section
{
    if (section == 0)
        return @"TRENDING SHOWROOMS";
    else if (section == 1)
        return  @"TRENDING MINTS";
    
    else return @"";
}

- (NSDictionary *)DiscoverTableView:(DiscoverTableview *)tableView DataForSection:(NSInteger)section
{
    if (section == 0) {
        return [NSDictionary dictionaryWithObjectsAndKeys:TrendingShowRoomdata ,@"images", nil];
        
    }
    return [self.images objectAtIndex:section];
    
}

#pragma THIS CODE FOR DISCOVER TABLE

- (void)didSelectSelectRow:(NSInteger)row inSection:(NSInteger)section
{

        MyShowViewController *allShow = [[MyShowViewController alloc]initWithNibName:@"MyShowViewController" bundle:nil];
        [self.navigationController pushViewController:allShow animated:YES];
}
- (void)didSelectSelectAllButtonClickedforSection:(NSInteger) section
{
    
}



- (void)dismissPopup {
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;

}

#pragma THIS CODE FOR MINT SCROLLER

- (void)scrollingTableViewDidSelectImageAtIndexPath:(NSIndexPath*)indexPathOfImage Withdata:(DiscoverDataModel*)data atCategoryRowIndex:(NSInteger)categoryRowIndex
 {
   [self.view addSubview:viewPopUp];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma THIS CODE FOR MULTI MINT

-(YSCollectionView *)discoverCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.aView;
}

-(void)multiMintCollectionViewClicked:(YSCollectionView *)sender AtRow:(NSInteger)row
{
    
}
- (BaseFeedModel *)multiMintCollectionViewView:(YSCollectionView *)collectionView DataForItemAtRow:(NSInteger)row
{
    return [DiscoverMintsdata objectAtIndex:row];
}

- (NSInteger)NumberOFSectionsMultiMintView:(YSCollectionView *)collectionView{
    return 1;
}

- (NSInteger)NumberOFItemsForMultiMintView:(YSCollectionView *)collectionView forSection:(NSInteger)section{
    return DiscoverMintsdata.count;
}

-(void)complimintClicked:(BaseFeedModel *)model
{
    [self.view addSubview:viewPopUp];
}

- (void)LoadNextPagetriggeredForSection:(NSInteger)section;
{
    if ([DiscoverMintsdata count]>=40) {
        
        if (![viewPopUp superview]) {
            [self.view addSubview:viewPopUp];

        }

    }
    
    if(!isLoading)
    {

            pagenumber = pagenumber+1;
            [self getMintsMyshow:pagenumber];
    }
}




-(void)getMintsMyshow:(int)pageNumber
{
    isLoading = YES;
    
    NSString*  submitData    = [NSString stringWithFormat:@"page_number=%d",pagenumber] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"customshowroom/trending_mints_landing",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    
    NSLog(@"submitDta123 data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isLoading = NO;
         
         [MBProgressHUD hideHUDForView:_DiscoverAllTable animated:YES];

         Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[YSSupport dictionaryFromResponseData:data] methodName:METHOD_MYSHOW_MINT];
      
         if ([gmodel.status_code integerValue])
                      {
                          if([gmodel.tempArray count]>0)
                          {
                              isDataFinishedInServer = NO;
                              [DiscoverMintsdata addObjectsFromArray: gmodel.tempArray];
                              [self batchreloadCollectionView];
                          }
                          else
                          {
                              isDataFinishedInServer = YES;
                          }
                      }
         
     }];

}
-(void)getTrendingsShowroom:(int)pageNumber
{
    isLoading = YES;
    
    NSString*  submitData    = [NSString stringWithFormat:@"page_number=%d",pagenumberShowroom] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"customshowroom/trending_showroom_landing",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    NSLog(@"submitDta123 data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isLoading = NO;
      
         
         // [allShowRoomdata removeAllObjects];
         NSDictionary *showroomDict = [self dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"Status"]);
         if ([[showroomDict valueForKey:@"Status"]isEqualToString:@"Success"])
         {
            
             NSDictionary *showroomDict = [self dictionaryFromResponseData:data];
             NSLog(@"SHOWROOMS ****** = %@",showroomDict);
             
             if([[showroomDict valueForKey:@"trending_showroom_list"] count]>0)
             {
                 isDataFinishedInServer = NO;
 
             for(NSDictionary *showRoom in [showroomDict valueForKey:@"trending_showroom_list"])
             {
                 [TrendingShowRoomdata addObject:[[DiscoverDataModel alloc]initWithDiscoverInfo:showRoom ForTrendingShowroom:YES ]];
             }
                 if (pagenumberShowroom > 1) {
                     [self.DiscoverAllTable reloadData];
                 }
                 
             }
             
             else
             {
                 isDataFinishedInServer = YES;
             }
             
             
         }
         else
         {
             isDataFinishedInServer = YES;

         
         
         }
     
     
     }];

    
}




-(void)batchreloadCollectionView
{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.aView performBatchUpdates:^{
        [self.aView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
            discoverMintHeight = self.aView.collectionViewLayout.collectionViewContentSize.height;
            CGFloat indexy = 5;
            self.aView.frame = CGRectMake(3, indexy, self.view.frame.size.width, discoverMintHeight);
            [self.DiscoverAllTable reloadData];
        }
    }];

}




-(IBAction)btnCloseTapped:(id)sender
{
    [viewPopUp removeFromSuperview];
}


- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}



-(void)reloadTableviewWithsection:(NSInteger)section
{
    [self.DiscoverAllTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:section], nil]  withRowAnimation:UITableViewRowAnimationNone];
}

-(void)gotoMintDetailsScreen{
    [self.view addSubview:viewPopUp];

}

-(void)hashTagClicked:(EntityModel *)selectedEnitity
{
    [self.view addSubview:viewPopUp];
}
- (void)remintClicked :(Feedmodel *)feedData
{
    [self.view addSubview:viewPopUp];

}
- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell{

    [self.view addSubview:viewPopUp];
}


-(void)gotoCommentScreen : (Feedmodel *)model
{
    [self.view addSubview:viewPopUp];

}

-(void)commentScreen : (Feedmodel *)model withSelectedMintTag :(NSInteger) tag
{
    [self.view addSubview:viewPopUp];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self.DiscoverAllTable removeFromSuperview];
    [allShowRoomdata removeAllObjects];
    [TrendingTagsData removeAllObjects];
    [TrendingShowRoomdata removeAllObjects];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btnEmailTapped:(id)sender
{
    [viewPopUp removeFromSuperview];

    SignUpOnBoard *viewSignUp = [[SignUpOnBoard alloc]initWithNibName:@"SignUpOnBoard" bundle:nil];
    [self.navigationController pushViewController:viewSignUp animated:YES];
}

-(IBAction)btnTermsAndAgreementTapped:(id)sender
{
    [viewPopUp removeFromSuperview];

    TermsAndAgree *viewTerms = [[TermsAndAgree alloc]initWithNibName:@"TermsAndAgree" bundle:nil];
    [self.navigationController pushViewController:viewTerms animated:YES];
}

-(IBAction)btnLoginTapped:(id)sender
{
    [viewPopUp removeFromSuperview];

    LoginOnBoard *viewLogin = [[LoginOnBoard alloc]initWithNibName:@"LoginOnBoard" bundle:nil];
    [self.navigationController pushViewController:viewLogin animated:YES];
}

- (void)selectdFeature :(NSString *)selectedEnitity withType:(int)type{
   
    [self.view addSubview:viewPopUp];

}


- (IBAction)FBSignUpTapped:(id)sender {
    [viewPopUp removeFromSuperview];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email",@"user_location",@"user_birthday",@"user_hometown"]
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    if ([FBSDKAccessToken currentAccessToken]) {
                                        NSString *access_token=[FBSDKAccessToken currentAccessToken].tokenString;
                                        SETVALUE(access_token, CEO_FacebookAccessToken);
                                        SYNCHRONISE;
                                        
                                        NSLog(@"Token is available : %@",GETVALUE(CEO_FacebookAccessToken));
                                        
                                        
                                        
                                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields": @"id,picture,first_name, last_name, email, gender"} tokenString:GETVALUE(CEO_FacebookAccessToken) version:nil HTTPMethod:@"GET"]
                                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                                         
                                         
                                         {
                                             if (!error) {
                                                 NSLog(@"fetched user:%@", result);
                                                 email = [result objectForKey:@"email"];
                                                 firstName = [result objectForKey:@"first_name"];
                                                 lastname = [result objectForKey:@"last_name"];
                                                 gender = [result objectForKey:@"gender"];
                                                 fbid = [result objectForKey:@"id"];
                                                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal",[result valueForKey:@"id"]] forKey:CEO_FBUSERIMAGE];
                                                 SYNCHRONISE;
                                                 [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                                 
                                                 [self createFBAccount:YES];
                                                 
                                             }
                                             
                                             else
                                             {
                                                 NSLog(@"error is %@",error.description);
                                             }
                                         }];
                                    }
                                    
                                }
                            }];
}

-(void)openFacebookPage
{


    FacebookInfoVC *facebookVC = [[FacebookInfoVC alloc]initWithNibName:@"FacebookInfoVC" bundle:nil];
    facebookVC.delegate = self;
    facebookVC.strFBUserEmailId = email;
    facebookVC.strFBUserFirstName = firstName;
    facebookVC.strFBUserLastName = lastname;
    facebookVC.strFBUserId = fbid;
    facebookVC.strFBUserImage = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal",fbid];
    
    
    [self.navigationController presentViewController:facebookVC animated:YES completion:^{
        
    }];
}
-(void)signUpwith:(NSString *)fName lastName:(NSString *)lName emailId:(NSString *)emailId;
{
    email = nil;
    email = emailId;
    firstName = nil;
    firstName = fName;
    lastname = lName;

    [self createFBAccount:NO];

}

-(void)continueWithEmail
{
    [self btnEmailTapped:nil];
}
-(void)continueWithLogin
{
    [self btnLoginTapped:nil];



}
-(IBAction)createFBAccount :(BOOL) checkAccountExist
{
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSData*  submitData = nil;
    if (checkAccountExist) {
         submitData    = [[NSString stringWithFormat:@"social_id=%@&social_type=%@",fbid,@"FB"]dataUsingEncoding:NSUTF8StringEncoding];
    }
    else
    submitData    = [[NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&social_id=%@&social_type=%@&gender=%@&referral_id=%@&lat=%@&lang=%@&device_token=%@&device_id=%@&device_type=iOS&app_version=%@&dob=%@&user_image_url=%@",firstName,lastname,email,email,fbid,@"FB",gender,@"",@"",@"",[appDelegate getDeviceTkenFromKeyChain],[appDelegate getDeviceID],@"",@"",GETVALUE(CEO_FBUSERIMAGE)]dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString*req = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"request is %@",req);
    [YSAPICalls signUp:submitData ForSuccessionBlock:^(id newResponse) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         Userfeed *model = newResponse;
        
        if ([model.status_code isEqualToString:@"1"]) {
            if ( checkAccountExist && ([model.status_Msg isEqualToString:@"Account does not exist"]|| [model.status_Msg isEqualToString:@"Invalid email"])) {
                [self openFacebookPage];
            }
            else{
                [self SaveUserDetail:model];
                NSLog(@"Login Success .......");
                self.logintype = YSLoginTypeFacebook;
                AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];

                model.email = email;
                [MSMixPanelTrackings trackSignupWithUserDetails:model];
                if ([model.discover_all_page_first_time_visit isEqualToString:@"yes"]) {
                    [appDell authenticationDidFinish:self selectedInde:1];

                }
               else
              [appDell authenticationDidFinish:self selectedInde:0];


            }
        }
        
    }
       andFailureBlock:^(NSError *error) {
           [MBProgressHUD hideHUDForView:self.view animated:YES];
           
           NSLog(@"Login Fail .......");
           
       }];
    
}


- (void)SaveUserDetail:(Userfeed*)response
{
    [UserAccessSession storeUserSession:response];
    SETVALUE(response.User_id,CEO_UserId);
    SETVALUE(@"yes",CEO_ISFACEBOOKLOGIN);
    SETVALUE(response.AccessToken,CEO_AccessToken);
    SETVALUE(response.User_id,CEO_UserId);
    SETVALUE(response.Referral_id,CEO_ReferalID);
    SETVALUE(response.discover_all_page_first_time_visit,CEO_DISCOVERALLFIRSTTIMEVISIT);
    SETVALUE(response.User_id,CEO_UserId);
    SETVALUE(response.userName,CEO_UserName);
    SETVALUE(response.user_image_url,CEO_UserImage);
    SETVALUE(response.first_name,CEO_UserFirstName);
    SETVALUE(response.last_name,CEO_UserLastName);
    SETVALUE(response.showroom_page_first_time_visit,CEO_PARTCULARSHOWROOMFIRSTTIMEVISIT);
    SETVALUE(response.is_already_posted_mint,CEO_ISMINTALREADYPOSTED);
    SETVALUE(response.mint_detail_page_first_time_visit,CEO_ISMINTDETAILFIRSTTIMEVISIT);
    SETVALUE(response.is_joined_showrooms,CEO_ISJOINEDSHOWROOM);
    SETVALUE(response.discover_all_page_first_time_visit,CEO_DISCOVERALLFIRSTTIMEVISIT);
    SETVALUE(response.addmint_culture_first_time_visit,CEO_ISADDMINTFIRSTTIMEVISIT);

    SYNCHRONISE;
}

-(void)getfriendslist
{
    NSDictionary *limitDictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"200",@"limit", @"id,picture,name",@"fields", nil];
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:limitDictionary tokenString:GETVALUE(CEO_FacebookAccessToken) version:nil HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             for (NSDictionary *dict in [result objectForKey:@"data"])
             {
                 fbProfileImages = [[FBFriends alloc]initWithFriendInfo:dict];
                 [friendslist addObject:fbProfileImages.friendId];
                 if([friendslist count])
                     SETVALUE(@"yes",CEO_ISFBMINTSHOWFRIEND);
                     SYNCHRONISE;
             }
         }
         else
         {
             NSLog(@"error == %@",error.description);
         }
     }];
}

-(void)selectedFeatureType:(int)featureType withString:(NSString *)selectedEnitity isPrivateShowroomAccess:(BOOL)access{
    [self.view addSubview:viewPopUp];

}
- (void)loadNextShowroomPages{
    if(!isLoading && !isDataFinishedInServer){
            pagenumberShowroom = pagenumberShowroom+1;
            [self getTrendingsShowroom:pagenumber];
    }
}

@end
