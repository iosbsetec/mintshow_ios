//
//  MSPSignatureShowroomVC.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 31/05/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscoverDataModel.h"


@class MultiMintCollectionView,EntityModel;

@protocol MSPSignatureShowroomDelegate
- (void)showroomJoinUnjoin:(NSString *)showTagName memberCount:(NSString *)row join:(BOOL)isJoiend;
@end



@interface MSPSignatureShowroomVC : UIViewController

@property (nonatomic, strong) NSString          *selectedShowroom;
@property (nonatomic, strong) NSString          *signatureshowroomShowtag;
@property (strong, nonatomic) DiscoverDataModel *modelShow;
@property (nonatomic, strong) NSString          *sendersName;

@property (nonatomic, assign) BOOL isFromDeepLinking,isFromProfileShowroom;
@property (nonatomic, assign) BOOL isFromDiscoverShowroom;
@property (nonatomic, retain) id   <MSPSignatureShowroomDelegate> delegate;

@end
