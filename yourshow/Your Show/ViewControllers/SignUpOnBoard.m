//
//  ViewController.m
//  OnBoardDemo
//
//  Created by Bsetec on 14/01/16.
//  Copyright (c) 2016 bsetec. All rights reserved.
//
#import "SignUpOnBoard.h"
#import "PopUpList.h"
#import "AppDelegate.h"
#import "RoundedRect.h"
#import "Userfeed.h"
#import "UserAccessSession.h"
#define MONTHLIST @[@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December"]

#define MAXTEXTLENGTH 20
#define ERRORTAG 404


#define GENDERLIST @[@"Male", @"Female", @"Other"]
#define kVALIDFIRSTNAME @" First Name Must Have\n Atleast 2 Characters"
#define kVALIDLASTNAME @" Last Name Must Have \n Atleast 2 Characters"
#define kVALIDEMAILID @" Invalid Email"
#define kNEEDEMAILID @" Please Enter Email"
#define kVALIDPASSWORDLENGTH @" Password Must Have Atleast 6 Characters"
#define kVALIDCONFIRMPASSWORD @" Password Don't Match. Try again"
#define kVALIDFIRSTNAMEEXCEED @" First Name Should Not\n Exceed 20 Characters"
#define kVALIDLASTNAMEEXCEED @" Last Name Should Not\n Exceed 20 Characters"
#define kVALIDPASSWORDEXCEED @" Password Should Not Exceed 20 Characters"
#define kNEEDFIRSTNAME @" Please Enter\n First Name"
#define kNEEDLASTNAME @" Please Enter\n Last Name"
#define kNEEDCONFIRMPASSWORD @" Please Enter Password"
#define kNEEDPASSWORD @" Please Enter Password"
#define kIMPROPERFIRSTNAME @" First Name Can Only \n Contain Alphanumeric\n Characters!"
#define kIMPROPERLASTTNAME @" Last Name Can Only \n Contain Alphanumeric\n Characters!"


@implementation SignUpOnBoard
{
    IBOutlet UILabel *lblMonth;
    IBOutlet UILabel *lblYear;
    IBOutlet UILabel *lblGender;
    IBOutlet UITextField *txtFldFirstName;
    IBOutlet UITextField *txtFldLastName;
    IBOutlet UITextField *txtFldEmail;
    IBOutlet UITextField *txtFldPassword;
    IBOutlet UITextField *txtFldConfirmPassword;
    
    IBOutlet UIButton *btnFirstName;
    IBOutlet UIButton *btnLastName;
    IBOutlet UIButton *btnEmail;
    IBOutlet UIButton *btnPassword;
    IBOutlet UIButton *btnConfirmPassword;
    
    
    IBOutlet RoundedRect *viewFirstName;
    IBOutlet RoundedRect *viewLastName;
    IBOutlet RoundedRect *viewEmail;
    IBOutlet RoundedRect *viewPassword;
    IBOutlet RoundedRect *viewConfirmPassword;
    
    
    IBOutlet UIButton *btnDone;
    PopUpList *listedview;
    
    CGRect doneRect;
    IBOutlet RoundedRect *btnDoneView;
    UILabel *lblTitle;
    
    BOOL isFirstNameExceed,isLastNameExceed,isEmailFieldEmpty,isPasswordExceed,isConfirmPasswordExceed,isImproperFirstname,isImproperLastname;
    NSInteger selectedGender;

}

-(void)viewDidLoad
{
    [super viewDidLoad];
//    lblYear.layer.borderColor = [UIColor redColor].CGColor;
//    lblYear.layer.borderWidth = 2.0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardShowing:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(KeyboardHiding:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [self makeValidationButtonAlpha:0.0];
    
    doneRect       = btnDoneView.frame;
    selectedGender = -1;
}


-(IBAction)onFirstNameValidationTapped:(id)sender
{
    if (![[viewFirstName viewWithTag:ERRORTAG] superview])
    {
        if([txtFldFirstName.text length] == 0)
            [self onCreateErrorMsg:viewFirstName title:kNEEDFIRSTNAME xOrigin:30 yOrigin:2 width:85 tipHeight:30];
        else if(![self validateSpecialCharactor:txtFldFirstName.text])
            [self onCreateErrorMsg:viewFirstName title:kIMPROPERFIRSTNAME xOrigin:0 yOrigin:-2 width:115 tipHeight:35];
        else
            [self onCreateErrorMsg:viewFirstName title:(isFirstNameExceed)?kVALIDFIRSTNAMEEXCEED:kVALIDFIRSTNAME xOrigin:0 yOrigin:2 width:115 tipHeight:30];
    }
    else
        [[viewFirstName viewWithTag:ERRORTAG] removeFromSuperview];
}

-(IBAction)onLastNameValidationTapped:(id)sender
{
    if (![[viewLastName viewWithTag:ERRORTAG] superview]) {
        if([txtFldLastName.text length] == 0)
            [self onCreateErrorMsg:viewLastName title:kNEEDLASTNAME xOrigin:30 yOrigin:2 width:85 tipHeight:30];
        else if (![self validateSpecialCharactor:txtFldLastName.text])
            [self onCreateErrorMsg:viewLastName title:kIMPROPERLASTTNAME xOrigin:0 yOrigin:-2 width:115 tipHeight:35];
         else
             [self onCreateErrorMsg:viewLastName title:(isLastNameExceed)?kVALIDLASTNAMEEXCEED:kVALIDLASTNAME xOrigin:0 yOrigin:2 width:115 tipHeight:30];
    }
    else
        [[viewLastName viewWithTag:ERRORTAG] removeFromSuperview];
}

-(IBAction)onEmailValidationTapped:(id)sender
{
    if (![[viewEmail viewWithTag:ERRORTAG] superview])
    {
        if([txtFldEmail.text length] == 0)
            [self onCreateErrorMsg:viewEmail title:kNEEDEMAILID xOrigin:163 yOrigin:2 width:100 tipHeight:24];
        else
            [self onCreateErrorMsg:viewEmail title:(isEmailFieldEmpty)?kNEEDEMAILID:kVALIDEMAILID xOrigin:(isEmailFieldEmpty)?163:188 yOrigin:2 width:(isEmailFieldEmpty)?100:75 tipHeight:24];
    }
    else
        [[viewEmail viewWithTag:ERRORTAG] removeFromSuperview];
}

-(IBAction)onPasswordValidationTapped:(id)sender
{
    if (![[viewPassword viewWithTag:ERRORTAG] superview]) {
        if([txtFldPassword.text length] == 0)
            [self onCreateErrorMsg:viewPassword title:kNEEDPASSWORD xOrigin:143 yOrigin:2 width:120 tipHeight:24];
        else
            [self onCreateErrorMsg:viewPassword title:(isPasswordExceed)?kVALIDPASSWORDEXCEED:kVALIDPASSWORDLENGTH xOrigin:(isPasswordExceed)?53:63 yOrigin:2 width:(isPasswordExceed)?210:200 tipHeight:24];
    }
    else
        [[viewPassword viewWithTag:ERRORTAG] removeFromSuperview];
}

-(IBAction)onConfirmPasswordValidationTapped:(id)sender
{
    if (![[viewConfirmPassword viewWithTag:ERRORTAG] superview])
    {
        if([txtFldConfirmPassword.text length] == 0)
            [self onCreateErrorMsg:viewConfirmPassword title:kNEEDCONFIRMPASSWORD xOrigin:103 yOrigin:2 width:120 tipHeight:24];
        else
            [self onCreateErrorMsg:viewConfirmPassword title:(isConfirmPasswordExceed)?kVALIDPASSWORDEXCEED:kVALIDCONFIRMPASSWORD xOrigin:(isConfirmPasswordExceed)?53:98 yOrigin:2 width:(isConfirmPasswordExceed)?210:165 tipHeight:24];
    }
    else
        [[viewConfirmPassword viewWithTag:ERRORTAG] removeFromSuperview];
}

-(void)onCreateErrorMsg:(UIView *)view title:(NSString *)strTitle xOrigin:(CGFloat)xPos yOrigin:(CGFloat)yPos width:(CGFloat)width  tipHeight:(CGFloat)height
{
//    [[self.view viewWithTag:ERRORTAG]removeFromSuperview];

    UIView *viewTips = [[UIView alloc]initWithFrame:CGRectMake(xPos, yPos, width, 36)];
    viewTips.backgroundColor = [UIColor clearColor];
    viewTips.layer.cornerRadius = 4.0;
    [view addSubview:viewTips];
    
    lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, (height==36)?0:6, width-10, height)];
    lblTitle.text = strTitle;
    lblTitle.backgroundColor = [UIColor whiteColor];
    lblTitle.font = [UIFont fontWithName:@"HelveticaNeue" size:10];
//    lblTitle.textAlignment = NSTextAlignmentLeft;
    lblTitle.textColor = [[UIColor blackColor]colorWithAlphaComponent:0.7];
    lblTitle.layer.borderColor = [UIColor lightGrayColor].CGColor;
    lblTitle.layer.borderWidth = 0.4;
    lblTitle.numberOfLines = 0;
    lblTitle.layer.cornerRadius = 4.0;
    [lblTitle setClipsToBounds:YES];
    [viewTips addSubview:lblTitle];

    UIImageView *imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(lblTitle.frame.size.width-3, 10, 14,16)];
    imgArrow.image = [UIImage imageNamed:@"rightarrow.png"];
    [imgArrow setTag:123];
    [viewTips addSubview:imgArrow];
    
    [viewTips setTag:ERRORTAG];
    [self makeLabelAnimation:viewTips];
}

-(void)makeLabelAnimation:(UIView *)viewTips
{
    CGRect rectLbl = lblTitle.frame;
    rectLbl.size.width = 0;
    rectLbl.origin.x = lblTitle.frame.size.width;
    lblTitle.frame = rectLbl;
    
    [UIView animateWithDuration:0.5
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect rectLbl2 = lblTitle.frame;
                         rectLbl2.size.width = viewTips.frame.size.width-10;
                         rectLbl2.origin.x = 0;
                         lblTitle.frame = rectLbl2;
                     } completion:^(BOOL finished)
                     {
                     }];
}

- (BOOL)validateSpecialCharactor: (NSString *) text {
    NSString *Regex = @"[A-Za-z0-9^]*";
    NSPredicate *TestResult = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
    return [TestResult evaluateWithObject:text];
}
-(void)removeAllErrorValidation
{
    [[self.view viewWithTag:ERRORTAG]removeFromSuperview];
}

-(void)KeyboardShowing:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int height = MIN(keyboardSize.height,keyboardSize.width);
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if (isSmallPad) // Changes for iPad done by mani
        btnDoneView.frame = CGRectMake(0, appDel.window.frame.size.height-height-40-self.view.frame.origin.y, self.view.window.frame.size.width, 40);
    else
        btnDoneView.frame = CGRectMake(0, self.view.window.frame.size.height-height-40, self.view.window.frame.size.width, 40);
}

-(void)KeyboardHiding:(NSNotification *)notification
{
    btnDoneView.frame =  CGRectMake(20, doneRect.origin.y, self.view.window.frame.size.width-40, 40);
    if (isSmallPad)
        [self changeYoriginForiPadScreen:0];
}
-(void)changeYoriginForiPadScreen:(int)yPos
{
    [UIView animateWithDuration:0.5 delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect rectView = self.view.frame;
        rectView.origin.y = yPos;
        [self.view setFrame:rectView];
    }
                     completion:^(BOOL finished){
                     }];
}
-(IBAction)btnBackTapped:(id)sender
{
    [self.view endEditing:YES];

    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnMonthTapped:(id)sender
{
    selectedGender = -1;

    [self.view endEditing:YES];
    
    [self showPopView:MONTHLIST selectedType:@"Month"];
}
- (IBAction)tapGuestureForView:(UITapGestureRecognizer *)sender {
    
    [self.view endEditing:YES];
}

-(IBAction)btnYearTapped:(id)sender
{
    selectedGender = -1;

    [self.view endEditing:YES];

    [self showPopView:nil selectedType:@"Year"];
}

-(IBAction)btnGenderTapped:(id)sender
{
    if ([[lblGender.text lowercaseString] isEqualToString:@"male"]) {
        selectedGender = 0;
    }
    else  if([[lblGender.text lowercaseString] isEqualToString:@"female"]) {
        selectedGender = 1;
    }
    else if([[lblGender.text lowercaseString] isEqualToString:@"other"])
        selectedGender = 2;
    
    [self.view endEditing:YES];
    [self showPopView:GENDERLIST selectedType:@"Gender"];
}

-(IBAction)btnDoneTapped:(id)sender
{
        if (![[self.view viewWithTag:ERRORTAG] superview])
            [[self.view viewWithTag:ERRORTAG]removeFromSuperview];
    
    [self.view endEditing:YES];
//    [self removeAllErrorValidation];
    BOOL isOK = [self checkUserGivenDetails];
    if (isOK)
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.view endEditing:YES];
//        NSData*  submitData    = [[NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&gender=%@",txtFldFirstName.text,txtFldLastName.text,txtFldEmail.text,txtFldPassword.text,lblGender.text]dataUsingEncoding:NSUTF8StringEncoding];
        AppDelegate *appDell  = (AppDelegate      *)[UIApplication sharedApplication].delegate;
        NSData*  submitData    = [[NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&social_id=%@&social_type=%@&gender=%@&referral_id=%@&lat=%@&lang=%@&device_token=%@&device_id=%@&device_type=iOS&app_version=%@&dob=%@&user_image_url=%@",txtFldFirstName.text,txtFldLastName.text,txtFldEmail.text,txtFldPassword.text,@"",@"",lblGender.text,@"",@"",@"",[appDell getDeviceTkenFromKeyChain],[appDell getDeviceID],@"",@"",@""]dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString*req = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
        NSLog(@"request is %@",req);
        [YSAPICalls signUp:submitData ForSuccessionBlock:^(id newResponse) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            Userfeed *model = newResponse;
            
            if ([model.status_code isEqualToString:@"1"] && [model.status isEqualToString:@"Success"]) {
             

                NSLog(@"Signup Success .......");

                [self SaveUserDetail:model];
                model.email = txtFldEmail.text;
                [MSMixPanelTrackings trackSignupWithUserDetails:model];
                [appDell authenticationDidFinish:self selectedInde:1];
                [appDell updateCounterValue:0];

            }
            else
            {
                
                [appDell onCreateToastMsgInWindow:model.status_Msg];
                [appDell updateCounterValue:0];
            }
            
            NSLog(@"success");
        } andFailureBlock:^(NSError *error) {

            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"error");
        }];
    }
}

- (void)SaveUserDetail:(Userfeed*)response
{
    [UserAccessSession storeUserSession:response];

    SETVALUE(response.AccessToken,CEO_AccessToken);
    SETVALUE(response.User_id,CEO_UserId);
    SETVALUE(response.Referral_id,CEO_ReferalID);
    SETVALUE(response.User_id,CEO_UserId);
    SETVALUE(response.userName,CEO_UserName);
    SETVALUE(response.user_image_url,CEO_UserImage);
    SETVALUE(response.first_name,CEO_UserFirstName);
    SETVALUE(response.last_name,CEO_UserLastName);
    SETVALUE(response.showroom_page_first_time_visit,CEO_PARTCULARSHOWROOMFIRSTTIMEVISIT);
    SETVALUE(response.is_already_posted_mint,CEO_ISMINTALREADYPOSTED);
    SETVALUE(response.mint_detail_page_first_time_visit,CEO_ISMINTDETAILFIRSTTIMEVISIT);
    SETVALUE(response.is_joined_showrooms,CEO_ISJOINEDSHOWROOM);
    SETVALUE(response.mint_detail_page_first_time_visit,CEO_ISMINTDETAILFIRSTTIMEVISIT);
    SETVALUE(response.addmint_culture_first_time_visit,CEO_ISADDMINTFIRSTTIMEVISIT);
    SETVALUE(response.discover_all_page_first_time_visit,CEO_DISCOVERALLFIRSTTIMEVISIT);
    SYNCHRONISE;
}

-(void)showPopView:(NSArray *)arrDatas selectedType:(NSString *)strType
{
    listedview = [[PopUpList alloc]setDatasToTableView:arrDatas selectedType:strType frame:self.view.bounds lastSelectdInfo:selectedGender] ;
    listedview.delegate = self;
}

#pragma mark - PopUpList Delegate

-(void)setDataFromPopUpList:(NSString *)strData type:(NSString *)strType
{
    if ([strType isEqualToString:@"Month"])
    {
        lblMonth.text = strData;
    }
    else if ([strType isEqualToString:@"Year"])
    {
        lblYear.text = strData;
    }
    else if ([strType isEqualToString:@"Gender"])
    {
        
        lblGender.text = strData;
    }
}

#pragma mark - ******* Validate Email ID *******
-(BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

-(void)makeValidationButtonAlpha:(CGFloat)alpha
{
    btnFirstName.alpha = alpha;
    btnLastName.alpha = alpha;
    btnEmail.alpha = alpha;
    btnPassword.alpha = alpha;
    btnConfirmPassword.alpha = alpha;
}

-(NSString *)removeWhiteSpaceChar:(NSString *)currentStr
{
    return [currentStr stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
}

-(BOOL)checkUserGivenDetails
{

    if ([self removeWhiteSpaceChar:txtFldFirstName.text].length==0 && [self removeWhiteSpaceChar:txtFldLastName.text].length==0 && [self removeWhiteSpaceChar:txtFldEmail.text].length==0 && [self removeWhiteSpaceChar:txtFldPassword.text].length==0 && [self removeWhiteSpaceChar:txtFldConfirmPassword.text].length==0)
    {
        [self makeValidationButtonAlpha:1.0];
        [btnConfirmPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        [btnFirstName setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"]       forState:UIControlStateNormal];
        [btnLastName setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"]        forState:UIControlStateNormal];
        [btnEmail setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"]           forState:UIControlStateNormal];
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"]        forState:UIControlStateNormal];
   
        if (![[viewFirstName viewWithTag:ERRORTAG] superview]) {
            [self onCreateErrorMsg:viewFirstName title:kNEEDFIRSTNAME xOrigin:30 yOrigin:2 width:85 tipHeight:30];
        }
        if (![[viewLastName  viewWithTag:ERRORTAG] superview]) {
            [self onCreateErrorMsg:viewLastName title:kNEEDLASTNAME xOrigin:30 yOrigin:2 width:85 tipHeight:30];
        }
        if (![[viewEmail viewWithTag:ERRORTAG] superview]) {
            [self onCreateErrorMsg:viewEmail title:kNEEDEMAILID xOrigin:163 yOrigin:2 width:100 tipHeight:24];
        }
        if (![[viewPassword viewWithTag:ERRORTAG] superview]) {
            [self onCreateErrorMsg:viewPassword title:kNEEDPASSWORD xOrigin:143 yOrigin:2 width:120 tipHeight:24];
        }
        
        if (![[viewConfirmPassword  viewWithTag:ERRORTAG] superview]) {
            [self onCreateErrorMsg:viewConfirmPassword title:kNEEDCONFIRMPASSWORD xOrigin:143 yOrigin:2 width:120 tipHeight:24];
        }
        return NO;
    }
    BOOL isAllOk = YES;
    
    if(![self validateSpecialCharactor:txtFldFirstName.text])
    {
        isAllOk = NO;
        [self checkImproperFirstname];
    }

    
    if ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 1)
    {
        isFirstNameExceed = NO;
        isAllOk = NO;
        [self checkValidFirstname];
    }
    else if ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] > MAXTEXTLENGTH)
    {
        isFirstNameExceed = YES;
        isAllOk = NO;
        [self checkValidFirstname];
    }
    
    else if ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 0)
    {
        isAllOk = NO;
        [self needFirstName];
    }
    
    if(![self validateSpecialCharactor:txtFldLastName.text])
    {
        isAllOk = NO;
        [self checkImproperLasttname];
    }

    
    if ([[self removeWhiteSpaceChar:txtFldLastName.text] length] == 1)
    {
        isLastNameExceed = NO;
        isAllOk = NO;
        [self checkValidLastname];
    }
    else if ([[self removeWhiteSpaceChar:txtFldLastName.text] length] > MAXTEXTLENGTH)
    {
        isLastNameExceed = YES;
        isAllOk = NO;
        [self checkValidLastname];
    }
    
    else if ([[self removeWhiteSpaceChar:txtFldLastName.text] length] == 0)
    {
        isAllOk = NO;
        [self needLastName];
    }

    
    if ([[self removeWhiteSpaceChar:txtFldEmail.text] length]==0)
    {
        isEmailFieldEmpty = YES;
        isAllOk = NO;
        [self checkEmail];
    }
    else if (![YSSupport validateEmail:[self removeWhiteSpaceChar:txtFldEmail.text]])
    {
        isEmailFieldEmpty = NO;
        isAllOk = NO;
        [self checkEmail];
    }
    
    if ([[self removeWhiteSpaceChar:txtFldPassword.text] length] < 6)
    {
        isPasswordExceed = NO;
        [btnPassword setUserInteractionEnabled:YES];
        isAllOk = NO;
        btnPassword.alpha = 1.0;
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        if (![[viewPassword viewWithTag:ERRORTAG] superview]) {
        
            if([txtFldPassword.text length] == 0)
                [self onCreateErrorMsg:viewPassword title:kNEEDPASSWORD xOrigin:143 yOrigin:2 width:120 tipHeight:24];
            else
                [self onCreateErrorMsg:viewPassword title:kVALIDPASSWORDLENGTH xOrigin:53 yOrigin:2 width:210 tipHeight:24];
        }
    }
    else if ([[self removeWhiteSpaceChar:txtFldPassword.text] length] >MAXTEXTLENGTH)
    {
        isPasswordExceed = YES;
        [btnPassword setUserInteractionEnabled:YES];
        isAllOk = NO;
        btnPassword.alpha = 1.0;
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        if (![[viewPassword viewWithTag:ERRORTAG] superview]) {
            [self onCreateErrorMsg:viewPassword title:kVALIDPASSWORDEXCEED xOrigin:43 yOrigin:2 width:220 tipHeight:24];
        }

    }
    
    
    if ([[self removeWhiteSpaceChar:txtFldConfirmPassword.text] length] < 6)
    {
        isConfirmPasswordExceed = NO;
        [btnConfirmPassword setUserInteractionEnabled:YES];
        isAllOk = NO;
        btnConfirmPassword.alpha = 1.0;
        [btnConfirmPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        if (![[viewConfirmPassword viewWithTag:ERRORTAG] superview])
        {
            if([txtFldConfirmPassword.text length] == 0)
                [self onCreateErrorMsg:viewConfirmPassword title:kNEEDCONFIRMPASSWORD xOrigin:143 yOrigin:2 width:120 tipHeight:24];
            else
                [self onCreateErrorMsg:viewConfirmPassword title:(isConfirmPasswordExceed)?kVALIDPASSWORDEXCEED:kVALIDCONFIRMPASSWORD xOrigin:(isConfirmPasswordExceed)?53:98 yOrigin:2 width:(isConfirmPasswordExceed)?210:165 tipHeight:24];
        }
    }
    else if ([[self removeWhiteSpaceChar:txtFldConfirmPassword.text] length] >MAXTEXTLENGTH)
    {
        isConfirmPasswordExceed = YES;
        [btnConfirmPassword setUserInteractionEnabled:YES];
        isAllOk = NO;
        btnConfirmPassword.alpha = 1.0;
        [btnConfirmPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        if (![[viewConfirmPassword viewWithTag:ERRORTAG] superview])
        {
            [self onCreateErrorMsg:viewConfirmPassword title:(isConfirmPasswordExceed)?kVALIDPASSWORDEXCEED:kVALIDCONFIRMPASSWORD xOrigin:(isConfirmPasswordExceed)?43:83 yOrigin:2 width:(isConfirmPasswordExceed)?220:180 tipHeight:24];
        }
    }
    
    if (![[self removeWhiteSpaceChar:txtFldPassword.text] isEqualToString:[self removeWhiteSpaceChar:txtFldConfirmPassword.text]])
    {
        [btnConfirmPassword setUserInteractionEnabled:YES];
        isAllOk = NO;
        btnConfirmPassword.alpha = 1.0;
        [btnConfirmPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        if (![[viewConfirmPassword viewWithTag:ERRORTAG] superview]) {
            [self onCreateErrorMsg:viewConfirmPassword title:(isConfirmPasswordExceed)?kVALIDPASSWORDEXCEED:kVALIDCONFIRMPASSWORD xOrigin:83 yOrigin:2 width:180 tipHeight:24];
        }
        
    }
    else
    {
        if ([self removeWhiteSpaceChar:txtFldConfirmPassword.text].length)
        {
            [btnConfirmPassword setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
            btnConfirmPassword.alpha = 1.0;
            [btnConfirmPassword setUserInteractionEnabled:NO];
            if ([[viewConfirmPassword viewWithTag:ERRORTAG] superview])
                [[viewConfirmPassword viewWithTag:ERRORTAG] removeFromSuperview];
        }
        else
        {
            isAllOk = NO;
            [btnConfirmPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
            btnConfirmPassword.alpha = 1.0;
            [btnConfirmPassword setUserInteractionEnabled:YES];
            if (![[viewConfirmPassword viewWithTag:ERRORTAG] superview]) {
                [self onCreateErrorMsg:viewConfirmPassword title:(isConfirmPasswordExceed)?kVALIDPASSWORDEXCEED:kVALIDCONFIRMPASSWORD xOrigin:(isConfirmPasswordExceed)?53:83 yOrigin:2 width:(isConfirmPasswordExceed)?210:180 tipHeight:24];
            }

        }
    }
    return isAllOk;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == txtFldFirstName)
    {
        if ([[viewFirstName viewWithTag:ERRORTAG] superview]) {
            [[viewFirstName viewWithTag:ERRORTAG] removeFromSuperview];
        }
        [btnFirstName setUserInteractionEnabled:NO];
        btnFirstName.alpha = 0.0;
        if (isSmallPad)// Changes for iPad done by mani
            [self changeYoriginForiPadScreen:0];
    }
    if(textField == txtFldLastName)
    {
        if([[viewLastName viewWithTag:ERRORTAG] superview])
        {
            [[viewLastName viewWithTag:ERRORTAG] removeFromSuperview];
        }
        [btnLastName setUserInteractionEnabled:NO];
        btnLastName.alpha = 0.0;
        if (isSmallPad)// Changes for iPad done by mani
            [self changeYoriginForiPadScreen:0];
    }
    if(textField == txtFldEmail)
    {
        
        
        if([[viewEmail viewWithTag:ERRORTAG] superview])
        {
            [[viewEmail viewWithTag:ERRORTAG] removeFromSuperview];
        }
        [btnEmail setUserInteractionEnabled:NO];
        btnEmail.alpha = 0.0;
        if (isSmallPad)// Changes for iPad done by mani
            [self changeYoriginForiPadScreen:-50];
    }
    if(textField == txtFldPassword)
    {
        if (isSmallPad)// Changes for iPad done by mani
            [self changeYoriginForiPadScreen:-100];
        
        if([[viewPassword viewWithTag:ERRORTAG] superview])
        {
            [[viewPassword viewWithTag:ERRORTAG] removeFromSuperview];
        }
        [btnPassword setUserInteractionEnabled:NO];
        btnPassword.alpha = 0.0;
    }
    if(textField == txtFldConfirmPassword)
    {
        if (isSmallPad)// Changes for iPad done by mani
            [self changeYoriginForiPadScreen:-140];
        
        if([[viewConfirmPassword viewWithTag:ERRORTAG] superview])
        {
            [[viewConfirmPassword viewWithTag:ERRORTAG] removeFromSuperview];
        }
        [btnConfirmPassword setUserInteractionEnabled:NO];
        btnConfirmPassword.alpha = 0.0;
    }

    
    if(textField == txtFldLastName && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 1))
    {
        isFirstNameExceed = NO;
        [self checkValidFirstname];

    }
    if(textField == txtFldLastName && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] > MAXTEXTLENGTH))
    {
        isFirstNameExceed = YES;
        [self checkValidFirstname];
        }
    
     if(textField == txtFldLastName && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 0))
    {
        [self needFirstName];
    }
    
    
    if(textField == txtFldLastName && ![self validateSpecialCharactor:txtFldFirstName.text])
    {
        [self checkImproperFirstname];
    }
    
        
    if (textField == txtFldEmail && ![self validateSpecialCharactor:txtFldFirstName.text])
    {
        [self checkImproperFirstname];
    }
    
    if (textField == txtFldEmail && ![self validateSpecialCharactor:txtFldLastName.text])
    {
        [self checkImproperLasttname];
    }

   if (textField == txtFldEmail &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] == 1))
    {
        isLastNameExceed = NO;
        [self checkValidLastname];
    }
    if (textField == txtFldEmail &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] > MAXTEXTLENGTH))
    {
        isLastNameExceed = YES;
        [self checkValidLastname];
    }
    
     if (textField == txtFldEmail &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] == 0))
    {
        [self needLastName];
    }
    
    if (textField == txtFldEmail && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 0))
    {
        [self needFirstName];
    }
    
     if (textField == txtFldEmail && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 1))
    {
        isFirstNameExceed = NO;
        [self checkValidFirstname];
    }
    if (textField == txtFldEmail && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] > MAXTEXTLENGTH))
    {
        isFirstNameExceed = YES;
        [self checkValidFirstname];
    }

   if(textField == txtFldPassword && ![YSSupport validateEmail:[self removeWhiteSpaceChar:txtFldEmail.text]])
    {
        isEmailFieldEmpty = NO;
        [self checkEmail];
    }
    
    if(textField == txtFldPassword &&  ([[self removeWhiteSpaceChar:txtFldEmail.text] length]==0) )
    {
        isEmailFieldEmpty = YES;
        [self checkEmail];
    }
    
    if(textField == txtFldPassword &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] == 1))
    {
        isLastNameExceed = NO;
        [self checkValidLastname];

    }
    
    if(textField == txtFldPassword &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] > MAXTEXTLENGTH))
    {
        isLastNameExceed = YES;
        [self checkValidLastname];

    }
    
    if(textField == txtFldPassword  &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] == 0))
    {
        [self needLastName];
    }

    if(textField == txtFldPassword  && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 0))
    {
        [self needFirstName];
    }
    if(textField == txtFldPassword  && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 1))
    {
        isFirstNameExceed = NO;
        [self checkValidFirstname];

    }
    if(textField == txtFldPassword && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] > MAXTEXTLENGTH))
        {
            isFirstNameExceed = YES;
            [self checkValidFirstname];
        }

   if(textField == txtFldPassword && ![self validateSpecialCharactor:txtFldFirstName.text])
    {
        [self checkImproperFirstname];
    }
    
    
    if (textField == txtFldPassword && ![self validateSpecialCharactor:txtFldLastName.text])
    {
        [self checkImproperLasttname];
    }

    

    if(textField == txtFldConfirmPassword && ![self validateSpecialCharactor:txtFldFirstName.text])
    {
        [self checkImproperFirstname];
    }
    
    if (textField == txtFldConfirmPassword && ![self validateSpecialCharactor:txtFldLastName.text])
    {
        [self checkImproperLasttname];
    }

    
    if (textField == txtFldConfirmPassword && [[self removeWhiteSpaceChar:txtFldPassword.text] length] < 6)
    {
        isPasswordExceed= NO;
        [btnPassword setUserInteractionEnabled:YES];
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        btnPassword.alpha = 1.0;
        if (![[viewPassword viewWithTag:ERRORTAG] superview])
        {
            if([txtFldPassword.text length] == 0)
                [self onCreateErrorMsg:viewPassword title:kNEEDPASSWORD xOrigin:143 yOrigin:2 width:120 tipHeight:24];
            else
                [self onCreateErrorMsg:viewPassword title:kVALIDPASSWORDLENGTH xOrigin:53 yOrigin:2 width:210 tipHeight:24];
        }
    }
   if (textField == txtFldConfirmPassword && [[self removeWhiteSpaceChar:txtFldPassword.text] length] == 0)
  {
      isPasswordExceed= NO;
      [btnPassword setUserInteractionEnabled:YES];
      [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
      btnPassword.alpha = 1.0;
      if (![[viewPassword viewWithTag:ERRORTAG] superview]) {
      [self onCreateErrorMsg:viewPassword title:kNEEDPASSWORD  xOrigin:53 yOrigin:2 width:210 tipHeight:24];
      }
  }
    
    if(textField == txtFldConfirmPassword && [[self removeWhiteSpaceChar:txtFldPassword.text] length] > MAXTEXTLENGTH)
    {
        isPasswordExceed= YES;
        [btnPassword setUserInteractionEnabled:YES];
        [btnPassword setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
        btnPassword.alpha = 1.0;
        if (![[viewPassword viewWithTag:ERRORTAG] superview]) {
            [self onCreateErrorMsg:viewPassword title:kVALIDPASSWORDEXCEED xOrigin:43 yOrigin:2 width:220 tipHeight:24];
        }
    }
   
    if (textField == txtFldConfirmPassword &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] == 1))
    {
        isLastNameExceed = NO;
        [self checkValidLastname];

    }
  
    if (textField == txtFldConfirmPassword &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] > MAXTEXTLENGTH))
    {
        isLastNameExceed = YES;
        [self checkValidLastname];

    }

    if (textField == txtFldConfirmPassword &&  ([[self removeWhiteSpaceChar:txtFldLastName.text] length] == 0))
    {
        [self needLastName];
    }
    
    
    if (textField == txtFldConfirmPassword && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 0))
    {
        [self needFirstName];
    }

    if (textField == txtFldConfirmPassword  && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] == 1))
    {
        isFirstNameExceed = NO;
        [self checkValidFirstname];
    }

    if (textField == txtFldConfirmPassword   && ([[self removeWhiteSpaceChar:txtFldFirstName.text] length] > MAXTEXTLENGTH))
    {
        isFirstNameExceed = YES;
        [self checkValidFirstname];
    }

    
    if (textField == txtFldConfirmPassword   && ![YSSupport validateEmail:[self removeWhiteSpaceChar:txtFldEmail.text]])
    {
        isEmailFieldEmpty = NO;
        [self checkEmail];
    }

    
    if (textField == txtFldConfirmPassword  &&  ([[self removeWhiteSpaceChar:txtFldEmail.text] length]==0) ){
        isEmailFieldEmpty = YES;
        [self checkEmail];
    }
    
    return YES;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if( (textField == txtFldFirstName && !([[self removeWhiteSpaceChar:txtFldFirstName.text] length] <= 1) && [self validateSpecialCharactor:txtFldFirstName.text] && !([[self removeWhiteSpaceChar:txtFldFirstName.text] length] > MAXTEXTLENGTH) ))
    {
        btnFirstName.alpha = 1.0;
        [btnFirstName setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
    }
    
   else if((textField == txtFldLastName && !([[self removeWhiteSpaceChar:txtFldLastName.text] length] <= 1) && [self validateSpecialCharactor:txtFldLastName.text] && !([[self removeWhiteSpaceChar:txtFldLastName.text] length] > MAXTEXTLENGTH) ))
    {
        btnLastName.alpha = 1.0;
        [btnLastName setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
    }
   else if(textField == txtFldEmail && [YSSupport validateEmail:[self removeWhiteSpaceChar:txtFldEmail.text]]&& ![[self removeWhiteSpaceChar:txtFldEmail.text] length] == 0)
   {
       btnEmail.alpha = 1.0;
       [btnEmail setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
   }

   else if(textField == txtFldPassword && !([[self removeWhiteSpaceChar:txtFldPassword.text] length]> MAXTEXTLENGTH) && !([[self removeWhiteSpaceChar:txtFldPassword.text] length] < 6 ) )
   {
       btnPassword.alpha = 1.0;
       [btnPassword setBackgroundImage:[UIImage imageNamed:@"CircleCheckicon.png"] forState:UIControlStateNormal];
   }
}


-(IBAction)onTxtFldDidChange:(UITextField *)textField
{
  
}

-(void)checkImproperFirstname
{
    isImproperFirstname = YES;
    [btnFirstName setUserInteractionEnabled:YES];
    btnFirstName.alpha = 1.0;
    [btnFirstName setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
    if (![[viewFirstName viewWithTag:ERRORTAG] superview]) {
        [self onCreateErrorMsg:viewFirstName title:kIMPROPERFIRSTNAME xOrigin:0 yOrigin:-2 width:115 tipHeight:35];
    }
}

-(void)checkImproperLasttname
{
   isImproperLastname = YES;
   [btnLastName setUserInteractionEnabled:YES];
   btnLastName.alpha = 1.0;
   [btnLastName setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
   if (![[viewLastName viewWithTag:ERRORTAG] superview]) {
    [self onCreateErrorMsg:viewLastName title:kIMPROPERLASTTNAME xOrigin:0 yOrigin:-2 width:115 tipHeight:35];
}
}

-(void)passwordExceed
{
    
}


-(void)needFirstName
{
    [btnFirstName setUserInteractionEnabled:YES];
    btnFirstName.alpha = 1.0;
    [btnFirstName setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
    if (![[viewFirstName viewWithTag:ERRORTAG] superview]) {
        [self onCreateErrorMsg:viewFirstName title:kNEEDFIRSTNAME xOrigin:30 yOrigin:2 width:85 tipHeight:30];
    }

}

-(void)needLastName
{
    [btnLastName setUserInteractionEnabled:YES];
    btnLastName.alpha = 1.0;
    [btnLastName setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
    if (![[viewLastName viewWithTag:ERRORTAG] superview]) {
        [self onCreateErrorMsg:viewLastName title:kNEEDLASTNAME xOrigin:30 yOrigin:2 width:85 tipHeight:30];
    }
}

-(void)checkEmail
{
    [btnEmail setUserInteractionEnabled:YES];
    btnEmail.alpha = 1.0;
    [btnEmail setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
    if (![[viewEmail viewWithTag:ERRORTAG] superview])
    {
        if([txtFldEmail.text length] == 0)
            [self onCreateErrorMsg:viewEmail title:kNEEDEMAILID xOrigin:163 yOrigin:2 width:100 tipHeight:24];
        else
            [self onCreateErrorMsg:viewEmail title:(isEmailFieldEmpty)?kNEEDEMAILID:kVALIDEMAILID xOrigin:(isEmailFieldEmpty)?163:188 yOrigin:2 width:(isEmailFieldEmpty)?100:75 tipHeight:24];
    }
}

-(void)checkValidFirstname
{
    [btnFirstName setUserInteractionEnabled:YES];
    btnFirstName.alpha = 1.0;
    [btnFirstName setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
    if (![[viewFirstName viewWithTag:ERRORTAG] superview]) {
        [self onCreateErrorMsg:viewFirstName title:(isFirstNameExceed)?kVALIDFIRSTNAMEEXCEED:kVALIDFIRSTNAME xOrigin:0 yOrigin:2 width:115 tipHeight:30];
    }

}

-(void)checkValidLastname
{
    [btnLastName setUserInteractionEnabled:YES];
    btnLastName.alpha = 1.0;
    [btnLastName setBackgroundImage:[UIImage imageNamed:@"Error_icon.png"] forState:UIControlStateNormal];
    if (![[viewLastName viewWithTag:ERRORTAG] superview]) {
        [self onCreateErrorMsg:viewLastName title:(isLastNameExceed)?kVALIDLASTNAMEEXCEED:kVALIDLASTNAME xOrigin:0 yOrigin:2 width:115 tipHeight:30];
        
    }

}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

