//
//  SecondViewController.h
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MSPShowroomDelegate <NSObject>
-(void)updateTabsCountWithCount:(int)count ForTab:(NSInteger)tab;

-(void)makeScrollUpWithContenYPosition:(CGFloat)viewHeight;
-(void)makeScrollDownWithContenYPosition:(CGFloat)viewHeight;
@end


@interface MSPShowroomVC : UIViewController
@property (strong, nonatomic)  NSString *profileUserId;
@property(nonatomic,retain)id<MSPShowroomDelegate>psdelegate;
@property (strong, nonatomic) NSString  *createdShowRoomCount;
@property (strong, nonatomic) NSString *joinedShowRoomCount;

@end
