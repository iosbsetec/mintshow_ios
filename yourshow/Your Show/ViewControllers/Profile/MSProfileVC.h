//
//  ViewController.h
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfilePagerViewController.h"



@protocol MSProfileVCDelegate

@optional

-(void)updateFollowUserFromProfile:(NSString *)userId;

@end



@interface MSProfileVC : ProfilePagerViewController

@property (strong, nonatomic)  NSString *selectdUserName;
@property (strong, nonatomic)  NSString *profileUserId;
@property (weak, nonatomic)  IBOutlet UIView *viewUserDetailsInfo;
@property (nonatomic, assign)    id <MSProfileVCDelegate> delegateProfile;
@property (assign, nonatomic)  BOOL isFromMintdetails;
@property     __weak IBOutlet UIButton *btnProfileback;

@end

