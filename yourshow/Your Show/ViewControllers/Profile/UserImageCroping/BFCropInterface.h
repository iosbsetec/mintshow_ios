//
//  BFCropInterface.h



#import <UIKit/UIKit.h>

@interface BFCropInterface : UIImageView {
    BOOL isPanning;
    NSInteger currentTouches;
    CGPoint   panTouch;
    CGFloat   scaleDistance;
    UIView    *currentDragView;
    
    UIView *topView;
    UIView *bottomView;
    UIView *leftView;
    UIView *rightView;
    
    UIView *topLeftView;
    UIView *topRightView;
    UIView *bottomLeftView;
    UIView *bottomRightView;

    UIImageView *tlnode;
    UIImageView *trnode;
    UIImageView *blnode;
    UIImageView *brnode;
    
    CGFloat nodeRadius;
}
@property (nonatomic, assign) CGRect crop;
@property (nonatomic, strong) UIView *cropView;
@property (nonatomic, strong) UIColor *shadowColor;
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic) CGFloat borderWidth;
@property (nonatomic) BOOL showNodes;
@property (nonatomic) CGFloat nodeRadius;

- (id)initWithFrame:(CGRect)frame andImage:(UIImage *)image nodeRadius:(CGFloat)nodeRadius;
- (UIImage*)getCroppedImage;
- (void)setCropViewPosition:(CGFloat)x y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height;

@end
