//
//  BFViewController.m
//  CropDemo
//
//  Created by John Nichols on 2/28/13.
//  Copyright (c) 2013 John Nichols. All rights reserved.
//

#import "BFViewController.h"
static inline CGFloat degreesToRadians(CGFloat degrees) {
    return (degrees) * M_PI / 180.0f;
}
@interface BFViewController ()
{

    CGFloat rotatevalue;



}
@end

@implementation BFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // make your image view content mode == aspect fit
    // yields best results
    self.displayImage.contentMode = UIViewContentModeScaleAspectFit;
    //_displayImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    // must have user interaction enabled on view that will hold crop interface
    self.displayImage.userInteractionEnabled = YES;
   // self.displayImage.frame = CGRectMake(20, 20, 280, 360);
    self.originalImage = self.selectdImage;
//    self.displayImage.image = self.originalImage;

    // ** this is where the magic happens
    
    // allocate crop interface with frame and image being cropped
    self.cropper = [[BFCropInterface alloc]initWithFrame:self.displayImage.bounds andImage:self.originalImage nodeRadius:2];
    // this is the default color even if you don't set it
    self.cropper.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.80];
    // white is the default border color.
    self.cropper.borderColor = ORANGECOLOR;
    self.cropper.borderWidth = 1.0;
    self.cropper.showNodes = YES;
    [self.cropper setCropViewPosition:50.0 y:100.0 width:150.0 height:150.0];

    // add interface to superview. here we are covering the main image view.
    [self.displayImage addSubview:self.cropper];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cropPressed:(id)sender {
   
    
    if ([self.delegate respondsToSelector:@selector(ImageCropViewControllerSuccess:didFinishCroppingImage:)])
    {
        // crop image
        UIImage *croppedImage = [self.cropper getCroppedImage];
        
        // remove crop interface from superview
        [self.cropper removeFromSuperview];
        self.cropper = nil;
        
        // display new cropped image
        self.displayImage.image = nil;
        [self.delegate ImageCropViewControllerSuccess:self didFinishCroppingImage:croppedImage];
    }
    
    
}

- (IBAction)retake:(id)sender {
    if ([self.delegate respondsToSelector:@selector(ImageCropViewControllerRetake:)])
    {
        [self.delegate ImageCropViewControllerRetake:self];

    }
}
- (IBAction)cancel:(id)sender {
    if ([self.delegate respondsToSelector:@selector(ImageCropViewControllerDidCancel:)])
    {
        [self.delegate ImageCropViewControllerDidCancel:self];
        
    }
}

- (IBAction)btnRotate:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^
     {
         rotatevalue = rotatevalue +45.0;
         // self.cropView.transform = CGAffineTransformMakeRotation(M_PI/2);
         [self rotateVoizdLogo:rotatevalue];
     }];
    
    
    
}

-(void)rotateVoizdLogo:(CGFloat)degreeToRotate {
    
    [UIView beginAnimations:@"rotate" context:nil];
    [UIView setAnimationDuration:0.5];
    self.displayImage.transform = CGAffineTransformMakeRotation(degreesToRadians(degreeToRotate));
    [UIView commitAnimations];
    
}

-(void)rotateAntiClockWise:(id)sender {
    
    [UIView beginAnimations:@"rotate" context:nil];
    [UIView setAnimationDuration:0.5];
    
    self.displayImage.transform = CGAffineTransformMakeRotation(degreesToRadians(360));
    [UIView commitAnimations];
}

/*
NSLog(@"SIZE =%@",NSStringFromCGSize(self.selectdImage.size));
self.displayImage.frame = CGRectMake(0, 50,
                                     (self.selectdImage.size.width>[UIScreen mainScreen].bounds.size.width)?([UIScreen mainScreen].bounds.size.width):self.selectdImage.size.width, (self.selectdImage.size.height>(([UIScreen mainScreen].bounds.size.height)- 135))?(([UIScreen mainScreen].bounds.size.height)- 135):self.selectdImage.size.height);


// self.displayImage.center = CGPointMake(self.view.center.x - self.view.frame.origin.x, self.view.center.y- self.view.frame.origin.y);

*/

@end
