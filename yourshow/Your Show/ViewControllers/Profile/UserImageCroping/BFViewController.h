//
//  BFViewController.h
//  CropDemo
//
//  Created by John Nichols on 2/28/13.
//  Copyright (c) 2013 John Nichols. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BFCropInterface.h"



#pragma mark ImageCropViewController interface
@protocol UserImageCropViewControllerDelegate <NSObject>

- (void)ImageCropViewControllerSuccess:(UIViewController* )controller didFinishCroppingImage:(UIImage *)croppedImage;
- (void)ImageCropViewControllerDidCancel:(UIViewController *)controller;
- (void)ImageCropViewControllerRetake:(UIViewController *)controller;


@end


@interface BFViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIImageView *displayImage;
@property (nonatomic, strong) UIImage *originalImage;
@property (nonatomic, strong) BFCropInterface *cropper;
@property (nonatomic, strong) UIImage *selectdImage;
@property (nonatomic, weak) id<UserImageCropViewControllerDelegate> delegate;

- (IBAction)cropPressed:(id)sender;
//- (IBAction)originalPressed:(id)sender;

@end
