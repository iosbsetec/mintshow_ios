//
//  ViewController.m
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import "MSProfileVC.h"


#import "MSPMintsVC.h"
#import "MSPShowroomVC.h"
#import "MSPFollowVC.h"
#import "MSPStatsVC.h"
#import "MSPBioVC.h"
#import "UserAccessSession.h"
#import "ProfileEditViewController.h"
#import "MSSettingVC.h"

#define KLOCATIONPLACEHOLDER @"Add Location"
#define KWEBSITEPLACEHOLDER @"Add Website"
#import "StreakCircleView.h"

@interface MSProfileVC ()<ProfileViewPagerDataSource,ProfileViewPagerDelegate,UIWebViewDelegate,MSPMintsVCDelegate,MSPFollowerDelegate,MSPShowroomDelegate,MSPStatsDelegate>
{
    CGFloat currentScreenHeight;

    IBOutlet UIView    *viewMorePopUp;
    IBOutlet UIView    *viewSettings;
    IBOutlet UIWebView *webSettingsView;
    IBOutlet UILabel   *userName;
    IBOutlet UILabel   *acheivementText;
    IBOutlet UILabel   *lblWebsite;
    IBOutlet UIButton  *btnEdit;
    IBOutlet UILabel   *lblLocation;
    IBOutlet UIButton  *btnHeaderFollow;
    IBOutlet UIImageView *imgViewEditProfile;
    __weak IBOutlet UIView *viewHeader;
    
    IBOutlet StreakCircleView *viewStreak;
    IBOutlet UIView *viewStreakGray;
    CGFloat profileHeaderHeight;
    IBOutlet UILabel   *lblStreakInfo;

    Profilemodel       * userProfilemodel;
    NSString           *strCurrentUserId;
    IBOutlet UIView    *viewLocation;
    
    BOOL _isNavigated;
    
    NSInteger selectedTabIndexVal;
    IBOutlet UIImageView *userThumbImg;
    IBOutlet UILabel *lblUserName;

    __weak IBOutlet UIButton *btnInvite;

}
@property (nonatomic) NSUInteger numberOfTabs;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;

- (IBAction)backButtonPressed:(UIButton *)sender;

@end

@implementation MSProfileVC

- (void)viewDidLoad {

    [super viewDidLoad];

    [userThumbImg setHidden:YES];
    [lblUserName setHidden:YES];
    userThumbImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
    userThumbImg.layer.borderWidth = 2.0;
    userThumbImg.layer.cornerRadius = userThumbImg.frame.size.width / 2;
    userThumbImg.clipsToBounds = YES;
    
    // Do any additional setup after loading the view from its nib.
    self.dataSource = self;
    self.delegate = self;
    // Do any additional setup after loading the view, typically from a nib.
    
//    self.userImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.userImage.layer.borderWidth = 2.0;
    self.userImage.layer.cornerRadius = self.userImage.frame.size.width / 2;
    self.userImage.clipsToBounds = YES;
    
    CGRect rectHeader = viewHeader.frame;
    rectHeader.origin.y = 0;
    [viewHeader setFrame:rectHeader];
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegateApp.window addSubview:viewHeader];
    [self.view.window addSubview:viewHeader];
    
    currentScreenHeight = self.view.frame.size.height;
    CGRect rect = self.view.frame;
    rect.size.height = self.view.frame.size.height+_viewUserDetailsInfo.frame.size.height;
    self.view.frame = rect;

    
    if ([_profileUserId length]) {
        _btnProfileback.hidden = NO;
    }
    
    if (![GETVALUE(CEO_UserId) isEqualToString:self.profileUserId] && self.profileUserId.length ) {
        imgViewEditProfile.hidden = NO;
        btnEdit.hidden = NO;
        btnHeaderFollow.hidden = YES;
        btnInvite.hidden = YES;
    }
    
    else
    {
        self.profileUserId = GETVALUE(CEO_UserId);
        btnHeaderFollow.hidden = YES;
        btnInvite.hidden = NO;

    }

    viewStreakGray.layer.cornerRadius = viewStreakGray.frame.size.width/2;
    viewStreakGray.layer.borderWidth = 2.5;
    viewStreakGray.layer.borderColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0].CGColor;
    
    userProfilemodel = [Profilemodel new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileUpdated:) name:KPROFILEEDITED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProfileUsersIsInCircle:) name:KPROFILEINCIRCLE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showStreakInfo:) name:@"StreakNotification" object:nil];

}
-(void)viewDidAppear:(BOOL)animated
{
    [self getUserProfile];
    [super viewDidAppear:animated];
}
-(void)profileUpdated : (NSNotification *)notification
{
    userProfilemodel = (Profilemodel*)[[notification valueForKey:@"userInfo"] valueForKey:@"profileInfo"];
    [self setUpUserInfo];
    [[NSNotificationCenter defaultCenter] postNotificationName:KPROFILEIMAGEUPDATED object:[userProfilemodel user_thumb_image]];

}
-(void)uploadNextProfile:(NSString *)selectedUserId
{
  //  if(![GETVALUE(CEO_UserId) isEqualToString:selectedEnitity] )

    MSProfileVC *cvc = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    cvc.profileUserId = selectedUserId;
    [self.navigationController pushViewController:cvc animated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [viewHeader setHidden:NO];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self makeScrollUpWithContenYPosition:0];
    [viewHeader setHidden:YES];
    [MBProgressHUD hideHUDForView:self.view animated:YES];

}

/*! This API call is to get profile information of user. */

-(void)getUserProfile
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue:GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:self.profileUserId        forKey:@"user_id"];
    [YSAPICalls getUserProfile:dicts ForSuccessionBlock:^(id newResponse) {
        userProfilemodel = newResponse;
        if ([userProfilemodel.status_code integerValue]){
           
            [self setStatusofProfileHeader];
            if ([userProfilemodel.follow_status isEqualToString:@"No"] && ![self.profileUserId isEqualToString:GETVALUE(CEO_UserId)]) {
               selectedTabIndexVal = 0;
                [self createUserDetails:NO];

            }
            else
                selectedTabIndexVal = 0;
 
            [self performSelector:@selector(loadContent) withObject:nil afterDelay:1.0];
            [self setUpUserInfo];
//            [self reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];

        }
    }
               andFailureBlock:^(NSError *error) {
                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                   
                   NSLog(@"Failed from the server %@",[error description]);
               }];
}
-(void)userTappedOnLink:(UITapGestureRecognizer *)recognizer
{

    
    NSString *strUrl = userProfilemodel.website;
    if (([strUrl rangeOfString:@"http://"].location == NSNotFound) && ([strUrl rangeOfString:@"https://"].location == NSNotFound)) {
        NSLog(@"string does not contain bla");
        strUrl = [NSString stringWithFormat:@"http://%@",strUrl];
        
    } else {
        NSLog(@"string contains bla!");
        
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl]];


}
-(void)setUpUserInfo
{
    lblUserName.text              =   [NSString stringWithFormat:@"%@ %@",userProfilemodel.user_first_name,userProfilemodel.user_last_name];
    
    userThumbImg.mintImageURL    =   [NSURL URLWithString:userProfilemodel.user_thumb_image];
    
    
    if ([userProfilemodel.website length] && ![userProfilemodel.user_id isEqualToString:GETVALUE(CEO_UserId)]) {
 
      UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
      [lblWebsite setUserInteractionEnabled:YES];

      [lblWebsite addGestureRecognizer:gesture];
    }
    if ([userProfilemodel.user_location length])
        lblLocation.text            =   userProfilemodel.user_location;
    else
        lblLocation.text            =  ([userProfilemodel.user_id isEqualToString:GETVALUE(CEO_UserId)])?KLOCATIONPLACEHOLDER:@"";

    _userImage.mintImageURL    =   [NSURL URLWithString:userProfilemodel.user_thumb_image];
    userName.text              =   [NSString stringWithFormat:@"%@ %@",userProfilemodel.user_first_name,userProfilemodel.user_last_name];
    

    if ([userProfilemodel.user_id isEqualToString:GETVALUE(CEO_UserId)]) {
        lblWebsite.text            =   [NSString stringWithFormat:@"%@%@",@" - ",([userProfilemodel.website length])?userProfilemodel.website:KWEBSITEPLACEHOLDER];

    }
    else{
    
        lblWebsite.text            =   ([userProfilemodel.user_location length])?[NSString stringWithFormat:@"%@%@",@" - ",userProfilemodel.website]:userProfilemodel.website;
    
    }
    
    /*! frame of acheivement text label. */
    CGRect frameacheivementText      = acheivementText.frame;
    frameacheivementText.origin.y    = ([userProfilemodel.streak_days integerValue]>1)?lblUserName.frame.origin.y+30:lblUserName.frame.origin.y+17;
    frameacheivementText.size.height = [self getAcheivementtextHeight:userProfilemodel.achieving_text];
    acheivementText.frame            =    frameacheivementText;
    acheivementText.text             =   ([userProfilemodel.achieving_text length])?userProfilemodel.achieving_text:@"What are you working to acheive now?";

    
    /*! frame of location view. */
    CGRect frameViewLocation   = viewLocation.frame;
    frameViewLocation.origin.y = acheivementText.frame.origin.y+ acheivementText.frame.size.height;
    viewLocation.frame         = frameViewLocation;
    
    
    
    /*! frame of location label. */
    CGRect frameLocation        = lblLocation.frame;
    frameLocation.size.width    = [self getLocationWidth:lblLocation.text];
    lblLocation.frame           = frameLocation;
    
    /*! frame of website label. */
    CGRect frameWebsite         = lblWebsite.frame;
    frameWebsite.origin.x       = frameLocation.origin.x+ frameLocation.size.width;
    lblWebsite.frame            = frameWebsite;

    profileHeaderHeight = frameViewLocation.origin.y+ 20;
    [viewLocation setHidden:(([lblLocation.text length]==0 && [lblWebsite.text length]==0)?YES:NO)];
    profileHeaderHeight = frameViewLocation.origin.y+(([lblLocation.text length]==0 && [lblWebsite.text length]==0)?0:20);
    profileHeaderHeight = (profileHeaderHeight>63)?profileHeaderHeight:63;
    
    self.profileHeaderHeight = profileHeaderHeight+44;
    
    CGRect frameViewHolder      = _viewUserDetailsInfo.frame;
    frameViewHolder.size.height = profileHeaderHeight;
    _viewUserDetailsInfo.frame  = frameViewHolder;
    
    if ([userProfilemodel.streak_days integerValue]>1)
    {
        [self createMintstreakInfo:userProfilemodel.streak_days lblView:lblStreakInfo streakText:[NSString stringWithFormat:@"%@ day streak",userProfilemodel.streak_days]];
        [lblStreakInfo setHidden:NO];
        [viewStreak setStreakColorWithDays:(int)[userProfilemodel.streak_days integerValue]];
    }
    else
    {
        [lblStreakInfo setHidden:YES];
        [viewStreak setStreakColorWithDays:0];
    }
    
    
    
}

-(void)showStreakInfo:(NSNotification*)notification
{
    if ([self.profileUserId isEqualToString:GETVALUE(CEO_UserId)])
    {
        NSDictionary* userInfo = notification.userInfo;
        NSNumber* notiType = (NSNumber*)userInfo[@"streak_info"];
        userProfilemodel.streak_days = [NSString stringWithFormat:@"%d day streak",notiType.intValue];
        [self createMintstreakInfo:userProfilemodel.streak_days lblView:lblStreakInfo streakText:[NSString stringWithFormat:@"%d day streak",notiType.intValue]];
        [viewStreak setStreakColorWithDays:notiType.intValue];
    }
}


-(NSMutableAttributedString *)setStreakText:(NSString *)streakText imgName:(NSString *)imgName
{
    if (streakText.length)
    {
        streakText = [NSString stringWithFormat:@" %@",streakText];
        NSAttributedString *labelAttributes = [[NSAttributedString alloc] initWithString:streakText];
        
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
        textAttachment.image = [UIImage imageNamed:imgName];
        textAttachment.bounds = CGRectMake(0, -2.0, 10, 10);
        NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
        [attrStr appendAttributedString:attrStringWithImage];
        [attrStr appendAttributedString:labelAttributes];
        return attrStr;
    }
    
    return nil;
}

-(void)createMintstreakInfo:(NSString *)days lblView:(UILabel *)lblStreak streakText:(NSString *)streakText
{
    if([days integerValue] <= 7)
    {
        lblStreak.attributedText = [self setStreakText:streakText imgName:@"streak_orange"];
        lblStreak.textColor = ORANGECOLOR;
    }
    else if([days integerValue] <=14)
    {
        lblStreak.attributedText = [self setStreakText:streakText imgName:@"streak_green"];
        lblStreak.textColor = [UIColor colorWithRed:80.0/255.0 green:172.0/255.0 blue:89.0/255.0 alpha:1.0];
    }
    else if([days integerValue] <=21)
    {
        lblStreak.attributedText = [self setStreakText:streakText imgName:@"streak_blue"];
        lblStreak.textColor = [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0];
    }
    else if([days integerValue] >21)
    {
        lblStreak.attributedText = [self setStreakText:streakText imgName:@"streak_fire_blue"];
        lblStreak.textColor = [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0];
    }
    
    lblStreak.tag = [days integerValue];
    
    if ([GETVALUE(CEO_UserId) isEqualToString:self.profileUserId])
    {
        UITapGestureRecognizer* gestureTimeCategory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mintStreakSelecedFromHeader:)];
        [lblStreak setUserInteractionEnabled:YES];
        [lblStreak addGestureRecognizer:gestureTimeCategory];
    }
}

-(void)mintStreakSelecedFromHeader:(UITapGestureRecognizer *)guesture
{
    UILabel *lable = (UILabel*)guesture.view;
    CGPoint touchPoint = [guesture locationInView:lable];
    if (touchPoint.x>15 && touchPoint.x<79 && touchPoint.y < 10) {
//        [AppDelegate createStreakCoachingTips:[NSString stringWithFormat:@"%d",[userProfilemodel.streak_days intValue]]];
        AppDelegate *obj = MINTSHOWAPPDELEGATE;
        [obj createStreakCoachingTips:[NSString stringWithFormat:@"%d",[userProfilemodel.streak_days intValue]]];
    }
}


/*! set the status of user follow/incircle,if loggedin user edit button */

-(void)setStatusofProfileHeader
{
 
    //Hiding/unhiding follow button

    if([userProfilemodel.follow_status isEqualToString:@"Yes"]&& ![_profileUserId isEqualToString:GETVALUE(CEO_UserId)])
    {
        imgViewEditProfile.hidden = YES;
        btnEdit.hidden = YES;
        btnHeaderFollow.hidden = NO;
        [btnHeaderFollow  setTitle: @"IN-CIRCLE" forState: UIControlStateNormal];
        [btnHeaderFollow  setBackgroundColor:[UIColor whiteColor]];
        btnHeaderFollow.layer.borderWidth = 1.0f;
        btnHeaderFollow.layer.borderColor = ORANGECOLOR.CGColor;
        [btnHeaderFollow  setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
        btnHeaderFollow.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
        
    }
    if([userProfilemodel.follow_status isEqualToString:@"No"]&&![_profileUserId isEqualToString:GETVALUE(CEO_UserId)])
    {
        imgViewEditProfile.hidden = YES;
        btnEdit.hidden = YES;
        btnHeaderFollow.hidden = NO;
        [btnHeaderFollow  setTitle: @"FOLLOW" forState: UIControlStateNormal];
        [btnHeaderFollow  setBackgroundColor:ORANGECOLOR];
        [btnHeaderFollow  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnHeaderFollow.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
        
    }
    
}

#pragma mark - Setters
- (void)setNumberOfTabs:(NSUInteger)numberOfTabs {
    // Set numberOfTabs
    _numberOfTabs = numberOfTabs;
    
    // Reload data
    [self reloadData];
    
}
#pragma mark - Helpers
- (void)selectTabWithNumberFive {
    [self selectTabAtIndex:selectedTabIndexVal];
}
- (void)loadContent {
    self.numberOfTabs = 5;
    [self performSelector:@selector(selectTabWithNumberFive) withObject:nil];
}

#pragma mark - Interface Orientation Changes
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    // Update changes after screen rotates
    [self performSelector:@selector(setNeedsReloadOptions) withObject:nil afterDelay:duration];
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ProfilePagerViewController *)viewPager {
    return self.numberOfTabs;
}

- (TabItemView *)viewPager:(ProfilePagerViewController *)viewPager viewDtatForTabAtIndex:(NSUInteger)index{
    TabItemView *aView = [TabItemView new];
    aView.shouldShowImage = NO;

    if (index == 0) {
        aView.titleText = ([userProfilemodel.profile_mint_count isEqualToString:@"1"] || [userProfilemodel.profile_mint_count isEqualToString:@"0"])?@"Mint":@"Mints";
        aView.subtitleText = @"";
        [self onUpdateCount:([userProfilemodel.profile_mint_count length]? userProfilemodel.profile_mint_count:@"0") viewTag:index];

    }else if (index == 1) {
        aView.titleText = ([userProfilemodel.profile_showroom_count isEqualToString:@"1"] || [userProfilemodel.profile_showroom_count isEqualToString:@"0"])?@"Showroom":@"Showrooms";
        aView.subtitleText = @"";
        [self onUpdateCount:([userProfilemodel.profile_showroom_count length]? userProfilemodel.profile_showroom_count:@"0") viewTag:index];

    }else if (index == 2) {
        aView.titleText = ([userProfilemodel.profile_followers_count isEqualToString:@"1"] || [userProfilemodel.profile_followers_count isEqualToString:@"0"])?@"Follower":@"Followers";
        aView.subtitleText = @"";
        [self onUpdateCount:([userProfilemodel.profile_followers_count length]? userProfilemodel.profile_followers_count:@"0") viewTag:index];
        
    }else if (index == 3) {
        aView.titleText = @"Stats";
        aView.subtitleText = @"";
        aView.shouldShowImage = YES;
        aView.imageName = @"Icon_Stats";
        aView.selectedimageName = @"Icon_Stats_Selected";
    }else{
        aView.titleText = @"Bio";
        aView.subtitleText = @"";
        aView.shouldShowImage = YES;
        aView.imageName = @"Icon_Bio";
        aView.selectedimageName = @"Icon_Bio_Selected";

    }
    return aView;
    
}

- (UIViewController *)viewPager:(ProfilePagerViewController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index
{
   if (index == 0) {
        MSPMintsVC *cvc = [[MSPMintsVC alloc]initWithNibName:@"MSPMintsVC" bundle:nil];
        cvc.userId      = ([self.profileUserId length])?self.profileUserId:GETVALUE(CEO_UserId);
        cvc.mintsCount  = userProfilemodel.profile_mint_count;
        cvc.delegate    = self;
        return cvc;
    }  else if (index == 1) {
        MSPShowroomVC *cvc = [[MSPShowroomVC alloc]initWithNibName:@"MSPShowroomVC" bundle:nil];
        cvc.profileUserId = ([self.profileUserId length])?self.profileUserId:GETVALUE(CEO_UserId);
        cvc.psdelegate     = self;
        cvc.createdShowRoomCount  = userProfilemodel.profile_showroom_created;
        cvc.joinedShowRoomCount   = userProfilemodel.profile_showroom_joined;
        return cvc;
        
    }else if (index == 2) {
        MSPFollowVC *cvc = [[MSPFollowVC alloc]initWithNibName:@"MSPFollowVC" bundle:nil];
        cvc.strCurrentUserId = userProfilemodel.user_id;
        cvc.pFollowerDelegate =self;

        cvc.followersCount   =  userProfilemodel.profile_followers_count;
        cvc.inMycircleCount  =  userProfilemodel.profile_circle_count;
        return cvc;
        
    }else if (index == 3) {
        MSPStatsVC *cvc = [[MSPStatsVC alloc]initWithNibName:@"MSPStatsVC" bundle:nil];
        //cvc.view.backgroundColor = [UIColor redColor];
        cvc.delegate =self;
        cvc.strUserId = self.profileUserId;
        return cvc;
        
    } else {
        MSPBioVC *cvc = [[MSPBioVC alloc]initWithNibName:@"MSPBioVC" bundle:nil];
       // cvc.view.backgroundColor = [UIColor grayColor];
        return cvc;
    }
}


- (UIColor *)viewPager:(ProfilePagerViewController *)viewPager colorForComponent:(ProfileViewPagerComponent)component withDefault:(UIColor *)color;
 {
    
    switch (component) {
        case ProfileViewPagerIndicator:
            return [UIColor orangeColor];
        case ProfileViewPagerTabsView:
            return [[UIColor whiteColor] colorWithAlphaComponent:0.32];
        case ProfileViewPagerContent:
            return [[UIColor darkGrayColor] colorWithAlphaComponent:0.32];
        default:
            return color;
    }
}

-(void)updateTabsCountWithCount:(int)count ForTab:(NSInteger)tab;
{
    if(tab == 1)
    {
        userProfilemodel.profile_showroom_count = [NSString stringWithFormat:@"%d",count];
        [self onUpdateCount:userProfilemodel.profile_showroom_count viewTag:1];
    }
    else if (tab == 0)
    {
        userProfilemodel.profile_mint_count = [NSString stringWithFormat:@"%d",count];
        [self onUpdateCount:userProfilemodel.profile_mint_count viewTag:0];
        
    }
    else if (tab == 2)
    {
        userProfilemodel.profile_followers_count = [NSString stringWithFormat:@"%d",count];
        [self onUpdateCount:userProfilemodel.profile_followers_count viewTag:2];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ******* SETTINGS PAGE INTEGRATION *******
-(IBAction)moreTapped:(id)sender
{
    viewMorePopUp.frame = [UIScreen mainScreen].bounds;
    [[[UIApplication sharedApplication] keyWindow] addSubview:viewMorePopUp];
    
    
}

- (IBAction)logoutTapped:(id)sender {

    [viewMorePopUp removeFromSuperview];

    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Are you sure?"
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert.view setTintColor:[UIColor blackColor]];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Log out"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             [UIApplication sharedApplication].applicationIconBadgeNumber = 0 ;
                             [self logoutComplete];
                             SPHSingletonClass *objectShareClass = [SPHSingletonClass sharedSingletonClass];
                             objectShareClass.glblActivityRequest_count = 0;
                             objectShareClass.glblActivityTotal_count = 0;
                             objectShareClass.glblActivityYou_count = 0;
                             [YSSupport resetDefaultsLogout:NO];
                             [UserAccessSession clearAllSession];
                             AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                             [appDell logOutDidFinish:appDell.ysTabBarController];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    
}

- (IBAction)btnSettingsTapped:(id)sender
{
      AppDelegate *delegate =  MINTSHOWAPPDELEGATE[viewSettings setFrame:delegate.window.frame];
    [viewMorePopUp removeFromSuperview];
    [self.view.window addSubview:viewSettings];
//    http://stage.mintshowapp.com/m/showroom/user_settings?mobile=on&token=5ffd460b2ef682e98db5f7755fc96c7062f2df50&device_token=bc262ea4f5a6618779e098363695036c85bbd3066a9490eff658059379eeb79a
    

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/m/showroom/user_settings?mobile=on&token=%@&device_token=%@",ServerUrlSetting,GETVALUE(CEO_AccessToken),[delegate getDeviceTkenFromKeyChain]]];
    webSettingsView.delegate = self;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webSettingsView loadRequest:requestObj];
    
    
    
//    MSSettingVC *settingVC =  [[MSSettingVC alloc]initWithNibName:@"MSSettingVC" bundle:nil];
//    settingVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:settingVC animated:YES];

}
-(IBAction)cancelViewSetting
{

    [viewSettings removeFromSuperview];

}
-(IBAction)removeMoreScreen:(id)sender
{
    [viewMorePopUp removeFromSuperview];

}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"\n\n-- %@\n--%@\n\n",request.URL,[request.URL absoluteString]);
    
    if (!([[NSString stringWithFormat:@"%@",[request URL]] rangeOfString:@"deactivateaccounts"].location == NSNotFound))
    {
        [self accountDeleteOrDeactivate:NO];
        return NO;
    }
    
    if (!([[NSString stringWithFormat:@"%@",[request URL]] rangeOfString:@"deleteaccounts"].location == NSNotFound))
    {
        [self accountDeleteOrDeactivate:YES];
        return NO;
    }
    
    if (!([[NSString stringWithFormat:@"%@",[request URL]] rangeOfString:@"change_Profile_info"].location == NSNotFound))
    {
        NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
        NSArray *urlComponents = [[request.URL absoluteString] componentsSeparatedByString:@"&"];
        
        for (NSString *keyValuePair in urlComponents)
        {
            NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
            NSString *key           = [[pairComponents firstObject] stringByRemovingPercentEncoding];
            NSString *value         = [[pairComponents lastObject] stringByRemovingPercentEncoding];
            
            [queryStringDictionary setObject:value forKey:key];
        }
        [self nameUpdatedSuccesFully:[queryStringDictionary objectForKey:@"first_name"] lastName:[queryStringDictionary objectForKey:@"last_name"]];
        
        return NO;
    }
    
    
    return YES;
}

-(void)nameUpdatedSuccesFully:(NSString *)firstName lastName:(NSString *)lastName
{
    [viewSettings removeFromSuperview];
    [webSettingsView stopLoading];
    userProfilemodel.user_first_name = firstName;
    userProfilemodel.user_last_name = lastName;
    
    userName.text              =   [NSString stringWithFormat:@"%@ %@",userProfilemodel.user_first_name,userProfilemodel.user_last_name];
    
    SETVALUE(userProfilemodel.user_first_name,CEO_UserFirstName);
    SETVALUE(userProfilemodel.user_last_name,CEO_UserLastName);
    SYNCHRONISE;
    
    showAlert(nil, @"Successfully Updated", @"ok", self);
}
-(void)accountDeleteOrDeactivate :(BOOL)isDelete
{
    
    if ([GETVALUE(CEO_AccessToken) length]) {

    [webSettingsView stopLoading];
    [viewSettings removeFromSuperview];
    showAlert(((isDelete)?@"Your account is now Deleted":@"Your account is now Deactivated"),nil, @"OK", nil)
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0 ;
    [YSSupport resetDefaultsLogout:isDelete];
    [UserAccessSession clearAllSession];
    AppDelegate *appdell = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appdell logOutDidFinish:self.tabBarController];
    }

}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    
}
#pragma mark - SERVER call
- (void)logoutComplete
{
  
    AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&device_id=%@",GETVALUE(CEO_AccessToken),[appDell getDeviceID]] dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_USERLOGOUT]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"Status"] isEqualToString:@"Success"])
         {
             NSLog(@"%@",[showroomDict valueForKey:@"StatusMsg"]);
         }
         else
         {
             NSLog(@"%@",[showroomDict valueForKey:@"StatusMsg"]);
         }
     }];
}


- (IBAction)backButtonPressed:(UIButton *)sender {
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.isAnyUserMuted = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.isAnyUserMuted = NO;

}

- (IBAction)editTapped:(id)sender {
    ProfileEditViewController *viewEditProfile =  [[ProfileEditViewController alloc]initWithNibName:@"ProfileEditViewController" bundle:nil];
    viewEditProfile.pmodeldata                 =    userProfilemodel ;
    viewEditProfile.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewEditProfile animated:YES];
    
}

/*! get the height of acheivement text. */

-(CGFloat)getAcheivementtextHeight : (NSString *) acheivementtext{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:11];
    CGFloat acheivementtextHeight  = [acheivementtext boundingRectWithSize:CGSizeMake(245, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return  acheivementtextHeight;
}

-(CGFloat)getAcheivementtextHeight{
    return [self getAcheivementtextHeight:acheivementText.text];
}

/*! get the width of location text. */

-(CGFloat)getLocationWidth : (NSString *) locationtext{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:11];
    CGFloat locationtextWidth  = [locationtext boundingRectWithSize:CGSizeMake(MAXFLOAT,20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.width;
    return  (locationtextWidth < 98  )?locationtextWidth:98;
}
/*! API call when follow button tapped in profile header */

- (IBAction)headerFollowTapped:(UIButton*)sender {
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&user_id=%@",GETVALUE(CEO_AccessToken),_profileUserId] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@follow/follow",ServerUrl]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:url];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSDictionary *profileDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",profileDict);
         if ([[profileDict valueForKey:@"status"] isEqualToString:@"true"]) {
             if ([[profileDict valueForKey:@"success_message"] isEqualToString:@"followed"])  {
                
                 [self setFollowButtonInfo:YES];
                 /*! once  clicked on follow, all tabs should be enabled */
                 _isNavigated = NO;
                 [self createUserDetails:YES];
                 [self updateTabsCountWithCount:(int)[userProfilemodel.profile_followers_count integerValue]+1 ForTab:2];
                 if (_isFromMintdetails) {
                     [self.delegateProfile updateFollowUserFromProfile:_profileUserId];
                 }

             }
             else
             {
                 [self setFollowButtonInfo:NO];
                 [self updateTabsCountWithCount:(int)[userProfilemodel.profile_followers_count integerValue]-1 ForTab:2];

                 [self createUserDetails:NO];

             }
             
         }
     }];
}
-(void)setFollowButtonInfo:(BOOL)isInCircle
{

    if (isInCircle) {
        [btnHeaderFollow setTitle:@"IN-CIRCLE" forState:UIControlStateNormal];
        [btnHeaderFollow setBackgroundColor:[UIColor whiteColor]];
        [btnHeaderFollow setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
        btnHeaderFollow.layer.borderWidth = 1.0f;
        btnHeaderFollow.layer.borderColor = ORANGECOLOR.CGColor;
    }
    else{
    
        [btnHeaderFollow setTitle:@"FOLLOW" forState:UIControlStateNormal];
        [btnHeaderFollow setBackgroundColor:ORANGECOLOR];
        [btnHeaderFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnHeaderFollow setImage:nil forState:UIControlStateNormal];
    
    }


}
-(void)createUserDetails:(BOOL)isInCircle
{
    _profileUserId = ([_profileUserId length])?_profileUserId:GETVALUE(CEO_UserId);
    strCurrentUserId = _profileUserId;
    self.contentView.userInteractionEnabled = (isInCircle)?YES:NO;
    self.tabsView.userInteractionEnabled = (isInCircle)?YES:NO;
    if (!isInCircle) {
        if (selectedTabIndexVal != 4) {
            selectedTabIndexVal = 4;
            [self selectTabAtIndex:4];
        }

    }
    else
    {
        if (selectedTabIndexVal != 0) {
            selectedTabIndexVal = 0;
            [self selectTabAtIndex:0];
        }
    }
  
    self.tabsView.scrollEnabled = (isInCircle)?YES:NO;;

      //  [self getUserProfile];

}

-(void)animateView
{
    [UIView animateWithDuration:0.3 animations:^{
        viewHeader.hidden = YES;
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}

-(void)makeScrollDownWithContenYPosition:(CGFloat)viewHeight
{
    //    NSLog(@"down Direction: %f",viewHeight);
    if (viewHeight>58) {
        [userThumbImg setHidden:NO];
        [lblUserName setHidden:NO];
        if (![GETVALUE(CEO_UserId) isEqualToString:self.profileUserId] && self.profileUserId.length ) {
            _btnProfileback.hidden = YES;
        }
    }
    [self makeWhatAppScroller:viewHeight scrDirection:@"down"];
    
}

-(void)makeScrollUpWithContenYPosition:(CGFloat)viewHeight
{
    if (viewHeight<58) {
        [userThumbImg setHidden:YES];
        [lblUserName setHidden:YES];
        if (![GETVALUE(CEO_UserId) isEqualToString:self.profileUserId] && self.profileUserId.length ) {
            _btnProfileback.hidden = NO;
        }
    }
    [self makeWhatAppScroller:viewHeight scrDirection:@"up"];
}

-(void)makeWhatAppScroller:(CGFloat)viewHeight scrDirection:(NSString *)strDirect
{
    viewHeight = (viewHeight>profileHeaderHeight)?profileHeaderHeight:viewHeight;

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    CGRect rect = self.view.frame;
    if ([strDirect isEqualToString:@"down"]) {
        rect.size.height = currentScreenHeight+viewHeight;
    }
    rect.origin.y = 0-viewHeight;
    self.view.frame = rect;
    [UIView commitAnimations];
}

-(void)updateProfileUsersIsInCircle:(NSNotification *)notification
{
    int followStatus = [[[notification valueForKey:@"userInfo"] valueForKey:@"FollowStatus"] intValue];

    //selectedTabIndexVal = 0;
    
    if (followStatus == 1) {
        userProfilemodel.follow_status = @"Yes";
        [self setFollowButtonInfo:YES];
        [self createUserDetails:YES];
        selectedTabIndexVal = 4;
    }
    else
    {
        userProfilemodel.follow_status = @"No";
        [self setFollowButtonInfo:NO];
        selectedTabIndexVal = 0;

        [self createUserDetails:NO];
    }

  
//    [self headerFollowTapped:btnHeaderFollow];


}
-(void)updateUserProfileDetails:(NSString*)userId
{
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = userId;
    // prof.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:prof animated:YES];
}
@end
