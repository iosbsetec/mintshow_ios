//
//  ProfilePagerViewController.h
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabItemView.h"


typedef NS_ENUM(NSUInteger, ProfileViewPagerOption) {
    ProfileViewPagerOptionTabHeight,
    ProfileViewPagerOptionTabOffset,
    ProfileViewPagerOptionTabWidth,
    ProfileViewPagerOptionTabLocation,
    ProfileViewPagerOptionStartFromSecondTab,
    ProfileViewPagerOptionCenterCurrentTab,
    ProfileViewPagerOptionFixFormerTabsPositions,
    ProfileViewPagerOptionFixLatterTabsPositions
};


typedef NS_ENUM(NSUInteger, ProfileViewPagerComponent) {
    ProfileViewPagerIndicator,
    ProfileViewPagerTabsView,
    ProfileViewPagerContent
};

@protocol ProfileViewPagerDataSource;
@protocol ProfileViewPagerDelegate;


@interface ProfilePagerViewController : UIViewController

@property UIScrollView *tabsView;
@property UIView *contentView;
@property NSMutableArray *tabs;

@property UILabel *lblShowroomCount;
@property UILabel *lblMintCount;
@property UILabel *lblFollowerCount;

@property CGFloat profileHeaderHeight;

@property (weak) id <ProfileViewPagerDataSource> dataSource;
@property (weak) id <ProfileViewPagerDelegate> delegate;

#pragma mark Methods

- (void)reloadData;
- (void)selectTabAtIndex:(NSUInteger)index;
- (void)setNeedsReloadOptions;
- (void)setNeedsReloadColors;
- (CGFloat)valueForOption:(ProfileViewPagerOption)option;
- (UIColor *)colorForComponent:(ProfileViewPagerComponent)component;

@end

#pragma mark dataSource
@protocol ProfileViewPagerDataSource <NSObject>

- (NSUInteger)numberOfTabsForViewPager:(ProfilePagerViewController *)viewPager;
- (UIView *)viewPager:(ProfilePagerViewController *)viewPager viewForTabAtIndex:(NSUInteger)index;
- (TabItemView *)viewPager:(ProfilePagerViewController *)viewPager viewDtatForTabAtIndex:(NSUInteger)index;

@optional

- (UIViewController *)viewPager:(ProfilePagerViewController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index;
- (UIView *)viewPager:(ProfilePagerViewController *)viewPager contentViewForTabAtIndex:(NSUInteger)index;

@end

#pragma mark delegate
@protocol ProfileViewPagerDelegate <NSObject>

@optional

- (void)viewPager:(ProfilePagerViewController *)viewPager didChangeTabToIndex:(NSUInteger)index;
- (void)viewPager:(ProfilePagerViewController *)viewPager didChangeTabToIndex:(NSUInteger)index fromIndex:(NSUInteger)previousIndex;
- (void)viewPager:(ProfilePagerViewController *)viewPager didChangeTabToIndex:(NSUInteger)index fromIndex:(NSUInteger)previousIndex didSwipe:(BOOL)didSwipe;
- (CGFloat)viewPager:(ProfilePagerViewController *)viewPager valueForOption:(ProfileViewPagerOption)option withDefault:(CGFloat)value;
- (UIColor *)viewPager:(ProfilePagerViewController *)viewPager colorForComponent:(ProfileViewPagerComponent)component withDefault:(UIColor *)color;
-(void)onUpdateCount:(NSString *)strCount viewTag:(NSInteger)tagValue;

@end

