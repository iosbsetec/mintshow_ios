//
//  FourthViewController.h
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MSPStatsDelegate
-(void)makeScrollUpWithContenYPosition:(CGFloat)viewHeight;
-(void)makeScrollDownWithContenYPosition:(CGFloat)viewHeight;
@end


@interface MSPStatsVC : UIViewController
@property (nonatomic,retain) NSString *strUserId;
@property (nonatomic) CGFloat lastContentOffset;
@property (nonatomic, assign)    id <MSPStatsDelegate> delegate;
@end
