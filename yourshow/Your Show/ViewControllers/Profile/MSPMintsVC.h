//
//  FirstViewController.h
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MSPMintsVCDelegate<NSObject>

@optional

-(void)uploadNextProfile:(NSString *)selectedUserId;
-(void)updateTabsCountWithCount:(int)count ForTab:(NSInteger)tab;
-(void)makeScrollUpWithContenYPosition:(CGFloat)viewHeight;
-(void)makeScrollDownWithContenYPosition:(CGFloat)viewHeight;


@end

@interface MSPMintsVC : UIViewController
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *mintsCount;
@property (nonatomic, assign)    id <MSPMintsVCDelegate> delegate;

@end
