//
//  FirstViewController.m
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import "MSPMintsVC.h"
#import "CommentsViewController.h"
#import "FilterListTable.h"
#import "UIViewController+CWPopup.h"
#import "UploadStatusView.h"
#import "EntityModel.h"
#import "MSRemintVC.h"
#import "MSMintDetails.h"

#import "PintCollecionCell.h"
#import "MSProfileVC.h"
#import "SocialVideoHelper.h"
#import "AddEditShowroomVC.h"
#import "MintShowroomViewController.h"
#import "UIViewController+CWPopup.h"
#import "CarbonKit.h"
#import "YSCollectionView.h"
#import "BaseFeedModel.h"
#import "UserAccessSession.h"
#import "UsersListView.h"
#import "CommentModel.h"

#import "MSGlobalSearchVCViewController.h"

#define NOMOREMINTS  @"No more mints"

@interface MSPMintsVC ()<YSCollectionViewDataSource,YSCollectionViewDelegate,FilterListTableDelegate,uploadDelegate,MSMintDetailsDelegate,RemintDelegate,AddEditShowroomVCDelegate,CommentsViewDelegate,MSGlobalSearchDelegate,UsersListDelegate>
{
    IBOutlet UIButton *btnFilter;
    
    __weak IBOutlet UIView *viewFilter;
    
    
    //Array if for saving the aparameters for Filter Search by the user
    NSMutableArray * arrayFilterSearchParameters;
    
    
    BOOL isLoading,isPullToreFresh;
    BOOL isfilterSearchApply;
    BOOL isDataFinishedInServer;
    int  pagenumber;
    int  pagenumberShowroom;
    
    CarbonSwipeRefresh *refreshCarbon;
    CGFloat frameViewheight;



}
@property (retain,nonatomic)  NSMutableArray *arrarProfileUsersMints;

@property (strong, nonatomic) IBOutlet YSCollectionView *aView;

@end

@implementation MSPMintsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    btnFilter.layer.cornerRadius = 5.;
    viewFilter.layer.cornerRadius = 5.;
    isPullToreFresh = NO;
    
    [self initializer];
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:self.aView withYPos:100];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(remintedSucssesfully)
                                                 name:@"REMINTCOMPLETE" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(commentUpdatedSuccesFully:) name:KCOMMNETUPDATED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(mintAddedSuccesFully:) name:KMINTADDED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(mintEditedSuccesFully:) name:KMINTEDITED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(profileUpdated:) name:KPROFILEIMAGEUPDATED object:nil];
    
    // Changes for iPad done by mani
    if (isSmallPad) {
        CGRect rctTransparent = _aView.frame;
        rctTransparent.size.height = self.view.frame.size.height-160;
        [_aView setFrame:rctTransparent];
    }


}
#pragma mark - YSCollectionView Delegates
-(void)didScrolledDown:(CGFloat)yPosition
{
    [self.delegate makeScrollDownWithContenYPosition:yPosition];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     pagenumber = 1;
    [self getMintsMyshow:pagenumber isDelete:(self.arrarProfileUsersMints.count)?YES:NO pull_request_time:NO];


}
-(void)didScrolledUp:(CGFloat)yPosition
{
    [self.delegate makeScrollUpWithContenYPosition:yPosition];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.aView setContentOffset:CGPointMake(0, 0)];
    self.aView.scrollsToTop = YES;
}
#pragma mark - Notification
-(void) profileUpdated:(NSNotification *) notification
{
    if ([notification.object isKindOfClass:[NSString class]])
    {
        NSString *userimageUrl = [notification object];
        // do stuff here with your message data
        NSInteger i=0;
        NSMutableArray *array = [self.arrarProfileUsersMints mutableCopy];
        for (BaseFeedModel *mint in array) {
            BaseFeedModel *existingFeedModeldata =  mint;
            
            
            if (existingFeedModeldata.IsMintReminted && [[existingFeedModeldata remint_user_id] isEqualToString:GETVALUE(CEO_UserId)]) {
                
                existingFeedModeldata.remint_userimg = userimageUrl;
            }
           else if ([[existingFeedModeldata feed_user_id] isEqualToString:GETVALUE(CEO_UserId)]) {
                
                existingFeedModeldata.feed_user_thumb_image = userimageUrl;
            }

            [self.arrarProfileUsersMints replaceObjectAtIndex:i withObject:existingFeedModeldata];
            i++;
        }
        array = nil;
       // [self.aView setContentOffset:CGPointZero animated:YES];

        [self batchreloadCollectionView];

    }
    else
    {
        NSLog(@"Error, object not recognised.");
    }
}
- (IBAction)searchTapped:(UIButton *)sender {
    MSGlobalSearchVCViewController  *globalSearchVC =  [[MSGlobalSearchVCViewController alloc] initWithNibName:@"MSGlobalSearchVCViewController" bundle:nil];
    globalSearchVC.delegate = self;
    [self.navigationController pushViewController:globalSearchVC animated:NO];
}

-(void)serverCallForTheSearch :(NSString *)textToSearch GlobalsearchApi : (BOOL)GlobalSearch
{
    [self gotoProfileView:textToSearch isGlobalSearch:YES];
}

-(void)initializer{
    // MULTI MINT COLLECTION VIEW
    self.aView.backgroundColor                  = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.view.backgroundColor                   = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.aView.showsHorizontalScrollIndicator   = NO;
    self.aView.scrollEnabled                    = YES;
    self.aView.ysDataSource                     = self;
    self.aView.ysDelegate                       = self;
    self.aView.bounces                          = YES;
    self.arrarProfileUsersMints                 = [[NSMutableArray alloc]init];
    isLoading = NO;
    isDataFinishedInServer = NO;
    pagenumber = 1;
    frameViewheight = self.aView.frame.size.height+50;
    
    CGRect frameSelfView = self.view.frame;
    frameSelfView.size.height = frameViewheight+89;
    self.view.frame = frameSelfView;
    
    CGRect frameView = self.aView.frame;
    frameView.size.height =  frameViewheight+10;
    self.aView.frame = frameView;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - QBRefreshControlDelegate


- (void)refresh:(id)sender {
    pagenumber = 1;
    [self getMintsMyshow:pagenumber isDelete:YES pull_request_time:NO];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}


- (void)endRefreshing {
    [refreshCarbon endRefreshing];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


-(void)getMintsMyshow:(int)pageNumber isDelete : (BOOL) isDelete pull_request_time:(BOOL)pullRequestNeedToAdd
{
    isLoading = YES;
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:@"2"                       forKey:@"mint_list_type"];
    [dicts setValue:self.userId                forKey:@"profile_user_id"];

    [dicts setValue:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page_number"];
    [YSAPICalls getMintsForMyShowTabwithDict:dicts SuccessionBlock:^(id newResponse){
              isLoading = NO;
              Generalmodel *gmodel = newResponse;
        if ([gmodel.status_code integerValue]){
            if([gmodel.tempArray count]>0){
                    isDataFinishedInServer = NO;
                    if (pageNumber  ==  1) {
                        SETVALUE(gmodel.pull_request_time, CEO_PULLREQUESTTIME);
                        SYNCHRONISE;
                    }
                    if (isDelete) {
                        [self.arrarProfileUsersMints removeAllObjects];
                    }
                    [self.arrarProfileUsersMints addObjectsFromArray: gmodel.tempArray];
                    [self batchreloadCollectionView];
            }
            else{
                if (self.arrarProfileUsersMints.count && pagenumber ==1 && [gmodel.status_Msg isEqualToString:NOMOREMINTS]) {
                    [self.arrarProfileUsersMints removeAllObjects];
                    if ([self.delegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])
                    [self.delegate updateTabsCountWithCount:self.arrarProfileUsersMints.count ForTab:0];
                    [self batchreloadCollectionView];
                }
                isDataFinishedInServer = YES;
            }
        }
    } andFailureBlock:^(NSError *error){
        
        NSLog(@"Failed from the server %@",[error description]);
    }];
}

-(void)batchreloadCollectionView{
    
    BOOL issuesCame = NO;
    @try {
        BOOL animationsEnabled = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        [self.aView performBatchUpdates:^{
            [self.aView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        } completion:^(BOOL finished) {
            if (finished) {
                [UIView setAnimationsEnabled:animationsEnabled];
            }
        }];
        issuesCame = NO;
    }
    @catch (NSException *theException) {
        issuesCame = YES;
        //  showAlert(@"try", @"load tableview again", @"OKAY", nil);
    }
    @finally  {
        issuesCame = NO;
        NSLog(@"Executing finally block");
    }
}



#pragma mark-  THIS CODE FOR MULTI MINT


-(void)multiMintCollectionViewClicked:(YSCollectionView *)sender AtRow:(NSInteger)row {
    
}

- (BaseFeedModel *)multiMintCollectionViewView:(YSCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    return [_arrarProfileUsersMints objectAtIndex:row];
}

- (NSInteger)NumberOFSectionsMultiMintView:(YSCollectionView *)collectionView{
    return 1;
}

- (NSInteger)NumberOFItemsForMultiMintView:(YSCollectionView *)collectionView forSection:(NSInteger)section{  return  _arrarProfileUsersMints.count;
}

- (void)didLoadNextTriggered{
    if(!isLoading){
        pagenumber = pagenumber+1;
        
        if (isfilterSearchApply) {
           // [self filterSearchApplyForMints:NO];
        }
        else
            [self getMintsMyshow:pagenumber isDelete:NO pull_request_time:NO];
    }
}


- (void)selectedFeatureType:(int )featureType withString:(NSString *)selectedEnitity isPrivateShowroomAccess:(BOOL)access{
    switch (featureType) {
        case 0:
            [self gotoHashTagView:selectedEnitity];
            break;
        case 1:
        {
            if(access) {
                showAlert(@"It's a private showroom", nil, @"Ok", nil);
            }
            else
                [self gotoOldShowroom:selectedEnitity];
        }
            break;
        case 2:case 44:
            [self gotoProfileView:selectedEnitity isGlobalSearch:NO];
            break;
        default:
            break;
    }
    
}


- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell{
    
    BaseFeedModel *existingFeedModeldata =              [ self.arrarProfileUsersMints objectAtIndex:row];
    if(buttonType == YSButtonTypeComplimint)  {
        [self commentScreen:existingFeedModeldata withSelectedMintTag:row];
    }else if (buttonType == YSButtonTypeHifiveCount)  {
        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeHighFive numberofPeoples:[existingFeedModeldata.feed_like_count intValue] feedId:existingFeedModeldata.feed_id delegate:self];
        
    }
    else if (buttonType == YSButtonTypeInspire)  {
        [self remintClicked:existingFeedModeldata];
    }
    else if (buttonType == YSButtonTypedetails){
        [self gotoMintDetailsScreen:existingFeedModeldata forRow:row];
    } else if (buttonType == YSButtonTypeUserImage) {
        [self showPopupWithModeldata:data withrow:row isRemint:NO];
    } else if (buttonType == YSButtonTypeRemintUserImage) {
        [self showPopupWithModeldata:data withrow:row isRemint:YES];
    }else if (buttonType == YSButtonTypeHifive){
        [ self.arrarProfileUsersMints replaceObjectAtIndex:row withObject:data];
    }
}

-(void)selectedUserIdFromHighfiveList:(NSString *)selectdUserId
{
    if (![selectdUserId isEqualToString:GETVALUE(CEO_UserId)])
    [self gotoProfileView:selectdUserId isGlobalSearch:NO];
    
}
-(void)gotoHashTagView:(NSString *)selectedEnitity{
    
    
}

-(void)gotoProfileView:(NSString *)selectedEnitity isGlobalSearch:(BOOL)globalSearch{
    
    if (globalSearch) {
        [self.delegate uploadNextProfile:selectedEnitity];

    }
    else{
//    if(![GETVALUE(CEO_UserId) isEqualToString:selectedEnitity] )
        
        if(![_userId isEqualToString:selectedEnitity] )
         [self.delegate uploadNextProfile:selectedEnitity];
    }
}


-(void)commentScreen : (BaseFeedModel *)model withSelectedMintTag :(NSInteger) tag{
    AppDelegate *aDelegate  =  (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndex =  [NSString stringWithFormat:@"%d",(int)tag];
    CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.commentCount              = [model.feed_comment_count intValue];
    comentVc.achievementdetails        = model ;
    comentVc.feedId                    = model.feed_id ;
//    comentVc.isFromTrendingMints = YES;
    comentVc.delegate = self;
    [self.navigationController pushViewController:comentVc animated:YES];
}

-(void)updateCommentCount:(NSInteger)commentCount alreadyCommented:(BOOL)commented
{
    
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    if (aDelegate.selectedIndexForTrendingMint >=self.arrarProfileUsersMints.count) {
        return;
    }
    BaseFeedModel *model = (BaseFeedModel*)[self.arrarProfileUsersMints objectAtIndex:aDelegate.selectedIndexForTrendingMint];
    model.feed_comment_count = [NSString stringWithFormat:@"%d",(int)commentCount];
    model.is_user_commented = (commented)?1:0;
    [self.arrarProfileUsersMints replaceObjectAtIndex:aDelegate.selectedIndexForTrendingMint withObject:model];
    [_aView performBatchUpdates:^{
        [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:aDelegate.selectedIndexForTrendingMint  inSection:0]]];
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(void)gotoOldShowroom:(NSString *)selectedEnitity{
  //  AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
   // delegateApp.lbl.hidden   = YES;
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [selectedEnitity substringFromIndex:1];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}


- (void)remintClicked :(BaseFeedModel *)feedData{
    NSLog(@"Remint screen ");
    MSRemintVC *reminytVW               = [[MSRemintVC alloc]initWithNibName:@"MSRemintVC" bundle:nil];
    reminytVW.aDelegate = self;
    reminytVW.feedData                  = feedData;
    reminytVW.isFromProfile             = YES;
    reminytVW.isFromInstagramTableView  =   @"0";
    [self presentViewController:reminytVW animated:NO completion:^{
        
    }];
}


-(void)showPopupWithModeldata:(BaseFeedModel *)feeddetails withrow:(NSInteger)row isRemint:(BOOL)remint {
    [self gotoProfileView:[NSString stringWithFormat:@"%@",(remint)?feeddetails.remint_user_id:feeddetails.feed_user_id]isGlobalSearch:NO];

}
-(void)gotoMintDetailsScreen: (BaseFeedModel *)model forRow:(NSInteger)row{
    
    BaseFeedModel *modelInfo = [self.arrarProfileUsersMints objectAtIndex:row];
    
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mint_row_id    = model.mint_row_id;
    mintDetailsVC.profileuser_id = _userId;
    mintDetailsVC.Selected_row_value = row;
    mintDetailsVC.delegate      = self;
    mintDetailsVC.isFromProfile = YES;
    mintDetailsVC.selectedProfileUserName  = (modelInfo.IsMintReminted)?modelInfo.remint_username:modelInfo.feed_user_name;
    mintDetailsVC.selectedProfileUserImage = (modelInfo.IsMintReminted)?modelInfo.remint_userimg:modelInfo.feed_user_thumb_image;
    mintDetailsVC.profileMintArrayDetails =_arrarProfileUsersMints;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId {
    [self getMintsMyshow:pagenumber isDelete:NO pull_request_time:NO];
    int nMintscount = 0;

    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[_arrarProfileUsersMints count]; currentIndex++){
        //do stuff with obj
        BaseFeedModel *feedlist = [_arrarProfileUsersMints objectAtIndex:currentIndex];
        if([mintId isEqualToString:feedlist.feed_id]){
            nMintscount += 1;
            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [_arrarProfileUsersMints removeObjectsAtIndexes:indexesToDelete];
        if ([self.delegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])
        [self.delegate updateTabsCountWithCount:[_mintsCount intValue]-nMintscount ForTab:0];
        [self batchreloadCollectionView];
    }
    
    
}

-(void)muteMintOfmintDetails : (NSString *)mintId{
    
    
}


-(void)muteUserOfmintDetails:(NSString *)muteUser{
    
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[_arrarProfileUsersMints count]; currentIndex++){
        //do stuff with obj
        BaseFeedModel *feedlist = [_arrarProfileUsersMints objectAtIndex:currentIndex];
        if([muteUser isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [_arrarProfileUsersMints removeObjectsAtIndexes:indexesToDelete];
        [self batchreloadCollectionView];
    }
    
    
}

-(void)highFiveMintOfmintDetails :(NSString *)mintId andCurerntValue : (NSString *)activityValue countValue: (NSString *)value{
    int i = 0;
    for (BaseFeedModel *model in self.arrarProfileUsersMints) {
        if ([model.feed_id isEqualToString:mintId]) {
            [model setIs_user_liked:[activityValue intValue]];
            [model setFeed_like_count:value];
            [self.arrarProfileUsersMints replaceObjectAtIndex:i withObject:model];
            YSMintCollectionViewCell *cell =  (YSMintCollectionViewCell *) [self.aView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
            [cell setBackgroundImageForButton:YSButtonTypeHifive AndValue:[NSString stringWithFormat:@"%d",model.is_user_liked] withLabelValue:[NSString stringWithFormat:@"%@",model.feed_like_count]];
            
            
            break;
        }
        i++;
    }
}
-(void)editRemintDetails : (NSString *)strMessage rowId:(NSInteger)rowId{
    if (rowId<self.arrarProfileUsersMints.count) {
        BaseFeedModel *model = [self.arrarProfileUsersMints objectAtIndex:rowId];
        model.remint_message = strMessage;
        [self.arrarProfileUsersMints replaceObjectAtIndex:rowId withObject:model];
        [_aView performBatchUpdates:^{
            [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowId inSection:0]]];
        } completion:^(BOOL finished) {
        }];
    }
    
    
 }

-(void)commentUpdatedSuccesFully:(NSNotification *) notification
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    if ([aDelegate.selectedIndex integerValue]>=self.arrarProfileUsersMints.count) {
        return;
    }
    CommentModel *commentObject = (CommentModel *) [[notification valueForKey:@"userInfo"] valueForKey:@"CommentData"];
    BaseFeedModel *model = [self.arrarProfileUsersMints objectAtIndex:[aDelegate.selectedIndex integerValue]];
    if ([[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:DELETECOMMENT]) {
        if ([model.feed_comment_count integerValue] == 1){
            [model setFeed_comment_count:@"0"];
            [model setIs_user_commented:0];
        }else{
            [model setFeed_comment_count:[NSString stringWithFormat:@"%d", [model.feed_comment_count intValue]-1]];
            [model setIs_user_commented:1];
            
            
        }
    }
        else {
        if (![[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:EDITCOMMENT])  {
            [model setFeed_comment_count:[NSString stringWithFormat:@"%d", [model.feed_comment_count intValue]+1]];
            [model setIs_user_commented:1];
            if (model.commentsArray.count){
                [model.commentsArray insertObject:commentObject atIndex:0];
            }else{
                model.commentsArray = [[NSMutableArray alloc] init];
                [model.commentsArray addObject:commentObject];
            }
            
        }
    }
    NSInteger commentCount = (model.commentsArray.count>3)?3:model.commentsArray.count;
    float CommentHeight = 0.0;
    for (int i = 0; i<commentCount; i++)
    {
        NSString *strText = [NSString stringWithFormat:@"%@ %@",[[model.commentsArray objectAtIndex:i] achieveCommentUname],[[model.commentsArray objectAtIndex:i] achieveCommentText]];
        
        CommentHeight += [YSSupport getCommentTextHeight:strText font:12.0];
    }
    model.feed_comment_height       = CommentHeight+5;
    [ self.arrarProfileUsersMints replaceObjectAtIndex:[aDelegate.selectedIndex integerValue] withObject:model];
    [_aView performBatchUpdates:^{
        [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:[aDelegate.selectedIndex integerValue] inSection:0]]];
    } completion:^(BOOL finished) {
    }];
}



-(void)remintedSucssesfully {
    pagenumber = 1;
    [self getMintsMyshow:pagenumber isDelete:YES pull_request_time:NO];
//    int count = 1;
//    [self.delegate updateTabsCountWithCount:[_mintsCount intValue]+count ForTab:0];
//    [self batchreloadCollectionView];
    
    if([_userId isEqualToString:GETVALUE(CEO_UserId)])
    {
        int joinedcount = [_mintsCount intValue] + 1;
        if ([self.delegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])
            [self.delegate updateTabsCountWithCount:joinedcount ForTab:0];
    
    }
    
    }
    

-(void)remintMintOfmintDetails :(NSString *)mintId withUserStatus: (NSString *)userStatus remintCount:(NSString *)count atIndex:(NSInteger)index{
    BaseFeedModel *model =  [_arrarProfileUsersMints objectAtIndex:index];
    model.is_user_reminted  = [userStatus intValue];
    model.remint_count      = count;
    [_arrarProfileUsersMints replaceObjectAtIndex:index withObject:model];
    [_aView performBatchUpdates:^{
        [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]];
    } completion:^(BOOL finished) {
    }];

    
}

-(void)mintAddedSuccesFully : (NSNotification *) notification{
    if([_userId isEqualToString:GETVALUE(CEO_UserId)])
    {
        _mintsCount = [NSString stringWithFormat:@"%d",[_mintsCount intValue]+1];
        
        if ([self.delegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])
            [self.delegate updateTabsCountWithCount:[_mintsCount intValue] ForTab:0];
    }
    pagenumber = 1;
    [self getMintsMyshow:pagenumber isDelete:YES pull_request_time:NO];
}

-(void)mintEditedSuccesFully:(NSNotification *)notification {

    
    if (self.arrarProfileUsersMints.count>0)
        return;
    
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    if ([aDelegate.selectedIndex integerValue]>=self.arrarProfileUsersMints.count) {
        return;
    }
    
    BaseFeedModel *model =  [self.arrarProfileUsersMints objectAtIndex:[aDelegate.selectedIndex integerValue]];
    
    model.feed_description = [[notification valueForKey:@"userInfo"] valueForKey:@"description"];
    model.feed_posted_edited_text = ([model.feed_description length])?@"Edited":@"";
    [self.arrarProfileUsersMints replaceObjectAtIndex:[aDelegate.selectedIndex integerValue] withObject:model];
    [_aView performBatchUpdates:^{
        [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:[aDelegate.selectedIndex integerValue] inSection:0]]];
    } completion:^(BOOL finished) {
    }];

   }


//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollViewd
//{
//    CGFloat y = -scrollViewd.contentOffset.y;
//    
//    if (y > 0){
//        self.view.frame = CGRectMake(0, scrollViewd.contentOffset.y, cachedImageViewSize.size.width+y, cachedImageViewSize.size.height+y);
//        self.imageView.center = CGPointMake(self.view.center.x, self.imageView.center.y);
//        transperentviewLabel.frame = self.imageView.frame;
//        if(y<2){
//            currentAlphaValue=0.37;
//        }else{
//            currentAlphaValue = 0.45+ (y/500.0);
//        }
//        transperentviewLabel.alpha= currentAlphaValue;
//    }else {
//        CGFloat offsetY = scrollViewd.contentOffset.y;
//        CGFloat alpha = MIN(1, 1 - ((NAVBAR_CHANGE_POINT + 110 - offsetY) / 84));
//        if (offsetY > NAVBAR_CHANGE_POINT)  {
//            [self.customNavBarView setAlpha:alpha];
//        } else  {
//            [self.customNavBarView setAlpha:alpha];
//        }
//    }
//}



@end
