//
//  FourthViewController.m
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import "MSPStatsVC.h"
#import "ProfileStatsCell.h"
#import "CarbonKit.h"

#define imgArr @[@"HighFive_Icon_24x24.png",@"CompliMint_Icon_36x36.png",@"ReMint_Icon_48x48.png",@"Share_Icon_36x36.png"]
#define statsTotalArr @[@"Total HighFives",@"Total CompliMints",@"Total ReMints",@"Total Shares"]
#define statsAverageArr @[@"Average HighFive per Mint",@"Average CompliMint per Mint",@"Average ReMint per Mint",@"Average Share per Mint"]


@interface MSPStatsVC ()
{
    
    IBOutlet UITableView *tblViewStats;
    NSArray *arrStatusAverage,*arrStatusTotal;
    CarbonSwipeRefresh *refreshCarbon;

}

@end

@implementation MSPStatsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrStatusAverage = [[NSArray alloc]init];
    arrStatusTotal = [[NSArray alloc]init];
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:tblViewStats withYPos:50];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    [self getProfileStatsDetails];
    // Do any additional setup after loading the view.
    
    // Changes for iPad done by mani
    if (isSmallPad) {
        CGRect rctTransparent = tblViewStats.frame;
        rctTransparent.size.height = self.view.frame.size.height-30;
        [tblViewStats setFrame:rctTransparent];
    }

}

#pragma mark - CarbonSwipeRefreshControlDelegate

- (void)refresh:(id)sender {
    
    [self getProfileStatsDetails];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}


- (void)endRefreshing {
    [refreshCarbon endRefreshing];
}



#pragma mark - UITableViewDataSource Methods


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 83;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrStatusAverage.count;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ProfileStatsCell";
    ProfileStatsCell *cell = (ProfileStatsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.imgViewStats.image       =     [UIImage imageNamed:[imgArr objectAtIndex:indexPath.row]];
    cell.imgViewStats.contentMode =     UIViewContentModeCenter;
    cell.selectionStyle           =     UITableViewCellSelectionStyleNone;
    cell.tag                      =     indexPath.row;
    cell.lblAverage.text          =     [statsAverageArr objectAtIndex:indexPath.row];
    cell.lblAverageCount.text     =     [arrStatusAverage objectAtIndex:indexPath.row];
    cell.lblTotal.text            =     [statsTotalArr objectAtIndex:indexPath.row];
    cell.lblTotalCount.text       =     [arrStatusTotal objectAtIndex:indexPath.row];
    return cell;
}

/*! This API call is to get  stats details. */

-(void)getProfileStatsDetails
{
    _strUserId = ([_strUserId length])?_strUserId:GETVALUE(CEO_UserId);
    
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:_strUserId forKey:@"user_id"];
    
    [YSAPICalls getProfileStatsDetails:dicts ForSuccessionBlock:^(id newResponse) {
        Profilemodel  *pmodel = newResponse;
        if ([pmodel.status_code integerValue]){
            NSLog(@"response == %@",newResponse);
            arrStatusAverage   =   @[pmodel.hifive_all,pmodel.comment_all,pmodel.remint_all,pmodel.share_all];
            arrStatusTotal     =   @[pmodel.hifive_total,pmodel.comment_total,pmodel.remint_total,pmodel.share_total];
            [tblViewStats reloadData];
        }
        
    } andFailureBlock:^(NSError *error) {
        NSLog(@"Failed from the server %@",[error description]);
        
    }];
}
#pragma mark - YSCollectionView Delegates
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int yPos =  scrollView.contentOffset.y;
    if (self.lastContentOffset > scrollView.contentOffset.y)
    {
        [self didScrolledUp:(scrollView.contentOffset.y<0)?0:scrollView.contentOffset.y];
        //            NSLog(@"Scrolling Up");
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        if(yPos<0||yPos==0)
        {
        }
        else
            [self didScrolledDown:scrollView.contentOffset.y];
    }
    self.lastContentOffset = scrollView.contentOffset.y;
}


-(void)didScrolledDown:(CGFloat)yPosition
{
    [self.delegate makeScrollDownWithContenYPosition:yPosition];
}

-(void)didScrolledUp:(CGFloat)yPosition
{
    [self.delegate makeScrollUpWithContenYPosition:yPosition];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
