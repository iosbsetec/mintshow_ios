//
//  ThirdViewController.m
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import "MSPFollowVC.h"
#import "CellFollowers.h"
#import "Profilemodel.h"
#import "MSProfileVC.h"
#import "CarbonKit.h"


@interface MSPFollowVC ()
{
    NSMutableArray *arrFollowingList;
    NSMutableArray *arrFollowersList;
    
    IBOutlet UITableView *tblViewProfile;
    BOOL isFollowerDataFinishedFromServer,isApiCalling,isInCircleDataFinishedFromServer;
    BOOL isPullToreFresh;

    NSString *profileUserId,*strCurrentFollowStatus;
    int pageNoFollower,pageNoInCircle;
    
    CarbonSwipeRefresh *refreshCarbon;
    CGFloat frameViewheight;


}
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedCOntrol;
@property (strong,nonatomic) NSString *profileUserId;
@end

@implementation MSPFollowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrFollowingList =  [[NSMutableArray alloc]init];
    arrFollowersList =  [[NSMutableArray alloc]init];
    
    pageNoFollower = 1;
    pageNoInCircle = 1;
    
    isPullToreFresh = NO;
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:tblViewProfile withYPos:70];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];

    [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%@ %@",_followersCount,([_followersCount isEqualToString:@"1"] || [_followersCount isEqualToString:@"0"])?@"Follower":@"Followers"] forSegmentAtIndex:0];
    [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%@ in My Circle",_inMycircleCount] forSegmentAtIndex:1];

    _segmentedCOntrol.selectedSegmentIndex = 0;
    [self segmentAction:_segmentedCOntrol];
    if (isSmallPad) {
        CGRect rctTransparent = self.view.frame;
        rctTransparent.size.height = self.view.frame.size.height-50;
        [self.view setFrame:rctTransparent];
    }
    
    frameViewheight = self.view.frame.size.height;
    
    
    CGRect frameSelfView = self.view.frame;
    frameSelfView.size.height = frameViewheight+86;
    self.view.frame = frameSelfView;
    
//  
//    CGRect frameView = tblViewProfile.frame;
//    frameView.size.height =  frameViewheight+30;
//    tblViewProfile.frame = frameView;
    
    NSLog(@"UserId:%@",_strCurrentUserId);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - YSCollectionView Delegates
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int yPos =  scrollView.contentOffset.y;
    if (self.lastContentOffset > scrollView.contentOffset.y)
    {
        [self didScrolledUp:(scrollView.contentOffset.y<0)?0:scrollView.contentOffset.y];
        //            NSLog(@"Scrolling Up");
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        if(yPos<0||yPos==0)
        {
        }
        else
            [self didScrolledDown:scrollView.contentOffset.y];
    }
    self.lastContentOffset = scrollView.contentOffset.y;
}



-(void)didScrolledDown:(CGFloat)yPosition
{
    [self.pFollowerDelegate makeScrollDownWithContenYPosition:yPosition];
}

-(void)didScrolledUp:(CGFloat)yPosition
{
    [self.pFollowerDelegate makeScrollUpWithContenYPosition:yPosition];
}


#pragma mark - CarbonSwipeRefreshControlDelegate

- (void)refresh:(id)sender {
    isPullToreFresh = YES;
    if(_segmentedCOntrol.selectedSegmentIndex == 0)
    {
        pageNoFollower = 1;
        [self getMyFollowersList:pageNoFollower];
    }
    else
    {
        pageNoInCircle = 1;
        [self getInCircleList:pageNoInCircle];
    }
    [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%@",([_followersCount isEqualToString:@"1"] || [_followersCount isEqualToString:@"0"])?@"Follower":@"Followers"] forSegmentAtIndex:0];

    [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%@ inMyCircle",_inMycircleCount] forSegmentAtIndex:1];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}


- (void)endRefreshing {
    [refreshCarbon endRefreshing];
}

/*! This segment action is for followers and inMycircle in followers tab. */

- (IBAction)segmentAction:(id)sender
{
    if(_segmentedCOntrol.selectedSegmentIndex == 0)
    {
        if(arrFollowersList.count == 0)
            [self getMyFollowersList:1];
        else
        {
            [self setHeightForTable:(int)arrFollowersList.count];

            [tblViewProfile reloadData];
        }
    }
    else
    {
        if(arrFollowingList.count == 0)
            [self getInCircleList:1];
        
        else
        {
            [self setHeightForTable:(int)arrFollowersList.count];

            [tblViewProfile reloadData];
        }
    }
}

/*! This API call is to get user followers list. */

-(void)getMyFollowersList:(int)pageNo
{
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&page_number=%d",GETVALUE(CEO_AccessToken),_strCurrentUserId,pageNo] dataUsingEncoding:NSUTF8StringEncoding];
    [YSAPICalls getProfileFollowers:submitData ForSuccessionBlock:^(id newResponse)
     {
         isApiCalling = NO;
         Generalmodel  *gmodel = newResponse;
         if ([gmodel.status_code integerValue]){
             NSLog(@"response == %@",newResponse);
             if (pageNoFollower == 1) {
                 if([arrFollowersList count])
                 [arrFollowersList removeAllObjects];
             }
             if(gmodel.tempArray.count>0)
             {
                 isFollowerDataFinishedFromServer = NO;
                 [arrFollowersList addObjectsFromArray:gmodel.tempArray];
             }
             else
             {
                 isFollowerDataFinishedFromServer = YES;
             }
             if(isPullToreFresh)
             {
             [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%@ Followers",gmodel.profile_totalcount] forSegmentAtIndex:0];
             [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%@ in My Circle",gmodel.profile_followingcount] forSegmentAtIndex:1];
             }
             [tblViewProfile reloadData];
             if([arrFollowingList count] == 0){
                 [self getInCircleList:1];
                 
             }
             if (pageNoFollower == 1 && _segmentedCOntrol.selectedSegmentIndex==0)
                 [self setHeightForTable:(int)arrFollowersList.count];
         }
         
     } andFailureBlock:^(NSError *error) {
         NSLog(@"Failed from the server %@",[error description]);
         
     }];
}

/*! This API call is to get user Incircle list. */

-(void)getInCircleList:(int)pageNo
{
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&page_number=%d",GETVALUE(CEO_AccessToken),_strCurrentUserId,pageNo] dataUsingEncoding:NSUTF8StringEncoding];
    [YSAPICalls getAllFriendsInCircle:submitData methodName:METHOD_PROFILEFOLLOWING  ForSuccessionBlock:^(id newResponse) {
        isApiCalling = NO;
        Generalmodel  *gmodel = newResponse;
        if ([gmodel.status_code integerValue]){
            NSLog(@"response == %@",newResponse);
            if (pageNoInCircle == 1) {
                if([arrFollowingList count])
                [arrFollowingList removeAllObjects];
            }
            if(gmodel.tempArray.count>0)
            {
                isInCircleDataFinishedFromServer = NO;
                [arrFollowingList addObjectsFromArray:gmodel.tempArray];
            }
            else
            {
                isInCircleDataFinishedFromServer = YES;
            }
            if(isPullToreFresh)
            {
            [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%@ in My Circle",gmodel.profile_totalcount] forSegmentAtIndex:1];
            [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%@ Followers ",gmodel.profile_followers_count] forSegmentAtIndex:0];
            }
            [tblViewProfile reloadData];
            if (pageNoInCircle == 1 && _segmentedCOntrol.selectedSegmentIndex==1)
                [self setHeightForTable:(int)arrFollowingList.count];
            
        }
    }andFailureBlock:^(NSError *error) {
        
    }];
    
}

/*! This API call is to FOLLOW/UNFOLLOW in followers. */

-(void)selectionFollow : (UIButton *) sender
{
    Profilemodel *profilelist;
    if(_segmentedCOntrol.selectedSegmentIndex == 0)
        profilelist= [arrFollowersList objectAtIndex:[sender tag]];
    else
        profilelist= [arrFollowingList objectAtIndex:[sender tag]];
    
    [YSAPICalls followUnfollowMint:profilelist withSuccessionBlock:^(id newResponse) {
        Generalmodel *gModel = newResponse;
        if ([gModel.status_code isEqualToString:@"1"]) {
            if ([gModel.status_Msg isEqualToString:@"followed"])  {
                profilelist.user_follow_status = @"Yes";
                profilelist.follow_status = @"1";
                [self updateInCircleWithObject:profilelist selectedIndex:_segmentedCOntrol needDelete:NO];
            }
            else
            {
                    profilelist.user_follow_status = @"No";
                    if(_segmentedCOntrol.selectedSegmentIndex == 0)
                    {
                        [self updateInCircleWithObject:profilelist selectedIndex:_segmentedCOntrol needDelete:YES];
                    }
                    
                    else
                    {
                        [self checkFollowersSegmentTitleWithObject:profilelist selectedIndex:_segmentedCOntrol];
                    }
            }
            
                 [tblViewProfile reloadData];
        }
        else
        {
            NSLog(@"error Msg=%@",gModel.status_Msg);
        }
    }
     
                   andFailureBlock:^(NSError *error){
        NSLog(@"Error = %@",error);
    }];
    
}


-(void)checkFollowersSegmentTitleWithObject:(Profilemodel *)profileData selectedIndex:(UISegmentedControl *)segMent
{
    
    if ([_strCurrentUserId isEqualToString:GETVALUE(CEO_UserId)])
                        {
        for(int i=0;i<[arrFollowersList count];i++)
        {
            if ([[[arrFollowersList objectAtIndex:i] valueForKey:@"follower_userid"] isEqualToString:profileData.follower_userid])
            {
                [arrFollowersList replaceObjectAtIndex:i withObject:profileData];
            }
        }
        
        NSInteger nInCirclecount = [[[[_segmentedCOntrol titleForSegmentAtIndex:1] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]-1;
        
        [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%ld in My Circle",(long)nInCirclecount] forSegmentAtIndex:1];
        
        for(int i=0;i<[arrFollowingList count];i++)
        {
            if ([[[arrFollowingList objectAtIndex:i] valueForKey:@"follower_userid"] isEqualToString:profileData.follower_userid])
                [arrFollowingList removeObjectAtIndex:i];
        }
  }
    
    else
    {
        for(int i=0;i<[arrFollowersList count];i++)
        {
            if ([[[arrFollowersList objectAtIndex:i] valueForKey:@"follower_userid"] isEqualToString:profileData.follower_userid])
            {
                [arrFollowersList replaceObjectAtIndex:i withObject:profileData];
            }
        }
        
        
        for(int i=0;i<[arrFollowingList count];i++)
        {
            if ([[[arrFollowingList objectAtIndex:i] valueForKey:@"follower_userid"] isEqualToString:profileData.follower_userid])
                [arrFollowingList replaceObjectAtIndex:i withObject:profileData];
        }
    }
    
    [tblViewProfile reloadData];
}

-(void)updateInCircleWithObject:(Profilemodel *)profileData selectedIndex:(UISegmentedControl *)segMent needDelete:(BOOL)isDelete
{
    
    if (isDelete) {
        
        if ([_strCurrentUserId isEqualToString:GETVALUE(CEO_UserId)])
        {

        NSInteger nInCirclecount = [[[[_segmentedCOntrol titleForSegmentAtIndex:1] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]-1;
        [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%ld in My Circle",(long)nInCirclecount] forSegmentAtIndex:1];
        for(int i=0;i<[arrFollowingList count];i++)
        {
            if ([[[arrFollowingList objectAtIndex:i] valueForKey:@"follower_userid"] isEqualToString:profileData.follower_userid])
            {
                [arrFollowingList removeObjectAtIndex:i];
            }
        }
    }
        
        else
        {
            for(int i=0;i<[arrFollowingList count];i++)
            {
                if ([[[arrFollowingList objectAtIndex:i] valueForKey:@"follower_userid"] isEqualToString:profileData.follower_userid])
                {
                    [arrFollowingList replaceObjectAtIndex:i withObject:profileData];
                }
            }

            
        }
        
    }
    else
    {
        if ([_strCurrentUserId isEqualToString:GETVALUE(CEO_UserId)])
        {
        NSInteger nInCirclecount = [[[[_segmentedCOntrol titleForSegmentAtIndex:1] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]+1;
        [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%ld in My Circle",(long)nInCirclecount] forSegmentAtIndex:1];
        [arrFollowingList addObject:profileData];

        }
    else
    {
        for(int i=0;i<[arrFollowingList count];i++)
        {
            if ([[[arrFollowingList objectAtIndex:i] valueForKey:@"follower_userid"] isEqualToString:profileData.follower_userid])
            {
                [arrFollowingList replaceObjectAtIndex:i withObject:profileData];
            }
        }
    }
    }

    [tblViewProfile reloadData];

}


//-(void)updateFollowingSegmentTitle:(NSArray*)arrList
//{
//    int nInCirclecount = 0;
//    for(NSString *status in [arrList valueForKey:@"user_follow_status"])
//    {
//        if([status isEqualToString:@"Yes"])
//            nInCirclecount += 1;
//    }
//    [_segmentedCOntrol setTitle:[NSString stringWithFormat:@"%d in MyCircle",nInCirclecount] forSegmentAtIndex:1];
//}



/*! get profile information when clicked on profile image from Followers/InMyCircle List. */

-(void)FollowerProfile: (UIButton *) sender
{
    Profilemodel *pmodel;
    if(_segmentedCOntrol.selectedSegmentIndex == 0 )
    {
        pmodel = [arrFollowersList objectAtIndex:sender.tag];
    }
    if(_segmentedCOntrol.selectedSegmentIndex == 1 )
    {
        pmodel = [arrFollowingList objectAtIndex:sender.tag];
    }
    
    _strCurrentUserId = pmodel.follower_userid;
    [self gotoProfileView:_strCurrentUserId];

    
}

-(void)gotoProfileView:(NSString *)userId{
    [self.pFollowerDelegate updateUserProfileDetails:userId];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_segmentedCOntrol.selectedSegmentIndex == 0 )
        return [arrFollowersList count];
    else
        return [arrFollowingList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 55;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier3 = @"CellFollowers";
    Profilemodel *pmodel;
    CellFollowers  *cell = (CellFollowers *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier3];
    if(_segmentedCOntrol.selectedSegmentIndex == 0 )
    {
        if(arrFollowersList.count > 0)
        {
            pmodel = [arrFollowersList objectAtIndex:indexPath.row];
        }
    }
    else if(_segmentedCOntrol.selectedSegmentIndex == 1 )
    {
        if(arrFollowingList.count > 0)
        {
            pmodel = [arrFollowingList objectAtIndex:indexPath.row];
        }
    }
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier3 owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.backgroundColor=[UIColor clearColor];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    cell.lblfllw.text       = pmodel.name;
    cell.profileFllwimgV.mintImageURL  = [NSURL URLWithString:pmodel.user_thumb_image];
    NSLog(@"pmodel.follower_userid == %@",pmodel.follower_userid);
    NSLog(@"GETVALUE(CEO_UserId) == %@",GETVALUE(CEO_UserId));

    if([pmodel.follower_userid  isEqualToString:GETVALUE(CEO_UserId)])
    {
               cell.btnFollow.hidden = YES;
    }
//    else
//    {
        if ([pmodel.user_follow_status isEqualToString:@"Yes"])
        {
            [cell.btnFollow  setTitle: @"IN CIRCLE" forState: UIControlStateNormal];
            [cell.btnFollow  setBackgroundColor:[UIColor whiteColor]];
            [cell.btnFollow  setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
        }
        else
        {
            [cell.btnFollow  setTitle: @"FOLLOW" forState: UIControlStateNormal];
            [cell.btnFollow  setBackgroundColor:ORANGECOLOR];
            [cell.btnFollow  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
//    }
    cell.btnFollow.tag    = indexPath.row;
    cell.btnUserImage.tag = indexPath.row;
    [cell.btnFollow addTarget:self action:@selector(selectionFollow:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnUserImage addTarget:self action:@selector(FollowerProfile:) forControlEvents:UIControlEventTouchUpInside];
   
      return  cell;
}


#pragma mark - UIScrollViewDelegate

//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(isApiCalling)
        return;
    
    BOOL shouldLoadNextPage = ShouldLoadNextPage(tblViewProfile);
    if(shouldLoadNextPage)
    {
        isPullToreFresh  =  NO;
        isApiCalling = YES;
        if(_segmentedCOntrol.selectedSegmentIndex == 0)
        {
            if (isFollowerDataFinishedFromServer) {
                return;
            }
            pageNoFollower = pageNoFollower+1;
            [self getMyFollowersList:pageNoFollower];
        }
        else if (_segmentedCOntrol.selectedSegmentIndex == 1)
        {
            if (isInCircleDataFinishedFromServer) {
                return;
            }
            pageNoInCircle = pageNoInCircle+1;
            [self getInCircleList:pageNoInCircle];
        }
    }
}

-(void)setHeightForTable:(int)nCount
{
    CGRect frameView = tblViewProfile.frame;
    CGFloat hight = [self.pFollowerDelegate getAcheivementtextHeight];
    if (hight<13) {
        if (nCount==6) {
            frameView.size.height =  frameViewheight-25;
        }
        else if (nCount==7) {
            frameView.size.height =  frameViewheight;
        }
        else if(nCount==8)
            frameView.size.height =  frameViewheight+30;
        else
            frameView.size.height =  frameViewheight+40;
    }
    else if (hight<26)
    {
        if (nCount==6) {
            frameView.size.height =  frameViewheight-25;
        }
        else if (nCount==7) {
            frameView.size.height =  frameViewheight;
        }
        else if (nCount==8)
            frameView.size.height =  frameViewheight+25;
        else
            frameView.size.height =  frameViewheight+35;
        
    }
    else if (hight<39)
    {
        if (nCount==6) {
            frameView.size.height =  frameViewheight-40;
        }
        else if (nCount==7) {
            frameView.size.height =  frameViewheight-10;
        }
        else  if (nCount==8)
            frameView.size.height =  frameViewheight+20;
        else
            frameView.size.height =  frameViewheight+35;
    }
    else if (hight>39)
    {
        if (nCount==6) {
            frameView.size.height =  frameViewheight-40;
        }
        else if (nCount==7) {
            frameView.size.height =  frameViewheight-15;
        }
        else  if (nCount==8)
            frameView.size.height =  frameViewheight+10;
        else
            frameView.size.height =  frameViewheight+40;
    }
    
    tblViewProfile.frame = frameView;
}

//-(void)setHeightForTable:(int)nCount
//{
//    CGRect frameView = tblViewProfile.frame;
//    if (nCount==6) {
//        frameView.size.height =  frameViewheight-40;
//    }
//    else if (nCount==7) {
//        frameView.size.height =  frameViewheight;
//    }
//    else
//        frameView.size.height =  frameViewheight+30;
//    
//    tblViewProfile.frame = frameView;
//}


@end
