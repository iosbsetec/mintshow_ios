//
//  Profilemodel.m
//  Your Show
//
//  Created by bsetec on 1/5/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "Profilemodel.h"

@implementation Profilemodel

-(Profilemodel *)initWithProfileListDict:(NSDictionary*)ProfileList
{
    self.follow_status              =   [ProfileList   valueForKey:@"follow_status"];
    self.name                       =   [ProfileList   valueForKey:@"name"];
    self.refer_status               =   [ProfileList   valueForKey:@"refer_status"];
    self.user_follow_status         =   [ProfileList   valueForKey:@"user_follow_status"];
    self.follower_userid            =   [[ProfileList   valueForKey:@"user_id"] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    self.user_name                  =   [ProfileList   valueForKey:@"user_name"];
    self.user_thumb_image           =   [ProfileList   valueForKey:@"user_thumb_image"];
    self.following_count            =   [ProfileList   valueForKey:@"following_count"];

    return self;
}





@end
