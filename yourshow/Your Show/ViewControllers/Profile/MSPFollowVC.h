//
//  ThirdViewController.h
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MSPFollowerDelegate
-(void)updateUserProfileDetails:(NSString*)userId;
-(void)makeScrollUpWithContenYPosition:(CGFloat)viewHeight;
-(void)makeScrollDownWithContenYPosition:(CGFloat)viewHeight;
-(CGFloat)getAcheivementtextHeight;
@end

@interface MSPFollowVC : UIViewController
@property (nonatomic)   NSString *strCurrentUserId;
@property (nonatomic) id <MSPFollowerDelegate> pFollowerDelegate;
@property (strong, nonatomic) NSString  *followersCount;
@property (strong, nonatomic) NSString  *inMycircleCount;
@property (nonatomic) CGFloat lastContentOffset;

@end
