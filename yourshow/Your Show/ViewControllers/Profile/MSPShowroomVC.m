//
//  SecondViewController.m
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import "MSPShowroomVC.h"

#import "DiscoverShowroomViewController.h"
#import "MintShowroomViewController.h"
#import "CarbonKit.h"
#import "JitenCollectionView.h"
#import "AddEditShowroomVC.h"
#import "UserAccessSession.h"

@interface MSPShowroomVC () <CarbonTabSwipeNavigationDelegate,JitenCollectionTableDelegate,JitenCollectionViewDataSource,AddEditShowroomVCDelegate,ParticularShowroomDelegate>

{
    CarbonSwipeRefresh *refreshCarbon;
    NSMutableArray     *allShowRoomdata;
    NSMutableArray     *createdShwroomsdata,*joinedShwroomsdata;

    int     pagenumberShowroom,showroomtype;
    BOOL    isLoading;
    int     pageNoShwrmCreated,pageNoShwrmJoined;
    int     shwrmtypeCreated,shwrmtypeJoined;
    BOOL    isCreatedShwrmFinishedFromServer,isJoinedShwrmFinishedFromServer,isPullToreFresh;
    CGFloat lastContentOffset;
    NSString *addShowRoomCount,*unJoinShowroomCount;

    CGFloat frameViewheight;

    IBOutlet UISegmentedControl *segmentedControlShwrooms;
    IBOutlet UIView* viewNoShowroom;

}
@property (strong, nonatomic) IBOutlet JitenCollectionView *showRoomCollectionview;

@end



@implementation MSPShowroomVC


- (void)viewDidLoad {
    [super viewDidLoad];
    [viewNoShowroom setHidden:YES];
    allShowRoomdata     = [[NSMutableArray alloc]init];
    createdShwroomsdata = [[NSMutableArray alloc]init];
    joinedShwroomsdata  = [[NSMutableArray alloc]init];

    //allShowRoomdata = [UserAccessSession getAllrecordForShowroomList];
    pagenumberShowroom = 1;
    pageNoShwrmCreated = 1;
    pageNoShwrmJoined  =  1;
    
    shwrmtypeCreated = 1;
    shwrmtypeJoined  = 2;
    isPullToreFresh = NO;

    // Changes for iPad done by mani
    if (isSmallPad) {
        CGRect rctTransparent = self.view.frame;
        rctTransparent.size.height = self.view.frame.size.height-80;
        [self.view setFrame:rctTransparent];
    }
    
    frameViewheight = self.view.frame.size.height;
    CGRect frameSelfView = self.view.frame;
    frameSelfView.size.height = frameViewheight+89;
    self.view.frame = frameSelfView;
    CGRect frameView = self.showRoomCollectionview.frame;
    frameView.size.height =  frameViewheight+80;
    self.showRoomCollectionview.frame = frameView;
    
    UIColor *backgroundCol =  [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.view.backgroundColor = backgroundCol;
    
    self.showRoomCollectionview.collectiondelegate   =  self;
    self.showRoomCollectionview.CollectionDataSource =  self;
    self.showRoomCollectionview.Addshowroom          =  YES;
    self.showRoomCollectionview.showJoinButton       =  NO;
    self.showRoomCollectionview.alwaysBounceVertical =  YES;
    
    
    [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%@ Created",_createdShowRoomCount] forSegmentAtIndex:0];
    [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%@ Joined",_joinedShowRoomCount] forSegmentAtIndex:1];

    [self createRefreshView];
    
    segmentedControlShwrooms.selectedSegmentIndex = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(deleteShowroomFromList)  name:@"DeleteShowRoom" object:nil];
    // Do any additional setup after loading the view from its nib.

}
-(void)viewDidAppear:(BOOL)animated
{

    [super viewDidAppear:animated];
    if(segmentedControlShwrooms.selectedSegmentIndex == 0)
    {
        pageNoShwrmCreated = 1;
    }
    else
    {
        pageNoShwrmJoined = 1;
    }
    
    
    [self segmentTapped:segmentedControlShwrooms];

}
#pragma mark - YSCollectionView Delegates
-(void)didScrolledDown:(CGFloat)yPosition
{
    CGRect frameView = self.showRoomCollectionview.frame;
    frameView.size.height =  frameViewheight+20;
    self.showRoomCollectionview.frame = frameView;
    [self.psdelegate makeScrollDownWithContenYPosition:yPosition];
}

-(void)didScrolledUp:(CGFloat)yPosition
{
    [self.psdelegate makeScrollUpWithContenYPosition:yPosition];
}
- (void)createRefreshView
{
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:self.showRoomCollectionview withYPos:70];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
}
#pragma mark - QBRefreshControlDelegate


- (void)refresh:(id)sender {
    isPullToreFresh = YES;
    if(segmentedControlShwrooms.selectedSegmentIndex == 0)
    {
        pageNoShwrmCreated = 1;
        [self getShowroomListwithPagenum:pageNoShwrmCreated :shwrmtypeCreated];
       
    }
    else
    {
        pageNoShwrmJoined = 1;
        [self getShowroomListwithPagenum:pageNoShwrmJoined :shwrmtypeJoined];
    }
    
    

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}


- (void)endRefreshing {
    [refreshCarbon endRefreshing];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Server Call
- (void)getShowroomListwithPagenum:(int)pagenum :(int)shwrmtype
{
    isLoading = YES;
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&showroom_list_type=%d&page_number=%d&user_id=%@",GETVALUE(CEO_AccessToken),shwrmtype,pagenum,_profileUserId];
    NSLog(@"_profileUserId == %@",_profileUserId);
    NSLog(@"submitDta123 data %@",submitData);

    [self CallAPIForShowroomsUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@customshowroom/get_all_showrooms_list_type?%@",ServerUrl,submitData]] :shwrmtype];
   }
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_showRoomCollectionview setContentOffset:CGPointMake(0, 0)];
    _showRoomCollectionview.scrollsToTop = YES;
}

-(void)CallAPIForShowroomsUrl:(NSURL *)myUrl :(int)showromType
{
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
        NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"Status"]);
        if ([[showroomDict valueForKey:@"Status"]isEqualToString:@"Success"])  {
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            isLoading = NO;
            if(showromType == shwrmtypeCreated){
                    if (pageNoShwrmCreated == 1) {
                            if([createdShwroomsdata count])
                            [createdShwroomsdata removeAllObjects];
                          }

                    if([[showroomDict valueForKey:@"created"] count] > 0){
                            for(NSDictionary *showRoom in [showroomDict valueForKey:@"created"])
                            {
                                [createdShwroomsdata addObject:[[ShowRoomModel alloc]initWithShowRoomDict:showRoom ]];
                            }
                             isCreatedShwrmFinishedFromServer = NO;
                        }
                    else{
                        isCreatedShwrmFinishedFromServer = YES;
                        }
                
                
                if(![_profileUserId isEqualToString:GETVALUE(CEO_UserId)] && segmentedControlShwrooms.selectedSegmentIndex == 0 )
                    [viewNoShowroom setHidden:(createdShwroomsdata.count>0)?YES:NO];
                else
                    [viewNoShowroom setHidden:YES];
                
                
            }
            
            else if (showromType ==  shwrmtypeJoined)
            {
                [viewNoShowroom setHidden:YES];

                if (pageNoShwrmJoined == 1) {
                    if([joinedShwroomsdata count])
                        [joinedShwroomsdata removeAllObjects];
                }

                if([[showroomDict valueForKey:@"joined"] count] > 0){
                    for(NSDictionary *showRoom in [showroomDict valueForKey:@"joined"])
                    {
                        [joinedShwroomsdata addObject:[[ShowRoomModel alloc]initWithShowRoomDict:showRoom ]];
                        NSLog(@"List are == %@",[showroomDict valueForKey:@"joined"]);
                    }
                    isJoinedShwrmFinishedFromServer = NO;
                    
                }
                else
                {
                    isJoinedShwrmFinishedFromServer = YES;
                }
            }
            
            if ([showroomDict valueForKey:@"created_count"] && [showroomDict valueForKey:@"joined_count"]) {
                addShowRoomCount        = [showroomDict valueForKey:@"created_count"];
                unJoinShowroomCount     = [showroomDict valueForKey:@"joined_count"];
                
                
                [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%@ Created",addShowRoomCount] forSegmentAtIndex:0];
                [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%@ Joined",unJoinShowroomCount] forSegmentAtIndex:1];
                
                if ([self.psdelegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])

                [self.psdelegate updateTabsCountWithCount:((int)[[[[segmentedControlShwrooms titleForSegmentAtIndex:0] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]+(int)[[[[segmentedControlShwrooms titleForSegmentAtIndex:1] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]) ForTab:1];
            }
            [self ReloadShowroomCollectionView];
            
        }
    }];
    
}


-(void)ReloadShowroomCollectionView
{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.showRoomCollectionview performBatchUpdates:^{
        [self.showRoomCollectionview reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
            
        }
    }];
}
//  Outside the implementation block:


- (void)loadNextShowrooms{
    
    if(!isLoading){
        isPullToreFresh = NO;
        if(segmentedControlShwrooms.selectedSegmentIndex == 0)
        {
            if (isCreatedShwrmFinishedFromServer) {
                return;
            }
            pageNoShwrmCreated = pageNoShwrmCreated+1;
            [self getShowroomListwithPagenum:pageNoShwrmCreated :shwrmtypeCreated];
        }
        else if(segmentedControlShwrooms.selectedSegmentIndex == 1)
        {
            if (isJoinedShwrmFinishedFromServer) {
                return;
            }
            pageNoShwrmJoined = pageNoShwrmJoined+1;
            [self getShowroomListwithPagenum:pageNoShwrmJoined :shwrmtypeJoined];
        }
        
    }
}

#pragma mark - THIS CODE FOR SHOWROOM COLLECTION VIEW


-(JitenCollectionView *)showroomCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.showRoomCollectionview;
}
- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
   
    if(segmentedControlShwrooms.selectedSegmentIndex == 0 )
    {
        if(createdShwroomsdata.count > 0)
        return  (ShowRoomModel *)[createdShwroomsdata objectAtIndex:row];
    }

        return  (ShowRoomModel *)[joinedShwroomsdata objectAtIndex:row];
  
}


- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView{
//    return allShowRoomdata.count;
    
    if(segmentedControlShwrooms.selectedSegmentIndex == 0 )
        return  createdShwroomsdata.count;
    else
        return  joinedShwroomsdata.count;
    
}
- (void) JitenCollectionViewDelegateMethod: (ShowRoomModel *) model selectedIndex :(NSInteger) selectedRowNo{
    
  //  AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
  //  delegateApp.lbl.hidden = YES;
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.delegate = self;
    vcShowRoom.isFromProfileShowroom  = YES;
    vcShowRoom.modelShow = model;
    [self.navigationController pushViewController:vcShowRoom animated:YES];
    
    
}

-(void)JitenCollectionViewAddClicked:(JitenCollectionView *)sender{
    AppDelegate* delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = YES;
   // delegateApp.lbl.hidden = YES;
    AddEditShowroomVC *samplePopupViewController = [[AddEditShowroomVC alloc] initWithNibName:@"AddEditShowroomVC" bundle:nil];
    samplePopupViewController.aDelegate = self;
    samplePopupViewController.hidesBottomBarWhenPushed = YES;
//    [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%d Created",[_createdShowRoomCount intValue] + 1] forSegmentAtIndex:0];

    [self.navigationController pushViewController:samplePopupViewController animated:YES];
    
}
- (void)showroomAddedOrEditedSuccesFully:(NSString *)showroomName
{
    pageNoShwrmCreated = 1;
    [self getShowroomListwithPagenum:pageNoShwrmCreated:shwrmtypeCreated];
    NSInteger createdcount = [[[[segmentedControlShwrooms titleForSegmentAtIndex:segmentedControlShwrooms.selectedSegmentIndex] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]+1;
    [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%ld Created",(long)createdcount] forSegmentAtIndex:0];
    if ([self.psdelegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])

    [self.psdelegate updateTabsCountWithCount:((int)createdcount+(int)[[[[segmentedControlShwrooms titleForSegmentAtIndex:1] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]) ForTab:1];
    [self gotoParticularShowroom:showroomName];

}

-(void)gotoParticularShowroom:(NSString *)selectedEnitity{

    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.delegate = self;
    vcShowRoom.isFromProfileShowroom  = YES;
    vcShowRoom.selectedShowroom = selectedEnitity;
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}

- (void) JitenCollectionViewJoinClicked: (NSInteger) selectedRow
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.isAnyShowroomJoinUnjoin = YES;
    if(segmentedControlShwrooms.selectedSegmentIndex == 0 )
    {
        ShowRoomModel *model = [createdShwroomsdata objectAtIndex:selectedRow];
        model.show_option = ([model.show_option isEqualToString:@""])?@"FOLLOWER":@"";
        model.is_user_joined     = ([model.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
        model.member_count     = [NSString stringWithFormat:@"%d",([model.is_user_joined isEqualToString:@"YES"])?([model.member_count intValue]+1):([model.member_count intValue]-1)];

        [createdShwroomsdata replaceObjectAtIndex:selectedRow withObject:model];
    }
    else
    {
        ShowRoomModel *model = [joinedShwroomsdata objectAtIndex:selectedRow];
        model.show_option = ([model.show_option isEqualToString:@""])?@"FOLLOWER":@"";
        model.is_user_joined     = ([model.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
        model.member_count     = [NSString stringWithFormat:@"%d",([model.is_user_joined isEqualToString:@"YES"])?([model.member_count intValue]+1):([model.member_count intValue]-1)];

        if([_profileUserId isEqualToString:GETVALUE(CEO_UserId)])
        {
            NSInteger joinedcount;
            if([model.is_user_joined  isEqualToString:@"NO"])
            joinedcount = [[[[segmentedControlShwrooms titleForSegmentAtIndex:segmentedControlShwrooms.selectedSegmentIndex] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]-1;
            else
             joinedcount = [[[[segmentedControlShwrooms titleForSegmentAtIndex:segmentedControlShwrooms.selectedSegmentIndex] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]+1;
            
            [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%ld Joined",(long)joinedcount] forSegmentAtIndex:1];
            
            if ([self.psdelegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])

            [self.psdelegate updateTabsCountWithCount:((int)joinedcount+(int)[[[[segmentedControlShwrooms titleForSegmentAtIndex:0] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]) ForTab:1];
        }
            [joinedShwroomsdata replaceObjectAtIndex:selectedRow withObject:model];
        }
        
    
    [self ReloadShowroomCollectionView];
    
    }


- (void)dismissPopup{
    
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
 //   delegateApp.lbl.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
    //    if (self.popupViewController != nil) {
    //        [self dismissPopupViewControllerAnimated:YES completion:^{
    //            NSLog(@"popup view dismissed");
    //        }];
    //    }
}

/*! This segment action is for Showroomscreated and ShowroomsJoined in showrooms tab. */

- (IBAction)segmentTapped:(id)sender {
    
    if(segmentedControlShwrooms.selectedSegmentIndex == 0)
    {
        if([_profileUserId isEqualToString:GETVALUE(CEO_UserId)] && segmentedControlShwrooms.selectedSegmentIndex == 0 )
        {
            self.showRoomCollectionview.Addshowroom    = YES;
            self.showRoomCollectionview.showJoinButton = NO;
        }
        else
        {
            self.showRoomCollectionview.Addshowroom    = NO;
            self.showRoomCollectionview.showJoinButton = YES;
        }

      
            [self getShowroomListwithPagenum:pageNoShwrmCreated :shwrmtypeCreated];
        
    
    }
    else
        
    {
        self.showRoomCollectionview.Addshowroom  = NO;
        self.showRoomCollectionview.showJoinButton = YES;
        
            [self getShowroomListwithPagenum:pageNoShwrmJoined :shwrmtypeJoined];
        
    }
    [self ReloadShowroomCollectionView];

}

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}

- (void)showroomJoinUnjoin:(NSString *)showTagName memberCount:(NSString *)count join:(BOOL)isJoiend
{
    if(!isJoiend)
    {
    if(segmentedControlShwrooms.selectedSegmentIndex == 1){
        
        for (int start = 0; start <joinedShwroomsdata.count; start++) {
            if ([[[joinedShwroomsdata objectAtIndex:start]show_tag] isEqualToString:showTagName]) {

        ShowRoomModel *model = [joinedShwroomsdata objectAtIndex:start];
        model.show_option = ([model.show_option isEqualToString:@""])?@"FOLLOWER":@"";
        model.is_user_joined     = ([model.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
        if([_profileUserId isEqualToString:GETVALUE(CEO_UserId)])
        {
            
            NSInteger joinedcount = [[[[segmentedControlShwrooms titleForSegmentAtIndex:segmentedControlShwrooms.selectedSegmentIndex] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]-1;
            
            [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%ld Joined",(long)joinedcount] forSegmentAtIndex:1];
            
            if ([self.psdelegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])

            
            [self.psdelegate updateTabsCountWithCount:((int)joinedcount+(int)[[[[segmentedControlShwrooms titleForSegmentAtIndex:0] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]) ForTab:1];
            
//            for (int start = 0; start <joinedShwroomsdata.count; start++) {
//                if ([[[joinedShwroomsdata objectAtIndex:start]show_tag] isEqualToString:model.show_tag])
//                {
//                    [joinedShwroomsdata replaceObjectAtIndex:start withObject:model];
//                }
//            }

            
        }
//                else
//                    
//                {
//                    for (int start = 0; start <joinedShwroomsdata.count; start++) {
//                        if ([[[joinedShwroomsdata objectAtIndex:start]show_tag] isEqualToString:model.show_tag])
//                        {

                            [joinedShwroomsdata replaceObjectAtIndex:start withObject:model];
//                        }
//                    }

                }
            }
        }
//    }
    
    else
    {
        for (int start = 0; start <createdShwroomsdata.count; start++) {
            if ([[[createdShwroomsdata objectAtIndex:start]show_tag] isEqualToString:showTagName]) {
                
                ShowRoomModel *model   = [createdShwroomsdata objectAtIndex:start];
                model.show_option      = ([model.show_option isEqualToString:@""])?@"FOLLOWER":@"";
                model.member_count     = count;
                model.is_user_joined   = ([model.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
                [createdShwroomsdata replaceObjectAtIndex:start withObject:model];
                
                
            }
            
        }
    }
    [self ReloadShowroomCollectionView];
  
}
else
{
    if(segmentedControlShwrooms.selectedSegmentIndex == 1){
        
                if([_profileUserId isEqualToString:GETVALUE(CEO_UserId)])
                {
//                        shwrmmodel.show_option     = ([shwrmmodel.show_option isEqualToString:@""])?@"FOLLOWER":@"";
//                        shwrmmodel.is_user_joined     = ([shwrmmodel.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
//                       [joinedShwroomsdata replaceObjectAtIndex:start withObject:model];
                        [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%lu Joined",joinedShwroomsdata.count ] forSegmentAtIndex:1];
                    if ([self.psdelegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])

                     [self.psdelegate updateTabsCountWithCount:((int)joinedShwroomsdata.count+(int)[[[[segmentedControlShwrooms titleForSegmentAtIndex:0] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]) ForTab:1];
                        [self ReloadShowroomCollectionView];
                    
                }
//                else
//                    
//                {
                    for (int start = 0; start <joinedShwroomsdata.count; start++) {
                        if ([[[joinedShwroomsdata objectAtIndex:start]show_tag] isEqualToString:showTagName])
                        {
                            ShowRoomModel *model = [joinedShwroomsdata objectAtIndex:start];
                            model.show_option = ([model.show_option isEqualToString:@""])?@"FOLLOWER":@"";
                            model.is_user_joined     = ([model.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
                            [joinedShwroomsdata replaceObjectAtIndex:start withObject:model];
                        }
                    }
                    
                }
//            }
    
    else
    {
        for (int start = 0; start <createdShwroomsdata.count; start++) {
            if ([[[createdShwroomsdata objectAtIndex:start]show_tag] isEqualToString:showTagName]) {
                ShowRoomModel *model   = [createdShwroomsdata objectAtIndex:start];
                model.show_option      = ([model.show_option isEqualToString:@""])?@"FOLLOWER":@"";
                model.member_count     = count;
                model.is_user_joined   = ([model.is_user_joined isEqualToString:@"YES"])?@"NO":@"YES";
                [createdShwroomsdata replaceObjectAtIndex:start withObject:model];
                
            }
            
        }
    }
    [self ReloadShowroomCollectionView];
//
    
                    }
    


    }


-(void)deleteShowroomFromList
{
    pageNoShwrmCreated = 1;
    [self getShowroomListwithPagenum:pageNoShwrmCreated :shwrmtypeCreated];
    NSInteger deletedcount = [[[[segmentedControlShwrooms titleForSegmentAtIndex:segmentedControlShwrooms.selectedSegmentIndex] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]-1;
    
    [segmentedControlShwrooms setTitle:[NSString stringWithFormat:@"%ld Created",(long)deletedcount] forSegmentAtIndex:0];
    if ([self.psdelegate respondsToSelector:@selector(updateTabsCountWithCount:ForTab:)])

    [self.psdelegate updateTabsCountWithCount:((int)deletedcount+(int)[[[[segmentedControlShwrooms titleForSegmentAtIndex:1] componentsSeparatedByString:@" "] objectAtIndex:0] integerValue]) ForTab:1];




}


@end
