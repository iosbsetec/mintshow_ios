//
//  ProfileEditViewController.m
//  YACC
//
//  Created by Pothiraj_BseTec on 01/08/15.
//  Copyright (c) 2015 BseTech. All rights reserved.
//
#define MAXTEXTLENGTH 20

#import "ProfileEditViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AsyncImageView.h"
#import "CropImageVC.h"
#import "Profilemodel.h"
#import "ImageListViewController.h"
#import "AVCamViewController.h"
#import "VPImageCropperViewController.h"
#import "ImageCropView.h"
#import "BFViewController.h"

#define kDescriptionPlaceHolderText           @"What are you working to achieve now?"



#define URLRegex @"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))"

@interface ProfileEditViewController () <CropImageDelegate,NewPhotoGalleryDelegate,AVCamVCDelegate,VPImageCropperDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UserImageCropViewControllerDelegate>
{
    NSString    *strNewImage;
    UIImage     *finalImage ;
    AppDelegate *appDelegate;

    IBOutlet UILabel      *lblAcheivementCount;
    IBOutlet UITextField  *txtFldName;
    IBOutlet UITextField  *txtFldWebsite;
    IBOutlet UITextField  *txtFldLocation;
    IBOutlet UITextField  *txtFldLevel;
    IBOutlet UITextView   *txtViewAchvngNow;
    IBOutlet UIImageView  *imgUserPhoto;
    IBOutlet UITextField  *txtFldLastName;
    IBOutlet UIScrollView *scrollViewEditprofile;
    BOOL anyUpdates;
    
    BOOL isCameraIconTapped;
}

@end

@implementation ProfileEditViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUserInfomation];
    
    [self devicesize];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 400.0f, 50.0f, 50.0f)];
    view.layer.cornerRadius = 25;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor],(id)[[UIColor redColor] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    view.layer.borderWidth = 3.0f;
    [view.layer setBorderColor:(__bridge CGColorRef _Nullable)(gradient.colors)];
    [view setClipsToBounds:YES];
    
    [[[scrollViewEditprofile subviews]firstObject] addSubview:view];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    isCameraIconTapped = NO;
    [[UIApplication sharedApplication]setStatusBarHidden:YES];

}

/*! displays user information when comes to editprofile screen  . */

-(void)setUserInfomation
{
    imgUserPhoto.layer.cornerRadius = imgUserPhoto.frame.size.width / 2;
    imgUserPhoto.clipsToBounds      = YES;
    imgUserPhoto.layer.borderColor  = [UIColor lightGrayColor].CGColor;;
    imgUserPhoto.layer.borderWidth  = 2.0;
    
    txtViewAchvngNow.layer.borderWidth  = 2.0;
    txtViewAchvngNow.layer.borderColor  = [UIColor colorWithRed:244.0/255 green:244.0/255  blue:244.0/255  alpha:1.0].CGColor;
    txtViewAchvngNow.layer.cornerRadius = 4.0f;
    [txtViewAchvngNow setClipsToBounds:YES];

    txtFldName.text            = [NSString stringWithFormat:@"%@",_pmodeldata.user_first_name];
    txtFldLastName.text        =  _pmodeldata.user_last_name;
    txtFldWebsite.text         = _pmodeldata.website;
    txtFldLocation.text        = _pmodeldata.user_location;
    if ( [_pmodeldata.achieving_text length]) {
        txtViewAchvngNow.textColor = [UIColor darkGrayColor];
        txtViewAchvngNow.text      = _pmodeldata.achieving_text;

    }
    else
    {
    
        txtViewAchvngNow.textColor = [UIColor lightGrayColor];
        txtViewAchvngNow.text      = kDescriptionPlaceHolderText;
    }
    imgUserPhoto.mintImageURL  = [NSURL URLWithString:_pmodeldata.user_thumb_image];
    scrollViewEditprofile.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

/*!  action to save all edited user details  . */

- (IBAction)saveTapped:(id)sender {
    
    if ([_pmodeldata.user_first_name isEqualToString:txtFldName.text] && [_pmodeldata.user_last_name isEqualToString:txtFldLastName.text]
        && [_pmodeldata.website isEqualToString:txtFldWebsite.text] && [_pmodeldata.user_location isEqualToString:txtFldLocation.text] &&[_pmodeldata.achieving_text isEqualToString:txtViewAchvngNow.text] && ![strNewImage length])
    {
        
        return;
    }
    BOOL isOk =  [self checkUserGivenDetails];
    if (isOk) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self editprofileDetails];
    }
}

/*!  action when user clicks on profile image . */

- (IBAction)phototapped:(id)sender {

    UIButton* button = (UIButton*)sender;
    UIActionSheet * actionSheet = [[UIActionSheet alloc] initWithTitle:@"Update User's Profile Image" delegate:self
                                     cancelButtonTitle:@"Cancel"
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@"Take photo",@"Choose existing photo",nil];
    
    actionSheet.tintColor = [UIColor redColor];


    //*********** Cancel
     [actionSheet showFromRect:button.frame inView:self.view animated:YES];

}


-(void)emailResultsTapped:(UIButton*)sender
{

}
-(void)cancelButtonClicked:(UIButton*)sender
{
    
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        [self takePhoto];
    }
    else if (buttonIndex==1)
    {
        [self choosePhoto];
    }
}

- (void)takePhoto
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = NO;
        [self presentViewController:picker animated:YES completion:NULL];
        SPHSingletonClass *obj = [SPHSingletonClass sharedSingletonClass];
        obj.isCameraSelectedForUserProfile = YES;
        
    }

}

- (void)choosePhoto
{

    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
       
    if(isSmallPad)
    {
        ImageListViewController *ilist = [[ImageListViewController alloc]initWithNibName:@"ImageListViewController" bundle:nil];
        
        ilist.isShowCameraButton = NO;
        ilist.isMediaTypeImage = YES;
        ilist.delegate = self;
        [self presentViewController:ilist animated:YES completion:^{
            // Do some stuffs
        }];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        SPHSingletonClass *obj = [SPHSingletonClass sharedSingletonClass];
        obj.isCameraSelectedForUserProfile = NO;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    }
    
    else if (status == PHAuthorizationStatusDenied) {
        // Access has been denied.
        
        //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"app-settings:"]];
        [self cancelPhotoGallery];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Allow Access to your Photos"
                                      message:@"This allows MintShow to access photos from your library."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [alert.view setTintColor:[UIColor blackColor]];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Access"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"app-settings:"]];
                                 AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                                 [appDell authenticationDidFinish:self selectedInde:0];
                                 [alert dismissViewControllerAnimated:YES completion:nil];

                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Denie"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }

    
    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
            }
            
            else {
                // Access has been denied.
            }
        }];
    }
    

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    //imgUserPhoto.image = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(335, 335)];
    //[self uploadImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self cropBarButtonClick:chosenImage];
    
}


- (IBAction)cropBarButtonClick:(UIImage *)selectdImageVal {
    if(imgUserPhoto.image != nil){
//        ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:imgUserPhoto.image];
//        controller.delegate = self;
//        controller.blurredBackground = YES;
//        // set the cropped area
//        // controller.cropArea = CGRectMake(0, 0, 100, 200);
//        [[self navigationController] pushViewController:controller animated:YES];
        
        
        BFViewController *controller = [[BFViewController alloc] initWithNibName:@"BFViewController" bundle:nil];
        controller.delegate = self;
        controller.selectdImage = selectdImageVal;
        [[self navigationController] pushViewController:controller animated:YES];
        
        
    }
}


- (void)ImageCropViewControllerSuccess:(ImageCropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{
    finalImage = croppedImage;
    imgUserPhoto.image = croppedImage;
    //    CGRect cropArea = controller.cropArea;
    [self uploadImage];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller{
//    imgUserPhoto.image = finalImage;
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)ImageCropViewControllerRetake:(UIViewController *)controller
{
    [[self navigationController] popViewControllerAnimated:YES];
    SPHSingletonClass *obj = [SPHSingletonClass sharedSingletonClass];
    
    if (!obj.isCameraSelectedForUserProfile) {
        [self choosePhoto];
    }
    else
        [self takePhoto];
    
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)cameraImageTappedforPhotoGallery
{
    [self cameraClicked];
}

-(void)cancelPhotoGallery
{
    
}

#pragma mark For Image FromLibrary
- (void)didTappedCancelButton
{

}

- (void)gallryTappedFromCustomCameraScreen
{
    [self choosePhoto];
}

- (void)cameraTappeddidFinished:(id)editedImage{
    imgUserPhoto.image = editedImage;
    [self uploadImage];
}

-(void)cameraClicked
{
    if (isCameraIconTapped == NO) {
        AVCamViewController *cameraVC = [[AVCamViewController alloc]initWithNibName:@"AVCamViewController" bundle:nil];
        cameraVC.isFromProfileEditScreen = YES;
        cameraVC.delegate = self;
        [self.navigationController pushViewController:cameraVC animated:NO];
        isCameraIconTapped = YES;
    }
}

//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//{
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
//    
//    // for Getting Selected Image Name
//    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
//    {
//        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
//        NSLog(@"[imageRep filename] : %@", [imageRep filename]);
////        strNewImage = [imageRep filename];
//    };
//    
//    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
//    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
//    
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//    [self selectedImageFromGallery:chosenImage];
//}

-(void)selectedImageFromGallery:(UIImage *)selectedImage{
    [self cropBarButtonClick:selectedImage];
//    VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:selectedImage cropFrame:CGRectMake(0, 30.0,self.view.frame.size.width,360.0) limitScaleRatio:3.0];  //0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
//    imgCropperVC.delegate = self;
//    imgCropperVC.isFromCameraView = NO;
//    [self presentViewController:imgCropperVC animated:NO completion:^{
//        
//    }];
}

-(void)GalleryClicked
{
    [self choosePhoto];
}

#pragma mark - VPImageCropperViewControllerDelegate
- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController{
    
}


- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage{
    
    imgUserPhoto.image = editedImage;
    [self uploadImage];
    
    NSArray *viewControllers1 = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers1 count];i++){
        id obj=[viewControllers1 objectAtIndex:i];
        if([obj isKindOfClass:[ProfileEditViewController class]]){
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
}

//
//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
//{
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)uploadImage
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //UIImage *img    = [self imageWithImage:imgUserPhoto.image scaledToSize:CGSizeMake(335, 335)];
    UIImage *img    = [self imageResize:imgUserPhoto.image andResizeToWidth:335];

    NSData *imgData = UIImagePNGRepresentation(img);
    
    NSString *strPostUrl = [NSString stringWithFormat:@"%@/successprofile/ajax_image_successprofile.php",ServerWebUrl];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strPostUrl]];
    
    [request setValue:@"jludmkiswzxmrdf3qewfuhasqrcmdjoqply" forHTTPHeaderField:@"X-APPKEY"];
    [request setValue:@"185EA28DB5E8C0D3B1C9BCE633596943" forHTTPHeaderField:@"X-AUTHKEY"];
    [request setValue:@"iOS" forHTTPHeaderField:@"X-DEVICE"];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"ebrj6b3qril766r";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"user_id\"\r\n\r\n"
                      dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[GETVALUE(CEO_UserId)  dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Disposition: form-data; name=\"images\"; filename=\"images.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: image/png\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imgData];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (data != nil) {
             
             id jsonObjectData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             NSLog(@"Upload status=%@",jsonObjectData);
             
             if([[jsonObjectData objectForKey:@"status"] boolValue])
             {
                 [self.view endEditing:YES];
                 strNewImage = [jsonObjectData objectForKey:@"image_url"];
                [MBProgressHUD  hideHUDForView:self.view animated:YES];

                 
             }
         }
     }];
}




/*! This API call is to save edited profile details . */

-(void)editprofileDetails
{
    NSString *achivementText = [YSSupport getVal:txtViewAchvngNow.text];
    NSString *firstName = [YSSupport getVal:txtFldName.text];
    NSString *lastNmae = [YSSupport getVal:txtFldLastName.text];

    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue:GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:firstName forKey:@"first_name"];
    [dicts setValue:lastNmae forKey:@"last_name"];
    [dicts setValue:achivementText forKey:@"text"];
    [dicts setValue:([strNewImage length])?strNewImage:@"" forKey:@"user_image"];
    [dicts setValue:[[self removeWhiteSpaceChar:txtFldLocation.text] length]?[self removeWhiteSpaceChar:txtFldLocation.text]:@"" forKey:@"location_text"];
    [dicts setValue:[[self removeWhiteSpaceChar:txtFldWebsite.text] length]?txtFldWebsite.text:@"" forKey:@"url"];
    [dicts setValue:@"" forKey:@"lattitude"];
    [dicts setValue:@"" forKey:@"longitude"];
    
    
    [YSAPICalls editProfileDetails: dicts ForSuccessionBlock:^(id newResponse) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];

        Profilemodel  *pmodel = newResponse;
        if ([pmodel.status_code integerValue]){
            NSLog(@"response == %@",newResponse);
            showAlert(nil, @"Successfully Updated", @"ok", self);
            
            if([pmodel.userImageUrl length])
            {
                SETVALUE(pmodel.userImageUrl, CEO_UserImage) ;
            }
            
            Profilemodel *userInfo = _pmodeldata;
            userInfo.user_first_name = firstName;
            userInfo.user_last_name = lastNmae;
            userInfo.website = txtFldWebsite.text;
            userInfo.user_location = txtFldLocation.text;
            userInfo.achieving_text = achivementText;
            userInfo.user_thumb_image = ([pmodel.userImageUrl length])?pmodel.userImageUrl:_pmodeldata.userImageUrl;
            
            
            SETVALUE(userInfo.user_first_name,CEO_UserFirstName);
            SETVALUE(userInfo.user_last_name,CEO_UserLastName);
            
            

            NSDictionary *dict   =   [NSDictionary dictionaryWithObject:userInfo forKey:@"profileInfo"];

            
            [[NSNotificationCenter defaultCenter] postNotificationName:KPROFILEEDITED object:nil userInfo:dict];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } andFailureBlock:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        NSLog(@"Failed from the server %@",[error description]);
        
    }];

}

-(void)removeAlert:(UIAlertController *) alert{
    [alert dismissViewControllerAnimated:YES
                              completion:^{
                                  
                              }];
}


/*! This alert is shown if first name or last name fields are less than 2 characters. */

-(void)createAlertView : (NSString *) string
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, alert.view.frame.size.width-50, 40)];
    fromLabel.text = string;
    fromLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12]; //custom font
    fromLabel.numberOfLines = 1;
    fromLabel.baselineAdjustment = YES;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = ORANGECOLOR;
    fromLabel.textAlignment = NSTextAlignmentCenter;
    [alert.view addSubview:fromLabel];
    
    [self presentViewController:alert animated:YES completion:nil];
    [self performSelector:@selector(removeAlert:) withObject:alert afterDelay:0.9];
}


-(void)showDescKeyBoard :(CGFloat )height
{
   self.view.backgroundColor = [UIColor whiteColor];
    CGRect containerframe=self.view.frame;
    containerframe.origin.y=height;
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame=containerframe;
    }];
}


-(void)HideDescKeyboard
{
    CGRect containerframe=self.view.frame;
    containerframe.origin.y=0;
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame=containerframe;
    }];
}

#pragma mark - ******* TEXTFIELD DELEGATE METHODS *******

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if(textField == txtFldLevel)
//        [self showDescKeyBoard:-80.0];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    [self HideDescKeyboard];
}

#pragma mark - ******* Description Handler **********




//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
//{
//
//        
//        txtVWShowroomDescription.backgroundColor = SHOWROOMBACKGROUNDCOLOR;
//    }
//    return YES;
//}
//
//- (BOOL)textViewShouldEndEditing:(UITextView *)textView
//{
//    if (textView == txtVWShowroomDescription)
//    {
//        txtVWShowroomDescription.backgroundColor = [UIColor clearColor];
//    }
//    return YES;
//}

#pragma mark - ******* TEXTVIEW DELEGATE METHODS *******

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([txtViewAchvngNow.text isEqualToString:kDescriptionPlaceHolderText]) {
        txtViewAchvngNow.text = @"";
        txtViewAchvngNow.textColor = [UIColor darkGrayColor]; //optional
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([txtViewAchvngNow.text isEqualToString:@""]) {
        txtViewAchvngNow.text = kDescriptionPlaceHolderText;
        txtViewAchvngNow.textColor = [UIColor lightGrayColor]; //optional
    }
    
    [self HideDescKeyboard];
  
}
-(NSString *)removeWhiteSpaceChar:(NSString *)currentStr
{
    return [currentStr stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
}

-(BOOL)checkUserGivenDetails
{
    BOOL isAllOk = YES;
    
//    if([[self removeWhiteSpaceChar:txtFldLocation.text] length] < 1 )
//    {
//        return NO;
//
//    }
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([[self removeWhiteSpaceChar:txtFldName.text] length] < 2 )
    {
        
        [appDelegate onCreateToastMsgInWindow:@"Please enter First Name atleast 2 characters"];
        return NO;
    }

    else if ([[self removeWhiteSpaceChar:txtFldName.text] length] > MAXTEXTLENGTH)
    {
        [appDelegate onCreateToastMsgInWindow:@"Firstname should not exceed 20 characters"];
        return NO;
    }
    
    if([[self removeWhiteSpaceChar:txtFldLastName.text] length] < 2)
    {
    [appDelegate onCreateToastMsgInWindow:@"Please enter last Name atleast 2 characters"];
    return NO;
    }
    
    else if ([[self removeWhiteSpaceChar:txtFldLastName.text] length] > MAXTEXTLENGTH)
    {
        [appDelegate onCreateToastMsgInWindow:@"Lastname should not exceed 20 characters"];
        return NO;
    }
   else if([txtViewAchvngNow.text length] < 15)
    {
        [appDelegate onCreateToastMsgInWindow:@"Achievement Statement should contain a minimum of 15 characters"];
        return NO;
    }
    
   else  if([txtViewAchvngNow.text length] > 200 )
   {
       [appDelegate onCreateToastMsgInWindow:@"Achievement Statement should contain a maximum of 200 characters"];
       return NO;
   }
    
    
    if([txtFldWebsite.text length])
    {
        NSString *strUrl = txtFldWebsite.text;
        if (([strUrl rangeOfString:@"http://"].location == NSNotFound) && ([strUrl rangeOfString:@"https://"].location == NSNotFound)) {
            NSLog(@"string does not contain bla");
            strUrl = [NSString stringWithFormat:@"http://%@",strUrl];
            
        } else {
            NSLog(@"string contains bla!");
            
        }
        if (![self validateUrl:strUrl])
        {
            [appDelegate onCreateToastMsgInWindow:@"Url is not valid"];
            return NO;
        }
    }
       return isAllOk;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    if ([text isEqualToString: @""])
    {
        return YES;
    }
    if (textView.text.length>=200)
    {
        [textView resignFirstResponder];
        return NO;
    }
    if(newLength <= 200)
    {
        return YES;
    } else {
        
        NSUInteger emptySpace = 200 - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
    
    return YES;
}



#pragma mark - ******* Validate Website url *******
//
//- (BOOL) validateUrl: (NSString *) candidate {
//    NSString *urlRegEx =URLRegex;
//    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
//    return [urlTest evaluateWithObject:candidate];
//    
//    
//    down vote
//    accepted
//    Thanks to this post, you can avoid using RegexKit. Here is my solution (works for iphone development with iOS > 3.0) :
//    
    - (BOOL) validateUrl: (NSString *) candidate {
        NSString *urlRegEx = @"^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
        NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
        
        return [urlTest evaluateWithObject:candidate];
    }
    
    
//}
-(void)devicesize
{
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height < 568.0f)
        {
            [scrollViewEditprofile setContentSize:CGSizeMake(300, 400)];
        }
        else
        {
            [scrollViewEditprofile setContentSize:CGSizeMake(300,450)];
        }
    }
}

-(UIImage *)imageResize :(UIImage*)img andResizeToWidth:(CGFloat)newWidth{
    CGFloat newheight=(img.size.height/img.size.width)*newWidth;
    CGFloat scale = [[UIScreen mainScreen]scale];
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, newWidth), NO, scale);
    [img drawInRect:CGRectMake(0,0,newWidth,newWidth)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

         /*((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+*/

@end
