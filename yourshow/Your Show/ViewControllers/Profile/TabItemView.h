//
//  TabItemView.h
//  ProfilePageConntrol
//
//  Created by Siba Prasad Hota on 09/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//




#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TabItemView : NSObject

@property(nonatomic,retain)NSString *titleText;
@property(nonatomic,retain)NSString *subtitleText;
@property(nonatomic,retain)NSString *imageName;

@property(nonatomic,retain)NSString *selectedimageName;

@property(nonatomic,assign) BOOL shouldShowImage;

@end
