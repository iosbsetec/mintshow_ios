//
//  MSPUserNotFoundVC.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 06/06/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "MSPUserNotFoundVC.h"

@interface MSPUserNotFoundVC ()
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

@end

@implementation MSPUserNotFoundVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.userImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userImage.layer.borderWidth = 2.0;
    self.userImage.layer.cornerRadius = self.userImage.frame.size.width / 2;
    self.userImage.clipsToBounds = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
