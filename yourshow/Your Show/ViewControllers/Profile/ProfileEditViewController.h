//
//  ProfileEditViewController.h
//  YACC
//
//  Created by Pothiraj_BseTec on 01/08/15.
//  Copyright (c) 2015 BseTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profilemodel.h"

@protocol EditProfileVCDelegate
- (void)profileedited;
@end;

@interface ProfileEditViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>

@property (strong, nonatomic) Profilemodel *pmodeldata;
@end
