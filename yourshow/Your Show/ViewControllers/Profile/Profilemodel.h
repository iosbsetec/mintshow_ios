//
//  Profilemodel.h
//  Your Show
//
//  Created by bsetec on 1/5/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profilemodel : NSObject

@property (strong,nonatomic) NSString *achieving_text,*level,*location,*profile_followers_count,*profile_mint_count,*profile_circle_count;
@property (strong,nonatomic) NSString *profile_showroom_count,*website;
@property (strong,nonatomic) NSString *first_name,*last_name;

@property (strong,nonatomic) NSString* user_id,*user_name,*name,*user_thumb_image,*user_location,*follow_status	,*refer_status;
@property (strong,nonatomic) NSString *user_follow_status,*profile_type,*profile_url,*user_first_name,*user_last_name;
@property (strong,nonatomic) NSString* following_count,*follower_userid,*incircle_userid;
@property (strong,nonatomic) NSString* hifive_daily,*hifive_ninety,*hifive_all,*comment_daily,*comment_ninety,*comment_all;
@property (strong,nonatomic) NSString *nominate_daily,*nominate_ninety,*nominate_all,*remint_daily,*remint_ninety,*remint_all;
@property (strong,nonatomic) NSString* share_daily,*share_ninety,*share_all,*userImageUrl;

@property (strong,nonatomic) NSString *hifive_total,*comment_total,*remint_total,*share_total;
@property(nonatomic,strong)  NSString *status;
@property(nonatomic,strong)  NSString *status_Msg;
@property(nonatomic,strong)  NSString *status_code,*profile_showroom_created,*profile_showroom_joined;
@property(nonatomic,strong)  NSMutableArray *profileArray,*tempArray;
@property (strong,nonatomic) NSString *streak_days;


-(Profilemodel *)initWithProfileListDict:(NSDictionary*)ProfileList;
-(Profilemodel *)initWithProfileStatsDetailsListDict:(NSDictionary*)ProfileList
;


@end
