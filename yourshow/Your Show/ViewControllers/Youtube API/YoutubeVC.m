//
//  YTSearchController.m
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import "YoutubeVC.h"
#import "YTSearchTableCell.h"
#import "YoutubeGeneralModel.h"
#import "YoutubeModelClass.h"
#import  <MediaPlayer/MediaPlayer.h>
#import "XMLDictionary.h"

#define LOG_ERROR(x) NSLog(@"[%@ %@] - error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), x)

#define APIKEY @"AIzaSyBDhQB43pAT-Jz3MtjIq8spzKokwiILYQk"

const CGFloat YouTubeStandardPlayerWidth = 640;
const CGFloat YouTubeStandardPlayerHeight = 390;

@interface YoutubeVC ()<UIWebViewDelegate>
{
    NSMutableDictionary *returnObj;
    IBOutlet UIView *viewVideoBG;
    SearchListVC *viewSearch;
    
    NSDictionary *dataResponse;
    NSArray *checkdata;
    
    NSString *strVideoUrl;
    MPMoviePlayerController *movieController;
    UIImageView *searchIcon;
    NSMutableArray *arrVideoTimeLength;
    UIWebView *webVideoView;
    
    BOOL isFirsttimeLoading;
    NSString *strSearchKeyword;
}

-(IBAction)btnSearchTapped:(id)sender;
@end


@implementation YoutubeVC

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self embedYouTube:CGRectMake(5, 5, viewVideoBG.frame.size.width-10, viewVideoBG.frame.size.height-15)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    firstLoad=YES;
    isFirsttimeLoading = YES;

    arrVideoTimeLength = [[NSMutableArray alloc]init];
    
     youtubeData=[[NSMutableArray alloc] init];
    [self GetYoutubeSearchFeed:@"You are a ceo" IsNext:NO];

    strVideoUrl = @"GGrZhrP4D_o";
    
    returnObj = [[NSMutableDictionary alloc]init];
    
    [SearchField.layer setBorderWidth:1.0f];
    [SearchField.layer setBorderColor:[UIColor colorWithRed:187.0f/255.0f green:187.0f/255.0f blue:187.0f/255.0f alpha:1.0f].CGColor];
    spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 8)];
    [SearchField setLeftViewMode:UITextFieldViewModeAlways];
    [SearchField setLeftView:spacerView];
    [activityView setHidden:YES];
    
    SearchField.layer.cornerRadius = 4.0;
    requestCount=0;
    
    CGRect tableFrame = YTtable.frame;
    tableFrame.origin.y = viewVideoBG.frame.size.height+45;
    tableFrame.size.height = self.view.frame.size.height - viewVideoBG.frame.size.height-90;
    [YTtable setFrame:tableFrame];
    
    viewSearch = [[SearchListVC alloc]initWithFrame:CGRectMake(0, 39, self.view.frame.size.width, self.view.frame.size.height-39)];
    viewSearch.searchDelegate = self;
    [viewSearch.view setHidden:YES];
    [self.view addSubview:viewSearch.view];


    
}
/* [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/videos?id=%@&part=contentDetails&key=%@",[dict valueForKeyPath:@"id.videoId"],APIKEY]]] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
 
 if (!error)
 {
 NSDictionary*  videoInfo=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
 
 YoutubeModelClass *modelObject = [[YoutubeModelClass alloc]initWithInfo:dict];
 [modelObject setDuration:[[[[videoInfo valueForKey:@"items"] firstObject] valueForKey:@"contentDetails"] valueForKey:@"duration"]];
 [array addObject:modelObject];
 modelObject = nil;
 }
 else
 {
 NSLog(@"error data %@",error);
 }
 }];*/


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    float scrollViewHeight          = scrollView.frame.size.height;
    float scrollContentSizeHeight   = scrollView.contentSize.height;
    float scrollOffset              = scrollView.contentOffset.y;
    
    if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        if(isFirsttimeLoading)
        {
            [self GetYoutubeSearchFeed:strSearchKeyword IsNext:YES];
            NSLog(@"*** Scroll End ****");
            isFirsttimeLoading = NO;
        }
        
    }
}

-(void)GetYoutubeSearchFeed:(NSString *)searchtext IsNext:(BOOL)isNextPageloaded
{
    [SearchField resignFirstResponder];
    strSearchKeyword = searchtext;

    SearchField.text = (firstLoad)?@"":searchtext;
    searchtext = [searchtext stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    
    NSURL *url;
    if (isNextPageloaded) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=%@&part=snippet&q=%@&maxResults=50&order=%@&pageToken=%@",APIKEY,searchtext,@"date",@"NEXT"]];
    }
    else
    {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=%@&part=snippet&q=%@&maxResults=50",APIKEY,searchtext]];
    }
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (!error)
        {
            [self parseSearchListWithData:data];
        }
        else
        {
            NSLog(@"error data %@",error);
        }
    }];
}

-(void) GetYoutubeSearchFeed:(NSString *)searchtext
{
    isFirsttimeLoading = YES;
    [arrVideoTimeLength removeAllObjects];
    [youtubeData  removeAllObjects];
    [YTtable reloadData];
    [self GetYoutubeSearchFeed:searchtext IsNext:NO];
}


-(void)getVideoTimeLenght:(NSString *)youtubeId
{
    NSLog(@"%@ Count %lu",youtubeId,(unsigned long)[[youtubeId componentsSeparatedByString:@"%2C"] count]);
    
    NSString *strUrl = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/videos?id=%@&part=contentDetails&key=AIzaSyBDhQB43pAT-Jz3MtjIq8spzKokwiILYQk",youtubeId];
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:strUrl]] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (!error)
        {
            NSDictionary *timeResponse=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            [arrVideoTimeLength addObjectsFromArray:[timeResponse valueForKeyPath:@"items.contentDetails.duration"]];
            [self performSelectorOnMainThread:@selector(onRefreshTable) withObject:nil waitUntilDone:YES];
        }
        else
        {
            NSLog(@"error data %@",error);
        }
    }];
}

-(void)parseSearchListWithData:(NSData *)data
{
    NSError * error = nil;
    dataResponse=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    checkdata=[[NSArray alloc]initWithArray:[dataResponse objectForKey:@"items"]];
    NSMutableArray *array = [NSMutableArray array];
    YoutubeGeneralModel *obj = [YoutubeGeneralModel new];
    
    if([checkdata count]>0)
    {
        int i = 0;
        NSString *strYoutubeIds = @"";
        for (NSDictionary *dict in checkdata)
        {
            YoutubeModelClass *modelObject = [[YoutubeModelClass alloc]initWithInfo:dict];
            NSString *ids = modelObject.youtubeLink;
            ids = (ids.length>0)?ids:@"cbut2K6zvJY"; //  sample video id
            if(i==0) {
                strYoutubeIds = @"GGrZhrP4D_o";
            }
            else
                strYoutubeIds = [NSString stringWithFormat:@"%@%%2C%@",strYoutubeIds,ids];
            
            [array addObject:modelObject];
            modelObject = nil;
            i++;
        }
        [self getVideoTimeLenght:strYoutubeIds];

        obj.tempArray = array;
        array = nil;
        obj.status = @"true";
        obj.nextPageToken = [dataResponse objectForKey:@"pageInfo"];
        obj.pageInfo = [dataResponse objectForKey:@"nextPageToken"];
    }
    else
    {
        obj.status = @"false";
    }
    
    [self processedData:obj];
}

-(void) processedData:(YoutubeGeneralModel *)data
{
    if([data.status isEqualToString:@"true"])
    {
//        if(youtubeData.count == 0)
//            youtubeData=[[NSMutableArray alloc] initWithArray:data.tempArray];
//        else
//        {
            [youtubeData addObjectsFromArray:data.tempArray];
//        }
        
        YTtable.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        YoutubeModelClass *model = [youtubeData objectAtIndex:0];
        strVideoUrl = (firstLoad)?@"GGrZhrP4D_o":model.youtubeLink;
        
        firstLoad=NO;


    }
}

-(void)onRefreshTable
{
    [webVideoView loadHTMLString:[self htmlContent] baseURL:nil];
    [YTtable reloadData];
    
    if (youtubeData.count > 0 && isFirsttimeLoading)
    {
        [YTtable setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
        [YTtable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    
    [self performSelectorOnMainThread:@selector(onAutoRefreshTable) withObject:nil waitUntilDone:YES];
}

-(void)onAutoRefreshTable
{
    if (youtubeData.count > 0 && isFirsttimeLoading)
        [YTtable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void)embedYouTube:(CGRect)frame
{
    webVideoView = [[UIWebView alloc] initWithFrame:frame];
    webVideoView.backgroundColor = self.view.backgroundColor;
    webVideoView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    webVideoView.delegate = self;
    webVideoView.scrollView.scrollEnabled = NO;
    webVideoView.opaque = NO;
    webVideoView.scrollView.bounces = NO;
//    webVideoView.layer.borderColor = [UIColor redColor].CGColor;
//    webVideoView.layer.borderWidth = 2.0;
    [viewVideoBG addSubview:webVideoView];
}

-(IBAction)searchAction:(UITextField *)txtFld{

        if([[txtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]>0)
        {
            NSString *urlAsString = [NSString stringWithFormat:@"https://suggestqueries.google.com/complete/search?q=%@&client=toolbar",txtFld.text];
            NSURL *url = [[NSURL alloc] initWithString:urlAsString];
            
            [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                
                if (error)
                {
                    NSLog(@"error data %@",error);
                }
                else
                {
                    NSDictionary * dictc = nil;
                    dictc = [NSDictionary dictionaryWithXMLData:data];
                    
                    [viewSearch getKeyWords:dictc];
                }
            }];
            
            if([viewSearch.view isHidden])
                [viewSearch.view setHidden:NO];
        }
        else
        {
            [viewSearch.view setHidden:YES];
        }
    }



- (IBAction)cancelTapped:(UIButton *)sender
{
    
}

-(IBAction)cancelAction:(id)sender
{
    [SearchField resignFirstResponder];
    [webVideoView stringByEvaluatingJavaScriptFromString:@"document.open();document.close();"];

    webVideoView.delegate = nil;
    webVideoView = nil;
    [self.delegate didTappedCancelButton];
    [self.view removeFromSuperview];
//    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)chooseAction:(id)sender
{
    strVideoUrl = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",strVideoUrl];
    NSLog(@"%@",strVideoUrl);
    [webVideoView stringByEvaluatingJavaScriptFromString:@"document.open();document.close();"];
    
    [self.view removeFromSuperview];
    [self.delegate YoutubeVCDelegateMethod:[strVideoUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    webVideoView.delegate = nil;
    webVideoView = nil;
}

-(IBAction)btnSearchTapped:(id)sender
{
    if(SearchField.text.length>0)
    {
        [viewSearch.view setHidden:YES];
        [self GetYoutubeSearchFeed:SearchField.text];
    }
}

#pragma mark - ****** UITextField Delegate *******
- (void)textFieldDidBeginEditing:(UITextField *)textField {
        [textField.layer setBorderColor:[UIColor colorWithRed:187.0f/255.0f green:187.0f/255.0f blue:187.0f/255.0f alpha:1.0f].CGColor];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}


#pragma mark - ****** UITableView delegate *******
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [youtubeData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    {
        YoutubeModelClass *model = [youtubeData objectAtIndex:indexPath.row];
        static NSString *MyIdentifier1 = @"MyCell";
        YTSearchTableCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier1];
        if (cell == nil) {
            [tableView registerNib:[UINib nibWithNibName:@"YTSearchTableCell" bundle:nil] forCellReuseIdentifier:MyIdentifier1];
            cell=[tableView dequeueReusableCellWithIdentifier:MyIdentifier1];

        }
        cell.selectionStyle= UITableViewCellSeparatorStyleNone;
        if(indexPath.row<arrVideoTimeLength.count)        {
        NSString *strtime = [self parseDuration:[arrVideoTimeLength objectAtIndex:indexPath.row]];
        cell.ytDuration.text = strtime;
        [cell setCellData:model];
        }
        return cell;
    }
}

- (NSString *)parseDuration:(NSString *)duration
{
    NSInteger hours = 0;
    NSInteger minutes = 0;
    NSInteger seconds = 0;
    
    NSRange timeRange = [duration rangeOfString:@"T"];
    duration = [duration substringFromIndex:timeRange.location];
    
    while (duration.length > 1) {
        duration = [duration substringFromIndex:1];
        
        NSScanner *scanner = [NSScanner.alloc initWithString:duration];
        NSString *part = [NSString.alloc init];
        [scanner scanCharactersFromSet:[NSCharacterSet decimalDigitCharacterSet] intoString:&part];
        
        NSRange partRange = [duration rangeOfString:part];
        
        duration = [duration substringFromIndex:partRange.location + partRange.length];
        
        NSString *timeUnit = [duration substringToIndex:1];
        if ([timeUnit isEqualToString:@"H"])
            hours = [part integerValue];
        else if ([timeUnit isEqualToString:@"M"])
            minutes = [part integerValue];
        else if ([timeUnit isEqualToString:@"S"])
            seconds = [part integerValue];
    }
    NSString *strTime = (hours>0)?[NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds]:[NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
    return strTime;
}

/*
-(NSString *)getOriginalTime:(NSString *)strTime
{
    
//    PT2H49S,
//    PT2H50M20S
    
    strTime = [strTime stringByReplacingOccurrencesOfString:@"PT" withString:@""];
//    strTime = [strTime stringByReplacingOccurrencesOfString:@"M" withString:@":"];
//    strTime = [strTime stringByReplacingOccurrencesOfString:@"S" withString:@""];
//    strTime = [strTime stringByReplacingOccurrencesOfString:@"H" withString:@":"];
    
    NSString *orginalTime = @"";
    NSString *strGetHour = @"";
    NSString *strGetMins = @"";
    NSString *strGetSecs = @"";

    
    if (([strTime rangeOfString:@"H"].location != NSNotFound )&& ([strTime rangeOfString:@"M"].location != NSNotFound) && ([strTime rangeOfString:@"S"].location != NSNotFound))
    {
        NSArray *arrHours = [strTime componentsSeparatedByString:@"H"];

        strGetHour = [arrHours objectAtIndex:0];
        strGetHour = (strGetHour.length>1)?strGetHour:[NSString stringWithFormat:@"0%@",strGetHour];

        NSArray *arrMins = [[arrHours objectAtIndex:1] componentsSeparatedByString:@"M"];
        strGetMins = [arrMins objectAtIndex:0];
        strGetMins = (strGetMins.length>1)?strGetMins:[NSString stringWithFormat:@"0%@",strGetMins];

        strGetSecs = [[arrMins objectAtIndex:1] stringByReplacingOccurrencesOfString:@"S" withString:@""];
        strGetSecs = (strGetSecs.length>1)?strGetSecs:[NSString stringWithFormat:@"0%@",strGetSecs];
        
        orginalTime = [NSString stringWithFormat:@"%@:%@:%@",strGetHour,strGetMins,strGetSecs];
    }
    else if(([strTime rangeOfString:@"H"].location != NSNotFound )&&([strTime rangeOfString:@"S"].location != NSNotFound))
    {
        NSArray *arrHours = [strTime componentsSeparatedByString:@"H"];
        strGetHour = [arrHours objectAtIndex:0];
        strGetHour = (strGetHour.length>1)?strGetHour:[NSString stringWithFormat:@"0%@",strGetHour];
        
        strGetSecs = [[arrHours objectAtIndex:1] stringByReplacingOccurrencesOfString:@"S" withString:@""];
        strGetSecs = (strGetSecs.length>1)?strGetSecs:[NSString stringWithFormat:@"0%@",strGetSecs];

        orginalTime = [NSString stringWithFormat:@"%@:00:%@",strGetHour,strGetSecs];
    }
    else if(([strTime rangeOfString:@"M"].location != NSNotFound) && ([strTime rangeOfString:@"S"].location != NSNotFound))
    {
        NSArray *arrMins = [strTime componentsSeparatedByString:@"M"];

        strGetMins = [[strTime componentsSeparatedByString:@"M"] objectAtIndex:0];
        strGetMins = (strGetMins.length>1)?strGetMins:[NSString stringWithFormat:@"0%@",strGetMins];
        
        strGetSecs = [[arrHours objectAtIndex:1] stringByReplacingOccurrencesOfString:@"S" withString:@""];
        strGetSecs = (strGetSecs.length>1)?strGetSecs:[NSString stringWithFormat:@"0%@",strGetSecs];
        
        orginalTime = [NSString stringWithFormat:@"%@:%@",strGetMins,strGetSecs];
    }
   
      orginalTime = [orginalTime stringByReplacingOccurrencesOfString:@"M" withString:@""];
      orginalTime = [orginalTime stringByReplacingOccurrencesOfString:@"S" withString:@""];
      orginalTime = [orginalTime stringByReplacingOccurrencesOfString:@"H" withString:@""];
    
//    strTime = [strTime stringByReplacingOccurrencesOfString:@"M" withString:@":"];
//    strTime = [strTime stringByReplacingOccurrencesOfString:@"S" withString:@""];
//    strTime = [strTime stringByReplacingOccurrencesOfString:@"H" withString:@":"];
//
//    NSError *error = NULL;
//    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@":" options:NSRegularExpressionCaseInsensitive error:&error];
//    NSUInteger numberOfMatches = [regex numberOfMatchesInString:strTime options:0 range:NSMakeRange(0, [strTime length])];
//    NSLog(@"Found %lu",(unsigned long)numberOfMatches);
//    
//    if (numberOfMatches>1)
//    {
//        NSString *strGetMins = [[strTime componentsSeparatedByString:@":"] objectAtIndex:0];
//        NSString *strGetSecs = [[strTime componentsSeparatedByString:@":"] objectAtIndex:1];
//
//        strTime = (strTime.length>1)?[NSString stringWithFormat:@"00:%@",strTime]:[NSString stringWithFormat:@"00:0%@",strTime];
//    }
//    else
//    {
//        if ([strTime rangeOfString:@":"].location == NSNotFound)
//        {
//            strTime = (strTime.length>1)?[NSString stringWithFormat:@"00:%@",strTime]:[NSString stringWithFormat:@"00:0%@",strTime];
//        }
//        else
//        {
//            NSString *strGetMins = [[strTime componentsSeparatedByString:@":"] objectAtIndex:0];
//            NSString *strGetSecs = [[strTime componentsSeparatedByString:@":"] objectAtIndex:1];
//            strTime = [NSString stringWithFormat:@"%@:%@",([strGetMins length]>1)?strGetMins:[NSString stringWithFormat:@"0%@",strGetMins],([strGetSecs length]>1)?strGetSecs:[NSString stringWithFormat:@"0%@",strGetSecs]];
//        }
//    }
//    NSLog(@"Time == %@",strTime);

    return strTime;
}
*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 91;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [webVideoView stringByEvaluatingJavaScriptFromString:@"document.open();document.close();"];
    strVideoUrl = [[youtubeData objectAtIndex:indexPath.row] youtubeLink];
    [self embedYouTube:CGRectMake(5, 5, viewVideoBG.frame.size.width-10, viewVideoBG.frame.size.height-15)];
    [webVideoView loadHTMLString:[self htmlContent] baseURL:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [webVideoView loadHTMLString:@"" baseURL:nil];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self updatePlayerSize];
}

#pragma mark - Actions

- (void)play
{
    [webVideoView stringByEvaluatingJavaScriptFromString:@"player.playVideo();"];
}

- (void)pause
{
    [webVideoView stringByEvaluatingJavaScriptFromString:@"player.pauseVideo();"];
}

- (void)stop
{
    [webVideoView stringByEvaluatingJavaScriptFromString:@"player.stopVideo();"];
}

#pragma mark - Accessors

- (void)setPlayerSize:(CGSize)playerSize
{
    [webVideoView stringByEvaluatingJavaScriptFromString:
     [NSString stringWithFormat:@"player.setSize(%u, %u);",
      (unsigned int) playerSize.width, (unsigned int) playerSize.height]];
}

- (NSTimeInterval)duration
{
    return [[webVideoView stringByEvaluatingJavaScriptFromString:@"player.getDuration();"] doubleValue];
}

- (NSTimeInterval)currentTime
{
    return [[webVideoView stringByEvaluatingJavaScriptFromString:@"player.getCurrentTime();"] doubleValue];
}

#pragma mark - Helpers

- (NSString *)htmlContent
{
    NSString *pathToHTML = [[NSBundle mainBundle] pathForResource:@"PBYouTubeVideoView" ofType:@"html"];
    NSAssert(pathToHTML != nil, @"could not find PBYouTubeVideoView.html");
    
    NSError *error = nil;
    NSString *template = [NSString stringWithContentsOfFile:pathToHTML encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        LOG_ERROR(error);
    }
    CGSize playerSize = [self playerSize];
    NSString *result = [NSString stringWithFormat:template, [NSString stringWithFormat:@"%.0f", playerSize.width], [NSString stringWithFormat:@"%.0f", playerSize.height],                         strVideoUrl];
    return result;
}

- (void)updatePlayerSize
{
    CGSize playerSize = [self playerSize];
    [self setPlayerSize:playerSize];
}

- (CGSize)playerSize
{
    float heightRatio = self.view.bounds.size.height / YouTubeStandardPlayerHeight;
    float widthRatio = self.view.bounds.size.width / YouTubeStandardPlayerWidth;
    float ratio = MIN(widthRatio, heightRatio);
    
    CGSize playerSize = CGSizeMake(YouTubeStandardPlayerWidth * ratio, YouTubeStandardPlayerHeight * ratio);
    return playerSize;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[request URL].scheme isEqualToString:@"ytplayer"]) {
        NSArray *components = [[request URL] pathComponents];
        if ([components count] > 1) {
            NSString *actionData = nil;
            if ([components count] > 2) {
                actionData = components[2];
            }
        }
        return NO;
    } else {
        return YES;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    LOG_ERROR(error);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
