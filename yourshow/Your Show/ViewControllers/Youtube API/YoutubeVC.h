//
//  YTSearchController.h
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchListVC.h"

@protocol YoutubeVCDelegate
- (void) YoutubeVCDelegateMethod:(NSString *) seectedStrings;
- (void)didTappedCancelButton;

@end

@interface YoutubeVC : UIViewController <UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,SearchListDelegate>
{
    IBOutlet UITextField *SearchField;
    IBOutlet UIButton *SearchBtn;
    IBOutlet UIButton *CancelBtn;
    IBOutlet UIButton *ChooseBtn;
    IBOutlet UITableView *YTtable;
    IBOutlet UIView *topBar;
    IBOutlet UIView *bottomBar;
    IBOutlet UIActivityIndicatorView *activityView;
    
    NSMutableArray *youtubeData;
    UIView *spacerView;
    BOOL firstLoad;
    NSDictionary *responseData,*responseItem ,*responseSnippet, *responseThumb, *responseDefaultImg;
    NSMutableDictionary *receivedData;
    UIImage *image;
    int requestCount;
}

@property (assign) id <YoutubeVCDelegate> delegate;

@end
