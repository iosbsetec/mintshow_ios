//
//  YTSearchTableCell.m
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import "YTSearchTableCell.h"

@implementation YTSearchTableCell

- (void)awakeFromNib {
    // Initialization code
}


-(void)setCellData : (YoutubeModelClass *)modelData {
    self.YtTitle.text = modelData.titleText;
    self.YtDesc.text = modelData.descriptionText;
    //self.ytDuration.text = modelData.duration;
    self.YtImage.mintImageURL = [NSURL URLWithString:modelData.smallPhoto];
}
@end
