//
//  SearchListVC.h
//  Youtube
//
//  Created by BseTec on 12/23/15.
//  Copyright © 2015 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchListDelegate <NSObject>

-(void) GetYoutubeSearchFeed:(NSString *)searchtext;

@end

@interface SearchListVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

//-(SearchListVC *)initWithFrame:(CGRect)rect;

@property(nonatomic,retain)id <SearchListDelegate>searchDelegate;

- (id)initWithFrame:(CGRect)frame;
-(void)getKeyWords:(NSDictionary *)dictWords;
@end
