//
//  YoutubeModelClass.m
//  Youtube
//
//  Created by JITENDRA KUMAR PRADHAN on 12/12/15.
//  Copyright © 2015 BSEtec. All rights reserved.
//

#import "YoutubeModelClass.h"

@implementation YoutubeModelClass



-(id)initWithInfo : (NSDictionary *) info{
    
    self.titleText             =    [info valueForKeyPath:@"snippet.title"];
    self.descriptionText       =    [info valueForKeyPath:@"snippet.description"];
    self.smallPhoto            =    [info valueForKeyPath:@"snippet.thumbnails.high.url"];
    self.largePhoto            =    [info valueForKeyPath:@"snippet.thumbnails.medium.url"];
    self.youtubeLink           =    [info valueForKeyPath:@"id.videoId"];
    
    
//    NSLog(@"title =%@ \n description= %@\n largephot= %@\n smallPhoto= %@\n youtubeLink= %@",_titleText,_descriptionText,_largePhoto,_smallPhoto,_youtubeLink);
    
    return self;
}


@end
