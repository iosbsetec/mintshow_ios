//
//  GeneralModel.h
//  Youtube
//
//  Created by JITENDRA KUMAR PRADHAN on 12/12/15.
//  Copyright © 2015 BSEtec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YoutubeGeneralModel : NSObject
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *pageInfo;
@property (nonatomic,strong) NSString *nextPageToken;
@property (nonatomic,strong) NSMutableArray *tempArray;

@end
