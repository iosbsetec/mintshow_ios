//
//  YTSearchTableCell.h
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YoutubeModelClass.h"

@interface YTSearchTableCell : UITableViewCell

@property(nonatomic,retain) IBOutlet UILabel *YtTitle;
@property(nonatomic,retain) IBOutlet UILabel *YtDesc;
@property(nonatomic,retain) IBOutlet UILabel *YtPublished;
@property(nonatomic,retain) IBOutlet UIImageView *YtImage;
@property(nonatomic,retain) IBOutlet UILabel *ytDuration;

-(void)setCellData : (YoutubeModelClass *)modelData;
@end
