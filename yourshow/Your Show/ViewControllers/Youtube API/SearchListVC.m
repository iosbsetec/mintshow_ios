//
//  SearchListVC.m
//  Youtube
//
//  Created by BseTec on 12/23/15.
//  Copyright © 2015 BSEtec. All rights reserved.
//

#import "SearchListVC.h"

@interface SearchListVC ()
{
    IBOutlet UITableView *tableKeyWords;
    IBOutlet UIView *viewKeywords;
    NSMutableDictionary *dictKeyWords;
}
@end

@implementation SearchListVC

@synthesize searchDelegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    tableKeyWords.layer.borderWidth = 1.0;
    tableKeyWords.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.view bringSubviewToFront:tableKeyWords];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTappedView)];
    [viewKeywords addGestureRecognizer:gesture];
}

-(void)onTappedView
{
    [self.view setHidden:YES];
}

-(void)getKeyWords:(NSDictionary *)dictWords
{
    if ([[dictWords valueForKey:@"CompleteSuggestion"] count]>0)
    {
        [self.view setHidden:NO];
        dictKeyWords = [[NSMutableDictionary alloc]initWithDictionary:dictWords];
        
        [self performSelectorOnMainThread:@selector(onReloadTable) withObject:nil waitUntilDone:YES];
    }
}

-(void)onReloadTable
{
    [tableKeyWords reloadData];
}
-(SearchListVC *)initWithFrame:(CGRect)rect
{
    [self.view setFrame:rect];
    return self;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dictKeyWords valueForKey:@"CompleteSuggestion"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [[[[dictKeyWords valueForKey:@"CompleteSuggestion"] objectAtIndex:indexPath.row] valueForKey:@"suggestion"] valueForKey:@"_data"];
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict1 = [[dictKeyWords valueForKey:@"CompleteSuggestion"] objectAtIndex:indexPath.row];
    NSString *val = [dict1 valueForKeyPath:@"suggestion._data"];

    if([searchDelegate respondsToSelector:@selector(GetYoutubeSearchFeed:)])
        [searchDelegate GetYoutubeSearchFeed:val];
    
    [self.view setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
