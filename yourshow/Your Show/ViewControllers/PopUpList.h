//
//  ListViewCell.h
//  SampleDemo
//
//  Created by bsetec on 11/30/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PopUpList;
@protocol PopUpListDelegate <NSObject>

-(void)setDataFromPopUpList:(NSString *)strData  type:(NSString *)strType;

@end

@interface PopUpList : UIView<UITableViewDataSource,UITableViewDelegate>
{
    
}

@property(nonatomic,retain)id<PopUpListDelegate>delegate;
- (id)setDatasToTableView:(NSArray *)arrDatas selectedType:(NSString *)strType frame:(CGRect)frame lastSelectdInfo:(NSInteger)selectedInfo;

@end
