//
//  JDViewController.m
//  JSPintDemo
//
//  Created by Jay Slupesky on 10/7/12.
//
typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;


#import "JDViewController.h"
#import "PintCollecionCell.h"
#import "YSAPICalls.h"
#import "Keys.h"
#import "Feedmodel.h"
#import "AsyncImageView.h"



@interface JDViewController ()
{
    NSMutableArray *allShowRoomdata;

    NSString *pageNumberVal;

}
@property (nonatomic, assign) NSInteger lastOffset;
@property (nonatomic, assign) CGFloat lastContentOffset;

@property (strong,nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong,nonatomic) NSMutableArray* photoList;
@end

#define kCollectionCellBorderTop 2.0
#define kCollectionCellBorderBottom 2.0
#define kCollectionCellBorderLeft 3.0
#define kCollectionCellBorderRight 3.0


@implementation JDViewController



#pragma mark - UIViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    allShowRoomdata = [[NSMutableArray alloc]init];

    // set up delegates
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

    [self.collectionView registerClass:[PintCollecionCell class] forCellWithReuseIdentifier:@"PintReuse"];
    // set inter-item spacing in the layout
    PintCollectionViewLayout* customLayout = (PintCollectionViewLayout*)self.collectionView.collectionViewLayout;
    customLayout.interitemSpacing = 4.0;

    // make up some test data
    self.photoList = [NSMutableArray arrayWithCapacity:1];
    [self.photoList addObject:@"danielle.jpg"];
    [self.photoList addObject:@"nature4.jpeg"];
    [self.photoList addObject:@"egret.png"];
    [self.photoList addObject:@"betceemay.jpg"];
    [self.photoList addObject:@"baby.jpg"];
    [self.photoList addObject:@"sc4.jpg"];
    [self.photoList addObject:@"bodegahead.png"];
    [self.photoList addObject:@"egret.png"];
    [self.photoList addObject:@"betceemay.jpg"];
    [self.photoList addObject:@"baby.jpg"];
    [self.photoList addObject:@"sc3.jpeg"];
    [self.photoList addObject:@"bodegahead.png"];
    [self.photoList addObject:@"egret.png"];
    [self.photoList addObject:@"betceemay.jpg"];
    [self.photoList addObject:@"sc2.jpeg"];
    
//    [self getHashTag];
}
-(void)viewDidAppear:(BOOL)animated
{

    [super viewDidAppear:animated];
    [self getMintsMyshow:@"1"];


}
-(void)getHashTag
{


    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%@",GETVALUE(CEO_AccessToken),@"j",@"1"] dataUsingEncoding:NSUTF8StringEncoding];




    [YSAPICalls HashTag:submitData  ForSuccessionBlock:^(id newResponse) {
        
               Generalmodel *gmodel = newResponse;
        NSLog(@"response = %@",gmodel.status);
        
        if ([gmodel.status_code integerValue])
        {
            
            
        }
        else
        {        }
        
        
    }
        andFailureBlock:^(NSError *error)
     {
     }];


}
-(void)getMintsMyshow : (NSString *)pageNumber
{
    NSDictionary *dicts = [NSDictionary dictionaryWithObjectsAndKeys:GETVALUE(CEO_AccessToken),@"access_token",@"0",@"mint_list_type",pageNumber,@"page_number", nil];
    
    [YSAPICalls getMintsForMyShowTabwithDict:dicts SuccessionBlock:^(id newResponse)
     {
         Generalmodel *gmodel = newResponse;
         
         if ([gmodel.status_code integerValue])
         {
                 [allShowRoomdata addObjectsFromArray: gmodel.tempArray];

       
                            BOOL animationsEnabled = [UIView areAnimationsEnabled];

             [UIView setAnimationsEnabled:NO];

             [self.collectionView performBatchUpdates:^{
                                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
             } completion:^(BOOL finished) {
                 if (finished) {
                     [UIView setAnimationsEnabled:animationsEnabled];

                 }

             
             }];
             
//            dispatch_async(dispatch_get_main_queue(), ^ {
//                //[self.collectionView reloadData];
//                // Reload the respective collection view row using the main thread.
//                
//                //[self.collectionView reloadItemsAtIndexPaths:[self.collectionView
//                BOOL animationsEnabled = [UIView areAnimationsEnabled];
//                [UIView setAnimationsEnabled:NO];
////                [myCollectionView reloadItemsAtIndexPaths:myIndexPaths];
//                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
//
//                [UIView setAnimationsEnabled:animationsEnabled];
//                // indexPathsForVisibleItems]];
//                
//                
//                
//                [self.collectionView performBatchUpdates:^{
//                    [self.collectionView reloadData];
//                } completion:^(BOOL finished) {}];
//                
//                
//
//             });
         }
         
         else
         {
             
         }
     } andFailureBlock:^(NSError *error) {
         
         
         
         
     }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Target Actions



- (IBAction)onAddCell:(id)sender
{
    [self.photoList addObject:@"egret.png"];

    NSUInteger newNumCells = [self.photoList count];
    NSIndexPath* newIndexPath = [NSIndexPath indexPathForItem:newNumCells - 1
                                                    inSection:0];
    [self.collectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]];
    
    [self.collectionView scrollToItemAtIndexPath:newIndexPath
                                atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                        animated:YES];
}



#pragma mark - UICollectionViewDelegateJSPintLayout




- (CGFloat)columnWidthForCollectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout
{
    if ([UIScreen mainScreen].bounds.size.height > 568) {
        return 185.0;

    }
    else
    return 155.0;
}

- (NSUInteger)maximumNumberOfColumnsForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
{
    NSUInteger numColumns = 2;

    return numColumns;
}

- (CGFloat)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout heightForItemAtIndexPath:(NSIndexPath*)indexPath
{
    NSUInteger index = [indexPath indexAtPosition:1];
    Feedmodel *feedData  = [allShowRoomdata objectAtIndex:index];

    CGFloat widthCell = 0.0;
    if ([UIScreen mainScreen].bounds.size.height > 568) {
        widthCell =  185.0;
        
    }
    else
        widthCell =  155.0;
    

    
//    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:self.photoList[index]]];
    CGSize rctSizeOriginal = CGSizeMake(feedData.width , feedData.height);
    double scale = (widthCell  - (kCollectionCellBorderLeft + kCollectionCellBorderRight)) / rctSizeOriginal.width;
    CGSize rctSizeFinal = CGSizeMake(rctSizeOriginal.width * scale,rctSizeOriginal.height * scale);
    //imageView.frame = CGRectMake(kCollectionCellBorderLeft,kCollectionCellBorderTop,rctSizeFinal.width,rctSizeFinal.height);

    CGFloat height = rctSizeFinal.height + 130;
    
    return height;
}

- (BOOL)collectionView:(UICollectionView*)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath*)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView*)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender
{
    return([NSStringFromSelector(action) isEqualToString:@"cut:"]);
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender
{
    if([NSStringFromSelector(action) isEqualToString:@"cut:"])
    {
        NSUInteger index = [indexPath indexAtPosition:1];
        
        [self.photoList removeObjectAtIndex:index];
        
        [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
    }
}

#pragma mark = UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [allShowRoomdata count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView
{
    return 1;
}

- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath
{
    //UICollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PintReuse" forIndexPath:indexPath];
     PintCollecionCell* cell = (PintCollecionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"PintReuse" forIndexPath:indexPath];
    cell.layer.cornerRadius  = 4.0f;
    [cell setClipsToBounds:YES];
    CGRect rectReference = cell.bounds;
    
    PNCollectionCellBackgroundView* backgroundView = [[PNCollectionCellBackgroundView alloc] initWithFrame:rectReference];
    cell.backgroundView = backgroundView;
    
    UIView* selectedBackgroundView = [[UIView alloc] initWithFrame:rectReference];
    selectedBackgroundView.backgroundColor = [UIColor clearColor];   // no indication of selection
    cell.selectedBackgroundView = selectedBackgroundView;
    
    // remove subviews from previous usage of this cell
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
//    NSUInteger index = [indexPath indexAtPosition:1];
//    
//    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:self.photoList[index]]];
//    CGSize rctSizeOriginal = imageView.bounds.size;
//    double scale = (cell.bounds.size.width  - (kCollectionCellBorderLeft + kCollectionCellBorderRight)) / rctSizeOriginal.width;
//    CGSize rctSizeFinal = CGSizeMake(rctSizeOriginal.width * scale,rctSizeOriginal.height * scale);
//    imageView.frame = CGRectMake(kCollectionCellBorderLeft,kCollectionCellBorderTop,rctSizeFinal.width,rctSizeFinal.height);
    
    
    
    NSUInteger index = [indexPath indexAtPosition:1];
    Feedmodel *feedData  = [allShowRoomdata objectAtIndex:index];
    
    CGFloat widthCell = 0.0;
    if ([UIScreen mainScreen].bounds.size.height > 568) {
        widthCell =  185.0;
        
    }
    else
        widthCell =  155.0;
    
    
    
    UIImageView* imageView = [[UIImageView alloc] init];
    CGSize rctSizeOriginal = CGSizeMake(feedData.width , feedData.height);
    double scale = (widthCell  - (kCollectionCellBorderLeft + kCollectionCellBorderRight)) / rctSizeOriginal.width;
    CGSize rctSizeFinal = CGSizeMake(rctSizeOriginal.width * scale,rctSizeOriginal.height * scale);
    imageView.frame = CGRectMake(kCollectionCellBorderLeft,kCollectionCellBorderTop,rctSizeFinal.width,rctSizeFinal.height);
    imageView.imageURL = [NSURL URLWithString:feedData.feed_thumb_image_small];

    [cell.contentView addSubview:imageView];

    
    
    
    CGRect rctLabel = CGRectMake(kCollectionCellBorderLeft,rctSizeFinal.height,rctSizeFinal.width,65);
    UILabel* label = [[UILabel alloc] initWithFrame:rctLabel];
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:12];


    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithString:@"Killed it today! Feeling super excited about my new #workout routine."];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorWithRed:243.0/255 green:94.0/255  blue:27.0/255  alpha:1.0]
                 range:NSMakeRange(text.length - 17, 16)];
    [label setAttributedText: text];
    [cell.contentView addSubview:label];
    

    
    
 
    
    
    
    UIView *userView                =   [[UIView alloc] initWithFrame: CGRectMake ( 2,rctSizeFinal.height+60, 148, 32)];
    UIImageView *userImage          =   [[UIImageView alloc] initWithFrame:CGRectMake(3,2,26,26)];
    userImage.layer.cornerRadius = 13.0f;
    [userImage setClipsToBounds:YES];
    userImage.imageURL = [NSURL URLWithString:feedData.user_thumb_image];
    
    CGRect rctLabelUserName         =   CGRectMake(32,1,100,30);
    UILabel* lableUserName          =   [[UILabel alloc] initWithFrame:rctLabelUserName];
    lableUserName.textColor         =   [UIColor grayColor];
    lableUserName.numberOfLines     =   2;
    lableUserName.font              =   [UIFont systemFontOfSize:10];
    //lableUserName.text              = [NSString stringWithFormat:@"%@ posted in *Sucsses 3h ago",feedData.user_name];
    NSMutableAttributedString *textUsername =
    [[NSMutableAttributedString alloc]
     initWithString:[NSString stringWithFormat:@"%@ posted %@",feedData.user_name,feedData.feed_posted_time]];
    
    
    //                 value:[UIColor colorWithRed:243.0/255 green:94.0/255  blue:27.0/255  alpha:1.0]

    [textUsername addAttribute:NSForegroundColorAttributeName
                 value:[UIColor blackColor]
                 range:NSMakeRange(0, feedData.user_name.length)];
    [lableUserName setAttributedText: textUsername];
    [cell.contentView addSubview:label];
//    CGRect rctLabelTime         =   CGRectMake(32,20,95,15);
//    UILabel* lableMintTime          =   [[UILabel alloc] initWithFrame:rctLabelTime];
//    lableMintTime.textColor         =   [UIColor blackColor];
//    lableMintTime.numberOfLines     =   0;
//    lableMintTime.font              =   [UIFont systemFontOfSize:10];
//    lableMintTime.text              =   @"july 28";
    
    [userView addSubview:lableUserName];
  //  [userView addSubview:lableMintTime];
    
    [userView addSubview:userImage];
    [cell.contentView addSubview:userView];
    [cell.contentView addSubview:[self createButtonView:rctSizeFinal index:indexPath.row withDta:feedData]];
    return cell;

}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //Compare sender.contentOffset.y with lastOffset
    
    ScrollDirection scrollDirection;
    if (self.lastContentOffset > scrollView.contentOffset.y)
    {
        scrollDirection = ScrollDirectionDown;
       // showAlert(@"Waooooo!!!", @"down", @"OKAY", nil);

        
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        scrollDirection = ScrollDirectionUp;
//        showAlert(@"Waooooo!!!", @"up", @"OKAY", nil);

        
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
        //NSLog(@"y value is %f",self.lastContentOffset);
    

    float height = self.collectionView.frame.size.height;
    //NSLog(@"height value is %f",height);

    if (_lastContentOffset >= height) {
        pageNumberVal = [NSString stringWithFormat:@"%d",[pageNumberVal integerValue]+1];
        [self getMintsMyshow:pageNumberVal];
    }

//    
//    
//    NSInteger section = [_collectionView numberOfSections] - 1 ;
//    NSInteger item = [_collectionView numberOfItemsInSection:section] - 1 ;
//    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:item inSection:section] ;
//    
//    NSLog(@"cell no is %d",lastIndexPath.row);
//    if (lastIndexPath.row == [allShowRoomdata count]) {
//        
//        
//        showAlert(@"Waooooo!!!", @"last cell", @"OKAY", nil);
//    }
}
-(UIView *)createButtonView : (CGSize)rctSizeFinal index :(NSInteger)index withDta : (Feedmodel *)modelData
{
    
    UIView *polygonView = [[UIView alloc] initWithFrame: CGRectMake ( 2,rctSizeFinal.height+65+32, 148, 20)];
    UIButton *btnTwo    = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnTwo.frame        = CGRectMake(32,0,40,25);
    btnTwo.tag          = index;
    // [btnTwo addTarget:self action:@selector(commentScreen:) forControlEvents:UIControlEventTouchUpInside];
    
   // UIImage *btnImage       =   [UIImage imageNamed:@"complimint.png"];
    //[btnTwo setImage:btnImage forState:UIControlStateNormal];
    UIImageView *inspire    =   [[UIImageView alloc] initWithFrame:CGRectMake(4,1,18,16)];
    inspire.image           =   [UIImage imageNamed:@"hive_it_normal"];
    UIImageView *highFive   =   [[UIImageView alloc] initWithFrame:CGRectMake(45,2,18,16)];
    highFive.image          =   [UIImage imageNamed:@"complimint"];
    UIImageView *plan       =   [[UIImageView alloc] initWithFrame:CGRectMake(85,2,30,15)];
    plan.image              =   [UIImage imageNamed:@"Icon_ReMint"];
    
    [polygonView addSubview:plan];
    [polygonView addSubview:inspire];
    [polygonView addSubview:highFive];
    
    
    CGRect rctLabel1               = CGRectMake(26,2,18,14);
    UILabel* labelinspire          = [[UILabel alloc] initWithFrame:rctLabel1];
    labelinspire.textColor         = [UIColor grayColor];
    labelinspire.numberOfLines     = 0;
    labelinspire.font              = [UIFont systemFontOfSize:10];
    labelinspire.text              = [modelData feed_inspiration_number];
    
    
    CGRect rctLabel2                = CGRectMake(67,2,18,14);
    UILabel* labelhighFive          = [[UILabel alloc] initWithFrame:rctLabel2];
    labelhighFive.textColor         = [UIColor grayColor];
    labelhighFive.numberOfLines     = 0;
    labelhighFive.font              = [UIFont systemFontOfSize:10];
    labelhighFive.text              = [modelData feed_comment_count];
    
    
    
    CGRect rctLabel3            = CGRectMake(120,2,18,15);
    UILabel* labelplan          = [[UILabel alloc] initWithFrame:rctLabel3];
    labelplan.textColor         = [UIColor grayColor];
    labelplan.numberOfLines     = 0;
    labelplan.font              = [UIFont systemFontOfSize:10];
    labelplan.text              = [modelData feed_like_count];
    
    [polygonView addSubview:labelinspire];
    [polygonView addSubview:labelhighFive];
    [polygonView addSubview:labelplan];
    [polygonView addSubview:btnTwo];
    return polygonView;
}

@end
