//
//  PhotoGallery.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 13/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

// define the protocol for the delegate
@protocol PhotoGalleryDelegate <NSObject>

// define protocol functions that can be used in any class using this delegate
-(void)selectedImageFromGallery:(UIImage *)selectedImage;
-(void)cancelPhotoGallery;
-(void)cameraImageTappedforPhotoGallery;

@end


@interface PhotoGallery : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

// define delegate property
@property (nonatomic, assign) id <PhotoGalleryDelegate>  delegate;
- (IBAction)hideView:(UIButton *)sender;
@property(strong ,atomic)NSString *isPhoto;
@property(assign ,nonatomic)BOOL isCameFromAddShowRoom;
@end
