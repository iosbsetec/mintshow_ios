//
//  ImageListViewController.m
//  Awesome Media Gallery
//
//  Created by Siba Prasad Hota on 30/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import "ImageListViewController.h"
#import "PhotoCell.h"
#import "GalleryOptionView.h"
#import "UICollectionView+Convenience.h"



@import Photos;

static CGSize AssetGridThumbnailSize;
@interface ImageListViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,OptionViewDelegate,OptionViewDataSource,PHPhotoLibraryChangeObserver>{
    BOOL isDrop;
    
    IBOutlet UILabel *lblHeaderInfo;
    
}

@property (weak, nonatomic) IBOutlet UIButton *dropDownBtn;

@property (nonatomic,strong) GalleryOptionView *aOptionView;

@property (nonatomic, strong) NSArray *sectionFetchResults;
@property (nonatomic, strong) NSArray *sectionLocalizedTitles;

// FOR COLLECTIO VIEW
@property (strong, nonatomic)  IBOutlet UICollectionView *photosCollectionView;
@property (nonatomic, strong) PHAssetCollection *assetCollection;
@property (nonatomic, strong) PHFetchResult *assetsFetchResults;
@property (nonatomic, strong) PHCachingImageManager *imageManager;

@property CGRect previousPreheatRect;

- (IBAction)backPressed:(id)sender;

@end

@implementation ImageListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.photosCollectionView registerClass:[PhotoCell class] forCellWithReuseIdentifier:@"photoCell"];
    [self createAttachMent:@"Camera Roll"];
    [self getAllPhotoAndTitles];
    [self setupOptionViewForFirstTime];
    isDrop = NO;
    self.photosCollectionView.backgroundColor = [UIColor colorWithRed:(9.0/255.0) green:(26.0/255.0) blue:(53.0/255.0) alpha:1.0];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // Determine the size of the thumbnails to request from the PHCachingImageManager
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cellSize = ((UICollectionViewFlowLayout *)self.photosCollectionView.collectionViewLayout).itemSize;
    AssetGridThumbnailSize = CGSizeMake(cellSize.width * scale, cellSize.height * scale);
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // Begin caching assets in and around collection view's visible rect.
    // [self updateCachedAssets];
}

- (NSString *)OptionView:(GalleryOptionView *)optionView TitleForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [self getTitleForIndexpath:indexPath];
}



-(NSString *)getTitleForIndexpath:(NSIndexPath*)indexPath{
    PHFetchResult *fetchResult = self.sectionFetchResults[indexPath.section+1];
    PHCollection *collection = fetchResult[indexPath.row];
    return collection.localizedTitle;
}



- (NSInteger)numberOfSectionsInoptionView:(GalleryOptionView *)optionView{
    return (self.sectionFetchResults.count)?self.sectionFetchResults.count-1:0;
}

- (NSInteger)optionView:(GalleryOptionView *)optionView numberOfRowsInSection:(NSInteger)section{
    PHFetchResult *fetchResult = self.sectionFetchResults[section+1];
    return fetchResult.count;
}
- (NSInteger)optionView:(GalleryOptionView *)optionView numberOfItemsInindexPath:(NSIndexPath *)indexPath{
    PHFetchResult *fetchResult = self.sectionFetchResults[indexPath.section+1];
    PHCollection *collection = fetchResult[indexPath.row];
    if (![collection isKindOfClass:[PHAssetCollection class]]) {
        return 0;
    }
    PHAssetCollection *assetCollection = (PHAssetCollection *)collection;
    PHFetchOptions *allPhotosOptions = [[PHFetchOptions alloc] init];
    allPhotosOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    allPhotosOptions.predicate = (_isMediaTypeImage)?[NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage]:[NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeVideo];
    PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:allPhotosOptions];
    return assetsFetchResult.count;
}



#pragma mark - collection view data source

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (!_isShowCameraButton)
        return self.assetsFetchResults.count;
    else
        return self.assetsFetchResults.count+1;
}



- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCell *cell = (PhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    cell.btnUser.tag = indexPath.item;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.layer.cornerRadius         = 6.0;
    
    if (_isShowCameraButton && indexPath.row==0)
    {
        cell.imgUser.image = [UIImage imageNamed:@"image_Camera.png"];
        cell.imgUser.contentMode = UIViewContentModeCenter;
        cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cell.layer.borderWidth = 1.0;
        cell.imgUser.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.7];
        [cell.btnCamera setHidden:NO];
        [cell.btnCamera addTarget:self action:@selector(cameraTapped:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else
    {
        [cell.btnCamera setHidden:YES];
        [cell.btnUser removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btnUser addTarget:self action:@selector(galleryImageTapped:) forControlEvents:UIControlEventTouchUpInside];
        PHAsset *asset = (!_isShowCameraButton)?self.assetsFetchResults[indexPath.item]:self.assetsFetchResults[indexPath.item-1];
        cell.representedAssetIdentifier = asset.localIdentifier;
        
        // Request an image for the asset from the PHCachingImageManager.
        [self.imageManager requestImageForAsset:asset
                                     targetSize:AssetGridThumbnailSize
                                    contentMode:(isSmallPad)?PHImageContentModeAspectFit:PHImageContentModeAspectFill
                                        options:nil
                                  resultHandler:^(UIImage *result, NSDictionary *info) {
                                      // Set the cell's thumbnail image if it's still showing the same asset.
                                      if ([cell.representedAssetIdentifier isEqualToString:asset.localIdentifier]) {
                                          cell.imgUser.image = result;
                                      }
                                  }];
        
        return cell;
    }
}


- (CGSize)targetSize {
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize targetSize = CGSizeMake(640 * scale, 450 * scale);
    return targetSize;
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Selected Image at Index %d",(int)indexPath.row);
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(6,4,4,6);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(90 , 90);
}

- (IBAction)cameraTapped:(UIButton *)sender
{
    if(_isMediaTypeImage)
        [self.delegate cameraImageTappedforPhotoGallery];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate cancelPhotoGallery];
    }];
}

-(void)galleryImageTapped : (UIButton *)sender {
    PHAsset *asset = (!_isShowCameraButton)?self.assetsFetchResults[sender.tag]:self.assetsFetchResults[sender.tag-1];
    // Request an image for the asset from the PHCachingImageManager.
    BOOL assetHasLivePhotoSubType = asset.mediaSubtypes ;
    if (assetHasLivePhotoSubType) {
        [self updateStaticImage:asset];
        
    }else if (asset.mediaType == PHAssetMediaTypeVideo) {
        [self updateStaticVideo:asset];
    }else{
        [self updateStaticImage:asset];
    }
}

- (void)updateStaticVideo:(PHAsset*)Selectedasset{
    PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    options.version = PHVideoRequestOptionsVersionOriginal;
    [[PHImageManager defaultManager] requestAVAssetForVideo:Selectedasset options:options resultHandler:^(AVAsset *asset, AVAudioMix *audioMix, NSDictionary *info) {
        if ([asset isKindOfClass:[AVURLAsset class]]) {
            AVURLAsset* urlAsset = (AVURLAsset*)asset;
            NSData *data = [NSData dataWithContentsOfURL:urlAsset.URL];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self dismissViewControllerAnimated:NO completion:^{
                    [self.delegate selectedVideoFromGallery:data AndURL:urlAsset.URL];
                }];
            });
        }
    }];
}

/*
 NSNumber *size;
 [urlAsset.URL getResourceValue:&size forKey:NSURLFileSizeKey error:nil];
 NSLog(@"size is %f",[size floatValue]/(1024.0*1024.0)); //size is 43.703005
 */


- (void)updateStaticImage:(PHAsset*)Selectedasset {
    // Prepare the options to pass when fetching the live photo.
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    options.networkAccessAllowed = YES;
    [[PHImageManager defaultManager] requestImageForAsset:Selectedasset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeAspectFit options:options resultHandler:^(UIImage *result, NSDictionary *info) {
        // Check if the request was successful.
        if (!result) {
            return;
        }
        // Get the Image From here
        [self dismissViewControllerAnimated:NO completion:^{
            [self.delegate selectedImageFromGallery:result];
        }];
    }];
}




-(void)setupOptionViewForFirstTime{
    self.aOptionView = [[GalleryOptionView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-200)/2.0,50, 200, 0) mediaType:YES];
    self.aOptionView.aDelegate = self;
    self.aOptionView.aDatasource = self;
    [self.view addSubview:self.aOptionView];
}


- (IBAction)pressedHeaderButton:(id)sender {
    if (!isDrop) {
        [UIView animateWithDuration:0.45 animations:^{
            self.aOptionView.frame = CGRectMake((self.view.frame.size.width-280)/2.0,55, 280, CGRectGetHeight([[UIScreen mainScreen] bounds])-100);
        } completion:^(BOOL finished) {
            NSLog(@"Finished");
        }];
    }  else {
        [UIView animateWithDuration:0.45 animations:^{
            self.aOptionView.frame = CGRectMake((self.view.frame.size.width-280)/2.0,55, 280, 0);
        } completion:^(BOOL finished) {
            NSLog(@"Finished");
        }];
    }
    isDrop =!isDrop;
}

#pragma mark - PHPhotoLibraryChangeObserver
-(void)galleryOptionViewDidclickedAtIndexPath:(NSIndexPath *)indexPath{
    [self pressedHeaderButton:nil];
    self.imageManager = [[PHCachingImageManager alloc] init];
    [self resetCachedAssets];
    
    PHFetchResult *fetchResult = self.sectionFetchResults[indexPath.section+1];
    // Get the PHAssetCollection for the selected row.
    PHCollection *collection = fetchResult[indexPath.row];
    if (![collection isKindOfClass:[PHAssetCollection class]]) {
        return;
    }
    // Configure the AAPLAssetGridViewController with the asset collection.
    PHAssetCollection *assetCollection = (PHAssetCollection *)collection;
    PHFetchOptions *allPhotosOptions = [[PHFetchOptions alloc] init];
    allPhotosOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    allPhotosOptions.predicate = (_isMediaTypeImage)?[NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage]:[NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeVideo];
    // PHFetchResult *allPhotos = [PHAsset fetchAssetsWithOptions:allPhotosOptions];
    
    PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:allPhotosOptions];
    self.assetsFetchResults = assetsFetchResult;
    self.assetCollection = assetCollection;
    [self createAttachMent:[self getTitleForIndexpath:indexPath]];
    [self.photosCollectionView reloadData];
    
    
  }
-(void)createAttachMent :(NSString *)titleval
{

    NSAttributedString *labelAttributes = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",titleval]];
    UIImage *img = [UIImage imageNamed:@"dropdown_while.png"];
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = img;
    textAttachment.bounds = CGRectMake(0, lblHeaderInfo.font.descender, textAttachment.image.size.width, textAttachment.image.size.height);
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
    [attrStr appendAttributedString:labelAttributes];
    [attrStr appendAttributedString:attrStringWithImage];
    lblHeaderInfo.attributedText = attrStr;
   // [self.dropDownBtn setAttributedTitle:attrStr forState:UIControlStateNormal];

}
- (void)getAllPhotoAndTitles {
    self.imageManager = [[PHCachingImageManager alloc] init];
    [self resetCachedAssets];
    // Create a PHFetchResult object for each section in the table view.
    PHFetchOptions *allPhotosOptions = [[PHFetchOptions alloc] init];
    allPhotosOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    allPhotosOptions.predicate = (_isMediaTypeImage)?[NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage]:[NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeVideo];
    
    PHFetchResult *allPhotos = [PHAsset fetchAssetsWithOptions:allPhotosOptions];
    
    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    
    PHFetchResult *topLevelUserCollections = [PHCollectionList fetchTopLevelUserCollectionsWithOptions:nil];
    
    // Store the PHFetchResult objects and localized titles for each section.
    self.sectionFetchResults = @[allPhotos, smartAlbums, topLevelUserCollections];
    self.sectionLocalizedTitles = @[@"", NSLocalizedString(@"Smart Albums", @""), NSLocalizedString(@"Albums", @"")];
    
    [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
    [self.aOptionView reloadOptionView];
    
    if(self.sectionFetchResults.count>0){
        self.assetsFetchResults = self.sectionFetchResults[0];
        [self.photosCollectionView reloadData];
    }
    
}





- (void)photoLibraryDidChange:(PHChange *)changeInstance {
    /*
     Change notifications may be made on a background queue. Re-dispatch to the
     main queue before acting on the change as we'll be updating the UI.
     */
    dispatch_async(dispatch_get_main_queue(), ^{
        // Loop through the section fetch results, replacing any fetch results that have been updated.
        NSMutableArray *updatedSectionFetchResults = [self.sectionFetchResults mutableCopy];
        __block BOOL reloadRequired = NO;
        
        [self.sectionFetchResults enumerateObjectsUsingBlock:^(PHFetchResult *collectionsFetchResult, NSUInteger index, BOOL *stop) {
            PHFetchResultChangeDetails *changeDetails = [changeInstance changeDetailsForFetchResult:collectionsFetchResult];
            
            if (changeDetails != nil) {
                [updatedSectionFetchResults replaceObjectAtIndex:index withObject:[changeDetails fetchResultAfterChanges]];
                reloadRequired = YES;
            }
        }];
        
        if (reloadRequired) {
            self.sectionFetchResults = updatedSectionFetchResults;
        }
        
    });
}

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate cancelPhotoGallery];
    }];
}

- (void)resetCachedAssets {
    [self.imageManager stopCachingImagesForAllAssets];
    self.previousPreheatRect = CGRectZero;
}

- (void)dealloc {
    [[PHPhotoLibrary sharedPhotoLibrary] unregisterChangeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
