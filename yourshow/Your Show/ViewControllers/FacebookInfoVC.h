//
//  FacebookInfoVC.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 11/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MSFacebookDelegate <NSObject>

// Display customization

-(void)signUpwith:(NSString *)firstName lastName:(NSString *)lastName emailId:(NSString *)emailId;
-(void)continueWithEmail;
-(void)continueWithLogin;

@end


@interface FacebookInfoVC : UIViewController

@property (nonatomic, strong) NSString *strFBUserFirstName;
@property (nonatomic, strong) NSString *strFBUserLastName;
@property (nonatomic, strong) NSString *strFBUserEmailId;
@property (nonatomic, strong) NSString *strFBUserId;
@property (nonatomic, strong) NSString *strFBUserImage;

@property (assign) id <MSFacebookDelegate> delegate;

@end
