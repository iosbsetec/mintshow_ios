//
//  PhotoGallery.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 13/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "PhotoGallery.h"
#import "PhotoCell.h"
#import "CameraFolderVw.h"

@interface PhotoGallery () <CameraFolderVwDelegate>
{

    CameraFolderVw *photoDDView;
    __weak IBOutlet UILabel *lblTitle;

}
@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong) NSMutableArray *assets;
@end

@implementation PhotoGallery

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_collectionView registerClass:[PhotoCell class] forCellWithReuseIdentifier:@"photoCell"];
    _assets = [@[] mutableCopy];
    __block NSMutableArray *tmpAssets = [@[] mutableCopy];
    ALAssetsLibrary *assetsLibrary = [PhotoGallery defaultAssetsLibrary];
    [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            if(result) {
                [tmpAssets addObject:result];
            }
        }];
        self.assets = tmpAssets;
        
        [self.assets sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSDate *date1 = [obj1 valueForProperty:ALAssetPropertyDate];
            NSDate *date2 = [obj2 valueForProperty:ALAssetPropertyDate];
            
            return ([date1 compare:date2] == NSOrderedAscending ? NSOrderedDescending : NSOrderedAscending);
        }];
        [self.collectionView reloadData];
    } failureBlock:^(NSError *error) {
        NSLog(@"Error loading images %@", error);
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


+ (ALAssetsLibrary *)defaultAssetsLibrary{
    static dispatch_once_t pred;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}


#pragma mark - collection view data source

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
     if (_isCameFromAddShowRoom)
        return self.assets.count;
    else
        return self.assets.count+1;
    }

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCell *cell = (PhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];

    if (_isCameFromAddShowRoom)
    {
        ALAsset *asset = self.assets[indexPath.row];
       // cell.asset = asset;
        [cell.btnUser addTarget:self action:@selector(galleryImageTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        
        if (indexPath.row == 0)
        {
            cell.imgUser.image = [UIImage imageNamed:@"image_Camera.png"];
            cell.imgUser.contentMode = UIViewContentModeCenter;
            [cell.btnUser addTarget:self action:@selector(cameraTapped:) forControlEvents:UIControlEventTouchUpInside];
            cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
            cell.layer.borderWidth = 1.0;
            cell.imgUser.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.7];
        }
        
        else
        {
            ALAsset *asset = self.assets[indexPath.row-1];
         //   cell.asset = asset;
            [cell.btnUser addTarget:self action:@selector(galleryImageTapped:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
    }
    
    cell.btnUser.tag                = indexPath.row;

    cell.backgroundColor = [UIColor clearColor];
    cell.layer.cornerRadius         = 6.0;
    return cell;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(6,4,4,6);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(90 , 90);
}


-(void)galleryImageTapped : (UIButton *)sender {
    

    ALAsset *asset;
    if (_isCameFromAddShowRoom)
        asset= self.assets[sender.tag];
    else
        asset= self.assets[sender.tag-1];
    
    
    ALAssetRepresentation *defaultRepresentation = [asset defaultRepresentation];
    UIImage *latestPhoto= [UIImage imageWithCGImage:[defaultRepresentation fullScreenImage]];
    UIImage *img = latestPhoto;
    CGFloat width = img.size.width;
    CGFloat height = img.size.height;
    if (height <=200 || width<=200)  {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"The Image you are Trying to Upload has Invalid Dimensions" message:@"Please Select Image Dimension 200 X 200 and Above" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
        [alert show];
    }
    else {
        if ([self.delegate respondsToSelector:@selector(selectedImageFromGallery:)])
        [self.delegate selectedImageFromGallery:latestPhoto];
    }
  
    
}



- (IBAction)hideView:(UIButton *)sender
{
    [self.delegate cancelPhotoGallery];
}


- (IBAction)cameraTapped:(UIButton *)sender
{
    [self.delegate cameraImageTappedforPhotoGallery];
}


#pragma Mark -- 

#pragma mark --- *** CAMERA ROLL ---
////////////////////

- (IBAction)cameraDropDown:(UIButton *)sender
{
    if ([photoDDView superview]) {
        [photoDDView closeAnimationZoom];
        return;
    }
    
    photoDDView = [[CameraFolderVw alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, _collectionView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-50) mediaType:YES];
    photoDDView.aDelegate = self;
    [photoDDView showAnimationZoom];
    [self.view addSubview:photoDDView];
}


-(void)selectedFolderGroup : (NSString *)title AssetList : (NSArray *) list
{
    
    
    if ([self.assets count]) {
        [self.assets removeAllObjects];
    }
    
    lblTitle.text = title;
    if (!self.assets) {
        _assets = [[NSMutableArray alloc] init];
    } else {
        [self.assets removeAllObjects];
    }
    
    [self.assets addObjectsFromArray:list];
    [self.collectionView reloadData];
    
}


@end
