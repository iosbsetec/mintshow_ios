//
//  InviteShareVCViewController.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 15/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "InviteShareVC.h"
#import "SocialVideoHelper.h"
#import "CellInvite.h"
#import "FriendsInfo.h"

@interface InviteShareVC ()

{
    NSArray *resultedFriends;
    IBOutlet UITableView *tblFriends;

}
@end

@implementation InviteShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    resultedFriends = [[NSArray alloc]init];
    // Do any additional setup after loading the view from its nib.
    [self checkTheAuthenticationForFb];
    //    NSDictionary *dict = [NSDictionary dictionaryWithObject:@"4076842812" forKey:@"id"];
    //
    //    [SocialVideoHelper sendPrivateMessage:[NSMutableArray arrayWithObjects:dict, nil]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backButton:(id)sender
{
    [self.view removeFromSuperview];
    // [self.navigationController popViewControllerAnimated:YES];
}


-(void)checkTheAuthentications
{

    if ([SocialVideoHelper userHasAccessToTwitter]) {
        
        [SocialVideoHelper getTwitterFriendsForSuccessionBlock:^(id newResponse) {
            NSLog(@"sucsses");

            if ([newResponse isKindOfClass:[NSArray class]]) {
                NSLog(@"Number of friends %d",[(NSArray *)newResponse count]);
                resultedFriends= (NSArray *)newResponse;
                [tblFriends reloadData];
            }
            
        } andFailureBlock:^(NSError *error) {
            
            NSLog(@"failure");

            
        }];
        
    }

}
-(void)checkTheAuthenticationForFb
{
    if (![FBSDKAccessToken currentAccessToken])
    {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithReadPermissions: @[@"public_profile",@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error) {
                NSLog(@"Process error");
            } else if (result.isCancelled) {
                NSLog(@"Cancelled");
            }
            else {
                NSLog(@"Logged in");
                
                [self getFriends];


                
                //TODO: process error or result.
            }
        }];
    }
    
    else
    {
        [self getFriends];
    
    
    }

    
    
}
-(void)getFriends
{
    [SocialVideoHelper getFbfriendslist:^(id newResponse) {
        NSLog(@"sucsses");
        
        if ([newResponse isKindOfClass:[NSArray class]]) {
            NSLog(@"Number of friends %d",[(NSArray *)newResponse count]);
            resultedFriends= (NSArray *)newResponse;
            [tblFriends reloadData];
        }
        
    } andFailureBlock:^(NSError *error) {
        NSLog(@"failure");
        
    }];


}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [resultedFriends count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *simpleTableIdentifier = @"CellInvite";
    
    CellInvite *cell = (CellInvite *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CellInvite" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
        if ([resultedFriends count]) {
        
        FriendsInfo *obj = [resultedFriends objectAtIndex:indexPath.row];
        [cell setInfo:obj.profile_image_url userName:obj.name];
        }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


@end
