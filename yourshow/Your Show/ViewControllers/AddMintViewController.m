//
//  AddMintViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota  on 8/12/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#define EDITTEXTIMAGE(VALUE)     [imgEditText setImage:[UIImage imageNamed:(VALUE)?@"":@""]]

typedef enum showRoomSelectionType
{
    SHOWROOM_NOTSELECTD,
    SHOWROOM_PUBLICSELECTD,
    SHOWROOM_PRIVATESELECTD,
} ShowroomType;


#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/PHAsset.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import <Social/Social.h>
#import <MapKit/MapKit.h>

#import "PhotoGallery.h"

#import "AddMintViewController.h"
#import "PrinterestGridTable.h"
#import "JitenCollectionView.h"
#import "ShowRoomModel.h"



#import "PhotoCell.h"
#import "KZCameraView.h"
#import "AppDelegate.h"
#import "NearLocation.h"
#import "FilterListTable.h"
#import "YoutubeVC.h"
#import "HCYoutubeParser.h"
#import "LocatioDetails.h"
#import "VideoGalleryVC.h"

#import "YSAPICalls.h"
#import "ProgressHUD.h"
#import "AVCamViewController.h"
#import "VPImageCropperViewController.h"
#import "SPHSingletonClass.h"
#import "Feedmodel.h"
#import "MapViewController.h"
#import "LocationModel.h"
#import "POPupTableView.h"
#import "TSPopoverController.h"
#import "EntitySupport.h"
#import "YSVideoPlayerView.h"
#import "STTweetView.h"
#import "CoachingTip.h"
#import "NSString+Emojize.h"



#import "ImageListViewController.h"

#define DUMMY_ACCESSTOKEN   @"4de021ed0a498a8c0937b63e31b3259de2f3d33a"



@interface AddMintViewController () <PinterestTableDelegate,PinterestTableViewDataSource,NearLocationTableDelegate,FilterListTableDelegate,YoutubeVCDelegate,AVCamVCDelegate,VPImageCropperDelegate,KZCameraViewDelegate,FBSDKSharingDelegate,textSuggestDelegate,POPUpTableViewDataSource,POPUpTableDelegate,JitenCollectionTableDelegate,JitenCollectionViewDataSource,VideoGalleryProtocol,ysPlayerViewDelegate,CLLocationManagerDelegate,mapPickerDelegate,NewPhotoGalleryDelegate>
{
    NearLocation        *nearLocationTableView;
    FilterListTable     *filterTableView;
    SPHSingletonClass   *sharedClass;
    TSPopoverController *popoverController;
    POPupTableView      *apopupTable;
    
    ShowroomType         theShowroomSelectState;
    EntityType           selectedEntitytype;
    NSRange              selectedRange;
    CGFloat              multiShowroomHeight;
    
    NSMutableArray       *dataArray;
    NSMutableArray       *showroomAddedArray;
    NSMutableArray       *hashTagAddedArray;
    NSMutableArray       *userNameAddedArray;
    NSMutableArray       *arrayVideos;
    NSMutableArray       *arrayFeatureUsers;
    NSMutableArray       *arrayStarFeatures;
    NSMutableArray       *allShowRoomdata;
    NSMutableArray       *arrayAddedPrivateShowroolList;


    
    BOOL isFirsttime,isSelectFromPopUp;
    BOOL isImageSelected;
    BOOL isYouTubeVideoSelected;
    BOOL isNormalVideoSelected;
    BOOL finalMintSelection;
    BOOL isImageSelectFromCustomCamera;
    BOOL isDescriptionContentPrivateShowroom;
    BOOL isPrivateShowroomSelectd;
    BOOL isCameraSelected;
    BOOL isAPICalling;
    BOOL isDataFinisedFromServer;
    BOOL isPostMintClicked;


    IBOutlet UIView *viewPhoto;
    IBOutlet UIView *viewCamera;
    IBOutlet UIView *VideoView;
    IBOutlet UICollectionView *ceoconnectCollectionVw;

    //selectd tag for the Header options - video,image ,text,youtube ,camera
    NSInteger selectdHeaderbuttonTag;
    
     // Here we will save the Info for the All showroom.
     // for future use after the Edit happen we need to add end of the edited Description
    NSMutableString *strAllShowroomsForLaterUse;
    NSString *textCaptions;
    NSString *mintType;
    NSString *selectedYoutubeUrl;
    NSString *privateShowRoomId;
    NSString *privateShowRoomName;
    NSString *selectedFeature;

    NSMutableDictionary *dic;

    
           IBOutlet UILabel *lblVCTitle;
    __weak IBOutlet UIView *viewButtom;
    
    int pageNumber;
    id jsonObjectData;
    
    BOOL isCameraIconTapped;
    BOOL isKeyBoardShown;

}

@property (nonatomic,retain)  CLLocationManager       *locationManager;

@property (weak, nonatomic)   IBOutlet UIView *viewTopSection;
@property (strong, nonatomic) IBOutlet UILabel *totaltextCounter;
@property (weak, nonatomic)   IBOutlet PrinterestGridTable *addmintTableview;
@property (strong, nonatomic) IBOutlet JitenCollectionView *showRoomCollectionview;

@property (nonatomic,retain)  ACAccountStore *accountStore;
@property (nonatomic,retain)  ACAccount *account;

@property (strong, nonatomic) YSVideoPlayerView* ysplayer;
@property (nonatomic, strong) KZCameraView *cam;
@property (strong, nonatomic) STTweetView *yourShowTextView;
@property (nonatomic, strong) NSDictionary *emojiDict;



- (IBAction)backPressed:(id)sender;

@end

@implementation AddMintViewController


#pragma mark------
#pragma mark --  VIEW LOAD METHID --
#pragma mark------
- (void)viewDidLoad
{
    [super viewDidLoad];
    pageNumber = 1;

    isFirsttime             = YES;
    isImageSelected         = NO;
    isYouTubeVideoSelected  = NO;
    isNormalVideoSelected   = NO;
    isSelectFromPopUp       = NO;
    isCameraSelected        = NO;
    isPostMintClicked       = NO;
    self.addmintTableview.isLoadingFinished = YES;

    allShowRoomdata                 = [[NSMutableArray alloc]init];
    arrayFeatureUsers               = [[NSMutableArray alloc]init];
    arrayStarFeatures               = [[NSMutableArray alloc]init];
    arrayAddedPrivateShowroolList   = [[NSMutableArray alloc]init];
    
    theShowroomSelectState = SHOWROOM_NOTSELECTD;

    self.anewModel = [VideoModel new];
    self.anewModel.isVideoPlaying = NO;
    self.anewModel.vcellType = VideoCellTypeRemoteUrl;
    
    
    multiShowroomHeight    = 100;
    selectdHeaderbuttonTag = 0;
    
    self.addmintTableview.separatorStyle     = UITableViewCellSeparatorStyleNone;
      self.addmintTableview.isFromMintDetail = _isFromMintDetail;
    // Do any additional setup after loading the view from its nib.
    [_addmintTableview setFrame:CGRectMake(_addmintTableview.frame.origin.x, _addmintTableview.frame.origin.y, _addmintTableview.frame.size.width, _addmintTableview.frame.size.height+50)];
 

    
    
    // BELOW CODES FOR DESCRIPTION IN ADD MINT
    selectedRange         = NSMakeRange(0, 0);
    self.yourShowTextView = [[STTweetView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-8, 85)];
    self.yourShowTextView.textDelegate = self;
    
    self.yourShowTextView.nMaxTextLength = 300;
    [self setPlaceHolder];
    [self createTableAndPopupView];
    [self initializer];


    
    

    self.emojiDict = [YSSupport reverseEmojiAliases];
    NSLog(@"Postmintval = %@",GETVALUE(CEO_ISADDMINTFIRSTTIMEVISIT));
    
    if ([GETVALUE(CEO_ISADDMINTFIRSTTIMEVISIT) isEqualToString:@"yes"])
    {
        [self showCultureTips];
    }

    
    
    self.locationManager = [[CLLocationManager alloc] init];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    else
        [self.locationManager requestAlwaysAuthorization];
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate=self;
    [self.locationManager startUpdatingLocation];

    
    
    [MSMixPanelTrackings trackForATaskForUser:nil withTaskType:MSTaskTypeOpenedAddMint];
    self.addmintTableview.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GalleryTapped:)          name:@"GalleryTapped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissVideoView:)            name:@"VideoGallery" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardHidingInAddMint:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardShowingInAddMint:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupViewHeightframe) name:@"BottomViewFrame" object:nil];
    
    // [self setupViewHeightframe];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BottomViewFrame" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self checkHeaderButtonStatus];
    showroomAddedArray = [[NSMutableArray alloc]init];
    hashTagAddedArray = [[NSMutableArray alloc]init];
    userNameAddedArray = [[NSMutableArray alloc]init];
    if(isFirsttime)[self getAllShowroomForMyShowTabWithPageNo:pageNumber];
    isFirsttime = NO;
    [viewButtom setFrame:CGRectMake(0,self.view.frame.size.height - 40, self.view.frame.size.width, 40)];
    //  [self.addmintTableview setHidden:NO];
    [viewButtom setUserInteractionEnabled:YES];
    isCameraIconTapped = NO;
    if (_isFromMintDetail)
        [self setEditMintScreenDatas];
}

- (void)didReceiveMemoryWarning{
    if ([self isViewLoaded] && [self.view window] == nil){
        self.view = nil;
        arrayVideos = nil;
    }
    [super didReceiveMemoryWarning];
}

#pragma mark------
#pragma mark --  LOCATION --
#pragma mark------

- (NSString *)deviceLocation
{
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
    return theLocation;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
    if(newLocation.coordinate.latitude && newLocation.coordinate.longitude) [self.locationManager stopUpdatingLocation];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count > 0) {
            CLPlacemark *placemark = placemarks[0];
            
           
            sharedClass.selectedLocation.yaacPlaceName = [NSString stringWithFormat:@"%@", [self stringForPlacemark:placemark]];
            [self.addmintTableview setLocation:sharedClass.selectedLocation.yaacPlaceName withSection: (isCameraSelected||isImageSelected ||isNormalVideoSelected||isYouTubeVideoSelected)?2:1];
            
        }
    }];
}


- (NSString *)stringForPlacemark:(CLPlacemark *)placemark {
    
    NSMutableString *string = [[NSMutableString alloc] init];
    if (placemark.locality) {
        [string appendString:placemark.locality];
    }
    
    if (placemark.administrativeArea) {
        if (string.length > 0)
            [string appendString:@", "];
        [string appendString:placemark.administrativeArea];
    }
    
    if (string.length == 0 && placemark.name)
        [string appendString:placemark.name];
    
    return string;
}

#pragma mark------
#pragma mark --  CULTURE TIP/COACHING TIPS --
#pragma mark------
-(void)showCultureTips
{
    self.viewCulture.frame = self.view.frame;
    [self.view addSubview:self.viewCulture];
}

-(IBAction)showCoachingTips:(id)sender{
    
//    SETVALUE(@"no", CEO_ISADDMINTFIRSTTIMEVISIT);
//    SYNCHRONISE;
    
    [self.viewCulture removeFromSuperview];
    CoachingTip *viewCoach = [[CoachingTip alloc]initWithFrame:[UIScreen mainScreen].bounds screenName:@"AddMint" headerPresent:NO];
    [[[UIApplication sharedApplication]keyWindow] addSubview:viewCoach];
 
}



-(void)showAlertForPrivateShowroom : (NSArray *) list
{

    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"This Mints Contains More than One Private Showroom. Please Add Only one private Showroom. The private showroom are listed here!!"
                                  delegate:nil
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles: nil];
    for(int i=0;i<[list count];i++)
        
    {
        
        [actionSheet addButtonWithTitle:[list objectAtIndex:i]];
        
    }
    
    actionSheet.actionSheetStyle = UIBarStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}


-(void)setupViewHeightframe
{
   // AppDelegate *delegateApp                     = (AppDelegate *)[UIApplication sharedApplication].delegate;
   // delegateApp.ysTabBarController.tabBar.hidden = YES;
   // delegateApp.lbl.hidden                       = YES;
    CGRect frame                                 = [[UIScreen mainScreen] bounds];
    self.view.frame                              = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,frame.size.height);
}



-(void)setEditMintScreenDatas
{
    lblVCTitle.text = @"Edit A Mint";

    if (_feedModelData.mintType == MSMintTypeImage)
    {
        isImageSelected = YES;
        sharedClass.mintDetailImgURL =  [NSURL URLWithString:_feedModelData.feed_thumb_image];
    }
    else if ( _feedModelData.mintType != MSMintTypeImage &&  _feedModelData.mintType != MSMintTypeText)
    {
        if(_feedModelData.youtube_id.length>0)
            [self YoutubeVCDelegateMethod:_feedModelData.youtube_id];
        else
            [self dismissVideoView:nil];
    }
    
  
    sharedClass.selectedLocation.yaacPlaceName = _feedModelData.feed_posted_location;
    sharedClass.categoryname = _feedModelData.category_type;
    
    NSString *removePrivateDATA = _feedModelData.feed_description;
   
    for (NSString *privateShowroom in _feedModelData.privateShowroomListArray)
    {
        if ([privateShowroom containsString:@"-1"]) {
            isDescriptionContentPrivateShowroom = YES;
            self.yourShowTextView.isStarCharBlocked = YES;
        }
    }
    
    if (isDescriptionContentPrivateShowroom)
    {
        strAllShowroomsForLaterUse = [[NSMutableString alloc]init];
        
        for (NSString *privateShowroom in _feedModelData.privateShowroomListArray)
        {
            NSString *originalShowroom = [privateShowroom stringByReplacingCharactersInRange:NSMakeRange(privateShowroom.length-2, 2) withString:@""];
            [strAllShowroomsForLaterUse appendString:[NSString stringWithFormat:@" *%@",originalShowroom]];
            removePrivateDATA = [removePrivateDATA stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"*%@",originalShowroom] withString:@""];
        }
    }
    if(_feedModelData.feed_description.length>0)
    {
        self.yourShowTextView.textView.textColor = [UIColor blackColor];
        self.yourShowTextView.textView.text = [[removePrivateDATA emojizedString] StringByChangingComplimintStringToNormalString];
        [self.yourShowTextView determineHotWordsNew];
    }
    
    [self.addmintTableview reloadData];
}

-(void)initializer
{
    // MULTI SHOW ROOM  COLLECTION VIEW
    self.showRoomCollectionview.CollectionDataSource    = self;
    self.showRoomCollectionview.collectiondelegate      = self;
    self.showRoomCollectionview.Addshowroom             = NO;
    self.showRoomCollectionview.isCellSelectable        = YES;
    self.showRoomCollectionview.backgroundColor         = [UIColor whiteColor];
    self.showRoomCollectionview.showsHorizontalScrollIndicator = NO;
    self.showRoomCollectionview.showsVerticalScrollIndicator   = NO;
    self.showRoomCollectionview.scrollEnabled           = NO;
}



-(void)createTableviewAndShowPopup:(EntityType)textSuggestType
{
    apopupTable.suggestType = textSuggestType;
    CGFloat yaxis = (isCameraSelected||isImageSelected|| isYouTubeVideoSelected||isNormalVideoSelected)? 270:40;
    [popoverController showPopoverWithRect:CGRectMake(0,yaxis,300,100)];
   // [popoverController.view setBackgroundColor:[UIColor redColor]];
    [apopupTable reloadData];
    
}
- (STTweetView *)addMintDescriptionTextViewforRow:(NSInteger)row
{
    return  self.yourShowTextView;
}
- (UILabel *)addMintCounterLabelforRow:(NSInteger)row
{
    return  self.totaltextCounter;
}
#pragma mark --
#pragma mark --PopUp table (For #tag, @feature and * feature)
#pragma mark --
// BELOW CODES BY SIBA PRASAD HOTA FOR DESCRIPTION
// DONT CHANGE ANYTHING WITHOUT ASKING

-(void)createTableAndPopupView
{
    apopupTable = [[POPupTableView alloc]initWithFrame:CGRectMake(0, 0, 250, 150)];
    apopupTable.popupDataSource = self;
    apopupTable.popupDelegate   = self;
    
    popoverController = [[TSPopoverController alloc] initWithView:apopupTable];
    popoverController.cornerRadius = 5;
    popoverController.popoverBaseColor = [UIColor lightGrayColor];
    popoverController.popoverGradient= NO;
    
}
- (NSInteger)rowsForBubbleTable:(POPupTableView *)tableView
{
    tableView.separatorStyle= (dataArray.count>0)?UITableViewCellSeparatorStyleSingleLine:UITableViewCellSeparatorStyleNone;
    return dataArray.count;
}
- (FeatureModel*)bubbleTableView:(POPupTableView *)tableView dataForRow:(NSInteger)row;
{
    return [dataArray objectAtIndex:row];
}
#pragma  Mark - Add/edit icon for Medias delegate
-(void)onEditMediaTapped
{
    if (isYouTubeVideoSelected) {
        [self YouTubeClicked];
    }
    else if (isNormalVideoSelected) {
        [self VideoClicked];
    }
    else if (isImageSelected) {
        [self GalleryClicked];
    }
    else if (isCameraSelected) {
        [self cameraClicked];
    }
}
-(void)onClearMediaTapped
{
    isCameraSelected            =   NO;
    isImageSelected             =   NO;
    isYouTubeVideoSelected      =   NO;
    isNormalVideoSelected       =   NO;
    sharedClass.selectedImage   =   nil;
    [self.addmintTableview reloadData];
}

#pragma mark -- Popup Table delegate

- (void) PopupTableSelectedIndex :(NSInteger)selectedRowNo
{
    FeatureModel *fdata = [dataArray objectAtIndex:selectedRowNo];
    
    if (fdata.is_private) {
        privateShowRoomId        = [fdata ct_id];
        privateShowRoomName      = [fdata name];
        isPrivateShowroomSelectd = YES;
    }
    else
    {
        isPrivateShowroomSelectd = NO;
    
    }
    if (selectedEntitytype == EntityTypeshowroom)
    {
        theShowroomSelectState =  ([fdata is_private])? SHOWROOM_PRIVATESELECTD:SHOWROOM_PUBLICSELECTD;
        NSString *showroomName = [self getShowrromWithoutSpecialCharacters:fdata.name];

        if (![self checkForshowroomIDexistance:showroomName]) {
            //[showroomAddedArray addObject:showroomName];
            // && showroomAddedArray.count<7
            isSelectFromPopUp = YES;
            [self updateShowRoom:showroomName];

            [self.yourShowTextView wordSelectedForSuggestType:selectedEntitytype AndString:showroomName andID:fdata.ct_id forRange:selectedRange];
        }
  
    }
    else if (selectedEntitytype == EntityTypeHashTag)
    {
        if (![self checkForHashtagIDexistance:fdata.ct_id]) {
            // && hashTagAddedArray.count<7
            [hashTagAddedArray addObject:fdata.ct_id];
            [self.yourShowTextView wordSelectedForSuggestType:selectedEntitytype AndString:fdata.name andID:fdata.ct_id forRange:selectedRange];
        }
        
    }
    else if (selectedEntitytype == EntityTypeMention)
    {
        if (![self checkForUserIDexistance:fdata.ct_id]) {
            [userNameAddedArray addObject:fdata.ct_id];
            // && userNameAddedArray.count<7
            [self.yourShowTextView wordSelectedForSuggestType:selectedEntitytype AndString:fdata.name andID:fdata.ct_id forRange:selectedRange];
        }
        
    }
   
    [popoverController dismissPopoverAnimatd:YES];
}

#pragma mark -- End Popup Table delegate
- (void)beginSuggestingForTextView:(EntityType)textSuggestType query:(NSString *)suggestionQuery Inrange:(NSRange)range
{
    
    if (suggestionQuery.length<1) {
        return;
    }
    
    selectedRange = range;
    
    switch (textSuggestType) {
        case EntityTypeHashTag:
        {
            selectedFeature = @"#";
            suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"#" withString:@""];
            [self getHashTagList:suggestionQuery withPagenum:1];
            
        }
            break;
        case EntityTypeMention:
        {
            selectedFeature = @"@";
            suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"@" withString:@""];
            [self getUserList:suggestionQuery withPagenum:1];
            
        }
            break;
        case EntityTypeshowroom:
        {
              if (!isPrivateShowroomSelectd) {
                selectedFeature = @"*";
                suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"*" withString:@""];
                  if (showroomAddedArray.count!=7)
                [self getShowroomList:suggestionQuery withPagenum:1];
              }
            
        }
            break;
        default:
            break;
    }
}
- (void)endSuggesting
{
    
    [popoverController dismissPopoverAnimatd:YES];
    
}
- (void)reloadSuggestionsWithType:(EntityType)suggestionType query:(NSString *)suggestionQuery range:(NSRange)suggestionRange
{
    
}


-(NSString*)getUrlFromMediaID:(NSString*)mediaID
{
    return [NSString stringWithFormat:@"%@/%@.mp4",VIDEO_DIRECT_URL,mediaID];    // need to change the url
}


#pragma mark -- SERVER CALL FOR *,@,# FEATURE LIST
- (void)getUserList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),name,pagenum ];
    [self CallAPIforType:EntityTypeMention andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", USER_LIST_API,submitData]]];
}

- (void)getShowroomList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),name,pagenum] ;
    [self CallAPIforType:EntityTypeshowroom andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", SHOWROOM_LIST_API,submitData]]];
}

- (void)getHashTagList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d&showroom_type=0",GETVALUE(CEO_AccessToken),name,pagenum];
    [self CallAPIforType:EntityTypeHashTag andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", HASHTAG_LIST_API,submitData]]];
}

#pragma mark --
-(void)CallAPIforType:(EntityType)type andAPIUrl:(NSURL *)myUrl
{
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
//    [submitrequest setHTTPMethod:@"POST"];
//    [submitrequest setHTTPBody:submitData];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         switch (type) {
             case EntityTypeMention:
                 dataArray =[EntitySupport UserlistDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
             case EntityTypeHashTag:
                 dataArray =[EntitySupport TagDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
             case EntityTypeshowroom:
                 dataArray =[EntitySupport showRoomDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
                 
             default:
                 break;
         }
         [self deleteExistingRecordWithModelData:type];
         selectedEntitytype = type;
         if (dataArray.count>0)
         {
             [self createTableviewAndShowPopup:type];
         }
         else
         {
             [popoverController dismissPopoverAnimatd:YES];
         }
     }];
}

-(void)wordDeletedForSuggestType:(EntityType)textSuggestion andID:(NSString*)contactID
{
    if (textSuggestion == EntityTypeshowroom) {
        for (int as=0; as<showroomAddedArray.count; as++ ){
            NSString *ct_id = [showroomAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                if ([privateShowRoomId isEqualToString:contactID]) {
                    privateShowRoomId = @"";
                    isPrivateShowroomSelectd = NO;
                }
                [showroomAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
    else if (textSuggestion == EntityTypeHashTag) {
        for (int as=0; as<hashTagAddedArray.count; as++ ){
            NSString *ct_id = [hashTagAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                [hashTagAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
    else if (textSuggestion == EntityTypeMention) {
        for (int as=0; as<userNameAddedArray.count; as++ ){
            NSString *ct_id = [userNameAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                [userNameAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
}

-(void)deleteExistingRecordWithModelData:(EntityType)entityType
{
    NSMutableArray *anArray = [dataArray mutableCopy];
    
    if (entityType == EntityTypeshowroom) {
        for (NSString *c_id in showroomAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.name]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }
    else if (entityType == EntityTypeHashTag) {
        for (NSString *c_id in hashTagAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.ct_id]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }

    else if (entityType == EntityTypeMention) {
        for (NSString *c_id in userNameAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.ct_id]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }

  
}

- (void)textDidChange:(STTweetView*)commentView
{
    NSLog(@"total charecter= %d", (int)commentView.counterCount);
    self.totaltextCounter.text = [NSString stringWithFormat:@"%d",300-(int)commentView.counterCount];
    
    if (selectdHeaderbuttonTag) {
        if(isSmallPad)
        {
            [self.addmintTableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        else
            [self.addmintTableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}



- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}



-(void)setLocalVideoPath:(NSString*)dataPath
{
    isNormalVideoSelected =YES;
    isImageSelected =NO;
    isYouTubeVideoSelected =NO;
    
    sharedClass.localVideopath = [NSURL fileURLWithPath:dataPath];
    sharedClass.selectedImage = [UIImage imageNamed:@"placeholderImage"];
    [self.addmintTableview reloadData];
}




-(NSString*) applicationDocumentsDirectory
{
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    return docsDir;
}




-(void)getAllShowroomForMyShowTabWithPageNo:(int)pgNo     //change method name here
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&showroom_list_type=%@&page_number=%d",GETVALUE(CEO_AccessToken),@"0",pgNo];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@users/get_showroom_list?%@",ServerUrl,submitData]]];
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isAPICalling = NO;
         
         Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:METHOD_MYSHOW_SHOWROOM];
         if ([gmodel.status_code integerValue])
         {
             if(gmodel.tempArray.count > 0)
             {
                 isDataFinisedFromServer = NO;
                 [allShowRoomdata addObjectsFromArray: gmodel.tempArray];
                 [self batchreloadCollectionView];
             }
             else
             {
                 isDataFinisedFromServer = YES;
                 
             }
             
         }
         
     }];
}


-(void)batchreloadCollectionView
{
    if (isDescriptionContentPrivateShowroom) {
        self.showRoomCollectionview.userInteractionEnabled = NO;
    }
    
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.showRoomCollectionview performBatchUpdates:^{
        [self.showRoomCollectionview reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
            multiShowroomHeight = self.showRoomCollectionview.collectionViewLayout.collectionViewContentSize.height;
            self.showRoomCollectionview.frame = CGRectMake(0, 0, self.view.frame.size.width, multiShowroomHeight);
            if(!isPostMintClicked)
            [self reloadTableviewWithsection:(isCameraSelected||isImageSelected||isNormalVideoSelected||isYouTubeVideoSelected)?3:2];
           // [self.addmintTableview reloadData];
        }
    }];
    
}

-(void)reloadTableviewWithsection:(NSInteger)section
{
    [self.addmintTableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:section], nil]  withRowAnimation:UITableViewRowAnimationNone];
}


-(void)setPlaceHolder {
    sharedClass = [SPHSingletonClass sharedSingletonClass];
    sharedClass.descriptionText     = @"Describe it positive...";
    sharedClass.categoryname        = @"Choose Mint Type";
    sharedClass.selectedLocation    = [LocatioDetails new];
    sharedClass.selectedLocation.yaacPlaceName = @"Name This Location";
//    // need to add
//    if ([sharedClass.glblShowTagName length]) {
//        self.yourShowTextView.textView.text = [NSString stringWithFormat:@"*%@",sharedClass.glblShowTagName];
//        [self.yourShowTextView determineHotWordsNew];
//    }
}

-(void)GalleryTapped : (NSNotification *)notification {
    if ([[[notification valueForKey:@"userInfo"] valueForKey:@"VideoORGallery"] integerValue] == 2)  {
    }  else {
        [self cameraClicked];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
   if (buttonIndex == 1)  {
        [alertView dismissWithClickedButtonIndex:1 animated:YES];
        [self resetAllUis];
    }
}


- (IBAction)backPressed:(id)sender {
    
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Cancel this Mint?"
                                      message:@"Your Mint post will be Deleted."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [alert.view setTintColor:[UIColor blackColor]];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [self resetAllUis];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Keep"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
}




-(void)resetAllUis{
    sharedClass.mintdetail_url     = nil;
    sharedClass.isFacebookSelected = NO;
    sharedClass.isTwitterSelected = NO;
    
    isImageSelected         = NO;
    isYouTubeVideoSelected  = NO;
    isNormalVideoSelected   = NO;
    
    [self setTabar];
    sharedClass.selectedImage = nil;
    [self.addmintTableview reloadData];
    [self removeFile:sharedClass.localVideopath];
}

- (void)switchToCameraScreenChange {
    ImageListViewController *vcVideoGal = [[ImageListViewController alloc]initWithNibName:@"ImageListViewController" bundle:nil];
    vcVideoGal.isShowCameraButton = YES;
    vcVideoGal.isMediaTypeImage = NO;
    vcVideoGal.delegate = self;
    [self  presentViewController:vcVideoGal animated:YES completion:^{
    }];
}


-(void)VideoGallerycancelTapped {
    [self.cam addSubviewToCamera];
}


//Notification method .
//Called once user selectd a video from Video Galery .
-(void)dismissVideoView : (NSNotification *)notification
{
    //[self.cam removeFromSuperview];
    self.cam.delegate = nil;
    self.cam = nil;
    [VideoView removeFromSuperview];
    isNormalVideoSelected =YES;
    isImageSelected = NO;
    isYouTubeVideoSelected= NO;
    
    self.anewModel.mintType       =   1;
    self.anewModel.vcellType      =   VideoCellTypeLocalPath;
    
    SPHSingletonClass* sharedClassObject = [SPHSingletonClass sharedSingletonClass];
    if (_isFromMintDetail)
        self.anewModel.VideoUrl = [NSURL URLWithString:[self getUrlFromMediaID:_feedModelData.media_id]];
    else
    {
        self.anewModel.VideoUrl = (NSURL *)sharedClassObject.remoteVideoURLString;
        [self.addmintTableview reloadData];
    }

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AVAsset *asset = [AVAsset assetWithURL:self.anewModel.VideoUrl];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        CMTime time = CMTimeMake(0, 1);
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.anewModel.thumbImage = thumbnail;
            CGImageRelease(imageRef);
            
        });
    });

    
    
}
#pragma mark - video play delegate






#pragma mark - Collectionview Methods

- (YSVideoPlayerView *)videoPlayerForDiscoverTableView:(DiscoverTableview *)tableView ForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.ysplayer;
}
- (VideoModel *)VideoTableViewViewDataForRow:(NSInteger)row{
    return self.anewModel;
}

#pragma mark - video play delegate
- (void)didClickedVideoBtn:(MintVideoTableViewCell *)videoCell AndvcelType:(VideoCellType)celltype {
    self.addmintTableview.viewEditMediaHolder.alpha = 0.0;
    self.anewModel.isVideoPlaying = YES;
    videoCell.frontView.alpha=1.0;
    // Do any additional setup after loading the view, typically from a nib.
    if (_isFromMintDetail && _feedModelData.youtube_id.length==0)
    {
        self.ysplayer = [[YSVideoPlayerView alloc] initWithFrame:videoCell.bounds contentURL:[NSURL URLWithString:[self getUrlFromMediaID:_feedModelData.media_id]]];
    }
    else
        self.ysplayer = [[YSVideoPlayerView alloc] initWithFrame:videoCell.bounds contentURL:self.anewModel.VideoUrl];
    
    [videoCell.contentView addSubview:self.ysplayer];
    self.ysplayer.ysdelegate = self;
    self.ysplayer.tintColor = [UIColor redColor];
    [self.ysplayer play];
}


-(void)ysPlayerViewZoomButtonClicked:(YSVideoPlayerView*)view isFullScreenMode:(BOOL)isfullScreen{
    
}



-(void)ysPlayerStoppedPlayback:(YSVideoPlayerView*)view{
        self.addmintTableview.viewEditMediaHolder.alpha = 1.0;
    [view pause];
    self.anewModel.isVideoPlaying = NO;
    [self.addmintTableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:1 ], nil] withRowAnimation:UITableViewRowAnimationNone];
    [view removeFromSuperview];
    view = nil;
}

-(void)ysPlayerFinishedPlayback:(YSVideoPlayerView*)view{
    self.anewModel.isVideoPlaying = NO;
    [self.addmintTableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:1 ], nil] withRowAnimation:UITableViewRowAnimationNone];
    
    [view removeFromSuperview];
    view = nil;
}


- ( NSInteger)DiscoverTableViewNumberOfsections:(PrinterestGridTable *)tableView
{
    return (isCameraSelected||isImageSelected||isNormalVideoSelected||isYouTubeVideoSelected)?4:3;
}


-(CGFloat)DiscoverTableView:(PrinterestGridTable *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isCameraSelected||isImageSelected||isNormalVideoSelected||isYouTubeVideoSelected)
    {
        if(indexPath.section == 0)
return (_isFromMintDetail)?0.0:50.0;
        if(indexPath.section == 1)
            return 195.0;
        if(indexPath.section == 2)
            return 248.0;
        else
            return multiShowroomHeight+40;
    }
    if(indexPath.section == 0)
return (_isFromMintDetail)?0.0:50.0;
    if(indexPath.section == 1)
        return 248.0;
    if(indexPath.section == 2)
        return multiShowroomHeight+40;
    else
        return 30.0;
}


- (PinterestViewCellType)DiscoverTableView:(PrinterestGridTable *)tableView cellTypeForSection:(NSInteger)section {
    if (isCameraSelected||isImageSelected||isNormalVideoSelected||isYouTubeVideoSelected)  {
        if (section == 0)
            return PinterestViewCellTypeAddMintOptions;
        if( section ==1) {
            if (isCameraSelected||isImageSelected)
                return PinterestViewCellTypeAddMintImage;
            else if(isNormalVideoSelected)
                return PinterestViewCellTypeVideo;
            else
                return PinterestViewCellTypeVideo;
        }
        if( section ==2)
            return PinterestViewCellTypeAddMintDescription;
        else
            return PinterestViewCellTypeMultiShowroom;
    }
    if (section == 0)
        return PinterestViewCellTypeAddMintOptions;
    if( section ==1)
        return PinterestViewCellTypeAddMintDescription;
    else
        return PinterestViewCellTypeMultiShowroom;
}


- (NSString *)DiscoverTableView:(PrinterestGridTable *)tableView TitleOfHeaderForSection:(NSInteger)section
{
    if  (!(isCameraSelected||isImageSelected ||isNormalVideoSelected||isYouTubeVideoSelected) && section == 2)
        return @"Tag More Showrooms";
    if ((isCameraSelected||isImageSelected ||isNormalVideoSelected||isYouTubeVideoSelected) && section == 3)
        return @"Tag More Showrooms";
    else return  @"";
}

- (NSDictionary *)DiscoverTableView:(PrinterestGridTable *)tableView DataForSection:(NSInteger)section
{
    return [NSDictionary new];
}

- (void)LoadNextPagetriggered{
    if (isAPICalling) {
        return;
    }
    if (isDataFinisedFromServer) {
        return;
    }
    pageNumber = pageNumber +1;
    NSLog(@"*** Scroll End ****");
    isAPICalling = YES;
    [self getAllShowroomForMyShowTabWithPageNo:pageNumber];
}


-(NSString *)getShowrromWithoutSpecialCharacters : (NSString *) showroomData {

    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    NSString *showroomName = [[showroomData componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    return showroomName;

}
#pragma THIS CODE FOR SHOWROOM COLLECTION VIEW

-(void)updateShowRoom:(NSString * )showroomName
{
    for (int i=0; i<allShowRoomdata.count; i++)
    {
        ShowRoomModel *showModel = [allShowRoomdata objectAtIndex:i];
        if ([showroomName isEqualToString:[self getShowrromWithoutSpecialCharacters:showModel.show_tag]])
        {
            //            showModel.selected = ([showModel.selected integerValue])?@"0":@"1";
            [self JitenCollectionViewDelegateMethod:showModel selectedIndex:i];
        }
    }
}


#pragma THIS CODE FOR SHOWROOM COLLECTION VIEW

- (void) JitenCollectionViewDelegateMethod: (ShowRoomModel *) selectedData selectedIndex :(NSInteger) selectedRowNo
{
    
    NSString *showroomName = [self getShowrromWithoutSpecialCharacters:selectedData.show_tag];
    
    if ([selectedData.selected integerValue] == 0)
    {
        if (![self checkForshowroomIDexistance:showroomName])
        {
            [showroomAddedArray addObject:showroomName];
            if ([selectedData.privacy_settings isEqualToString:@"on"]) {
                // && showroomAddedArray.count<7
                [arrayAddedPrivateShowroolList addObject:selectedData.show_tag];
            }
            selectedData.selected = @"1";
            
            if (!isSelectFromPopUp)
            {
                [self.yourShowTextView appandAStringWithEntityType:EntityTypeshowroom AndString:[NSString stringWithFormat:@"*%@",showroomName] andID:selectedData.showroom_id];
            }
            else
            {
                NSIndexPath *selection = [NSIndexPath indexPathForItem:selectedRowNo inSection:0];
                [self.showRoomCollectionview batchinsertCollectionView:selection];
                isSelectFromPopUp = NO;
            }
        }
    }
    else
    {
        selectedData.selected = @"0";
        [self.yourShowTextView removeShowroomNameInTweetTextView:[NSString stringWithFormat:@"*%@",[showroomName stringByReplacingOccurrencesOfString:@" " withString:@""]]];
        if ([selectedData.privacy_settings isEqualToString:@"on"]) {
            [arrayAddedPrivateShowroolList removeObject:selectedData.show_tag];
        }
        for (int i=0; i<[showroomAddedArray count]; i++)
        {
            if ([[showroomAddedArray objectAtIndex:i] isEqualToString:showroomName])
            {
                [showroomAddedArray removeObjectAtIndex:i];
            }
        }
    }
}





-(BOOL)checkForshowroomIDexistance:(NSString*)showroomID{
    BOOL status = NO;
    for (NSString *showID in showroomAddedArray) {
        if ([showID isEqualToString:showroomID]){
            return YES;
        }
    }
    return status;
    
}
-(BOOL)checkForHashtagIDexistance:(NSString*)hashtagID{
    BOOL status = NO;
    for (NSString *c_id in hashTagAddedArray) {
        if ([c_id isEqualToString:hashtagID]){
            return YES;
        }
    }
    return status;
    
}

-(BOOL)checkForUserIDexistance:(NSString*)userID{
    BOOL status = NO;
    for (NSString *c_id in userNameAddedArray) {
        if ([c_id isEqualToString:userID]){
            return YES;
        }
    }
    return status;
    
}


-(void)JitenCollectionViewAddClicked:(JitenCollectionView *)sender
{
    
}
-(JitenCollectionView *)showroomCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.showRoomCollectionview;
}

- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row
{
  
    ShowRoomModel *somemodel= [allShowRoomdata objectAtIndex:row];
    NSString *showrromName = [self getShowrromWithoutSpecialCharacters:somemodel.show_tag];
    if (![self checkForshowroomIDexistance:showrromName]) {
        somemodel.selected = @"0";
        [allShowRoomdata replaceObjectAtIndex:row withObject:somemodel];
    }
    return  somemodel;
}

- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView
{
    return allShowRoomdata.count;
}
-(void)didSelectOptionButtonForAddmint  : (UIButton *) selectedButton
{


    if (self.ysplayer) {
        [self.ysplayer stop];
        [self.ysplayer removeFromSuperview];
        [self ysPlayerFinishedPlayback:self.ysplayer];
    }

    
    [self.view endEditing:YES];
    switch (selectedButton.tag)
    {
        case 1:
            [self cameraClicked];
            break;
        case 2:
            [self GalleryClicked];
            break;
        case 3:
            [self VideoClicked];
            break;
        case 4:
            [self YouTubeClicked];
            break;
        default:
            break;
    }
}

-(NSInteger)DiscoverTableView:(PrinterestGridTable *)tableView selectedRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectdHeaderbuttonTag = [self checkHeaderButtonStatus];
    return selectdHeaderbuttonTag;
}

- (void)didSelectSelectOptionButtonClicked: (PrinterestGridTable *) sender forButton :(NSInteger) section
{
    
    
}

// **************  Need to add *************//     // New Method
-(NSInteger)checkHeaderButtonStatus
{
    if (isYouTubeVideoSelected) {
        selectdHeaderbuttonTag = 4;
    }
    else if (isNormalVideoSelected) {
        selectdHeaderbuttonTag = 3;
    }
    else if (isImageSelected) {
        selectdHeaderbuttonTag = 2;
    }
    else if (isCameraSelected) {
        selectdHeaderbuttonTag = 1;
    }
    else
    {
        selectdHeaderbuttonTag = 0;
    }
    [self.addmintTableview setButtonSelectdWithTag:selectdHeaderbuttonTag];
    
    return selectdHeaderbuttonTag;
}

-(void)cameraClicked{
    if([self.cam superview])
    {
        [self crossTapped:nil];
    }

    if (    isCameraIconTapped == NO) {
        AVCamViewController *cameraVC = [[AVCamViewController alloc]initWithNibName:@"AVCamViewController" bundle:nil];
        cameraVC.delegate = self;
        [self.navigationController pushViewController:cameraVC animated:NO];
        isCameraIconTapped = YES;
    }

}


-(void)GalleryClicked{
    
    
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status == PHAuthorizationStatusAuthorized) {
        // Access has been granted.
        isImageSelectFromCustomCamera = NO;
        ImageListViewController *ilist = [[ImageListViewController alloc]initWithNibName:@"ImageListViewController" bundle:nil];
        ilist.isShowCameraButton = YES;
        ilist.isMediaTypeImage = YES;
        ilist.delegate = self;
        [self presentViewController:ilist animated:YES completion:^{
            // Do some stuffs
        }];

    }
    
    else if (status == PHAuthorizationStatusDenied) {
        // Access has been denied.
        
        //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"app-settings:"]];
        [self cancelPhotoGallery];
              
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Allow Access to your Photos"
                                      message:@"This allows MintShow to access photos from your library."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [alert.view setTintColor:[UIColor blackColor]];
        
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Access"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"app-settings:"]];
                                 AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                                 [appDell authenticationDidFinish:self selectedInde:0];
                                 [alert dismissViewControllerAnimated:YES completion:nil];


                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Denie"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:cancel];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
            }
            
            else {
                // Access has been denied.
            }
        }];  
    }
    
   }

-(void)VideoClicked{

    if (IOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    if (isSmallPad) {
        self.cam = [[KZCameraView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 480+50) withVideoPreviewFrame:CGRectMake(0.0, 0.0, 320.0, 200.0)];
    }
    else
    self.cam = [[KZCameraView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height+100) withVideoPreviewFrame:CGRectMake(0.0, 0.0, 320.0, 320.0)];
    self.cam.backgroundColor = [UIColor colorWithRed:(9.0/255.0) green:(26.0/255.0) blue:(53.0/255.0) alpha:1.0];
    self.cam.maxDuration = 60.0;
    self.cam.delegate = self;
    self.cam.showCameraSwitch = YES;
    [VideoView addSubview:self.cam];
    [self.view addSubview:VideoView];
    [VideoView bringSubviewToFront:_viewTopSection];
}

-(void)YouTubeClicked{
    YoutubeVC* multimint = [[YoutubeVC alloc]initWithNibName:@"YoutubeVC" bundle:nil];
    [multimint.view setFrame:self.view.frame];
     multimint.delegate = self;
    [self displayContentController:multimint];
}





- (UIImage *)imageFromVideoURL :(NSURL *)videoURL{
    UIImage *image = nil;
    AVAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];;
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    NSTimeInterval durationInSeconds = 0.0;
    if (asset)
       durationInSeconds = CMTimeGetSeconds(asset.duration) ;
    NSLog(@"videoDuration: %.2f", durationInSeconds);
    // calc midpoint time of video
    Float64 durationSeconds = CMTimeGetSeconds([asset duration]);
    CMTime midpoint = CMTimeMakeWithSeconds(durationSeconds/2.0, 600);
    
    // get the image from
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef halfWayImage = [imageGenerator copyCGImageAtTime:midpoint actualTime:&actualTime error:&error];
    
    if (halfWayImage != NULL){
        // cgimage to uiimage
        image = [[UIImage alloc] initWithCGImage:halfWayImage];
        [dic setValue:image forKey:@"ImageThumbnail"];//kImage
        CGImageRelease(halfWayImage);
    }
    return image;
}


#pragma mark For Image FromLibrary
- (void)didTappedCancelButton
{
    selectdHeaderbuttonTag = 0;
    [self.addmintTableview setAllHeaderButtonNormal];
    [self checkHeaderButtonStatus];                   // Need to add
}

- (void)gallryTappedFromCustomCameraScreen{
    isImageSelectFromCustomCamera = YES;
    ImageListViewController *ilist = [[ImageListViewController alloc]initWithNibName:@"ImageListViewController" bundle:nil];
    ilist.isShowCameraButton = YES;
    ilist.isMediaTypeImage = YES;
    ilist.delegate = self;
    [self presentViewController:ilist animated:YES completion:^{
        // Do some stuffs
    }];
}

#pragma mark --
#pragma mark --PhotoGalleryDelegate
#pragma mark --

-(void)selectedImageFromGallery:(UIImage *)selectedImage{
    
    VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:selectedImage cropFrame:CGRectMake(0, 30.0,self.view.frame.size.width,360.0) limitScaleRatio:3.0];  //0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
    imgCropperVC.delegate = self;
    imgCropperVC.isFromCameraView = NO;
    [self presentViewController:imgCropperVC animated:NO completion:^{
        
    }];
}

-(void)selectedVideoFromGallery:(NSData *)moviedata AndURL:(NSURL *)localVideoUrl{

    [VideoView removeFromSuperview];

    sharedClass= [SPHSingletonClass sharedSingletonClass];
    sharedClass.remoteVideoURLString    = [localVideoUrl path];
    sharedClass.localVideopath          = localVideoUrl;
    sharedClass.selectVideoData = moviedata;
    [self setupVideoForURL:localVideoUrl];
}


-(void)setupVideoForURL:(NSURL *)localVideoUrl{
    self.anewModel.mintType       =   1;
    self.anewModel.vcellType      =   VideoCellTypeLocalPath;
    self.cam.delegate = nil;
    self.cam = nil;
    isNormalVideoSelected =YES;
    isImageSelected = NO;
    isYouTubeVideoSelected= NO;
    if (_isFromMintDetail)
        self.anewModel.VideoUrl = [NSURL URLWithString:[self getUrlFromMediaID:_feedModelData.media_id]];
    else {
        self.anewModel.VideoUrl = localVideoUrl;
        [self.addmintTableview reloadData];
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AVAsset *asset = [AVAsset assetWithURL:localVideoUrl];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        CMTime time = CMTimeMake(0, 1);
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.anewModel.thumbImage = thumbnail;
            CGImageRelease(imageRef);
            
        });
    });
}


-(void)cancelPhotoGallery{
    [self.addmintTableview setAllHeaderButtonNormal];
    [self checkHeaderButtonStatus];                   // Need to add

}

-(void)cameraImageTappedforPhotoGallery {
    [self cancelPhotoGallery];
    [self cameraClicked];
}

#pragma mark - VPImageCropperViewControllerDelegate
- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController{
    
}


- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage{

    isImageSelected = YES;
    isCameraSelected=NO;               // Need to add

    isNormalVideoSelected = NO;
    isYouTubeVideoSelected = NO;
    sharedClass.selectedImage = editedImage;
    [self.addmintTableview reloadData];
    
   // [self.addmintTableview setButtonSelectdWithTag:selectdHeaderbuttonTag];

    NSArray *viewControllers1 = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers1 count];i++){
        id obj=[viewControllers1 objectAtIndex:i];
        if([obj isKindOfClass:[AddMintViewController class]]){
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
    self.anewModel.thumbImage = sharedClass.selectedImage;
    [self postDataToURl:nil];
}




#pragma mark--

- (IBAction)crossTapped:(UIButton *)sender{
   // [self.cam removeFromSuperview];
    [self.cam removeDeleteUseVideo];
    [self.cam removeSubView];
    self.cam.delegate = nil;
    self.cam = nil;
    [VideoView removeFromSuperview];
    [self checkHeaderButtonStatus];                   // Need to add
    
}

- (IBAction)frontCameraTapped:(UIButton *)sender{
    [self.cam switchCamera];
}

- (void)videoGallerySelectedFromKZCameraView{
    ImageListViewController *vcVideoGal = [[ImageListViewController alloc]initWithNibName:@"ImageListViewController" bundle:nil];
    vcVideoGal.isShowCameraButton = YES;
    vcVideoGal.isMediaTypeImage = NO;
    vcVideoGal.delegate = self;
    [self  presentViewController:vcVideoGal animated:YES completion:^{
        
    }];
}

- (void)useVideoSelectedFromKZCameraView
{
    [self.cam saveVideoWithCompletionBlock:^(BOOL success,NSURL *outPUTurl){
         if (success) {
             NSLog(@"url is %@",outPUTurl);
             NSLog(@"WILL PUSH NEW CONTROLLER HERE");
             self.cam.delegate = nil;
             self.cam = nil;

             [VideoView removeFromSuperview];
             
             isNormalVideoSelected  = YES;
             isImageSelected        = NO;
             isCameraSelected       =NO;               // Need to add
             isYouTubeVideoSelected = NO;
             
             self.anewModel.mintType = 1;
             self.anewModel.vcellType = VideoCellTypeLocalPath;
             sharedClass.localVideopath = outPUTurl;
             sharedClass.selectVideoData  = [NSData dataWithContentsOfURL:sharedClass.localVideopath];
             self.anewModel.VideoUrl = outPUTurl;
             [self.addmintTableview reloadData];
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 AVAsset *asset = [AVAsset assetWithURL:sharedClass.localVideopath];
                 AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
                 CMTime time = CMTimeMake(0, 1);
                 CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
                 UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self.anewModel.thumbImage = thumbnail;
                     CGImageRelease(imageRef);
                     
                 });
             });
         }
     }];
}



- (IBAction)cameraRollClosed:(UIButton *)sender
{
    [arrayVideos removeAllObjects];
    arrayVideos = nil;
    [viewPhoto removeFromSuperview];
}

- (void)didPostMintButtonClicked: (PrinterestGridTable *) sender
{
    // Post mint Clicked
}

- (void)didLocationButtonClicked: (PrinterestGridTable *) sender
{
//    nearLocationTableView = [[NearLocation alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds];
//    nearLocationTableView.delegate = self;
    
    MapViewController *pickit = [[MapViewController alloc]initWithNibName:@"MapViewController" bundle:nil];
    //pickit.hidesBottomBarWhenPushed=YES;
    pickit.delegate= self;
    [self.navigationController pushViewController:pickit animated:YES];

}

-(void)aaddressPickingDidFinish:(LocationModel *)searchedLocation {
    NSLog(@"picked address = %@",searchedLocation.searchedAddress);
//    currentlongitude = [searchedLocation.longitude floatValue];
//    currentlatitude = [searchedLocation.latitude floatValue];
    sharedClass.selectedLocation.yaacPlaceName = searchedLocation.searchedAddress;
    [self.addmintTableview setLocation:searchedLocation.searchedAddress withSection: (isCameraSelected||isImageSelected ||isNormalVideoSelected||isYouTubeVideoSelected)?2:1];
    // sharedClass.selectedLocation = sender;
}

-(UIImage *)takeScreenShot{
    CGSize screenSize = [[UIScreen mainScreen] applicationFrame].size;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(nil, screenSize.width, screenSize.height, 8, 4*(int)screenSize.width, colorSpaceRef, kCGImageAlphaPremultipliedLast);
    CGContextTranslateCTM(ctx, 0.0, screenSize.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    
    [(CALayer*)self.view.layer renderInContext:ctx];
    
    CGImageRef cgImage = CGBitmapContextCreateImage(ctx);
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    CGContextRelease(ctx);
    
    return image;
}



- (void)didCategoryButtonClicked:(PrinterestGridTable *)sender
{
    [self.view endEditing:YES];
    filterTableView = [[FilterListTable alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds withTitle:@"Choose Mint Type" isCateGoryView:YES withPreviousSelection:([sharedClass.categoryname isEqualToString:@"Choose Mint Type"])?nil:sharedClass.categoryname cateGoryId:nil showcategoryList:NO];
    filterTableView.delegate = self;
}



- (void) NearLocationTableDelegateMethod: (LocatioDetails *) sender stringWithCustomText :(NSString *) customLocation;
{
    sharedClass.selectedLocation = (sender)?sender:sharedClass.selectedLocation;
   // sharedClass.selectedLocation = sender;
    
    if ([customLocation length])
    {
        sharedClass .selectedLocation.yaacPlaceName =customLocation;
        [self.addmintTableview setLocation:customLocation withSection: (isCameraSelected||isImageSelected ||isNormalVideoSelected||isYouTubeVideoSelected)?2:1];
    }
    else
        [self.addmintTableview setLocation:sender.yaacPlaceName withSection: (isCameraSelected||isImageSelected ||isNormalVideoSelected||isYouTubeVideoSelected)?2:1];

    [nearLocationTableView hideDropDown];
}

-(void)FilterListTableDelegateMethod:(NSString *)sender selectedIndex:(NSInteger)selectedRowNo
{
    
}

- (void) FilterListTableDelegateMethod: (NSString *) textData selectedId :(NSString *) iddee;
{
    sharedClass.categoryname = textData;
    sharedClass.categoryIds = iddee;
    [self.addmintTableview setCategoryText:textData withSection: (isCameraSelected||isImageSelected ||isNormalVideoSelected||isYouTubeVideoSelected)?2:1];

    [filterTableView hideDropDown];
    [self.addmintTableview reloadData];
}


-(NSInteger)getCount:(NSString*)str{
    NSUInteger count = 0, length = [str length];
    NSRange range = NSMakeRange(0, length);
    while(range.location != NSNotFound)
    {
        range = [str rangeOfString: @"*" options:0 range:range];
        if(range.location != NSNotFound)
        {
            range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
            count++; 
        }
    }
    return count;
}

-(NSMutableArray *)getAllShowroomNames
{
    NSMutableArray *allShowrooms = [NSMutableArray new];
    NSString *serverString = self.yourShowTextView.textView.text;
    NSArray *starArray = [serverString componentsSeparatedByString:@" "];
    for(NSString *starttsring in starArray){
        if ([starttsring rangeOfString:@"*"].location == NSNotFound) {
            continue;
        } else {
            if ([self getCount:starttsring]>1) {
                NSString *newstring = [starttsring substringFromIndex:1];
                NSArray *newstarArray = [newstring componentsSeparatedByString:@"*"];
                for(NSString *astring in newstarArray){
                    NSLog(@"astring =*%@",astring);
                    [allShowrooms addObject:[NSString stringWithFormat:@"*%@",astring]];
                }
            }else{
                [allShowrooms addObject:starttsring];
            }
        }
    }
    return allShowrooms;
}





-(void)postEditMintData {
   // [self getAllShowroomNames];
    //    [self.yourShowTextView determineYSWordsForAddmint:feedModelData.feed_text];
    NSLog(@"%@",self.yourShowTextView.textView.text);
    NSString *serverString = self.yourShowTextView.textView.text;
    serverString = [serverString stringByReplacingOccurrencesOfString:@"](contact:(null))" withString:@""];
    
    
    NSString *finalPost =(isDescriptionContentPrivateShowroom)? [NSString stringWithFormat:@"%@%@",[YSSupport stringByReplacingEmoji:serverString withDict:self.emojiDict],strAllShowroomsForLaterUse]:[YSSupport stringByReplacingEmoji:serverString withDict:self.emojiDict];
    
    if( (isImageSelected == YES) || (isYouTubeVideoSelected == YES) || (isNormalVideoSelected == YES) ){
        if([finalPost isEqualToString:@"Describe it positive..."]){
            finalPost = @"";
        }
        
//        if (![serverString length]) {
//            return;
//        }
    }
    else
    {
        NSLog(@"text = %@",finalPost);
        if([finalPost isEqualToString:@"Describe it positive..."]){
            showAlert(@"Please add at least 3 characters", nil, @"OK", nil);
            return;
        }
        
        else if ([finalPost length] < 3 && ![finalPost isEqualToString:@"Describe it positive..."] )  {
            showAlert(@"Please add at least 3 characters", nil, @"OK", nil);
            return;
        }
    }
    
 
   
        NSLog(@"text = %@",finalPost);
        //finalPost = [self escapedValue:finalPost];

        NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&mint_id=%@&mint_desc=%@",GETVALUE(CEO_AccessToken),_feedModelData.feed_id,[YSSupport escapedValue:finalPost]] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *request = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
        NSLog(@"request is %@",request);
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
        [YSAPICalls EditMint:submitData ForSuccessionBlock:^(id newResponse) {
            [self.addmintTableview reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            Generalmodel *gmodel = newResponse;
            if ([gmodel.status_code integerValue])  {
                NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:finalPost,@"description",nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:KMINTEDITED object:nil userInfo:dict ];
                isImageSelected        = NO;
                isYouTubeVideoSelected = NO;
                isNormalVideoSelected  = NO;
                sharedClass.selectedImage = nil;
                [self setTabar];
            }else{
//                showAlert(gmodel.status_Msg, nil, @"OK", nil);
                showAlert(@"Server Error", nil, @"OK", nil);

            }
        } andFailureBlock:^(NSError *error){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
}

- (IBAction)postMint:(UIButton *)sender{
    isPostMintClicked = YES;
    if ([arrayAddedPrivateShowroolList count]>1) {
        NSMutableArray *arrTemp = [[NSMutableArray alloc]init];
        NSArray*arrStarArray =   [self.yourShowTextView getAllShowRoomArrayFromTextView];
        for (NSString *strPrivateShowroom in arrayAddedPrivateShowroolList) {
            for (NSString *starTagArray in arrStarArray)  {
                if ([strPrivateShowroom isEqualToString:starTagArray])  {
                    [arrTemp addObject:strPrivateShowroom];
                }
            }
        }
        [self showAlertForPrivateShowroom:arrTemp];
        return;
    }
    
    if (_isFromMintDetail) {
        [self postEditMintData];
        return;
    }
    NSString *serverString = self.yourShowTextView.textView.text;
    NSLog(@"lenght is %d", [_totaltextCounter.text intValue]);
   
    
    serverString= [serverString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    if ([sharedClass.categoryname length] == 0 ||[sharedClass.categoryname isEqualToString:@"Choose Mint Type"] )  {
        showAlert(@"Please Select a Mint Type", nil, @"OK", nil);
    } else  {
        if (isImageSelected == YES || isCameraSelected==YES)  {
            self.anewModel.thumbImage = sharedClass.selectedImage;
            self.anewModel.mintType = 0;
            [self postDataToURl:nil];
        } else if (isYouTubeVideoSelected == YES)  {
            
            [self youtubeUpload];
        }  else if  (isNormalVideoSelected == YES)  {
            NSLog(@"path is %@",[self.anewModel.VideoUrl absoluteString ]);
            [self postDataToURl:sharedClass.selectVideoData];
        } else {
            
            NSString *finalPost = [YSSupport stringByReplacingEmoji:serverString withDict:self.emojiDict];
            NSLog(@"text = %@",finalPost);
            if([finalPost isEqualToString:@"Describe it positive..."]){
                showAlert(@"Please add at least 3 characters", nil, @"OK", nil);
                return;
            }
            
            else if ([finalPost length] < 3 && ![finalPost isEqualToString:@"Describe it positive..."] )  {
                showAlert(@"Please add at least 3 characters", nil, @"OK", nil);
                return;
            }
            
            
            finalPost = [YSSupport escapedValue:finalPost];

            NSString *locationName = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?@"":[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName;
            NSString *latituteVal = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacLatitute:@"0.0";
            NSString *longituteVal = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacLongitute:@"0.0";
            
                if ([sharedClass.categoryname isEqualToString:@"Choose Mint Type"]){
                    showAlert(@"Please Select a Mint Type", nil, @"OK", nil);
                    return;
                }
            
            NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&mint_text=%@&category_id=%@&type_id=%@&custom_mint_text=%@&achievement_location_text=%@&lat=%@&lan=%@",GETVALUE(CEO_AccessToken),finalPost,sharedClass.categoryIds,@"12",@"customMintTxt",locationName,latituteVal,longituteVal] dataUsingEncoding:NSUTF8StringEncoding];
            NSString *request = [[NSString alloc]initWithData:submitData encoding:NSASCIIStringEncoding];
            NSLog(@"request is %@",request);
            //   [ProgressHUD show:@"loading"];`
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self postMintToServer:submitData textMint:YES];
        }
    }
}




// remove Video URL from Device
- (void) removeFile:(NSURL *)fileURL{
    NSString *filePath = [fileURL path];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        NSError *error;
        if ([fileManager removeItemAtPath:filePath error:&error] == NO) {
        }
    }
}
-(BOOL)ValidateMintPost{
    return YES;
}

- (void) YoutubeVCDelegateMethod:(NSString *) seectedStrings{

    isYouTubeVideoSelected  =   YES;
    self.anewModel.mintType       =   2;
    self.anewModel.vcellType      =   VideoCellTypeYoutube;
    isNormalVideoSelected   =   NO;
    isImageSelected         =   NO;
    sharedClass.selectedImage = [UIImage imageNamed:@"placeholderImage"];
    [self.addmintTableview reloadData];
    
    NSString *vID = nil;
    if (_isFromMintDetail)
        vID = seectedStrings;
    else
        vID = [seectedStrings componentsSeparatedByString:@"="][1];

    
    NSLog(@"fetched string is %@",seectedStrings);
    NSLog(@"YOUTUBE %@", [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",vID]);
    NSURL *youtubeUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",vID]];
    selectedYoutubeUrl = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",vID];
    [HCYoutubeParser thumbnailForYoutubeURL:youtubeUrl thumbnailSize:YouTubeThumbnailDefaultMedium completeBlock:^(UIImage *image, NSError *error)  {
         if (!error) {
             dispatch_async(dispatch_get_main_queue(), ^{
                  sharedClass.selectedImage=image;
                  self.anewModel.thumbImage = image;

                 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                 [self.addmintTableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
             });
             
             [HCYoutubeParser h264videosWithYoutubeURL:youtubeUrl completeBlock:^(NSDictionary *videoDictionary, NSError *error) {
               
                 NSDictionary *qualities = videoDictionary;
                 
                 NSString *URLString = nil;
                 if ([qualities objectForKey:@"small"] != nil)  {
                     URLString = [qualities objectForKey:@"small"];
                 } else if ([qualities objectForKey:@"live"] != nil) {
                     URLString = [qualities objectForKey:@"live"];
                 }  else  {
                     [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"This YouTube video is restricted to watch out side you tube." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
                     return;
                 }
                 sharedClass.remoteVideoURLString = URLString;
                 self.anewModel.VideoUrl = [NSURL URLWithString:URLString];
                 [self.addmintTableview reloadData];
             }];
             
         }
         else
         {
             NSLog(@"error %@",error);
         }
     }];
}

-(void)youtubeUpload {
 // [ProgressHUD show:@"loading"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *serverString = self.yourShowTextView.textView.text;
    NSString *finalPost = [YSSupport stringByReplacingEmoji:serverString withDict:self.emojiDict];
    NSLog(@"text = %@",finalPost);
    NSString *locationName = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?@"":[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName;
    NSString *latituteVal = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacLatitute:@"0.0";
    NSString *longituteVal = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacLongitute:@"0.0";
    
    if([finalPost isEqualToString:@"Describe it positive..."]){
      finalPost = @"";
    }

    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&achievement_text=%@&achievement_category_id=%@&achievement_youtube_code=%@&achievement_location_text=%@&lat=%@&lon=%@",GETVALUE(CEO_AccessToken),finalPost,sharedClass.categoryIds,selectedYoutubeUrl,locationName,latituteVal,longituteVal] dataUsingEncoding:NSUTF8StringEncoding];
       NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@achievements/post_youtubemint_achievement",ServerUrl]]];
    NSString *request = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"request is %@",request);
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    [NSURLConnection sendAsynchronousRequest:submitrequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
        // [ProgressHUD dismiss];
         [MBProgressHUD hideHUDForView:self.view animated:YES];

         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
             NSLog(@"json data=%@",json);
             if ([[NSString stringWithFormat:@"%@",[json objectForKey:@"status"]] isEqualToString:@"true"])
             {
                 [self.addmintTableview reloadData];
                 NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"data",@"addmint",@"Youtube",@"type",[json objectForKey:@"mintdetail_url"],@"mintdetail_url",nil];
                 [[NSNotificationCenter defaultCenter] postNotificationName:KMINTADDED object:nil userInfo:dict];
                // [self shareFacebook:@""];
                 SETVALUE(@"yes", CEO_ISMINTALREADYPOSTED);
                 SYNCHRONISE;
                 
                 NSString *mintCount = [NSString stringWithFormat:@"%d",[(([GETVALUE(PUSHMINTCOUNT) intValue] == -1)?@"0":GETVALUE(PUSHMINTCOUNT)) intValue]+1];
                 
                 SETVALUE(mintCount,PUSHMINTCOUNT);
                 SYNCHRONISE;

                 
                 isImageSelected = NO;
                 isYouTubeVideoSelected= NO;
                 isNormalVideoSelected = NO;

                 sharedClass.selectedImage = nil;
                 
                 [self setTabar];
             }
             else{
                 showAlert(@"Server Error!",@"please try again", @"OK", nil);}
         }  else  {
               showAlert(@"Server Error!",@"please try again", @"OK", nil);         }
     }];

}

- (void) displayContentController: (UIViewController*) content;{
    [self addChildViewController:content];
    [content willMoveToParentViewController:self];
    [content didMoveToParentViewController:self];
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
}



- (void)cameraTappeddidFinished:(id)editedImage{

    isImageSelected = NO;
    isCameraSelected = YES;
    isNormalVideoSelected = NO;
    isYouTubeVideoSelected = NO;
    

    self.anewModel.thumbImage = editedImage;
    sharedClass.selectedImage = editedImage;
    [self.addmintTableview reloadData];
}

- (void)videoTappedFromCustomCameraScreen{
    NSArray *viewControllers1 = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers1 count];i++){
        id obj=[viewControllers1 objectAtIndex:i];
        if([obj isKindOfClass:[AddMintViewController class]]){
            [[self navigationController] popToViewController:obj animated:YES];
            [self VideoClicked];
            return;
        }
    }
}





-(void)setTabar
{
    [self setPlaceHolder];

    self.totaltextCounter.text = @"300";
    isFirsttime = YES;
//    [self.yourShowTextView resetTextView];
    
    if (self.ysplayer) {
        [self.ysplayer stop];
        [self.ysplayer removeFromSuperview];
    }
    
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    CGRect frame = self.view.frame;
    self.view.frame = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,
                                 frame.size.height - 49);
    
    //delegateApp.ysTabBarController.tabBar.hidden = NO;
    if (!_isFromMintDetail) {
        delegateApp.ysTabBarController.selectedIndex = 0;
        [delegateApp changeBackgroundOfTabBarForIndex:0];

    }
    [self.navigationController popViewControllerAnimated:NO];

}


#pragma mark------
#pragma mark -- SERVER CALL METHODS --
#pragma mark------
-(void)postDataToURl:(NSData *)imgData{
    
    self.anewModel.dataGaleryVideo = imgData;
    NSString *placeName = [NSString stringWithFormat:@"%@",[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName];
    NSString *latitute = nil;
    NSString *longitute = nil;
    if ([placeName length]) {
        latitute = [NSString stringWithFormat:@"%@",[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacLatitute];
        longitute = [NSString stringWithFormat:@"%@",[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacLongitute];
    }
    //    NSString *category = [[SPHSingletonClass sharedSingletonClass] categoryIds];
    
    NSString *finalPost = [YSSupport stringByReplacingEmoji:self.yourShowTextView.textView.text withDict:self.emojiDict];
    
    NSString *locationName = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?@"":[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName;
    locationName = [locationName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSString *latituteVal = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacLatitute:@"0.0";
    NSString *longituteVal = ([[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacPlaceName isEqualToString:@"Name This Location"])?[[SPHSingletonClass sharedSingletonClass] selectedLocation].yaacLongitute:@"0.0";
    
    if([finalPost isEqualToString:@"Describe it positive..."]){
        finalPost = @"";
    }
   
    finalPost = [finalPost stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    /*
     access_token=f9984ccf0a9e6499f7527f40373eeb2bf1c5713b&achievement_text=Hello%20All%20Please%20Join%20This%20Showroom%20*Random,%20*peace%20And%20at%20*ROBOT&achievement_category_id=22&mint_type=image
     */
    
    self.anewModel.mintDataStr =  [NSString stringWithFormat:@"access_token=%@&achievement_text=%@&achievement_category_id=%@&mint_type=image&achievement_location_text=%@&achievement_lat=%@&achievement_long=%@", GETVALUE(CEO_AccessToken),finalPost,sharedClass.categoryIds,locationName,latituteVal,longituteVal];
    
    //[self shareFacebook:@""];
    SPHSingletonClass *singleTon = [SPHSingletonClass sharedSingletonClass];
    singleTon.selectedPostMintImage = _anewModel.thumbImage;
    [self.delegate postMintDidClicked:self.anewModel];
    
    isImageSelected        = NO;
    isYouTubeVideoSelected = NO;
    isNormalVideoSelected  = NO;
    [self setTabar];
}



-(void)postMintToServer : (NSData *)submitData  textMint : (BOOL) isTextType{
    
    [YSAPICalls AddMint:submitData textMint:isTextType ForSuccessionBlock:^(id newResponse) {
        [self.addmintTableview reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        Generalmodel *gmodel = newResponse;
        if ([gmodel.status_code integerValue])  {
            
            SPHSingletonClass *singleTon = [SPHSingletonClass sharedSingletonClass];
            singleTon.selectedPostMintImage = nil;
            SETVALUE(@"yes", CEO_ISMINTALREADYPOSTED);
            SYNCHRONISE;
            //[self shareFacebook:@""];
            
            NSString *mintCount = [NSString stringWithFormat:@"%d",[(([GETVALUE(PUSHMINTCOUNT) intValue] == -1)?@"0":GETVALUE(PUSHMINTCOUNT)) intValue]+1];
            SETVALUE(mintCount,PUSHMINTCOUNT);
            SYNCHRONISE;
            

            
            
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"data",@"addmint",@"Text",@"type",gmodel.mintdetail_url,@"mintdetail_url",nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:KMINTADDED object:nil userInfo:dict];
            isImageSelected        = NO;
            isYouTubeVideoSelected = NO;
            isNormalVideoSelected  = NO;
            sharedClass.selectedImage = nil;
            [self setTabar];
        }else{
           // showAlert(gmodel.status_Msg, nil, @"OK", nil);
            showAlert(@"Server Error!", @"please try again", @"OK", nil);

        }
    } andFailureBlock:^(NSError *error){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}
-(void)gotoDiscoverShowroomSection
{
    [self setPlaceHolder];
    sharedClass.descriptionText     = @"Describe it positive...";
    self.totaltextCounter.text = @"300";
    isFirsttime = YES;
    
    if (self.ysplayer) {
        [self.ysplayer stop];
        [self.ysplayer removeFromSuperview];
    }
    
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    CGRect frame    = self.view.frame;
    self.view.frame = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,
                                 frame.size.height - 49);
    
    delegateApp.ysTabBarController.selectedIndex = 1;
    [delegateApp changeBackgroundOfTabBarForIndex:1];

}

#pragma mark------
#pragma mark -- FACEBOOK SHARING --
#pragma mark------

- (IBAction)signUpWithFacebook:(UIButton *)sender {

    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email"]
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    if ([FBSDKAccessToken currentAccessToken]) {
                                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name,email"}]
                                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                             if (!error) {
                                                 NSLog(@"fetched user:%@", result);
                                             }
                                         }];
                                    }
                                }
                            }];
}

-(void)shareFacebook : (NSString *)mediaId {
    
  //  [self signUpWithFacebook:nil];
    if ([FBSDKAccessToken currentAccessToken]) {
        if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
            if (isImageSelected == YES) {
               UIImage *someImage = sharedClass.selectedImage;
                FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
                content.photos = @[[FBSDKSharePhoto photoWithImage:someImage userGenerated:YES] ];
                [FBSDKShareAPI shareWithContent:content delegate:self];
                
            } else if (isYouTubeVideoSelected == YES) {
                FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                content.contentURL = [NSURL URLWithString:selectedYoutubeUrl];
                content.contentDescription = sharedClass.descriptionText;
                [FBSDKShareAPI shareWithContent:content delegate:self];
                
            }  else if  (isNormalVideoSelected == YES) {
                FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
                content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@.mp4",VIDEO_DIRECT_URL,mediaId]];
                [FBSDKShareAPI shareWithContent:content delegate:self];
                
            }
            else{
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/feed"  parameters: @{ @"message" : @"jitendra posted this mint for Mintshow app"}  HTTPMethod:@"POST"]  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (!error) {
                        NSLog(@"Post id:%@", result[@"id"]);
                    }
                }];
            }
        }
    
        else{
        
            NSLog(@"NO PUBILSH STILL YET");
        
        }
    }
}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results

{
      NSLog(@"sharing result %@",results);
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"error result %@",error);
}
- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    
}



-(void)resetShowroomSelction
{
    for (int i=0; i<allShowRoomdata.count; i++)
    {
        ShowRoomModel *showModel = [allShowRoomdata objectAtIndex:i];
        if ([showModel.selected isEqualToString:@"1"])
        {
            showModel.selected = @"0";
            [allShowRoomdata replaceObjectAtIndex:i withObject:showModel];
            [showroomAddedArray removeObject:showModel.show_tag];
        }
    }
    [self batchreloadCollectionView];
}

-(void)checkShowroomSelectedOrNot:(NSArray *)arrTextViewStarArray
{
    [self resetShowroomSelction];
    
    if ([arrTextViewStarArray count]>0)
    {
        for (NSString *strShowroom in arrTextViewStarArray)
        {
            for (int i=0; i<allShowRoomdata.count; i++)
            {
                ShowRoomModel *showModel = [allShowRoomdata objectAtIndex:i];
                if ([strShowroom isEqualToString:[self getShowrromWithoutSpecialCharacters:showModel.show_tag]])
                {
                    isSelectFromPopUp = YES;
                    [self JitenCollectionViewDelegateMethod:showModel selectedIndex:i];
                    showModel.selected = @"1";
                }
                [allShowRoomdata replaceObjectAtIndex:i withObject:showModel];
                
            }
        }
        [self batchreloadCollectionView];
    }
}
-(void)KeyboardShowingInAddMint:(NSNotification *)notification
{
    isKeyBoardShown = YES;
}

-(void)KeyboardHidingInAddMint:(NSNotification *)notification
{
    if (isKeyBoardShown)
    {
        isKeyBoardShown = NO;
        NSArray *arrTextViewStarArray =   [self.yourShowTextView getAllShowRoomArrayFromTextView];
        [self checkShowroomSelectedOrNot:arrTextViewStarArray];
    }
}


@end





