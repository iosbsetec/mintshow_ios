//

//

#import "CommentsViewController.h"
#import "CommentsCell.h"
#import "AppDelegate.h"
#import "Generalmodel.h"
#import "CommentModel.h"
#import "YSSupport.h"
#import "MBProgressHUD.h"
#import "YSAPICalls.h"
#import "NSString+Emojize.h"
#import "AGEmojiKeyboardView.h"
#import "MSProfileVC.h"
#import "MSMintDetails.h"

#import "POPupTableView.h"
#import "TSPopoverController.h"
#import "EntitySupport.h"


#define ACC 1e-7

typedef enum showRoomSelectionType
{
    MSGSHOWROOM_NOTSELECTD,
    MSGSHOWROOM_PUBLICSELECTD,
    MSGSHOWROOM_PRIVATESELECTD,
} MSGShowroomType;


@interface CommentsViewController ()<UITextFieldDelegate,POPUpTableViewDataSource,POPUpTableDelegate>
{
    IBOutlet UITableViewCell *cellCommentAdd;
    IBOutlet UILabel         *lblUserName;
    IBOutlet UILabel         *lblTime;
    IBOutlet UILabel         *lblcomment;
    
    NSInteger seletedCell,buttonTag;
    BOOL isEditedCell;
    AppDelegate *delegateApp ;
    BOOL isFirstTime;

    
    
    NSMutableArray *dataArray;
    
    NSMutableArray *showroomAddedArray;
    NSMutableArray *hashTagAddedArray;
    NSMutableArray *userNameAddedArray;
    
    BOOL isPrivateShowroomSelectd;
    NSString *privateShowRoomId;
    NSString *privateShowRoomName;
    
    TSPopoverController *popoverController;
    POPupTableView *apopupTable;
    EntityType selectedEntitytype;
    NSRange selectedRange;
    NSString *selectedFeature;
    
    MSGShowroomType theShowroomSelectState;
    
    
    NSMutableArray *substrings;
    
   
    __weak IBOutlet UIView *headerView;
    IBOutlet UIView *viewOptionHolder;
    IBOutlet UIView *viewOptions;
    
    int pageNumber;
    BOOL isDataFinishedFromServer;
    BOOL isAPICalling;
}
@property (nonatomic ,assign) NSString *strCommentCreatedTime;
@property (nonatomic ,assign) BOOL isSendTapped;
@property (nonatomic ,assign) BOOL isEditBtnTapped;
@property (nonatomic ,assign) NSInteger   counter;
@property(nonatomic,retain)   IBOutlet UILabel *lblHeaderTitle;
@property (nonatomic, strong) UIFont *customFont;
@property (nonatomic, strong)  NSDictionary *emojiDict;
@end

@implementation CommentsViewController
@synthesize  tblComment;
@synthesize  isSendTapped;
@synthesize  arrAllCommentsDetails;
@synthesize  strCommentTextValue;
@synthesize  strPhotoId;
@synthesize  counter;
@synthesize  isEditBtnTapped;
@synthesize  strCommentById,strCommentCreatedTime;
@synthesize rowId,commentArray;
@synthesize commonString;


//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
       UIBezierPath *maskPath  = [UIBezierPath bezierPathWithRoundedRect:headerView.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight                                                         cornerRadii:CGSizeMake(10.0, 10.0)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame         = headerView.bounds;
        maskLayer.path          = maskPath.CGPath;
        headerView.layer.mask    = maskLayer;
    

    
    
  isFirstTime = YES;
    pageNumber = 1;
    
    
    [_lblHeaderTitle setFont:[UIFont fontWithName:@"LithosPro-Regular" size:25]];
     btnEdit.hidden=YES;
     btnSend.highlighted=NO;
     btnSend.userInteractionEnabled=NO;
    arrAllCommentsDetails = [NSMutableArray array];
    [txtAddComment setClearsOnBeginEditing:NO];
    [imageEgoforUser.layer  setCornerRadius:5.0f];
    [imageEgoforUser.layer  setBorderWidth:1];
    [imageEgoforUser.layer  setBorderColor:[UIColor grayColor].CGColor];
    [imageEgoforUser.layer  setMasksToBounds:YES];
    [imageEgoforUser	     setClipsToBounds:YES];
    tblComment.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    [tblComment addGestureRecognizer:longPressRecognizer];
    
    
    substrings = [NSMutableArray new];

    self.emojiDict = [YSSupport reverseEmojiAliases];
    
    viewOptions.layer.cornerRadius = 4.0;
}


- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.composeView!=nil)
    {
        [self.composeView setHidden:NO];
    }
    
    if (_commentCount > 0 && isFirstTime)
    {
        [self getAllComplimints:pageNumber];
        isFirstTime = NO;
    }
    
    // [self setupViewHeightframe];
    [txtAddComment becomeFirstResponder];
}


- (void)prefix_addUpperBorder
{
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColor greenColor] CGColor];
    upperBorder.frame = CGRectMake(0, CGRectGetHeight(self.view.frame) - 4.0, CGRectGetWidth(self.view.frame), 4.0);
    [self.view.layer addSublayer:upperBorder];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self performSelector:@selector(onShowMsgComposeView) withObject:nil afterDelay:0.5];

     NSLog(@"viewWillAppear");
}
//
- (void)viewWillDisappear:(BOOL)animated{
    
    [self.composeView.messageTextView resignFirstResponder];
    [self.composeView removeFromSuperview];
    self.composeView = nil;
    NSLog(@"view dissapear");
}


-(IBAction)onOptionButtonTapped:(id)sender
{
    UIButton *btn = sender;
    if (btn.tag==1) {
        [self showeditCommentInfo];
    }
    else if (btn.tag==2)
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Delete this CompliMint?"
                                      message:@"Your compliMint will be deleted."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [alert.view setTintColor:[UIColor blackColor]];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [self deleteRecord];
                                 [self.composeView setUserInteractionEnabled:YES];

                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Keep"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self.composeView setUserInteractionEnabled:YES];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:cancel];
        [alert addAction:ok];

        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else if (btn.tag==3) {
        [self.composeView setUserInteractionEnabled:YES];
        [tblComment beginUpdates];
        [tblComment reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:seletedCell inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
        [tblComment endUpdates];

    }
    [viewOptionHolder removeFromSuperview];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [alertView dismissWithClickedButtonIndex:1 animated:YES];
        [self deleteRecord];
    }
}

-(void)onShowMsgComposeView
{
    if (self.composeView==nil)
    {
        delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
        self.composeView = [[MessageComposerView alloc]initWithFrame:CGRectMake(0, 500, self.view.frame.size.width, 50)];
        self.composeView.delegate = self;
        [self.composeView.messageTextView setTextColor:[UIColor lightGrayColor]];
        self.composeView.messageTextView.autocorrectionType  = UITextAutocorrectionTypeDefault; 
       [self.composeView.messageTextView performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
        [self.composeView.sendButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [delegateApp.window addSubview:self.composeView];
        [self createTableAndPopupView];
    }
}

#pragma mark - *********** @ # * Feature Implementation *************

// BELOW CODES BY SIBA PRASAD HOTA FOR DESCRIPTION
// DONT CHANGE ANYTHING WITHOUT ASKING

-(void)createTableAndPopupView
{
    apopupTable = [[POPupTableView alloc]initWithFrame:CGRectMake(0, 0, 250, 200)];
    apopupTable.popupDataSource=self;
    apopupTable.popupDelegate = self;
    
    popoverController = [[TSPopoverController alloc] initWithView:apopupTable];
    popoverController.cornerRadius = 5;
    //    popoverController.titleText = @"change order";
    popoverController.popoverBaseColor = [UIColor lightGrayColor];
    popoverController.popoverGradient= NO;
    
}

-(void)createTableviewAndShowPopup:(EntityType)textSuggestType
{
    apopupTable.suggestType = textSuggestType;
   
    [popoverController showPopoverWithRect:CGRectMake(0,0,300,100)];
    [apopupTable reloadData];
    
}

//- (UILabel *)addMintCounterLabelforRow:(NSInteger)row
//{
//    return  self.totaltextCounter;
//}
- (NSInteger)rowsForBubbleTable:(POPupTableView *)tableView
{
    tableView.separatorStyle= (dataArray.count>0)?UITableViewCellSeparatorStyleSingleLine:UITableViewCellSeparatorStyleNone;
    
    return dataArray.count;
}
- (FeatureModel*)bubbleTableView:(POPupTableView *)tableView dataForRow:(NSInteger)row;
{
    return [dataArray objectAtIndex:row];
}

-(NSString *)getShowrromWithoutSpecialCharacters : (NSString *) showroomData
{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    NSString *showroomName = [[showroomData componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    return showroomName;
}

- (void) PopupTableSelectedIndex :(NSInteger)selectedRowNo
{
    FeatureModel *fdata = [dataArray objectAtIndex:selectedRowNo];
    
    if ([[fdata type]isEqualToString:@"private"]) {
        privateShowRoomId = [fdata ct_id];
        privateShowRoomName = [fdata name];
        
        isPrivateShowroomSelectd = YES;
    }
    else
    {
        isPrivateShowroomSelectd = NO;
        
    }
    if (selectedEntitytype == EntityTypeshowroom)
    {
        theShowroomSelectState =  ([[fdata type]isEqualToString:@"private"])? MSGSHOWROOM_PRIVATESELECTD:MSGSHOWROOM_PUBLICSELECTD;
        NSString *showroomName = [self getShowrromWithoutSpecialCharacters:fdata.name];
        
        if (![self checkForshowroomIDexistance:showroomName] && showroomAddedArray.count<7) {
            [showroomAddedArray addObject:showroomName];
            
            [self.composeView wordSelectedForSuggestType:selectedEntitytype AndString:showroomName andID:fdata.ct_id forRange:selectedRange];
        }
        
    }
    else if (selectedEntitytype == EntityTypeHashTag)
    {
        if (![self checkForHashtagIDexistance:fdata.ct_id] && hashTagAddedArray.count<7) {
            [hashTagAddedArray addObject:fdata.ct_id];
            [self.composeView wordSelectedForSuggestType:selectedEntitytype AndString:fdata.name andID:fdata.ct_id forRange:selectedRange];
        }
        
    }
    else if (selectedEntitytype == EntityTypeMention)
    {
        if (![self checkForUserIDexistance:fdata.ct_id] && userNameAddedArray.count<7) {
            [userNameAddedArray addObject:fdata.ct_id];
            [self.composeView wordSelectedForSuggestType:selectedEntitytype AndString:fdata.name andID:fdata.ct_id forRange:selectedRange];
        }
        
    }
    
    [popoverController dismissPopoverAnimatd:YES];
}


- (void)beginSuggestingForTextView:(EntityType)textSuggestType query:(NSString *)suggestionQuery Inrange:(NSRange)range
{
    
    if (suggestionQuery.length<1) {
        
        if ([[[[UIApplication sharedApplication] keyWindow] subviews] count]>0) {
            [popoverController dismissPopoverAnimatd:YES];
        }
        
        return;
    }
    
    selectedRange = range;
    
    switch (textSuggestType) {
        case EntityTypeHashTag:
        {
            selectedFeature = @"#";
            suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"#" withString:@""];
            [self getHashTagList:suggestionQuery withPagenum:1];
            
        }
            break;
        case EntityTypeMention:
        {
            selectedFeature = @"@";
            suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"@" withString:@""];
            [self getUserList:suggestionQuery withPagenum:1];
            
        }
            break;
        case EntityTypeshowroom:
        {
            if (!isPrivateShowroomSelectd) {
                selectedFeature = @"*";
                suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"*" withString:@""];
                if (showroomAddedArray.count!=7)
                    [self getShowroomList:suggestionQuery withPagenum:1];
            }
            
        }
            break;
        default:
            break;
    }
}
- (void)endSuggesting
{
    
    [popoverController dismissPopoverAnimatd:YES];
    
}
- (void)reloadSuggestionsWithType:(EntityType)suggestionType query:(NSString *)suggestionQuery range:(NSRange)suggestionRange
{
    
}

- (void)getUserList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),name,pagenum ];
    [self CallAPIforType:EntityTypeMention andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", USER_LIST_API,submitData]]];
}

- (void)getShowroomList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),name,pagenum] ;
    [self CallAPIforType:EntityTypeshowroom andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", SHOWROOM_LIST_API,submitData]]];
}

- (void)getHashTagList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d&showroom_type=0",GETVALUE(CEO_AccessToken),name,pagenum];
    [self CallAPIforType:EntityTypeHashTag andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", HASHTAG_LIST_API,submitData]]];
}

-(void)CallAPIforType:(EntityType)type andAPIUrl:(NSURL *)myUrl
{
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
    //    [submitrequest setHTTPMethod:@"POST"];
    //    [submitrequest setHTTPBody:submitData];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         switch (type) {
             case EntityTypeMention:
                 dataArray =[EntitySupport UserlistDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
             case EntityTypeHashTag:
                 dataArray =[EntitySupport TagDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
             case EntityTypeshowroom:
                 dataArray =[EntitySupport showRoomDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
                 
             default:
                 break;
         }
         [self deleteExistingRecordWithModelData:type];
         selectedEntitytype = type;
         if (dataArray.count>0)
         {
             [self createTableviewAndShowPopup:type];
         }
         else
         {
             [popoverController dismissPopoverAnimatd:YES];
         }
     }];
}

-(void)wordDeletedForSuggestType:(EntityType)textSuggestion andID:(NSString*)contactID
{
    if (textSuggestion == EntityTypeshowroom) {
        for (int as=0; as<showroomAddedArray.count; as++ ){
            NSString *ct_id = [showroomAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                if ([privateShowRoomId isEqualToString:contactID]) {
                    privateShowRoomId = @"";
                    isPrivateShowroomSelectd = NO;
                }
                [showroomAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
    else if (textSuggestion == EntityTypeHashTag) {
        for (int as=0; as<hashTagAddedArray.count; as++ ){
            NSString *ct_id = [hashTagAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                [hashTagAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
    else if (textSuggestion == EntityTypeMention) {
        for (int as=0; as<userNameAddedArray.count; as++ ){
            NSString *ct_id = [userNameAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                [userNameAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
}

-(void)deleteExistingRecordWithModelData:(EntityType)entityType
{
    NSMutableArray *anArray = [dataArray mutableCopy];
    
    if (entityType == EntityTypeshowroom) {
        for (NSString *c_id in showroomAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.name]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }
    else if (entityType == EntityTypeHashTag) {
        for (NSString *c_id in hashTagAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.ct_id]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }
    
    else if (entityType == EntityTypeMention) {
        for (NSString *c_id in userNameAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.ct_id]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }
}

- (void)textDidChange:(MessageComposerView*)commentView
{
    //    NSLog(@"total charecter= %d", (int)commentView.counterCount);
//    self.totaltextCounter.text = [NSString stringWithFormat:@"%d",300-(int)commentView.counterCount];
    
//    if (selectdHeaderbuttonTag)
//    {
//        [self.addmintTableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
}



- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}


-(BOOL)checkForshowroomIDexistance:(NSString*)showroomID{
    BOOL status = NO;
    for (NSString *showID in showroomAddedArray) {
        if ([showID isEqualToString:showroomID]){
            return YES;
        }
    }
    return status;
    
}
-(BOOL)checkForHashtagIDexistance:(NSString*)hashtagID{
    BOOL status = NO;
    for (NSString *c_id in hashTagAddedArray) {
        if ([c_id isEqualToString:hashtagID]){
            return YES;
        }
    }
    return status;
    
}

-(BOOL)checkForUserIDexistance:(NSString*)userID{
    BOOL status = NO;
    for (NSString *c_id in userNameAddedArray) {
        if ([c_id isEqualToString:userID]){
            return YES;
        }
    }
    return status;
    
}

#pragma mark - *********** End *******************

- (void)showPressedOrigin :(CGFloat)yPos
{

    CGRect rect = viewOptions.frame;
    rect.origin.y = (yPos>self.view.frame.size.height-150)?(yPos-viewOptions.frame.size.height):yPos;
    rect.origin.x = 10;
    [viewOptions setFrame:rect];
    [self.view.window addSubview:viewOptionHolder];
    [self.composeView setUserInteractionEnabled:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getAllComplimints:(int)pageNo
{
    if (![YSSupport isNetworkRechable])
        return;
    
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSString *feedId        = _feedId;
    
    NSData*  submitData     = [[NSString stringWithFormat:@"access_token=%@&mint_id=%d&page_number=%d",GETVALUE(CEO_AccessToken),[feedId intValue],pageNo] dataUsingEncoding:NSUTF8StringEncoding];

    [YSAPICalls getAllComplimints:submitData ForSuccessionBlock:^(id response) {
        isAPICalling = NO;
        Generalmodel *list_param  = response;
        if ([list_param.status_code integerValue ])
        {
            if([list_param.tempArray count]>0)
            {
                isDataFinishedFromServer = NO;
                
                [arrAllCommentsDetails addObjectsFromArray:list_param.tempArray];
                
                NSArray* originalArray = [NSArray arrayWithArray:arrAllCommentsDetails]; // However you fetch it
                NSMutableSet* existingNames = [NSMutableSet set];
                [arrAllCommentsDetails removeAllObjects];
                [originalArray enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop) {
                    if (![existingNames containsObject:[object achieveCommentId]]) {
                        [existingNames addObject:[object achieveCommentId]];
                        [arrAllCommentsDetails addObject:object];
                    }

                }];
                existingNames = nil;
                originalArray = nil;

            }
            else
                isDataFinishedFromServer = YES;
               [tblComment reloadData];

            //[MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        else
        {
            isDataFinishedFromServer = YES;
        }

    } andFailureBlock:^(NSError *error) {
    }];
}

#pragma mark - ******* Scroll View Delegate ********
//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (isAPICalling)
        return;
    
    if (isDataFinishedFromServer)
        return;
    
    BOOL shouldLoadNextPage = ShouldLoadNextPage(tblComment);
    if (shouldLoadNextPage)
    {
        if(!isAPICalling && !isDataFinishedFromServer)
        {
            isAPICalling = YES;
            pageNumber = pageNumber+1;
            [self getAllComplimints:pageNumber];
        }
    }
}

#pragma mark - ******* End ********

- (IBAction)btnBackTapped:(id)sender
{
    [self.composeView.messageTextView resignFirstResponder];
    [self.composeView removeFromSuperview];
    self.composeView = nil;
    //  [self setTabar];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setupViewHeightframe
{
   // delegateApp.ysTabBarController.tabBar.hidden = YES;
   // delegateApp.lbl.hidden                       = YES;
    CGRect frame                                 = self.view.frame;
    self.view.frame                              = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,delegateApp.window.frame.size.height);
}

-(void)hashTagSelected : (NSString *)selectedEnitity;
{
        MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
      //  mintDetailsVC.delegate = self;
        mintDetailsVC.isToshowSingleRecord = NO;
        mintDetailsVC.isFromTrendingTags = YES;
        mintDetailsVC.navigationHeaderText = selectedEnitity;
        [self.navigationController pushViewController:mintDetailsVC animated:YES];
  
}


#pragma mark - ********* UITableView delegate *********

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![YSSupport isNetworkRechable]) {
        return ([arrAllCommentsDetails count])?[arrAllCommentsDetails count]:0;
    }
    
    if ([arrAllCommentsDetails count]>=10) {
        return ([arrAllCommentsDetails count])?((isDataFinishedFromServer)?[arrAllCommentsDetails count]:[arrAllCommentsDetails count]+1):0;
    }
    
    return ([arrAllCommentsDetails count])?((isDataFinishedFromServer)?[arrAllCommentsDetails count]:[arrAllCommentsDetails count]):0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row>=arrAllCommentsDetails.count)
    {
        return 40;
    }
    
    NSString *commentContent = [[[self.arrAllCommentsDetails objectAtIndex:indexPath.row] achieveCommentText] emojizedString] ;
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGFloat contentSize  = [commentContent boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.view.frame)-60, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    contentSize=  contentSize+35;

    return (contentSize > kMinimumCellHeight) ? contentSize : kMinimumCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CommentsCell";
    CommentsCell *cell = (CommentsCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSString *nibname = @"CommentsCell";
        NSArray *arrItems = [[NSBundle mainBundle]loadNibNamed:nibname owner:nil options:nil ];
        for (id object in arrItems){
            if([object isKindOfClass:[CommentsCell class]]){
                cell = (CommentsCell *)object;
                break;
            }
        }
    }
    
    if(indexPath.row>=arrAllCommentsDetails.count)
    {
        [cell showLoadMore];
        [cell.contentView addSubview:cell.viewLoadMoreComments];
        return cell;
    }
    
    if ([[cell viewWithTag:111] superview])
        [[cell viewWithTag:111] removeFromSuperview];
    
    UIView *viewBG = [[UIView alloc]initWithFrame:cell.frame];
    viewBG.backgroundColor =[UIColor clearColor];
    [cell setSelectedBackgroundView:viewBG];
    [cell setLayer];
    
    CommentModel *amodel        = [self.arrAllCommentsDetails objectAtIndex:indexPath.row];
    cell.lblUserName.text       = [amodel achieveCommentUname];
    cell.lblcomment.text        = [[amodel achieveCommentText] emojizedString];
    [cell.imageUser setUserInteractionEnabled:YES];
    cell.imageUser.tag = [amodel.achieveCommentUserId integerValue];
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [cell.imageUser addGestureRecognizer:singleTap];

    
    
    cell.lblcomment.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
    {
        if (hotWord == STTweetHashtag)
        {
            [self hashTagSelected:string];
        }
        if (hotWord == STTweetMention)
        {
            NSString *strUserId = [self checkUserNameExistWithName:[string substringFromIndex:1] model:amodel];
            
            if ([strUserId isEqualToString:@"0"])
                [self gotoNouserFoundScreen];
            else
                [self gotoProfileView:strUserId];
        }
        
    };
    
    
    
    cell.lblUserName.highlightedTextColor  = cell.lblUserName.textColor;
    
    CGRect frameComment = cell.lblcomment.frame;
    
    CGRect frameTime = cell.lblTime.frame;
    frameTime.origin.y = frameComment.size.height+frameComment.origin.y-2;
    cell.lblTime.frame = frameTime;
    
    CGRect lblDeviderFrame = cell.lblDevider.frame;
    lblDeviderFrame.origin.y = frameTime.size.height+frameTime.origin.y-1;
    cell.lblDevider.frame = lblDeviderFrame;
    
    cell.lblTime.text           = ([amodel.status_commnetEdited integerValue])?[NSString stringWithFormat:@"%@ - Edited",[amodel achieveCommentDate] ]:[amodel achieveCommentDate];
    
    cell.imageUser .mintImageURL    = [NSURL URLWithString:[amodel achieveCommentUserImage]];
    cell.lblcomment.font        = [UIFont fontWithName:@"Helvetica" size:11];
    
    cell.postingLabel.alpha =(amodel.postingStatus)?0.0:1.0;
    cell.delegate = self;
    cell.tag = indexPath.row;
    
    return cell;
}
-(void)singleTapping:(UITapGestureRecognizer *)guesture
{
    UIButton *btn = (UIButton*)guesture.view;
    [self gotoProfileView:[NSString stringWithFormat:@"%d",btn.tag]];

}
-(void)gotoNouserFoundScreen
{
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"UserNotFoundVC" owner:nil options:nil];
    UIView *view = nibContents[0];
    UIButton *button = (UIButton *)[view viewWithTag:3];
    [button addTarget:self action:@selector(removeNouserfoundScreen:) forControlEvents:UIControlEventTouchUpInside];
    
    AppDelegate *delegate = MINTSHOWAPPDELEGATE;
    [delegate.window addSubview:view];
}

-(void)removeNouserfoundScreen:(UIButton *)button
{
    [[[button superview] superview] removeFromSuperview];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)gotoProfileView:(NSString *)selectedEnitity{
    
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
   // prof.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:prof animated:YES];
}


-(void)onLongPress:(UILongPressGestureRecognizer*)pGesture
{
    if (pGesture.state == UIGestureRecognizerStateRecognized)
    {
        //Do something to tell the user!
    }
    if (pGesture.state == UIGestureRecognizerStateEnded)
    {
        CGPoint touchPoint = [pGesture locationInView:tblComment];
        NSIndexPath* selectedIndex = [tblComment indexPathForRowAtPoint:touchPoint];
        
        if(selectedIndex.row>=arrAllCommentsDetails.count)
        {
            return;
        }
        [tblComment reloadData];
        CommentsCell *cell = (CommentsCell *)[tblComment cellForRowAtIndexPath:selectedIndex];
        [cell setSelected:YES];
        
        if ([[[self.arrAllCommentsDetails objectAtIndex:selectedIndex.row] achieveCommentUserId] isEqualToString:GETVALUE(CEO_UserId)])
        {
            if (selectedIndex != nil)
            {
                seletedCell = selectedIndex.row;
                UIView *viewBG = [[UIView alloc]initWithFrame:cell.frame];
                viewBG.backgroundColor =[UIColor colorWithRed:(235.0/255.0) green:(244.0/255.0) blue:(249.0/255.0) alpha:1.0];
                cell.selectedBackgroundView = viewBG;
                
                NSLog(@"%f %f", [pGesture locationInView:self.view].x,  [pGesture locationInView:self.view].y);
                [self showPressedOrigin:[pGesture locationInView:self.view].y];
            }
        }
    }
}


-(void)showeditCommentInfo
{
    [self.composeView setUserInteractionEnabled:YES];
    [self.composeView.sendButton setEnabled:YES];
    [self.composeView.placeHolderLabel setHidden:YES];    // Need to add
    self.composeView.messageTextView.text = [[[self.arrAllCommentsDetails objectAtIndex:seletedCell] achieveCommentText] emojizedString];
    [self.composeView beginTextEditing];
    isEditedCell = YES;
    [self.composeView.sendButton setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
    [self.composeView.sendButton setTitle:@"Update" forState:UIControlStateNormal];
    self.composeView.isCompliMintEdited = YES;
    [self.composeView.messageTextView performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1f];
    [self.composeView determineHotWordsNew];   // Need to add


}

#pragma mark--textFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.33];
    [UIView setAnimationBeginsFromCurrentState:YES];
        viewAddComment.frame = CGRectMake(viewAddComment.frame.origin.x,viewAddComment.frame.origin.y - 216, viewAddComment.frame.size.width, viewAddComment.frame.size.height);
    [self.view bringSubviewToFront:viewHeader];
    [UIView commitAnimations];
    if (textField.text.length>0){
       // NSLog(@"come here");
        btnSend.highlighted=YES;
        btnSend.userInteractionEnabled=YES;
    }
    else{
       // NSLog(@"come here");
        btnSend.highlighted=NO;
        btnSend.userInteractionEnabled=NO;
    }
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    float yCord;
    float height;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationBeginsFromCurrentState:YES];
    yCord=0;
    height=self.view.frame.size.height;
    viewAddComment.frame = CGRectMake(viewAddComment.frame.origin.x, [UIScreen mainScreen].bounds.size.height - viewAddComment.frame.size.height, viewAddComment.frame.size.width,viewAddComment.frame.size.height);
    [UIView commitAnimations];
    yCord=0;
    height=0;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"come here");
    btnSend.highlighted=YES;
    btnSend.userInteractionEnabled=YES;
    return  YES;
}

- (void)messageComposerSendMessageClickedWithMessage:(NSString*)message{
    NSString *titleVal = self.composeView.sendButton.titleLabel.text;
    [self.composeView.messageTextView resignFirstResponder];

    if ([titleVal isEqualToString:@"Cancel"])
    {
        [self.composeView removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }

        self.strCommentTextValue=[NSString stringWithFormat:@"%@",message];
        NSLog(@"strCommentTextValue %@",self.strCommentTextValue);
        btnEdit.hidden= NO;
        [txtAddComment resignFirstResponder];
    if (![YSSupport isNetworkRechable])
        return;
    
    if (isEditedCell) {
        
        [self serverEditComment:seletedCell text:message];
    }
    else
        [self serverPostComment:GETVALUE(CEO_AccessToken) text:message];
}

-(void)serverEditComment : (NSInteger) tag  text:(NSString *)commentText
{
    NSLog(@"selectd cell %ld and tag %ld",(long)seletedCell,(long)tag);
    [self.view endEditing:YES];
    YSAPICalls *support    =   [YSAPICalls new];
    
    NSString       *theString = [YSSupport stringByReplacingEmoji:commentText withDict:self.emojiDict];
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate    *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    
    NSArray *parts = [theString componentsSeparatedByCharactersInSet:whitespaces];
    NSArray *names = [parts filteredArrayUsingPredicate:noEmptyStrings];
    theString = [names componentsJoinedByString:@" "];
    
    if ([[[arrAllCommentsDetails objectAtIndex:tag] achieveCommentText]isEqualToString:theString]) {
        NSIndexPath *indexPath          =       [NSIndexPath indexPathForRow:tag inSection:0];
        [tblComment reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:NO];
        self.composeView.isCompliMintEdited = NO;
        isEditedCell                        = NO;
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];


    NSString *commentId  = [[self.arrAllCommentsDetails objectAtIndex:seletedCell] achieveCommentId];
    [support editCompleMint:theString feedModel:_achievementdetails url:@"newsfeed/edit_comment" commentId:commentId withSuccessionBlock:^(id response) {
        
        CommentModel *cmt=response;
        if ([cmt.status_code intValue]==1)
        {
            self.composeView.isCompliMintEdited =       NO;
            isEditedCell                        =       NO;
            CommentModel *comment               =       [self.arrAllCommentsDetails objectAtIndex:tag];
            comment.achieveCommentText          =       theString;
            comment.achieveCommentDate          =       @"one moment ago";
            comment.status_commnetEdited        =       @"1";
            comment.atMentionedArray            =       cmt.atMentionedArray;
            comment.commentTextHeight           =       [YSSupport getCommentTextHeight:comment.achieveCommentText font:12.0];
            NSIndexPath *indexPath          =       [NSIndexPath indexPathForRow:tag inSection:0];
            NSDictionary *dict                  =       [NSDictionary dictionaryWithObjectsAndKeys:EDITCOMMENT,@"activity",comment,@"CommentData",_feedId,@"FEEDID",nil];
           
            [[NSNotificationCenter defaultCenter]   postNotificationName:KCOMMNETUPDATED object:nil userInfo:dict] ;
            [self.arrAllCommentsDetails replaceObjectAtIndex:tag withObject:comment];
            [tblComment reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:NO];
            txtAddComment.text = nil;
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
            andFailureBlock:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
}
- (void)serverPostComment:(NSString *)accessToken text:(NSString *)commentText
{
    isEditedCell = NO;
    self.composeView.isCompliMintEdited = NO;

    [self.view endEditing:YES];

//    NSString *finalPost = [YSSupport stringByReplacingEmoji:commentText withDict:self.emojiDict];
    NSString *theString = [YSSupport stringByReplacingEmoji:commentText withDict:self.emojiDict];
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    
    NSArray *parts = [theString componentsSeparatedByCharactersInSet:whitespaces];
    NSArray *names = [parts filteredArrayUsingPredicate:noEmptyStrings];
    theString = [names componentsJoinedByString:@" "];
    
    CommentModel *newmodel=[CommentModel new];
    newmodel.achieveCommentText      = theString;
    //newmodel.achieveCommentText    = strComment;
    newmodel.achieveCommentUserId    = GETVALUE(CEO_UserId);
    newmodel.achieveCommentUname     = [NSString stringWithFormat:@"%@ %@",GETVALUE(CEO_UserFirstName),GETVALUE(CEO_UserLastName)];
    newmodel.postingStatus           = NO;
    newmodel.achieveCommentUserImage = GETVALUE(CEO_UserImage);
    newmodel.status_code             = @"1";
  
    
    
    YSAPICalls *support = [YSAPICalls new];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [support CompleMint:theString feedModel:_achievementdetails url:@"newsfeed/add_comment" withSuccessionBlock:^(id response) {
        CommentModel *cmt=response;
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        if ([cmt.status_code intValue]==1){
            AppDelegate *aDelegate           = (AppDelegate *) [UIApplication sharedApplication].delegate;
            aDelegate.isCommentActionUpdate  = YES;
            newmodel.achieveCommentDate      = cmt.achieveCommentDate;
            newmodel.achieveCommentId        = cmt.achieveCommentId;
            newmodel.commentTextHeight       = [YSSupport getCommentTextHeight:cmt.achieveCommentText font:12.0];
            cmt.postingStatus                = YES;
            newmodel.atMentionedArray = cmt.atMentionedArray;
            [self.arrAllCommentsDetails addObject:newmodel];
            [tblComment reloadData];
            txtAddComment.text = nil;

            if(self.arrAllCommentsDetails.count> 0)
            [tblComment scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.arrAllCommentsDetails.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
            if (_isFromTrendingMints) {
                
                [self.delegate updateCommentCount:[self.arrAllCommentsDetails count] alreadyCommented:YES];
            }
else
{
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:ADDCOMMENT,@"activity",newmodel,@"CommentData",_feedId,@"FEEDID",nil];
            [[NSNotificationCenter defaultCenter]postNotificationName:KCOMMNETUPDATED object:nil userInfo:dict];
}
        }
    } andFailureBlock:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

 -(void)deleteRecord
{
    self.composeView.messageTextView.text = nil;
    [self.composeView.sendButton setEnabled:NO];
    [self.composeView.placeHolderLabel setHidden:NO];
    isEditedCell = NO;
    self.composeView.isCompliMintEdited = NO;
    [self.composeView.sendButton setTitle:@"Cancel" forState:UIControlStateNormal];

    
                 CommentModel *newModel=[CommentModel new];
                 newModel=[self.arrAllCommentsDetails objectAtIndex: seletedCell];
                 NSString *url = [NSString stringWithFormat:@"/newsfeed/delete_comment?access_token=%@&feed_id=%@",[[NSUserDefaults standardUserDefaults] objectForKey:CEO_AccessToken],newModel.achieveCommentId];
                 
                 
                 NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,url]]];
                 NSLog(@"delete comment url=%@",[NSString stringWithFormat:@"%@%@",ServerUrl,url]);
                 [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                 [NSURLConnection sendAsynchronousRequest:submitrequest
                 queue:[NSOperationQueue mainQueue]
                 completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
                 {
                     [self.composeView.sendButton setEnabled:YES];
                     [self.composeView.sendButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                    if ([(NSHTTPURLResponse *)response statusCode]==200)
                     {
                         id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                         NSLog(@"json data=%@",json);
                         if([[NSString stringWithFormat:@"%@",[json objectForKey:@"status"]] isEqualToString:@"true"])
                         {
                            AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
                            aDelegate.isCommentActionUpdate = YES;
                            [self.arrAllCommentsDetails removeObjectAtIndex:seletedCell];
                             
                             if (_isFromTrendingMints) {
                                 [self.delegate updateCommentCount:[self.arrAllCommentsDetails count] alreadyCommented:([[json objectForKey:@"Already_commented"] isEqualToString:@"no"])?NO:YES];
                             }
                             else
                             {
                                 NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:DELETECOMMENT,@"activity",newModel,@"CommentData",_feedId,@"FEEDID",nil];
                                 
                                [[NSNotificationCenter defaultCenter]postNotificationName:KCOMMNETUPDATED object:dict userInfo:dict] ;
                             }
                                [tblComment reloadData];
                         }
                     }
                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                 }];
 }

-(void)setTabar {
    delegateApp            = (AppDelegate *)[UIApplication sharedApplication].delegate;
   // delegateApp.lbl.hidden = NO;
    CGRect frame           = self.view.frame;
    self.view.frame        = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,frame.size.height - 49);
    delegateApp.ysTabBarController.tabBar.hidden = NO;
    delegateApp.ysTabBarController.selectedIndex = 0;
    [delegateApp changeBackgroundOfTabBarForIndex:0];
}
-(NSString *)checkUserNameExistWithName:(NSString *)selectUserName model:(CommentModel *)fmodel
{
    NSLog(@"%@",fmodel.atMentionedArray);
    if(fmodel.atMentionedArray.count>0)
    {
        for (int i=0; i<fmodel.atMentionedArray.count; i++) {
            NSString *strName = [[fmodel.atMentionedArray objectAtIndex:i]valueForKey:@"name"];
            strName = [strName stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([[strName lowercaseString] isEqualToString:[selectUserName lowercaseString]]) {
                //                NSLog(@"%@",[[fmodel.atMentionedArray objectAtIndex:i]valueForKey:@"id"]);
                return [[fmodel.atMentionedArray objectAtIndex:i]valueForKey:@"id"];
            }
        }
    }
    return nil;
}


@end
