//
//  AddEditShowroomVC.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 04/01/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddEditShowroomVCDelegate
- (void)showroomAddedOrEditedSuccesFully:(NSString *)showroomName;
//- (void)showroomAddedOrEditedSuccesFullyShowRoomVc:(NSString *)showroomDescription showroomImage :(UIImage *)showroomImage;
- (void)showroomAddedOrEditedSuccesFullyShowRoomVc:(NSString *)showroomDescription showroomImage:(UIImage *)showroomImage showroomImageUrl:(NSString *)url showroomName:(NSString *)showroomName showroomPrivacy : (NSString *)privacySetting privacyType :(NSString *)privacyType;
- (void)deleteShowroom : (NSString *) showroomName;
@end;
@interface AddEditShowroomVC : UIViewController
@property (assign) BOOL isFromShowRoomVC;
@property (weak, nonatomic) ShowRoomModel *modelShow;
@property (assign,nonatomic) id  <AddEditShowroomVCDelegate> aDelegate;

@end
