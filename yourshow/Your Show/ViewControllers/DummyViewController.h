//
//  DummyViewController.h
//  Your Show
//
//  Created by Siba Prasad Hota on 20/10/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyShowViewController.h"

@interface DummyViewController : UIViewController

@property (nonatomic,strong) id myshow;

@end
