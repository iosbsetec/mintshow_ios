//
//  MSForgotPasswordVC.m
//  Your Show
//
//  Created by bsetec on 2/3/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//
#define kVALIDEMAILID @"Please add a valid email id"

#import "MSForgotPasswordVC.h"

@interface MSForgotPasswordVC ()
{
    
    IBOutlet UITextField *txtFieldEmail;
    IBOutlet UIView *viewEmail;
    AppDelegate *appDelegate;
}

@end

@implementation MSForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewEmail.layer.borderColor=[[UIColor whiteColor]CGColor];
    viewEmail.layer.borderWidth=1.0;
    txtFieldEmail.tintColor = [UIColor whiteColor];
    txtFieldEmail.textColor = [UIColor whiteColor];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];


    // Do any additional setup after loading the view from its nib.
}
- (IBAction)submitTapped:(id)sender {
    BOOL isOK = [self checkUserGivenDetails];
    if (isOK)
    {
        NSData *submitData = [[NSString stringWithFormat:@"email=%@",txtFieldEmail.text] dataUsingEncoding:NSUTF8StringEncoding];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/users/post_newforgetpassword",ServerUrl]]];
        [submitrequest setHTTPMethod:@"POST"];
        [submitrequest setHTTPBody:submitData];
        NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
        NSLog(@"submit data %@",submitDta123);
        
        [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
             NSDictionary *Info = [YSSupport dictionaryFromResponseData:data];
             NSLog(@"%@",Info);
             if ([[Info valueForKey:@"status"] isEqualToString:@"true"] )
             {
                 [self cancelTapped:nil];
                 [appDelegate onCreateToastMsgInWindow:[Info valueForKey:@"success_message"]];
                 
             }
             else if([[Info valueForKey:@"error_message"] isEqualToString:@"You didn't register yet."])
             {
                 [appDelegate onCreateToastMsgInWindow:@"This email have not been register yet"];

                 NSLog(@"%@",Info);
             }
 else
 {
     NSLog(@"%@",Info);

 
 }
     
             
         }];
        

    }

}

- (IBAction)closeTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cancelTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];


}

#pragma mark -Textfield Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self showDescKeyBoard];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self HideDescKeyboard];
}


-(BOOL)checkUserGivenDetails
{
    
    if (txtFieldEmail.text.length==0)
    {
        [appDelegate onCreateToastMsgInWindow:@"Please enter your Email id"];
        return NO;
    }

    if (![YSSupport validateEmail:txtFieldEmail.text])
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDEMAILID];
        return NO;
    }
    return YES;

}


-(void)showDescKeyBoard
{
    CGRect containerframe=self.view.frame;
    containerframe.origin.y=-200;
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame=containerframe;
    }];
}


-(void)HideDescKeyboard
{
    CGRect containerframe=self.view.frame;
    containerframe.origin.y=0;
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame=containerframe;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
