//
//  MSRemintVC.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 21/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MSRemintVC.h"
#import "AppDelegate.h"
#import "CollectionViewTableViewCell.h"
#import "RemintDescriptionCell.h"
#import "JitenCollectionView.h"
#import "STTweetView.h"
#import "RemintTextMintCell.h"
#import "TSPopoverController.H"
 #import "POPupTableView.h"
#import "TSPopoverController.h"
#import "EntitySupport.h"
#import "NSString+Emojize.h"
#import "HCYoutubeParser.h"
#import "YSVideoPlayerView.h"
#import "MintVideoTableViewCell.h"
#import "EmptyTableViewCell.h"

typedef enum showRoomSelectionType
{
    RMSHOWROOM_NOTSELECTD,
    RMSHOWROOM_PUBLICSELECTD,
    RMSHOWROOM_PRIVATESELECTD,
} RMShowroomType;

@interface MSRemintVC () <POPUpTableViewDataSource,POPUpTableDelegate,textSuggestDelegate,ysPlayerViewDelegate,JitenCollectionTableDelegate,JitenCollectionViewDataSource>

{
        NSString *OriginalFrontendText;
        IBOutlet UITableView *tblViewRemint;
        NSMutableArray *allShowRoomdata;
        CGFloat multiShowroomHeight;

    __weak IBOutlet UIView *viewRemint;
    
    /*! AT,STAR,SHOWRROM FEATURES */
    NSMutableArray *dataArray;
    TSPopoverController *popoverController;
    POPupTableView *apopupTable;
    EntityType selectedEntitytype;
    NSMutableArray *userNameAddedArray;
    NSRange selectedRange;

    NSMutableArray *showroomAddedArray;
    NSMutableArray *hashTagAddedArray;
    
    BOOL isPrivateShowroomSelectd;
    NSString *privateShowRoomId;
    NSString *privateShowRoomName;
    
    NSMutableString *strAllShowroomsForLaterUse;
    RMShowroomType rmshowroomSelectState;

    
    /*! For Playing Video Mint */
    NSURL *urlYoutube;
    YSVideoPlayerView* ysplayer;
    BOOL isVideoPlaying,isDescriptionContentPrivateShowroom;
    
    IBOutlet UILabel *lblTextCount;
    
    
    
    NSMutableArray  *arrayAddedPrivateShowroolList;
    BOOL isSelectFromPopUp;
    
    
    BOOL isAPICalling;
    BOOL isDataFinisedFromServer;
    int pageNumber;
}

@property (strong, nonatomic) IBOutlet JitenCollectionView *showRoomCollectionview;
@property(nonatomic,retain) STTweetView *txtCmtRemint;
@property (nonatomic, strong)  NSDictionary *emojiDict;

@end

@implementation MSRemintVC
@synthesize strImgUserUrl;
@synthesize strDescription;
@synthesize strThumbImgUrl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    pageNumber = 1;

    rmshowroomSelectState = RMSHOWROOM_NOTSELECTD;
    self.emojiDict = [YSSupport reverseEmojiAliases];

    
    tblViewRemint.tableHeaderView   = tableHeader;
    imgUser.layer.cornerRadius      = imgUser.frame.size.height/2;
    imgUser.mintImageURL                = [NSURL URLWithString:GETVALUE(CEO_UserImage)];
    [imgUser setClipsToBounds:YES];

    multiShowroomHeight = 100;
  //  [self initializer];
    isSelectFromPopUp       = NO;
    
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [viewRemint addGestureRecognizer:singleFingerTap];
  //  AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
   // delegateApp.lbl.hidden = YES;
    [self createDataForYSCommentView];
    
    if ([_feedData.youtube_id length])
    {
        [self YoutubeVCDelegateMethod:_feedData.youtube_id];
    }
//    tblViewRemint.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self setDataForPrivateShowroom];
    
    [self initializer];
    
    if (_isEditRemint) {
        [self setEditRemintText];
    }
    
    //Jitendra changes ----------
    
    
    pageNumber = 1;

    arrayAddedPrivateShowroolList = [NSMutableArray array];
}

-(void)setEditRemintText
{
    _txtCmtRemint.textView.text = [_feedData.remint_message emojizedString];
    [_txtCmtRemint determineHotWordsNew];
}

-(void)initializer
{
    // MULTI SHOW ROOM  COLLECTION VIEW
    self.showRoomCollectionview.CollectionDataSource    = self;
    self.showRoomCollectionview.collectiondelegate      = self;
    self.showRoomCollectionview.Addshowroom             = NO;
    self.showRoomCollectionview.isCellSelectable        = YES;
    self.showRoomCollectionview.backgroundColor         = [UIColor whiteColor];
    self.showRoomCollectionview.showsHorizontalScrollIndicator = NO;
    self.showRoomCollectionview.showsVerticalScrollIndicator   = NO;
    self.showRoomCollectionview.scrollEnabled           = NO;
    allShowRoomdata = [[NSMutableArray alloc]init];
    
    [self getAllShowroomForMyShowTabWithPageNo:pageNumber];

}


-(void)getAllShowroomForMyShowTabWithPageNo:(int)pgNo
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&showroom_list_type=%@&page_number=%d",GETVALUE(CEO_AccessToken),@"0",pgNo];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@users/get_showroom_list?%@",ServerUrl,submitData]]];
    
    
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isAPICalling = NO;
         
         Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:METHOD_MYSHOW_SHOWROOM];
         if ([gmodel.status_code integerValue])
         {
             if(gmodel.tempArray.count > 0)
             {
                 isDataFinisedFromServer = NO;
                 [allShowRoomdata addObjectsFromArray: gmodel.tempArray];
                 [self ReloadShowroomCollectionView];
             }
             else
             {
                 isDataFinisedFromServer = YES;
                 
             }
             
         }
         
     }];
}

#pragma mark - THIS CODE FOR RELATED SHOWROOM COLLECTION VIEW

-(void)ReloadShowroomCollectionView
{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.showRoomCollectionview performBatchUpdates:^{
        [self.showRoomCollectionview reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
            multiShowroomHeight = self.showRoomCollectionview.collectionViewLayout.collectionViewContentSize.height;
            self.showRoomCollectionview.frame = CGRectMake(0,0, self.view.frame.size.width,multiShowroomHeight);

            [self reloadTableviewWithsection:0];
        }
    }];
}

-(void)reloadTableviewWithsection:(NSInteger)section
{
    [tblViewRemint reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:section], nil]  withRowAnimation:UITableViewRowAnimationNone];
}

-(JitenCollectionView *)showroomCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.showRoomCollectionview;
}
- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    return  (ShowRoomModel *)[allShowRoomdata objectAtIndex:row];
    // return  nil;
}
- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView{
//    NSLog(@"NUMBER OF SHOWROOMS %ld",(unsigned long)allShowRoomdata.count);
    return allShowRoomdata.count;
}

#pragma mark JITEN COLLECTION DID SELECT CELL METHOD



- (void) JitenCollectionViewDelegateMethod: (ShowRoomModel *) selectedData selectedIndex :(NSInteger) selectedRowNo
{
    
    NSString *showroomName = [self getShowrromWithoutSpecialCharacters:selectedData.showroom_name];
    
    if ([selectedData.selected integerValue] == 0)
    {
        if (![self checkForshowroomIDexistance:showroomName])
        {
            [showroomAddedArray addObject:showroomName];
            if ([selectedData.privacy_settings isEqualToString:@"on"]) {
                // && showroomAddedArray.count<7
                [arrayAddedPrivateShowroolList addObject:selectedData.showroom_name];
            }
            selectedData.selected = @"1";
            
            if (!isSelectFromPopUp)
            {
                [self.txtCmtRemint appandAStringWithEntityType:EntityTypeshowroom AndString:[NSString stringWithFormat:@"*%@",showroomName] andID:selectedData.showroom_id];
            }
            else
            {
                NSIndexPath *selection = [NSIndexPath indexPathForItem:selectedRowNo inSection:0];
                [self.showRoomCollectionview batchinsertCollectionView:selection];
                isSelectFromPopUp = NO;
            }
        }
    }
    else
    {
        selectedData.selected = @"0";
        [self.txtCmtRemint removeShowroomNameInTweetTextView:[NSString stringWithFormat:@"*%@",[showroomName stringByReplacingOccurrencesOfString:@" " withString:@""]]];
        if ([selectedData.privacy_settings isEqualToString:@"on"]) {
            [arrayAddedPrivateShowroolList removeObject:selectedData.showroom_name];
        }
        for (int i=0; i<[showroomAddedArray count]; i++)
        {
            if ([[showroomAddedArray objectAtIndex:i] isEqualToString:showroomName])
            {
                [showroomAddedArray removeObjectAtIndex:i];
            }
        }
    }
}


#pragma mark - **** SHOWROOM COLLECTION VIEW END *****


-(void)setDataForPrivateShowroom
{
    for (NSString *privateShowroom in _feedData.privateShowroomListArray) {
        
        if ([privateShowroom containsString:@"-1"]) {
            isDescriptionContentPrivateShowroom = YES;
            self.txtCmtRemint.isStarCharBlocked = YES;
        }
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   // [self setupViewHeightframe];

}
-(void)viewDidDisappear:(BOOL)animated
{
    
    [super viewDidDisappear:animated];
    [self stopVideo];
    
}


-(void)stopVideo{
    
    
    if(_feedData.mintType==MSMintTypeText){
        return;
    }
        if(ysplayer)
        {
            [ysplayer stop];
            [ysplayer removeFromSuperview];
            ysplayer = nil;
        }
        
    
}
#pragma mark - UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (section==1)?35:0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==1)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
        [view setBackgroundColor:[UIColor clearColor]];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        label.textColor=[UIColor blackColor];
        label.textAlignment = NSTextAlignmentCenter;
        [label setText:@"Tag More Showrooms"];
        [label setBackgroundColor:[UIColor colorWithRed:244/255.0 green:244.0/255.0 blue:244.0/255. alpha:1.0]];
       // [label setBackgroundColor:[UIColor clearColor]];
        [view addSubview:label];
        return view;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        float height = [self getHeight:_feedData];

        return   height;
    }
    else
        return multiShowroomHeight+20;
}

-(float)getHeight : (BaseFeedModel *)newModel
{

    CGSize rctSizeFinal = CGSizeZero;
    if (!(_feedData.mintType==MSMintTypeText))
    {
        CGFloat widthCell       = [UIScreen mainScreen].bounds.size.width;
        
        if (newModel.mintType == MSMintTypeVideo||newModel.mintType == MSMintTypeYouTube||newModel.mintType == MSMintTypeInsthaGram) // Video Mint Cell
        {
            rctSizeFinal            = CGSizeMake(200,200);
        }
        else
        {
            CGSize rctSizeOriginal  = (newModel.feed_thumb_height>=300)?CGSizeMake(newModel.feed_thumb_Width , newModel.feed_thumb_height): CGSizeMake(300 , 300);
            double scale            = (widthCell  - (2 + 2)) / rctSizeOriginal.width;
            rctSizeFinal            = CGSizeMake(rctSizeOriginal.width*scale,rctSizeOriginal.height*scale);
        }
    }
    
    // Description
    NSString *OriginalText    = [[newModel.feed_description emojizedString]StringByChangingComplimintStringToNormalString];
    UIFont *font                      = [UIFont fontWithName:@"HelveticaNeue" size:11];
    CGRect newRect                    = [OriginalText boundingRectWithSize:CGSizeMake(self.view.frame.size.width-10, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
    
    float newHeight = rctSizeFinal.height + CGRectGetHeight(newRect)+ 15 ;
    return newHeight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return  1;
    }
    else
        return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (_feedData.mintType==MSMintTypeText) {
            static NSString *CellIdentifier = @"RemintTextMintCell";
            
            RemintTextMintCell *cell = (RemintTextMintCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RemintTextMintCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
                
                cell.lblCategoryType.text       = _feedData.category_type;
                cell.txtmintDescription.text    = [[_feedData.feed_description emojizedString] StringByChangingComplimintStringToNormalString];
                [cell setWithInfoAlignment:_feedData];
                cell.txtmintDescription.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
                    if (hotWord == STTweetHashtag) {
                        [self hashTagSelected:string];
                    }
                }; }
            return cell;
        } else  {
            static NSString *CellIdentifier = @"RemintDescriptionCell";
            
            RemintDescriptionCell *cell = (RemintDescriptionCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RemintDescriptionCell" owner:self options:nil];
                cell = [topLevelObjects objectAtIndex:0];
                if ([_feedData.feed_thumb_image length])
                    cell.imgUser.mintImageURL     = [NSURL URLWithString:_feedData.feed_thumb_image];
                    cell.lblCategoryType.text     = _feedData.category_type;
                    cell.mintDescription.text     = [_feedData.feed_description emojizedString];
                
                cell.mintDescription.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
                    if (hotWord == STTweetHashtag)  {
                        [self hashTagSelected:string];
                    }
                };
                
                if (_feedData.mintType == MSMintTypeVideo||_feedData.mintType == MSMintTypeYouTube||_feedData.mintType == MSMintTypeInsthaGram) {
                    cell.imgUser.mintImageURL     = [NSURL URLWithString:@""];
                    [self createVideoCell:cell.viewTop];
                }
                else
                    cell.imgUser.mintImageURL     = [NSURL URLWithString:_feedData.feed_thumb_image];
                
                [cell setWithInfoAlignment:_feedData];
            }
            
            return cell;
        }
    }
    else
    {
        static NSString *CellIdentifier = @"MultiMintCollectionTableViewCell";
        
        EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableViewCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        
        for(id aview in [cell.contentView subviews])
        {
            [aview removeFromSuperview];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.contentView addSubview:self.showRoomCollectionview];
        
        return cell;
    }
}

-(void)createVideoCell:(UIView *)cellView
{
    if (_feedData.mintType == MSMintTypeVideo||_feedData.mintType == MSMintTypeYouTube||_feedData.mintType == MSMintTypeInsthaGram)
    {
        static NSString *CellIdentifier = @"MintVideoTableViewCell";
        
        MintVideoTableViewCell *videoCell = (MintVideoTableViewCell *)[tblViewRemint dequeueReusableCellWithIdentifier:CellIdentifier];
        if (videoCell == nil)
        {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MintVideoTableViewCell" owner:self options:nil];
            videoCell = [topLevelObjects objectAtIndex:0];
            
            [videoCell setFrame:CGRectMake(0, 0, 312, 200)];
            [videoCell.mintImageView setFrame:videoCell.frame];
            videoCell.mintImageView.mintImageURL = [NSURL URLWithString:_feedData.feed_thumb_image];
            [videoCell setCustomVideoDelegate:self];
            if (isVideoPlaying)
            {
               
                    YSVideoPlayerView *ysPlayer = ysplayer;
                    CGRect rectVideoFrame = ysPlayer.frame;
                    rectVideoFrame.size.height = 200;
                    [ysPlayer setFrame:rectVideoFrame];
                    [videoCell.contentView addSubview:ysPlayer];
                    videoCell.frontView.alpha=1.0;
     
                
            }
            else
                videoCell.frontView.alpha = 0.0;

            [cellView addSubview:videoCell];
        }
    }
}

-(void)ysPlayerFinishedPlayback:(YSVideoPlayerView*)view
{
    isVideoPlaying = NO;
    [tblViewRemint reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0 ], nil] withRowAnimation:UITableViewRowAnimationNone];
    
    [view removeFromSuperview];
    view = nil;
}

-(NSString*)getUrlFromMediaID:(NSString*)mediaID
{
    return [NSString stringWithFormat:@"%@/%@.mp4",VIDEO_DIRECT_URL,mediaID];
}

#pragma mark - video play delegate
- (void)didClickedVideoBtn:(RemintDescriptionCell *)videoCell AndvcelType:(VideoCellType)celltype
{
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"%@",NSStringFromCGRect(videoCell.frame));
    if (!_feedData.youtube_id.length>0)
    {
        urlYoutube = [NSURL URLWithString:[self getUrlFromMediaID:_feedData.media_id]];
    }
    
    ysplayer = [[YSVideoPlayerView alloc] initWithFrame:CGRectMake(0, videoCell.frame.origin.y, 312, 200) contentURL:urlYoutube];
    [videoCell.contentView addSubview:ysplayer];
    ysplayer.ysdelegate = self;
    ysplayer.tintColor = [UIColor redColor];
    [ysplayer play];
    isVideoPlaying = YES;
}

- (void)YoutubeVCDelegateMethod:(NSString *) seectedStrings{
    
    NSURL *youtubeUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",seectedStrings]];
    //    selectedYoutubeUrl = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",vID];
    [HCYoutubeParser thumbnailForYoutubeURL:youtubeUrl thumbnailSize:YouTubeThumbnailDefaultMedium completeBlock:^(UIImage *image, NSError *error)  {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //                sharedClass.selectedImage=image;
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [tblViewRemint reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
            });
            
            [HCYoutubeParser h264videosWithYoutubeURL:youtubeUrl completeBlock:^(NSDictionary *videoDictionary, NSError *error) {
                
                NSDictionary *qualities = videoDictionary;
                
                NSString *URLString = nil;
                if ([qualities objectForKey:@"small"] != nil)  {
                    URLString = [qualities objectForKey:@"small"];
                } else if ([qualities objectForKey:@"live"] != nil) {
                    URLString = [qualities objectForKey:@"live"];
                }  else  {
//                    [[[UIAlertView alloc] initWithTitle:@"Warning!" message:@"This YouTube video is restricted to watch out side you tube." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
                    return;
                }
                urlYoutube =  [NSURL URLWithString:URLString];
            }];
            
        }
        else
        {
            NSLog(@"error %@",error);
        }
    }];
}

- (IBAction)backTapped:(UIButton *)sender
{

    
    [self dismissViewControllerAnimated:NO completion:^{
        //AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
        //delegateApp.lbl.hidden = NO;
    }];

}

-(void)setTabar
{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
   // delegateApp.lbl.hidden = NO;
    CGRect frame = self.view.frame;
    self.view.frame = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,
                                 frame.size.height - 49);
    
    delegateApp.ysTabBarController.tabBar.hidden = NO;
    delegateApp.ysTabBarController.selectedIndex = 0;
    [delegateApp changeBackgroundOfTabBarForIndex:0];
}

-(void)editRemintMintTapped
{
    NSString *serverString = self.txtCmtRemint.textView.text;
    serverString = [YSSupport stringByReplacingEmoji:serverString withDict:self.emojiDict];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&remint_id=%@&mint_text=%@",GETVALUE(CEO_AccessToken),_feedData.feed_id,serverString] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_EDITREMINT]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"success_message"] isEqualToString:@"Updated successfully"])
         {
//             [self.aDelegate editRemintedSucssesfully:[showroomDict valueForKey:@"data"]];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"REMINTCOMPLETE" object:nil];
             
             AppDelegate *delegateApp = (AppDelegate *) [UIApplication sharedApplication].delegate;
             delegateApp.ysTabBarController.selectedIndex = 0;
             [delegateApp changeBackgroundOfTabBarForIndex:0];
             
             [self dismissViewControllerAnimated:NO completion:^{
                 //AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
                 //delegateApp.lbl.hidden = NO;
             }];        }
         else
         {
             showAlert([showroomDict valueForKey:@"success_message"], nil, @"OK", nil);
         }
     }];
}

-(void)remintMintTapped
{

    NSString *serverString = self.txtCmtRemint.textView.text;
    serverString = [YSSupport stringByReplacingEmoji:serverString withDict:self.emojiDict];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&feed_id=%@&original_id=%@&source=Ios&mint_text=%@",GETVALUE(CEO_AccessToken),_feedData.feed_id,_feedData.original_mint_id,serverString] dataUsingEncoding:NSUTF8StringEncoding];
    
    [YSAPICalls remintMint:submitData  ForSuccessionBlock:^(id newResponse) {
        //[ProgressHUD dismiss];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        Generalmodel *gmodel = newResponse;
        NSLog(@"response = %@",gmodel.status);
        
        if ([gmodel.status_code integerValue])
        {
           // [self.aDelegate remintedSucssesfully];
            SETVALUE(@"yes", CEO_ISMINTALREADYPOSTED);
            SYNCHRONISE;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"REMINTCOMPLETE" object:nil];
            
            AppDelegate *delegateApp = (AppDelegate *) [UIApplication sharedApplication].delegate;
            delegateApp.ysTabBarController.selectedIndex = 0;
            [delegateApp changeBackgroundOfTabBarForIndex:0];
            [self dismissViewControllerAnimated:NO completion:^{

                
            }];
        }
        else
        {
            showAlert(@"Server Error", nil, @"OK", nil);
        }
        
        
    }
           andFailureBlock:^(NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         //        [ProgressHUD dismiss];
     }];
}

-(void)createDataForYSCommentView
{
    selectedRange         = NSMakeRange(0, 0);
    
    _txtCmtRemint = [[STTweetView alloc]initWithFrame:CGRectMake(46, 13, self.view.frame.size.width-50, 35)];
    _txtCmtRemint.textDelegate = self;
    _txtCmtRemint.nMaxTextLength = 25;
//    [_txtCmtRemint setIneraction:YES];
    [tableHeader addSubview:_txtCmtRemint];
    [self createTableAndPopupView];
    
    showroomAddedArray = [[NSMutableArray alloc]init];
    userNameAddedArray = [[NSMutableArray alloc]init];
    hashTagAddedArray = [[NSMutableArray alloc]init];
}

#pragma mark - PopUp TableView Creation & Datasource

-(void)createTableAndPopupView
{
    apopupTable = [[POPupTableView alloc]initWithFrame:CGRectMake(0, 0, 250, 200)];
    apopupTable.popupDataSource=self;
    apopupTable.popupDelegate = self;
    
    popoverController = [[TSPopoverController alloc] initWithView:apopupTable];
    popoverController.cornerRadius = 5;
    popoverController.titleText = @"change order";
    popoverController.popoverBaseColor = [UIColor lightGrayColor];
    popoverController.popoverGradient= NO;
}

-(void)createTableviewAndShowPopup:(EntityType)textSuggestType
{
    apopupTable.suggestType = textSuggestType;
    [popoverController showPopoverWithRect:CGRectMake(10,-30,300,100)];
    [apopupTable reloadData];
}

- (NSInteger)rowsForBubbleTable:(POPupTableView *)tableView
{
    tableView.separatorStyle= (dataArray.count>0)?UITableViewCellSeparatorStyleSingleLine:UITableViewCellSeparatorStyleNone;
    
    return dataArray.count;
}

- (FeatureModel*)bubbleTableView:(POPupTableView *)tableView dataForRow:(NSInteger)row;
{
    return [dataArray objectAtIndex:row];
}

#pragma mark - PopUp TableView Delegate
- (void) PopupTableSelectedIndex :(NSInteger)selectedRowNo
{
    FeatureModel *fdata = [dataArray objectAtIndex:selectedRowNo];
    
    if ([[fdata type]isEqualToString:@"private"]) {
        privateShowRoomId = [fdata ct_id];
        privateShowRoomName = [fdata name];
        
        isPrivateShowroomSelectd = YES;
    }
    else
    {
        isPrivateShowroomSelectd = NO;
        
    }
    if (selectedEntitytype == EntityTypeshowroom)
    {
        rmshowroomSelectState =  ([[fdata type]isEqualToString:@"private"])? RMSHOWROOM_PRIVATESELECTD:RMSHOWROOM_PUBLICSELECTD;
        NSString *showroomName = [self getShowrromWithoutSpecialCharacters:fdata.name];
        
        if (![self checkForshowroomIDexistance:showroomName] && showroomAddedArray.count<7) {
            isSelectFromPopUp = YES;

            [showroomAddedArray addObject:showroomName];
            [_txtCmtRemint wordSelectedForSuggestType:selectedEntitytype AndString:showroomName andID:fdata.ct_id forRange:selectedRange];
        }
        
    }
    else if (selectedEntitytype == EntityTypeMention)
    {
        if (![self checkForUserIDexistance:fdata.ct_id] && userNameAddedArray.count<7) {
            [userNameAddedArray addObject:fdata.ct_id];
            [_txtCmtRemint wordSelectedForSuggestType:selectedEntitytype AndString:fdata.name andID:fdata.ct_id forRange:selectedRange];
        }
        
    }
    else if (selectedEntitytype == EntityTypeHashTag)
    {
        if (![self checkForHashtagIDexistance:fdata.ct_id] && hashTagAddedArray.count<7) {
            [hashTagAddedArray addObject:fdata.ct_id];
            [_txtCmtRemint wordSelectedForSuggestType:selectedEntitytype AndString:fdata.name andID:fdata.ct_id forRange:selectedRange];
        }
        
    }
    [popoverController dismissPopoverAnimatd:YES];
}

-(NSString *)getShowrromWithoutSpecialCharacters : (NSString *) showroomData
{
    
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    NSString *showroomName = [[showroomData componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    return showroomName;
    
}

-(BOOL)checkForshowroomIDexistance:(NSString*)showroomID{
    BOOL status = NO;
    for (NSString *showID in showroomAddedArray) {
        if ([showID isEqualToString:showroomID]){
            return YES;
        }
    }
    return status;
    
}

-(BOOL)checkForUserIDexistance:(NSString*)userID{
    BOOL status = NO;
    for (NSString *c_id in userNameAddedArray) {
        if ([c_id isEqualToString:userID]){
            return YES;
        }
    }
    return status;
    
}

-(BOOL)checkForHashtagIDexistance:(NSString*)hashtagID{
    BOOL status = NO;
    for (NSString *c_id in hashTagAddedArray) {
        if ([c_id isEqualToString:hashtagID]){
            return YES;
        }
    }
    return status;
    
}

- (void)reloadSuggestionsWithType:(EntityType)suggestionType query:(NSString *)suggestionQuery range:(NSRange)suggestionRange
{
    
}

#pragma mark - API Implementation for @ Feature

- (void)getShowroomList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),name,pagenum] ;
    [self CallAPIforType:EntityTypeshowroom andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", SHOWROOM_LIST_API,submitData]]];
}

- (void)getUserList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),name,pagenum ];
    [self CallAPIforType:EntityTypeMention andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", USER_LIST_API,submitData]]];
}

- (void)getHashTagList:(NSString*)name withPagenum:(int)pagenum
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d&showroom_type=0",GETVALUE(CEO_AccessToken),name,pagenum];
    [self CallAPIforType:EntityTypeHashTag andAPIUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", HASHTAG_LIST_API,submitData]]];
}

-(void)CallAPIforType:(EntityType)type andAPIUrl:(NSURL *)myUrl
{
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
    //    [submitrequest setHTTPMethod:@"POST"];
    //    [submitrequest setHTTPBody:submitData];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         switch (type) {
             case EntityTypeMention:
                 dataArray =[EntitySupport UserlistDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
             case EntityTypeHashTag:
                 dataArray =[EntitySupport TagDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
             case EntityTypeshowroom:
                 dataArray =[EntitySupport showRoomDataWithDictioNary:[self dictionaryFromResponseData:data]];
                 break;
                 
             default:
                 break;
         }
         [self deleteExistingRecordWithModelData:type];
         selectedEntitytype = type;
         if (dataArray.count>0)
         {
             [self createTableviewAndShowPopup:type];
         }
         else
         {
             [popoverController dismissPopoverAnimatd:YES];
         }
     }];
}

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}

#pragma mark - YSCommentView Delegate Methods

- (void)beginSuggestingForTextView:(EntityType)textSuggestType query:(NSString *)suggestionQuery Inrange:(NSRange)range
{
    if (suggestionQuery.length<1)
        return;
    
    selectedRange = range;
    
    switch (textSuggestType)
    {
        case EntityTypeMention:
        {
            suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"@" withString:@""];
            if (userNameAddedArray.count!=7)
                [self getUserList:suggestionQuery withPagenum:1];
        }
            break;
        case EntityTypeHashTag:
        {
            suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"#" withString:@""];
            if (hashTagAddedArray.count!=7)
                [self getHashTagList:suggestionQuery withPagenum:1];
        }
            break;
        case EntityTypeshowroom:
        {
            if (!isPrivateShowroomSelectd)
            {
                suggestionQuery = [suggestionQuery stringByReplacingOccurrencesOfString:@"*" withString:@""];
                if (showroomAddedArray.count!=7)
                    [self getShowroomList:suggestionQuery withPagenum:1];
            }
        }
            break;
        default:
            break;
    }
}


-(void)hashTagSelected : (NSString *)selectedEnitity;
{
    

    
}
- (void)textDidChange:(STTweetView *)commentView
{
     lblTextCount.text = [NSString stringWithFormat:@"%d",25-(int)commentView.counterCount];
}

- (void)endSuggesting
{
    [popoverController dismissPopoverAnimatd:YES];
}


#pragma mark - @Feature text Deletion Methods
-(void)wordDeletedForSuggestType:(EntityType)textSuggestion andID:(NSString*)contactID
{
    if (textSuggestion == EntityTypeshowroom)
    {
        for (int as=0; as<showroomAddedArray.count; as++ )
        {
            NSString *ct_id = [showroomAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                if ([privateShowRoomId isEqualToString:contactID])
                {
                    privateShowRoomId = @"";
                    isPrivateShowroomSelectd = NO;
                }
                [showroomAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
    else if (textSuggestion == EntityTypeUserName) {
        for (int as=0; as<userNameAddedArray.count; as++ ){
            NSString *ct_id = [userNameAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                [userNameAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
    else if (textSuggestion == EntityTypeHashTag) {
        for (int as=0; as<hashTagAddedArray.count; as++ ){
            NSString *ct_id = [hashTagAddedArray objectAtIndex:as];
            if ([ct_id isEqualToString:contactID]){
                [hashTagAddedArray removeObjectAtIndex:as];
                break;
            }
        }
    }
    
}

-(void)deleteExistingRecordWithModelData:(EntityType)entityType
{
    NSMutableArray *anArray = [dataArray mutableCopy];
    
    if (entityType == EntityTypeshowroom) {
        for (NSString *c_id in showroomAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.name]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }
    else if (entityType == EntityTypeMention) {
        for (NSString *c_id in userNameAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.ct_id]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }
    else if (entityType == EntityTypeHashTag) {
        for (NSString *c_id in hashTagAddedArray) {
            for (FeatureModel *fmodel in anArray ){
                if ([c_id isEqualToString:fmodel.ct_id]){
                    [dataArray removeObject:fmodel];
                }
            }
        }
    }
}



- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    const CGFloat* colors = CGColorGetComponents( self.txtCmtRemint.textView.textColor.CGColor );
    NSLog(@"r=%f, g=%f, b=%f, a=%f", colors[0], colors[1], colors[2], colors[3]);
    
    const CGFloat* colors1 = CGColorGetComponents( [UIColor lightGrayColor].CGColor );
    NSLog(@"r=%f, g=%f, b=%f, a=%f", colors1[0], colors1[1], colors1[2], colors1[3]);
    
    NSString *serverString= [self.txtCmtRemint.textView.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    NSString *finalPost = [YSSupport stringByReplacingEmoji:serverString withDict:self.emojiDict];

    if ((colors<colors1 || colors1<colors) && finalPost.length>2)
    {
        if (_isEditRemint)
            [self editRemintMintTapped];
        else
            [self remintMintTapped];
    }
    else if (finalPost.length<3)
    {
        showAlert(@"Please add atleast 3 character in description", nil, @"OK", nil);
    }
    else
    {
        showAlert(@"Please add atleast 3 character in description", nil, @"OK", nil);
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    NSLog(@"delegate scrollView"); //this is dont'work
    
    [self.txtCmtRemint.textView resignFirstResponder];
}
#pragma mark - UIScrollViewDelegate

//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (isAPICalling) {
        return;
    }
    
    if (isDataFinisedFromServer) {
        return;
    }
    
    BOOL shouldLoadNextPage = ShouldLoadNextPage(tblViewRemint);
    if(shouldLoadNextPage)
    {
        isAPICalling = YES;
        pageNumber = pageNumber+1;
        [self getAllShowroomForMyShowTabWithPageNo:pageNumber];
    }
}


@end
