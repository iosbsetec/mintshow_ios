//
//  MSRemintVC.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 21/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundedRect.h"

@protocol RemintDelegate <NSObject>

-(void)remintedSucssesfully;
@optional
-(void)editRemintedSucssesfully:(NSString *)strRemintText;


@end
@interface MSRemintVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    // Add Description Cell
    IBOutlet UIImageView *imgUser;
    IBOutlet UITextView *txtRemint;
    
    IBOutlet UIView *tableHeader;
}

@property(nonatomic,retain)NSString *strThumbImgUrl,*categoryType,*mintType;
@property(nonatomic,retain)NSString *strImgUserUrl;
@property(nonatomic,retain)NSString *strDescription;
@property(nonatomic,retain)NSString *isFromInstagramTableView;
@property(nonatomic,assign)BOOL isEditRemint,isFromProfile;
@property(nonatomic,retain)NSString *strEditRemintText;
@property(nonatomic,retain)BaseFeedModel *feedData;
@property (nonatomic,assign) id <RemintDelegate> aDelegate;

@end
