//

//

#import <UIKit/UIKit.h>

@interface UITextField (Padding)

- (CGRect)textRectForBounds:(CGRect)bounds;
- (CGRect)editingRectForBounds:(CGRect)bounds;

@end
