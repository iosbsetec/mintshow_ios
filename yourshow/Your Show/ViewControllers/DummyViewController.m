//
//  DummyViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota on 20/10/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "DummyViewController.h"
#import "AddMintViewController.h"

@interface DummyViewController ()

@end

@implementation DummyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AddMintViewController *addMint = [[AddMintViewController alloc]initWithNibName:@"AddMintViewController" bundle:nil];
    addMint.delegate = self.myshow;
    addMint.isFromMintDetail = NO;
    addMint.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addMint animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
