//
//  ImageListViewController.h
//  Awesome Media Gallery
//
//  Created by Siba Prasad Hota on 30/03/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <Photos/PHAsset.h>

#import <UIKit/UIKit.h>

// define the protocol for the delegate
@protocol NewPhotoGalleryDelegate <NSObject>

// define protocol functions that can be used in any class using this delegate
-(void)selectedImageFromGallery:(UIImage *)selectedImage;
-(void)selectedVideoFromGallery:(NSData *)videoData AndURL:(NSURL *)localURl;

-(void)cancelPhotoGallery;
-(void)cameraImageTappedforPhotoGallery;

@end


@interface ImageListViewController : UIViewController

@property (nonatomic, assign) id <NewPhotoGalleryDelegate>  delegate;

@property(assign ,nonatomic)BOOL isShowCameraButton;
@property (nonatomic,assign)BOOL isMediaTypeImage;

@end

