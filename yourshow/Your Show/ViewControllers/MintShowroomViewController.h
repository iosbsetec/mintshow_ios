//
//  MintShowroomViewController.h
//  Your Show
//
//  Created by Siba Prasad Hota on 22/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MultiMintCollectionView,EntityModel;

@protocol ParticularShowroomDelegate

- (void)showroomJoinUnjoin:(NSString *)showTagName memberCount:(NSString *)row join:(BOOL)isJoiend;
@end
@interface MintShowroomViewController : UIViewController

@property (nonatomic, strong) NSString *selectedShowroom;
@property (strong, nonatomic) ShowRoomModel *modelShow;
@property (nonatomic, strong) NSString *sendersName;

@property (nonatomic, assign) BOOL isFromDeepLinking,isFromProfileShowroom;
@property (nonatomic, assign) BOOL isFromDiscoverShowroom;
@property (nonatomic, retain) id <ParticularShowroomDelegate> delegate;

@end
