//
//  TermsAndAgree.m
//  OnBoardDemo
//
//  Created by BseTec on 1/18/16.
//  Copyright © 2016 bsetec. All rights reserved.
//

#import "TermsAndAgree.h"

@interface TermsAndAgree ()<UIWebViewDelegate>
{
    IBOutlet UIWebView *webTermsView;
}
@end

@implementation TermsAndAgree

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/m/showroom/terms_conditions",ServerUrlSetting]];
    webTermsView.delegate = self;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webTermsView loadRequest:requestObj];
    // Do any additional setup after loading the view from its nib.
    [webTermsView setFrame:CGRectMake(0, 49, CGRectGetWidth([UIScreen mainScreen].bounds),  CGRectGetHeight([UIScreen mainScreen].bounds))];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark     Webview Delegate Methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:webTermsView animated:YES];
    [MBProgressHUD showHUDAddedTo:webTermsView animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:webTermsView animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
{
    [MBProgressHUD hideHUDForView:webTermsView animated:YES];
}

-(IBAction)btnBackTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
