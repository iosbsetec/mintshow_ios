//
//  CellSelectInterests.m
//  Your Show
//
//  Created by bsetec on 2/10/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "CellSelectInterests.h"

@implementation CellSelectInterests

- (void)awakeFromNib {
    
    _imgVwIntShwrmImage.layer.cornerRadius = 8.0f;
    _imgVwIntShwrmImage.layer.masksToBounds = YES;
    
    
    _viewCheckMark.layer.cornerRadius = 8.0f;
    _viewCheckMark.layer.masksToBounds = YES;
    _viewCheckMark.alpha = 0.0;
    _imgTickMark.hidden = YES;

    // Initialization code
}

@end
