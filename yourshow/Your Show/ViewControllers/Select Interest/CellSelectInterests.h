//
//  CellSelectInterests.h
//  Your Show
//
//  Created by bsetec on 2/10/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellSelectInterests : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVwIntShwrmImage;
@property (strong, nonatomic) IBOutlet UILabel *lblIntShwrmName;
@property (strong, nonatomic) IBOutlet UIView *viewCheckMark;
@property (strong, nonatomic) IBOutlet UIImageView *imgTickMark;
//@property (strong, nonatomic) IBOutlet UIButton *btnCheckMark;

@end
