//
//  VideoGalleryViewController.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 05/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol VideoGalleryProtocol <NSObject>

-(void)VideoGallerycancelTapped;

@end

@interface VideoGalleryVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>


@property (nonatomic, strong)NSMutableArray *videoData;


@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, assign) id <VideoGalleryProtocol> delegate;
@end
