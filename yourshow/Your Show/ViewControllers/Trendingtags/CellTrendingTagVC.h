//
//  CellTrendingTagVC.h
//  Your Show
//
//  Created by bsetec on 1/21/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellTrendingTagVC : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgViewTrendingTag;
@property (strong, nonatomic) IBOutlet UILabel *lblTrendingTag;

@end
