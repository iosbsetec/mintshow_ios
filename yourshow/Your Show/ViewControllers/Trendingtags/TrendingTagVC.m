//
//  TrendingTagVC.m
//  Your Show
//
//  Created by bsetec on 1/21/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "TrendingTagVC.h"
#import "TrendingTagHeaderView.h"

@interface TrendingTagVC ()
{
    
    IBOutlet UICollectionView *cllctnViewTrendingTags;
    NSArray *arrimages;
    NSArray *arrTitles;
    AppDelegate *delegateApp;
    IBOutlet UIButton *btnSearch;


}

@end

@implementation TrendingTagVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    cllctnViewTrendingTags.delegate = self;
    cllctnViewTrendingTags.dataSource = self;
    
    btnSearch.layer.cornerRadius = 4.0f;
    [btnSearch setClipsToBounds:YES];
    
    UINib *cellNib = [UINib nibWithNibName:@"CellTrendingTagVC" bundle:nil];
    [cllctnViewTrendingTags registerNib:cellNib forCellWithReuseIdentifier:@"cellidentifier"];
    
    
    [cllctnViewTrendingTags registerClass:[TrendingTagHeaderView class]
            forSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                   withReuseIdentifier:@"HeaderView"];
    
    arrimages = [NSArray arrayWithObjects:@"nature1.png",@"nature2.jpeg",@"nature3.png",@"nature5.jpeg",@"nature6.jpeg",@"nature1.png",@"nature2.jpeg",@"nature3.png",@"nature5.jpeg",@"nature6.jpeg",@"nature1.png",@"nature2.jpeg",@"nature3.png",@"nature5.jpeg",@"nature6.jpeg", nil];
    arrTitles  =  [NSArray arrayWithObjects:@"Zero to Hero",@"Love to Dance",@"Entrepreneurs",@"My best friend",@"Lovely",@"Zero to Hero",@"Love to Dance",@"Entrepreneurs",@"My best friend",@"Lovely",@"Zero to Hero",@"Love to Dance",@"Entrepreneurs",@"My best friend",@"Lovely",nil];
    
    [cllctnViewTrendingTags registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];

    // Do any additional setup after loading the view from its nib.
}


- (IBAction)backTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0)
        return 6;
    else
        return 15;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
        CellTrendingTagVC  *cell     =    [collectionView dequeueReusableCellWithReuseIdentifier:@"cellidentifier" forIndexPath:indexPath];
        cell.lblTrendingTag.text = arrTitles[indexPath.item];
        cell.imgViewTrendingTag.image = [UIImage imageNamed:[arrimages objectAtIndex:indexPath.row]];
        return cell;
       }
    


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(94, 94);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,4,0,4);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        
        if (reusableview==nil) {
            reusableview=[[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        }
        
        UILabel *lblTrendingTagheader =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
        if(indexPath.section == 0)
            lblTrendingTagheader.text =  @"  TOP MINTS";
        else if (indexPath.section == 1)
            lblTrendingTagheader.text =  @"  MOST RECENT";
    
        lblTrendingTagheader.font  = [UIFont fontWithName:@"Helvetica Neue" size:12];
        lblTrendingTagheader.textColor  = [UIColor lightGrayColor];
        lblTrendingTagheader.backgroundColor = [UIColor whiteColor];
        [reusableview addSubview:lblTrendingTagheader];
        return reusableview;
    }
    return nil;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    return CGSizeMake(0, 35);
    
    }



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
