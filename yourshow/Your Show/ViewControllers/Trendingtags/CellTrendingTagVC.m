//
//  CellTrendingTagVC.m
//  Your Show
//
//  Created by bsetec on 1/21/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "CellTrendingTagVC.h"

@implementation CellTrendingTagVC

- (void)awakeFromNib {
    _imgViewTrendingTag.layer.cornerRadius = 8.0f;
    _imgViewTrendingTag.layer.masksToBounds = YES;

    // Initialization code
}

@end
