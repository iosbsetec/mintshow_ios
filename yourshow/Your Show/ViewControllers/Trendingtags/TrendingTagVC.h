//
//  TrendingTagVC.h
//  Your Show
//
//  Created by bsetec on 1/21/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellTrendingTagVC.h"

@interface TrendingTagVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end
