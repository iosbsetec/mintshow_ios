//
//  LocationModel.h
//  GebeChat
//
//  Created by Siba Prasad Hota  on 6/5/15.
//  Copyright (c) 2015 WemakeAppz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LocationModel : NSObject

@property (nonatomic,strong)    NSString *searchedAddress;
@property (nonatomic,strong)    NSString *longitude;
@property (nonatomic,strong)    NSString *latitude;
@property (nonatomic, strong)   CLLocation *currentLocation;

@end
