//
//  MapViewController.m
//  HeyMD-iOS
//
//  Created by Siba Prasad Hota on 10/11/15.
//  Copyright © 2015 Lemonpeak. All rights reserved.
//

#import "MapViewController.h"
#import "MKMapView+ZoomLevel.h"
#import "LocationModel.h"
#import <MapKit/MapKit.h>
#import "AddressPickerViewController.h"

@interface MapViewController ()<CLLocationManagerDelegate,MKMapViewDelegate,AddressPickerDelegate>
{
    CGFloat currentlongitude;
    CGFloat currentlatitude;
    
    CGFloat gpslongitude;
    CGFloat gpsLatiitude;
    
    
    UIImageView     *_pickupLocationMarker;
    
    BOOL isSearched;

    
}
@property (nonatomic,retain)    CLLocationManager       *locationManager;
@property (nonatomic,retain)    IBOutlet MKMapView      *mapView;
@property (strong, nonatomic)   IBOutlet UIView         *pickupCalloutView;
@property (weak, nonatomic)     IBOutlet UITextField    *locationTextField;
@property (nonatomic , strong) LocationModel *selectedlocation;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isSearched = NO;
    
    currentlongitude = -87.62980;
    currentlatitude = 41.87811;
    
    gpsLatiitude = currentlatitude;
    gpslongitude = currentlongitude;
    
    self.locationManager = [[CLLocationManager alloc] init];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    else
        [self.locationManager requestAlwaysAuthorization];

    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate=self;
    [self.locationManager startUpdatingLocation];
    
    self.mapView.showsUserLocation = YES;
    //[self.mapView setUserTrackingMode:MKUserTrackingModeFollow];
    
    [self performSelector:@selector(addLocationCalloutView) withObject:nil afterDelay:0.1];
}



-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

-(UIImage *)takeScreenShot{
    CGSize screenSize = [[UIScreen mainScreen] applicationFrame].size;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(nil, screenSize.width, screenSize.height, 8, 4*(int)screenSize.width, colorSpaceRef, kCGImageAlphaPremultipliedLast);
    CGContextTranslateCTM(ctx, 0.0, screenSize.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    
    [(CALayer*)self.view.layer renderInContext:ctx];
    
    CGImageRef cgImage = CGBitmapContextCreateImage(ctx);
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    CGContextRelease(ctx);

    return image;
}




- (NSString *)deviceLocation {
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
    return theLocation;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    currentlongitude = newLocation.coordinate.longitude;
    currentlatitude = newLocation.coordinate.latitude;
    gpsLatiitude = currentlatitude;
    gpslongitude = currentlongitude;
    
    
    
    
   if(currentlongitude && currentlatitude) [self.locationManager stopUpdatingLocation];
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    if (isSearched) {
        return;
    }
    currentlongitude = mapView.centerCoordinate.longitude;
    currentlatitude  = mapView.centerCoordinate.latitude;
    
    self.selectedlocation = [LocationModel new];
    self.selectedlocation.longitude=[NSString stringWithFormat:@"%2f",currentlongitude];
    self.selectedlocation.latitude=[NSString stringWithFormat:@"%2f",currentlatitude];
    self.selectedlocation.currentLocation = [[CLLocation alloc] initWithLatitude:gpsLatiitude longitude:gpslongitude];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:currentlatitude longitude:currentlongitude];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count > 0) {
            CLPlacemark *placemark = placemarks[0];
            self.locationTextField.text = [NSString stringWithFormat:@"%@", [self stringForPlacemark:placemark]];
            self.selectedlocation.searchedAddress = self.locationTextField.text;
        }
    }];

}





-(void)zoomtoLocationWithlongitude:(double)longitude andLatitude:(double)latitude{
    
    CLLocationCoordinate2D center;
    center.longitude = longitude;
    center.latitude = latitude;
    //center.latitude -= self.mapView.region.span.latitudeDelta * 0.40;
    [self.mapView setCenterCoordinate:center zoomLevel:13 animated:YES];
    
}

-(void)didSetUserLocation{
    NSLog(@"clicked on location");
}

-(void)aaddressPickingDidFinish:(LocationModel *)searchedLocation {
    isSearched =YES;
    self.selectedlocation = searchedLocation;
    NSLog(@"picked address = %@",searchedLocation.searchedAddress);
    currentlongitude = [searchedLocation.longitude floatValue];
    currentlatitude = [searchedLocation.latitude floatValue];
    self.locationTextField.text = searchedLocation.searchedAddress;
    [self zoomtoLocationWithlongitude:currentlongitude andLatitude:currentlatitude];
    [self.delegate aaddressPickingDidFinish:self.selectedlocation];
    [self performSelector:@selector(gobackToAddMintScreen) withObject:nil afterDelay:0.4];

}


- (IBAction)currentLocationClicked:(id)sender {
    currentlongitude = gpslongitude;
    currentlatitude = gpsLatiitude;
    [self zoomtoLocationWithlongitude:gpslongitude andLatitude:gpsLatiitude];
}

-(void)gobackToAddMintScreen{
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSString *)stringForPlacemark:(CLPlacemark *)placemark {
    
    NSArray *lines = placemark.addressDictionary[ @"FormattedAddressLines"];
    NSString *addressString = [lines componentsJoinedByString:@","];
    NSLog(@"Address: %@", addressString);
    if (addressString) {
        return addressString;
    }
    
    NSMutableString *string = [[NSMutableString alloc] init];
    if (placemark.locality) {
        [string appendString:placemark.locality];
    }
    
    if (placemark.administrativeArea) {
        if (string.length > 0)
            [string appendString:@", "];
        [string appendString:placemark.administrativeArea];
    }
    
    if (string.length == 0 && placemark.name)
        [string appendString:placemark.name];
    
    return string;
}

- (IBAction)gotobackScreen:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)addLocationCalloutView{
    
    UIImage *pin_default = [UIImage imageNamed:@"pin_green"];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    int pinX = screenBounds.size.width / 2 - pin_default.size.width / 2;
    int pinY = screenBounds.size.height / 2 - pin_default.size.height;
    
    _pickupLocationMarker = [[UIImageView alloc] initWithFrame:CGRectMake(pinX, pinY, pin_default.size.width, pin_default.size.height)];
    _pickupLocationMarker.image = pin_default;
    [self.view addSubview:_pickupLocationMarker];
    
    [self.view addSubview:self.pickupCalloutView];
    self.pickupCalloutView.center = _pickupLocationMarker.center;

    CGRect mainFrame = self.pickupCalloutView.frame;
    mainFrame.origin.y-=5;
    self.pickupCalloutView.frame = mainFrame;
    
    self.pickupCalloutView.alpha=1.0;
    _pickupLocationMarker.alpha=1.0;
}






-(void)updateSetPickup {
    // set pickup
    NSLog(@"Set pickup clicked");
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.locationTextField resignFirstResponder];
    AddressPickerViewController *pickit = [[AddressPickerViewController alloc]initWithNibName:@"AddressPickerViewController" bundle:nil];
    pickit.screenShot = [self takeScreenShot];
    pickit.delegate=self;
    [self.navigationController pushViewController:pickit animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)setPickupClicked:(id)sender {
    CLLocationCoordinate2D center;
    center.longitude = currentlongitude;
    center.latitude = currentlatitude;
    //center.latitude -= self.mapView.region.span.latitudeDelta * 0.40;
    [self.mapView setCenterCoordinate:center zoomLevel:15 animated:YES];
    
    self.selectedlocation = [LocationModel new];
    self.selectedlocation.searchedAddress = self.locationTextField.text;
    self.selectedlocation.longitude=[NSString stringWithFormat:@"%2f",center.longitude];
    self.selectedlocation.latitude=[NSString stringWithFormat:@"%2f",center.latitude];
    self.selectedlocation.currentLocation = [[CLLocation alloc] initWithLatitude:gpsLatiitude longitude:gpslongitude];
    
    [self.delegate aaddressPickingDidFinish:self.selectedlocation];
    [self performSelector:@selector(gobackToAddMintScreen) withObject:nil afterDelay:0.9];
    
}

@end


