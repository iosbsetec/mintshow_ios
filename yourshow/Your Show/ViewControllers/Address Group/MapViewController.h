//
//  MapViewController.h
//  HeyMD-iOS
//
//  Created by Siba Prasad Hota on 10/11/15.
//  Copyright © 2015 Lemonpeak. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LocationModel;

@protocol mapPickerDelegate;


@interface MapViewController : UIViewController

@property (nonatomic, assign) id <mapPickerDelegate> delegate;
- (IBAction)gotobackScreen:(id)sender;

@end



@protocol mapPickerDelegate
@required
-(void)aaddressPickingDidFinish:(LocationModel *)searchedLocation;
@end