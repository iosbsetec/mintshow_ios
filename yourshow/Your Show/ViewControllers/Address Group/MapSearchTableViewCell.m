//
//  MapSearchTableViewCell.m
//  HeyMD-iOS
//
//  Created by Siba Prasad Hota on 14/01/16.
//  Copyright © 2016 Lemonpeak. All rights reserved.
//

#import "MapSearchTableViewCell.h"

@implementation MapSearchTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setAddressDetails:(NSDictionary *)addressData{
    self.AddressTitle.text = [addressData valueForKey:kAddressTitleString];
    self.addressSubTitle.text = [addressData valueForKey:kAddressSubtitleString];
    [self.addressTypeImageView setImage:([[addressData valueForKey:kAddressTYpeString] intValue]==0)?[UIImage imageNamed:@"historyIconSearch"]:[UIImage imageNamed:@"mapiconSearch"]];
}


@end
