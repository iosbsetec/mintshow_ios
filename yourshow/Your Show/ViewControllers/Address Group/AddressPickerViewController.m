//
//  AddressPickerViewController.m
//  GebeChat
//
//  Created by Siba Prasad Hota  on 5/22/15.
//  Copyright (c) 2015 WemakeAppz. All rights reserved.
//

#import "AddressPickerViewController.h"
#import <MapKit/MapKit.h>
#import "GooglePlacesDataLoaderDelegate.h"
#import "MKMapView+ZoomLevel.h"
#import "PlaceDetailsAnnotation.h"
#import "CurrentLocationAnnotation.h"
#import "GoogleKeys.h"
#import "GooglePlacesDataLoader.h"
#import "MapSearchTableViewCell.h"
#import "UserAccessSession.h"


NSString * const CURRENT_LOCATION_CELL_ID = @"currentLocationCell";
NSString * const GOOGLE_PLACES_CELL_ID = @"googlePlacesCell";
NSInteger const CURRENT_LOCATION_CELL_INDEX = 0;


@interface AddressPickerViewController ()< MKMapViewDelegate,UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate,GooglePlacesDataLoaderDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *searcheddataTable;
@property (weak, nonatomic) IBOutlet MKMapView *searchedLocation;
@property (weak, nonatomic) IBOutlet UITextField *locationSearchBar;

@property (nonatomic, strong) id<MKAnnotation> locationAnnotation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;

@property (nonatomic, strong) NSTimer *searchCountdownTimer;
@property (nonatomic, strong) GooglePlacesDataLoader *dataLoader;
@property (nonatomic, strong) UIActivityIndicatorView *searchProgressView;
@property (nonatomic, strong) NSArray *autocompletePredictions;
@property (weak, nonatomic) IBOutlet UIView *backGroundView;

@property (nonatomic, strong) NSMutableArray *recentSearchedLocations;


@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
- (IBAction)selectLocationComplete:(id)sender;
- (IBAction)gotobackScreen:(id)sender;

@end

@implementation AddressPickerViewController

@synthesize locationSearchBar = _locationSearchBar;
@synthesize locationManager = _locationManager;
@synthesize currentLocation = _currentLocation;
@synthesize dataLoader = _dataLoader;
@synthesize searchCountdownTimer = _searchCountdownTimer;
@synthesize searchProgressView = _searchProgressView;
@synthesize autocompletePredictions = _autocompletePredictions;
@synthesize searchedLocation = _searchedLocation;
@synthesize locationAnnotation = _locationAnnotation;

- (void)viewDidLoad {
    [super viewDidLoad];
    [_locationSearchBar addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    self.dataLoader = [[GooglePlacesDataLoader alloc] initWithDelegate:self];
    self.searchProgressView =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.searchProgressView.frame = CGRectMake(0, 0, 32, 32);
    self.searchProgressView.hidesWhenStopped = YES;
    self.selectedLocation = [LocationModel new];
    [self updateCurrentLocation];
    
    self.backImageView.image = self.screenShot;
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor whiteColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        blurEffectView.opaque = 0.55;
        blurEffectView.alpha=0.65;
        [self.backGroundView addSubview:blurEffectView];
    }
    self.recentSearchedLocations = [UserAccessSession getStoredLocationHistory];

    // Do any additional setup after loading the view from its nib.
}




-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.locationSearchBar becomeFirstResponder];
}



-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    //this is the space
    if (section==0 && self.autocompletePredictions.count)
        return 10.0;
    else
        return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    CGFloat heighty =  (section==0 && self.autocompletePredictions.count)?10.0:1.0;
    UIView *aview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.searcheddataTable.frame.size.width, heighty)];
    aview.backgroundColor = [UIColor clearColor];
    return aview;
}// custom view for footer. will be adjusted to default or specified footer height


#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.searcheddataTable setUserInteractionEnabled:NO];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
     if (indexPath.section==1) {
         NSDictionary *prediction = [self.recentSearchedLocations objectAtIndex:[indexPath row]];
         NSString *referenceID = [prediction objectForKey:RESPONSE_KEY_REFERENCE];
         [self.dataLoader cancelAllRequests];
         [self.dataLoader sendDetailRequestWithReferenceID:referenceID];
     }else{
         NSDictionary *prediction = [self.autocompletePredictions objectAtIndex:[indexPath row]];
         NSString *referenceID = [prediction objectForKey:RESPONSE_KEY_REFERENCE];
         [self.dataLoader cancelAllRequests];
         [self.dataLoader sendDetailRequestWithReferenceID:referenceID];
         [UserAccessSession storeLocationData:prediction];
     }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MapSearchTableViewCell *cell = (MapSearchTableViewCell *)[self.searcheddataTable dequeueReusableCellWithIdentifier:@"MapSearchTableViewCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MapSearchTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGRect framecell = cell.frame;
    framecell.size.width = self.searcheddataTable.frame.size.width;
    cell.frame= framecell;
    
    
    NSInteger arrayCount = (indexPath.section==1)?self.recentSearchedLocations.count:self.autocompletePredictions.count;
    
    if(indexPath.row==0 && arrayCount==1){
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds
                                         byRoundingCorners:(UIRectCornerTopRight|UIRectCornerTopLeft|UIRectCornerBottomRight|UIRectCornerBottomLeft)
                                               cornerRadii:CGSizeMake(7.0, 7.0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = cell.bounds;
        maskLayer.path = maskPath.CGPath;
        cell.layer.mask = maskLayer;
    }else if(indexPath.row==0){
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds
                                         byRoundingCorners:(UIRectCornerTopRight|UIRectCornerTopLeft)
                                               cornerRadii:CGSizeMake(7.0, 7.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = cell.bounds;
        maskLayer.path = maskPath.CGPath;
        cell.layer.mask = maskLayer;
    }else if(indexPath.row == (arrayCount-1) || (indexPath.row == 4)){
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds
                                         byRoundingCorners:(UIRectCornerBottomRight|UIRectCornerBottomLeft)
                                               cornerRadii:CGSizeMake(7.0, 7.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = cell.bounds;
        maskLayer.path = maskPath.CGPath;
        cell.layer.mask = maskLayer;
    }
    
    
    cell.backgroundColor = [UIColor whiteColor];
    
    NSDictionary *adict = (indexPath.section==1)?[self.recentSearchedLocations objectAtIndex:indexPath.row]:[self locationDescriptionAtIndex:[indexPath row]];
    NSString *titleString = [adict objectForKey:RESPONSE_KEY_DESCRIPTION];
    NSArray *addresArray = [titleString componentsSeparatedByString:@","];
    
    NSString *finalTitle = (addresArray.count>0)?[addresArray objectAtIndex:0]:@"";
    
    NSString *finalSubTitle = @"";
    
    for(int i=1; i<addresArray.count;i++){
        finalSubTitle = [finalSubTitle stringByAppendingString:[addresArray objectAtIndex:i]];
        if (i<addresArray.count-1)
            finalSubTitle = [finalSubTitle stringByAppendingString:@","];
    }
    
    if (indexPath.section==1) {
        [cell setAddressDetails:[NSDictionary dictionaryWithObjectsAndKeys:finalTitle,kAddressTitleString,finalSubTitle,kAddressSubtitleString,@"0",kAddressTYpeString, nil]];
    }else{
        [cell setAddressDetails:[NSDictionary dictionaryWithObjectsAndKeys:finalTitle,kAddressTitleString,finalSubTitle,kAddressSubtitleString,@"1",kAddressTYpeString, nil]];
    }
    return cell;


}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==1) {
        return self.recentSearchedLocations.count;
    }
    return self.autocompletePredictions.count; // offset by 1 to account for "Current location" row
}

-(void)setFrameOfTableview{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect Tframe = self.searcheddataTable.frame;
    CGFloat heightEachRow = 50.0;
    CGFloat totalHeight = heightEachRow * self.autocompletePredictions.count + heightEachRow * self.recentSearchedLocations.count;
    self.searcheddataTable.frame = CGRectMake(Tframe.origin.x, Tframe.origin.y, Tframe.size.width, totalHeight);
    [UIView commitAnimations];

}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    self.autocompletePredictions = [NSArray new];
    [self.searcheddataTable reloadData];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self keyboardWillShow];
    [self updateCurrentLocation];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    [self keyboardWillHide];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self.dataLoader cancelAllRequests];
}

-(void)textFieldDidChange:(UITextField *)textField
{
    [self startCountdownTimerForSearchString:textField.text andLocation:self.currentLocation];
}

- (void)startCountdownTimerForSearchString:(NSString *)searchString andLocation:(CLLocation *)location {
    
    //stop the current countdown
    if (self.searchCountdownTimer) {
        
        [self.searchCountdownTimer invalidate];
    }
    
    //cancel all pending requests
    [self.dataLoader cancelAllRequests];
    
    NSDate *fireDate = [NSDate dateWithTimeIntervalSinceNow:1.0];
    
    // add search data to the userinfo dictionary so it can be retrieved when the timer fires
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          searchString, @"searchString",
                          location, @"location",
                          nil];
    self.searchCountdownTimer = [[NSTimer alloc] initWithFireDate:fireDate
                                                         interval:0
                                                           target:self
                                                         selector:@selector(searchCountdownTimerFired:)
                                                         userInfo:info
                                                          repeats:NO];
    
    [[NSRunLoop mainRunLoop] addTimer:self.searchCountdownTimer forMode:NSDefaultRunLoopMode];
}

- (void)searchCountdownTimerFired:(NSTimer *)countdownTimer {
    
    NSString *searchString = [countdownTimer.userInfo objectForKey:@"searchString"];
    CLLocation *location = [countdownTimer.userInfo objectForKey:@"location"];
    [self.dataLoader sendAutocompleteRequestWithSearchString:searchString andLocation:location];
    
    [self.searchProgressView startAnimating];
    
}

#pragma mark - GooglePlacesDataLoaderDelegate methods

- (void)googlePlacesDataLoader:(GooglePlacesDataLoader *)loader didLoadAutocompletePredictions:(NSArray *)predictions {
    self.autocompletePredictions = predictions;
    //[self setFrameOfTableview];
    [self.searcheddataTable reloadData];
    [self.searchProgressView stopAnimating];
}

- (void)googlePlacesDataLoader:(GooglePlacesDataLoader *)loader didLoadPlaceDetails:(NSDictionary *)placeDetails withAttributions:(NSString *)htmlAttributions{
    
    //   [self setActive:NO animated:YES];
    
    [self searchDidCompleteWithPlaceDetails:placeDetails andAttributions:htmlAttributions];
    
    [self.searchProgressView stopAnimating];
}

- (void)googlePlacesDataLoader:(GooglePlacesDataLoader *)loader autocompletePredictionsDidFailToLoad:(NSError *)error {
    [self.searchProgressView stopAnimating];
}

- (void)googlePlacesDataLoader:(GooglePlacesDataLoader *)loader placeDetailsDidFailToLoad:(NSError *)error {
    [self.searchProgressView stopAnimating];
}
- (NSDictionary *)locationDescriptionAtIndex:(NSInteger)index {
    
    NSDictionary *jsonData = [self.autocompletePredictions objectAtIndex:index];
    return jsonData;
}




#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    self.currentLocation = newLocation;
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    //[self setFrameOfTableview];
    [self.searcheddataTable reloadData];
}

#pragma mark - Public methods

- (void)updateCurrentLocation {
    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
    }
    [self.locationManager startUpdatingLocation];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)searchDidCompleteWithPlaceDetails:(NSDictionary *)placeDetails andAttributions:(NSString *)htmlAttributions {
    
    if (self.locationAnnotation) {
        [self.searchedLocation removeAnnotation:self.locationAnnotation];
        self.searchedLocation.showsUserLocation = NO;
    }
    
    self.locationAnnotation = [[PlaceDetailsAnnotation alloc] initWithPlaceDetails:placeDetails];
    
    [self.searchedLocation addAnnotation:self.locationAnnotation];
    [self.searchedLocation setCenterCoordinate:self.locationAnnotation.coordinate zoomLevel:14 animated:YES];
    [self.searchedLocation selectAnnotation:self.locationAnnotation animated:NO];
    
    self.selectedLocation.searchedAddress = [placeDetails valueForKey:@"formatted_address"];

    self.selectedLocation.longitude=[NSString stringWithFormat:@"%2f",self.locationAnnotation.coordinate.longitude];
    self.selectedLocation.latitude=[NSString stringWithFormat:@"%2f",self.locationAnnotation.coordinate.latitude];
    self.selectedLocation.currentLocation = [[CLLocation alloc] initWithLatitude:self.locationAnnotation.coordinate.latitude longitude:self.locationAnnotation.coordinate.longitude];
    
    [self.delegate aaddressPickingDidFinish:self.selectedLocation];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchDidCompleteWithCurrentLocationSelected:(CLLocation *)currentLocation {
    if (currentLocation) {
        self.locationAnnotation = [[CurrentLocationAnnotation alloc] initWithLocation:currentLocation];
        self.selectedLocation.longitude=[NSString stringWithFormat:@"%2f",currentLocation.coordinate.longitude];
        self.selectedLocation.latitude=[NSString stringWithFormat:@"%2f",currentLocation.coordinate.latitude];
        self.selectedLocation.currentLocation = currentLocation;
        [self.searchedLocation addAnnotation:self.locationAnnotation];
        [self.searchedLocation setCenterCoordinate:self.locationAnnotation.coordinate zoomLevel:14 animated:YES];
        [self.searchedLocation selectAnnotation:self.locationAnnotation animated:NO];
    } else {
        [self showAlertWithTitle:@"No location found" Description:@"There was a problem getting the current location."];
    }
}


-(void)showAlertWithTitle:(NSString*)title Description:(NSString*)description{
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:title  message:description  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)selectLocationComplete:(id)sender
{
   
}

- (IBAction)gotobackScreen:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


#define kOFFSET_FOR_KEYBOARD 80.0

-(void)dismissAllKeyBoard{
    [self.view endEditing:YES];
    [self keyboardWillHide];
}
-(void)keyboardWillShow {
    // Animate the current view out of the way
    [self setViewMovedUp:YES];
    [self performSelector:@selector(scrolltoBottomOfTableView) withObject:nil afterDelay:0.5];
}

-(void)scrolltoBottomOfTableView{
    if ([self.searcheddataTable numberOfRowsInSection:1]<2) {
        return;
    }
}

-(void)keyboardWillHide {
    [self setViewMovedUp:NO];
}

-(void)setViewMovedUp:(BOOL)movedUp{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect Tablerect = self.searcheddataTable.frame;
    if(movedUp){
        Tablerect.size.height = self.view.frame.size.height- 270;
    }else{
        Tablerect.size.height =  self.view.frame.size.height- 45;
    }
    self.searcheddataTable.frame = Tablerect;
    
    [UIView commitAnimations];
}



@end
