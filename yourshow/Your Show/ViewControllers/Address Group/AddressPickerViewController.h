//
//  AddressPickerViewController.h
//  GebeChat
//
//  Created by Siba Prasad Hota  on 5/22/15.
//  Copyright (c) 2015 WemakeAppz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationModel.h"

@protocol AddressPickerDelegate;

@interface AddressPickerViewController : UIViewController

@property (nonatomic, assign) id <AddressPickerDelegate> delegate;
@property (nonatomic, strong) LocationModel *selectedLocation;
@property (nonatomic,retain) NSString *longitude;
@property (nonatomic,retain) UIImage *screenShot;


@end






@protocol AddressPickerDelegate
@required
-(void)aaddressPickingDidFinish:(LocationModel *)searchedLocation;
@end