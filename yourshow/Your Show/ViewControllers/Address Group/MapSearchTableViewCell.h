//
//  MapSearchTableViewCell.h
//  HeyMD-iOS
//
//  Created by Siba Prasad Hota on 14/01/16.
//  Copyright © 2016 Lemonpeak. All rights reserved.
//

#import <UIKit/UIKit.h>


#define kAddressTitleString     @"AddressTitle"
#define kAddressSubtitleString  @"AddressSubTitle"
#define kAddressTYpeString      @"AddressTypeString"



@interface MapSearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *AddressTitle;
@property (weak, nonatomic) IBOutlet UILabel *addressSubTitle;
@property (weak, nonatomic) IBOutlet UIImageView *addressTypeImageView;

-(void)setAddressDetails:(NSDictionary *)addressData;

@end
