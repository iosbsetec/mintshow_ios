//
//  CurrentLocationAnnotation.m
//  ADMapkitSearchExample
//
//  Created by David Stemmer on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CurrentLocationAnnotation.h"

@interface CurrentLocationAnnotation()

@property (nonatomic, strong, readwrite) CLLocation *currentLocation;

@end

@implementation CurrentLocationAnnotation 

@synthesize currentLocation = _currentLocation;

- (id)initWithLocation:(CLLocation *)currentLocation {
    if (self = [super init]) {
        self.currentLocation = currentLocation;
    }
    return self;
}

- (NSString *)title {
    return @"Current Location";
}

- (CLLocationCoordinate2D)coordinate {
    return self.currentLocation.coordinate;
}

@end
