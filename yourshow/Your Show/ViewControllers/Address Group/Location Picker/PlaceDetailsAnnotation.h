//
//  PlaceDetailsAnnotation.h
//  ADMapkitSearchExample
//
//  Created by David Stemmer on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PlaceDetailsAnnotation : NSObject <MKAnnotation>

@property (nonatomic, strong, readonly) NSDictionary *placeDetails;


- (id)initWithPlaceDetails:(NSDictionary *)placeDetails;


@end
