
//
//  MintShowroomViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota on 22/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "MintShowroomViewController.h"
#import "AsyncImageView.h"
#import "YSCollectionView.h"
#import "YSCollectionViewDataSource.h"
#import "EmptyTableViewCell.h"
#import "AddEditShowroomVC.h"
#import "MSShareVC.h"
#import "ViewingMembersVC.h"
#import "UIViewController+CWPopup.h"
#import "CommentsViewController.h"
#import "MSRemintVC.h"
#import "MSMintDetails.h"
#import "YSSupport.h"
#import "MSProfileVC.h"
#import "JitenCollectionView.h"
#import "CoachingTip.h"
#import "UsersListView.h"
#import "UserAccessSession.h"
#import "MSPSignatureShowroomVC.h"

#define kDefaultImagePagerHeight 375.0f
#define kDefaultTableViewHeaderMargin 95.0f
#define kDefaultImageAlpha 1.0f
#define kDefaultImageScalingFactor 450.0f

#define NAVBAR_CHANGE_POINT 15

@interface MintShowroomViewController ()<YSCollectionViewDataSource,YSCollectionViewDelegate,AddEditShowroomVCDelegate,RemintDelegate,MSMintDetailsDelegate,JitenCollectionTableDelegate,JitenCollectionViewDataSource,ViewingMembersDelegate,UsersListDelegate>
{
    IBOutlet UILabel *transperentviewLabel;
    CGFloat currentAlphaValue;

    NSMutableArray *allMintsdata;
    
    // For related showroom List
    
    NSMutableArray *allShowRoomdata;
    BOOL isShowRoomLoaded;
    int pagenumberShowroom;
    CGFloat multiShowroomHeight;
    NSInteger nTotalShowRoomCount;

    
    BOOL isLoading,isDataFinishedInServer;
    int pagenumber;
    CGFloat MultiMintHeight;
    
    CGRect cachedImageViewSize;
   
    IBOutlet UIView *viewNoMoreMints;
    IBOutlet UIView *viewHeader;
    IBOutlet UIImageView *imgHeader;
    IBOutlet UIButton *btnInvite1;
    IBOutlet UIButton *btnInvite;
    IBOutlet UILabel *showRoomDescriptionLabel;
    IBOutlet UILabel *lblshowTag;
    IBOutlet UILabel *lblUserName;

    
    IBOutlet UIImageView *imgStar;
    IBOutlet UILabel *lblMintsTitle;
    IBOutlet UILabel *lblFollowerTitle;
    
    // Private Showroom Page

    IBOutlet UILabel *lblInviterName;
    IBOutlet UILabel *lblPrivateShowroomName;
    IBOutlet UIView *viewPrivateHolder;
    
    
    NSString *showroom_link;
    
    ShowRoomModel *shareModel;
}

@property (strong, nonatomic) IBOutlet JitenCollectionView *showRoomCollectionview;
@property (strong, nonatomic) IBOutlet YSCollectionView *aView;
@property (weak, nonatomic) IBOutlet   UITableView *multiMintTableview;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@property (nonatomic, strong) IBOutlet UIButton *btnFollow;
@property (nonatomic, strong) IBOutlet UIButton *btnFollow1;

@property (strong, nonatomic) IBOutlet UIView *detailsView;

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *showroomNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *showroomNameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *mintsCountlabel;
@property (weak, nonatomic) IBOutlet UILabel *followerscountLabel;

@property (weak, nonatomic) IBOutlet UIView *customNavBarView;

@property (nonatomic,assign) CGRect initialTableRect;
@property (nonatomic,assign) CGRect initialHeaderRect;
@property (nonatomic,assign) NSInteger kHiddenPixels;

- (IBAction)gobacktoPrevieousScreen:(id)sender;
-(IBAction)gotoShowroomOwnerProfileScreen;

@end
 
@implementation MintShowroomViewController

- (void)viewDidLoad {
    
    shareModel = [[ShowRoomModel alloc]init];
    [super viewDidLoad];
    [self.customNavBarView setAlpha:0];
    
    if (isSmallPad) {
        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        [self.view setFrame:appDel.window.frame];
        
    }
    [_customNavBarView setBackgroundColor:[UIColor colorWithRed:9.0/255.0 green:26.0/255.0  blue:53.0/255.0  alpha:1.0]];
    currentAlphaValue = 0.37;
    transperentviewLabel.alpha = currentAlphaValue;

    self.initialTableRect = self.multiMintTableview.frame;
    self.initialHeaderRect = self.customNavBarView.frame;
    self.kHiddenPixels = self.customNavBarView.frame.size.height;
  
    _userImageView.layer.borderColor    = [UIColor colorWithRed:203.0/255.0 green:203.0/255.0  blue:203.0/255.0  alpha:1.0].CGColor;//[UIColor lightGrayColor].CGColor;
    _userImageView.layer.borderWidth    = 2.0;
    _userImageView.layer.cornerRadius = _userImageView.frame.size.width/2;
    [_userImageView setClipsToBounds:YES];
    
    _btnFollow.layer.cornerRadius = 4.0;
    _btnFollow1.layer.cornerRadius = 4.0;

    
    pagenumber          = 1;
    nTotalShowRoomCount = 0;
    isShowRoomLoaded    = NO;
    pagenumberShowroom  = 1;
    isDataFinishedInServer = NO;

    multiShowroomHeight = 500;
    MultiMintHeight     = 500;

    
    

    [self initializetableheaderView];
    [self initializer];
    isLoading = NO;
    
    if (_modelShow)
    {
        shareModel = _modelShow;
        [self hideStaticDatas:NO];
        [self setScreenDetails:_modelShow];

    }
    else
    {
        [viewHeader setAlpha:0.3];
        [self hideStaticDatas:NO];
        [self getShowroomDetails:_selectedShowroom];
    }
    
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissMemberList:) name:@"CLosePopupNow" object:nil];

    
    if ([_modelShow.privacy_settings isEqualToString:@"on" ] && ![_modelShow.user_name isEqualToString: GETVALUE(CEO_UserName)])
    {
        btnInvite.hidden = YES;
        btnInvite1.hidden = YES;

    }
    
    else if([_modelShow.is_user_joined isEqualToString:@"NO"])
    {
        btnInvite.hidden = YES;
        btnInvite1.hidden = YES;

    }


}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    
}
-(void)viewDidAppear:(BOOL)animated
{

    [super viewDidAppear:animated];
    
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;

    if (aDelegate.isCommentActionUpdate) {
        [self getParticularShowrromMintswithPageNumber:1 deleted:YES];
        aDelegate.isCommentActionUpdate = NO;
    }
}
 -(void)privateShowRoomView
{
    [self.view addSubview:viewPrivateHolder];
    NSDictionary *attributesUserName = @{NSForegroundColorAttributeName:[UIColor darkGrayColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:14.0]};
    
     NSDictionary *attributesYou = @{NSForegroundColorAttributeName:[UIColor darkGrayColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:14.0]};
    
    
    NSMutableAttributedString *labelAttributes = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ invited You to join",self.sendersName]];
    [labelAttributes addAttributes:attributesUserName range:NSMakeRange(0, [_sendersName length])];
    [labelAttributes addAttributes:attributesYou range:NSMakeRange([_sendersName length]+9, 3)];

    
    lblPrivateShowroomName.text = [NSString stringWithFormat:@"%@ Showroom",self.selectedShowroom];
    lblInviterName.attributedText = labelAttributes;
}
-(void)hideStaticDatas:(BOOL)isHidden
{
    [_imageView setHidden:isHidden];
    [imgStar setHidden:isHidden];
    [_userImageView setHidden:isHidden];
    [btnInvite setHidden:isHidden];
    [btnInvite1 setHidden:isHidden];
    [lblFollowerTitle setHidden:isHidden];
    [lblMintsTitle setHidden:isHidden];
    [_mintsCountlabel setHidden:isHidden];
    [_followerscountLabel setHidden:isHidden];
    [lblMintsTitle setHidden:isHidden];
    [_btnFollow setHidden:isHidden];
    [_btnFollow1 setHidden:isHidden];
    [lblshowTag setHidden:isHidden];
    [_showroomNameLabel setHidden:isHidden];
    [lblUserName setHidden:isHidden];
}

- (void)dismissMemberList : (NSNotification *) notify{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
        }];
    }
}

-(void)setScreenDetails:(ShowRoomModel *)model
{
    _selectedShowroom = model.show_tag;

    if ([model.mint_count integerValue]>0){
        [self getParticularShowrromMintswithPageNumber:1 deleted:NO];
    }
    
    NSDictionary *attributesUserName = @{NSForegroundColorAttributeName:ORANGECOLOR, NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:14.0]};
    NSMutableAttributedString *labelAttributes = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Created By %@",model.user_name]];
    [labelAttributes addAttributes:attributesUserName range:NSMakeRange([@"Created By " length], [model.user_name length])];
    
    
    lblUserName.attributedText      = labelAttributes;
    _showroomNameLabel.text         = model.showroom_name;
    _showroomNameLabel1.text        = model.showroom_name;
    lblshowTag.text                 = [NSString stringWithFormat:@"%@",model.show_tag];
    showRoomDescriptionLabel.text   = model.showroom_description;
    [self resizeDescriptionLblHeighWithDesc:model.showroom_description];
        _mintsCountlabel.text       = [NSString stringWithFormat:@"%@",model.mint_count];
    _followerscountLabel.text       = [NSString stringWithFormat:@"%@",model.member_count];
    
    if ([model.show_option isEqualToString:@"owner"])
    {
        [_btnFollow setTitle:@"EDIT" forState:UIControlStateNormal];
        [_btnFollow1 setTitle:@"EDIT" forState:UIControlStateNormal];
    }
    else
    {

        if ([model.is_user_joined isEqualToString:@"NO"])
        {
            
            if ([GETVALUE(CEO_PARTCULARSHOWROOMFIRSTTIMEVISIT) isEqualToString:@"yes"])
            {
//                SETVALUE(@"no",CEO_PARTCULARSHOWROOMFIRSTTIMEVISIT);
//                SYNCHRONISE;
//                [CoachingTip showCoachingTip:[[UIApplication sharedApplication] keyWindow] ForScreen:@"ParticularShowroomJoin" headerPresent:NO];
                
                CoachingTip *viewCoach = [[CoachingTip alloc]initWithFrame:[UIScreen mainScreen].bounds screenName:@"ParticularShowroomJoin" headerPresent:NO];
                [self.view addSubview:viewCoach];
                [UserAccessSession setShowroomFirstTimeVisitCompleted];

            }
            [self setJoinButtonBGColor:_btnFollow filledBG:YES];
            [self setJoinButtonBGColor:_btnFollow1 filledBG:YES];

            [_btnFollow setTitle:@"JOIN" forState:UIControlStateNormal];
            [_btnFollow1 setTitle:@"JOIN" forState:UIControlStateNormal];
            
            
            
        }
        else
        {
            [_btnFollow setTitle:@"JOINED" forState:UIControlStateNormal];
            [_btnFollow1 setTitle:@"JOINED" forState:UIControlStateNormal];
            [self setJoinButtonBGColor:_btnFollow filledBG:NO];
            [self setJoinButtonBGColor:_btnFollow1 filledBG:NO];
        }
    
    }
    
    if ([_modelShow.privacy_settings isEqualToString:@"on" ] && ![_modelShow.user_name isEqualToString: GETVALUE(CEO_UserName)])
    {
        btnInvite.hidden = YES;
        btnInvite1.hidden = YES;

    }
    else if([_modelShow.is_user_joined isEqualToString:@"NO"])
    {
        btnInvite.hidden = YES;
        btnInvite1.hidden = YES;
    }



    imgHeader.mintImageURL = [NSURL URLWithString:model.showroom_img_Url];
   // imgHeader.contentMode = UIViewContentModeBottom ;
    _imageView.mintImageURL = [NSURL URLWithString:model.showroom_imageSmall];
    _imageView.mintImageURL = [NSURL URLWithString:model.showroom_img_Url];

    _userImageView.mintImageURL = [NSURL URLWithString:model.user_thumb_image_url];
    
    if ([model.mint_count integerValue]==0)
    {
        [self showNoMoreMints];
    }
    else
    {
        [viewNoMoreMints setHidden:YES];
    }
}

-(void)setJoinButtonBGColor:(UIButton *)joinBtn filledBG:(BOOL)filled
{
    if(filled)
    {
        [joinBtn setBackgroundColor:ORANGECOLOR];
        [joinBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        [joinBtn setBackgroundColor:[UIColor clearColor]];
        [joinBtn setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
        joinBtn.layer.borderColor = [[UIColor colorWithRed:(243/255.0f) green:(94/255.0f) blue:(27/255.0f) alpha:1.0]CGColor];
        joinBtn.layer.borderWidth = 1.3f;
        joinBtn.layer.masksToBounds = YES;
    }
}

-(void)showNoMoreMints
{
    CGRect rectView = viewNoMoreMints.frame;
    rectView.origin.y = viewHeader.frame.size.height;
    [viewNoMoreMints setFrame:rectView];
    [_multiMintTableview addSubview:viewNoMoreMints];
}
- (IBAction)followEditTapped:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:@"EDIT"]) {
        
        AddEditShowroomVC *samplePopupViewController = [[AddEditShowroomVC alloc] initWithNibName:@"AddEditShowroomVC" bundle:nil];
        samplePopupViewController.isFromShowRoomVC = YES;
        samplePopupViewController.modelShow = _modelShow;
        samplePopupViewController.aDelegate = self;
        samplePopupViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:samplePopupViewController animated:YES];
    }
    else
    {
        if ([viewPrivateHolder superview]) {
            [viewPrivateHolder removeFromSuperview];
        }
        [self joinTapped:nil];
    
    }
}
-(IBAction)acceptInvitationTapped:(id)sender
{
    [self joinTapped:nil];
}
- (IBAction)viewMembersTapped:(id)sender {
    ViewingMembersVC *viewingMembers = [[ViewingMembersVC alloc] initWithNibName:@"ViewingMembersVC" bundle:nil];
    viewingMembers.showroomName = lblshowTag.text;
    viewingMembers.is_user_joined = _modelShow.is_user_joined;
    viewingMembers.user_name =  _modelShow.user_name;
    viewingMembers.privacy_settings = _modelShow.privacy_settings;

    viewingMembers.delegate = self;
    NSLog(@"showroom id ==%@", viewingMembers.showroomName);
    viewingMembers.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewingMembers animated:NO];
}

-(void) selectedFriendsUserId:(NSString *)userId
{
    [[self navigationController] popViewControllerAnimated:NO];
    [self gotoProfileView:userId];
}
- (void)showroomAddedOrEditedSuccesFullyShowRoomVc:(NSString *)showroomDescription showroomImage:(UIImage *)showroomImage showroomImageUrl:(NSString *)url showroomName:(NSString *)showroomName showroomPrivacy:(NSString *)privacySetting privacyType:(NSString *)privacyType
{
    _modelShow.showroom_img_Url = url;
    _modelShow.privacy_settings = ([privacySetting isEqualToString:@"on"])?@"on":@"off";
    _modelShow.showroom_type = privacyType;
    _modelShow.showroom_description = showroomDescription;
    _modelShow.showroom_name = showroomName;
    showRoomDescriptionLabel.text = _modelShow.showroom_description;
    _showroomNameLabel.text =  _modelShow.showroom_name;
    _showroomNameLabel1.text =  _modelShow.showroom_name;

    _imageView.image = showroomImage;
    [self resizeDescriptionLblHeighWithDesc:showroomDescription];
}

-(void)resizeDescriptionLblHeighWithDesc:(NSString *)strDesc
{
    CGRect recttxtLbl  = [strDesc boundingRectWithSize:CGSizeMake(showRoomDescriptionLabel.frame.size.width-8, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:12.0]} context:nil];
    
    CGRect rectLbl = showRoomDescriptionLabel.frame;
    rectLbl.size.height = (recttxtLbl.size.height>45)?45:recttxtLbl.size.height;
    [showRoomDescriptionLabel setFrame:rectLbl];
}

-(void)initializetableheaderView
{
    [self.imageView setUserInteractionEnabled:YES];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    
    CGRect rctImage = self.imageView.frame;
    rctImage.size.height = viewHeader.frame.size.height-16;
    [self.imageView setFrame:rctImage];
    cachedImageViewSize = self.imageView.frame;
    
    // Need to add
    CGRect rctTransparent = transperentviewLabel.frame;
    rctTransparent.size.height = viewHeader.frame.size.height+2;
    [transperentviewLabel setFrame:rctTransparent];

}
-(void)initializer
{
    // MULTI MINT COLLECTION VIEW
    self.aView.backgroundColor                  = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.multiMintTableview.backgroundColor                  = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];

    self.view.backgroundColor = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    
    self.aView.showsHorizontalScrollIndicator   = NO;
    //self.aView.scrollEnabled                    = NO;
    self.aView.ysDataSource              = self;
    self.aView.ysDelegate                = self;
    self.aView.scrollEnabled = NO;
    
    allMintsdata = [[NSMutableArray alloc]init];
    
    // MULTI SHOW ROOM  COLLECTION VIEW
    self.showRoomCollectionview.CollectionDataSource    = self;
    self.showRoomCollectionview.collectiondelegate      = self;
    self.showRoomCollectionview.Addshowroom             = NO;
    self.showRoomCollectionview.backgroundColor         = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.showRoomCollectionview.showsHorizontalScrollIndicator = NO;
    self.showRoomCollectionview.showsVerticalScrollIndicator = NO;
    self.showRoomCollectionview.scrollEnabled           = NO;
    self.showRoomCollectionview.relatedShowRoom         = YES;
    allShowRoomdata = [[NSMutableArray alloc]init];
}


#pragma mark - THIS CODE FOR RELATED SHOWROOM COLLECTION VIEW

-(void)ReloadShowroomCollectionView
{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.showRoomCollectionview performBatchUpdates:^{
        [self.showRoomCollectionview reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
            multiShowroomHeight = self.showRoomCollectionview.collectionViewLayout.collectionViewContentSize.height;
            self.showRoomCollectionview.frame = CGRectMake(0,10, self.view.frame.size.width,self.view.frame.size.height);
            [self reloadTableviewWithsection:0];
        }
    }];
}

-(void)reloadTableviewWithsection:(NSInteger)section
{
    [self.multiMintTableview reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:section], nil]  withRowAnimation:UITableViewRowAnimationNone];
}

-(JitenCollectionView *)showroomCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.showRoomCollectionview;
}
- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    
    return  (ShowRoomModel *)[allShowRoomdata objectAtIndex:row];
    // return  nil;
}
- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView{
    NSLog(@"NUMBER OF SHOWROOMS %ld",(unsigned long)allShowRoomdata.count);
    return allShowRoomdata.count;
    //return 6;
}

#pragma mark JITEN COLLECTION DID SELECT CELL METHOD

- (void) JitenCollectionViewDelegateMethod: (ShowRoomModel *) modeldata selectedIndex :(NSInteger) selectedRowNo
{
    shareModel = modeldata;
    self.showRoomCollectionview.alpha = 0.0;
    [self hideStaticDatas:NO];
    [self setScreenDetails:modeldata];
    isShowRoomLoaded = NO;
    [self.multiMintTableview reloadData];
}

#pragma mark - **** SHOWROOM COLLECTION VIEW END *****

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return   1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (isShowRoomLoaded)?35:0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (isShowRoomLoaded)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
        [view setBackgroundColor:[UIColor whiteColor]];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        label.textColor=[UIColor blackColor];
        [label setText:@"  RELATED SHOWROOM"];
        [label setBackgroundColor:[UIColor whiteColor]];
        [view addSubview:label];
        return view;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isShowRoomLoaded == 0 )
        return MultiMintHeight+200;
    else
        return multiShowroomHeight+20;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MultiMintCollectionTableViewCell";
    
    EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    for(id aview in [cell.contentView subviews])
    {
        [aview removeFromSuperview];
    }
    
    
    if (isShowRoomLoaded) {
        [cell.contentView addSubview:self.showRoomCollectionview];
    }
    else
    {
        [cell.contentView addSubview:self.aView];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

#pragma mark-  THIS CODE FOR MULTI MINT

-(YSCollectionView *)discoverCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.aView;
}

-(void)multiMintCollectionViewClicked:(YSCollectionView *)sender AtRow:(NSInteger)row
{
    
}

- (BaseFeedModel *)multiMintCollectionViewView:(YSCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    return [allMintsdata objectAtIndex:row];
}



- (NSInteger)NumberOFSectionsMultiMintView:(YSCollectionView *)collectionView{
    return 1;
}


- (NSInteger)NumberOFItemsForMultiMintView:(YSCollectionView *)collectionView forSection:(NSInteger)section {
    return  allMintsdata.count;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollViewd
{
    CGFloat y = -scrollViewd.contentOffset.y;
    
    if (y > 0){
        self.imageView.frame = CGRectMake(0, scrollViewd.contentOffset.y, cachedImageViewSize.size.width+y, cachedImageViewSize.size.height+y);
        self.imageView.center = CGPointMake(self.view.center.x, self.imageView.center.y);
        transperentviewLabel.frame = self.imageView.frame;
        if(y<2){
            currentAlphaValue=0.37;
        }else{
            currentAlphaValue = 0.45+ (y/500.0);
        }
        transperentviewLabel.alpha= currentAlphaValue;
    }else {
        CGFloat offsetY = scrollViewd.contentOffset.y;
        CGFloat alpha = MIN(1, 1 - ((NAVBAR_CHANGE_POINT + 110 - offsetY) / 84));
        if (offsetY > NAVBAR_CHANGE_POINT)  {
            [self.customNavBarView setAlpha:alpha];
        } else  {
            [self.customNavBarView setAlpha:alpha];
        }
    }
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)aScrollView {
    return self.imageView;
}

//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self.multiMintTableview);
    if (shouldLoadNextPage)
    {
        
        if ([_modelShow.mint_count integerValue]>0)
        {
            if(!isLoading && !isDataFinishedInServer && [_mintsCountlabel.text integerValue]>[allMintsdata count])
            {
                isLoading = YES;
                pagenumber = pagenumber+1;
                [self getParticularShowrromMintswithPageNumber:pagenumber deleted:NO];
            }
        }
    }
}

#pragma mark - Star Feature
- (void)selectedFeatureType:(int )featureType withString:(NSString *)tagString
{
    if ([allMintsdata count]) {
        [allMintsdata removeAllObjects];
        [self batchreloadCollectionView];
    }
    _selectedShowroom = tagString;
    [self.customNavBarView setAlpha:0];
    [self getShowroomDetails:[_selectedShowroom stringByReplacingOccurrencesOfString:@"*" withString:@""]];
}



- (void)selectedFeatureType:(int )featureType withString:(NSString *)selectedEnitity isPrivateShowroomAccess:(BOOL)access{
    switch (featureType) {
        case 0:
            [self goToMintdetails:NO mintId:nil navigationHeaderText:selectedEnitity];

            break;
        case 1:
        {
            if(access) {
                showAlert(@"It's a private showroom", nil, @"Ok", nil);
            }
            else
                [self selectedFeatureType:1 withString:selectedEnitity];
        }
            break;
        case 2:case 44:
            [self gotoProfileView:selectedEnitity];
            break;
        case 88:
            [self gotoSignatureShowroom:selectedEnitity];
            break;
        default:
            break;
    }
    
}



-(void)gotoSignatureShowroom:(NSString *)mintType
{
    
    MSPSignatureShowroomVC *vcShowRoom = [[MSPSignatureShowroomVC alloc]initWithNibName:@"MSPSignatureShowroomVC" bundle:nil];
    vcShowRoom.isFromDiscoverShowroom = NO;
    vcShowRoom.selectedShowroom = mintType;
    [self.navigationController pushViewController:vcShowRoom animated:YES];
    
    
}
-(void)buttonActionclickedFortype:(int)typeVal forRow:(NSInteger)row feedModelData:(BaseFeedModel *)data {
    switch (typeVal) {
    }
}


- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndex = [NSString stringWithFormat:@"%ld",(long)row];
    if(buttonType == YSButtonTypeComplimint)
    {
        CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
        comentVc.commentCount              = [data.feed_comment_count intValue];
        comentVc.achievementdetails        = data ;
        comentVc.feedId                    = data.feed_id ;
       // comentVc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:comentVc animated:YES];
    }
    else if (buttonType == YSButtonTypeHifiveCount)  {
        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeHighFive numberofPeoples:[data.feed_like_count intValue] feedId:data.feed_id delegate:self];
    }
    
    else if (buttonType == YSButtonTypeInspire)
    {
        NSLog(@"Remint screen ");
        MSRemintVC *reminytVW               = [[MSRemintVC alloc]initWithNibName:@"MSRemintVC" bundle:nil];
        reminytVW.aDelegate = self;
        reminytVW.feedData                  = data;
        reminytVW.isFromInstagramTableView  =   @"0";
        [self presentViewController:reminytVW animated:NO completion:^{
            
        }];
    }
    else if (buttonType == YSButtonTypedetails)
    {
        [self goToMintdetails:YES mintId:[NSString stringWithFormat:@"%d",row]  navigationHeaderText:_showroomNameLabel.text];
    }
    else if (buttonType == YSButtonTypeUserImage)
    {
        [self showPopupWithModeldata:data withrow:row isRemint:NO];
    }
}
-(void)selectedUserIdFromHighfiveList:(NSString *)selectdUserId
{
    
    [self gotoProfileView:selectdUserId];
    
}
-(void)goToMintdetails: (BOOL)isforShowroomMintDetails mintId:(NSString *)mintIdVal navigationHeaderText:(NSString *)textVal
{

    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mintArrayDetails = allMintsdata;
    mintDetailsVC.mint_row_id = mintIdVal;
    
    //BOOL isOwner =     (remint)?feeddetails.remint_user_id:feeddetails.feed_user_id]];

    mintDetailsVC.isMintOwner = NO;
    mintDetailsVC.Selected_row_value = [mintIdVal integerValue];
    if (isforShowroomMintDetails) {
        mintDetailsVC.isFromParticularShowroom = YES;
    }
    else
            mintDetailsVC.isFromTrendingTags = YES;
    
    mintDetailsVC.delegate = self;
    mintDetailsVC.showtagText = lblshowTag.text;
    mintDetailsVC.navigationHeaderText = textVal;
    mintDetailsVC.strShowroomImageUrl = _modelShow.showroom_image;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];




}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [[self.view.window viewWithTag:2233]removeFromSuperview];
}

-(void)showPopupWithModeldata:(BaseFeedModel *)feeddetails withrow:(NSInteger)row isRemint:(BOOL)remint {
   
    [self gotoProfileView:[NSString stringWithFormat:@"%@",(remint)?feeddetails.remint_user_id:feeddetails.feed_user_id]];

}

-(void)gotoProfileView:(NSString *)selectedEnitity{
    MSProfileVC *prof  = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    [self.navigationController pushViewController:prof animated:YES];
    
}

-(IBAction)gotoShowroomOwnerProfileScreen{
    MSProfileVC *prof  = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = _modelShow.showroom_createdby_user_id;
    [self.navigationController pushViewController:prof animated:YES];
    
}
-(void)getMintsMyshow:(int)pageNumber isDelete : (BOOL) isDelete pull_request_time:(BOOL)pullRequestNeedToAdd
{
    
    if (![YSSupport isNetworkRechable]) {
        return;
    }
    isLoading = YES;
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:@"0" forKey:@"mint_list_type"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page_number"];
    
    [YSAPICalls getMintsForMyShowTabwithDict:dicts SuccessionBlock:^(id newResponse){
        Generalmodel *gmodel = newResponse;
        if ([gmodel.status_code integerValue]){
            if([gmodel.tempArray count]>0){
                if (isDelete) {
                    [allMintsdata removeAllObjects];
                }
                [allMintsdata addObjectsFromArray: gmodel.tempArray];
                [self batchreloadCollectionView];
            }
            else{
                isLoading = NO;

            }
        }
    } andFailureBlock:^(NSError *error){
        isLoading = NO;

        NSLog(@"Failed from the server %@",[error description]);
    }];
}


-(void)batchreloadCollectionView{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.aView performBatchUpdates:^{
        [self.aView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:^(BOOL finished) {
        if (finished) {
            MultiMintHeight = self.aView.collectionViewLayout.collectionViewContentSize.height;
            self.aView.frame = CGRectMake(0, 5, self.view.frame.size.width,MultiMintHeight+400);
            [UIView setAnimationsEnabled:animationsEnabled];
            [self.multiMintTableview beginUpdates];
            [self.multiMintTableview endUpdates];
            [self.multiMintTableview reloadData];
            isLoading = NO;
        }
    }];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gobacktoPrevieousScreen:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES ];
}




#pragma mark - Server API Calls

-(void)getParticularShowrromMintswithPageNumber: (int)pageNumber deleted:(BOOL)isDeleted
{
    if (![YSSupport isNetworkRechable]) {
        return;
    }
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    _selectedShowroom = [_selectedShowroom stringByReplacingOccurrencesOfString:@"*" withString:@""];
    [dicts setValue:_selectedShowroom forKey:@"showroom_tag"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page_number"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [YSAPICalls getParticularShowroom:dicts ForSuccessionBlock:^(id newResponse)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         Generalmodel *gmodel = newResponse;
         isLoading = NO;

         if ([gmodel.status_code isEqualToString:@"1"])    // GET RELATED Mints
         {
             isShowRoomLoaded = NO;
             self.aView.alpha = 1.0;
             self.showRoomCollectionview.alpha = 0.0;
             if (isDeleted) {
                 [allMintsdata removeAllObjects];
                 [self batchreloadCollectionView];
             }
             
             if([gmodel.tempArray count]>0)
             {
                 isDataFinishedInServer = NO;
                 [allMintsdata addObjectsFromArray: gmodel.tempArray];
                 [self batchreloadCollectionView];
             }
             else
             {
                 isDataFinishedInServer = YES;
             }
         }
         if ([gmodel.status_code isEqualToString:@"2"])   // GET RELATED SHOWROOM LIST
         {
             isShowRoomLoaded = YES;
             
             self.aView.alpha = 0.0;
             self.showRoomCollectionview.alpha = 1.0;
             
             if([gmodel.tempArray count]>0)
             {
                 isLoading = NO;
                 if (pagenumberShowroom == 1) {
                     [allShowRoomdata removeAllObjects];
                 }
                 
                 [allShowRoomdata addObjectsFromArray:gmodel.tempArray];
                 nTotalShowRoomCount =  [gmodel.totalpages integerValue];
                 [viewNoMoreMints removeFromSuperview];
                 [self ReloadShowroomCollectionView];
             }
             else
             {
                 isLoading = NO;
             }
         }
     }
                      andFailureBlock:^(NSError *error) {
                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                          NSLog(@"Failed from the server %@",[error description]);
                          isLoading = NO;

                          
                      }];
}

-(IBAction)joinTapped : (UIButton *) button
{
    
    if (![YSSupport isNetworkRechable]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&inviter_id=0&showroom_tag=%@",GETVALUE(CEO_AccessToken),_selectedShowroom] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_JOINSHOWROOM]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
  
    NSString *submitVal = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submitData val %@",submitVal);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSDictionary *showroomDict = [self dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"StatusMsg"]);
         [MBProgressHUD hideHUDForView:self.view animated:YES];

         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
              AppDelegate *delegateApp = MINTSHOWAPPDELEGATE
             delegateApp.showroomJoienedCount = [[showroomDict valueForKey:@"count"] integerValue];
             delegateApp.isAnyShowroomJoinUnjoin = YES;
             (delegateApp.showroomJoienedCount == 0)?SETVALUE(@"no", CEO_ISJOINEDSHOWROOM):SETVALUE(@"yes", CEO_ISJOINEDSHOWROOM);
             SYNCHRONISE;

            
             NSLog(@"Joiend properly");
             if ([[showroomDict valueForKey:@"follow_message"] isEqualToString:@"true"])
             {
                 _followerscountLabel.text = [NSString stringWithFormat:@"%d",[_followerscountLabel.text intValue]+1];
                 [self setJoinButtonBGColor:_btnFollow filledBG:NO];
                 [self setJoinButtonBGColor:_btnFollow1 filledBG:NO];
                 [_btnFollow setTitle:@"JOINED" forState:UIControlStateNormal];
                 [_btnFollow1 setTitle:@"JOINED" forState:UIControlStateNormal];
                 btnInvite.hidden = NO;
                 btnInvite1.hidden = NO;
                 if (_isFromDiscoverShowroom || _isFromProfileShowroom) {
                     [self.delegate showroomJoinUnjoin:lblshowTag.text memberCount:_followerscountLabel.text join:YES];
                 }
                 
             }
             
             else
             {
                 _followerscountLabel.text = [NSString stringWithFormat:@"%d",[_followerscountLabel.text intValue]-1];

                 if (_isFromDiscoverShowroom || _isFromProfileShowroom) {
                     [self.delegate showroomJoinUnjoin:lblshowTag.text memberCount:_followerscountLabel.text join:NO];
                 }
                 if ([_modelShow.privacy_settings isEqualToString:@"on"])
                 {
                     [self showNoContentPopUp];
                 }
                 btnInvite.hidden  = YES;
                 btnInvite1.hidden = YES;
                 [self setJoinButtonBGColor:_btnFollow filledBG:YES];
                 [self setJoinButtonBGColor:_btnFollow1 filledBG:YES];
                 [_btnFollow setTitle:@"JOIN" forState:UIControlStateNormal];
                 [_btnFollow1 setTitle:@"JOIN" forState:UIControlStateNormal];
             }
         }
         else
         {

         }
     }];
}

-(void)showNoContentPopUp
{
    UIView *rootView = [[[NSBundle mainBundle] loadNibNamed:@"NoContent" owner:self options:nil] objectAtIndex:0];
    UIView *containerView = [[[NSBundle mainBundle] loadNibNamed:@"NoContent" owner:self options:nil] lastObject];
    [rootView addSubview:containerView];
    rootView.tag = 2233;

    //For Return Myshow button
    UIButton *btnRetMyShow =(UIButton *)[containerView viewWithTag:35];
    [btnRetMyShow addTarget:self action:@selector(onReturnMyShow) forControlEvents:UIControlEventTouchUpInside];
    
    //For Close button
    UIButton *btnClose =(UIButton *)[containerView viewWithTag:45];
    [btnClose addTarget:self action:@selector(onClosePopup) forControlEvents:UIControlEventTouchUpInside];
    
    [containerView viewWithTag:25].layer.cornerRadius = 5.0;
    [containerView viewWithTag:25].layer.borderColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.6].CGColor;
    [containerView viewWithTag:25].layer.borderWidth = 1.0;
    [self.view addSubview:rootView];
}


-(void)onClosePopup
{
    [[self.view viewWithTag:2233]removeFromSuperview];
    NSLog(@"Close Tapped");
}

-(void)onReturnMyShow
{
    [[self.view viewWithTag:2233]removeFromSuperview];
    [self gobacktoPrevieousScreen:nil];
    NSLog(@"Return Tapped");
}

-(void)showshare :(NSString *)titleMsg
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:titleMsg
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    [alert show];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}

- (void)getShowroomDetails:(NSString *)showroomName // Getting the information about the showroom like showroomname,showroomimage,MInt/members count
{
    
    if (![YSSupport isNetworkRechable]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&showroom_tag=%@",GETVALUE(CEO_AccessToken),showroomName] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,API_GETSHOWROOMDETAILS,submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    
    NSLog(@"submitDta123 data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         [MBProgressHUD hideHUDForView:self.view animated:YES];

         id info = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
         
         NSLog(@"Showroom Info %@",(NSDictionary *)info);
         
         
         if ([[info valueForKey:@"is_mint_streak"] isEqualToString:@"yes"]) {
             AppDelegate *obj = MINTSHOWAPPDELEGATE;
             [obj createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];

            // [AppDelegate createStreakPopup:[NSString stringWithFormat:@"%d",[[info valueForKey:@"login_user_streak"] intValue]]];
         }
         
         

         if ([[info valueForKey:@"Status"] isEqualToString:@"Failed"])
         {
             _showroomNameLabel1.text = _selectedShowroom;
             _imageView.backgroundColor = [UIColor lightGrayColor];
             showRoomDescriptionLabel.text = @"SHOWROOM NOT FOUND";
             showRoomDescriptionLabel.font = [UIFont fontWithName:@"Helvetica Bold" size:14.0];
             [self hideStaticDatas:YES];
             [self getParticularShowrromMintswithPageNumber:1 deleted:NO];
         }
         else
         {
             [self hideStaticDatas:NO];
             [viewHeader setAlpha:1.0];

             ShowRoomModel *gmodel = [[ShowRoomModel alloc]initWithShowRoomDict:info];
             _modelShow            = gmodel ;
             shareModel = _modelShow;

             if ([_modelShow.privacy_settings isEqualToString:@"on"] && [_modelShow.is_user_joined isEqualToString:@"NO"]) {
                 
                 [self showNoContentPopUp];
             }
             else
             [self setScreenDetails:gmodel];
             
             /*
              * Check if We came to this screen via deeplinking(via Email) or from the activity page (via Application)
              * The showroom creator  should not be the Logged in user
              * The logged in user should not joiend to this showroom
              * Then we will show the screen for accepting this invitation
              */
             
                 if (_isFromDeepLinking  && ![gmodel.show_option isEqualToString:@"owner"] && [gmodel.is_user_joined isEqualToString:@"NO"]) {
                     [self privateShowRoomView];
                 }
             
         }
         
         
     }];
}

- (void)deleteShowroom : (NSString *) showroomName
{
    NSDictionary *dict= [NSDictionary dictionaryWithObjectsAndKeys:showroomName,@"KShowroomName",nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteShowRoom"object:nil userInfo:dict];
    [self gobacktoPrevieousScreen:nil];
}

-(IBAction)shareShowroom
{
    MSShareVC *vc        = [[MSShareVC alloc]initWithNibName:@"MSShareVC" bundle:nil];
    [vc setCurrentScreen:1];
    vc.txtDescription   = showRoomDescriptionLabel.text;
    vc.linkUrl           = _modelShow.showroom_link;
    vc.isFromMintDetail  = NO;
    vc.showroomName      = _showroomNameLabel.text;
    vc.showTag  = lblshowTag.text;
    vc.showroomdata = shareModel;
    if ([_modelShow.privacy_settings isEqualToString:@"on"])
    {
        vc.isShowroomPrivate = YES;
    }
    else
        vc.isShowroomPrivate = NO;
 
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}

-(void)highFiveMintOfmintDetails :(NSString *)mintId andCurerntValue : (NSString *)activityValue countValue: (NSString *)value
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    int i = 0;
    
    for (BaseFeedModel *model in allMintsdata)
    {
        if ([model.feed_id isEqualToString:mintId])
        {
            [model setIs_user_liked:[activityValue intValue]];
            [model setFeed_like_count:value];
            //            [allMintsdata replaceObjectAtIndex:i withObject:model];
            
            [allMintsdata replaceObjectAtIndex:[aDelegate.selectedIndex integerValue] withObject:model];
            [_aView performBatchUpdates:^{
                [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:[aDelegate.selectedIndex integerValue] inSection:0]]];
            } completion:^(BOOL finished) {
            }];
            
            break;
        }
        i++;
    }
}


-(void)commentAddedSuccesFully:(NSInteger) row
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    if ([aDelegate.selectedIndex integerValue]>=allMintsdata.count) {
        return;
    }
    
    @try {
        //        [_aView reloadData];
        [_aView performBatchUpdates:^{
            [ _aView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]]];
        } completion:^(BOOL finished) {
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    @finally {
        
    }
    
}

-(void)remintedSucssesfully
{
    [self getParticularShowrromMintswithPageNumber:1 deleted:YES];
}

//For Mute mint, Delete Mint. FlagInappropriate
-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId
{
    int mintIndex = 0;
    BOOL isMintFoundFromList = NO;

    for (int i = 0; i<allMintsdata.count; i++) {
       
        
            if ([[[allMintsdata objectAtIndex:i]feed_id] isEqualToString:mintId]) {
                mintIndex = i;
                isMintFoundFromList = YES;
                break;
            }
        
    }
    
    if (isMintFoundFromList) {
        
        [allMintsdata removeObjectAtIndex:mintIndex];
        _mintsCountlabel.text       = [NSString stringWithFormat:@"%d",[allMintsdata count]];

        [self batchreloadCollectionView];
    }


}
//Update the tableview if the MintUser Mint exist
-(void)muteUserOfmintDetails:(NSString *)muteUser{
    
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[allMintsdata count]; currentIndex++){
        //do stuff with obj
        BaseFeedModel *feedlist = [allMintsdata objectAtIndex:currentIndex];
        if([muteUser isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [allMintsdata removeObjectsAtIndexes:indexesToDelete];
        _mintsCountlabel.text       = [NSString stringWithFormat:@"%d",[allMintsdata count]];
        [self batchreloadCollectionView];
    }
}


@end
