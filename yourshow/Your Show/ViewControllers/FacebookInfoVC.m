//
//  FacebookInfoVC.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 11/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "FacebookInfoVC.h"
#import "UITextField+Padding.h"
@interface FacebookInfoVC ()
{
    __weak IBOutlet UIImageView *userImage;
    __weak IBOutlet UITextField *txtFldFirstname;
    __weak IBOutlet UITextField *txtFldLastName;
    __weak IBOutlet UITextField *txtFldEmailId;
}
@end

@implementation FacebookInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
        [self setDesign:txtFldEmailId];
        [self setDesign:txtFldLastName];
        [self setDesign:txtFldFirstname];
        userImage.layer.cornerRadius = 40;
        userImage.clipsToBounds = YES;
    
        txtFldFirstname.text  = _strFBUserFirstName;
        txtFldLastName.text   = _strFBUserLastName;
        txtFldEmailId.text    = _strFBUserEmailId;
        userImage.mintImageURL    = [NSURL URLWithString:_strFBUserImage];
}
- (IBAction)disMissKeyboard:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setDesign : (UITextField *)yourTextField
{
    yourTextField.clipsToBounds = YES;
    yourTextField.layer.cornerRadius = 5.0f;
    yourTextField.layer.borderWidth = 1.0;
    yourTextField.layer.borderColor = [UIColor lightGrayColor].CGColor;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)acceptTapped:(UIButton *)sender {
    
    if ([self checkUserGivenDetails]) {
        [self cancelClicked:nil];
    if ([self.delegate respondsToSelector:@selector(signUpwith:lastName:emailId:)]) {
        [self.delegate signUpwith:txtFldFirstname.text lastName:txtFldLastName.text emailId:txtFldEmailId.text];
    }
        
    }
}
-(IBAction)cancelClicked:(id)sender
{
   [self dismissViewControllerAnimated:YES completion:^{
       
   }];

}



-(BOOL)checkUserGivenDetails
{
    if (txtFldEmailId.text.length==0 && txtFldFirstname.text.length==0 && txtFldLastName.text.length == 0)
    {
        showAlert(@"Error", @"All field are mandatory", @"Ok", nil);
        return NO;
    }
   
    if (txtFldFirstname.text.length == 0)
    {
        showAlert(@"Error", @"firstname cannot be blank", @"Ok", nil);
        return NO;
    }
    else if  (txtFldLastName.text.length == 0)
    {
        showAlert(@"Error", @"lastname cannot be blank", @"Ok", nil);
        return NO;
    }
    else if  (txtFldEmailId.text.length == 0)
    {
        showAlert(@"Error", @"email cannot be blank", @"Ok", nil);
        return NO;
    }
    if (![YSSupport validateEmail:txtFldEmailId.text])
    {
        showAlert(@"Error", @"Invalid email", @"Ok", nil);
        return NO;
    }
    return YES;
}

- (IBAction)continueWithemailPressed:(UIButton *)sender {
    [self cancelClicked:nil];
    if ([self.delegate respondsToSelector:@selector(continueWithEmail)])
        [self.delegate continueWithEmail];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)loginPressed:(UIButton *)sender {
    [self cancelClicked:nil];
    if ([self.delegate respondsToSelector:@selector(continueWithLogin)])
        [self.delegate continueWithLogin];
}
@end
