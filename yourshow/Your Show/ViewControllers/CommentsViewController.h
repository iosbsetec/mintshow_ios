//
//  CommentsViewController.h
//  Photopinion
//
//  Created by developer on 2/13/13.
//  Copyright (c) 2013 Shiv Mohan Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kMinimumCellHeight  58.0f
#define kUserNameLabelWidth 182.0f
#define kCommentLabelWidth  216.0f

#import "MessageComposerView.h"
#import "SWTableViewCell.h"

@protocol CommentsViewDelegate

@optional

-(void)updateCommentCount:(NSInteger)commentCount alreadyCommented:(BOOL)commented;

@end



@interface CommentsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,MessageComposerViewDelegate,SWTableViewCellDelegate>
{
    IBOutlet UITableView    *tblComment;
    IBOutlet UITextField    *txtAddComment;
    IBOutlet UIButton       *btnSend;
    IBOutlet UIButton       *btnEdit;
    IBOutlet UIView         *viewHeader;
    IBOutlet UIView         *viewAddComment;
    IBOutlet UIImageView    *imageEgoforUser;
}

@property(nonatomic, assign)    BOOL isFromTrendingMints;
@property(nonatomic,retain)     UITableView *tblComment;
@property(nonatomic,retain)     NSMutableArray *arrAllCommentsDetails;
@property (nonatomic ,retain)   NSString *strCommentTextValue;
@property (nonatomic ,retain)   NSString *strPhotoId;
@property (nonatomic ,retain)   NSString *strCommentById;
@property (nonatomic, assign)   int commentCount;
@property (nonatomic, assign)   NSInteger selectdCommentId;

@property (nonatomic, strong)   NSString *rowId;
@property (nonatomic, strong)   NSMutableArray *commentArray;
@property (strong,nonatomic)    BaseFeedModel *achievementdetails;
@property (strong,nonatomic)    NSString *feedId;
@property (strong,nonatomic)    NSString *originalFeedType;
@property (strong, nonatomic)   MessageComposerView *composeView;
@property (nonatomic, strong)   NSString * commonString;
@property (nonatomic, assign)    id <CommentsViewDelegate> delegate;

- (IBAction)btnBackTapped:(id)sender;
- (IBAction)btneEditTapped:(id)sender;
- (void)serverPostComment:(NSString *)accessToken text:(NSString *)commentText;


@end
