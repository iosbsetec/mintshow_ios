//
//  KeywordVC.h
//  ZFTokenFieldDemo
//
//  Created by BseTec on 12/16/15.
//  Copyright © 2015 Amornchai Kanokpullwad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KeyWordDelegate <NSObject>

-(void)updateAddShowRoomKeyword:(NSArray *)arrKeywords;

@end

@interface KeywordVC : UIViewController
{
    
}
@property (nonatomic,retain)id <KeyWordDelegate> keyDelegate;
@property (nonatomic,strong) NSArray *arrayKeywords;
@end
