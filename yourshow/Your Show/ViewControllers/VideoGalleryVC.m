//
//  VideoGalleryViewController.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 05/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "VideoGalleryVC.h"
#import "VideoCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AddMintViewController.h"
#import "SPHSingletonClass.h"
#import <AVFoundation/AVFoundation.h>
#import "VideoModel.h"
#import "CameraFolderVw.h"




@interface VideoGalleryVC () <CameraFolderVwDelegate>
{
    ALAsset *selectdAsset;
    CameraFolderVw *photoDDView;
    IBOutlet UILabel *lblTitle;

}
@property(nonatomic, strong) NSMutableArray *assets;
@property (strong, nonatomic) IBOutlet UICollectionView *VideoCollectionView;
- (IBAction)Close_Action:(id)sender;

@end

@implementation VideoGalleryVC
@synthesize videoData;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    videoData =[[NSMutableArray alloc]init];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    UINib *cellNib = [UINib nibWithNibName:@"VideoCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"VideoCell"];
    [self getVideoFromLibrary];
}




#pragma mark --- *** CAMERA ROLL ---
////////////////////

- (IBAction)cameraDropDown:(UIButton *)sender
{
    if ([photoDDView superview]) {
        [photoDDView closeAnimationZoom];
        return;
    }
    
    photoDDView = [[CameraFolderVw alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, _collectionView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-50) mediaType:NO];
    photoDDView.aDelegate = self;
    [photoDDView showAnimationZoom];
    [self.view addSubview:photoDDView];
}


-(void)selectedFolderGroup : (NSString *)title AssetList : (NSArray *) list {
    lblTitle.text = title;
    if ([videoData count]) {
        [videoData removeAllObjects];
    }
    [videoData addObjectsFromArray:list];
    [self.collectionView reloadData];
    
}

- (IBAction)Close_Action:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate VideoGallerycancelTapped];

}

- (void)galleryVideoTapped
{
    
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return [videoData count]+1;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Setup cell identifier
    static NSString *cellIdentifier = @"VideoCell";
    VideoCell *cell = (VideoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    UITapGestureRecognizer *tapRecognizer;
    
    if (indexPath.row == 0)
        
    {
        cell.previewImageView.contentMode = UIViewContentModeCenter;
        cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cell.layer.borderWidth = 1.0;
        cell.previewImageView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.7];
        cell.previewImageView.image= [UIImage imageNamed:@"Video_Icon.png"];
        tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        [cell.previewImageView addGestureRecognizer:tapRecognizer];
        cell.previewImageView.tag = indexPath.row;
        cell.layer.cornerRadius = 6.0;
    }
    else
    {
        @autoreleasepool {
            [[cell viewWithTag:123]removeFromSuperview];
            VideoModel *amodel = [videoData objectAtIndex:indexPath.row-1];
            cell.previewImageView.image = amodel.thumbImage;
            cell.videoTimeLabel.text = [NSString stringWithFormat:@"%.2f",[amodel.videoDuration floatValue ]];
            cell.previewImageView.userInteractionEnabled = YES;
            tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
            [cell.previewImageView addGestureRecognizer:tapRecognizer];
            cell.layer.borderColor = [UIColor clearColor].CGColor;
            cell.previewImageView.tag = indexPath.row;
            cell.contentView.backgroundColor = [UIColor darkGrayColor];
            cell.layer.cornerRadius = 6.0;
        }
    }
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(6,4,4,6);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}


- (void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view; //cast pointer to the derived class if needed
    NSLog(@"%d", (int) view.tag);
    if (view.tag==0)
    {
        [self Close_Action:nil];
    }
    else
    {
        VideoModel *aModel = [videoData objectAtIndex:[view tag]-1];
        
        SPHSingletonClass* sharedClass      = [SPHSingletonClass sharedSingletonClass];
        sharedClass.remoteVideoURLString    = aModel.videoLocalUrlAsset;
        sharedClass.localVideopath          = (NSURL *)aModel.videoLocalUrlAsset;
        
        
        
        NSURL* aURL = (NSURL *)aModel.videoLocalUrlAsset;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                       {
                           [library assetForURL:aURL resultBlock:^(ALAsset *asset)
                            {
                                ALAssetRepresentation *rep = [asset defaultRepresentation];
                                Byte *buffer                = (Byte*)malloc((NSUInteger)rep.size);
                                NSUInteger buffered         = [rep getBytes:buffer fromOffset:0.0 length:(NSUInteger)rep.size error:nil];
                                NSData* __autoreleasing dataObject= [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
                                aModel.dataGaleryVideo   = dataObject;
                                sharedClass.selectVideoData = aModel.dataGaleryVideo;
                            }
                                   failureBlock:^(NSError *error)
                            {
                                // error handling
                                NSLog(@"failure-----");
                            }];
                           
                           //Background Thread
                           dispatch_async(dispatch_get_main_queue(), ^(void){
                               
                               [[NSNotificationCenter defaultCenter]postNotificationName:@"VideoGallery" object:nil];
                               NSLog(@"%@ sharedClass.selectedVideoURL", sharedClass.remoteVideoURLString);
                               [self dismissViewControllerAnimated:YES completion:nil];
                               
                           });
                       });
    }
    
}

- (void)getVideoFromLibrary
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    @autoreleasepool {
        
        ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
        [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop)  {
            if (group) {
                [group setAssetsFilter:[ALAssetsFilter allVideos]];
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
                               {
                                   [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                                       if (result) {
                                           CGImageRef thum         = [result thumbnail];
                                           VideoModel *aModel      = [VideoModel new];
                                           aModel.result = result;
                                           aModel.videoDuration    = [result valueForProperty:ALAssetPropertyDuration];
                                           aModel.videoLocalUrlAsset = [result valueForProperty:ALAssetPropertyAssetURL];
                                           aModel.thumbImage       = [UIImage imageWithCGImage:thum];
                                           aModel.VideoUrl = [result valueForProperty:ALAssetPropertyAssetURL];
                                           selectdAsset            = result;
                                           [videoData addObject:aModel];
                                       }
                                   }];
                                   dispatch_async(dispatch_get_main_queue(), ^(void){
                                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                                       [self.collectionView reloadData];
                                   });
                               });
            }
        } failureBlock:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end


