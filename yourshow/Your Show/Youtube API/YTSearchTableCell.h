//
//  YTSearchTableCell.h
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YoutubeModelClass.h"

@interface YTSearchTableCell : UITableViewCell
{
 IBOutlet UILabel *YtTitle;
 IBOutlet UILabel *YtDesc;
 IBOutlet UILabel *YtPublished;
 IBOutlet UIImageView *YtImage;
}

-(void)setCellData : (YoutubeModelClass *)modelData;
@end
