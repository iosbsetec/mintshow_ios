//
//  YTSearchTableCell.m
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import "YTSearchTableCell.h"
#import "AsyncImageView.h"

@implementation YTSearchTableCell

- (void)awakeFromNib {
    // Initialization code
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//}

-(void)setCellData : (YoutubeModelClass *)modelData

{
    YtTitle.text = modelData.titleText;
    YtDesc.text = modelData.descriptionText;
    YtImage.imageURL = [NSURL URLWithString:modelData.smallPhoto];
}
@end
