//
//  YTSearchController.m
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import "YoutubeVC.h"
#import "YTSearchTableCell.h"
#import "YoutubeGeneralModel.h"
#import "YoutubeModelClass.h"
#import "YoutubeACApi.h"
#import "AsyncImageView.h"
#import  <MediaPlayer/MediaPlayer.h>

#define LOG_ERROR(x) NSLog(@"[%@ %@] - error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), x)

#define APIKEY @"AIzaSyBDhQB43pAT-Jz3MtjIq8spzKokwiILYQk"

const CGFloat YouTubeStandardPlayerWidth = 640;
const CGFloat YouTubeStandardPlayerHeight = 390;

@interface YoutubeVC ()<UIWebViewDelegate>
{
    NSMutableDictionary *returnObj;
    IBOutlet UIView *viewVideoBG;
    SearchListVC *viewSearch;
    
    NSDictionary *dataResponse;
    NSArray *checkdata;
    
    NSString *strVideoUrl;
    MPMoviePlayerController *movieController;
    UIImageView *searchIcon;
    
    UIWebView *webVideoView;
}

-(IBAction)btnSearchTapped:(id)sender;
@end


@implementation YoutubeVC

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self embedYouTube:CGRectMake(5, 5, viewVideoBG.frame.size.width-10, viewVideoBG.frame.size.height-15)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    firstLoad=YES;
    

    [self GetYoutubeSearchFeed:@"You are a ceo"];

    strVideoUrl = @"jfCT72aO67I";
    
    returnObj = [[NSMutableDictionary alloc]init];
    
    [SearchField.layer setBorderWidth:1.0f];
    [SearchField.layer setBorderColor:[UIColor colorWithRed:187.0f/255.0f green:187.0f/255.0f blue:187.0f/255.0f alpha:1.0f].CGColor];
    spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 8)];
    [SearchField setLeftViewMode:UITextFieldViewModeAlways];
    [SearchField setLeftView:spacerView];
    [activityView setHidden:YES];
    
    SearchField.layer.cornerRadius = 4.0;
    requestCount=0;
    
    CGRect tableFrame = YTtable.frame;
    tableFrame.origin.y = viewVideoBG.frame.size.height+45;
    tableFrame.size.height = self.view.frame.size.height - viewVideoBG.frame.size.height-90;
    [YTtable setFrame:tableFrame];
    
    
}


-(void)GetYoutubeSearchFeed:(NSString *)searchtext
{
    [SearchField resignFirstResponder];

    SearchField.text = (firstLoad)?@"":searchtext;
    searchtext = [searchtext stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=%@&part=snippet&maxResults=50&q=%@&type=video",APIKEY,searchtext]]] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (!error)
        {
            [self parseSearchListWithData:data];
        }
        else
        {
            NSLog(@"error data %@",error);
        }
    }];
}

-(void)parseSearchListWithData:(NSData *)data
{
    NSError * error = nil;
    dataResponse=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    checkdata=[[NSArray alloc]initWithArray:[dataResponse objectForKey:@"items"]];
    NSMutableArray *array = [NSMutableArray array];
    YoutubeGeneralModel *obj = [YoutubeGeneralModel new];
    
    if([checkdata count]>0)
    {
        for (NSDictionary *dict in checkdata)
        {
            YoutubeModelClass *modelObject = [[YoutubeModelClass alloc]initWithInfo:dict];
            [array addObject:modelObject];
            modelObject = nil;
        }
        
        obj.tempArray = array;
        array = nil;
        obj.status = @"true";
        obj.nextPageToken = [dataResponse objectForKey:@"pageInfo"];
        obj.pageInfo = [dataResponse objectForKey:@"nextPageToken"];
    }
    else
    {
        obj.status = @"false";
    }
    
    [self processedData:obj];
}

-(void) processedData:(YoutubeGeneralModel *)data
{
    if([data.status isEqualToString:@"true"])
    {
        firstLoad=NO;

        youtubeData=[[NSArray alloc] initWithArray:data.tempArray];

        YTtable.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
        YoutubeModelClass *model = [youtubeData objectAtIndex:0];
        strVideoUrl = model.youtubeLink;
        [self performSelectorOnMainThread:@selector(onRefreshTable) withObject:nil waitUntilDone:YES];

    }
}

-(void)onRefreshTable
{
        [webVideoView loadHTMLString:[self htmlContent] baseURL:nil];
    [YTtable reloadData];
    
    if (youtubeData.count > 0)
    {
        [YTtable setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
        [YTtable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    
    //        [self performSelector:@selector(onRefreshTable:) withObject:nil afterDelay:0.2];
    //   [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(onRefreshTable:) userInfo:nil repeats:NO];
    [self performSelectorOnMainThread:@selector(onAutoRefreshTable) withObject:nil waitUntilDone:YES];
}

-(void)onAutoRefreshTable
{
    [YTtable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void)embedYouTube:(CGRect)frame
{
    webVideoView = [[UIWebView alloc] initWithFrame:frame];
    webVideoView.backgroundColor = self.view.backgroundColor;
    webVideoView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    webVideoView.delegate = self;
    webVideoView.scrollView.scrollEnabled = NO;
    webVideoView.opaque = NO;
    webVideoView.scrollView.bounces = NO;
//    webVideoView.layer.borderColor = [UIColor redColor].CGColor;
//    webVideoView.layer.borderWidth = 2.0;
    [viewVideoBG addSubview:webVideoView];
}

-(IBAction)searchAction:(UITextField *)txtFld{
    NSLog(@"text:%@",txtFld.text);
    if([[txtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]>0){
        [activityView setHidden:NO];
        requestCount++;
        YoutubeACApi *youtubeApiObj=[[YoutubeACApi alloc]init];
        [youtubeApiObj GetYoutubeACSearch:[txtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] withResponseCallback:^(NSDictionary *dictc)
        {
            NSMutableDictionary *mutable = [dictc mutableCopy];
            returnObj = [mutable mutableCopy];
        }];

        if([viewSearch.view isHidden])
            [viewSearch.view setHidden:NO];

        if (viewSearch==nil)
        {
            viewSearch = [[SearchListVC alloc]initWithFrame:CGRectMake(0, 39, self.view.frame.size.width, self.view.frame.size.height-39)];
            viewSearch.searchDelegate = self;
            [self.view addSubview:viewSearch.view];
        }
        
        [viewSearch getKeyWords:returnObj];
    }
}


- (IBAction)cancelTapped:(UIButton *)sender
{
    
}



-(IBAction)cancelAction:(id)sender
{
    [SearchField resignFirstResponder];
    [webVideoView stringByEvaluatingJavaScriptFromString:@"document.open();document.close();"];

    webVideoView.delegate = nil;
    webVideoView = nil;
    [self.delegate didTappedCancelButton];
    [self.view removeFromSuperview];
//    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)chooseAction:(id)sender
{
    strVideoUrl = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",strVideoUrl];
    NSLog(@"%@",strVideoUrl);
    [webVideoView stringByEvaluatingJavaScriptFromString:@"document.open();document.close();"];
    
    [self.view removeFromSuperview];
    [self.delegate YoutubeVCDelegateMethod:[strVideoUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    webVideoView.delegate = nil;
    webVideoView = nil;
}

-(IBAction)btnSearchTapped:(id)sender
{
    if(SearchField.text.length>0)
    {
        [viewSearch.view setHidden:YES];
        [self GetYoutubeSearchFeed:SearchField.text];
    }
}

#pragma mark - ****** UITextField Delegate *******
- (void)textFieldDidBeginEditing:(UITextField *)textField {
        [textField.layer setBorderColor:[UIColor colorWithRed:187.0f/255.0f green:187.0f/255.0f blue:187.0f/255.0f alpha:1.0f].CGColor];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}


#pragma mark - ****** UITableView delegate *******
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [youtubeData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    {
        YoutubeModelClass *model = [youtubeData objectAtIndex:indexPath.row];
        static NSString *MyIdentifier1 = @"MyCell";
        YTSearchTableCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier1];
        if (cell == nil) {
            [tableView registerNib:[UINib nibWithNibName:@"YTSearchTableCell" bundle:nil] forCellReuseIdentifier:MyIdentifier1];
            cell=[tableView dequeueReusableCellWithIdentifier:MyIdentifier1];

        }
        cell.selectionStyle= UITableViewCellSeparatorStyleNone;
        [cell setCellData:model];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 91;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [webVideoView stringByEvaluatingJavaScriptFromString:@"document.open();document.close();"];
    strVideoUrl = [[youtubeData objectAtIndex:indexPath.row] youtubeLink];
    [self embedYouTube:CGRectMake(5, 5, viewVideoBG.frame.size.width-10, viewVideoBG.frame.size.height-15)];
    [webVideoView loadHTMLString:[self htmlContent] baseURL:nil];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [webVideoView loadHTMLString:@"" baseURL:nil];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self updatePlayerSize];
}

#pragma mark - Actions

- (void)play
{
    [webVideoView stringByEvaluatingJavaScriptFromString:@"player.playVideo();"];
}

- (void)pause
{
    [webVideoView stringByEvaluatingJavaScriptFromString:@"player.pauseVideo();"];
}

- (void)stop
{
    [webVideoView stringByEvaluatingJavaScriptFromString:@"player.stopVideo();"];
}

#pragma mark - Accessors

- (void)setPlayerSize:(CGSize)playerSize
{
    [webVideoView stringByEvaluatingJavaScriptFromString:
     [NSString stringWithFormat:@"player.setSize(%u, %u);",
      (unsigned int) playerSize.width, (unsigned int) playerSize.height]];
}

- (NSTimeInterval)duration
{
    return [[webVideoView stringByEvaluatingJavaScriptFromString:@"player.getDuration();"] doubleValue];
}

- (NSTimeInterval)currentTime
{
    return [[webVideoView stringByEvaluatingJavaScriptFromString:@"player.getCurrentTime();"] doubleValue];
}

#pragma mark - Helpers

- (NSString *)htmlContent
{
    NSString *pathToHTML = [[NSBundle mainBundle] pathForResource:@"PBYouTubeVideoView" ofType:@"html"];
    NSAssert(pathToHTML != nil, @"could not find PBYouTubeVideoView.html");
    
    NSError *error = nil;
    NSString *template = [NSString stringWithContentsOfFile:pathToHTML encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        LOG_ERROR(error);
    }
    CGSize playerSize = [self playerSize];
    NSString *result = [NSString stringWithFormat:template, [NSString stringWithFormat:@"%.0f", playerSize.width], [NSString stringWithFormat:@"%.0f", playerSize.height],                         strVideoUrl];
    return result;
}

- (void)updatePlayerSize
{
    CGSize playerSize = [self playerSize];
    [self setPlayerSize:playerSize];
}

- (CGSize)playerSize
{
    float heightRatio = self.view.bounds.size.height / YouTubeStandardPlayerHeight;
    float widthRatio = self.view.bounds.size.width / YouTubeStandardPlayerWidth;
    float ratio = MIN(widthRatio, heightRatio);
    
    CGSize playerSize = CGSizeMake(YouTubeStandardPlayerWidth * ratio, YouTubeStandardPlayerHeight * ratio);
    return playerSize;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[request URL].scheme isEqualToString:@"ytplayer"]) {
        NSArray *components = [[request URL] pathComponents];
        if ([components count] > 1) {
            NSString *actionData = nil;
            if ([components count] > 2) {
                actionData = components[2];
            }
        }
        return NO;
    } else {
        return YES;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    LOG_ERROR(error);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
