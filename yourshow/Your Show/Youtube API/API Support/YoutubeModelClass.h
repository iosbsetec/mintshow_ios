//
//  YoutubeModelClass.h
//  Youtube
//
//  Created by JITENDRA KUMAR PRADHAN on 12/12/15.
//  Copyright © 2015 BSEtec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YoutubeModelClass : NSObject

@property (nonatomic,strong) NSString *titleText;
@property (nonatomic,strong) NSString *descriptionText;
@property (nonatomic,strong) NSString *smallPhoto;
@property (nonatomic,strong) NSString *largePhoto;
@property (nonatomic,strong) NSString *youtubeLink;

-(id)initWithInfo : (NSDictionary *) data;

@end
