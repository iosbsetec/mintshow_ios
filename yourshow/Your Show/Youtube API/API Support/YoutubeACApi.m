//
//  YoutubeACApi.m
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import "YoutubeACApi.h"
#import "XMLDictionary.h"
@implementation YoutubeACApi

-(void) GetYoutubeACSearch:(NSString *)searchtext withResponseCallback:(void (^)(NSDictionary *dictc))callback {
    
        NSString *urlAsString = [NSString stringWithFormat:@"https://suggestqueries.google.com/complete/search?q=%@&client=toolbar",searchtext];
        NSURL *url = [[NSURL alloc] initWithString:urlAsString];
//        NSLog(@"%@", urlAsString);
    
        [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            if (error) {
                NSLog(@"error data %@",error);
            } else {
                NSDictionary * dictc = nil;
                dictc = [NSDictionary dictionaryWithXMLData:data];
                callback(dictc);
            }
        }];
}

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    searchlistbypage = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [searchlistbypage appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:searchlistbypage];
    [parser setDelegate:self];
    [parser parse];
    
 
//    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLParser:parser];
//    NSLog(@"key responce%@",xmlDoc);
    
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    
    
}

// xml delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict;
{
    
    if ([elementName isEqualToString:@"suggestion"]) {
        NSLog(@"%@",attributeDict);
    }
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string;
{
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName;
{
}


@end