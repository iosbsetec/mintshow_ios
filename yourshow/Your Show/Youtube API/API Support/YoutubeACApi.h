//
//  YoutubeACApi.h
//  Youtube
//
//  Created by Sankar on 11/12/15.
//  Copyright (c) 2015 BSEtec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YoutubeACApi : NSObject <NSURLConnectionDelegate, NSXMLParserDelegate> {
    NSMutableData *searchlistbypage;
}

-(void) GetYoutubeACSearch:(NSString *)searchtext withResponseCallback:(void (^)(NSDictionary *dictc))callback;

@end