//
//  CellUserList.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 04/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellUserList : UITableViewCell
@property (weak, nonatomic)  IBOutlet UIButton *btnFollow;
@property (weak, nonatomic)  IBOutlet UIImageView *imgVUserPic;
@property (weak, nonatomic)  IBOutlet UILabel *lblName;
@property (weak, nonatomic)  IBOutlet UIButton *btnUserImage;

-(void)setInfo : (NSString *)imageData userName : (NSString *) name;
@end
