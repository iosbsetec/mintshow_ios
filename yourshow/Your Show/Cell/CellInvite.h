//
//  CellInvite.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 15/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellInvite : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnFollow;
@property (weak, nonatomic) IBOutlet UIImageView *imgVCheck,*imgVUserPic;
;
@property (weak, nonatomic)  IBOutlet UILabel *lblName;
;

-(void)setInfo : (NSString *)imageData userName : (NSString *) name;
@end
