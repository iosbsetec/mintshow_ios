//
//  ProfileStatsCell.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 01/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileStatsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgViewStats;
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UILabel *lblAverage;
@property (strong, nonatomic) IBOutlet UILabel *lblTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblAverageCount;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalCount;
@end
