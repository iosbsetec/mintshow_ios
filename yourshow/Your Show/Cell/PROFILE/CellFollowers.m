//
//  CellFollowers.m
//  Your Show
//
//  Created by bsetec on 8/3/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "CellFollowers.h"

@implementation CellFollowers{
    
    __weak IBOutlet UIView *containerview;
}

- (void)awakeFromNib {
    _profileFllwimgV.layer.cornerRadius = 15;
    [_profileFllwimgV setClipsToBounds:YES];

    containerview.layer.cornerRadius = 5.;
    [containerview setClipsToBounds:YES];
    
    _btnFollow.layer.borderWidth = 1.0f;
    _btnFollow.layer.borderColor = ORANGECOLOR.CGColor;
    _btnFollow.hidden = NO;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
