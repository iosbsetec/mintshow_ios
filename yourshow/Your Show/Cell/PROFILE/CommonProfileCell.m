//
//  CommonProfileCell.m
//  YACC
//
//  Created by Pothiraj_BseTec on 01/08/15.
//  Copyright (c) 2015 BseTech. All rights reserved.
//

#import "CommonProfileCell.h"

@implementation CommonProfileCell

- (void)awakeFromNib {
    // Initialization code
    
    self.followButton.layer.cornerRadius = 3.0f;
    self.followButton.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 3.0f;
    self.backView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
