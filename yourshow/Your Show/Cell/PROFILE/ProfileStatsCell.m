//
//  ProfileStatsCell.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 01/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "ProfileStatsCell.h"

@implementation ProfileStatsCell

@synthesize backView,imgViewStats,lblAverage,lblAverageCount,lblTotal,lblTotalCount;

- (void)awakeFromNib {
    // Initialization code
    self.backView.layer.cornerRadius = 4.0f;
    self.backView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
