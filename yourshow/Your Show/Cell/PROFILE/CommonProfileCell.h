//
//  CommonProfileCell.h
//  YACC
//
//  Created by Pothiraj_BseTec on 01/08/15.
//  Copyright (c) 2015 BseTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonProfileCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *userThumbImage;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UIButton *followButton;
@property (strong, nonatomic) IBOutlet UIView *backView;

@end
