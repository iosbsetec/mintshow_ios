//
//  DiYuCell2.h
//  IYLM
//
//  Created by JianYe on 13-1-11.
//  Copyright (c) 2013年 Jian-Ye. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cell2 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVBg;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckMarck;
@property (nonatomic,retain)IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *checkImage;
@end
