//
//  CellMyCircle3.m
//  Your Show
//
//  Created by bsetec on 8/1/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "CellMyCircle.h"
#import "UIImage+animatedGIF.h"

@implementation CellMyCircle
{
    IBOutlet UILabel *lblTextMint;

}

- (void)awakeFromNib {
    _lblActivityText.isActivityScreen = YES;
    lblTextMint.layer.cornerRadius = 4.0;
    [lblTextMint setClipsToBounds:YES];
    _imgVuserImage.layer.cornerRadius = _imgVuserImage.frame.size.width/2;
    _imgVMintThumbImage.layer.cornerRadius = 4;
    [_imgVuserImage setClipsToBounds:YES];
    [_imgVMintThumbImage setClipsToBounds:YES];
    _viewContainer.layer.cornerRadius = 5.;
    [_viewContainer setClipsToBounds:YES];
    [self showloader];
    // Initialization code
}

-(void)showloader
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"page_loader" withExtension:@"gif"];
    imgVLoader.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
}

-(void)setCellData:(ActivityModel *)actModel index:(int)nIndex
{
    _imgVMediaPlay.hidden = ([actModel.activity_isVideo isEqualToString:@"1"])?NO:YES;
    
    _imgVMintThumbImage.mintImageURL = ([actModel.activity_type isEqualToString:@"showroom"])?[NSURL URLWithString:actModel.activity_showroom_thumb_image]:[NSURL URLWithString:actModel.activity_thumb_image];
    _imgVuserImage.mintImageURL = [NSURL URLWithString:actModel.activity_image];
    [lblTextMint setHidden:([actModel.activity_thumb_image length]|| [actModel.activity_showroom_thumb_image length])?YES:NO];
    
    NSString *strActText = [self getActivityString:actModel.activity_text date:actModel.activity_time width:_lblActivityText.frame.size.width];
    
    _lblActivityText.strFirstName = actModel.first_user_name;
    _lblActivityText.strSecondName = [NSString stringWithFormat:@"%@",actModel.second_user_name];
    _lblActivityText.strUserName = actModel.activity_user_name;
    _lblActivityText.strDateTime = actModel.activity_time;
    _lblActivityText.text = strActText;
    
    CGFloat strHeight = [self getStringHeight:strActText width:_lblActivityText.frame.size.width];
    CGRect rectLbl = _lblActivityText.frame;
    rectLbl.origin.y = (strHeight>28)?2:10;
    rectLbl.size.height = strHeight+5;
    [_lblActivityText setFrame:rectLbl];
    
    if ((strHeight>40))
    {
        CGRect rectContainerFrame = _viewContainer.frame;
        rectContainerFrame.size.height = rectContainerFrame.size.height+14;
        [_viewContainer setFrame:rectContainerFrame];
    }
    
    CGRect rectSelfFrame = self.frame;
    UIView *viewPhoto;
    int yPosForMoreMintImage = _lblActivityText.frame.size.height+_lblActivityText.frame.origin.y+4;
    if (actModel.arrFeedDetails.count>1)
    {
        NSString *strProfileImage = [[actModel.arrFeedDetails objectAtIndex:0]valueForKey:@"feed_thumb_image"];
        if ([strProfileImage length]>0)
        {
            viewPhoto = [self onCreateMintThumbImage:actModel.arrFeedDetails yPos:yPosForMoreMintImage type:@"mint"];
            [_btnMintThumb setHidden:YES];

        }
        else
        {
            viewPhoto = [self onCreateGroupTextMint:actModel.arrFeedDetails yPos:yPosForMoreMintImage type:@"mint"];
//            [lblTextMint setHidden:NO];
        }
        [lblTextMint setHidden:YES];
    }
    
    if (actModel.arrShowroomDetails.count>1 && [actModel.activity_type isEqualToString:@"showroom"])
    {
        NSString *strProfileImage = [[actModel.arrShowroomDetails objectAtIndex:0]valueForKey:@"showroom_thumb_image"];
        if ([strProfileImage length]>0)
        {
            viewPhoto = [self onCreateMintThumbImage:actModel.arrShowroomDetails yPos:yPosForMoreMintImage type:nil];
            [lblTextMint setHidden:YES];
            [_btnMintThumb setHidden:YES];
        }
        else
            [lblTextMint setHidden:NO];
    }
    else if(actModel.arrShowroomDetails.count==1 && [actModel.activity_type isEqualToString:@"showroom"])
    {
        @try {
            _imgVMintThumbImage.mintImageURL = [NSURL URLWithString:[[actModel.arrShowroomDetails objectAtIndex:0] valueForKey:@"showroom_thumb_image"]];
            _imgVMediaPlay.hidden = ([[[actModel.arrShowroomDetails objectAtIndex:0] valueForKey:@"is_video"] isEqualToString:@"1"])?NO:YES;
            [_btnMintThumb setHidden:([[[actModel.arrShowroomDetails objectAtIndex:0] valueForKey:@"showroom_thumb_image"] length])?NO:YES];
            [lblTextMint setHidden:YES];
        }
        @catch (NSException *exception) {
            NSLog(@"exception in activity %@",[exception description]);
        }
        @finally {
            
        }
        
    }
    
    if (actModel.arrMintListDetails.count>1)
    {
        NSString *strProfileImage = [[actModel.arrMintListDetails objectAtIndex:0]valueForKey:@"feed_thumb_image"];
        if ([strProfileImage length]>0)
        {
            viewPhoto = [self onCreateMintThumbImage:actModel.arrMintListDetails yPos:yPosForMoreMintImage type:@"mint"];
            [_btnMintThumb setHidden:YES];
            [lblTextMint setHidden:YES];
        }
        else
        {
            viewPhoto = [self onCreateGroupTextMint:actModel.arrMintListDetails yPos:yPosForMoreMintImage type:@"mint"];
            [lblTextMint setHidden:NO];
        }
        [lblTextMint setHidden:YES];

    }
    else if(actModel.arrMintListDetails.count==1)
    {
        @try {
            _imgVMintThumbImage.mintImageURL = [NSURL URLWithString:[[actModel.arrMintListDetails objectAtIndex:0] valueForKey:@"feed_thumb_image"]];
            _imgVMediaPlay.hidden = ([[[actModel.arrMintListDetails objectAtIndex:0] valueForKey:@"is_video"] isEqualToString:@"1"])?NO:YES;
            [_btnMintThumb setHidden:NO];
            [lblTextMint setHidden:([[[actModel.arrMintListDetails objectAtIndex:0] valueForKey:@"feed_thumb_image"] length])?YES:NO];
        }
        @catch (NSException *exception) {
            NSLog(@"exception in activity %@",[exception description]);
        }
        @finally {
            
        }
    }
    
    if ([actModel.activity_type isEqualToString:@"follow"]) {
        @try {
            NSLog(@"%@",[[actModel.arrFollowerDetails objectAtIndex:0]valueForKey:@"user_image"]);
            NSString *strProfileImage = [[actModel.arrFollowerDetails objectAtIndex:0]valueForKey:@"user_image"];
            if ([strProfileImage length]>0)
            {
                viewPhoto = [self onCreateFollowerThumbImage:actModel.arrFollowerDetails yPos:yPosForMoreMintImage];
                [_btnMintThumb setHidden:YES];
            }
            [lblTextMint setHidden:YES];
        }
        @catch (NSException *exception) {
            NSLog(@"exception in activity %@",[exception description]);
        }
        @finally {
            
        }
    }
    
    CGRect rectContainerFrame = _viewContainer.frame;
    
    if (viewPhoto!=nil)
    {
        rectSelfFrame.size.height = _lblActivityText.frame.origin.y+strHeight+30;
        rectContainerFrame.size.height = _lblActivityText.frame.origin.y+strHeight+45;
        [self addSubview:viewPhoto];
    }
    else
    {
        rectContainerFrame.size.height = (strHeight<45)?50:strHeight+_lblActivityText.frame.origin.y+15;
        rectLbl.size.height = (strHeight>50)?strHeight+15:strHeight+5;
    }
    [_lblActivityText setFrame:rectLbl];
    [self setFrame:rectSelfFrame];
    [_viewContainer setFrame:rectContainerFrame];
    
    _viewContainer.tag = nIndex;
}

-(UIView *)onCreateFollowerThumbImage:(NSArray *)arrDetails yPos:(float)yPosition
{
    int nWidthHeight = 34;
    UIView *viewPhotoHolder = [[UIView alloc]initWithFrame:CGRectMake( _lblActivityText.frame.origin.x, yPosition, _lblActivityText.frame.size.width, 30)];
    
    for (int i=0;i<[arrDetails count];i++)
    {
        UIImageView *imgViewProfile = [[UIImageView alloc]init];
        imgViewProfile.frame = CGRectMake(i+(i%8)*36, 34*(i/8), nWidthHeight, nWidthHeight);
        NSString *strImage = [[arrDetails  objectAtIndex:i]valueForKey:@"user_image"];
        
        imgViewProfile.backgroundColor = (strImage.length)?[UIColor lightGrayColor]:[UIColor clearColor];
        
        imgViewProfile.mintImageURL   = [NSURL URLWithString:strImage];
        imgViewProfile.layer.cornerRadius = imgViewProfile.frame.size.width/2;
        [imgViewProfile setClipsToBounds:YES];
        
        UIButton *btnProfile          = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnProfile setFrame:imgViewProfile.frame];
        btnProfile.tag = [[[arrDetails  objectAtIndex:i]valueForKey:@"user_id"] integerValue];
        [btnProfile addTarget:self action:@selector(followerSelected:) forControlEvents:UIControlEventTouchUpInside];
        [viewPhotoHolder addSubview:imgViewProfile];
        [viewPhotoHolder addSubview:btnProfile];
        
        [self addSubview:viewPhotoHolder];
        
        if (i==5) {
            break;
        }
    }
    
    return viewPhotoHolder;
}

-(void)followerSelected:(UIButton *)sender
{
    NSLog(@"%ld",(long)sender.tag);
    [self.cellDelegate gotoProfilePageView:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
}



-(UIView *)onCreateGroupTextMint:(NSArray *)arrDetails yPos:(float)yPosition type:(NSString *)strType
{
    UIView *viewPhotoHolder = [[UIView alloc]initWithFrame:CGRectMake( _lblActivityText.frame.origin.x, yPosition+7, _lblActivityText.frame.size.width, 30)];
    
    for (int i=0;i<[arrDetails count];i++)
    {
        UILabel *lblText = [[UILabel alloc]init];
        lblText.frame = CGRectMake(i+(i%8)*44, 0, 42, 27);
        lblText.text = @"T";
        lblText.textAlignment = NSTextAlignmentCenter;
        lblText.backgroundColor = [UIColor colorWithRed:170/255.0f green:170/255.0f blue:170/255.0f alpha:1.0];
        lblText.font = [UIFont systemFontOfSize:17];
        lblText.layer.cornerRadius = 4.0;
        [lblText setClipsToBounds:YES];

        UIButton *btnProfile          = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnProfile setFrame:lblText.frame];
        btnProfile.tag = i;
        [btnProfile addTarget:self action:@selector(profileSelected:) forControlEvents:UIControlEventTouchUpInside];
        [viewPhotoHolder addSubview:lblText];
        [viewPhotoHolder addSubview:btnProfile];
        [self addSubview:viewPhotoHolder];
        
        if (i==5) {
            break;
        }
    }
    
    return viewPhotoHolder;
}


-(UIView *)onCreateMintThumbImage:(NSArray *)arrDetails yPos:(float)yPosition type:(NSString *)strType
{
    int nWidthHeight = 34;
    UIView *viewPhotoHolder = [[UIView alloc]initWithFrame:CGRectMake( _lblActivityText.frame.origin.x, yPosition, _lblActivityText.frame.size.width, 30)];
    
    for (int i=0;i<[arrDetails count];i++)
    {
        UIImageView *imgViewProfile = [[UIImageView alloc]init];
        imgViewProfile.frame = CGRectMake(i+(i%8)*36, 34*(i/8), nWidthHeight, nWidthHeight);
        NSString *strImage = ([strType isEqualToString:@"mint"])?[[arrDetails  objectAtIndex:i]valueForKey:@"feed_thumb_image"]:[[arrDetails objectAtIndex:i]valueForKey:@"showroom_thumb_image"];
        
        imgViewProfile.backgroundColor = (strImage.length)?[UIColor lightGrayColor]:[UIColor clearColor];
        
        imgViewProfile.mintImageURL   = [NSURL URLWithString:strImage];
        imgViewProfile.layer.cornerRadius = 4.0;
        [imgViewProfile setClipsToBounds:YES];
        
        UIButton *btnProfile          = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnProfile setFrame:imgViewProfile.frame];
        btnProfile.tag = i;
        [btnProfile addTarget:self action:@selector(profileSelected:) forControlEvents:UIControlEventTouchUpInside];
        [viewPhotoHolder addSubview:imgViewProfile];
        [viewPhotoHolder addSubview:btnProfile];
        if ([strType isEqualToString:@"mint"])
        {
            if([[[arrDetails  objectAtIndex:i] valueForKey:@"is_video"]isEqualToString:@"1"])
            {
                UIImageView *imgPlayIcon = [[UIImageView alloc]initWithFrame:CGRectMake(imgViewProfile.frame.origin.x, imgViewProfile.frame.origin.y, 12, 12)];
                imgPlayIcon.center = imgViewProfile.center;
                imgPlayIcon.image=[UIImage imageNamed:@"media_laundge_play_btn_ios.png"];
                [viewPhotoHolder addSubview:imgPlayIcon];
            }
        }
        [self addSubview:viewPhotoHolder];
        
        if (i==5) {
            break;
        }
    }
    
    return viewPhotoHolder;
}


-(CGFloat)getStringHeight:(NSString *)strText  width:(CGFloat)width
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGFloat textHeight  = [strText boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    
    return textHeight;
}

-(NSString *)getActivityString:(NSString *)strText date:(NSString *)strDate width:(CGFloat)width
{
    NSString *strActText;
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    
    CGSize stringTextSize = [strText sizeWithAttributes:@{NSFontAttributeName:font}];
    CGSize stringDateSize = [strDate sizeWithAttributes:@{NSFontAttributeName:font1}];
    
    CGFloat totalWidth = stringTextSize.width + stringDateSize.width;
    if (stringTextSize.width<width)
    {
        strActText = [NSString stringWithFormat:@"%@\n%@",strText,strDate];
    }
    else
    {
        if(totalWidth>((width*3))) // More than two line
            strActText = [NSString stringWithFormat:@"%@ %@",strText,strDate];
        if(totalWidth>((width*2)-20)) // More than one line
            strActText = [NSString stringWithFormat:@"%@\n%@",strText,strDate];
        else
            strActText = [NSString stringWithFormat:@"%@ %@",strText,strDate];
    }
    
    
    return strActText;
}



- (NSMutableAttributedString *)buildAgreeTextViewFromString:(NSString *)localizedString AndUserName:(NSString*)userName firstName:(NSString*)firstUserName secondName:(NSString*)secondUserName date:(NSString *)dateAndTime andType:(NSString *)strType showTag:(NSString *)showRoomTag
{
    
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    NSMutableAttributedString * finalString      = [[NSMutableAttributedString alloc] initWithString:localizedString];
    
    NSDictionary *attributesUserName = @{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName:font};
    NSDictionary *attributesShowroomName = @{NSForegroundColorAttributeName:ORANGECOLOR, NSFontAttributeName:font};
    NSDictionary *attributesDateTime = @{NSForegroundColorAttributeName:[UIColor lightGrayColor], NSFontAttributeName:font1};
    
    
    if ([firstUserName length])
    {
        NSRange rangeYours = [localizedString rangeOfString:firstUserName];
        if (rangeYours.location != NSNotFound) {
            [finalString addAttributes:attributesUserName range:NSMakeRange(rangeYours.location,[firstUserName length])];
        }
        
        if([secondUserName length])
        {
            NSRange rangeYours = [localizedString rangeOfString:secondUserName];
            if (rangeYours.location != NSNotFound) {
                [finalString addAttributes:attributesUserName range:NSMakeRange(rangeYours.location,[secondUserName length])];
            }
        }
    }
    else if ([userName length]) {
        [finalString addAttributes:attributesUserName range:[localizedString rangeOfString:userName]];
    }
    
    if ([dateAndTime length])
    {
        [finalString addAttributes:attributesDateTime range:NSMakeRange([localizedString length]-[dateAndTime length],[dateAndTime length])];
    }
    
    NSRange rangeYours = [localizedString rangeOfString:@"Your's"];
    if (rangeYours.location != NSNotFound) {
        [finalString addAttributes:attributesUserName range:NSMakeRange(rangeYours.location,[@"Your's" length])];
    }
    
    NSRange rangeYour = [localizedString rangeOfString:@"Your"];
    if (rangeYour.location != NSNotFound) {
        [finalString addAttributes:attributesUserName range:NSMakeRange(rangeYour.location,[@"Your" length])];
    }
    
    NSRange rangeYou = [localizedString rangeOfString:@"You"];
    if (rangeYou.location != NSNotFound) {
        [finalString addAttributes:attributesUserName range:NSMakeRange(rangeYou.location,[@"You" length])];
    }
    
    if([showRoomTag length]>0)
    {
        NSRange rangeYou = [localizedString rangeOfString:showRoomTag];
        if (rangeYou.location != NSNotFound) {
            NSLog(@"position %lu", (unsigned long)rangeYou.location);
            [finalString addAttributes:attributesShowroomName range:NSMakeRange(rangeYou.location-1,[showRoomTag length]+1)];
        }
    }
    //    [finalString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange([userName length],[localizedString length]-[userName length])];
    
    return finalString;
}
-(void)profileSelected:(UIButton *)sender
{
    NSLog(@"selected tag %ld",(long)[_viewContainer tag]);
    ActivityModel *actModel = [_arrModelActivity objectAtIndex:[_viewContainer tag]];
    if (actModel.arrFeedDetails.count>1)
    {
        [self.cellDelegate gotoMintDetailsView:[[actModel.arrFeedDetails objectAtIndex:sender.tag] valueForKey:@"feed_id"]];
    }
    
    if (actModel.arrShowroomDetails.count>1)
    {
        NSLog(@"%@",[[actModel.arrShowroomDetails objectAtIndex:sender.tag] valueForKey:@"showroom_tag"]);
        [self.cellDelegate gotoShowroomViewWithTag:(int)[sender tag] showTag:[NSString stringWithFormat:@"*%@",[[actModel.arrShowroomDetails objectAtIndex:sender.tag] valueForKey:@"showroom_tag"]]];
    }
    
    if (actModel.arrMintListDetails.count>1)
    {
        NSLog(@"%ld",(long)sender.tag);
        
        NSLog(@"%@",[[actModel.arrMintListDetails objectAtIndex:sender.tag] valueForKey:@"feed_id"]);
        
        [self.cellDelegate gotoMintDetailsView:[[actModel.arrMintListDetails objectAtIndex:sender.tag] valueForKey:@"feed_id"]];
    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
