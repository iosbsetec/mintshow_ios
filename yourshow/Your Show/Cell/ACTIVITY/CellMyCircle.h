//
//  CellMyCircle3.h
//  Your Show
//
//  Created by bsetec on 8/1/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityModel.h"
#import "STTweetLabel.h"

@protocol MyCircleCellDelegate <NSObject>
-(void)gotoShowroomViewWithTag:(int)tag showTag:(NSString *)strShowroomName;
-(void)gotoMintDetailsView: (NSString *)feedid;
-(void)gotoProfilePageView:(NSString *)selectedEnitity;

@end

@interface CellMyCircle : UITableViewCell
{
    IBOutlet UIImageView *imgVLoader;
}
@property (nonatomic ,strong)  IBOutlet UIView *viewNoMoreMints;
@property (nonatomic ,strong)  IBOutlet UIView *viewContainer;
@property (nonatomic ,strong)  IBOutlet STTweetLabel *lblActivityText;
@property (nonatomic ,strong)  IBOutlet UIImageView *imgVMintThumbImage;
@property (strong, nonatomic)  IBOutlet UIImageView *imgVuserImage;
@property (strong, nonatomic)  IBOutlet UIImageView *imgVMediaPlay;
@property (strong, nonatomic)  IBOutlet UIButton *btnMintThumb;
@property (strong, nonatomic)  IBOutlet UIButton *btnUserThumb;
@property (strong, nonatomic)  NSArray *arrModelActivity;

@property (nonatomic,assign) id <MyCircleCellDelegate> cellDelegate;
-(void)setCellData:(ActivityModel *)actModel index:(int)nIndex;


@end
