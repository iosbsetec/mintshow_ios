//
//  CellInvite.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 15/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "CellInvite.h"

@implementation CellInvite
{

}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setInfo : (NSString *)imageData userName : (NSString *) name
{
    _btnFollow.hidden = NO;
    _imgVUserPic.hidden = NO;
    _lblName.text = name;
    _imgVUserPic.mintImageURL = [NSURL URLWithString:imageData];
    _imgVUserPic.layer.cornerRadius = 18;
    [_imgVUserPic setClipsToBounds:YES];
    _btnFollow.layer.cornerRadius = 2.0;
    
    
}
@end
