//
//  CollectionViewTableViewCell.m
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "CollectionViewTableViewCell.h"
#import "YSAPICalls.h"
#import "Keys.h"
#import "SPHSingletonClass.h"
@implementation CollectionViewTableViewCell
{

    NSMutableArray *allShowRoomdata;


}

- (void)awakeFromNib {
    // Initialization code
    allShowRoomdata = [[NSMutableArray alloc]init];
    if (![[[SPHSingletonClass sharedSingletonClass] showroomData] count]) {
        [self getAllShowroomForMyShowTab];

    }

}

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}
-(void)getAllShowroomForMyShowTab
{
     NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&showroom_list_type=%@&page_number=%@",GETVALUE(CEO_AccessToken),@"0",@"1"] dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@users/get_showroom_list",ServerUrl]]];
        
        [submitrequest setHTTPMethod:@"POST"];
        [submitrequest setHTTPBody:submitData];
        [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             
             Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:METHOD_MYSHOW_SHOWROOM];
             if ([gmodel.status_code integerValue])
             {
                 allShowRoomdata = gmodel.tempArray;
                 
                 [[SPHSingletonClass sharedSingletonClass] setShowroomData:allShowRoomdata];
                 [_discovershowroomcollectionview reloadData];
             }
             
         }];
    }



-(void)layoutSubviews
{
    [super layoutSubviews];
    self.discovershowroomcollectionview.backgroundColor = [UIColor whiteColor];
    self.discovershowroomcollectionview.showsHorizontalScrollIndicator = NO;
    self.discovershowroomcollectionview.CollectionDataSource = self;
    self.discovershowroomcollectionview.Addshowroom = NO;
    self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
   // self.discovershowroomcollectionview.frame = self.contentView.bounds;
}

#pragma THIS CODE FOR SHOWROOM COLLECTION VIEW
- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row
{
    return  (ShowRoomModel *)[[[SPHSingletonClass sharedSingletonClass] showroomData] objectAtIndex:row];
    // return  nil;
}

- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView
{
    return [[SPHSingletonClass sharedSingletonClass] showroomData].count;
    //return 6;
}

- (void)setCollectionViewDataSourceDelegateforindexPath:(NSIndexPath *)indexPath
{
    self.discovershowroomcollectionview.CollectionDataSource = self;
    self.discovershowroomcollectionview.Addshowroom = NO;
    [self.discovershowroomcollectionview reloadData];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
