//
//  PintCollecionCell.h
//  YACC
//
//  Created by JITENDRA KUMAR PRADHAN on 30/06/15.
//  Copyright (c) 2015 BseTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PintCollecionCell : UICollectionViewCell

@property (nonatomic,strong) UIButton *buttonHighFive;
@property (nonatomic,strong) UILabel *labeHighFiveCount;

@end
