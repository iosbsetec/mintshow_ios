//
//  cellSearchUserCollection.h
//  Your Show
//
//  Created by Bsetec on 31/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cellSearchUserCollection : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnselectUser;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblName;


-(void) setCellData  :(NSArray *) array;
@end
