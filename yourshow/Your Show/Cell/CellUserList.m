//
//  CellUserList.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 04/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "CellUserList.h"

@implementation CellUserList

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setInfo : (NSString *)imageData userName : (NSString *) name
{
    _imgVUserPic.hidden = NO;
    _lblName.text = name;
    _imgVUserPic.mintImageURL = [NSURL URLWithString:imageData];
    _imgVUserPic.layer.cornerRadius = 18;
    [_imgVUserPic setClipsToBounds:YES];
    _btnFollow.layer.cornerRadius = 3.0;
    _imgVUserPic.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _imgVUserPic.layer.borderWidth = 1.0;

    
}
@end
