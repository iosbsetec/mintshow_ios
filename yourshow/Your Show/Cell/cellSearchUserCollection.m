//
//  cellSearchUserCollection.m
//  Your Show
//
//  Created by Bsetec on 31/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "cellSearchUserCollection.h"
#import <QuartzCore/QuartzCore.h>


@implementation cellSearchUserCollection

- (void)awakeFromNib {
    // Initialization code
    
    _imgUser.layer.cornerRadius = 30.0;
    _imgUser.clipsToBounds = 2.0;
    
    CALayer *layer = _imgUser.layer;
    layer.borderColor = [[UIColor lightGrayColor] CGColor];
    layer.borderWidth = 2;
}

-(void) setCellData  :(NSArray *) array
{
    _imgUser.image = [UIImage imageNamed:[array objectAtIndex:1]];
    _lblName.text = [array objectAtIndex:0];

}




@end
