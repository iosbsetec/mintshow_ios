//
//  VideoCell.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 05/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *previewImageView;
@property (strong, nonatomic) IBOutlet UILabel *videoTimeLabel;

@end
