//
//  RemintAddDescriptionCell.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 21/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTweetLabel.h"
#import "RoundedRect.h"
@interface RemintTextMintCell : UITableViewCell
@property (retain, nonatomic) IBOutlet    STTweetLabel *txtmintDescription;
@property(nonatomic,retain) IBOutlet    UILabel *lblCategoryType;
@property (retain, nonatomic) IBOutlet    UIView *viewComponent;
@property (nonatomic, retain) IBOutlet    UIView *desComponent;

-(void)setWithInfoAlignment : (BaseFeedModel *) newModel;

@end
