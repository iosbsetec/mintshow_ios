//
//  MintSelectOptionTableViewCell.m
//  Your Show
//
//  Created by Siba Prasad Hota  on 8/12/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MintSelectOptionTableViewCell.h"
#import "RoundedRect.h"
@implementation MintSelectOptionTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.selectedButtonTag =0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)buttonTapped:(UIButton *)sender {
    
    [sender setSelected:YES];
    [self.delegate didSelectOptionButtonForAddmint:sender];
    
}

-(void)setCustomDelegate : (id)sender
{
    self.delegate = sender;
    [self setImageForSelected];
}
-(void)deselectAll
{
    self.selectedButtonTag=0;
    for (UIButton *button in [_roundRect subviews])  {
            [button setSelected:NO];
        }
}


-(void)setButtonSelectdWithTag : (NSInteger ) tagValue
{
    self.selectedButtonTag = tagValue;
    [self setImageForSelected];

}

-(void)setImageForSelected
{
    for (UIButton *button in [_roundRect subviews]) {
        if (button.tag == self.selectedButtonTag) {
            [button setSelected:YES];
        }
        else
            [button setSelected:NO];
    }
}

@end
