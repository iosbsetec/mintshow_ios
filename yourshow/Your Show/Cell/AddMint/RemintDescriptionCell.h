//
//  RemintDescriptionCell.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 21/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMATweetView.h"
#import "RoundedRect.h"
#import "STTweetLabel.h"

@protocol RemintDescriptionCellDelegate <NSObject>


-(void)hashTagSelected : (NSString *)selectedEnitity;


@end


@interface RemintDescriptionCell : UITableViewCell
@property(nonatomic,retain)IBOutlet     RoundedRect *viewTop;
@property(nonatomic,retain)IBOutlet     UITextView *txtDescription;
@property(nonatomic,retain)IBOutlet     UIImageView *imgUser;
@property(nonatomic,retain) IBOutlet    UILabel *lblCategoryType;
@property (weak, nonatomic) IBOutlet    STTweetLabel *mintDescription;
@property (weak, nonatomic) IBOutlet    UIView *viewComponent;
@property (nonatomic,assign) id <RemintDescriptionCellDelegate> aDelegate;

-(void)setWithInfoAlignment : (BaseFeedModel *) newModel
;
@end
