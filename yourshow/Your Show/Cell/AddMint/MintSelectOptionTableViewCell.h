//
//  MintSelectOptionTableViewCell.h
//  Your Show
//
//  Created by Siba Prasad Hota  on 8/12/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RoundedRect;
@protocol mintselectOptionButtonSelect <NSObject>

-(void)didSelectOptionButtonForAddmint  : (UIButton *) selectedButton;

@end

@interface MintSelectOptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *camerabtn;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn;
@property (weak, nonatomic) IBOutlet UIButton *videoBtn;
@property (weak, nonatomic) IBOutlet UIButton *youtubeBtn;
@property (weak, nonatomic) IBOutlet RoundedRect   *roundRect;
@property (nonatomic, assign)  id  <mintselectOptionButtonSelect> delegate;
@property (nonatomic, assign)  NSInteger selectedButtonTag;



-(void)setCustomDelegate : (id)sender;
-(void)deselectAll;
-(void)setButtonSelectdWithTag : (NSInteger ) tagValue;



@end
