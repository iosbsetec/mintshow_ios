//
//  MintImageTableViewCell.h
//  Your Show
//
//  Created by Siba Prasad Hota  on 8/12/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MintImageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mintImageView;

@end
