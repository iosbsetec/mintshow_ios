//
//  MintImageTableViewCell.m
//  Your Show
//
//  Created by Siba Prasad Hota  on 8/12/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MintImageTableViewCell.h"

@implementation MintImageTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.mintImageView.layer.cornerRadius = 10.0;
    self.mintImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
