//
//  RemintAddDescriptionCell.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 21/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "RemintTextMintCell.h"
#import "NSString+Emojize.h"

@implementation RemintTextMintCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setWithInfoAlignment : (BaseFeedModel *) newModel
{
    // Description
    NSString *OriginalFrontendText      = [[newModel.feed_description emojizedString]StringByChangingComplimintStringToNormalString];
    UIFont *font                        = [UIFont fontWithName:@"HelveticaNeue" size:11];
    CGFloat newRect                      = [OriginalFrontendText boundingRectWithSize:CGSizeMake(_txtmintDescription.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    
    _txtmintDescription.font = font;

    _desComponent.layer.cornerRadius = 4.0;
    
    CGRect frameViewDescription         = _txtmintDescription.frame;
    frameViewDescription.size.height    = newRect+5;
    [_txtmintDescription setFrame:frameViewDescription];

    CGRect frameDescriptionHolder         = _desComponent.frame;
    frameDescriptionHolder.size.height    = newRect+5;
    [_desComponent setFrame:frameDescriptionHolder];
    
//    
//    // Category & Social Holder
//    CGRect frameViewComponent           = _viewComponent.frame;
//    frameViewComponent.origin.y         = _txtmintDescription.frame.origin.y + _txtmintDescription.frame.size.height +8;
//    _viewComponent.frame                = frameViewComponent;
}
@end
