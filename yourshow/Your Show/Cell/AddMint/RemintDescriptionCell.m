//
//  RemintDescriptionCell.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 21/09/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "RemintDescriptionCell.h"
#import "YSSupport.h"
#import "NSString+Emojize.h"


@implementation RemintDescriptionCell
@synthesize txtDescription;
@synthesize imgUser;

- (void)awakeFromNib
{
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setWithInfoAlignment : (BaseFeedModel *) newModel
{
    // Calculate Image Height
    CGRect imageFrame       = imgUser.frame;
    CGFloat widthCell       = imgUser.frame.size.width;
    
    CGSize rctSizeFinal;
    if (newModel.mintType == MSMintTypeVideo||newModel.mintType == MSMintTypeYouTube||newModel.mintType == MSMintTypeInsthaGram) {
        rctSizeFinal            = CGSizeMake(200,200);
    } else {
        CGSize rctSizeOriginal  = (newModel.feed_thumb_height>=300)?CGSizeMake(newModel.feed_thumb_Width , newModel.feed_thumb_height): CGSizeMake(300 , 300);
        double scale            = (widthCell  - (2 + 2)) / rctSizeOriginal.width;
        rctSizeFinal            = CGSizeMake(rctSizeOriginal.width*scale,rctSizeOriginal.height*scale);
    }
    
    imageFrame.size.height  = rctSizeFinal.height;
    imgUser.frame           = imageFrame;
    
    
    // Description
    NSString *OriginalFrontendText    = [[newModel.feed_description emojizedString] StringByChangingComplimintStringToNormalString];
    UIFont *font                      = [UIFont fontWithName:@"HelveticaNeue" size:11];
    CGRect newRect                    = [OriginalFrontendText boundingRectWithSize:CGSizeMake(_mintDescription.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
    
    CGRect frameViewDescription     = _mintDescription.frame;
    frameViewDescription.origin.y   = imageFrame.origin.y + imageFrame.size.height+5;
    frameViewDescription.size.height = newRect.size.height+5;
    _mintDescription.frame          = frameViewDescription;
    _mintDescription.font = font;
    
    // Image & Description Holder
    CGRect frameViewTop     = _viewTop.frame;
    frameViewTop.size.height = imageFrame.origin.y + imageFrame.size.height+newRect.size.height + 10;
    _viewTop.frame          = frameViewTop;
    
    
//    // Category & Social Holder
//    CGRect frameViewComponent       = _viewComponent.frame;
//    frameViewComponent.origin.y        = _viewTop.frame.origin.y + _viewTop.frame.size.height +7;
//    _viewComponent.frame      = frameViewComponent;
////    float newHeight = _viewComponent.frame.origin.y + _viewComponent.frame.size.height + 40 ;
////    return newHeight;
}
@end
