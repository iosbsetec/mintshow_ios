//
//  CellDiscoverShowrooms.h
//  Yaac21
//
//  Created by bsetec on 7/21/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellDiscoverShowrooms : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *Cellimgview;
@property (strong, nonatomic) IBOutlet UIImageView *checkMarck;

@property (strong, nonatomic) IBOutlet UILabel *lblmain;
@property (strong, nonatomic) IBOutlet UILabel *lblMintCount;
@property (strong, nonatomic) IBOutlet UILabel *lblMemberCount
;
@property (strong, nonatomic) IBOutlet UIButton *btnJoin;
@property (strong, nonatomic) IBOutlet UIImageView *imgVRound;
@property (strong, nonatomic) IBOutlet UIImageView *imgVSqr;
@property (weak, nonatomic) IBOutlet UIImageView *imgVStar;

@end
