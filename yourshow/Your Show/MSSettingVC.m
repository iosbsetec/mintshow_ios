//
//  MSSettingVC.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 28/06/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "MSSettingVC.h"
#import "UserAccessSession.h"

@interface MSSettingVC () <UIWebViewDelegate>
{

    IBOutlet UIWebView *webSettingsView;

}
@end

@implementation MSSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/m/showroom/user_settings?mobile=on&token=%@",ServerUrlSetting,GETVALUE(CEO_AccessToken)]];
    webSettingsView.delegate = self;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webSettingsView loadRequest:requestObj];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)cancelViewSetting
{
   // [self.view removeFromSuperview];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    //[self.navigationController popToRootViewControllerAnimated:YES];
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"\n\n-- %@\n--%@\n\n",request.URL,[request.URL absoluteString]);
    
    
    if (!([[NSString stringWithFormat:@"%@",[request URL]] rangeOfString:@"deactivateaccounts"].location == NSNotFound))
    {
        [self accountDeleteOrDeactivate:NO];
        return NO;
    }
    
    if (!([[NSString stringWithFormat:@"%@",[request URL]] rangeOfString:@"deleteaccounts"].location == NSNotFound))
    {
        [self accountDeleteOrDeactivate:YES];
        return NO;
    }
    if (!([[NSString stringWithFormat:@"%@",[request URL]] rangeOfString:@"change_Profile_info"].location == NSNotFound))
    {
        NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
        NSArray *urlComponents = [[request.URL absoluteString] componentsSeparatedByString:@"&"];
        
        for (NSString *keyValuePair in urlComponents)
        {
            NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
            NSString *key           = [[pairComponents firstObject] stringByRemovingPercentEncoding];
            NSString *value         = [[pairComponents lastObject] stringByRemovingPercentEncoding];
            
            [queryStringDictionary setObject:value forKey:key];
        }
        [self nameUpdatedSuccesFully:[queryStringDictionary objectForKey:@"first_name"] lastName:[queryStringDictionary objectForKey:@"last_name"]];
        
        return NO;
    }
    
    
    return YES;
}

-(void)nameUpdatedSuccesFully:(NSString *)firstName lastName:(NSString *)lastName
{
    [webSettingsView stopLoading];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
    SETVALUE(firstName,CEO_UserFirstName);
    SETVALUE(lastName,CEO_UserLastName);
    SYNCHRONISE;
    
    showAlert(nil, @"Successfully Updated", @"ok", self);
}
-(void)accountDeleteOrDeactivate :(BOOL)isDelete
{
    
    showAlert(((isDelete)?@"Your account is now Deleted":@"Your account is now Deactivated"),nil, @"OK", nil)
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0 ;
    [YSSupport resetDefaultsLogout:isDelete];
    [UserAccessSession clearAllSession];
    AppDelegate *appdell = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appdell logOutDidFinish:self.tabBarController];
    
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
}

@end
