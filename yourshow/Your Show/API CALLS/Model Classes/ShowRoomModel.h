//
//  ShowRoomModel.h
//  Your Show
//
//  Created by Siba Prasad Hota on 30/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowRoomModel : NSObject

@property(nonatomic,strong)     NSString  *status;
@property(nonatomic,strong)     NSString  *status_Msg;
@property(nonatomic,strong)     NSString  *status_code;

@property(nonatomic,strong)     NSString  *showroom_id;
@property(nonatomic,strong)     NSString  *showroom_link;
@property(nonatomic,strong)     NSString  *showroom_name;
@property(nonatomic,strong)     NSString  *showroom_type;
@property(nonatomic,strong)     NSString  *showroom_categoryName;
@property(nonatomic,strong)     NSString  *is_user_joined;
@property(nonatomic,strong)     NSString  *showroom_img_Url;
@property(nonatomic,strong)     NSString  *showroom_image;
@property(nonatomic,strong)     NSString  *showroom_imageSmall;

@property(nonatomic,strong)     NSString  *showroom_tag_keywords_ids;
@property(nonatomic,strong)     NSString  *showroom_category_ids;
@property(nonatomic,strong)     NSString  *follow_icon_status;
@property(nonatomic,strong)     NSString  *follow_status;
@property(nonatomic,strong)     NSString  *member_count;
@property(nonatomic,strong)     NSString  *show_tag;
@property(nonatomic,strong)     NSString  *mint_count;
@property(nonatomic,strong)     NSString  *privacy_settings;
@property(nonatomic,strong)     NSString  *show_option;
@property(nonatomic,strong)     NSString  *showroom_description;
@property(nonatomic,strong)     NSString  *user_link;
@property(nonatomic,strong)     NSString  *user_name;
@property(nonatomic,strong)     NSString  *user_thumb_image_url;
@property(nonatomic,strong)     NSString  *selected,*showroom_createdby_user_id;


-(ShowRoomModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict;

@end




