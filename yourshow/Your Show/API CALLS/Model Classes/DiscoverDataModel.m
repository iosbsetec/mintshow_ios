//
//  DiscoverDataModel.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 03/02/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "DiscoverDataModel.h"

@implementation DiscoverDataModel
-(id)initWithDiscoverInfo : (NSDictionary *) dicoverInfo ForTrendingShowroom : (BOOL)isForTrendingShowroom
{
    self =  [super init];

    if (self) {
        self.discoverText                   =   (!isForTrendingShowroom)?[dicoverInfo valueForKey:@"feed_text"]:[dicoverInfo valueForKey:@"showroom_description"];
        self.discoverName                   =   (!isForTrendingShowroom)?[NSString stringWithFormat:@"#%@",[dicoverInfo valueForKey:@"hashtag_key"]]: [dicoverInfo valueForKey:@"showroom_tag"];
        self.discoverId                     =   (!isForTrendingShowroom)?[dicoverInfo valueForKey:@"hashtag_id"]:[dicoverInfo valueForKey:@"showroom_id"];
        self.discoverImg                    =   (!isForTrendingShowroom)?[dicoverInfo valueForKey:@"feed_media"]:(([dicoverInfo valueForKey:@"showroom_img"])?[dicoverInfo valueForKey:@"showroom_img"]:[dicoverInfo valueForKey:@"showroom_img_small"]);
        
        if ([dicoverInfo valueForKey:@"showroom_type"])
            self.discover_showroom_name =   [dicoverInfo valueForKey:@"showroom_type"];

        

        self.mint_count                   =   [dicoverInfo valueForKey:@"mint_count"];

        

        if ([dicoverInfo valueForKey:@"at_mentioned_users"]) {
            NSArray *arrUserList = [dicoverInfo valueForKey:@"at_mentioned_users"];
            self.at_mentioned_users  = arrUserList;
        }
        
        if (isForTrendingShowroom) {
            self.discoverType                   =   @"SHOWROOM";
                   }
        else
        {

            if ([[dicoverInfo valueForKey:@"feed_media"] length]) {
                self.discoverType                   =   @"IMAGE";
            }
            else
                self.discoverType                   =   @"TEXT";
         

        }
    }

    return self;
}


-(id)initWithSignatureShowroom : (NSDictionary *) dicoverInfo
{
    self =  [super init];
    
    if (self) {
        
        self.discoverText                   =  [dicoverInfo valueForKey:@"showroom_description"];
        self.discoverName                   =    [dicoverInfo valueForKey:@"showroom_tag"];
//        self.discoverId                     =   (!isForTrendingShowroom)?[dicoverInfo valueForKey:@"hashtag_id"]:[dicoverInfo valueForKey:@"showroom_id"];
        self.discoverImg                    =  [dicoverInfo valueForKey:@"showroom_img_small"];
        
        self.mint_count                   =   [dicoverInfo valueForKey:@"mint_count"];
        
        
            self.discoverType                   =   @"SHOWROOM";
        }
    
    return self;
}


@end
