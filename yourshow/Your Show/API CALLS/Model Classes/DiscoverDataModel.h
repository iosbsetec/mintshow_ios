//
//  DiscoverDataModel.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 03/02/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiscoverDataModel : NSObject

@property(nonatomic,strong)  NSString *discoverText; // (showroom_description/feed_text)//it will be empty if its a text mint for trending_tags.
@property(nonatomic,strong)  NSString *discoverName; // (it will be both for trending_showroom name i.e showroom_name /trending_tags name i.e hashtag_key)
@property(nonatomic,strong)  NSString *discoverId;   // (it will be both for trending_showroom i.e showroom_id/trending tags i.e hashtag_id)
@property(nonatomic,strong)  NSString *discoverImg;  // it will be empty if its a image mint (i.e "trending_showroom" ->"showroom_img" /trending_tags -> feed_media).
@property(nonatomic,strong)  NSString *discoverType;  // it will be for the image/video/text type
@property(nonatomic,strong)  NSString *discover_showroom_name;  

@property(nonatomic,strong)  NSString *mint_count;  // it will be for the image/video/text type
@property(nonatomic,retain)  NSArray *at_mentioned_users;

-(id)initWithDiscoverInfo : (NSDictionary *) dicoverInfo ForTrendingShowroom : (BOOL)isForTrendingShowroom;
-(id)initWithSignatureShowroom : (NSDictionary *) dicoverInfo;
@end

