//
//  ShowRoomModel.m
//  Your Show
//
//  Created by Siba Prasad Hota on 30/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "ShowRoomModel.h"
#import "NSNull+JSON.h"


@implementation ShowRoomModel

-(ShowRoomModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict
{
    if ([showRoomDict valueForKey:@"big_cover"])
    {
        self.showroom_img_Url       =   [showRoomDict valueForKey:@"big_cover"];
        self.showroom_image         =   [showRoomDict valueForKey:@"medium_cover"];
        self.showroom_imageSmall    =   [showRoomDict valueForKey:@"medium_cover"];

    }
    else
    {
        self.showroom_img_Url       =   [showRoomDict valueForKey:@"showroom_img_medium"];
        self.showroom_image         =   [showRoomDict valueForKey:@"showroom_img_big"];
        self.showroom_imageSmall    =   [showRoomDict valueForKey:@"showroom_img_small"];

        self.selected               =   @"0";

    }
    self.showroom_name              =    [[showRoomDict valueForKey:@"showroom_name"]stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    self.showroom_id               =   [showRoomDict valueForKey:@"showroom_id"];
    self.showroom_categoryName     = [showRoomDict valueForKey:@"showroom_category"];
    self.showroom_category_ids     = [showRoomDict valueForKey:@"showroom_category_ids"];
    self.showroom_tag_keywords_ids = [showRoomDict valueForKey:@"showroom_keywords"];
    self.showroom_description      = [showRoomDict valueForKey:@"showroom_description"];
    self.showroom_link             =   ([showRoomDict valueForKey:@"showroom_url"])?[showRoomDict valueForKey:@"showroom_url"]:[showRoomDict valueForKey:@"showroom_share_url"];
    self.mint_count                =   [showRoomDict valueForKey:@"mint_count"];
    self.member_count              =   [showRoomDict valueForKey:@"member_count"];
    self.privacy_settings          =   [showRoomDict valueForKey:@"privacy_settings"];
    self.showroom_type             =   [showRoomDict valueForKey:@"privacy_type"];
    self.show_tag                  =   [showRoomDict valueForKey:@"showroom_tag"];
    self.showroom_createdby_user_id  =   [showRoomDict valueForKey:@"showroom_createdby_user_id"];

    
    if ([showRoomDict valueForKey:@"is_member"]) {

        self.is_user_joined  = [[showRoomDict valueForKey:@"is_member"] isEqualToString:@"yes"]?@"YES":@"NO";
    }
    if ([showRoomDict valueForKey:@"is_user_joined"])
    {
        self.is_user_joined  = [showRoomDict valueForKey:@"is_user_joined"];
    }




    self.follow_status      =   [showRoomDict valueForKey:@"follow_status"];
    self.follow_icon_status =   [showRoomDict valueForKey:@"follow_icon_status"];
    self.show_option        =   ([showRoomDict valueForKey:@"show_option"])?[showRoomDict valueForKey:@"show_option"]:[showRoomDict valueForKey:@"show_options"];
    self.user_link          =   [showRoomDict valueForKey:@"user_link"];
    
    if ([showRoomDict valueForKey:@"showroom_createdby_userimage"]) {
        self.user_thumb_image_url=  [showRoomDict valueForKey:@"showroom_createdby_userimage"];
        self.user_name          =   [showRoomDict valueForKey:@"showroom_createdby_username"];
    }
    else
    {
        self.user_name          =   [showRoomDict valueForKey:@"user_name"];
        self.user_thumb_image_url=  [showRoomDict valueForKey:@"user_thumb_image"];
    }

    return self;
}

@end



