//
//  CatModel.h
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 06/10/14.
//
//

#import <Foundation/Foundation.h>

@interface CatModel : NSObject

@property(nonatomic,assign)BOOL isCustomMint;


@property(nonatomic,strong)NSString *cat_id;
@property(nonatomic,strong)NSString *cat_name;
@property(nonatomic,strong)NSString *mint_type_id;
@property(nonatomic,strong)NSString *mint_type_name;

@end
