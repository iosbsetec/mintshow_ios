//
//  Generalmodel.h
//  YACC
//
//  Created by Siba Prasad Hota on 04/11/14.
//  Copyright (c) 2014 BseTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Generalmodel : NSObject

@property int numberOfMission;

@property(nonatomic,strong)  NSString *status;
@property(nonatomic,strong)  NSString *status_Msg;
@property(nonatomic,strong)  NSString *status_code;
@property(nonatomic,strong)  NSString *temprecord;
@property(nonatomic,strong)  NSString *accesToken;
@property(nonatomic,strong)  NSString *userId,*mintdetail_url;
@property(nonatomic,strong)  NSString        *pull_request_time,*member_count,*currentpage,*totalpages;

@property(nonatomic,strong)  NSString *achievement_id;
@property(nonatomic,strong)  NSString *profile_totalcount,*profile_followingcount,*profile_followers_count;

@property(nonatomic,strong)  NSMutableArray *tempArray;
@property(nonatomic,strong)  NSMutableArray *tempArray2;
@property(nonatomic,strong)  NSMutableArray *tempArray3;


@property(nonatomic,strong) NSString *request_count;
@property(nonatomic,strong) NSString *you_count;
@property(nonatomic,strong) NSString *total_count;


@property(nonatomic,strong)  NSDictionary *serchDefaultWords;
@property(nonatomic,strong)  NSDictionary *searchPeople;


@end
