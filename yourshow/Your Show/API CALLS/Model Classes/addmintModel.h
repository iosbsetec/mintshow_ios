//
//  addmintModel.h
//  Your Show
//
//  Created by Siba Prasad Hota on 21/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LocatioDetails.h"

@interface addmintModel : NSObject

@property(nonatomic,assign)     BOOL   isImageSelected;


@property(nonatomic,assign)     UIImage        *selectedImage;
@property(nonatomic,assign)     NSString       *descriptionText;
@property(nonatomic,assign)     NSString       *categoryname;
@property(nonatomic,assign)     NSString       *selectedYoutubeURL;
@property(nonatomic,assign)     NSString       *selectedVideoURL;
@property(nonatomic,assign)     LocatioDetails *selectedLocation;

@end
