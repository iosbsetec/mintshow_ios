//
//  YSAPICalls.h
//  Your Show
//
//  Created by Siba Prasad Hota on 29/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseFeedModel.h"
#import "Generalmodel.h"
#import "FBFriends.h"
#import "Profilemodel.h"


@interface YSAPICalls : NSObject

//Checking for whiteSpace
+(BOOL)CheckForWhiteSpace : (NSString *) string;
+ (NSString*)escapedValue:(NSString *)originalValue;

+(void)hifiveMint:(BaseFeedModel   *)fmodel;
+(void)inspireMint:(BaseFeedModel   *)fmodel;
+(void)nominateMint:(BaseFeedModel   *)fmodel;
+(void)followMint:(BaseFeedModel   *)fmodel;

+(void)filterSearch :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
     andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)flagMint :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
 andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)hifiveMint:(BaseFeedModel   *)fmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
  andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)nominateMint:(BaseFeedModel   *)fmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
    andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)inspireMint:(BaseFeedModel   *)fmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock;
//+(CommentModel *)CompleMint:(NSString *)completext  feedModel:(BaseFeedModel   *)achievementdetails;

- (void)CompleMint:(NSString*)completext feedModel:(BaseFeedModel   *)achievementdetails withSuccessionBlock:(void(^)(id response))successBlock andFailureBlock:(void(^)(NSError *error))failureBlock;

+(NSDictionary *)getUrlAndSubmitDataForAll :(BaseFeedModel   *) feedModelObject;
+ (BOOL) isNetworkRechable;
+(NSMutableArray *)mintpe;
+(void)getAllShowroomForDiscoverTabwithSuccessionBlock:(void(^)(id newResponse))successBlock
                                       andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getAllShowroomForMyShowTabwithDict  : (NSDictionary * ) dict SuccessionBlock:(void(^)(id newResponse))successBlock
                            andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)AddShowroomWithData :(NSData *)submitData ForSuccessionBlock:(void(^)(id newResponse))successBlock
            andFailureBlock:(void(^)(NSError *error))failureBlock;
/*
+(void)AddMint :(NSData *)submitData ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock;

*/

+(void)AddMint :(NSData *)submitData textMint:(BOOL)istextMint ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock;



//FOLLOW UNFOLLOW USER (USED FOR JOIN FUNCTIONALITY ALSO IN SHOWROOM)
+(void)followUnfollowMint:(id)fmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
  andFailureBlock:(void(^)(NSError *error))failureBlock;


+(void)getMintsForMyShowTabwithDict  : (NSDictionary * ) dict SuccessionBlock:(void(^)(id newResponse))successBlock
                      andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)HashTag :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock
;
+(void)getShowrooms :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
     andFailureBlock:(void(^)(NSError *error))failureBlock;
- (void)CompleMint:(NSString*)completext
         feedModel:(BaseFeedModel   *)achievementdetails
               url:(NSString*)addUrl
withSuccessionBlock:(void(^)(id response))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock;
- (void)editCompleMint:(NSString*)completext
             feedModel:(BaseFeedModel   *)achievementdetails
                   url:(NSString*)addUrl
             commentId:(NSString*)commentId
   withSuccessionBlock:(void(^)(id response))successBlock
       andFailureBlock:(void(^)(NSError *error))failureBlock;


+(void)MintDetail :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)muteAMint :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
  andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)muteAUser :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
  andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)myshowMintDetails : (NSDictionary * ) dict ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getAllComplimints :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
          andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)remintMint :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)deleteMint :(NSDictionary *)dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock;

//EDIT MINT API
+(void)EditMint :(NSData *)submitData ForSuccessionBlock:(void(^)(id newResponse))successBlock
 andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getHashTagMints  : (NSDictionary * ) dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
        andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getParticularShowroom  : (NSDictionary * ) dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
         andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getAllFriendsInCircle :(NSData *)submitData methodName:(NSString *)methodName  ForSuccessionBlock:(void(^)(id newResponse))successBlock andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getShowDetails :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getMemberList: (NSData * )submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
     andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getForDiscoverTab:(NSData *)submitData withSuccessionBlock:(void(^)(id newResponse))successBlock
         andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)UpdateCoachingTipsFirstTimeVisit: (NSData * )submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
            andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)signUp :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)FBFriendfollowUnfollowMint:(FBFriends *)fbmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
                  andFailureBlock:(void(^)(NSError *error))failureBlock;


//PROFILE  API
+(void)getLoggedInUserProfile  : (NSDictionary * ) dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
                andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getUserProfile  : (NSDictionary * ) dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
        andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getProfileFollowers :(NSData *)submitData   ForSuccessionBlock:(void(^)(id newResponse))successBlock
            andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getProfileFollowUnFollow:(Profilemodel *)pmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
                andFailureBlock:(void(^)(NSError *error))failureBlock;


+(void)editProfileDetails :(NSDictionary * ) dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
           andFailureBlock:(void(^)(NSError *error))failureBlock;


+(void)getActivitySegmentInfo:(NSDictionary * )dict methodName:(NSString * )methodName SuccessionBlock:(void(^)(id newResponse))successBlock
              andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getSuggestionList :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
          andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getProfileStatsDetails :(NSDictionary *)dict   ForSuccessionBlock:(void(^)(id newResponse))successBlock
               andFailureBlock:(void(^)(NSError *error))failureBlock;
@end

