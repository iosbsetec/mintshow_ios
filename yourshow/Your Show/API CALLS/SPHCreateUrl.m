

//==============================================================================
///////////    THIS CLASS CREATES ALL URL             //////////////////////////
//==============================================================================



#import "SPHCreateUrl.h"

@implementation SPHCreateUrl

@synthesize
url = _url,
methodName=_methodName,
params = _params,
error = _error;




//==============================================================================
///////////    THIS BELOW METHOD SEPARATES ALL METHODS  ////////////////////////
//==============================================================================


 // Single Pameters
+ (NSString*)SerializeURLWithSingleParam:(NSString *)parameter
                              methodName:(NSString *)method_Name
{
    NSLog(@"Create URL Method Name : %@   AND Passed parameter = %@",method_Name,parameter);
    
    if ([method_Name isEqualToString:METHOD_DISCOVER_ALL])
        return [self serializeURLForDiscoverAll:parameter];
    

    return nil;
}

+ (NSString*)SerializeURL:(NSDictionary *)params  methodName:(NSString *)method_Name
{
    NSLog(@"Create URL Method Name : %@   AND Passed params = %@",method_Name,params);
    
    if ([method_Name isEqualToString:METHOD_MYSHOW_SHOWROOM])
        return [self serializeURLForMyShowShowroom:params];
    if ([method_Name isEqualToString:METHOD_LOGIN])
        return [self serializeURLForLogin:params];
    if ([method_Name isEqualToString:METHOD_MYSHOW_MINT])
        return [self serializeURLForMintMyshow:params];
    
    if ([method_Name isEqualToString:METHOD_GETHASHTAGMINTS])
        return [self serializeURLForgetHashTagMints:params];
    if ([method_Name isEqualToString:METHOD_GETPARTICULARSHOWROOM])
        return [self serializeURLForgetPartcicularShowroom:params];

    if ([method_Name isEqualToString:@""])
        return [self serializeURLForLogin:params];
    
    if ([method_Name isEqualToString:METHOD_MINTDETAIL])
        return [self serializeURLForMintDetail:params];
    
    
    if ([method_Name isEqualToString:METHOD_MYSHOW_MINTDETAIL])
        return [self serializeURLForMyshowMintDetail:params];
    
    if ([method_Name isEqualToString:METHOD_DELETE_MINT])
        return [self serializeURLForDeleteMint:params];

    
    if ([method_Name isEqualToString:METHOD_SIGNUP])
        return [self serializeURLForSignUp:params];
    
    if ([method_Name isEqualToString:METHOD_GETUSERPROFILE])
        return [self serializeURLForGetUserProfile:params];
    if ([method_Name isEqualToString:METHOD_FILTERSEARCH])
        return [self serializeURLForFilterSearch:params];

    if ([method_Name isEqualToString:METHOD_GETACTIVITYYOU] || [method_Name isEqualToString:METHOD_GETACTIVITYREQUEST])
        return [self serializeURLForActivityYouAndRequest:params];
    
    if ([method_Name isEqualToString:METHOD_GETACTIVITYMYCIRCLE])
        return [self serializeURLForActivityMyCircle:params];
  
    
    if ([method_Name isEqualToString:METHOD_EDITPROFILE])
    {
        return [self serializeURLForEditProfilesDetails:params];
    }
    if ([method_Name isEqualToString:METHOD_PROFILESTATS])
        return [self serializeURLForGetProfileStatsDetails:params];
    
    
    return nil;
}
+ (NSString*)serializeURLForGetProfileStatsDetails:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_GETPROFILESTATSDETAILS,query];
}

+ (NSString*)serializeURLForEditProfilesDetails:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"first_name=%@",params[@"first_name"]]];
    [pairs addObject:[NSString stringWithFormat:@"last_name=%@",params[@"last_name"]]];
    [pairs addObject:[NSString stringWithFormat:@"text=%@",params[@"text"]]];
    [pairs addObject:[NSString stringWithFormat:@"user_image=%@",params[@"user_image"]]];
    [pairs addObject:[NSString stringWithFormat:@"location_text=%@",params[@"location_text"]]];
    [pairs addObject:[NSString stringWithFormat:@"url=%@",params[@"url"]]];
    [pairs addObject:[NSString stringWithFormat:@"lat=%@",params[@"lattitude"]]];
    [pairs addObject:[NSString stringWithFormat:@"lon=%@",params[@"longitude"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [[NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_PROFILEEDIT,query] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}
+ (NSString*)serializeURLForActivityMyCircle:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_GETACTIVITYMYCIRCLE,query];
    
}

+ (NSString*)serializeURLForActivityYouAndRequest:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
    [pairs addObject:[NSString stringWithFormat:@"type=%@",params[@"type"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_GETACTIVITYYOUANDREQUEST,query];
    
}


+ (NSString*)serializeURLForFilterSearch:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"sort_by_option=%@",params[@"sort_by_option"]]];
    [pairs addObject:[NSString stringWithFormat:@"category_id=%@",params[@"category_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"sort_by_feature=%@",params[@"sort_by_feature"]]];
    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_FILTERSEARCH,query];
    
}

+ (NSString*)serializeURLForSignUp:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"firstname=%@",params[@"firstname"]]];
    [pairs addObject:[NSString stringWithFormat:@"lastname=%@",params[@"lastname"]]];
    [pairs addObject:[NSString stringWithFormat:@"email=%@",params[@"email"]]];
    [pairs addObject:[NSString stringWithFormat:@"password=%@",params[@"password"]]];
    [pairs addObject:[NSString stringWithFormat:@"gender=%@",params[@"gender"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_SIGNUP,query];

}
//Mint Detail
+ (NSString*)serializeURLForMintDetail:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"mint_id=%@",params[@"mint_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"related_mint=%@",params[@"related_mint"]]];
    [pairs addObject:[NSString stringWithFormat:@"page_num=%@",params[@"page_num"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_MINTDETAIL,query];
}
+ (NSString*)serializeURLForMyshowMintDetail:(NSDictionary *)params
{
    // @"access_token=%@&firstTimeRequest=%@&mintRowId=%d&page_num=%d"
    
    
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"mintRowId=%@",params[@"mintRowId"]]];
    if ([[params valueForKey:@"user_id"] length]) {
        [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    }
    
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    
    NSLog(@"Url =%@",[NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_MYSHOW_MINTDETAIL,query]);
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_MYSHOW_MINTDETAIL,query];
}

+ (NSString*)serializeURLForDeleteMint:(NSDictionary *)params
{
    // @"access_token=%@&firstTimeRequest=%@&mintRowId=%d&page_num=%d"
    
    
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"feed_id=%@",params[@"feed_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"deletetype=%@",params[@"deletetype"]]];

    NSString* query = [pairs componentsJoinedByString:@"&"];
    
    NSLog(@"Url =%@",[NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_DELETE_MINT,query]);
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_DELETE_MINT,query];
}

//==============================================================================
///////////    ALL  METHOD SEPARATED ABOVE            //////////////////////////
//==============================================================================



+ (NSString*)serializeURLForSingle:(NSString *)params
{
    return [NSString stringWithFormat:@"%@/users/get_login",@""];
}
+ (NSString*)serializeURLForLogin:(NSDictionary *)params
{
    return [NSString stringWithFormat:@"%@users/get_login",ServerUrl];
}

+ (NSString*)serializeURLForDiscoverAll:(NSString *)params
{
    return [NSString stringWithFormat:@"%@%@?access_token=%@",ServerUrl,API_DISCOVER_ALL,params];
}
+ (NSString*)serializeURLForMintDetails:(NSString *)params
    {
        return [NSString stringWithFormat:@"%@%@?access_token=%@",ServerUrl,API_MYSHOW_MINTDETAIL,params];
    }
         //=
//==============================================================================

//dictionaryWithObjectsAndKeys:DummyAccessToken,@"access_token",@"0",@"showroom_list_type",@"0",@"page_number", nil];
+ (NSString*)serializeURLForMyShowShowroom:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",GETVALUE(CEO_AccessToken)]];
    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_MYSHOW_SHOWROOM,query];
    
}


+ (NSString*)serializeURLForMintMyshow:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"mint_list_type=%@",params[@"mint_list_type"]]];
    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
    if ([params valueForKey:@"pull_request_time"]) {
        [pairs addObject:[NSString stringWithFormat:@"pull_request_time=%@",params[@"pull_request_time"]]];
    }
    if ([params valueForKey:@"profile_user_id"]) {
        [pairs addObject:[NSString stringWithFormat:@"profile_user_id=%@",params[@"profile_user_id"]]];
    }
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_MYSHOW_MINTS,query];
}

+ (NSString*)serializeURLForgetHashTagMints:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"hash_key=%@",params[@"hash_key"]]];
    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
       NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_HASHTAGMINTS,query];
}
+ (NSString*)serializeURLForgetPartcicularShowroom:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"showroom_tag=%@",params[@"showroom_tag"]]];
    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];

    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_GETPARTICULARSHOWROOM,query];
}


+ (NSString*)serializeURLForGetUserProfile:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_GETUSERPROFILE,query];
}


//==============================================================================

+ (NSString*)escapedValue:(NSString *)originalValue
{
    NSString* escaped_value = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( NULL,(CFStringRef)originalValue,NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    return escaped_value;
}


@end

