//
//  SPHServiceRequest.h
//  Exam_View
//
//  Created by Heart on 31/05/14.
//  Copyright (c) 2014 BSEtec. All rights reserved.
//


//==============================================================================
///////////    THIS CLASS CREATES ALL REQUESTS        //////////////////////////
//==============================================================================


#import <Foundation/Foundation.h>


@interface SPHServiceRequest : NSObject <NSURLConnectionDelegate>

@property(nonatomic,strong) NSURLConnection *someConnection;



-(void)cancelReq:(SPHServiceRequest *)serviceRequest;


- (void)getBlockServerResponseForparam:(NSDictionary *)params
                                mathod:(NSString*)method
                 withSuccessionBlock:(void(^)(id response))successBlock
                     andFailureBlock:(void(^)(NSError *error))failureBlock;


- (void)getBlockServerResponseForString:(NSString *)params
                                 mathod:(NSString*)method
                    withSuccessionBlock:(void(^)(id response))successBlock
                        andFailureBlock:(void(^)(NSError *error))failureBlock;

- (void)postBlockServerResponseForData :(NSData *)somedata
                                 paramDict:(NSDictionary*)params
                                 mathod:(NSString*)method
                    withSuccessionBlock:(void(^)(id response))successBlock
                        andFailureBlock:(void(^)(NSError *error))failureBlock;

- (void)PostRequestWithUrlAndData:(NSData *)somedata
                              paramDict:(NSDictionary*)params
                                 mathod:(NSString*)method
                                   link:(NSURL*)url
                    withSuccessionBlock:(void(^)(id response))successBlock
                        andFailureBlock:(void(^)(NSError *error))failureBlock;

- (void)GETRequestWithUrlAndData:(NSDictionary*)params
                          mathod:(NSString*)method
                            link:(NSString*)url
             withSuccessionBlock:(void(^)(id response))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock;



@end
