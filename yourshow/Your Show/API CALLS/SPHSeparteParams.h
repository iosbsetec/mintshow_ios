
//==============================================================================
///////////    THIS CLASS SEPARATES ALL PARAMETRES    //////////////////////////
//==============================================================================

#import <Foundation/Foundation.h>

@protocol SeparateParamDelegate;

@interface SPHSeparteParams : NSObject

@property(nonatomic,retain) NSError* error;
@property(nonatomic,copy) NSString* methodName;
@property(nonatomic,retain) NSMutableDictionary* params;


+ (id)SeparateParams:(NSDictionary *)params  methodName:(NSString *)method_Name;

@end




