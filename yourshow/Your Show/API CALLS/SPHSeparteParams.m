


//==============================================================================
///////////    THIS CLASS SEPARATES ALL PARAMETRES    //////////////////////////
//==============================================================================


#import "SPHSeparteParams.h"
#import "NSNull+JSON.h"
#import "Generalmodel.h"
#import "ShowRoomModel.h"
#import "CommentModel.h"
#import "Userfeed.h"
#import "FriendsInfo.h"
#import "MemberList.h"
#import "DiscoverDataModel.h"
#import "BaseFeedModel.h"
#import "ActivityModel.h"
#import "RequestActivityModel.h"

@implementation SPHSeparteParams

@synthesize
methodName=_methodName,
params =_params,
error =_error;

// Profile_IamgeURL METHOD_USERDETAILS




//==============================================================================
///////////    THIS BELOW METHOD SEPARATES ALL METHODS  ////////////////////////
//==============================================================================


+ (id)SeparateParams:(NSDictionary *)params  methodName:(NSString *)method_Name
{
    
    if ([method_Name isEqualToString:METHOD_SEARCHSUGGETION])
        return [self SeparateParamForSearchSuggestion:params];
    if ([method_Name isEqualToString:METHOD_SIGNUP])
        return [self SeparateParamForUserLogin:params];
    if ([method_Name isEqualToString:METHOD_FILTERSEARCH])
        return [self SeparateParamForMyshowMints:params];
    if ([method_Name isEqualToString:METHOD_TRENDING_SHOWROOM])
        return [self SeparateParamForDiscoverAllTab:params];
    if ([method_Name isEqualToString:METHOD_DISCOVER_ALL])
        return [self SeparateParamForDiscoverAllTab:params];
    if ([method_Name isEqualToString:METHOD_MYSHOW_SHOWROOM])
        return [self SeparateParamForMyshowShowroom:params];
    if ([method_Name isEqualToString:METHOD_GETHASHTAGMINTS])
        return [self SeparateParamForHashTagMints:params];
    if ([method_Name isEqualToString:METHOD_GETPARTICULARSHOWROOM])
        return [self SeparateParamForParticularShowroom:params];
    if ([method_Name isEqualToString:METHOD_HIFIVE_MINT])
        return [self SeparateParamForhifivemint:params];
    if ([method_Name isEqualToString:METHOD_FOLLOW_UNFOLLOW])
        return [self seperateParamFollowunFollow:params];
    if ([method_Name isEqualToString:METHOD_GETUSERPROFILE])
        return [self seperateParamGetUserProfile:params];
    if ([method_Name isEqualToString:METHOD_FLAGMINT])
        return [self SeparateParamForFlagMint:params];
    if ([method_Name isEqualToString:METHOD_GETSHOWROOMDETAILS])
        return [self SeparateParamForGetShowroomDetails:params];
    if ([method_Name isEqualToString:METHOD_FOLLOWERLIST])
        return [self SeparateParamForFollowerList:params];
    if ([method_Name isEqualToString:METHOD_MEMBER_LIST])
        return [self SeparateParamForMemberList:params];
 
   
    if ([method_Name isEqualToString:METHOD_GETACTIVITYMYCIRCLE] || [method_Name isEqualToString:METHOD_GETACTIVITYREQUEST] || [method_Name isEqualToString:METHOD_GETACTIVITYYOU])
        return [self SeparateParamForGetActivityUserFeeds:params method:method_Name];
    
    
    if ([method_Name isEqualToString:METHOD_ADDORUPDATESHOWROOM])
        return [self SeparateParamForAddShowroom:params];
    if ([method_Name isEqualToString:METHOD_ADDMINT])
        return [self SeparateParamForAddShowrrom:params];
    if ([method_Name isEqualToString:METHOD_LOGIN])
        return [self SeparateParamForUserLogin:params];
    if ([method_Name isEqualToString:@""])
        return [self SeparateParamForComplimintUserList:params];
    if ([method_Name isEqualToString:METHOD_HASGTAG])
        return [self SeparateParamForHashTag:params];
    
    if ([method_Name isEqualToString:METHOD_MYSHOW_MINT])
        return [self SeparateParamForMyshowMints:params];
    
    if ([method_Name isEqualToString:METHOD_SINGLE_ACHIVMENT])
        return [self SeparateParamGet_single_achievements:params];
    
    if ([method_Name isEqualToString:METHOD_GET_SINGLEMINT])
        return [self SeparateParamForSingleMint:params];
    
    
    if ([method_Name isEqualToString:METHOD_GETALLCOMPLIMINTS])
        return [self SeparateParamForGetAllCompliments:params];
    
    
    if ([method_Name isEqualToString:METHOD_INSPIRE_MINT])
        return [self SeparateParamForhifivemint:params];
    
    if ([method_Name isEqualToString:METHOD_COMPLI_MINT])
        return [self SeparateParamForCompliMint:params];
    
    
    if ([method_Name isEqualToString:METHOD_MINTDETAIL])
        return [self seperateParamMintDetail:params];
    if ([method_Name isEqualToString:METHOD_MUTEUSER])
        return [self seperateParamForMuteUser:params];
    if ([method_Name isEqualToString:METHOD_MUTEMINT])
        return [self seperateParamForMuteMInt:params];
    if ([method_Name isEqualToString:METHOD_REMINTMINT])
        return [self SeparateParamFormintRemint:params];
    if ([method_Name isEqualToString:METHOD_SINGLE_MINTDETAIL])
        return [self seperateParamSingleMintDetails:params];
    
    if ([method_Name isEqualToString:METHOD_MYSHOW_MINTDETAIL])
        return [self seperateParamMyshowMintDetais:params];
    
    if ([method_Name isEqualToString:METHOD_DELETE_MINT])
        return [self SeparateParamForDeleteMint:params];
   
    if ([method_Name isEqualToString:METHOD_EDITMINT])
        return [self SeparateParamForEditMint:params];
    
    if ([method_Name isEqualToString:METHOD_FIRSTTIMEVISIT])
        return [self SeparateParamForCoachingTipsUpdate:params];
    
    if ([method_Name isEqualToString:METHOD_PROFILEFOLLOWING])
        return [self SeparateParamForProfileFollowingList:params];
    
    if ([method_Name isEqualToString:METHOD_PROFILEFOLLOWERS])
        return [self SeparateParamForProfileFollowersList:params];

    if ([method_Name isEqualToString:METHOD_EDITPROFILE])
        return [self SeparateParamForEditProfile:params];

    if ([method_Name isEqualToString:METHOD_PROFILESTATS])
        return [self SeparateParamForProfileStatsDetails:params];
    
    return nil;
}
+ (id)SeparateParamForProfileStatsDetails:(NSDictionary *)params
{
    Profilemodel *pdetails  = [[Profilemodel alloc]init];
    
    pdetails.status= [params valueForKey:@"Status"];
    pdetails.tempArray = [NSMutableArray new];
    
    
    if ([pdetails.status isEqualToString:@"Success"])
    {
        NSDictionary *qdict= [params valueForKey:@"data"];
        
        pdetails.user_id                    =   [NSString stringWithFormat:@"%@",[qdict   valueForKey:@"user_id"]];
        pdetails.user_name                  =   [NSString stringWithFormat:@"%@",[qdict   valueForKey:@"user_name"]];
        pdetails.hifive_daily               =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"hifive_daily"]intValue]];
        pdetails.hifive_ninety              =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"hifive_ninety"]intValue]];
        pdetails.hifive_all                 =   [NSString stringWithFormat:@"%@",[[qdict   valueForKey:@"hifive_all"]stringValue]];
//        pdetails.hifive_total               =   [NSString stringWithFormat:@"%d",[qdict   valueForKey:@"hifive_total"]int];
        pdetails.hifive_total                =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"hifive_total"]intValue]];

        pdetails.comment_daily              =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"comment_daily"]intValue]];
        pdetails.comment_ninety             =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"comment_ninety"]intValue]];
        pdetails.comment_all                =   [NSString stringWithFormat:@"%@",[[qdict   valueForKey:@"comment_all"]stringValue]];
        pdetails.comment_total              =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"comment_total"]intValue]];
        pdetails.nominate_daily             =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"nominate_daily"]intValue]];
        pdetails.nominate_ninety            =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"nominate_ninety"]intValue]];
        pdetails.nominate_all               =   [NSString stringWithFormat:@"%@",[[qdict   valueForKey:@"nominate_all"]stringValue]];
        pdetails.remint_daily               =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"remint_daily"]intValue]];
        pdetails.remint_ninety              =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"remint_ninety"]intValue]];
        pdetails.remint_all                 =   [NSString stringWithFormat:@"%@",[[qdict   valueForKey:@"remint_all"]stringValue]];
        pdetails.remint_total               =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"remint_total"]intValue]];
        pdetails.share_daily                =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"share_daily"]intValue]];
        pdetails.share_ninety               =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"share_ninety"]intValue]];
        pdetails.share_all                  =   [NSString stringWithFormat:@"%@",[[qdict   valueForKey:@"share_all"]stringValue]];
        pdetails.share_total                =   [NSString stringWithFormat:@"%d",[[qdict   valueForKey:@"share_total"]intValue]];
        
        
        pdetails.status_Msg= @"Success";
        pdetails.status_code=@"1";
        
    }
    else
    {
        pdetails.status_Msg= [params valueForKey:@"error_message"];
        pdetails.status_code=@"0";
        
    }
    return pdetails;
    
}
+ (id)SeparateParamForSearchSuggestion:(NSDictionary *)params
{
    
    NSLog(@"SeparateParamForSearchSugegstion  %@",params);
    
    Generalmodel *gModel  = [[Generalmodel alloc]init];
    
    
    if ([[params valueForKey:@"Status"] isEqualToString:@"Success"])
    {
        
        gModel.searchPeople = [NSDictionary dictionaryWithDictionary:[params valueForKey:@"people"]];
        gModel.serchDefaultWords = [NSDictionary dictionaryWithDictionary:[params valueForKey:@"default_words"]];
        
        gModel.status           =   [params valueForKey:@"Status"];
        gModel.status_code      =   @"1";
        gModel.status_Msg       =   [params valueForKey:@"StatusMsg"];
    }
    else
    {
        gModel.status_code=@"0";
    }
    return gModel;
    
    
}
+ (id)SeparateParamForEditProfile:(NSDictionary *)params
{
    NSLog(@"EditProfile response =%@",params);
    Profilemodel *pmodel  = [[Profilemodel alloc]init];
    
    pmodel.status= [params valueForKey:@"Status"];
    pmodel.tempArray = [NSMutableArray new];
    
    
    if ([pmodel.status isEqualToString:@"Success"])
    {
        pmodel.status_Msg= @"StatusMsg";
        pmodel.status_code=@"1";
        pmodel.userImageUrl = [params valueForKey:@"image_url"];
    }
    else{
        pmodel.status_Msg= [params valueForKey:@"error_message"];
        pmodel.status_code=@"0";
    }
    return pmodel;
    
}


+ (id)SeparateParamForGetActivityUserFeeds:(NSDictionary *)params method:(NSString *)methodName
{
    
    NSLog(@"SeparateParamForGetActivityUserFeeds  %@",params);
    Generalmodel *gModel = [Generalmodel new];
    
    if ([[params valueForKey:@"Status"] isEqualToString:@"Success"]) {
        
        gModel.status         = [params valueForKey:@"Status"];
        gModel.status_code    = @"1";
        gModel.status_Msg     = [params valueForKey:@"success_message"];
        
        NSArray *complimint_list = nil;
        if ([methodName isEqualToString:METHOD_GETACTIVITYMYCIRCLE]) {
            complimint_list  =   [params valueForKey:@"feeds"];
        }
        else  if ([methodName isEqualToString:METHOD_GETACTIVITYYOU]) {
            complimint_list  =   [params valueForKey:@"you_feeds"];
            gModel.request_count              = [params valueForKey:@"request_count"];
            gModel.total_count                = [params valueForKey:@"total_count"];
            gModel.you_count                  = [params valueForKey:@"you_count"];
        }
        else  if ([methodName isEqualToString:METHOD_GETACTIVITYREQUEST]) {
            complimint_list  =   [params valueForKey:@"request_feeds"];
            gModel.request_count              = [params valueForKey:@"request_count"];
            gModel.total_count                = [params valueForKey:@"total_count"];
            gModel.you_count                  = [params valueForKey:@"you_count"];
        }
        
        
        
        gModel.tempArray = [NSMutableArray new];    //  Need to add
        gModel.tempArray2 = [NSMutableArray new];
        gModel.tempArray3 = [NSMutableArray new];    //  Need to add

        for(NSDictionary *complimint in complimint_list)
        {
            //  Need to add
            
            
            if ([methodName isEqualToString:METHOD_GETACTIVITYMYCIRCLE]) {
                [gModel.tempArray addObject:[[ActivityModel alloc]initWithMyCircleActivity:complimint]];
            }
            
           else  if ([methodName isEqualToString:METHOD_GETACTIVITYYOU]) {
                [gModel.tempArray2 addObject:[[ActivityModel alloc]initWithActivity:complimint]];
            }
            else if([methodName isEqualToString:METHOD_GETACTIVITYREQUEST])
            {
                [gModel.tempArray3 addObject:[[RequestActivityModel alloc]initWithRequestActivity:complimint]];
            }
  
       
        }
        
        
    }
    else
    {
        gModel.status_code=@"0";
    }
    return gModel;
}




//==============================================================================
+ (id)SeparateParamForProfileFollowingList:(NSDictionary *)params
{
    NSLog(@"ProfileFollowingList response =%@",params);
    Generalmodel *gmodel  = [[Generalmodel alloc]init];
    
    gmodel.status= [params valueForKey:@"status"];
    gmodel.tempArray = [NSMutableArray new];
    
    
    if ([gmodel.status isEqualToString:@"true"])
    {
        
        NSArray *followinglist = [params  valueForKey:@"followinglist"];
        
        for(NSDictionary *dict in followinglist)
        {
            [gmodel.tempArray addObject:[[Profilemodel alloc]initWithProfileListDict:dict]];
        }
        gmodel.profile_followers_count = [params valueForKey:@"followers_count"];
        gmodel.profile_totalcount = [params valueForKey:@"total_count"];
        gmodel.status_Msg= @"Sucsses";
        gmodel.status_code=@"1";
    }
    else{
        gmodel.status_Msg= [params valueForKey:@"error_message"];
        gmodel.status_code=@"0";
    }
    return gmodel;
    
}



//==============================================================================
+ (id)SeparateParamForProfileFollowersList:(NSDictionary *)params
{
    NSLog(@"ProfileFollowersList response =%@",params);
    Generalmodel *gmodel  = [[Generalmodel alloc]init];
    
    gmodel.status= [params valueForKey:@"status"];
    gmodel.tempArray = [NSMutableArray new];
    
    
    if ([gmodel.status isEqualToString:@"true"])
    {
        
        NSArray *followinglist = [params  valueForKey:@"followerlist"];
        
        for(NSDictionary *dict in followinglist)
        {
            [gmodel.tempArray addObject:[[Profilemodel alloc]initWithProfileListDict:dict]];
        }
        gmodel.profile_followingcount = [params valueForKey:@"following_count"];
        gmodel.profile_totalcount = [params valueForKey:@"total_count"];
        gmodel.status_Msg= @"Sucsses";
        gmodel.status_code=@"1";
    }
    else{
        gmodel.status_Msg= [params valueForKey:@"error_message"];
        gmodel.status_code=@"0";
    }
    return gmodel;
    
}


+ (id)SeparateParamForCoachingTipsUpdate:(NSDictionary *)params
{
    
    NSLog(@"CoachingTipsUpdate  %@",params);
    
    Generalmodel *gModel  = [[Generalmodel alloc]init];
    gModel.status= [params valueForKey:@"Status"];
    
    if ([gModel.status isEqualToString:@"Success"])
    {
        gModel.status           =   [params valueForKey:@"Success"];
        gModel.status_code      =   @"1";
        gModel.status_Msg       =   [params valueForKey:@"StatusMsg"];
    }
    else
    {
        gModel.status_code=@"0";
    }
    return gModel;
    
    
}

+ (id)SeparateParamForDeleteMint:(NSDictionary *)params
{
    
        NSLog(@"SeparateParamForDeleteMint  %@",params);
    
        Generalmodel *gModel  = [[Generalmodel alloc]init];
        gModel.status= [params valueForKey:@"Status"];
    
    if ([gModel.status isEqualToString:@"Success"])
    {
        if (![[params valueForKey:@"mint_count"] integerValue]) {
            SETVALUE(@"no", CEO_ISMINTALREADYPOSTED);
            SYNCHRONISE;
        }
        gModel.status           =   [params valueForKey:@"Status"];
        gModel.status_code      =   @"1";
        gModel.status_Msg       =   [params valueForKey:@"StatusMsg"];
    }
    else
    {
        gModel.status_code=@"0";
    }
    return gModel;
    
    
}

+ (id)SeparateParamForMemberList:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    
    if ([[params valueForKey:@"Status"] isEqualToString:@"Success"]) {
        
        gModel.status         = [params valueForKey:@"Status"];
        gModel.status_code    = @"1";
        gModel.status_Msg     = [params valueForKey:@"success_message"];
        gModel.member_count   = [params valueForKey:@"total_count"];
        gModel.currentpage    = [params valueForKey:@"current_page"];
        gModel.totalpages     = [NSString stringWithFormat:@"%ld",(long)[[params valueForKey:@"max_pages"] integerValue]];
        
        
        NSArray *member_list  = [params valueForKey:@"members_list"];
        NSLog(@"member list %@",params);
        gModel.tempArray2     = [NSMutableArray new];
        
        
        for(NSDictionary *members in member_list)
        {
        
            MemberList *obj = [[MemberList alloc]initWithMembersListDict:members];
            [gModel.tempArray2 addObject:obj];
            obj = nil;
            
        }
        
        
    }
    
    else {
        
        gModel.status =[params valueForKey:@"status"];
        gModel.status_code =@"0";
        gModel.status_Msg =[params valueForKey:@"error_message"];
        
        
    }
    return gModel;
}


+ (id)SeparateParamForGetAllCompliments:(NSDictionary *)params
{
    
    NSLog(@"SeparateParamForGetAllCompliments  %@",params);

    Generalmodel *gModel  = [[Generalmodel alloc]init];
    gModel.status= [params valueForKey:@"Status"];
    
    if ([gModel.status isEqualToString:@"Success"])
    {
        gModel.status           =   [params valueForKey:@"Status"];
        gModel.status_code      =   @"1";
        gModel.status_Msg       =   [params valueForKey:@"StatusMsg"];
        NSArray *complimint_list  =   [params valueForKey:@"comment_list"];
        
        gModel.tempArray = [NSMutableArray new];
        
        for(NSDictionary *complimint in complimint_list)
        {
            [gModel.tempArray addObject:[[CommentModel alloc]initWithComplimint:complimint]];
        }
    }
    else
    {
        gModel.status_code=@"0";
    }
    return gModel;
    
    
}
+ (id)SeparateParamFormintRemint:(NSDictionary *)params
{
    
    NSLog(@"SeparateParamFormintRemint  %@",params);
    
    Generalmodel *gModel  = [[Generalmodel alloc]init];
    gModel.status= [params valueForKey:@"status"];
    
    if (gModel.status)
    {
        gModel.status           =   [params valueForKey:@"status"];
        gModel.status_code      =   @"1";
        gModel.status_Msg       =   [params valueForKey:@"success_message"];
       
    
    }
    else
    {
        gModel.status_code=@"0";
    }
    return gModel;
    
    
}

+ (id)seperateParamMyshowMintDetais:(NSDictionary *)params
{
    
    Generalmodel *gModel  = [[Generalmodel alloc]init];
    gModel.status= [params valueForKey:@"Status"];
    
    if ([gModel.status isEqualToString:@"Success"])
    {
        gModel.status_code = @"1";
        gModel.status_Msg =[params valueForKey:@"StatusMsg"];
        NSArray *mint_list = [params valueForKey:@"list"];
        
  
        [[SPHSingletonClass sharedSingletonClass] setLogin_user_streak:[[params valueForKey:@"login_user_streak"] integerValue]];
        [[SPHSingletonClass sharedSingletonClass]
         setIsUser_mint_streak:([[params valueForKey:@"is_mint_streak"] isEqualToString:@"yes"])?YES:NO];

        
        NSLog(@"Mintdetila MInt list %@",mint_list);
        gModel.tempArray = [NSMutableArray new];
        
        for(NSDictionary *showRoom in mint_list)
        {
            [gModel.tempArray addObject:[[BaseFeedModel alloc]initWithMintDetails:showRoom ]];
        }
        
       
        
    }
    else
    {
        gModel.status_code=@"0";
        //        list_param.status_Msg= [params valueForKey:@"error_message"];
    }
    return gModel;
}
+ (id)seperateParamSingleMintDetails:(NSDictionary *)params
{
    Generalmodel *gModel  = [[Generalmodel alloc]init];
    gModel.status= [params valueForKey:@"Status"];
    
    if ([gModel.status isEqualToString:@"Success"])
    {
        gModel.status =[params valueForKey:@"Status"];
        gModel.status_code =@"1";
        gModel.status_Msg =[params valueForKey:@"StatusMsg"];
    
        
        if ([[params valueForKey:@"feed"] count])
        {
        NSLog(@"showroom list %@",[params valueForKey:@"feed"]);
        gModel.tempArray = [NSMutableArray new];
        [gModel.tempArray addObject:[[BaseFeedModel alloc]initWithMintDetails:[params valueForKey:@"feed"]]];
        }
    }
    else
    {
        gModel.status_code=@"0";
        //        list_param.status_Msg= [params valueForKey:@"error_message"];
    }
    return gModel;
}
+ (id)seperateParamForMuteMInt:(NSDictionary *)params
{
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    list_param.status= ([[params valueForKey:@"status"] boolValue]) ? @"true" : @"false";
    if ([list_param.status isEqualToString:@"true"])
    {
        list_param.status_code=@"1";
//        list_param.status_Msg= [params valueForKey:@"msg"];
    }
    else
    {
        list_param.status_code=@"0";
//        list_param.status_Msg= [params valueForKey:@"error_message"];
    }
    return list_param;
}
+ (id)seperateParamForMuteUser:(NSDictionary *)params
{
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    list_param.status= ([[params valueForKey:@"status"] integerValue]) ? @"true" : @"false";

    if ([list_param.status isEqualToString:@"true"])
    {
        list_param.status_code=@"1";
        list_param.status_Msg= [params valueForKey:@"success_message"];
    }
    else
    {
        list_param.status_code=@"0";
        list_param.status_Msg= [params valueForKey:@"error_message"];
    }
    return list_param;
}

+ (id)seperateParamFollowunFollow:(NSDictionary *)params
{
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    list_param.status= [params valueForKey:@"status"];
    
    if ([list_param.status isEqualToString:@"true"])
    {
        list_param.status_code=@"1";
        list_param.status_Msg= [params valueForKey:@"success_message"];
    }
    else
    {
        list_param.status_code=@"0";
        list_param.status_Msg= [params valueForKey:@"error_message"];
    }
    return list_param;
}

+ (id)seperateParamGetUserProfile:(NSDictionary *)params
{
    Profilemodel *list_param  = [[Profilemodel alloc]init];
    list_param.status         = [params valueForKey:@"Status"];
    
    if ([list_param.status isEqualToString:@"Success"])
    {
        list_param.status_code              =  @"1";
        list_param.status_Msg               =  [params valueForKey:@"StatusMsg"];
        list_param.user_name                =  [params valueForKey:@"user_name"];
        list_param.user_first_name          =  [YSSupport getVal:[params valueForKey:@"first_name"]];
        list_param.user_last_name           =  [YSSupport getVal:[params valueForKey:@"last_name"]];
        list_param.user_id                  =  [params valueForKey:@"user_id"];
        list_param.achieving_text           =  [YSSupport getVal:[params valueForKey:@"achieving_text"]];
        list_param.level                    =  [params valueForKey:@"level"];
        list_param.profile_mint_count       =  [NSString stringWithFormat:@"%@",[params valueForKey:@"profile_mint_count"]];
        list_param.profile_showroom_count   =  [NSString stringWithFormat:@"%@",[params valueForKey:@"profile_showroom_count"]];
       
        list_param.profile_circle_count     =  [NSString stringWithFormat:@"%@",[params valueForKey:@"profile_circle_count"]];
        list_param.profile_followers_count  =  [NSString stringWithFormat:@"%@",[params valueForKey:@"profile_followers_count"]];
        list_param.streak_days            =  [params valueForKey:@"streak_days"];

        list_param.profile_type             =  [params valueForKey:@"profile_type"];
        list_param.profile_url              =  [params valueForKey:@"profile_url"];
        list_param.user_location            =  [params valueForKey:@"location"];
        list_param.user_thumb_image         =  [params valueForKey:@"user_thumb_image_large"];
        list_param.website                  =  [params valueForKey:@"website"];
        list_param.follow_status            =  [params valueForKey:@"followstatus"];
        list_param.profile_showroom_created =  [NSString stringWithFormat:@"%@",[params valueForKey:@"profile_showroom_created"]];
        list_param.profile_showroom_joined  = [NSString stringWithFormat:@"%@", [params valueForKey:@"profile_showroom_joined"]];
    }
    else
    {
        list_param.status_code=@"0";
        list_param.status_Msg= [params valueForKey:@"StatusMsg"];
    }
    return list_param;
}

// Mint Detail

+ (id)seperateParamMintDetail:(NSDictionary *)params
{
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    list_param.status= [params valueForKey:@"status"];
    if ([list_param.status isEqualToString:@"true"])
    {
        list_param.status_code=@"1";
        list_param.status_Msg= [params valueForKey:@"Success"];
        
    }
    else
    {
        list_param.status_Msg= [params valueForKey:@"error_message"];
        list_param.status_code=@"0";
    }
    return list_param;

}


//==============================================================================
+ (id)SeparateParamForhifivemint:(NSDictionary *)params
{
    NSLog(@"CEO hifive Mints response =%@",params);
    
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    list_param.status= [params valueForKey:@"status"];
    if ([list_param.status isEqualToString:@"true"])
    {
        list_param.status_code=@"1";
        list_param.status_Msg= [params valueForKey:@"Success"];
        
    }
    else
    {
        list_param.status_Msg= [params valueForKey:@"error_message"];
        list_param.status_code=@"0";
    }
    return list_param;
    
}
//==============================================================================
+ (id)SeparateParamForAddShowroom:(NSDictionary *)params
{
    NSLog(@"FlagMInt response =%@",params);
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    
    
    
    if ([[params valueForKey:@"StatusCode"] boolValue] && [[params valueForKey:@"Status"] isEqualToString:@"Success"])
    {
        list_param.mintdetail_url = [params valueForKey:@"showroom_img"];
        list_param.status_Msg     = @"Sucsses";
        list_param.status_code    = @"1";
    }
    else{
        list_param.status_Msg= [params valueForKey:@"error_message"];
        list_param.status_code=@"0";
    }
    return list_param;
    
}
//==============================================================================
+ (id)SeparateParamForFlagMint:(NSDictionary *)params
{
    NSLog(@"FlagMInt response =%@",params);
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    
    list_param.status= [params valueForKey:@"Status"];

    
    if ([[NSString stringWithFormat:@"%@",[params objectForKey:@"Status"]] isEqualToString:@"Success"])
    {
        
        list_param.status_Msg= @"Sucsses";
        list_param.status_code=@"1";
    }
    else{
        list_param.status_Msg= [params valueForKey:@"error_message"];
        list_param.status_code=@"0";
    }
    return list_param;
    
}
+ (id)SeparateParamForGetShowroomDetails:(NSDictionary *)params
{
    NSLog(@" GetShowRoomDetails =%@",params);
    Generalmodel *gModel  = [Generalmodel new];
    gModel.status= [params valueForKey:@"status"];
    if ([gModel.status isEqualToString:@"true"])
    {
        gModel.status           =   [params valueForKey:@"status"];
        gModel.status_code      =   @"1";
        gModel.status_Msg       =   [params valueForKey:@"success_message"];
        gModel.temprecord       =   [params valueForKey:@"total_showroom_records"];
        gModel.pull_request_time =  [params valueForKey:@"pull_request_time"];
        NSArray *showroom_list  =   [params valueForKey:@"feeds"];
        gModel.tempArray        =   [NSMutableArray new];
        
        for(NSDictionary *showRoom in showroom_list)
        {
            [gModel.tempArray addObject:[[BaseFeedModel alloc]initWithMintDetails:showRoom ]];
            // changed Here from Showroom Dict to mint details
        }
    }
    else
    {
        gModel.status_Msg= [params valueForKey:@"error_message"];
        gModel.status_code=@"0";
    }
    //error_message
    
    
    return gModel;
    
}

//==============================================================================
+ (id)SeparateParamForFollowerList:(NSDictionary *)params
{
    NSLog(@"FlagMInt response =%@",params);
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    
    list_param.status= [params valueForKey:@"status"];
    list_param.tempArray = [NSMutableArray new];

    
    if ([list_param.status isEqualToString:@"true"])
    {
        
        NSArray *showroom_list = [params  valueForKey:@"followinglist"];
        
        for(NSDictionary *dict in showroom_list)
        {
            
            FriendsInfo *obj = [[FriendsInfo alloc]initWithFriendsInfo:[dict valueForKey:@"name"] screenName:[dict valueForKey:@"user_name"] profileImage:[dict valueForKey:@"user_thumb_image"] accountId:[[dict valueForKey:@"user_id"] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]];
            //                     [arrayFriendList addObject:obj];
            
            
            [list_param.tempArray addObject:obj];
            
        }

        list_param.status_Msg= @"Sucsses";
        list_param.status_code=@"1";
    }
    else{
        list_param.status_Msg= [params valueForKey:@"error_message"];
        list_param.status_code=@"0";
    }
    return list_param;
    
}


//==============================================================================
+ (id)SeparateParamForCompliMint:(NSDictionary *)params
{
    NSLog(@"CEO CompliMints response =%@",params);
    CommentModel *newmodel =[CommentModel new];
    newmodel.status= [params valueForKey:@"status"];
    
    if ([[NSString stringWithFormat:@"%@",[params objectForKey:@"status"]] isEqualToString:@"true"])
    {
        
        newmodel.achieveCommentDate      =  ((NSNull *)[params valueForKey:@"comment_time"] != nil) ?[NSString stringWithFormat:@"%@",[params valueForKey:@"comment_time"]]:[NSString stringWithFormat:@"%@",[params valueForKey:@"comment_date"]];;
        newmodel.achieveCommentId        = [NSString stringWithFormat:@"%@",[params valueForKey:@"comment_id"]];
        newmodel.achieveCommentText      = [params valueForKey:@"cmt"];
        newmodel.achieveCommentUserId    = GETVALUE(CEO_UserId);
        newmodel.achieveCommentUname     = [NSString stringWithFormat:@"%@ %@",GETVALUE(CEO_UserFirstName),GETVALUE(CEO_UserLastName)];
        newmodel.achieveCommentUserImage = GETVALUE(CEO_UserImage);
        newmodel.status_code =  @"1";
        [newmodel  setCBuckNotifyText:[NSString stringWithFormat:@"%@",[params valueForKey:@"notify_text"]]];
        [newmodel setCBuckNotifyVal:[NSString stringWithFormat:@"%@",[params valueForKey:@"notify_val"]]];
        newmodel.postingStatus=YES;
        
    }
    else{
        newmodel.status_Msg= [params valueForKey:@"error_message"];
        newmodel.status_code=@"0";
    }
    return newmodel;
}
+ (id)SeparateParamForSingleMint:(NSDictionary *)params
{
    // NSLog(@"CEO SeparateParamForOfficeFeedList  =%@",params);
    
    
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    list_param.status         = [params valueForKey:@"status"];
    
    if ([list_param.status isEqualToString:@"true"]||[list_param.status isEqualToString:@"True"])
    {
        list_param.status_Msg=@"Success";
        list_param.status_code=@"1";
        
        NSMutableArray *tempArray= [[NSMutableArray alloc]init];
        NSDictionary *qdict= [params valueForKey:@"mint"];
        
        
        if ([qdict count])
        {
            Feedmodel *myFeed= [Feedmodel new];
            
            // MOFDIFIED PARAMS
            myFeed.is_user_inspired           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"has_user_inspired"]];
            myFeed.is_user_liked              = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"has_user_hifived"]];
            myFeed.feed_inspiration_number    = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"inspiration_number"]];
            myFeed.feed_like_count            = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_highfive_number"]];
            myFeed.feed_category_type       = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"category_name"]];
            
            
            
            
            myFeed.feed_comment_count         = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_comment_number"]];
            myFeed.feed_id                   = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_id"]];
            myFeed.feed_posted_time           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_date_posted"]];
            myFeed.feed_text                 = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_text"]];
            myFeed.feed_thumb_image           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_thumb_image"]];
            myFeed.feed_title                = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_title"]];
            myFeed.feed_type                 = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_type"]];
            myFeed.isMintReminted           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"IsMintReminted"]];
            
            myFeed.userId                   = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_id"]];
            myFeed.user_name                 = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_name"]];
            
            myFeed.user_thumb_image             = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_thumb_image"]];
            //myFeed.feedTypeVideo            = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_type"]];
            myFeed.achievementType          = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_type"]];
            myFeed.media_id             = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_id"]];
            myFeed.youtube_id                = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"is_youtube_id"]];
            
            myFeed.feed_category_type       =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_mint_type"]];
            myFeed.feed_posted_time         =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_posted_time"]];
            myFeed.feed_posted_location     =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_posted_location"]];
            
            
            
            myFeed.next_mint_id         =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"next_mint_id"]];
            myFeed.next_mint_type     =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"next_mint_type"]];
            myFeed.prev_mint_id           =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"prev_mint_id"]];
            myFeed.prev_mint_type       = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"prev_mint_type"]];
            
            
            
            
           id CommentsList = [params valueForKey:@"comment"];
            NSLog(@"json data=%@",CommentsList);
            myFeed.CommentsArray = ([CommentsList isKindOfClass:[NSArray class]])?[self separateCommentsFromData:CommentsList]:[[NSMutableArray alloc] init];
            
            id feed_hifive_usersList = [qdict valueForKey:@"feed_hifive_users"];
            NSLog(@"feed_hifive_usersList data=%@",feed_hifive_usersList);
            
  
            
            [tempArray addObject:myFeed];
        }
        list_param.tempArray= [tempArray mutableCopy];
        
    }
    else
    {
        list_param.status_Msg= [params valueForKey:@"error_message"];
        list_param.status_code=@"0";
    }
    return list_param;
}

+(NSMutableArray*)separateHifiveUsersFromData:(NSArray*)hifiVeusers
{
    NSLog(@"CEO separateHifiveUsersFromData  =%@",hifiVeusers);
    
    NSMutableArray *hifiveUsersArray= [[NSMutableArray alloc]init];
    
    for (NSDictionary *cmt_Dict in hifiVeusers)
    {
        CommentModel *cmtmdl= [CommentModel new];
        cmtmdl.achieveCommentId         = [NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"profile_id"]];
        
        [hifiveUsersArray addObject:cmtmdl];
    }
    
    
    return hifiveUsersArray;
}
+(NSMutableArray*)separateCommentsFromData:(NSArray*)comments
{
    NSLog(@"CEO separateCommentsFromData =%@",comments);
    
    NSMutableArray *commentsArray= [[NSMutableArray alloc]init];
    for (NSDictionary *cmt_Dict in comments)
    {
        CommentModel *cmtmdl= [CommentModel new];
        cmtmdl.achieveCommentDate       =  ((NSNull *)[cmt_Dict valueForKey:@"comment_time"] != nil) ?[NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"comment_time"]]:[NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"comment_date"]];
        cmtmdl.achieveCommentId         = [NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"comment_id"]];
        cmtmdl.achieveCommentText       = [NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"comment_text"]];
        cmtmdl.achieveCommentUname      = [NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"user_name"]];
        cmtmdl.achieveCommentUserId     = [NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"user_id"]];
        cmtmdl.achieveCommentUserImage  = [NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"user_thumb_image"]];
        cmtmdl.achieveCommentUserLink   = [NSString stringWithFormat:@"%@",[cmt_Dict valueForKey:@"user_link"]];
        cmtmdl.postingStatus=YES;
        [commentsArray addObject:cmtmdl];
    }
    
    return commentsArray;
}

+ (id)SeparateParamGet_single_achievements:(NSDictionary *)params
{
    
    
    // NSLog(@"CEO SeparateParamForOfficeFeedList  =%@",params);
    
    Generalmodel *list_param  = [[Generalmodel alloc]init];
    
    list_param.status= [params valueForKey:@"status"];
    if ([list_param.status isEqualToString:@"true"])
    {
        list_param.status_Msg=@"Success";
        list_param.status_code=@"1";
        
        NSMutableArray *tempArray= [[NSMutableArray alloc]init];
        NSDictionary *qdict= [params valueForKey:@"achievement"];
        
        if ([qdict count]) {
            
            Feedmodel *myFeed= [Feedmodel new];
            
            // MOFDIFIED PARAMS
            
            // myFeed.user_gender              = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_gender"]];
            //myFeed.isUserNominated          = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"has_user_nominated"]];
            myFeed.is_user_inspired           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"has_user_inspired"]];
            myFeed.is_user_liked              = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"is_user_hifive"]];
            myFeed.feed_inspiration_number    = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"inspiration_number"]];
            myFeed.feed_like_count            = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_highfive_number"]];
            //myFeed.fee     = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_nominations_number"]];
            // myFeed.feed_name                = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_name"]];
            // myFeed.spy_data_id              = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"spy_data_id"]];
            //myFeed.user_profile_link        = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_profile_link"]];
            //myFeed.user_join_link           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_join_link"]];
            // myFeed.sender_pronoun           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"sender_pronoun"]];
            // myFeed.feed_category_id         = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"category_id"]];
            myFeed.feed_category_type       = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"category_name"]];
            
            
            
            
            myFeed.feed_comment_count         = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_comment_number"]];
            myFeed.feed_id                   = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_id"]];
            myFeed.feed_posted_time           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_date_posted"]];
            myFeed.feed_text                 = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_text"]];
            myFeed.feed_thumb_image           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_thumb_image"]];
            myFeed.feed_title                = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_text"]];
            myFeed.feed_type                 = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_type"]];
            //            myFeed.mint_earned_cbucks       = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_earned_cbucks"]];
            myFeed.isMintReminted           = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"IsMintReminted"]];
            
            myFeed.userId                   = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_id"]];
            myFeed.user_name                 = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_name"]];
            
            myFeed.user_thumb_image             = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"user_thumb_image"]];
            //myFeed.feedTypeVideo            = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"achievement_type"]];
            myFeed.achievementType          = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"type_name"]];
            myFeed.media_id             = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"media_id"]];
            myFeed.youtube_id                = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"is_youtube_id"]];
            
            myFeed.feed_category_type       =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_mint_type"]];
            myFeed.feed_posted_time         =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_posted_time"]];
            myFeed.feed_posted_location     =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"feed_posted_location"]];
            // myFeed.feed_type_name           =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"type_name"]];
            
            
            
            myFeed.next_mint_id         =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"next_mint_id"]];
            myFeed.next_mint_type     =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"next_mint_type"]];
            myFeed.prev_mint_id           =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"prev_mint_id"]];
            myFeed.prev_mint_type       = [NSString stringWithFormat:@"%@",[qdict valueForKey:@"prev_mint_type"]];
            
            if (![[qdict valueForKey:@"IsMintReminted"] isEqualToString:@"NO"])
            {
               // myFeed.remintDetails = [qdict valueForKey:@"remintDetails"];
            }
            
            //            if ([[qdict valueForKey:@"type_name"] isEqualToString:@"CustomMint"])
            //                myFeed.feed_type_name           =[NSString stringWithFormat:@"%@",[qdict valueForKey:@"mint_name"]];
            
            id CommentsList = [params  valueForKey:@"comment"];
            NSLog(@"json data=%@",CommentsList);
            myFeed.CommentsArray = ([CommentsList isKindOfClass:[NSArray class]])?[self separateCommentsFromData:CommentsList]:[[NSMutableArray alloc] init];
            
            //            id feed_hifive_usersList = [params valueForKey:@"feed_hifive_users"];
            //            NSLog(@"feed_hifive_usersList data=%@",feed_hifive_usersList);
            
            //   myFeed.hifiveUsersArray = ([feed_hifive_usersList isKindOfClass:[NSArray class]])?[self separateHifiveUsersFromData:feed_hifive_usersList]:[[NSMutableArray alloc] init];
            
            [tempArray addObject:myFeed];
        }
        list_param.tempArray= [tempArray mutableCopy];
        
    }
    else
    {
        list_param.status_Msg= [params valueForKey:@"error_message"];
        list_param.status_code=@"0";
    }
    return list_param;
    
}
+ (id)SeparateParamForParticularShowroom:(NSDictionary *)params
{
    NSLog(@" SeparateParamForParticularShowroom =%@",params);
    Generalmodel *gModel  = [Generalmodel new];
    
    gModel.status= [params valueForKey:@"Status"];

    if ([gModel.status isEqualToString:@"Success"])
    {
        gModel.status           =   [params valueForKey:@"Status"];
        gModel.status_Msg       =   [params valueForKey:@"StatusMsg"];
        
        if ([params valueForKey:@"feeds"]) {
            gModel.status_code      =   @"1";
            NSArray *recent_mints  =   [[params valueForKeyPath:@"feeds"] allObjects];
            gModel.tempArray        =   [NSMutableArray new];
            
            for(NSDictionary *mints in recent_mints)
            {
                [gModel.tempArray addObject:[[BaseFeedModel alloc]initWithMintDetails:mints ]];
            }

        }
        else if ([params valueForKey:@"related_showroom_list"]) {
            NSArray *recent_mints  =   [[params valueForKeyPath:@"related_showroom_list"] allObjects];
            gModel.tempArray        =   [NSMutableArray new];
            gModel.status_code      =   @"2";
            gModel.totalpages       =   [params valueForKeyPath:@"total_showroom_records"];
            for(NSDictionary *mints in recent_mints)
            {
                [gModel.tempArray addObject:[[ShowRoomModel alloc]initWithShowRoomDict:mints]];
            }
        }
    }
    else
    {
        gModel.status_Msg= [params valueForKey:@"error_message"];
        gModel.status_code=@"0";
    }
    //error_message
    

    return gModel;
    
}
+ (id)SeparateParamForHashTagMints:(NSDictionary *)params
{
    NSLog(@" SeparateParamForHashTagMints =%@",params);
    Generalmodel *gModel  = [Generalmodel new];
    
    gModel.status= [params valueForKey:@"Status"];
    if ([gModel.status isEqualToString:@"Success"])
    {
        gModel.status           =   [params valueForKey:@"Status"];
        gModel.status_code      =   @"1";
        gModel.status_Msg       =   [params valueForKey:@"StatusMsg"];
        gModel.temprecord       =   [params valueForKey:@"total_showroom_records"];
        gModel.pull_request_time =  [params valueForKey:@"pull_request_time"];
        NSArray *recent_mints  =   [params valueForKey:@"recent_mints"];
        NSArray *top_mints  =   [params valueForKey:@"top_mints"];

        gModel.tempArray        =   [NSMutableArray new];
        
    
        for(NSDictionary *mints in top_mints)
        {
            [gModel.tempArray addObject:[[BaseFeedModel alloc]initWithMintDetails:mints ]];
        }
        for(NSDictionary *mints in recent_mints)
        {
            [gModel.tempArray addObject:[[BaseFeedModel alloc]initWithMintDetails:mints ]];
        }
        
        
        
    }
    else
    {
        gModel.status_Msg= [params valueForKey:@"error_message"];
        gModel.status_code=@"0";
    }
    //error_message
    
    
    return gModel;
    
}
+ (id)SeparateParamForMyshowMints:(NSDictionary *)params
{
    NSLog(@" SeparateParamForMyshowMints =%@",params);
    Generalmodel *gModel  = [Generalmodel new];
    
    if ([[params valueForKey:@"Status"] isEqualToString:@"Success"])
    {
        gModel.status           =   [params valueForKey:@"Status"];
        gModel.status_code      =   @"1";
        gModel.status_Msg       =   [params valueForKey:@"StatusMsg"];
        gModel.temprecord       =   [params valueForKey:@"total_showroom_records"];
        gModel.total_count      =   [params valueForKey:@"total_count"];
        gModel.pull_request_time =  [params valueForKey:@"pull_request_time"];
        
        
        NSArray *showroom_list  =   ( [params valueForKey:@"trending_mint_list"])?[params valueForKey:@"trending_mint_list"]:[params valueForKey:@"feeds"];
        gModel.tempArray        =   [NSMutableArray new];
       
        
        [[SPHSingletonClass sharedSingletonClass] setLogin_user_streak:[[params valueForKey:@"login_user_streak"] integerValue]];
        [[SPHSingletonClass sharedSingletonClass]
         setIsUser_mint_streak:([[params valueForKey:@"is_mint_streak"] isEqualToString:@"yes"])?YES:NO];
        
        
        for(NSDictionary *showRoom in showroom_list)
        {
            [gModel.tempArray addObject:[[BaseFeedModel alloc]initWithMintDetails:showRoom]];
        }
  
    }
    else
    {
        gModel.status_Msg= [params valueForKey:@"StatusMsg"];
        gModel.status_code=@"0";
    }
    //error_message
    
    
    return gModel;
    
}
+ (id)SeparateParamForHashTag:(NSDictionary *)json
{   NSLog(@" Login SeparateParamForUserLogin =%@",json);
    Generalmodel *list_param  = [Generalmodel new];
    
    list_param.status= [json valueForKey:@"status"];
    if ([list_param.status isEqualToString:@"true"])
    {
        list_param.status_Msg  = @"Login Success";
        list_param.status_code = @"1";
        list_param.accesToken  = [json valueForKey:@"access token"];
        
    }
    else
    {
        list_param.status_Msg= [json valueForKey:@"error_message"];
        list_param.status_code=@"0";
    }
    //error_message
    
    
    return list_param;
}
+ (id)SeparateParamForUserLogin:(NSDictionary *)json
{
    NSLog(@" Login SeparateParamForUserLogin =%@",json);
    
    Userfeed *list_param  = [[Userfeed alloc]init];
    list_param.status      = [json valueForKey:@"Status"];
    list_param.status_Msg  = [json valueForKey:@"StatusMsg"];
    list_param.status_code = [json valueForKey:@"StatusCode"];

    if ([list_param.status isEqualToString:@"Success"])
    {
        list_param.AccessToken      = [json valueForKey:@"access_token"];
        list_param.assistantid      = [json valueForKey:@"assistantid"];
        list_param.assistantname    = [json valueForKey:@"assistantname"];
        list_param.gender           = [json valueForKey:@"gender"];
        list_param.stage            = [json valueForKey:@"stage"];
        list_param.User_id          = [json valueForKey:@"user_id"];
        list_param.userName         = [json valueForKey:@"user_name"];
        list_param.user_image_url   = [json valueForKey:@"user_image_url"];
        list_param.first_name       = [json valueForKey:@"first_name"];
        list_param.last_name        = [json valueForKey:@"last_name"];
        list_param.is_Account_deactivated        = [json valueForKey:@"is_deactivated"];

        list_param.Referral_id      = [json valueForKey:@"referralid"];
        list_param.powerMember      = [json valueForKey:@"power_member"];
        list_param.followerCount    = [json valueForKey:@"follower_list_count"];
        list_param.followingCount   = [json valueForKey:@"following_list_count"];
        list_param.notify_val       = [json valueForKey:@"notify_val"];
        list_param.notify_text      = [json valueForKey:@"notify_text"];
        list_param.discover_all_page_first_time_visit   = [json valueForKey:@"discover_all_page_first_time_visit"];
        list_param.showroom_page_first_time_visit       = [json valueForKey:@"showroom_page_first_time_visit"];
        list_param.is_already_posted_mint               = [json valueForKey:@"is_already_posted_mint"];
        list_param.mint_detail_page_first_time_visit    = [json valueForKey:@"mint_detail_page_first_time_visit"];
        list_param.is_joined_showrooms                  = [json valueForKey:@"is_joined_showrooms"];
        list_param.addmint_culture_first_time_visit                  = [json valueForKey:@"addmint_culture_first_time_visit"];

        list_param.notify_val                           =  [NSString stringWithFormat:@"%d",[[json valueForKey:@"total_count"] intValue]];

    }
    else
    {
        list_param.status_Msg= [json valueForKey:@"StatusMsg"];
    }
    //error_message
    
    return list_param;
    
    
    
  
}

+ (id)SeparateParamForDiscoverAllTab:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =[params valueForKey:@"Status"];
    gModel.status_Msg =[params valueForKey:@"StatusMsg"];
    
    
    
    if ([[params valueForKey:@"Status"] isEqualToString:@"Success"]) {
            gModel.status_code =@"1";

        
        [[SPHSingletonClass sharedSingletonClass] setLogin_user_streak:[[params valueForKey:@"login_user_streak"] integerValue]];
        [[SPHSingletonClass sharedSingletonClass]
         setIsUser_mint_streak:([[params valueForKey:@"is_mint_streak"] isEqualToString:@"yes"])?YES:NO];
        
        NSArray *trending_showroom_list = [params valueForKey:@"trending_showroom_list"];
        NSArray *trending_showTag_list = [params valueForKey:@"trending_tags_list"];
        NSArray *trending_Mint_list = [params valueForKey:@"trending_mint_list"];

            gModel.tempArray = [NSMutableArray new];
            gModel.tempArray2 = [NSMutableArray new];
            gModel.tempArray3 = [NSMutableArray new];

        for(NSDictionary *showRoom in trending_showroom_list)
        {
            [gModel.tempArray addObject:[[DiscoverDataModel alloc]initWithDiscoverInfo:showRoom ForTrendingShowroom:YES]];
        }
        
        for(NSDictionary *showRoomTag in trending_showTag_list)
        {
            [gModel.tempArray2 addObject:[[DiscoverDataModel alloc]initWithDiscoverInfo:showRoomTag ForTrendingShowroom:NO]];
        }
        for(NSDictionary *mint in trending_Mint_list)
        {
            [gModel.tempArray3 addObject:[[BaseFeedModel alloc]initWithMintDetails:mint]];
        }
    
    }
    
    else
    {
        gModel.status_code = @"0";

    }
    return gModel;
}

+ (id)SeparateParamForMyshowShowroom:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    
    if ([[params valueForKey:@"Status"] isEqualToString:@"Success"]) {
  
    gModel.status =[params valueForKey:@"Status"];
    gModel.status_code =@"1";
    gModel.status_Msg =[params valueForKey:@"StatusMsg"];
    gModel.temprecord =[params valueForKey:@"total_showrooms"];
    NSArray *showroom_list = [params  valueForKey:@"showroom_list"];
    gModel.tempArray = [NSMutableArray new];
    
    for(NSDictionary *showRoom in showroom_list)
    {
        [gModel.tempArray addObject:[[ShowRoomModel alloc]initWithShowRoomDict:showRoom ]];
        
    }
        
    }
    
    else
    {
    
        gModel.status = [params valueForKey:@"status"];
        gModel.status_code = @"0";
        gModel.status_Msg = @"No record Found";
    
    
    }
    return gModel;
}
+ (id)SeparateParamForAddShowrrom:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];

    if ([[params valueForKey:@"status"] isEqualToString:@"true"]) {
   
    gModel.status         = [params valueForKey:@"status"];
    gModel.status_code    = @"1";
    gModel.achievement_id = [params valueForKey:@"achievement_id"];
    gModel.status_Msg     = [params valueForKey:@"success_message"];
    gModel.mintdetail_url     = [params valueForKey:@"mintdetail_url"];

    
    }
    
    else {
    
        gModel.status =[params valueForKey:@"status"];
        gModel.status_code =@"0";
        gModel.status_Msg =[params valueForKey:@"error_message"];
    
    
    }
    return gModel;
}
+ (id)SeparateParamForEditMint:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    
    if ([[params valueForKey:@"status"] isEqualToString:@"true"]) {
        
        gModel.status         = [params valueForKey:@"status"];
        gModel.status_code    = @"1";
        gModel.status_Msg     = [params valueForKey:@"success_message"];
        
    }
    
    else {
        
        gModel.status =[params valueForKey:@"status"];
        gModel.status_code =@"0";
        gModel.status_Msg =[params valueForKey:@"error_message"];
        
        
    }
    return gModel;
}


//list
+ (id)SeparateParamForComplimintUserList:(NSDictionary *)params
{
   
    return nil;
}


@end
