//
//  SPHServiceRequest.m
//  Exam_View
//  Created by Heart on 31/05/14.
//  Copyright (c) 2014 BSEtec. All rights reserved.
//

//==============================================================================
///////////    THIS CLASS CREATES ALL REQUESTS        //////////////////////////
//==============================================================================


#import "SPHServiceRequest.h"
#import "SPHCreateUrl.h"
#import "SPHSeparteParams.h"
#import "NSDictionary+ReplaceNull.h"

@implementation SPHServiceRequest

#pragma single parameters

//==============================================================================
///////////    BELOW CODES FOR DIFFERENT REQUEST CREATION  /////////////////////
//==============================================================================


- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    
    if (!responseData ) {
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"Status",@"-1",@"StatusCode",@"Server issue",@"result", nil];
        return dictionary;
    }
    id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
    if ([object isKindOfClass:[NSDictionary class]]){
        dictionary = (NSDictionary*)object;
    } else {
        dictionary = (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
    }
    return [dictionary dictionaryByReplacingNullsWithStrings];
}

-(void)cancelReq:(SPHServiceRequest *)serviceRequest{
    [serviceRequest.someConnection cancel];
}



- (void)getCancelableServerResponseForparam:(NSDictionary *)params mathod:(NSString*)method withSuccessionBlock:(void(^)(id response))successBlock
                        andFailureBlock:(void(^)(NSError *error))failureBlock;
{
    NSLog(@"%@",[NSURL URLWithString:[SPHCreateUrl SerializeURL:params methodName:method]]);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[SPHCreateUrl SerializeURL:params methodName:method]]];

    self.someConnection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    [self.someConnection start];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
}
- (void)getBlockServerResponseForString:(NSString *)params mathod:(NSString*)method
                    withSuccessionBlock:(void(^)(id response))successBlock
                        andFailureBlock:(void(^)(NSError *error))failureBlock
{
    NSLog(@"%@",[NSURL URLWithString:[SPHCreateUrl SerializeURLWithSingleParam:params methodName:method]]);
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[SPHCreateUrl SerializeURLWithSingleParam:params methodName:method]]];
     [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];
}

#pragma Multiple Parameters

- (void)getBlockServerResponseForparam:(NSDictionary *)params mathod:(NSString*)method withSuccessionBlock:(void(^)(id response))successBlock
                       andFailureBlock:(void(^)(NSError *error))failureBlock

{
    NSURL *myURL = [NSURL URLWithString:[SPHCreateUrl SerializeURL:params methodName:method]];
    NSLog(@"paramsssss =%@",params);
    NSLog(@"My Url =%@",myURL);
   
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:myURL];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];

}
- (void)postBlockServerResponseForData :(NSData *)somedata
                              paramDict:(NSDictionary*)params
                                 mathod:(NSString*)method
                    withSuccessionBlock:(void(^)(id response))successBlock
                        andFailureBlock:(void(^)(NSError *error))failureBlock
{
    NSURL *myURL = [NSURL URLWithString:[SPHCreateUrl SerializeURL:params methodName:method]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myURL];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:somedata];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];
}
- (void)PostRequestWithUrlAndData:(NSData *)somedata
                        paramDict:(NSDictionary*)params
                           mathod:(NSString*)method
                             link:(NSURL*)url
              withSuccessionBlock:(void(^)(id response))successBlock
                  andFailureBlock:(void(^)(NSError *error))failureBlock
{
    NSLog(@"My Url =%@",url);
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:url];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:somedata];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)  {
             failureBlock(error);
         }
         else {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];
}
- (void)GETRequestWithUrlAndData:(NSDictionary*)params
                          mathod:(NSString*)method
                            link:(NSString*)url
             withSuccessionBlock:(void(^)(id response))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock
{
    //NSURL *myURL = [NSURL URLWithString:[SPHCreateUrl SerializeURL:params methodName:method]];
    NSLog(@"paramsssss =%@",params);
    NSLog(@"My Url =%@",url);
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];
}

@end
