//
//  YSAPICalls.m
//  Your Show
//
//  Created by Siba Prasad Hota on 29/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "YSAPICalls.h"
#import "Reachability.h"
#import "SPHServiceRequest.h"
#import "Generalmodel.h"
#import "CatModel.h"
#import "ProgressHUD.h"
#import "CommentModel.h"
#import "UsesListInfo.h"

@implementation YSAPICalls

+ (BOOL) isNetworkRechable {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    }
    return !(networkStatus == NotReachable);
}


+(void)getActivitySegmentInfo:(NSDictionary * )dict methodName:(NSString * )methodName SuccessionBlock:(void(^)(id newResponse))successBlock
               andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq getBlockServerResponseForparam:dict mathod:methodName withSuccessionBlock:^(id response) {
        successBlock(response);
        
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];
}



#pragma mark - Filter Search
+(void)filterSearch :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
     andFailureBlock:(void(^)(NSError *error))failureBlock {
    
    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq postBlockServerResponseForData:submitData paramDict:nil mathod:METHOD_FILTERSEARCH withSuccessionBlock:^(id response){
        successBlock(response);
    }andFailureBlock:^(NSError *error){
        
    }];
}


+(void)editProfileDetails :(NSDictionary *)dict   ForSuccessionBlock:(void(^)(id newResponse))successBlock
           andFailureBlock:(void(^)(NSError *error))failureBlock{
    
    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_EDITPROFILE withSuccessionBlock:^(id response) {
        successBlock(response);
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];
}



+(void)hifiveMint:(BaseFeedModel *)fmodel
              withSuccessionBlock:(void(^)(id newResponse))successBlock
                  andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;

    NSDictionary *submitDict=[self getUrlAndSubmitData:fmodel];
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:[[submitDict valueForKey:@"submitData"] dataUsingEncoding:NSUTF8StringEncoding] paramDict:nil mathod:METHOD_HIFIVE_MINT link:[NSURL URLWithString:[submitDict valueForKey:@"url"]]
                withSuccessionBlock:^(id response){
          successBlock(response);
     } andFailureBlock:^(NSError *error){
         failureBlock(error);
         NSLog(@"Error message = %@",error);
     }];
    
    
}


+(void)followMint:(BaseFeedModel   *)fmodel
{
    if (![self isNetworkRechable])
        return;
    NSString *url = @"/follow/follow";
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&status=%@",[[NSUserDefaults standardUserDefaults] objectForKey:CEO_AccessToken],fmodel.feed_user_id,(fmodel.is_user_followed)?@"1":@"0"]  dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,url]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSLog(@"followers Url=%@",url);
    NSLog(@"submit data =%@",[NSString stringWithFormat:@"access_token=%@&user_id=%@&status=%@",[[NSUserDefaults standardUserDefaults] objectForKey:CEO_AccessToken],fmodel.feed_user_id,(fmodel.is_user_followed)?@"1":@"0"]);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
         if ([(NSHTTPURLResponse *)response statusCode]==200) {
             if (data==nil||[data isEqual:[NSNull null]])
                 return ;
             id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
             NSLog(@"json data=%@",jsonObject);
             if ([[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"status"]] isEqualToString:@"true"])  {
             }
         }
         else{
         }
     }];
}


+(NSDictionary *)getUrlAndSubmitData :(BaseFeedModel   *) feedModelObject
{
    NSString *url;
    NSString *submitData;
    if (feedModelObject.mintType == MSMintTypeText){
        NSString *highFiveType =  [NSString stringWithFormat:@"%d",feedModelObject.is_user_liked];
        NSString* appurl = @"mint/highfive";
        url =[NSString stringWithFormat:@"%@%@",ServerUrl,appurl];
        
        submitData    = [NSString stringWithFormat:@"access_token=%@&mint_id=%@&action=%@",
                         GETVALUE(CEO_AccessToken),
                         feedModelObject.feed_id,
                         highFiveType] ;
    }  else  {
        NSString *highFiveType = [NSString stringWithFormat:@"%d",feedModelObject.is_user_liked];
        NSString* appurl = @"achievements/highfive";
        url =[NSString stringWithFormat:@"%@%@",ServerUrl,appurl];
        
        submitData    = [NSString stringWithFormat:@"access_token=%@&achievement_id=%@&is_hifive=%@",
                         GETVALUE(CEO_AccessToken),
                         feedModelObject.feed_id,
                         highFiveType];
    }
    NSLog(@"submitData=%@",submitData);
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:url,@"url",submitData,@"submitData", nil];
    
    return dict;
    
}


+(void)inspireMint:(BaseFeedModel   *)fmodel
{
    if (![self isNetworkRechable])
        return;
    
    NSString *markType = (fmodel.is_user_inspired )? @"0":@"1";
    NSString *url =[NSString stringWithFormat:@"%@achievements/inspire",ServerUrl];
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&achievement_id=%@&mark_type=%@",
                           [[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],
                           fmodel.feed_id,
                           markType] dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"submitData=%@",[NSString stringWithFormat:@"access_token=%@&achievement_id=%@&mark_type=%@",
                            [[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],
                            fmodel.feed_id,
                            markType]);
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_INSPIRE_MINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         Generalmodel *gmodel=response;
         
         if (![gmodel.status_code isEqualToString:@"0"])
         {
             NSLog(@"Error message = %@",gmodel.status_Msg);
         }
     } andFailureBlock:^(NSError *error) {
         NSLog(@"Error message = %@",error);
     }];
    
    
}


+(void)nominateMint:(BaseFeedModel   *)fmodel
{
    if (![self isNetworkRechable])
        return;
    
    NSString *markType = (fmodel.is_user_inspired)? @"0":@"1";
    NSString *url =[NSString stringWithFormat:@"%@achievements/nominate",ServerUrl];
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&achievement_id=%@&mark_type=%@",
                           GETVALUE(CEO_AccessToken),
                           fmodel.feed_id,
                           markType] dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"submitData=%@",[NSString stringWithFormat:@"access_token=%@&achievement_id=%@&mark_type=%@",
                            GETVALUE(CEO_AccessToken),
                            fmodel.feed_id,
                            markType]);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_INSPIRE_MINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         Generalmodel *gmodel=response;
         
         if (![gmodel.status_code isEqualToString:@"0"])
         {
             NSLog(@"Error message = %@",gmodel.status_Msg);
         }
     } andFailureBlock:^(NSError *error) {
         NSLog(@"Error message = %@",error);
     }];
}

+(void)joinShowroom:(BaseFeedModel   *)fmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
          andFailureBlock:(void(^)(NSError *error))failureBlock
{

    if (![self isNetworkRechable])
        return;
    
    NSString *url =[NSString stringWithFormat:@"%@follow/follow",ServerUrl];
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&status=%@",DummyAccessToken,fmodel.feed_user_id,(!fmodel.is_user_followed)?@"0":@"1"]  dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"submitData=%@",[NSString stringWithFormat:@"access_token=%@&user_id=%@&status=%@",DummyAccessToken,fmodel.feed_user_id,(!fmodel.is_user_followed)?@"0":@"1"]);
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_FOLLOW_UNFOLLOW link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         Generalmodel *gmodel = response;
         
         if ([gmodel.status isEqualToString:@"true"])
         {
             NSLog(@"Error message = %@",gmodel.status_Msg);
             successBlock(gmodel);
             
         }
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
         
         NSLog(@"Error message = %@",error);
         
     }];
}
+(void)followUnfollowMint:(id)modelObject withSuccessionBlock:(void(^)(id newResponse))successBlock
          andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    NSString *url =[NSString stringWithFormat:@"%@follow/follow",ServerUrl];
    NSData *submitData = nil;
    if ([modelObject isKindOfClass:[BaseFeedModel class]]) {
        BaseFeedModel *fmodel = modelObject;
        submitData = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&type=%d",[[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],((fmodel.IsMintReminted)?fmodel.remint_user_id:fmodel.feed_user_id),1]  dataUsingEncoding:NSUTF8StringEncoding];
     }
   else  if ([modelObject isKindOfClass:[UsesListInfo class]]) {
        UsesListInfo *userInfo = modelObject;

        submitData = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&type=%d",[[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],userInfo.user_id,1]  dataUsingEncoding:NSUTF8StringEncoding];
        
        }
    
    else
    {
        if ([modelObject isKindOfClass:[Profilemodel class]]) {
            Profilemodel *pmodel = modelObject;
            submitData = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&status=%@",[[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],pmodel.follower_userid,([pmodel.user_follow_status isEqualToString:@"1"])?@"1":@"0"]  dataUsingEncoding:NSUTF8StringEncoding];
        }
        
    }
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_FOLLOW_UNFOLLOW link:[NSURL URLWithString:url] withSuccessionBlock:^(id response) {
         Generalmodel *gmodel = response;
         if ([gmodel.status isEqualToString:@"true"]){
             NSLog(@"Sucess message = %@",gmodel.status_Msg);
             successBlock(gmodel);
         } else {
             NSLog(@"Error message = %@",gmodel.status_Msg);
             successBlock(gmodel);
         }
     } andFailureBlock:^(NSError *error)  {
         failureBlock(error);
         NSLog(@"Error message = %@",error);
     }];
}

+(void)FBFriendfollowUnfollowMint:(FBFriends *)fbmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
                  andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    NSString *url =[NSString stringWithFormat:@"%@follow/follow",ServerUrl];
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&type=%@,&user_id=%@",[[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],@"3",fbmodel.friendId]  dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"submitData=%@",[NSString stringWithFormat:@"access_token=%@&user_id=%@",[[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],fbmodel.friendId]);
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_FOLLOW_UNFOLLOW link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         Generalmodel *gmodel = response;
         if ([gmodel.status isEqualToString:@"true"])  {
             NSLog(@"Sucess message = %@",gmodel.status_Msg);
             successBlock(gmodel);
         }  else  {
             NSLog(@"Error message = %@",gmodel.status_Msg);
             successBlock(gmodel);
         }
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
         NSLog(@"Error message = %@",error);
     }];
}



+(void)nominateMint:(BaseFeedModel   *)fmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
    andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    
    NSString *markType = [NSString stringWithFormat:@"%d",fmodel.is_user_nominated];
    NSString *url =[NSString stringWithFormat:@"%@achievements/nominate",ServerUrl];
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&achievement_id=%@&mark_type=%@",
                           GETVALUE(CEO_AccessToken),
                           fmodel.feed_id,
                           markType] dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"submitData=%@",[NSString stringWithFormat:@"access_token=%@&achievement_id=%@&mark_type=%@",
                             GETVALUE(CEO_AccessToken),
                             fmodel.feed_id,
                             markType] );
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_INSPIRE_MINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         Generalmodel *gmodel = response;
         
         if ([gmodel.status isEqualToString:@"true"])
         {
             NSLog(@"Error message = %@",gmodel.status_Msg);
             successBlock(gmodel);
             
         }
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
         NSLog(@"Error message = %@",error);
         
     }];
    
    
}
+(void)inspireMint:(BaseFeedModel   *)fmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    
    NSString *markType = (fmodel.is_user_inspired)? @"0":@"1";
    NSString *url =[NSString stringWithFormat:@"%@achievements/inspire",ServerUrl];
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&achievement_id=%@&mark_type=%@",
                           [[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],
                           fmodel.feed_id,
                           markType] dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"submitData=%@",[NSString stringWithFormat:@"access_token=%@&achievement_id=%@&mark_type=%@",
                            [[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],
                            fmodel.feed_id,
                            markType]);
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_HIFIVE_MINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)  {
         Generalmodel *gmodel = response;
         if ([gmodel.status isEqualToString:@"true"]) {
             NSLog(@"Error message = %@",gmodel.status_Msg);
             successBlock(gmodel);
         }
     } andFailureBlock:^(NSError *error) {
         failureBlock(error);
         NSLog(@"Error message = %@",error);
     }];
}







+(BOOL)CheckForWhiteSpace : (NSString *) string
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [string stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0) {
        // Text was empty or only whitespace.
        return YES;
    }  else
        return NO;
    
}


+(NSMutableArray *)mintpe
{
    NSMutableArray *catArray;
    NSMutableArray *AllMintTypeArray = [NSMutableArray array];
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    
    // get documents path
    
    NSString *documentsPath = [paths objectAtIndex:0];
    
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"MintType.plist"];
    catArray= [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    
    if (![catArray count]) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"MintType" ofType:@"plist"];
        catArray= [[NSMutableArray alloc] initWithContentsOfFile:path];
    }
    
    if ([catArray isKindOfClass:[NSArray class]]||[catArray isKindOfClass:[NSMutableArray class]])
    {
        for (NSDictionary *catdict in catArray)
        {
            CatModel *cats=[[CatModel alloc]init];
            cats.mint_type_id = [catdict valueForKey:@"mint_type_id"];
            cats.mint_type_name = [catdict valueForKey:@"mint_type_name"];
            [AllMintTypeArray addObject:cats];
        }
    }
    
    return AllMintTypeArray;
}


+(void)hifiveMint:(BaseFeedModel   *)fmodel

{
    if (![self isNetworkRechable])
        return;
    NSDictionary *submitDict=[self getUrlAndSubmitData:fmodel];
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    
    [sreq PostRequestWithUrlAndData:[[submitDict valueForKey:@"submitData"] dataUsingEncoding:NSUTF8StringEncoding] paramDict:nil mathod:METHOD_HIFIVE_MINT link:[NSURL URLWithString:[submitDict valueForKey:@"url"]] withSuccessionBlock:^(id response)
     
     {
         Generalmodel *gmodel=response;
         if (![gmodel.status_code isEqualToString:@"0"])
         {
             NSLog(@"Error message = %@",gmodel.status_Msg);
         }
         
     } andFailureBlock:^(NSError *error)
     
     {
         NSLog(@"Error message = %@",error);
     }];
}



- (void)CompleMint:(NSString*)completext
         feedModel:(BaseFeedModel   *)achievementdetails
               url:(NSString*)addUrl
withSuccessionBlock:(void(^)(id response))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock
{
    __block CommentModel *gmodel = nil;
    NSString *url = addUrl;
    
    
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&feed_id=%@&comment_text=%@",
                           GETVALUE(CEO_AccessToken),
                           achievementdetails.feed_id,
                           completext] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@%@?access_token=%@&feed_id=%@&comment_text=%@",ServerUrl,url,
          GETVALUE(CEO_AccessToken), achievementdetails.feed_id,completext);
    
    SPHServiceRequest *sreq=[[SPHServiceRequest alloc]init];
    
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_COMPLI_MINT link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,url]] withSuccessionBlock:^(id response)
     {
         gmodel=response;
         successBlock(gmodel);
         
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}


- (void)editCompleMint:(NSString*)completext
             feedModel:(BaseFeedModel   *)achievementdetails
                   url:(NSString*)addUrl
             commentId:(NSString*)commentId
   withSuccessionBlock:(void(^)(id response))successBlock
       andFailureBlock:(void(^)(NSError *error))failureBlock {
    __block CommentModel *gmodel = nil;
    NSString *url = addUrl;
    
    
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&comment_text=%@&comment_id=%@",
                           GETVALUE(CEO_AccessToken),
                           completext,commentId] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@%@?access_token=%@&comment_text=%@&comment_id=%@",ServerUrl,url,
          GETVALUE(CEO_AccessToken), completext,commentId);
    
    SPHServiceRequest *sreq=[[SPHServiceRequest alloc]init];
    
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_COMPLI_MINT link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,url]] withSuccessionBlock:^(id response)
     {
         gmodel=response;
         successBlock(gmodel);
         
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}



+(void)getShowrooms :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    
    // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_MYSHOW_SHOWROOM];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_MYSHOW_SHOWROOM link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         
         
         successBlock(response);
         
         
         
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
    
    
}


+(void)getAllShowroomForMyShowTabwithDict  : (NSDictionary * ) dict SuccessionBlock:(void(^)(id newResponse))successBlock
                                       andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_MYSHOW_SHOWROOM withSuccessionBlock:^(id response) {
        successBlock(response);
        
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];
}

+(void)getAchievementCategorywithSuccessionBlock:(void(^)(id newResponse))successBlock
                                       andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq getBlockServerResponseForString:DummyAccessToken mathod:METHOD_DISCOVER_ALL withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
     }];


}


+(void)getForDiscoverTab:(NSData *)submitData withSuccessionBlock:(void(^)(id newResponse))successBlock
                                       andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    
    // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_DISCOVER_ALL];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_DISCOVER_ALL link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         
         successBlock(response);
         
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
}

+(void)getAllShowroomForDiscoverTabwithSuccessionBlock:(void(^)(id newResponse))successBlock
  andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq getBlockServerResponseForString:GETVALUE(CEO_AccessToken) mathod:METHOD_DISCOVER_ALL withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error) {
        failureBlock (error);
     }];
}
//ysapicalls.m
+(void)UpdateCoachingTipsFirstTimeVisit: (NSData * )submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
            andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_FIRSTTIMEVISIT link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_FIRSTTIMEVISIT]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
    
}
//ysapicalls.m
+(void)signUp :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq postBlockServerResponseForData:submitData paramDict:nil mathod:METHOD_SIGNUP withSuccessionBlock:^(id response){
        
        successBlock(response);
        
    }andFailureBlock:^(NSError *error){
        
    }];
    
}

+(void)AddShowroomWithData :(NSData *)submitData ForSuccessionBlock:(void(^)(id newResponse))successBlock
                                       andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    
     // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_ADDORUPDATESHOWROOM];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_ADDORUPDATESHOWROOM link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {

         successBlock(response);
 
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
    
    
   }
#pragma MARK --
#pragma mark -----
+ (NSString*)escapedValue:(NSString *)originalValue
{
    NSString* escaped_value = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL, /* allocator */
                                                                                                    (CFStringRef)originalValue,
                                                                                                    NULL, /* charactersToLeaveUnescaped */
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8));
    return escaped_value;
}



+(void)HashTag :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock {
    if (![self isNetworkRechable])
        return;
    
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_HASHTAG];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"strData is %@",strData);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_HASGTAG link:[NSURL URLWithString:url] withSuccessionBlock:^(id response) {
         successBlock(response);
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
     }];
}


+(void)flagMint :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock {
    if (![self isNetworkRechable])
        return;
    
    // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_FLAGMINT];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"strData is %@",strData);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_FLAGMINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)  {
    successBlock(response);
    } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
}


+(void)muteAMint :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
 andFailureBlock:(void(^)(NSError *error))failureBlock {
    if (![self isNetworkRechable])
        return;
    // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_MUTEAMINT];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"strData is %@",strData);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_MUTEMINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)   {
        successBlock(response);
    } andFailureBlock:^(NSError *error) {
         failureBlock (error);
     }];
}


+(void)remintMint :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock {
    if (![self isNetworkRechable])
        return;
    // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_REMINTMINT];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"strData is %@",strData);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_REMINTMINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)  {
         successBlock(response);
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
     }];
}



+(void)EditMint :(NSData *)submitData ForSuccessionBlock:(void(^)(id newResponse))successBlock
 andFailureBlock:(void(^)(NSError *error))failureBlock; {
    if (![self isNetworkRechable])
        return;
    
    // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_EDITMINT];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_EDITMINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response) {
         successBlock(response);
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
     }];
    
}


+(void)getAllComplimints :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
  andFailureBlock:(void(^)(NSError *error))failureBlock {
    if (![self isNetworkRechable])
        return;
    
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_GETALLCOMPLIMENTS];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_GETALLCOMPLIMINTS link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)  {
        successBlock(response);
    } andFailureBlock:^(NSError *error) {
         failureBlock (error);
     }];
}


+(void)myshowMintDetails : (NSDictionary * ) dict ForSuccessionBlock:(void(^)(id newResponse))successBlock
          andFailureBlock:(void(^)(NSError *error))failureBlock {
    if (![self isNetworkRechable])
        return ;
  
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_MYSHOW_MINTDETAIL withSuccessionBlock:^(id response) {
        successBlock(response);
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];
}


+(void)muteAUser :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
  andFailureBlock:(void(^)(NSError *error))failureBlock {
    if (![self isNetworkRechable])
        return ;
    
    NSString *url       =   [NSString stringWithFormat:@"%@%@",ServerUrl,API_MUTEUSER];
    NSString *strData   =   [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"strData is %@",strData);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_MUTEUSER link:[NSURL URLWithString:url] withSuccessionBlock:^(id response) {
        successBlock(response);
    } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
}



+(void)AddMint :(NSData *)submitData textMint:(BOOL)istextMint ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock; {
    if (![self isNetworkRechable])
        return;
    // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,(istextMint)?API_ADDMINTTEXT:API_ADDMINTMEDIA];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"strData is %@",strData);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_ADDMINT link:[NSURL URLWithString:url] withSuccessionBlock:^(id response) {
         successBlock(response);
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
     }];
}

+(void)getShowDetails :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock andFailureBlock:(void(^)(NSError *error))failureBlock {
    if (![self isNetworkRechable])
        return;
    // [ProgressHUD show:@"uploading"];
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_GETSHOWROOMDETAILS];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_GETSHOWROOMDETAILS link:[NSURL URLWithString:url] withSuccessionBlock:^(id response) {
         successBlock(response);
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
}


+(void)getMemberList: (NSData * )submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
     andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return ;
    
    // [ProgressHUD show:@"uploading"];
    NSString *url       =   [NSString stringWithFormat:@"%@%@",ServerUrl,API_GETMEMBERLIST];
    NSString *strData   =   [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"strData is %@",strData);
    NSLog(@"URL is %@",url);
    
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_MEMBER_LIST link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         successBlock(response);
         
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
}

+(void)joinShowroom :(NSData *)submitData   ForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_JOINSHOWROOM];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_JOINSHOWROOM link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         successBlock(response);
         
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
    
}
+(void)getAllFriendsInCircle :(NSData *)submitData methodName:(NSString *)methodName  ForSuccessionBlock:(void(^)(id newResponse))successBlock andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_INCIRCLE];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:methodName link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         successBlock(response);
         
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
    
}





+(void)getHashTagMints  : (NSDictionary * ) dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
         andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    

    SPHServiceRequest *sreq=[SPHServiceRequest new];
    
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_GETHASHTAGMINTS withSuccessionBlock:^(id response) {
        successBlock(response);
        
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];
    
}

+(void)getParticularShowroom  : (NSDictionary * ) dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
               andFailureBlock:(void(^)(NSError *error))failureBlock
{



    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_GETPARTICULARSHOWROOM withSuccessionBlock:^(id response) {
        successBlock(response);
        
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];






}
+ (void)MintItpressed:(id)sender
{    
        // NSLog(@"%@",currentLocation.);
        NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&mint_text=%@&category_id=%@&type_id=%@&custom_mint_text=%@&achievement_location_text=%@&lat=%f&lan=%f",DummyAccessToken,@"Tested by IOS team",@"",@"",@"customMintTxt",@"salem",45.9,23.09] dataUsingEncoding:NSUTF8StringEncoding];
    
        NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@mint/post_single_mint",ServerUrl]]];
        [submitrequest setHTTPMethod:@"POST"];
        [submitrequest setHTTPBody:submitData];
          [ProgressHUD show:@"Loading"];
        [NSURLConnection sendAsynchronousRequest:submitrequest
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if ([(NSHTTPURLResponse *)response statusCode]==200)
             {
                 
                 [ProgressHUD dismiss];
                 
                 id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                 NSLog(@"json data=%@",json);
                 if ([[NSString stringWithFormat:@"%@",[json objectForKey:@"status"]] isEqualToString:@"true"])
                 {
                     
                 }
                 
             }
             else
             {
                 [ProgressHUD dismiss];
             }
    }];
}


+(void)getMintsForMyShowTabwithDict  : (NSDictionary * ) dict SuccessionBlock:(void(^)(id newResponse))successBlock
                      andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_MYSHOW_MINT withSuccessionBlock:^(id response) {
        successBlock(response);
        
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];
}


#pragma mark - Mint Detail

+(void)MintDetail :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq postBlockServerResponseForData:submitData paramDict:nil mathod:METHOD_MINTDETAIL withSuccessionBlock:^(id response){
      
        successBlock(response);

    }andFailureBlock:^(NSError *error){
        
    }];
    
    
}
+(void)deleteMint :(NSDictionary *)dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
   andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return ;
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_DELETE_MINT withSuccessionBlock:^(id response) {
        successBlock(response);
        
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];

    
}


#pragma mark - Profile


+(void)getUserProfile  : (NSDictionary * ) dict  ForSuccessionBlock:(void(^)(id newResponse))successBlock
        andFailureBlock:(void(^)(NSError *error))failureBlock
{
    
    
    if (![self isNetworkRechable])
        return;
    
    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_GETUSERPROFILE withSuccessionBlock:^(id response) {
        successBlock(response);
        
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];
    
    
    
}


+(void)getProfileFollowers :(NSData *)submitData   ForSuccessionBlock:(void(^)(id newResponse))successBlock
            andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_PROFILEFOLLOWERS];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_PROFILEFOLLOWERS link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         successBlock(response);
         
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
    
}


+(void)getProfileFollowUnFollow:(Profilemodel *)pmodel withSuccessionBlock:(void(^)(id newResponse))successBlock
                andFailureBlock:(void(^)(NSError *error))failureBlock
{
    if (![self isNetworkRechable])
        return;
    NSString *url =[NSString stringWithFormat:@"%@follow/follow",ServerUrl];
    NSData *submitData = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&status=%@",[[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],pmodel.user_id,([pmodel.user_follow_status isEqualToString:@"1"])?@"1":@"0"]  dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"submitData=%@",[[NSString stringWithFormat:@"access_token=%@&user_id=%@&status=%@",[[NSUserDefaults standardUserDefaults] valueForKey:CEO_AccessToken],pmodel.user_id,([pmodel.user_follow_status isEqualToString:@"1"])?@"1":@"0"]  dataUsingEncoding:NSUTF8StringEncoding]);
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_FOLLOW_UNFOLLOW link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         Generalmodel *gmodel = response;
         if ([gmodel.status isEqualToString:@"true"])
         {
             NSLog(@"Sucess message = %@",gmodel.status_Msg);
             successBlock(gmodel);
         }
         else
         {
             NSLog(@"Error message = %@",gmodel.status_Msg);
             successBlock(gmodel);
         }
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
         NSLog(@"Error message = %@",error);
     }];
    
    
}

+(void)getSuggestionList :(NSData *)submitData  ForSuccessionBlock:(void(^)(id newResponse))successBlock
          andFailureBlock:(void(^)(NSError *error))failureBlock

{
    if (![self isNetworkRechable])
        return;
    
    
    
    NSString *url =[NSString stringWithFormat:@"%@%@",ServerUrl,API_SEARCHSUGGESTION];
    NSString *strData = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"strData is %@",strData);
    
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    [sreq PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_SEARCHSUGGETION link:[NSURL URLWithString:url] withSuccessionBlock:^(id response)
     {
         successBlock(response);
         
     } andFailureBlock:^(NSError *error) {
         failureBlock (error);
         
     }];
    
}
+(void)getProfileStatsDetails :(NSDictionary *)dict   ForSuccessionBlock:(void(^)(id newResponse))successBlock
               andFailureBlock:(void(^)(NSError *error))failureBlock
{
    
    
    if (![self isNetworkRechable])
        return;
    SPHServiceRequest *sreq=[SPHServiceRequest new];
    
    [sreq getBlockServerResponseForparam:dict mathod:METHOD_PROFILESTATS withSuccessionBlock:^(id response) {
        successBlock(response);
        
    } andFailureBlock:^(NSError *error) {
        failureBlock(error);
    }];
    
    
}


@end
