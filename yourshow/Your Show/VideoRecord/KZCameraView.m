//
//  KZCameraView.m
//  VideoRecorder
//
//  Created by Kseniya Kalyuk Zito on 10/21/13.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
 #define NSLog(FORMAT, ...) nil
#import "KZCameraView.h"
#import "CaptureManager.h"
#import "AVCamRecorder.h"
#import <AVFoundation/AVFoundation.h>

static void *AVCamFocusModeObserverContext = &AVCamFocusModeObserverContext;

@interface KZCameraView () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) CaptureManager *captureManager;
@property (nonatomic, strong) UIView *videoPreviewView;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic, strong) UILabel *focusModeLabel;

@property (nonatomic, strong) UIImageView *recordBtn,*camImageView;

//Exporting progress
@property (nonatomic,strong) UIView *progressView,*bottomView;
@property (nonatomic,strong) UIProgressView *progressBar;
@property (nonatomic,strong) UILabel *progressLabel;
@property (nonatomic,strong) UIActivityIndicatorView *activityView;

//Recording progress
@property (nonatomic,strong) UIProgressView *durationProgressBar;
@property (nonatomic,assign) float duration;
@property (nonatomic,strong) NSTimer *durationTimer;

//Button to switch between back and front cameras
@property (nonatomic,strong) UIButton *camerasSwitchBtn,*cameraButton;
@property (nonatomic,strong) UIButton *buttonDelete,*buttonVideo;

//Delete last piece
@property (nonatomic,strong) UIButton *deleteLastBtn;
@property (nonatomic,strong) UILabel *galleryLabel,*videoLabel, *cameraLabel,*recordTimeLable,*lblPause;
@end

@interface KZCameraView (InternalMethods) <UIGestureRecognizerDelegate>

- (CGPoint)convertToPointOfInterestFromViewCoordinates:(CGPoint)viewCoordinates;
- (void)tapToAutoFocus:(UIGestureRecognizer *)gestureRecognizer;
- (void)tapToContinouslyAutoFocus:(UIGestureRecognizer *)gestureRecognizer;

@end

@interface KZCameraView (CaptureManagerDelegate) <CaptureManagerDelegate>
@end

@implementation KZCameraView

- (id)initWithFrame:(CGRect)frame withVideoPreviewFrame:(CGRect)videoFrame
{
    self = [super initWithFrame:frame];
    if (self) {
        if ([self captureManager] == nil) {
            CaptureManager *manager = [[CaptureManager alloc] init];
            [self setCaptureManager:manager];
            
            [[self captureManager] setDelegate:self];
            
            if ([[self captureManager] setupSession]) {
                // Create video preview layer and add it to the UI
                AVCaptureVideoPreviewLayer *newCaptureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:[[self captureManager] session]];
                
                self.videoPreviewView = [[UIView alloc]init];
                self.videoPreviewView.frame =  CGRectMake(0.0, 44.0, videoFrame.size.width, videoFrame.size.width+44);
                CALayer *viewLayer = self.videoPreviewView.layer;
                [viewLayer setMasksToBounds:YES];
                [self addSubview:self.videoPreviewView];
                
                CGRect bounds = self.videoPreviewView.bounds;
                [newCaptureVideoPreviewLayer setFrame:bounds];
                
                if ([newCaptureVideoPreviewLayer.connection isVideoOrientationSupported]) {
                    [newCaptureVideoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
                }
                
                [newCaptureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
                
                [viewLayer insertSublayer:newCaptureVideoPreviewLayer below:[[viewLayer sublayers] objectAtIndex:0]];
                
                [self setCaptureVideoPreviewLayer:newCaptureVideoPreviewLayer];
                
                // Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [[[self captureManager] session] startRunning];
                });
                
                //set record button image. Replace with any image
                UIImage *recordImage = [UIImage imageNamed:@"CameraCapture.png"];
                self.recordBtn = [[UIImageView alloc]initWithImage:recordImage];
                self.recordBtn.bounds = CGRectMake(0.0, 0.0, 48, 48);
                self.recordBtn.center = CGPointMake(self.frame.size.width/2, self.videoPreviewView.frame.size.height + (self.frame.size.height - self.videoPreviewView.frame.size.height-55)/2 -40);
                self.recordBtn.userInteractionEnabled = YES;
                [self addSubview:self.recordBtn];
                
                //Record Long Press Gesture on the record button
                UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(startRecording:)];
                [longPress setDelegate:self];
                [self.recordBtn addGestureRecognizer:longPress];
                [self addObserver:self forKeyPath:@"captureManager.videoInput.device.focusMode" options:NSKeyValueObservingOptionNew context:AVCamFocusModeObserverContext];
                
                if (isSmallPad) {
                    self.durationProgressBar = [[UIProgressView alloc]initWithFrame:CGRectMake(0.0, self.frame.size.height-188, videoFrame.size.width, 5.0)];
                }
                else

                self.durationProgressBar = [[UIProgressView alloc]initWithFrame:CGRectMake(0.0, videoFrame.origin.y + videoFrame.size.height+88, videoFrame.size.width, 5.0)];
                _durationProgressBar.progressTintColor = [UIColor whiteColor];
                [_durationProgressBar setTransform:CGAffineTransformMakeScale(1.0, 4.0)];
                
                _durationProgressBar.trackTintColor = [UIColor colorWithRed:(40/255.0) green:(39/255.0) blue:(38/255.0) alpha:1.0];
                
                [self addSubview:self.durationProgressBar];
                
                
                // Add a single tap gesture to focus on the point tapped, then lock focus
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToAutoFocus:)];
                [singleTap setDelegate:self];
                [singleTap setNumberOfTapsRequired:1];
                [self.videoPreviewView addGestureRecognizer:singleTap];
                
                // Add a double tap gesture to reset the focus mode to continuous auto focus
                UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToContinouslyAutoFocus:)];
                [doubleTap setDelegate:self];
                [doubleTap setNumberOfTapsRequired:2];
                [singleTap requireGestureRecognizerToFail:doubleTap];
                [self.videoPreviewView addGestureRecognizer:doubleTap];
                
                //Create progress view for saving the video
                self.progressView = [[UIView alloc]initWithFrame:videoFrame];
                self.progressView.backgroundColor = [UIColor clearColor];
                self.progressView.hidden = YES;
                
                self.progressBar = [[UIProgressView alloc]initWithFrame:CGRectMake(0.0, 0.0, videoFrame.size.width - 60.0, 5.0)];
                
                self.progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, videoFrame.size.width - 60.0, 20.0)];
                self.progressLabel.backgroundColor = [UIColor clearColor];
                self.progressLabel.textColor = [UIColor whiteColor];
                self.progressLabel.textAlignment = NSTextAlignmentCenter;
                
                self.activityView = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0, 0.0, 50.0, 50.0)];
                self.activityView.hidesWhenStopped = YES;
                
                self.progressBar.center = self.progressView.center;
                self.activityView.center = self.progressView.center;
                self.progressLabel.center = CGPointMake(self.progressView.center.x, self.progressView.center.y + 20.0);
                
                [self addSubview:self.progressView];
                [self.progressView addSubview:self.progressBar];
                [self.progressView addSubview:self.progressLabel];
                [self.progressView addSubview:self.activityView];
                [self addSubviewToCamera];
            }
        }
    }
    return self;
}

-(void)removeDeleteUseVideo
{
    [self.buttonDelete removeFromSuperview];
    self.buttonDelete = nil;
    [self.buttonVideo removeFromSuperview];
    self.buttonVideo = nil;
    [self.lblPause removeFromSuperview];
    self.lblPause = nil;
}
-(void)useVideo
{
    //self.buttonVideo.hidden = YES;
    self.lblPause.hidden = YES;
    if (self.durationProgressBar.progress > .0) {
        [self.delegate useVideoSelectedFromKZCameraView];
        [self  removeDeleteUseVideo];
    }
    
}
-(void)videoGalleryTapped
{
   // [self  removeDeleteUseVideo];
  //  [self  removeSubView];
    [self.delegate videoGallerySelectedFromKZCameraView];
}
-(void)setShowCameraSwitch:(BOOL)showCameraSwitch
{
    if (showCameraSwitch)
    {
        if (!self.camerasSwitchBtn)
        {
            
           
     
        }
    }
    else
    {
        if (self.camerasSwitchBtn)
        {
            [self.camerasSwitchBtn removeFromSuperview];
            self.camerasSwitchBtn = nil;
            
          
        }
    }
}

-(void)switchToCameraScreen
{
    [self.delegate switchToCameraScreenChange];


}
-(void)addSubviewToCamera
{

    //set record button image. Replace with any image
    UIImage *camImage = [UIImage imageNamed:@"SwitchPhoto.png"];
        self.cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cameraButton setImage:camImage forState:UIControlStateNormal];
    self.cameraButton.bounds = CGRectMake(0.0, 0.0, 48, 48);
    self.cameraButton.center = CGPointMake(self.frame.size.width/2, self.videoPreviewView.frame.size.height + (self.frame.size.height - self.videoPreviewView.frame.size.height-55)/2+10);
    [self.cameraButton addTarget:self action:@selector(goToCameraScreen) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.cameraButton];
    
    
    self.cameraLabel = [[UILabel alloc] initWithFrame:CGRectMake(_cameraButton.frame.origin.x-5,_cameraButton.frame.origin.y+_cameraButton.frame.size.height, 70, 20)];
    [self.cameraLabel setBackgroundColor:[UIColor clearColor]];
    [self.cameraLabel setTextColor:[UIColor whiteColor]];
    [self.cameraLabel setText:@"CAMERA"];
    //                [cameraLabel setTextAlignment:NSTextAlignmentCenter];
    [self.cameraLabel setFont:[UIFont boldSystemFontOfSize:13.0]];
    [self addSubview:self.cameraLabel];
    
   
    
    self.deleteLastBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *SwitchPhoto = [UIImage imageNamed:@"GalleryIcon.png"];
    [self.deleteLastBtn setImage:SwitchPhoto forState:UIControlStateNormal];
    self.deleteLastBtn.bounds = CGRectMake(0.0, 0.0, 48, 48);
    self.deleteLastBtn.center = CGPointMake(60.0, self.videoPreviewView.frame.size.height + (self.frame.size.height - self.videoPreviewView.frame.size.height-55)/2 +10);
    [self.deleteLastBtn addTarget:self action:@selector(videoGalleryTapped) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.deleteLastBtn];
    
    self.galleryLabel = [[UILabel alloc] initWithFrame:CGRectMake(_deleteLastBtn.frame.origin.x,_deleteLastBtn.frame.origin.y+_deleteLastBtn.frame.size.height, 70, 20)];
    [self.galleryLabel setBackgroundColor:[UIColor clearColor]];
    [self.galleryLabel setTextColor:[UIColor whiteColor]];
    [self.galleryLabel setText:@"GALLERY"];
    [self.galleryLabel setFont:[UIFont boldSystemFontOfSize:13.0]];
    [self addSubview:self.galleryLabel];
    
    
    UIImage *btnImg = [UIImage imageNamed:@"SwitchVideo.png"];
    self.camerasSwitchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.camerasSwitchBtn setImage:btnImg forState:UIControlStateNormal];
    self.camerasSwitchBtn.bounds = CGRectMake(0.0, 0.0, 48, 48);
    self.camerasSwitchBtn.center = CGPointMake(self.frame.size.width - btnImg.size.width/2 - 30.0, self.videoPreviewView.frame.size.height + (self.frame.size.height - self.videoPreviewView.frame.size.height-55)/2+10);
    //            [self.camerasSwitchBtn addTarget:self action:@selector(switchToCameraScreen) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.camerasSwitchBtn];
    
    
    self.videoLabel = [[UILabel alloc] initWithFrame:CGRectMake(_camerasSwitchBtn.frame.origin.x,_camerasSwitchBtn.frame.origin.y+_camerasSwitchBtn.frame.size.height, 70, 20)];
    [self.videoLabel setBackgroundColor:[UIColor clearColor]];
    [self.videoLabel setTextColor:ORANGECOLOR];
    [self.videoLabel setText:@"VIDEO"];
    [self.videoLabel setFont:[UIFont boldSystemFontOfSize:13.0]];
    [self addSubview:self.videoLabel];
    
    
    
    self.buttonDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.buttonDelete addTarget:self.captureManager action:@selector(deleteLastAsset) forControlEvents:UIControlEventTouchUpInside];
    self.buttonDelete.titleLabel.font = [UIFont boldSystemFontOfSize:13.0];
    
    [self.buttonDelete setTitle:@"DELETE" forState:UIControlStateNormal];
    self.buttonDelete.frame = CGRectMake(15,_deleteLastBtn.frame.origin.y+30, 75, 48);
    [self addSubview:self.buttonDelete];
    self.buttonDelete.hidden = YES;
    
    
    self.buttonVideo = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.buttonVideo addTarget:self
                         action:@selector(useVideo)
               forControlEvents:UIControlEventTouchUpInside];
    [self.buttonVideo setTitle:@"USE VIDEO" forState:UIControlStateNormal];
    self.buttonVideo.frame = CGRectMake( self.videoPreviewView.frame.size.width- 115,_deleteLastBtn.frame.origin.y+30, 105, 48);
    self.buttonVideo.titleLabel.font = [UIFont boldSystemFontOfSize:13.0];
    
    ;
    [self addSubview:self.buttonVideo];
    self.buttonVideo.hidden = YES;
    
    
    
    
    //Video Paused Lable
    self.lblPause = [[UILabel alloc] initWithFrame:CGRectMake( (self.videoPreviewView.frame.size.width-60)/2,_deleteLastBtn.frame.origin.y-5, 60, 48)];
    [self.lblPause setBackgroundColor:[UIColor clearColor]];
    [self.lblPause setTextColor:[UIColor colorWithRed:(255/255.0) green:(0/255.0) blue:(0/255.0) alpha:1.0]];
    [self.lblPause setText:@"Paused"];
    [self.lblPause setTextAlignment:NSTextAlignmentCenter];
    [self.lblPause setFont:[UIFont boldSystemFontOfSize:12.0]];
    [self addSubview:self.lblPause];
    self.lblPause.hidden = YES;
    [self.recordTimeLable removeFromSuperview];
     self.recordTimeLable = nil;
    //RecordTimeLable
    self.recordTimeLable = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.origin.x+2,self.durationProgressBar.frame.origin.y-20, 70, 20)];
//    self.recordTimeLable = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.origin.x+2,380, 70, 20)];
    [self.recordTimeLable setBackgroundColor:[UIColor clearColor]];
    [self.recordTimeLable setTextColor:[UIColor whiteColor]];
    [self.recordTimeLable setText:@"00.00"];
    [self.recordTimeLable setFont:[UIFont boldSystemFontOfSize:12.0]];
    [self addSubview:self.recordTimeLable];
    _recordTimeLable.hidden = YES;

}
-(void)removeSubView
{
    
    [self.deleteLastBtn removeFromSuperview];
    self.deleteLastBtn = nil;
    
    
    [self.camerasSwitchBtn removeFromSuperview];
    self.camerasSwitchBtn = nil;
    
    
    [self.cameraButton removeFromSuperview];
    self.cameraButton = nil;
    
    [self.cameraLabel removeFromSuperview];
    self.cameraLabel = nil;
    
    [self.galleryLabel removeFromSuperview];
    self.galleryLabel = nil;
    
    [self.camerasSwitchBtn removeFromSuperview];
    self.camerasSwitchBtn = nil;
    
    
    [self.videoLabel removeFromSuperview];
    self.videoLabel = nil;
    
    
}
-(void)switchCamera
{
   //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GalleryTapped:) name:@"GalleryTapped" object:nil];
   //[[NSNotificationCenter defaultCenter] postNotificationName:@"GalleryTapped" object:nil userInfo:[NSDictionary dictionaryWithObject:@"2" forKey:@"VideoORGallery"]];
  
   [self.captureManager switchCamera];
}

- (NSString *)stringForFocusMode:(AVCaptureFocusMode)focusMode
{
	NSString *focusString = @"";
	
	switch (focusMode) {
		case AVCaptureFocusModeLocked:
			focusString = @"locked";
			break;
		case AVCaptureFocusModeAutoFocus:
			focusString = @"auto";
			break;
		case AVCaptureFocusModeContinuousAutoFocus:
			focusString = @"continuous";
			break;
	}
	
	return focusString;
}
//need to check
- (void)dealloc
{

    @try{
        [self removeObserver:self forKeyPath:@"captureManager.videoInput.device.focusMode"];

    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
    
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == AVCamFocusModeObserverContext) {
        // Update the focus UI overlay string when the focus mode changes
		[self.focusModeLabel setText:[NSString stringWithFormat:@"focus: %@", [self stringForFocusMode:(AVCaptureFocusMode)[[change objectForKey:NSKeyValueChangeNewKey] integerValue]]]];
	} else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark

- (IBAction)startRecording:(UILongPressGestureRecognizer*)recognizer {
    switch (recognizer.state)  {
        case UIGestureRecognizerStateBegan:
        {
            NSLog(@"START");
            if (![[[self captureManager] recorder] isRecording]) {
                if (self.duration < self.maxDuration) {
                    [[self captureManager] startRecording];
                }
                self.lblPause.hidden = YES;
            }
            break;
        }
        case UIGestureRecognizerStateEnded: {
            if ([[[self captureManager] recorder] isRecording]) {
                self.lblPause.hidden = NO;

                [self.durationTimer invalidate];
                [[self captureManager] stopRecording];
                self.videoPreviewView.layer.borderColor = [UIColor clearColor].CGColor;
                NSLog(@"END number of pieces %lu", (unsigned long)[self.captureManager.assets count]);
            }
            break;
        }
        default:
            break;
    }
}

- (void) updateDuration {
    if ([[[self captureManager] recorder] isRecording]) {
        self.duration = self.duration + 0.1;

        _recordTimeLable.hidden = NO;
        int seconds = (int)self.duration % 60;
        int minutes = ((int)self.duration / 60) % 60;

        _recordTimeLable.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];


        self.durationProgressBar.progress = self.duration/self.maxDuration;
        NSLog(@"self.duration %f, self.progressBar %f", self.duration, self.durationProgressBar.progress);
        if (self.durationProgressBar.progress > .99) {
            [self.durationTimer invalidate];
            self.durationTimer = nil;
            [[self captureManager] stopRecording];
        }
    }  else {
        [self.durationTimer invalidate];
        self.durationTimer = nil;
    }
    
    if (self.buttonDelete.hidden) {
        self.buttonDelete.hidden     = NO;
        self.buttonVideo.hidden      = NO;
        [self removeSubView];
    }
}

- (void) removeTimeFromDuration:(float)removeTime {
    _recordTimeLable.hidden = (self.duration <= 0)? YES:NO;
    self.duration = self.duration - removeTime;
    _recordTimeLable.hidden = NO;
    int seconds = (int)self.duration % 60;
    int minutes = ((int)self.duration / 60) % 60;
    
    _recordTimeLable.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];

    if (self.duration != self.duration)  {
        
    }
    else
        self.durationProgressBar.progress = self.duration/self.maxDuration;
    
    
    if (!self.buttonDelete.hidden)  {
        if (self.durationProgressBar.progress == 0.0) {
            [self removeDeleteUseVideo];
            [self addSubviewToCamera];
        }
    }
}


-(void)goToCameraScreen
{

    [self removeDeleteUseVideo];
    [self removeSubView];

   [[NSNotificationCenter defaultCenter] postNotificationName:@"GalleryTapped" object:nil userInfo:[NSDictionary dictionaryWithObject:@"1" forKey:@"VideoORGallery"]];


}

- (void)saveVideoWithCompletionBlock:(void(^)(BOOL success,NSURL *outPUTurl))completion {
    
    __block id weakSelf = self;

    [self.captureManager saveVideoWithCompletionBlock:^(BOOL success,NSURL *outPUTurl) {
        
        if (completion)
        {
           // self.progressLabel.text = @"Saved To Photo Album";
            [weakSelf performSelector:@selector(refresh) withObject:nil afterDelay:0.5];
        }
        else
        {
            self.progressLabel.text = @"Video Saving Failed";
        }
        
        [self.activityView stopAnimating];
        
        completion (success,outPUTurl);
    }];
}

-(void)refresh
{
    self.progressView.hidden = YES;
    self.duration = 0.0;
    self.durationProgressBar.progress = 0.0;
    [self.durationTimer invalidate];
    self.durationTimer = nil;
 }

@end

@implementation KZCameraView (InternalMethods)

// Convert from view coordinates to camera coordinates, where {0,0} represents the top left of the picture area, and {1,1} represents
// the bottom right in landscape mode with the home button on the right.
- (CGPoint)convertToPointOfInterestFromViewCoordinates:(CGPoint)viewCoordinates
{
    CGPoint pointOfInterest = CGPointMake(.5f, .5f);
    CGSize frameSize = self.videoPreviewView.frame.size;
    
    if ([self.captureVideoPreviewLayer.connection isVideoMirrored]) {
        viewCoordinates.x = frameSize.width - viewCoordinates.x;
    }
    
    if ( [[self.captureVideoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResize] ) {
		// Scale, switch x and y, and reverse x
        pointOfInterest = CGPointMake(viewCoordinates.y / frameSize.height, 1.f - (viewCoordinates.x / frameSize.width));
    } else {
        CGRect cleanAperture;
        for (AVCaptureInputPort *port in [[[self captureManager] videoInput] ports]) {
            if ([port mediaType] == AVMediaTypeVideo) {
                cleanAperture = CMVideoFormatDescriptionGetCleanAperture([port formatDescription], YES);
                CGSize apertureSize = cleanAperture.size;
                CGPoint point = viewCoordinates;
                
                CGFloat apertureRatio = apertureSize.height / apertureSize.width;
                CGFloat viewRatio = frameSize.width / frameSize.height;
                CGFloat xc = .5f;
                CGFloat yc = .5f;
                
                if ( [[self.captureVideoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspect] ) {
                    if (viewRatio > apertureRatio) {
                        CGFloat y2 = frameSize.height;
                        CGFloat x2 = frameSize.height * apertureRatio;
                        CGFloat x1 = frameSize.width;
                        CGFloat blackBar = (x1 - x2) / 2;
						// If point is inside letterboxed area, do coordinate conversion; otherwise, don't change the default value returned (.5,.5)
                        if (point.x >= blackBar && point.x <= blackBar + x2) {
							// Scale (accounting for the letterboxing on the left and right of the video preview), switch x and y, and reverse x
                            xc = point.y / y2;
                            yc = 1.f - ((point.x - blackBar) / x2);
                        }
                    } else {
                        CGFloat y2 = frameSize.width / apertureRatio;
                        CGFloat y1 = frameSize.height;
                        CGFloat x2 = frameSize.width;
                        CGFloat blackBar = (y1 - y2) / 2;
						// If point is inside letterboxed area, do coordinate conversion. Otherwise, don't change the default value returned (.5,.5)
                        if (point.y >= blackBar && point.y <= blackBar + y2) {
							// Scale (accounting for the letterboxing on the top and bottom of the video preview), switch x and y, and reverse x
                            xc = ((point.y - blackBar) / y2);
                            yc = 1.f - (point.x / x2);
                        }
                    }
                } else if ([[self.captureVideoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspectFill]) {
					// Scale, switch x and y, and reverse x
                    if (viewRatio > apertureRatio) {
                        CGFloat y2 = apertureSize.width * (frameSize.width / apertureSize.height);
                        xc = (point.y + ((y2 - frameSize.height) / 2.f)) / y2; // Account for cropped height
                        yc = (frameSize.width - point.x) / frameSize.width;
                    } else {
                        CGFloat x2 = apertureSize.height * (frameSize.height / apertureSize.width);
                        yc = 1.f - ((point.x + ((x2 - frameSize.width) / 2)) / x2); // Account for cropped width
                        xc = point.y / frameSize.height;
                    }
                }
                
                pointOfInterest = CGPointMake(xc, yc);
                break;
            }
        }
    }
    
    return pointOfInterest;
}

// Auto focus at a particular point. The focus mode will change to locked once the auto focus happens.
- (void)tapToAutoFocus:(UIGestureRecognizer *)gestureRecognizer
{
    if ([[[self.captureManager videoInput] device] isFocusPointOfInterestSupported]) {
        CGPoint tapPoint = [gestureRecognizer locationInView:[self videoPreviewView]];
        CGPoint convertedFocusPoint = [self convertToPointOfInterestFromViewCoordinates:tapPoint];
        [self.captureManager autoFocusAtPoint:convertedFocusPoint];
    }
}

// Change to continuous auto focus. The camera will constantly focus at the point choosen.
- (void)tapToContinouslyAutoFocus:(UIGestureRecognizer *)gestureRecognizer
{
    if ([[[self.captureManager videoInput] device] isFocusPointOfInterestSupported])
        [self.captureManager continuousFocusAtPoint:CGPointMake(.5f, .5f)];
}

@end

@implementation KZCameraView (CaptureManagerDelegate)

- (void) updateProgress
{
    self.progressView.hidden    = NO;
    self.progressBar.hidden     = NO;
    self.activityView.hidden    = YES;
    self.progressLabel.text     = @"Creating the video";
    self.progressBar.progress   = self.captureManager.exportSession.progress;
    if (self.progressBar.progress > .99) {
        [self.captureManager.exportProgressBarTimer invalidate];
        self.captureManager .exportProgressBarTimer = nil;
    }
}

- (void) removeProgress
{
    self.progressBar.hidden = YES;
    [self.activityView startAnimating];
    self.progressLabel.text = @"Saving to Camera Roll";
}

- (void)captureManager:(CaptureManager *)captureManager didFailWithError:(NSError *)error
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                                            message:[error localizedFailureReason]
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
                                                  otherButtonTitles:nil];
        [alertView show];
    });
}

- (void)captureManagerRecordingBegan:(CaptureManager *)captureManager
{
//    self.videoPreviewView.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.videoPreviewView.layer.borderWidth = 2.0;
    self.durationTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateDuration) userInfo:nil repeats:YES];
}

- (void)captureManagerRecordingFinished:(CaptureManager *)captureManager
{
    
}

- (void)captureManagerDeviceConfigurationChanged:(CaptureManager *)captureManager
{
    //Do something
}


-(void)showBottomView
{
    _bottomView = [[UIView alloc] initWithFrame: CGRectMake ( 0,self.videoPreviewView.frame.size.height+100, CGRectGetWidth(self.frame), 120)];
    _bottomView.backgroundColor = self.backgroundColor;
    
    UIButton *buttonDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonDelete addTarget:self.captureManager action:@selector(deleteLastAsset) forControlEvents:UIControlEventTouchUpInside];
    [buttonDelete setTitle:@"DELETE" forState:UIControlStateNormal];
    buttonDelete.frame = CGRectMake(0.0, 9.0, 75.0, 40.0);
    [_bottomView addSubview:buttonDelete];
    
    
    UIButton *buttonVideo = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonVideo addTarget:self
                    action:@selector(aMethod:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonVideo setTitle:@"USE VIDEO" forState:UIControlStateNormal];
    buttonVideo.frame = CGRectMake( CGRectGetWidth(self.frame) - 110.0, 9.0, 105.0, 40.0);
    [_bottomView addSubview:buttonVideo];
    [self addSubview:_bottomView];
    //_bottomView.hidden = YES;
    
    
    
}

@end
