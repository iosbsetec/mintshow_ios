

#import <UIKit/UIKit.h>


@protocol KZCameraViewDelegate

- (void)switchToCameraScreenChange;
- (void)useVideoSelectedFromKZCameraView;
- (void)videoGallerySelectedFromKZCameraView;



@end

@class CaptureManager, AVCamPreviewView, AVCaptureVideoPreviewLayer;

@interface KZCameraView : UIView <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, assign) float maxDuration;
@property (nonatomic,assign) BOOL showCameraSwitch;
@property (assign) id  <KZCameraViewDelegate> delegate;


- (id)initWithFrame:(CGRect)frame withVideoPreviewFrame:(CGRect)videoFrame;
- (void)saveVideoWithCompletionBlock:(void(^)(BOOL success,NSURL *outPUTurl))completion;
//- (void)saveVideoWithCompletionBlock:(void(^)(BOOL success))completion;
-(void)switchCamera;
-(void)useVideoSelectedFromKZCameraView;
-(void)removeDeleteUseVideo;
-(void)addSubviewToCamera;
-(void)removeSubView;


@end
