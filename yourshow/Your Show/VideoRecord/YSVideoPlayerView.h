//
//  YSVideoPlayerView.h
//  TableviewVideo
//
//  Created by Siba Prasad Hota on 17/10/15.
//  Copyright © 2015 Bsetec. All rights reserved.
//

/*!
 @header YSVideoPlayerView.h
 
 @brief This is the header file where all the properties of YSVideoPlayerView is declared
 
 This file contains the most importnant method and properties decalaration. It's parted by four methods and two delegate methodsthe video pla in total, which can be usey used to perform the video play.
 
 @author     Siba Prasad Hota
 @copyright  2015 Bsetec
 @version    1.0.0
 */

#import <UIKit/UIKit.h>
#import "YSVideoPlayerView.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>


@class YSVideoPlayerView;

@protocol ysPlayerViewDelegate <NSObject>
@optional
-(void)ysPlayerViewZoomButtonClicked:(YSVideoPlayerView*)view isFullScreenMode:(BOOL)isfullScreen;
-(void)ysPlayerFinishedPlayback:(YSVideoPlayerView*)view;

@end


@interface YSVideoPlayerView : UIView

@property (assign, nonatomic) id <ysPlayerViewDelegate> ysdelegate;
@property (assign, nonatomic) BOOL isFullScreenMode;
@property (retain, nonatomic) NSURL *contentURL;
@property (retain, nonatomic) AVPlayer *moviePlayer;
@property (assign, nonatomic) BOOL isPlaying;

@property (retain, nonatomic) UIButton *playPauseButton;
@property (retain, nonatomic) UIButton *volumeButton;
@property (retain, nonatomic) UIButton *bigvolumeButton;

@property (retain, nonatomic) UIButton *closeButton;

@property (retain, nonatomic) UIButton *zoomButton;
@property (retain, nonatomic) MPVolumeView *airplayButton;

@property (retain, nonatomic) UISlider *progressBar;
@property (retain, nonatomic) UISlider *volumeBar;

@property (retain, nonatomic) UILabel *playBackTime;
@property (retain, nonatomic) UILabel *playBackTotalTime;

@property (retain, nonatomic) UIView *overLayView;
@property (retain, nonatomic) UIActivityIndicatorView *aindView;

@property (retain,nonatomic) UIView *playerHudCenter;
@property (retain,nonatomic) UIView *playerHudBottom;

- (id)initWithFrame:(CGRect)frame contentURL:(NSURL*)contentURL AndwithSuccessionBlock:(void(^)(id response))successBlock;
-(id)initWithFrame:(CGRect)frame contentURL:(NSURL*)contentURL;
-(id)initWithFrame:(CGRect)frame playerItem:(AVPlayerItem*)playerItem;
-(void)play;
-(void)pause;
-(void)stop;

-(void)setVolumeMute:(BOOL)mute;
-(void)setCloseButtonHidden:(BOOL)ahidden;
-(void)setSmallVolumeButtonHidden:(BOOL)ahidden;
-(void)setPlayButtonHidden:(BOOL)ahidden;




@end

