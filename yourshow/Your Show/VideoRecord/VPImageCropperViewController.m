//
//  VPImageCropperViewController.m
//  VPolor

//  Copyright (c) 2013 Huang Vinson. All rights reserved.
//

#define TitleWidth 200

#import "VPImageCropperViewController.h"
#import "AddMintViewController.h"
#import "AppDelegate.h"

//#import "ILTranslucentView.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define SCALE_FRAME_Y 70.0f //100.f
#define BOUNDCE_DURATION 0.3f




static const CGFloat kCLImageToolAnimationDuration = 0.3;


@interface CLRotatePanel : UIView
@property(nonatomic, strong) UIColor *bgColor;
@property(nonatomic, strong) UIColor *gridColor;
@property(nonatomic, assign) CGRect gridRect;
- (id)initWithSuperview:(UIView*)superview frame:(CGRect)frame;
@end




@interface VPImageCropperViewController ()

{
    CGRect _initialRect;
    UIImageView *_rotateImageView;
    CGFloat rotatevalue;CLRotatePanel *_gridView;
    
}

@property (nonatomic, retain) UIImage *originalImage;
@property (nonatomic, retain) UIImage *editedImage;

@property (nonatomic, retain) UIImageView *showImgView;
@property (nonatomic, retain) UIView *overlayView;
@property (nonatomic, retain) UIView *ratioView;

@property (nonatomic, assign) CGRect oldFrame;
@property (nonatomic, assign) CGRect largeFrame;
@property (nonatomic, assign) CGFloat limitRatio;

@property (nonatomic, assign) CGRect latestFrame;

@end

@implementation VPImageCropperViewController

- (void)dealloc {
    self.originalImage = nil;
    self.showImgView = nil;
    self.editedImage = nil;
    self.overlayView = nil;
    self.ratioView = nil;
}

- (id)initWithImage:(UIImage *)originalImage cropFrame:(CGRect)cropFrame limitScaleRatio:(NSInteger)limitRatio {
    self = [super init];
    if (self) {
        self.cropFrame = cropFrame;
        self.limitRatio = limitRatio;
        self.originalImage = [self fixOrientation:originalImage];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    [self initControlBtn];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}
- (UIImage*)resizeImage:(UIImage*)image withWidth:(int)width withHeight:(int)height
{
    CGSize newSize = CGSizeMake(width, height);
    float widthRatio = newSize.width/image.size.width;
    float heightRatio = newSize.height/image.size.height;
    
    if(widthRatio > heightRatio)
    {
        newSize=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    }
    else
    {
        newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)resetImageViewFrame
{
    CGSize size = (self.showImgView.image) ? self.showImgView.image.size : self.showImgView.frame.size;
    if(size.width>0 && size.height>0){
        CGFloat ratio = MIN(self.view.frame.size.width / size.width, self.view.frame.size.height / size.height);
        CGFloat W = ratio * size.width * 1.0;
        W = (W >300)?300:ratio * size.width * 1.0;
        
        CGFloat H = ratio * size.height * 1.0;
        if (H > 420) {
            H =420;
        }
          if (isSmallPad) {
               self.showImgView.frame = CGRectMake(2, 52, self.view.frame.size.width-4, self.view.frame.size.height-135);
          }
        else
        self.showImgView.frame = CGRectMake(MAX(0, (self.view.frame.size.width-W)/2), MAX(0, (self.view.frame.size.height-H)/2), W, H);
        
        NSLog(@"Updated x %f",self.showImgView.frame.origin.x);
        NSLog(@"Updated Y = %f",self.showImgView.frame.origin.y);
        NSLog(@"Updated HI = %f",self.showImgView.frame.size.width);
        NSLog(@"Updated WI = %f",self.showImgView.frame.size.height);
    }
}


- (void)initView {
    //////
    self.view.backgroundColor = [UIColor colorWithRed:23.0/255 green:43./255 blue:91./255 alpha:1.0];
    
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
   // back.alpha = .5f;
    back.backgroundColor        = [UIColor colorWithRed:23.0/255 green:43./255 blue:91./255 alpha:1.0];
    back.userInteractionEnabled = NO;
    back.layer.cornerRadius     = 4.0f;
    back.layer.masksToBounds    = YES;
    back.autoresizingMask       = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:back];
    
    UIView *headerView               = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    headerView.backgroundColor =  [UIColor colorWithRed:23.0/255 green:43./255 blue:91./255 alpha:1.0];
    
    
    UIView *bottomView               = [[UIView alloc]initWithFrame:CGRectMake(0, back.frame.size.height-80, self.view.frame.size.width, 80)];
    bottomView.backgroundColor =  [UIColor colorWithRed:23.0/255 green:43./255 blue:91./255 alpha:1.0];

    
    UIView *whiteView               = [[UIView alloc]initWithFrame:CGRectMake(0, 50, back.frame.size.width, back.frame.size.height-120)];
    whiteView.backgroundColor = [UIColor blackColor];
    //whiteView.layer.cornerRadius = 3.0f;
    whiteView.layer.masksToBounds   = YES;
    [back addSubview:whiteView];
    
   
    self.showImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 60,  back.frame.size.width, 200)];//320,480
    [self.showImgView setMultipleTouchEnabled:YES];
    [self.showImgView setUserInteractionEnabled:YES];
    [self.showImgView setImage:self.originalImage];
    [self.showImgView setUserInteractionEnabled:YES];
    [self.showImgView setMultipleTouchEnabled:YES];
    [self resetImageViewFrame ];
    [back addSubview:self.showImgView];
    [self.view addSubview:headerView];
    [self.view addSubview:bottomView];

    
    rotatevalue = 0.000000;
    _initialRect = self.showImgView.frame;
    _gridView = [[CLRotatePanel alloc] initWithSuperview:self.showImgView.superview frame:self.showImgView.frame];
    _gridView.backgroundColor = [UIColor clearColor];
    _gridView.bgColor = [UIColor clearColor];
    _gridView.gridColor = ORANGECOLOR;
    _gridView.clipsToBounds = NO;
    
    //    _gridView.backgroundColor = [UIColor redColor];
    
    
    
    [UIView animateWithDuration:kCLImageToolAnimationDuration
                     animations:^{
                         // _menuScroll.transform = CGAffineTransformIdentity;
                     }
                     completion:^(BOOL finished) {
                         
                         _rotateImageView        = [[UIImageView alloc] initWithFrame:_initialRect];
                         _rotateImageView.image  = self.showImgView.image;
                         [_gridView.superview insertSubview:_rotateImageView belowSubview:_gridView];
                       self.showImgView.hidden = YES;
                         
                     }];
    
    //self.showImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 60, whiteView.frame.size.width, 480)];//320,480
    [self.showImgView setMultipleTouchEnabled:YES];
    [self.showImgView setUserInteractionEnabled:YES];
    [self.showImgView setImage:self.originalImage];
    [self.showImgView setUserInteractionEnabled:YES];
    [self.showImgView setMultipleTouchEnabled:YES];
    
    // scale to fit the screen
    CGFloat oriWidth    = self.cropFrame.size.width;
    CGFloat oriHeight   = self.originalImage.size.height * (oriWidth / self.originalImage.size.width);
    CGFloat oriX        = self.cropFrame.origin.x + (self.cropFrame.size.width - oriWidth) / 2;
    CGFloat oriY        = self.cropFrame.origin.y + (self.cropFrame.size.height - oriHeight) / 2;
    self.oldFrame       = CGRectMake(oriX, oriY, oriWidth, oriHeight);
    self.latestFrame    = self.oldFrame;
    self.largeFrame     = CGRectMake(0, 0, self.limitRatio * self.oldFrame.size.width, self.limitRatio * self.oldFrame.size.height);
    self.showImgView.frame = self.oldFrame;
    
    [self addGestureRecognizers];
    //[whiteView addSubview:self.showImgView];
    
    self.overlayView        = [[UIView alloc] initWithFrame:self.view.bounds];
    self.overlayView.alpha  = 0.2f;
    self.overlayView.backgroundColor        = [UIColor blackColor];
    self.overlayView.userInteractionEnabled = NO;
    self.overlayView.autoresizingMask       = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
   // [whiteView addSubview:self.overlayView];
    
    self.ratioView = [[UIView alloc] initWithFrame:self.cropFrame];
    self.ratioView.layer.borderColor = [UIColor clearColor].CGColor;//yellow color
    self.ratioView.layer.borderWidth = 1.0f;
    self.ratioView.autoresizingMask  = UIViewAutoresizingNone;
   // [whiteView addSubview:self.ratioView];
    
    [self overlayClipping];
}

- (void)initControlBtn {
    
    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 50, 40)];//self.view.frame.size.height - 50.0f
    
    UIImageView *stretchableButtonImage = [[UIImageView alloc]initWithFrame:CGRectMake(cancelBtn.frame.origin.x +14, 20, 16, 18)];
    [stretchableButtonImage setImage:[UIImage imageNamed:@"close_Icon.png"]];
    [cancelBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:stretchableButtonImage];
    [self.view addSubview:cancelBtn];
    
    UILabel *lbl        = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width-TitleWidth)/2, 15, TitleWidth, 30)];
    lbl.text            = @"Edit Photo";
    lbl.textAlignment   = NSTextAlignmentCenter;
    lbl.textColor       = [UIColor whiteColor];
    lbl.font            = [UIFont boldSystemFontOfSize:16.0f];
    [self.view addSubview:lbl];

    UIButton *confirmBtn            = [UIButton buttonWithType:UIButtonTypeCustom];//self.view.frame.size.height - 420.0f
    [confirmBtn setFrame:CGRectMake(10, self.view.frame.size.height-60, 70, 30)];
    confirmBtn.backgroundColor      = [UIColor clearColor];
    confirmBtn.titleLabel.textColor = [UIColor orangeColor];
    [confirmBtn setTitle:@"Retake" forState:UIControlStateNormal];
    [confirmBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [confirmBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    confirmBtn.titleLabel.textColor = [UIColor orangeColor];
    [confirmBtn.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [confirmBtn.titleLabel setNumberOfLines:0];
    confirmBtn.tag=111;

    [confirmBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f)];
    confirmBtn.layer.cornerRadius   =   4.0f;
    confirmBtn.layer.masksToBounds  =   YES;
    [confirmBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:confirmBtn];
    
    // Rotate Iamge
    
    UIButton *rotateBtn       = [UIButton buttonWithType:UIButtonTypeCustom];//self.view.frame.size.height - 420.0f
    [rotateBtn setFrame:CGRectMake(self.view.frame.size.width/2-20, self.view.frame.size.height-60, 35, 35)];
    rotateBtn.backgroundColor = [UIColor clearColor];
    [rotateBtn setImage:[UIImage imageNamed:@"rotateImage"]  forState:UIControlStateNormal];
       [rotateBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f)];
    rotateBtn.layer.cornerRadius  =   4.0f;
    rotateBtn.layer.masksToBounds =   YES;
    [rotateBtn addTarget:self action:@selector(btnRotate:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rotateBtn];
    
    
    
    UIButton *nextBtn       = [UIButton buttonWithType:UIButtonTypeCustom];//self.view.frame.size.height - 420.0f
    [nextBtn setFrame:CGRectMake(self.view.frame.size.width-100, self.view.frame.size.height-60, 100, 30)];
    nextBtn.backgroundColor = [UIColor clearColor];
    [nextBtn setTitle:@"Use Photo" forState:UIControlStateNormal];
    [nextBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [nextBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [nextBtn.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [nextBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f)];
    nextBtn.layer.cornerRadius  =   4.0f;
    nextBtn.layer.masksToBounds =   YES;
    [nextBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextBtn];
//
//    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(20, 80, 290, 1)];
//    [line setBackgroundColor:[UIColor lightGrayColor]];
//    [self.view addSubview:line];
    
}


- (void)cancel:(id)sender {
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(VPImageCropperDelegate)])
    {
        [self.delegate imageCropperDidCancel:self];
        [self dismissViewControllerAnimated:NO completion:^{
            UIButton *btn = sender;
            if(btn.tag==111)
            {
                if([self.delegate respondsToSelector:@selector(GalleryClicked)])
                    [self.delegate GalleryClicked];
            }
        }];
    }
}

- (void)confirm:(id)sender {
    [_gridView addSubview:_rotateImageView];
    _gridView.gridColor = [UIColor clearColor];

//    

    
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(VPImageCropperDelegate)]) {
        [self.delegate imageCropper:self didFinished:[self captureView:_gridView]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    

    
    
}
-(void)setval : (UIImage *)image
{
   // self.showImgView.hidden = NO;
    self.showImgView.image = image;
}


- (UIImage*)captureView:(UIView *)yourView {
//    CGRect rect = [[UIScreen mainScreen] bounds];
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    [yourView.layer renderInContext:context];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGRect rect = _rotateImageView.frame;
    //CGRectMake(10, 20, 305, 185);
    CGImageRef imageRef = CGImageCreateWithImageInRect([viewImage CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    //UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
    CGImageRelease(imageRef);
    return (img);
}

- (void)overlayClipping
{
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    CGMutablePathRef path = CGPathCreateMutable();
    // Left side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(0, 0,
                                        self.ratioView.frame.origin.x,
                                        self.overlayView.frame.size.height));
    // Right side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(
                                        self.ratioView.frame.origin.x + self.ratioView.frame.size.width,
                                        0,
                                        self.overlayView.frame.size.width - self.ratioView.frame.origin.x - self.ratioView.frame.size.width,
                                        self.overlayView.frame.size.height));
    // Top side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(0, 0,
                                        self.overlayView.frame.size.width,
                                        self.ratioView.frame.origin.y));
    // Bottom side of the ratio view
    CGPathAddRect(path, nil, CGRectMake(0,
                                        self.ratioView.frame.origin.y + self.ratioView.frame.size.height,
                                        self.overlayView.frame.size.width,
                                        self.overlayView.frame.size.height - self.ratioView.frame.origin.y + self.ratioView.frame.size.height));
    maskLayer.path = path;
    self.overlayView.layer.mask = maskLayer;
    CGPathRelease(path);
}

// register all gestures
- (void) addGestureRecognizers
{
    // add pinch gesture 
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchView:)];
    [self.view addGestureRecognizer:pinchGestureRecognizer];
    
    // add pan gesture
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
    [self.view addGestureRecognizer:panGestureRecognizer];
}

// pinch gesture handler
- (void) pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    UIView *view = self.showImgView;
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan || pinchGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformScale(view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
        pinchGestureRecognizer.scale = 1;
    }
    else if (pinchGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGRect newFrame = self.showImgView.frame;
        newFrame = [self handleScaleOverflow:newFrame];
        newFrame = [self handleBorderOverflow:newFrame];
        [UIView animateWithDuration:BOUNDCE_DURATION animations:^{
            self.showImgView.frame = newFrame;
            self.latestFrame = newFrame;
        }];
    }
}

// pan gesture handler
- (void) panView:(UIPanGestureRecognizer *)panGestureRecognizer
{
    UIView *view = self.showImgView;
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan || panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        // calculate accelerator
        CGFloat absCenterX = self.cropFrame.origin.x + self.cropFrame.size.width / 2;
        CGFloat absCenterY = self.cropFrame.origin.y + self.cropFrame.size.height / 2;
        CGFloat scaleRatio = self.showImgView.frame.size.width / self.cropFrame.size.width;
        CGFloat acceleratorX = 1 - ABS(absCenterX - view.center.x) / (scaleRatio * absCenterX);
        CGFloat acceleratorY = 1 - ABS(absCenterY - view.center.y) / (scaleRatio * absCenterY);
        CGPoint translation = [panGestureRecognizer translationInView:view.superview];
        [view setCenter:(CGPoint){view.center.x + translation.x * acceleratorX, view.center.y + translation.y * acceleratorY}];
        [panGestureRecognizer setTranslation:CGPointZero inView:view.superview];
    }
    else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        // bounce to original frame
        CGRect newFrame = self.showImgView.frame;
        newFrame = [self handleBorderOverflow:newFrame];
        [UIView animateWithDuration:BOUNDCE_DURATION animations:^{
            self.showImgView.frame = newFrame;
            self.latestFrame = newFrame;
        }];
        
    }
}

- (CGRect)handleScaleOverflow:(CGRect)newFrame {
    // bounce to original frame
    CGPoint oriCenter = CGPointMake(newFrame.origin.x + newFrame.size.width/2, newFrame.origin.y + newFrame.size.height/2);
    if (newFrame.size.width < self.oldFrame.size.width) {
        newFrame = self.oldFrame;
    }
    if (newFrame.size.width > self.largeFrame.size.width) {
        newFrame = self.largeFrame;
    }
    newFrame.origin.x = oriCenter.x - newFrame.size.width/2;
    newFrame.origin.y = oriCenter.y - newFrame.size.height/2;
    return newFrame;
}

- (CGRect)handleBorderOverflow:(CGRect)newFrame {
    // horizontally
    if (newFrame.origin.x > self.cropFrame.origin.x) newFrame.origin.x = self.cropFrame.origin.x;
    if (CGRectGetMaxX(newFrame) < self.cropFrame.size.width) newFrame.origin.x = self.cropFrame.size.width - newFrame.size.width;
    // vertically
    if (newFrame.origin.y > self.cropFrame.origin.y) newFrame.origin.y = self.cropFrame.origin.y;
    if (CGRectGetMaxY(newFrame) < self.cropFrame.origin.y + self.cropFrame.size.height) {
        newFrame.origin.y = self.cropFrame.origin.y + self.cropFrame.size.height - newFrame.size.height;
    }
    // adapt horizontally rectangle
    if (self.showImgView.frame.size.width > self.showImgView.frame.size.height && newFrame.size.height <= self.cropFrame.size.height) {
        newFrame.origin.y = self.cropFrame.origin.y + (self.cropFrame.size.height - newFrame.size.height) / 2;
    }
    return newFrame;
}

-(UIImage *)getSubImage{
    CGRect squareFrame = self.cropFrame;
    CGFloat scaleRatio = self.latestFrame.size.width / self.originalImage.size.width;
    CGFloat x = (squareFrame.origin.x - self.latestFrame.origin.x) / scaleRatio;
    CGFloat y = (squareFrame.origin.y - self.latestFrame.origin.y) / scaleRatio;
    CGFloat w = squareFrame.size.width / scaleRatio;
    CGFloat h = squareFrame.size.height / scaleRatio;
    if (self.latestFrame.size.width < self.cropFrame.size.width) {
        CGFloat newW = self.originalImage.size.width;
        CGFloat newH = newW * (self.cropFrame.size.height / self.cropFrame.size.width);
        x = 0; y = y + (h - newH) / 2;
        w = newH; h = newH;
    }
    if (self.latestFrame.size.height < self.cropFrame.size.height) {
        CGFloat newH = self.originalImage.size.height;
        CGFloat newW = newH * (self.cropFrame.size.width / self.cropFrame.size.height);
        x = x + (w - newW) / 2; y = 0;
        w = newH; h = newH;
    }
    CGRect myImageRect = CGRectMake(x, y, w, h);
    CGImageRef imageRef = self.originalImage.CGImage;
    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, myImageRect);
    CGSize size;
    size.width = myImageRect.size.width;
    size.height = myImageRect.size.height;
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, myImageRect, subImageRef);
    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
    CGImageRelease(subImageRef);
    UIGraphicsEndImageContext();
    return smallImage;
}

- (UIImage *)fixOrientation:(UIImage *)srcImg {
    if (srcImg.imageOrientation == UIImageOrientationUp) return srcImg;
    CGAffineTransform transform = CGAffineTransformIdentity;
    switch (srcImg.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, srcImg.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, srcImg.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (srcImg.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, srcImg.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    CGContextRef ctx = CGBitmapContextCreate(NULL, srcImg.size.width, srcImg.size.height,
                                             CGImageGetBitsPerComponent(srcImg.CGImage), 0,
                                             CGImageGetColorSpace(srcImg.CGImage),
                                             CGImageGetBitmapInfo(srcImg.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (srcImg.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,srcImg.size.height,srcImg.size.width), srcImg.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,srcImg.size.width,srcImg.size.height), srcImg.CGImage);
            break;
    }
    
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

- (IBAction)btnRotate:(id)sender
{
    _gridView.gridColor = [UIColor clearColor];

    CGFloat value = (int)floorf((rotatevalue + 1)*2) + 1;
    if(value>4){ value -= 4; }
    rotatevalue = value / 2 - 1;
    [UIView animateWithDuration:kCLImageToolAnimationDuration
                     animations:^{
                         // sender.view.alpha = 1;
                     }
     ];
    
    [UIView animateWithDuration:kCLImageToolAnimationDuration
                     animations:^{
                         [self rotateStateDidChange];
                     }
                     completion:^(BOOL finished) {
                         _gridView.hidden = NO;
                     }
     ];
}


-(CATransform3D)rotateFrame
{
    CATransform3D transform = [self rotateTransform:CATransform3DIdentity clockwise:YES];
    CGFloat arg = rotatevalue*M_PI;
    CGFloat Wnew = fabs(_initialRect.size.width * cos(arg)) + fabs(_initialRect.size.height * sin(arg));
    CGFloat Hnew = fabs(_initialRect.size.width * sin(arg)) + fabs(_initialRect.size.height * cos(arg));
    CGFloat Rw = _gridView.frame.size.width / Wnew;
    CGFloat Rh = _gridView.frame.size.height / Hnew;
    
    CGFloat scale = MIN(Rw, Rh) * 0.95;
    transform = CATransform3DScale(transform, scale, scale, 1);
    return transform;
}


- (void)rotateStateDidChange
{
    _rotateImageView.layer.transform = [self rotateFrame];
     NSLog(@"Updated Height %f",_rotateImageView.frame.size.height);
     NSLog(@"Updated Y = %f",_rotateImageView.frame.origin.y);

    
    if (_rotateImageView.frame.size.height > ((isSmallPad)?(self.view.frame.size.height-140):420)) {
        CGRect frame       = _rotateImageView.bounds;
        frame.origin.y = (_rotateImageView.frame.origin.y < 50)?55:_rotateImageView.frame.origin.y;
        frame.origin.x     = (isSmallPad)?(self.view.frame.origin.x):_rotateImageView.frame.origin.x;
        frame.size.height  = (isSmallPad)?(self.view.frame.size.height-140):420;
        frame.size.width   = (isSmallPad)?(self.view.frame.size.width):_rotateImageView.frame.size.width;
        _rotateImageView.frame = frame;
        _gridView.gridRect = frame;
    }
    else
    {
        _gridView.gridRect = _rotateImageView.frame;
    }
}


- (CATransform3D)rotateTransform:(CATransform3D)initialTransform clockwise:(BOOL)clockwise
{
    CGFloat arg = rotatevalue*M_PI;
    if(!clockwise){
        arg *= -1;
    }
    
    CATransform3D transform = initialTransform;
    transform = CATransform3DRotate(transform, arg, 0, 0, 1);
    transform = CATransform3DRotate(transform, 0, 0, 1, 0);
    transform = CATransform3DRotate(transform,0, 1, 0, 0);
    
    return transform;
}



@end






@implementation CLRotatePanel

- (id)initWithSuperview:(UIView*)superview frame:(CGRect)frame
{
    self = [super initWithFrame:superview.bounds];
    if(self){
        self.gridRect = frame;
        [superview addSubview:self];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect rct = self.gridRect;
    
    CGContextSetStrokeColorWithColor(context, self.gridColor.CGColor);
    CGContextStrokeRect(context, rct);
}

- (void)setGridRect:(CGRect)gridRect
{
    _gridRect = gridRect;
    [self setNeedsDisplay];
}

@end

