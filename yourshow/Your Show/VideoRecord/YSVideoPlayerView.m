//
//  YSVideoPlayerView.m
//  TableviewVideo
//
//  Created by Siba Prasad Hota on 17/10/15.
//  Copyright © 2015 Power Electronics. All rights reserved.
//

#import "YSVideoPlayerView.h"

@implementation YSVideoPlayerView
{
    id playbackObserver;
    AVPlayerLayer *playerLayer;
    BOOL viewIsShowing;
}

/*!
 This method initiates the player view with specific frame and initializes the AVPlayer and sets its Delegates.

 */

-(id)initWithFrame:(CGRect)frame playerItem:(AVPlayerItem*)playerItem
{
    self = [super initWithFrame:frame];
    if (self) {
        self.moviePlayer = [AVPlayer playerWithPlayerItem:playerItem];
        playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.moviePlayer];
        [playerLayer setFrame:frame];
        [self.moviePlayer seekToTime:kCMTimeZero];
        [self.layer addSublayer:playerLayer];
        self.contentURL = nil;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerFinishedPlaying) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        
        [self initializePlayer:frame];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame contentURL:(NSURL*)contentURL
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.overLayView = [[UIView alloc] initWithFrame:frame];
        self.overLayView.backgroundColor = [UIColor blackColor];
        [self addSubview:self.overLayView];
        self.overLayView.alpha = 1.0;
        
        self.aindView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.aindView.center = self.overLayView.center;
        [self addSubview:self.aindView];
        [self.aindView startAnimating];
        
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:contentURL];
        self.moviePlayer = [AVPlayer playerWithPlayerItem:playerItem];
        playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.moviePlayer];
        [playerLayer setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.moviePlayer seekToTime:kCMTimeZero];
        [self.layer addSublayer:playerLayer];
        self.contentURL = contentURL;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerFinishedPlaying) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(delayPlay) name:AVPlayerItemPlaybackStalledNotification object:playerItem];
        [self initializePlayer:frame];
    }
    return self;
}

-(void)delayPlay{
    NSLog(@"%@", @"AVPlayerItemPlaybackStalledNotification");
    [self performSelector:@selector(play) withObject:nil afterDelay:1.5];
}

/* Asset keys */
NSString * const kTracksKey = @"tracks";
NSString * const kPlayableKey = @"playable";

/* PlayerItem keys */
NSString * const kStatusKey         = @"status";
NSString * const kCurrentItemKey	= @"currentItem";

- (void)setPlayerURL:(NSURL*)URL {
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:URL options:nil];
    
    NSArray *requestedKeys = [NSArray arrayWithObjects:kTracksKey, kPlayableKey, nil];
    
    [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:
     ^{
         dispatch_async( dispatch_get_main_queue(),
                        ^{
                            [self prepareToPlayAsset:asset withKeys:requestedKeys];
                        });
     }];
}



- (void)prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys {
    for (NSString *thisKey in requestedKeys) {
        NSError *error = nil;
        AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
        if (keyStatus == AVKeyValueStatusFailed) {
            return;
        }
    }
    
    if (!asset.playable) {
        return;
    }
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:asset];
    self.moviePlayer = [AVPlayer playerWithPlayerItem:playerItem];
    [self.moviePlayer seekToTime:kCMTimeZero];
    playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerFinishedPlaying) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
}

- (id)initWithFrame:(CGRect)frame contentURL:(NSURL*)contentURL AndwithSuccessionBlock:(void(^)(id response))successBlock{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.overLayView = [[UIView alloc] initWithFrame:CGRectMake(0,0, frame.size.width, frame.size.height)];
        self.aindView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:contentURL options:nil];
        NSArray *requestedKeys = [NSArray arrayWithObjects:kTracksKey, kPlayableKey, nil];
        [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:
         ^{
             dispatch_async( dispatch_get_main_queue(),
                            ^{
              [self prepareToPlayAsset:asset withKeys:requestedKeys];
              self.overLayView.backgroundColor = [UIColor blackColor];
              // [self addSubview:self.overLayView];
              self.overLayView.alpha = 1.0;
              self.aindView.center = self.overLayView.center;
              [self addSubview:self.aindView];
              [self.aindView startAnimating];
              [playerLayer setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
              [self.layer addSublayer:playerLayer];
              // self.contentURL = contentURL;
              [self initializePlayer:frame];
              [self play];
              successBlock(@"Play Now");
            });
         }];
    }
    return self;
}



-(void) setFrame:(CGRect)frame{
    [super setFrame:frame];
    [playerLayer setFrame:frame];
}



-(void)initializePlayer:(CGRect)frame{
    int frameWidth =  frame.size.width;
    self.backgroundColor = [UIColor clearColor];
    viewIsShowing =  NO;
    [self.layer setMasksToBounds:YES];

    
    [self addSubview:[self getBGView:CGRectMake(0,frame.size.height-40, frameWidth, 40) AndInverted:NO]];
    [self addSubview:[self getBGView:CGRectMake(0,0,frameWidth,40) AndInverted:YES]];
    
    //Play Pause Button
    self.playPauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.playPauseButton.frame = CGRectMake(7,10,15, 15);
    [self.playPauseButton addTarget:self action:@selector(playButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.playPauseButton setSelected:NO];
    
    [self.playPauseButton setBackgroundImage:[UIImage imageNamed:@"playback_pause"] forState:UIControlStateSelected];
    [self.playPauseButton setBackgroundImage:[UIImage imageNamed:@"playback_play"] forState:UIControlStateNormal];
    self.playPauseButton.layer.opacity = 1.0;
    [self addSubview:self.playPauseButton];
    
    //Play Pause Button
    self.zoomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.zoomButton.frame = CGRectMake(frameWidth-20,175,15, 15);
    [self.zoomButton addTarget:self action:@selector(zoomButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.zoomButton setSelected:NO];
    
    [self.zoomButton setBackgroundImage:[UIImage imageNamed:@"zoomout"] forState:UIControlStateSelected];
    [self.zoomButton setBackgroundImage:[UIImage imageNamed:@"zoomin"] forState:UIControlStateNormal];
    [self.zoomButton setTintColor:[UIColor clearColor]];
    self.zoomButton.layer.opacity = 1.0;
    // [self addSubview:self.zoomButton];
    
    //Play Pause Button
    self.volumeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.volumeButton.frame = CGRectMake(32,8,22, 22);
    [self.volumeButton addTarget:self action:@selector(volumeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.volumeButton setSelected:YES];
    
    [self.volumeButton setBackgroundImage:[UIImage imageNamed:@"Mute_video"] forState:UIControlStateSelected];
    [self.volumeButton setBackgroundImage:[UIImage imageNamed:@"UnMute_video"] forState:UIControlStateNormal];
    [self.volumeButton setTintColor:[UIColor clearColor]];
    self.volumeButton.layer.opacity = 1.0;
    [self addSubview:self.volumeButton];
    
    
    //Play Pause Button
    self.bigvolumeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.bigvolumeButton.frame = CGRectMake(0,65,self.frame.size.width,self.frame.size.width-80);
    [self.bigvolumeButton addTarget:self action:@selector(volumeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.bigvolumeButton setSelected:YES];
    [self.bigvolumeButton setTintColor:[UIColor clearColor]];
    self.bigvolumeButton.layer.opacity = 1.0;
    [self addSubview:self.bigvolumeButton];

    
    //Play Pause Button
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.frame = CGRectMake(frameWidth-25,5,20, 20);
    [self.closeButton addTarget:self action:@selector(StopButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton setSelected:YES];
    
    [self.closeButton setBackgroundImage:[UIImage imageNamed:@"video_close_btn"] forState:UIControlStateNormal];
    [self.closeButton setTintColor:[UIColor clearColor]];
    self.closeButton.layer.opacity = 1.0;
    [self addSubview:self.closeButton];
    
    //video_close_btn
    //Seek Time Progress Bar
    UIImage *thumbImage = [UIImage imageNamed:@"SliderAppearance.png"];
    self.progressBar = [[UISlider alloc] init];

    [self.progressBar setThumbImage:thumbImage forState:UIControlStateNormal];
//    [[UISlider appearance] setThumbImage:thumbImage
//                                forState:UIControlStateNormal];
    self.progressBar.frame = CGRectMake( 50 ,frame.size.height-25, frameWidth- 110, 25);
    [self.progressBar addTarget:self action:@selector(progressBarChanged:) forControlEvents:UIControlEventValueChanged];
    [self.progressBar addTarget:self action:@selector(proressBarChangeEnded:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.progressBar setThumbImage:[UIImage imageNamed:@"Slider_button"] forState:UIControlStateNormal];
    [self addSubview:self.progressBar];
    
    //Current Time Label
    self.playBackTime = [[UILabel alloc] initWithFrame:CGRectMake(5,frame.size.height-25, 60, 20)];
    self.playBackTime.text = [self getStringFromCMTime:self.moviePlayer.currentTime];
    [self.playBackTime setTextAlignment:NSTextAlignmentLeft];
    [self.playBackTime setTextColor:[UIColor whiteColor]];
    self.playBackTime.font = [UIFont systemFontOfSize:12.0];
    [self addSubview:self.playBackTime];
    
    //Total Time label
    self.playBackTotalTime = [[UILabel alloc] initWithFrame:CGRectMake(frameWidth-70,frame.size.height-25, 60, 20)];
    self.playBackTotalTime.text = [self getStringFromCMTime:self.moviePlayer.currentItem.asset.duration];
    [self.playBackTotalTime setTextAlignment:NSTextAlignmentRight];
    [self.playBackTotalTime setTextColor:[UIColor whiteColor]];
    self.playBackTotalTime.font = [UIFont systemFontOfSize:12.0];
    [self addSubview:self.playBackTotalTime];
    
    CMTime interval = CMTimeMake(33, 1000);
    __weak __typeof(self) weakself = self;
    playbackObserver = [self.moviePlayer addPeriodicTimeObserverForInterval:interval queue:dispatch_get_main_queue() usingBlock: ^(CMTime time) {
        CMTime endTime = CMTimeConvertScale (weakself.moviePlayer.currentItem.asset.duration, weakself.moviePlayer.currentTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
        if (CMTimeCompare(endTime, kCMTimeZero) != 0) {
            double normalizedTime = (double) weakself.moviePlayer.currentTime.value / (double) endTime.value;
            weakself.progressBar.value = normalizedTime;
        }
        weakself.playBackTime.text = [weakself getStringFromCMTime:weakself.moviePlayer.currentTime];
        
    }];
    
    
}

-(NSString*)getStringFromCMTime:(CMTime)time
{
    Float64 currentSeconds = CMTimeGetSeconds(time);
    int mins = currentSeconds/60.0;
    int secs = fmodf(currentSeconds, 60.0);
    NSString *minsString = mins < 10 ? [NSString stringWithFormat:@"0%d", mins] : [NSString stringWithFormat:@"%d", mins];
    NSString *secsString = secs < 10 ? [NSString stringWithFormat:@"0%d", secs] : [NSString stringWithFormat:@"%d", secs];
    return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
}


-(UIView*)getBGView:(CGRect)frame AndInverted:(BOOL)isInverted{
    UIView *bgView = [[UIView alloc] init];
    bgView.frame = frame;
    bgView.backgroundColor = [UIColor blackColor];
    
    // Create the colors for our gradient.
    UIColor *transparent = [UIColor colorWithWhite:1.0f alpha:0.f];
    UIColor *opaque = [UIColor colorWithWhite:1.0f alpha:1.0f];
    
    // Create a masklayer.
    CALayer *maskLayer = [[CALayer alloc]init];
    maskLayer.frame = bgView.bounds;
    CAGradientLayer *gradientLayer = [[CAGradientLayer alloc]init];
    gradientLayer.frame = CGRectMake(0,0,bgView.bounds.size.width, bgView.bounds.size.height);
    gradientLayer.colors = @[(id)transparent.CGColor, (id)transparent.CGColor, (id)opaque.CGColor, (id)opaque.CGColor];
    if (isInverted) {
          gradientLayer.colors = @[(id)opaque.CGColor, (id)opaque.CGColor, (id)transparent.CGColor, (id)transparent.CGColor];
    }
    gradientLayer.locations = @[@0.0f, @0.09f, @0.8f, @1.0f];
    // Add the mask.
    [maskLayer addSublayer:gradientLayer];
    bgView.layer.mask = maskLayer;

    return bgView;
}


-(void)setCloseButtonHidden:(BOOL)ahidden{
    self.closeButton.hidden = ahidden;
}

-(void)setSmallVolumeButtonHidden:(BOOL)ahidden{
    self.volumeButton.hidden = ahidden;
}

-(void)setPlayButtonHidden:(BOOL)ahidden{
    self.playPauseButton.hidden = ahidden;
}

//zoomin
-(void)zoomButtonPressed:(UIButton*)sender
{
    if (!self.isFullScreenMode) {
        self.isFullScreenMode = YES;
        [self.zoomButton setSelected:YES];
    }
    else  {
        self.isFullScreenMode = NO;
        [self.zoomButton setSelected:NO];
    }
    if ([self.ysdelegate respondsToSelector:@selector(ysPlayerViewZoomButtonClicked: isFullScreenMode:)])
        [self.ysdelegate ysPlayerViewZoomButtonClicked:self isFullScreenMode:self.isFullScreenMode];
}


-(void)playerFinishedPlaying{
    [self.moviePlayer pause];
    [self.moviePlayer seekToTime:kCMTimeZero];
    [self.playPauseButton setSelected:NO];
    self.isPlaying = NO;
    
    if ([self.ysdelegate respondsToSelector:@selector(ysPlayerFinishedPlayback:)]) {
        [self.ysdelegate ysPlayerFinishedPlayback:self];
    }
}

-(void)StopButtonAction:(UIButton*)sender{
    [self playerFinishedPlaying];
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint point = [(UITouch*)[touches anyObject] locationInView:self];
    if (CGRectContainsPoint(playerLayer.frame, point)) {
        [self showHud:!viewIsShowing];
    }
}

-(void) showHud:(BOOL)show{
}

-(void)muteTheVolume{
    [self.moviePlayer setMuted:YES];
    [self.volumeButton setSelected:NO];
    [self.bigvolumeButton setSelected:NO];
}

-(void)UnmuteTheVolume{
    [self.moviePlayer setMuted:NO];
    [self.volumeButton setSelected:YES];
    [self.bigvolumeButton setSelected:YES];
}


-(void)setVolumeMute:(BOOL)mute{
    if (mute) {
        [self muteTheVolume];
    }
    else {
        [self UnmuteTheVolume];
    }
}


-(void)volumeButtonPressed:(UIButton*)sender{
    if (sender.isSelected) {
        [self muteTheVolume];
    } else {
        [self UnmuteTheVolume];
    }
}

-(void)playButtonAction:(UIButton*)sender{
    if (self.isPlaying) {
        [self pause];
    } else {
        [self play];
    }
}

-(void)progressBarChanged:(UISlider*)sender{
    if (self.isPlaying) {
        [self.moviePlayer pause];
    }
    CMTime seekTime = CMTimeMakeWithSeconds(sender.value * (double)self.moviePlayer.currentItem.asset.duration.value/(double)self.moviePlayer.currentItem.asset.duration.timescale, self.moviePlayer.currentTime.timescale);
    [self.moviePlayer seekToTime:seekTime];
}

-(void)proressBarChangeEnded:(UISlider*)sender{
    if (self.isPlaying) {
        [self.moviePlayer play];
    }
}

-(void)volumeBarChanged:(UISlider*)sender{
    [self.moviePlayer setVolume:sender.value];
}

-(void)play{
    [self.moviePlayer play];
    self.isPlaying = YES;
    [self.playPauseButton setSelected:YES];

}

-(void)pause{
    [self.moviePlayer pause];
    self.isPlaying = NO;
    [self.playPauseButton setSelected:NO];
}


-(void)stop{
    [self.moviePlayer pause];
    self.isPlaying = NO;
    [self.playPauseButton setSelected:NO];
}
-(void)dealloc{
    [self.moviePlayer removeTimeObserver:playbackObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
