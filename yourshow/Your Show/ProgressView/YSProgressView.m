//
//  YSProgressView.m
//  ProgressViewDemo
//
//  Created by Siba Prasad Hota on 01/04/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import "YSProgressView.h"
#import "YSShapeLayer.h"

@interface YSProgressView ()

@property (nonatomic, strong) YSShapeLayer *progressLayer;

@end


@implementation YSProgressView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib {
    [self setupViews];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.progressLayer.frame = CGRectMake(3, 3, CGRectGetWidth(self.bounds)-6, CGRectGetHeight(self.bounds)-6);
}

- (void)updateConstraints {
    [super updateConstraints];
}


#pragma mark - Private Methods

- (void)setupViews {
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = false;
    
    //add Progress layer
    self.progressLayer = [[YSShapeLayer alloc] init];
    self.progressLayer.backgroundColor = [UIColor clearColor].CGColor;
    self.progressLayer.frame = CGRectMake(3, 3, CGRectGetWidth(self.bounds)-6, CGRectGetHeight(self.bounds)-6);

    [self.layer addSublayer:self.progressLayer];
    self.progressLayer.timeLimit = 1.0;
    self.progressLayer.elapsedTime = 0.0;
}

- (void)setProgress:(float)progress animated:(BOOL)animated{
    self.progressLayer.elapsedTime = progress;
}

@end
