//
//  YSShapeLayer.h
//  ProgressViewDemo
//
//  Created by Siba Prasad Hota on 01/04/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>



@interface YSShapeLayer : CAShapeLayer

@property (nonatomic) NSTimeInterval elapsedTime;
@property (nonatomic) NSTimeInterval timeLimit;

@property (assign, nonatomic, readonly) double percent;

@property (nonatomic) UIColor *progressColor;

@end

