//
//  YSProgressView.h
//  ProgressViewDemo
//
//  Created by Siba Prasad Hota on 01/04/16.
//  Copyright © 2016 Sibahota. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface YSProgressView : UIView

@property(nonatomic) float progress;
- (void)setProgress:(float)progress animated:(BOOL)animated NS_AVAILABLE_IOS(5_0);


@end
