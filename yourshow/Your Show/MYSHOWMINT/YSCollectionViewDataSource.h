//
//  MultimintViewDataSource.h
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseFeedModel.h"

@class YSCollectionView;

@protocol YSCollectionViewDataSource <NSObject>

@optional

@required

- (BaseFeedModel *)multiMintCollectionViewView:(YSCollectionView *)collectionView DataForItemAtRow:(NSInteger)row;
- (NSInteger)NumberOFItemsForMultiMintView:(YSCollectionView *)collectionView forSection:(NSInteger)section;
- (NSInteger)NumberOFSectionsMultiMintView:(YSCollectionView *)collectionView;
- (BOOL)HorizontalScrollerSection:(NSInteger)section;

@end