//
//  YSMintCollectionViewCell.h
//  Your Show
//
//  Created by Siba Prasad Hota on 08/11/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseFeedModel.h"


typedef enum {
    YSButtonTypeHifive,
    YSButtonTypeHifiveCount,
    YSButtonTypeComplimint,
    YSButtonTypeInspire,
    YSButtonTypeRemint,
    YSButtonTypeUserImage,
    YSButtonTypeRemintUserImage,
    YSButtonTypedetails
} YSButtonType;


@protocol mintCollectionDelegate;

@interface YSMintCollectionViewCell : UICollectionViewCell

@property (nonatomic,assign)id <mintCollectionDelegate> cellDelegate;
@property (weak, nonatomic) IBOutlet UIButton *buttonHighFive;
@property (weak, nonatomic) IBOutlet UIView *buttonView;

-(void)setDataForCollectionViewCellWithData:(BaseFeedModel*)feedData andUserID:(NSString*)userID AndIndex:(NSInteger)row;
-(void)setCustomDelegate:(id)sender;
-(void)setBackgroundImageForButton:(YSButtonType)actionButtonType AndValue:(NSString *)value withLabelValue:(NSString*)labelValue;

@end



@protocol mintCollectionDelegate
- (void)hashTagClicked :(EntityModel *)selectedEnitity;
- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row forCell:(YSMintCollectionViewCell*)cell;
- (void)SpecialFeatureWordclickedFortype:(int)type WithWord:(NSString *)string forRow:(NSInteger)row forCell:(YSMintCollectionViewCell*)cell withPrivateAccess:(BOOL)privateAccess;

@end
