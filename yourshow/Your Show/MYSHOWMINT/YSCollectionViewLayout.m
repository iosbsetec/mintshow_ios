//
//  ComicListFlowLayout.m
//  xkcd Open Source
//
//  Created by Mike on 5/17/15.
//  Copyright (c) 2015 Mike Amaral. All rights reserved.
//


#import "YSCollectionViewLayout.h"

@implementation YSCollectionViewLayout {
    NSMutableArray *_columns;
    NSMutableArray *_itemsAttributes;
}

#define   LEFT_SPACE    10.0
#define   RIGHT_SPACE   10.0
#define   MIDDLE_SPACE  10.0
#define   EXTRA_HEIGHT  15.0

- (void)prepareLayout {
    NSUInteger numberOfColumns = [self numberOfColumns];
    _columns = [NSMutableArray arrayWithCapacity:numberOfColumns];
    for (NSInteger i = 0; i < numberOfColumns; i++){
        [_columns addObject:@(0)];
    }
    NSUInteger itemsCount = [self.collectionView numberOfItemsInSection:0];
    _itemsAttributes = [NSMutableArray arrayWithCapacity:itemsCount];
    for (NSUInteger i = 0; i < itemsCount; i++){
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        NSUInteger columnIndex = [self indexForShortestColumn];
        CGFloat xOffset = columnIndex * [self columnWidth];
        CGFloat yOffset = [[_columns objectAtIndex:columnIndex] floatValue];
        CGSize relativeItemHeight = [self.delegate collectionView:self.collectionView relativeHeightForItemAtIndexPath:indexPath];
        NSUInteger itemWidth = [self columnWidth];
        NSUInteger itemHeight = relativeItemHeight.height+EXTRA_HEIGHT;
        _columns[columnIndex] = @(yOffset + itemHeight);
        
        xOffset = (xOffset>100)?itemWidth+LEFT_SPACE+MIDDLE_SPACE:xOffset+LEFT_SPACE;
        
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        
        if (itemWidth ==0 || itemHeight ==0 ) {
            attributes.frame = CGRectMake(xOffset, yOffset, itemWidth, itemHeight);
        }else{
            attributes.frame = CGRectMake(xOffset, yOffset, itemWidth, itemHeight);
        }
        [_itemsAttributes addObject:attributes];
    }
}
-(CALayer *)simpleLayer{
    CALayer *layer = [[CALayer alloc] init];
    [layer setFrame:CGRectMake(0, 0, 10, 10)];
    [layer setHidden:TRUE];
    return layer;
}


- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    NSPredicate *filterPredicate = [NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes *evaluatedObject, NSDictionary *bindings){
        return CGRectIntersectsRect(rect, evaluatedObject.frame);
    }];
    return [_itemsAttributes filteredArrayUsingPredicate:filterPredicate];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [_itemsAttributes objectAtIndex:indexPath.row];
}

- (CGSize)collectionViewContentSize {
    CGSize contentSize = self.collectionView.bounds.size;
    NSUInteger indexForLongestColumn = [self longestColumnIndex];
    float columnHeight = [_columns[indexForLongestColumn] floatValue];
    contentSize.height = columnHeight;
    return contentSize;
}

- (NSUInteger)indexForShortestColumn {
    CGFloat shortestHeight = MAXFLOAT;
    NSUInteger indexForShortest = 0;
    for (NSUInteger i = 0; i < _columns.count; i++) {
        CGFloat height = [_columns[i] floatValue];
        if (height < shortestHeight) {
            shortestHeight = height;
            indexForShortest = i;
        }
    }
    return indexForShortest;
}

- (NSUInteger)longestColumnIndex {
    CGFloat longestHeight = 0;
    NSUInteger indexForLongest = 0;
    for (NSUInteger i = 0; i < _columns.count; i++) {
        CGFloat height = [_columns[i] floatValue];
        if (height > longestHeight) {
            longestHeight = height;
            indexForLongest = i;
        }
    }
    return indexForLongest;
}

- (NSUInteger)numberOfColumns {
    return [self.delegate numberOfColumnsInCollectionView:self.collectionView];
}

- (CGFloat)columnWidth {
    return ((self.collectionView.frame.size.width) - (LEFT_SPACE+RIGHT_SPACE+MIDDLE_SPACE))/2.0;
}

@end
