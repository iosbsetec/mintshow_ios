//
//  YSMintCollectionViewCell.m
//  Your Show
//
//  Created by Siba Prasad Hota on 08/11/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//
#define MINTYPECOLOCR                [UIColor colorWithRed:41./255.0 green:172./255.0 blue:230.0/255. alpha:1.0]


#import "YSMintCollectionViewCell.h"
#import "YSCommentLabel.h"
#import "STTweetLabel.h"
#import "YSCommentUserLable.h"
#import "NSString+Emojize.h"
#import <CoreText/CoreText.h>

#define TXTMINTHEIGHT 50

@interface YSMintCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIView *baseBgView;
@property (weak, nonatomic) IBOutlet STTweetLabel *mintDescriptionlabel;
@property (weak, nonatomic) IBOutlet UIImageView *mintImageView;
@property (weak, nonatomic) IBOutlet UIImageView *playImageview;
@property (weak, nonatomic) IBOutlet UIButton *remintuserBtn;

// Below are for remint details view
@property (weak, nonatomic) IBOutlet UIView *remintContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *remint_UserImage;
@property (weak, nonatomic) IBOutlet UILabel *lblDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblRemintPostedTime;
@property (weak, nonatomic) IBOutlet STTweetLabel *remint_message;

// Below are for user details view
@property (weak, nonatomic) IBOutlet UIView *userDetailsView;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet YSCommentUserLable *lableUserName;
@property (weak, nonatomic) IBOutlet UIButton *btnUserImage;
@property (weak, nonatomic) IBOutlet UILabel *lablefeed_posted_time;
@property (weak, nonatomic) IBOutlet UILabel *lable_posted_time;


// Below are for Button view
@property (weak, nonatomic) IBOutlet UIButton *buttonComplimint;
@property (weak, nonatomic) IBOutlet UIButton *buttonRemint;
@property (weak, nonatomic) IBOutlet UIButton *buttonHighFiveCount;
@property (weak, nonatomic) IBOutlet UIImageView *highFiveImageView;


@property (weak, nonatomic) IBOutlet UILabel* labeHighFiveCount;
@property (weak, nonatomic) IBOutlet UILabel* labelComplimintCount ;
@property (weak, nonatomic) IBOutlet UILabel* rctLabelRemint ;

@end


@implementation YSMintCollectionViewCell

-(void)setCustomDelegate:(id)sender{
    self.cellDelegate = sender;
}
-(void)setDataForCollectionViewCellWithData:(BaseFeedModel*)feedData andUserID:(NSString*)userID AndIndex:(NSInteger)row{

    self.playImageview .alpha =0.0;

    CGRect frameRect = self.bounds;
    /*
     * Setup the Background View here
     */
    self.baseBgView.frame =CGRectMake(0, 0, frameRect.size.width, frameRect.size.height-15);
    self.baseBgView.backgroundColor     = [UIColor whiteColor];
    self.baseBgView.layer.cornerRadius  = 3;
    self.baseBgView.layer.masksToBounds = YES;
    

    
    CGFloat heightTobeadded = 0.0; // if remint view exist, this height will change
    
    /*
     * Check if Remint view required
     * Setup the Background View here
     */
    if (feedData.IsMintReminted){
        /*
         * Calculate the message height and width
         * set container view Frame
         */
        [self.remintuserBtn setTag:row ];

        // Changed by Mani
        
        CGRect recttxtLbl  = [[feedData.remint_message emojizedString]boundingRectWithSize:CGSizeMake(frameRect.size.width-8, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:9]} context:nil];
        heightTobeadded = 40+recttxtLbl.size.height;
        self.remintContainerView.frame = CGRectMake (0, 0,frameRect.size.width, heightTobeadded);
        self.remintContainerView.backgroundColor  = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1];
        self.remintContainerView.hidden = NO;

//         NSLog(@"MC : %f",frameRect.size.width-8);
//        self.remint_message.layer.borderColor = [UIColor redColor].CGColor;
//        self.remint_message.layer.borderWidth = 2.0;
        
        /*
         * Setup the user image View here with frame
         */
        self.remint_UserImage.frame = CGRectMake(5,5,24,24);
        self.remint_UserImage.backgroundColor       = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1];
        self.remint_UserImage.layer.cornerRadius    = 12.0f;
        self.remint_UserImage.image              = nil;
        self.remint_UserImage.mintImageURL              = [NSURL URLWithString:feedData.remint_userimg];
        [self.remint_UserImage setClipsToBounds:YES];
        
        /*
         * Setup the Remint time with user name
         */
        self.lblDetails.frame = CGRectMake(32,5,frameRect.size.width-40,17);
        self.lblDetails.numberOfLines                = 1;
        self.lblDetails.font                = [UIFont fontWithName:@"Helvetica-Bold" size:8.0f];
        self.lblDetails.attributedText = [self stringForRemintLabel:feedData];
        self.lblRemintPostedTime.frame = CGRectMake(32,19,frameRect.size.width-30,12);
        self.lblRemintPostedTime.font                = [UIFont fontWithName:@"Helvetica-Bold" size:8.0f];
        self.lblRemintPostedTime.textColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.5];
        self.lblRemintPostedTime.text = feedData.remint_postedTime;


        /*
         * Setup the Remint message here
         */
        self.remint_message.frame = CGRectMake(2,35,frameRect.size.width-5,recttxtLbl.size.height+3);
        [self.remint_message setBackgroundColor:[UIColor clearColor]];
        self.remint_message.text              =  [feedData.remint_message emojizedString];
        [self.remint_message setBackgroundColor:[UIColor clearColor]];
        self.remint_message.textAlignment = NSTextAlignmentLeft;
        self.remint_message.font=[UIFont fontWithName:@"HelveticaNeue" size:9];
        
        self.remint_message.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
        {
            BOOL isAccess = NO;
            int Typest = 0;
            if (hotWord == STTweetHashtag) {
                Typest = 0;
            } else if (hotWord == STTweetStartag)  {
                Typest = 1;
                for (NSDictionary *dictShowroomInfo in feedData.privateShowroomListArray) {
                    if (![dictShowroomInfo isKindOfClass:[NSDictionary class]]) {
                        continue;
                    }
                    if ([[dictShowroomInfo valueForKey:@"access_key"]  isEqualToString:@"no"] && [[dictShowroomInfo valueForKey:@"name"]  isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                        isAccess = YES;
                        break;
                    }
                }
            }  else if (hotWord == STTweetMention)  {
                Typest = 22;
            }
            [self.cellDelegate SpecialFeatureWordclickedFortype:Typest WithWord:string forRow:row forCell:self withPrivateAccess:isAccess];
        };
    }
    else
    {
        self.remintContainerView.frame = CGRectZero;
        self.remintContainerView.hidden = YES;
    }
    
    BOOL isTextMint = (feedData.mintType==MSMintTypeText)?YES:NO;
    
    NSString *OriginalFrontendText    =  [feedData.feed_description emojizedString];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:OriginalFrontendText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:12] }];
    
    CGRect boundingRect = [attributedString boundingRectWithSize:CGSizeMake(frameRect.size.width-5, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//   NSLog(@"MC : %f",frameRect.size.width-5);

    CGSize fitSize = boundingRect.size;
    CGFloat mintdescriptionHeight = fitSize.height+5;
    CGFloat mintImageHeight = 0.0;
    
    if (isTextMint)
    {
        int yPos = (boundingRect.size.height<TXTMINTHEIGHT)?((TXTMINTHEIGHT-boundingRect.size.height+8)/2):7;
        boundingRect.size.height = (boundingRect.size.height<TXTMINTHEIGHT)?TXTMINTHEIGHT:boundingRect.size.height;

        self.mintDescriptionlabel.frame = CGRectMake(2,heightTobeadded+yPos,frameRect.size.width-4, mintdescriptionHeight+5);
        self.mintImageView.frame = CGRectZero;
        self.mintImageView.hidden = YES;
    }
    else
    {
        /*
         * Calculate the Frame of Media (video,photo,youtube) here !!
         * Setup the Image view here
         * its Height and with is calculated already
         * setup Minimum And Maximum Width : 185.0:155.0
         * calculate the required Rect Size
         */
        CGSize rctSizeOriginal = CGSizeMake(feedData.feed_thumb_Width , feedData.feed_thumb_height);
        double scale = frameRect.size.width / rctSizeOriginal.width;
        CGSize rctSizeFinal = CGSizeMake(rctSizeOriginal.width * scale -10,rctSizeOriginal.height * scale -10);
        mintImageHeight =(rctSizeFinal.height<135)?135:rctSizeFinal.height;
        self.mintImageView.image=nil;
        self.mintImageView.hidden = NO;
        self.mintImageView.frame         = CGRectMake(0,heightTobeadded,frameRect.size.width,mintImageHeight);
        [self.mintImageView setBackgroundColor:[YSSupport randomColor]];
        //self.mintImageView.mintImageURL      = [NSURL URLWithString:feedData.feed_thumb_image_small];
        self.mintImageView.newimageURL          = [NSURL URLWithString:feedData.feed_thumb_image];
        if (feedData.mintType == MSMintTypeVideo || feedData.mintType == MSMintTypeInsthaGram || feedData.mintType == MSMintTypeYouTube) {
            [self.playImageview setCenter:self.mintImageView.center];
            self.playImageview .alpha =1.0;
        }
        
        self.mintDescriptionlabel.frame = CGRectMake(2,self.mintImageView.frame.origin.y+self.mintImageView.frame.size.height ,frameRect.size.width-4, mintdescriptionHeight);
    }
    
    self.mintDescriptionlabel.text               = [feedData.feed_description emojizedString];
    self.mintDescriptionlabel.textAlignment = NSTextAlignmentCenter;

    self.mintDescriptionlabel.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
    {
        BOOL isAccess = NO;
        int Typest = 0;
        if (hotWord == STTweetHashtag) {
            Typest = 0;
        } else if (hotWord == STTweetStartag)  {
            Typest = 1;
            for (NSDictionary *dictShowroomInfo in feedData.privateShowroomListArray) {
                if (![dictShowroomInfo isKindOfClass:[NSDictionary class]]) {
                    continue;
                }
                if ([[dictShowroomInfo valueForKey:@"access_key"]  isEqualToString:@"no"] && [[dictShowroomInfo valueForKey:@"name"]  isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                    isAccess = YES;
                    break;
                }
            }
        }  else if (hotWord == STTweetMention)  {
            Typest = 44;
        }
        [self.cellDelegate SpecialFeatureWordclickedFortype:Typest WithWord:string forRow:row forCell:self withPrivateAccess:isAccess];
    };
   
    self.mintDescriptionlabel.backgroundColor = [UIColor clearColor];
    /*
     * Setup the User view here
     * setup user name label which is customized.
     * calculate the required Rect Size
     */
    self.userDetailsView.frame = CGRectMake ( 2,frameRect.size.height-75,frameRect.size.width-4,60);
    self.userDetailsView. backgroundColor = [UIColor clearColor];
    self.userImage.frame = CGRectMake(1,2,26,26);
    self.userImage.backgroundColor       = [UIColor grayColor];
    self.userImage.layer.cornerRadius    = 13.0f;
    self.userImage.image      =nil;
    self.userImage.mintImageURL              = [NSURL URLWithString:feedData.feed_user_thumb_image];
    [self.userImage setClipsToBounds:YES];
    [self.btnUserImage setTag:row];
//    [self.userDetailsView setHidden:YES];
    self.lableUserName.frame             = CGRectMake(32,2,CGRectGetWidth(self.userDetailsView.frame)- 30,15);
    self.lableUserName.text              = [NSString stringWithFormat:@"√[%@](contact:%@)",feedData.feed_user_name ,feedData.feed_user_id] ;
    self.lableUserName.numberOfLines = 1;
    self.lable_posted_time.frame = CGRectMake(34,16,CGRectGetWidth(self.userDetailsView.frame)- 30,15);
    self.lable_posted_time.text = feedData.feed_posted_time;
//    NSMutableAttributedString *text =
//    [[NSMutableAttributedString alloc]
//     initWithString: [NSString stringWithFormat:@"%@ | %@",feedData.feed_posted_time,feedData.category_type]];
//    
//    [text addAttribute:NSForegroundColorAttributeName
//                 value:MINTYPECOLOCR
//                 range:NSMakeRange(feedData.feed_posted_time.length+3, feedData.category_type.length)];
//    
//     /*
//     @brief It Show the Posted time with the Mint type (Signature showroom).
//       - Added TapGuesture to Get the Mint type for Navigating to Signature Showroom page
//     @remark This is a super-easy method.
//     */
//        self.lable_posted_time.attributedText = text;
//        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnMintType:)];
//        // if labelView is not set userInteractionEnabled, you must do so
//        [self.lable_posted_time setUserInteractionEnabled:YES];
//        [self.lable_posted_time addGestureRecognizer:gesture];
        self.lable_posted_time.textAlignment = NSTextAlignmentLeft;
        self.lable_posted_time.tag = row;
        
    
    self.lableUserName.tag = [feedData.feed_user_id integerValue];
   
    self.lableUserName.detectionBlock = ^(EntityModel *hotWord, NSRange range)
    {
        if (hotWord.entityType == EntityTypeUserName)
        {
            [self.cellDelegate SpecialFeatureWordclickedFortype:2 WithWord:[NSString stringWithFormat:@"%d",(int)self.lableUserName.tag] forRow:row forCell:self withPrivateAccess:NO];
        }
    };
    
    //self.buttonView.frame = CGRectMake(0, self.userDetailsView.frame.origin.y+self.userDetailsView.frame.size.height, frameRect.size.width, 25) ;
   self.buttonView.backgroundColor = [UIColor clearColor];
    
   // [self.buttonHighFive setFrame:CGRectMake( 8,2,24,20)];
    [self.highFiveImageView setImage:[UIImage imageNamed:(feedData.is_user_liked)?@"HighFive_Icon_24x24":@"hive_it_normal"]];
    [self.buttonHighFive setTag:row];

    [self.buttonHighFive addTarget:self action:@selector(hifiveClicked:) forControlEvents:UIControlEventTouchUpInside];

//    if ([feedData.feed_like_count integerValue]>0)
//    {

        UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] init];
        [gr addTarget:self action:@selector(handleLongPress:)];
        [self.buttonHighFive addGestureRecognizer:gr];

   // }

   

    
  //  self.labeHighFiveCount.frame           = CGRectMake( 35 ,0,20,24);
    self.labeHighFiveCount.textColor         =  (feedData.is_user_liked)?ORANGECOLOR:[UIColor lightGrayColor];
    self.labeHighFiveCount.numberOfLines     = 0;
    self.labeHighFiveCount.font              = [UIFont fontWithName:@"Helvetica Bold" size:11.0];
    self.labeHighFiveCount.text              = [feedData feed_like_count];
    self.labeHighFiveCount.textAlignment     = NSTextAlignmentLeft;
    
    //[self.buttonComplimint setFrame:CGRectMake( deviderFrame ,2,24,20)];
    [self.buttonComplimint setImage:(feedData.is_user_commented)?[UIImage imageNamed:@"complimint_Selected"]:[UIImage imageNamed:@"Btn_CompliMint"] forState:UIControlStateNormal];

    [self.buttonComplimint addTarget:self action:@selector(gotoCommentScreen:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonComplimint setTag:row ];

    
    
   // self.labelComplimintCount.frame             = CGRectMake( deviderFrame ,0,40,24);
    self.labelComplimintCount.textColor         = (feedData.is_user_commented)?ORANGECOLOR:[UIColor lightGrayColor];
    self.labelComplimintCount.numberOfLines     = 0;
    self.labelComplimintCount.font                 = [UIFont fontWithName:@"Helvetica Bold" size:11.0];
    self.labelComplimintCount.textAlignment = NSTextAlignmentLeft;
    self.labelComplimintCount.text              = [feedData feed_comment_count];
    [self.buttonRemint addTarget:self action:@selector(gotoRemintScreen:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonRemint setTag:row];
    
    //  self.rctLabelRemint.frame         = CGRectMake(deviderFrame*2+60 ,0,40,24);
    self.rctLabelRemint.textColor         = (feedData.is_user_reminted)?ORANGECOLOR:[UIColor lightGrayColor];
    self.rctLabelRemint.numberOfLines     = 0;
    self.rctLabelRemint.font              = [UIFont fontWithName:@"Helvetica Bold" size:11.0];
    self.rctLabelRemint.textAlignment     = NSTextAlignmentLeft;
    self.rctLabelRemint.text              = [feedData remint_count];
    [self.buttonRemint                      setImage:[UIImage imageNamed:(feedData.is_user_reminted)?@"Icon_ReMint_Selected":@"Icon_ReMint"] forState:UIControlStateNormal];
    
    if (([feedData.feed_user_id isEqualToString:userID] && !feedData.IsMintReminted)) {
        [self.buttonRemint setHidden:YES];
        [self.rctLabelRemint setHidden:YES];
    }
    else if (![feedData.feed_user_id isEqualToString:userID] && !feedData.IsMintReminted) {
        [self.buttonRemint setHidden:NO];
        [self.rctLabelRemint setHidden:NO];
    }
    else if (![feedData.feed_user_id isEqualToString:userID] && ![feedData.remint_user_id isEqualToString:userID] && feedData.IsMintReminted) {
        [self.buttonRemint setHidden:NO];
        [self.rctLabelRemint setHidden:NO];
    }
    else
    {
        [self.buttonRemint setHidden:YES];
        [self.rctLabelRemint setHidden:YES];
    }
}
-(void)userTappedOnMintType:(UITapGestureRecognizer *)guesture
{
    UILabel *lable = (UILabel*)guesture.view;
    NSArray *myArray = [lable.text componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" | "]];
    NSLog(@"selectd mint type %@", [myArray description]);


    [self.cellDelegate SpecialFeatureWordclickedFortype:88 WithWord:[myArray lastObject] forRow:lable.tag forCell:self withPrivateAccess:NO];
}

-(void)setBackgroundImageForButton:(YSButtonType)actionButtonType AndValue:(NSString *)value withLabelValue:(NSString*)labelValue{
    switch (actionButtonType) {
        case YSButtonTypeComplimint:{
            [self.buttonComplimint setImage:([value integerValue])?[UIImage imageNamed:@"complimint_Selected"]:[UIImage imageNamed:@"Btn_CompliMint"] forState:UIControlStateNormal];
            self.labelComplimintCount.text              = labelValue;
        }
            break;
        case YSButtonTypeHifive:{
            [self.highFiveImageView setImage:[UIImage imageNamed:([value integerValue])?@"HighFive_Icon_24x24":@"hive_it_normal"]];
            self.labeHighFiveCount.text              = labelValue;
            self.labeHighFiveCount.textColor         = ([value integerValue])?ORANGECOLOR:[UIColor lightGrayColor];
        }
            break;
        case YSButtonTypeInspire:
            break;
        case YSButtonTypeRemint:{
            [self.buttonRemint setImage:[UIImage imageNamed:([value isEqualToString:@"active"])?@"Icon_ReMint_Selected":@"Icon_ReMint"] forState:UIControlStateNormal];
            self.rctLabelRemint.text              = labelValue;
        }
            break;
        default:
            break;
    }
}
- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture{
    
    if (gesture.state != UIGestureRecognizerStateBegan) {
        [self.cellDelegate buttonActionclickedFortype:YSButtonTypeHifiveCount forRow:[gesture.view tag] forCell:self];

    }
 
}
//
//-(IBAction)clickedProfileUserImageWithTag:(id)sender{
//    Feedmodel *fmodel  = [self.multiMintDataSource multiMintCollectionViewView:self DataForItemAtRow:[sender tag]];
//    [self.multiMintdelegate buttonActionclickedFortype:4 forRow:[sender tag] feedModelData:fmodel];
//    [self.cellDelegate buttonActionclickedFortype:YSButtonTypeHifiveCount forRow:[sender tag] forCell:self];
//
//}
-(void)gotouserlistView : (UIButton *) sender {
    [self.cellDelegate buttonActionclickedFortype:YSButtonTypeHifiveCount forRow:[sender tag] forCell:self];
}

- (IBAction)hifiveClicked:(UIButton*)sender{
    [self.cellDelegate buttonActionclickedFortype:YSButtonTypeHifive forRow:[sender tag] forCell:self];
}

- (IBAction)UserImageClicked:(UIButton*)sender{
    [self.cellDelegate buttonActionclickedFortype:YSButtonTypeUserImage forRow:[sender tag] forCell:self];
}
- (IBAction)remintUserImageClicked:(id)sender {
    [self.cellDelegate buttonActionclickedFortype:YSButtonTypeRemintUserImage forRow:[sender tag] forCell:self];

}


- (IBAction)gotoCommentScreen:(UIButton*)sender{
    [self.cellDelegate buttonActionclickedFortype:YSButtonTypeComplimint forRow:[sender tag] forCell:self];
 
}

- (IBAction)gotoRemintScreen:(UIButton*)sender{
    [self.cellDelegate buttonActionclickedFortype:YSButtonTypeInspire forRow:[sender tag] forCell:self];
}



-(NSMutableAttributedString *)stringForRemintLabel:(BaseFeedModel*)feedData{
    NSMutableAttributedString * string      = [[NSMutableAttributedString alloc] initWithString:feedData.remint_AtributtedString];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:88.0/255.0 green:172.0/255.0 blue:230.0/255.0 alpha:1] range:NSMakeRange(0,10)];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(11,[feedData.remint_AtributtedString length]-11)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-Bold" size:9.0f] range:NSMakeRange(0, ([feedData.remint_user_id isEqualToString:GETVALUE(CEO_UserId)])?14:[feedData.remint_username length]+10)];
   
    return string;
}

- (void)awakeFromNib {
    // Initialization code
}


@end
