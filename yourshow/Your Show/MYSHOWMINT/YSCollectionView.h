//
//  YSCollectionView.h
//  YSCollectionView
//
//  Created by Siba Prasad Hota on 05/11/15.
//  Copyright © 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseFeedModel.h"
#import "YSCollectionViewDataSource.h"
#import "YSCollectionViewLayout.h"
#import "YSMintCollectionViewCell.h"



@class YSCollectionView;

@protocol YSCollectionViewDelegate<NSObject>

-(void)didLoadNextTriggered;
- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell;
- (void)multiMintCollectionViewClicked:(YSCollectionView *)sender AtRow:(NSInteger)row;
- (void)selectedFeatureType:(int )featureType withString:(NSString *)selectedEnitity isPrivateShowroomAccess:(BOOL)access;
-(void)didScrolledUp:(CGFloat)yPosition;
-(void)didScrolledDown:(CGFloat)yPosition;
@end


@interface YSCollectionView : UICollectionView <UICollectionViewDataSource,UICollectionViewDelegate,YSCollectionViewLayoutDelegate>
@property (nonatomic) CGFloat lastContentOffset;
@property (nonatomic, retain) id <YSCollectionViewDelegate> ysDelegate;
@property (nonatomic, retain) id <YSCollectionViewDataSource> ysDataSource;
@property (nonatomic, assign) BOOL isForLandingPage;

- (void)buttonActionclickedFortype:(YSButtonType)buttonType forRow:(NSInteger)row forCell:(YSMintCollectionViewCell*)cell;

@end
