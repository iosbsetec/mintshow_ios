//
//  YSCollectionView.m
//  YSCollectionView
//
//  Created by Siba Prasad Hota on 05/11/15.
//  Copyright © 2015 Bsetec. All rights reserved.
//

#import "YSCollectionView.h"
#import "YSCommentLabel.h"
#import "NSString+Emojize.h"
#import <CoreText/CoreText.h>
#import "MSPMintsVC.h"


@implementation YSCollectionView

- (void)initializator{
    YSCollectionViewLayout *comicListLayout = [YSCollectionViewLayout new];
    comicListLayout.delegate = self;
    [self setCollectionViewLayout:comicListLayout];
    UINib *nibCellDiscoverShowrooms = [UINib nibWithNibName:@"YSMintCollectionViewCell" bundle: nil];
    [self registerNib:nibCellDiscoverShowrooms forCellWithReuseIdentifier:@"YSMintCollectionViewCell"];
    self.backgroundColor                  = [UIColor lightGrayColor];
    // set up delegates
    self.delegate = self;
    self.dataSource = self;

}

- (id)init{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

#pragma mark - Layout delegate
#define TXTMINTHEIGHT 50
- (CGSize)collectionView:(UICollectionView *)collectionView relativeHeightForItemAtIndexPath:(NSIndexPath *)indexPath {
   
    BaseFeedModel *feedData  = [self.ysDataSource multiMintCollectionViewView:self DataForItemAtRow:indexPath.row];
    CGFloat widthCell = (self.frame.size.width-20) /2.0;
    CGFloat heightTobeadded = 0.0;
    /*
     * Check if Remint view required
     * Setup the Background View here
     */
    if (feedData.IsMintReminted==1){
        /*
         * Calculate the message height and width
         * set container view Frame
         */
        
        // Changed by Mani
        
        CGRect recttxtLbl  = [[feedData.remint_message emojizedString]boundingRectWithSize:CGSizeMake(widthCell-13, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:9]} context:nil];
        
//     NSLog(@"YSC : %f",widthCell-13);

        heightTobeadded = 40+recttxtLbl.size.height;
        
    }
    
    /*
     * Calculate the Frame of Media (video,photo,youtube) here !!
     * Setup the Mint related view and sub view here
     * calculate the required Rect Size
     */
    BOOL isTextMint =(feedData.mintType == MSMintTypeText)?YES:NO;
    
    NSString *OriginalFrontendText    =   [feedData.feed_description emojizedString];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:OriginalFrontendText attributes:@{NSFontAttributeName : font }];
    CGRect boundingRect = [attributedString boundingRectWithSize:CGSizeMake(widthCell-10, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize fitSize = boundingRect.size;
    CGFloat mintdescriptionHeight = ([OriginalFrontendText length])?(fitSize.height+5):5 ;//
    
//    NSLog(@"YSC : %f",widthCell-10);
    
    CGFloat mintImageHeight = 0.0;
    if (isTextMint){
        
        heightTobeadded= heightTobeadded+((fitSize.height<TXTMINTHEIGHT)?TXTMINTHEIGHT:fitSize.height+5)+10;
    }
    else
    {
        CGSize rctSizeOriginal = CGSizeMake(feedData.feed_thumb_Width , feedData.feed_thumb_height);
        double scale = (widthCell-5) / rctSizeOriginal.width;
        CGSize rctSizeFinal = CGSizeMake(rctSizeOriginal.width * scale -10,rctSizeOriginal.height * scale -10);
        mintImageHeight =(rctSizeFinal.height<135)?135:rctSizeFinal.height;
  
        heightTobeadded = heightTobeadded + mintImageHeight+mintdescriptionHeight;
        //For Description add more height
        
    }
    //For userview + Button view height
    heightTobeadded = heightTobeadded +60;
    
    return CGSizeMake(widthCell, heightTobeadded);
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldBeDoubleColumnAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (NSUInteger)numberOfColumnsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}




- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.ysDataSource NumberOFItemsForMultiMintView:self forSection:section];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"YSMintCollectionViewCell";

      YSMintCollectionViewCell *cell = (YSMintCollectionViewCell*)[self dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    BaseFeedModel *feedData = [self.ysDataSource multiMintCollectionViewView:self DataForItemAtRow:indexPath.row];
    [cell setCustomDelegate:self];
    [cell setDataForCollectionViewCellWithData:feedData andUserID:GETVALUE(CEO_UserId) AndIndex:indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected Image is Item %d",(int)indexPath.row);
    BaseFeedModel *fmodel  = [self.ysDataSource multiMintCollectionViewView:self DataForItemAtRow:indexPath.row];
    [self.ysDelegate buttonActionclickedFortype:YSButtonTypedetails forRow:indexPath.row feedModelData:fmodel forCell:nil];
}

-(void)gotoNouserFoundScreen
{
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"UserNotFoundVC" owner:nil options:nil];
    UIView *view = nibContents[0];
    UIButton *button = (UIButton *)[view viewWithTag:3];
    [button addTarget:self action:@selector(removeNouserfoundScreen:) forControlEvents:UIControlEventTouchUpInside];
    
    AppDelegate *delegate = MINTSHOWAPPDELEGATE;
    [delegate.window addSubview:view];
}
-(void)removeNouserfoundScreen:(UIButton *)button
{
    [[[button superview] superview] removeFromSuperview];
    
}
- (void)SpecialFeatureWordclickedFortype:(int)type WithWord:(NSString *)string forRow:(NSInteger)row forCell:(YSMintCollectionViewCell*)cell withPrivateAccess:(BOOL)privateAccess {
    NSLog(@"type = %d and word = %@",type,string);
   
    if (type==22) //  For Remint
    {
        NSString *strUserId = [self checkRemintUserNameExistOrNotInRow:row Name:[string substringFromIndex:1]];
        if ([strUserId isEqualToString:@"0"])
        {
            [self gotoNouserFoundScreen];
        }
        else
            [self.ysDelegate selectedFeatureType:44 withString:strUserId isPrivateShowroomAccess:privateAccess];
    }
    else if (type==44) {
        NSString *strUserId = [self checkUserNameExistOrNotInRow:row Name:[string substringFromIndex:1]];
        
        if ([strUserId isEqualToString:@"0"]) {
            [self gotoNouserFoundScreen];
        }
        else
        [self.ysDelegate selectedFeatureType:type withString:strUserId isPrivateShowroomAccess:privateAccess];
    }
    else
    {
        [self.ysDelegate selectedFeatureType:type withString:string isPrivateShowroomAccess:privateAccess];
    }
    
}
// Need to add
-(NSString *)checkUserNameExistOrNotInRow:(NSInteger)row Name:(NSString *)selectUserName
{
    BaseFeedModel *fmodel  = [self.ysDataSource multiMintCollectionViewView:self DataForItemAtRow:row];
    if(fmodel.atMentionedArray.count>0)
    {
        for (int i=0; i<fmodel.atMentionedArray.count; i++) {
            NSString *strName = [[fmodel.atMentionedArray objectAtIndex:i]valueForKey:@"name"];
            strName = [strName stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([[strName lowercaseString] isEqualToString:[selectUserName lowercaseString]]) {
                return [[fmodel.atMentionedArray objectAtIndex:i]valueForKey:@"id"];
            }
        }
    }
    return nil;
}

- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row forCell:(YSMintCollectionViewCell*)cell{
   
    BaseFeedModel *fmodel  = [self.ysDataSource multiMintCollectionViewView:self DataForItemAtRow:row];
    if(buttonType == YSButtonTypeHifive) {
        if (_isForLandingPage) {
            [self.ysDelegate buttonActionclickedFortype:YSButtonTypeHifive forRow:row feedModelData:fmodel forCell:cell];

        }
        else
        [self hifiveClicked:fmodel forCell:cell androw:row];
    }
   else if(buttonType == YSButtonTypeHifiveCount) {
       if ([fmodel.feed_like_count integerValue])
           [self.ysDelegate buttonActionclickedFortype:buttonType forRow:row feedModelData:fmodel forCell:cell];
    }else if(buttonType == YSButtonTypeUserImage)  {
        [self.ysDelegate buttonActionclickedFortype:YSButtonTypeUserImage forRow:row feedModelData:fmodel forCell:cell];
    }else if(buttonType == YSButtonTypeRemintUserImage)  {
        [self.ysDelegate buttonActionclickedFortype:buttonType forRow:row feedModelData:fmodel forCell:cell];
    } else {
        [self.ysDelegate buttonActionclickedFortype:buttonType forRow:row feedModelData:fmodel forCell:cell];
    }
}




#pragma MARK -- Highfive Activities
- (void)hifiveClicked:(BaseFeedModel *)fmodel forCell:(YSMintCollectionViewCell*)cell androw:(NSInteger)row{

    int hiveCount      = [fmodel.feed_like_count intValue];
    int originalStatus = fmodel.is_user_liked;
    
    if(!fmodel.is_user_liked){
        fmodel.feed_like_count = [NSString stringWithFormat:@"%d",hiveCount+1];
        fmodel.is_user_liked   = 1;
    }else{
        fmodel.feed_like_count = [NSString stringWithFormat:@"%d",hiveCount-1];
        fmodel.is_user_liked   = 0;
    }
    
    [cell setBackgroundImageForButton:YSButtonTypeHifive AndValue:[NSString stringWithFormat:@"%d",fmodel.is_user_liked] withLabelValue:fmodel.feed_like_count];
    [cell.buttonHighFive setUserInteractionEnabled:NO];
    [self.ysDelegate buttonActionclickedFortype:YSButtonTypeHifive forRow:row feedModelData:fmodel forCell:cell];

    [YSAPICalls hifiveMint:fmodel withSuccessionBlock:^(id response) {
        Generalmodel *newResponse=response;
        if ([newResponse.status_code intValue] == 1) {
            [cell.buttonHighFive setUserInteractionEnabled:YES];
        }  else {
            [cell setBackgroundImageForButton:YSButtonTypeHifive AndValue:            [NSString stringWithFormat:@"%d",hiveCount] withLabelValue:[NSString stringWithFormat:@"%d",originalStatus]];
        }
    } andFailureBlock:^(NSError *error)  {
        [cell.buttonHighFive setUserInteractionEnabled:YES];
         [cell setBackgroundImageForButton:YSButtonTypeHifive AndValue:            [NSString stringWithFormat:@"%d",hiveCount] withLabelValue:[NSString stringWithFormat:@"%d",originalStatus]];
    }];
}



#pragma mark CHECK IF NEED TO LOAD MORE

static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.89;

static BOOL ShouldLoadNextPage(UICollectionView *collectionView){
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self);
    if (shouldLoadNextPage)  {
        [self.ysDelegate didLoadNextTriggered];
    }
}
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    
//    if ([self.ysDelegate isKindOfClass:[MSPMintsVC class]])
//    {
//        int yPos =  scrollView.contentOffset.y;
//        if (self.lastContentOffset > scrollView.contentOffset.y)
//        {
//            [self.ysDelegate didScrolledUp:(scrollView.contentOffset.y<0)?0:(yPos>86)?86:scrollView.contentOffset.y];
//        }
//        else if (self.lastContentOffset < scrollView.contentOffset.y)
//        {
//            if(yPos<0||yPos==0)
//            {
//                //                NSLog(@"blinking happening");
//            }
//            else
//                [self.ysDelegate didScrolledDown:(scrollView.contentOffset.y>86)?86:scrollView.contentOffset.y];
//        }
//        self.lastContentOffset = scrollView.contentOffset.y;
//    }
//}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([self.ysDelegate isKindOfClass:[MSPMintsVC class]])
    {
        int yPos =  scrollView.contentOffset.y;
        if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            [self.ysDelegate didScrolledUp:(scrollView.contentOffset.y<0)?0:scrollView.contentOffset.y];
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            if(yPos<0||yPos==0)
            {
                //                NSLog(@"blinking happening");
            }
            else
                [self.ysDelegate didScrolledDown:scrollView.contentOffset.y];
        }
        self.lastContentOffset = scrollView.contentOffset.y;
    }
}


// Need to add
-(NSString *)checkRemintUserNameExistOrNotInRow:(NSInteger)row Name:(NSString *)selectUserName
{
    BaseFeedModel *fmodel  = [self.ysDataSource multiMintCollectionViewView:self DataForItemAtRow:row];
    if(fmodel.remintAtMentionedArray.count>0)
    {
        for (int i=0; i<fmodel.remintAtMentionedArray.count; i++) {
            NSString *strName = [[fmodel.remintAtMentionedArray objectAtIndex:i]valueForKey:@"name"];
            strName = [strName stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([[strName lowercaseString] isEqualToString:[selectUserName lowercaseString]]) {
                return [[fmodel.remintAtMentionedArray objectAtIndex:i]valueForKey:@"id"];
            }
        }
    }
    return nil;
}



@end


/*
 
 - (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
 UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"TrendingHeaderView" forIndexPath:indexPath];
 if (reusableview==nil) {
 reusableview=[[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 200)];
 }
 return reusableview;
 }
 return nil;
 }
 
 
 - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
 return CGSizeMake(0, 200);
 }
 */
