//
//  Keys.h
//  Conciergist
//
//  Created by Siba Prasad Hota  on 3/30/15.
//  Copyright (c) 2015 Conciergist. All rights reserved.
//
#import <Foundation/Foundation.h>


//********
/*
openssl x509 -in aps.cer -inform der -out PushChatCert.pem
openssl pkcs12 -nocerts -out PushChatKey.pem -in DistPushChatKey.p12
cat PushChatCert.pem PushChatKey.pem > ck.pem
telnet gateway.sandbox.push.apple.com 2195
openssl s_client -connect gateway.sandbox.push.apple.com:2195 -cert PushChatCert.pem -key PushChatKey.pem
 */
//********

#pragma mark VIDEO URL API NAMES WITH ENDPOINT
//*********************************************

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define isSmallPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) && [[[UIDevice currentDevice] model] hasPrefix:@"iPad"]

//STAGE INFO
/*

#define  USER_LIST_API                       @"http://sapi.mintshowapp.com/v1/users/get_search_users"
#define  HASHTAG_LIST_API                    @"http://sapi.mintshowapp.com/v1/users/get_search_hashtag"
#define  SHOWROOM_LIST_API                   @"http://sapi.mintshowapp.com/v1/users/get_search_showroom"
#define  ServerWebUrl                        @"http://sapi.mintshowapp.com"
#define  ServerUrl                           @"http://sapi.mintshowapp.com/v1/"


#define  VIDEO_UPLOAD_URL                    @"http://stage.mintshowapp.com/ajax_video_mint.php"
#define  IMAGE_UPLOAD_URL                    @"http://stage.mintshowapp.com/ajax_image_mint.php"
#define  ZENCODER_API_KEY                    @"50adf0409e413209c7cdc0ca82c4ee2b"
#define  CLOUD_UPLOAD                        @"http://sapi.mintshowapp.com/v1/customshowroom/cloud_upload_status"


//GET https://app.zencoder.com/api/v2/jobs/1234/progress.json?api_key=50adf0409e413209c7cdc0ca82c4ee2b
//stage.mintshowapp.com/video_mint.php?access_token=f9984ccf0a9e6499f7527f40373eeb2bf1c5713b&achievement_category_id=5
//stage.mintshowapp.com/ajax_video_mint.php?access_token=f8c9818128af4e06d5af69c4a129019bc252e138&achievement_category_id=5

#define  ServerUrlSetting                    @"http://stage.mintshowapp.com"
#define  VIDEO_DIRECT_URL                    @"http://66f57756e8f0458766ae-822a7d983ab6dcec7495bcb93022ef08.r12.cf1.rackcdn.com"
#define   MixPanelToken                      @"39682a1b178682bd7750589c2e29478b"
#define   MixPanelKey                        @"dca4a7f0535b28c4c337fb307b241661"
#define   MixpanelSecret                     @"68049ce075ca56276298a6307bc5581b"
*/



 //LIVE INFO

 #define  USER_LIST_API                       @"http://api.mintshowapp.com/v1/users/get_search_users"
 #define  HASHTAG_LIST_API                    @"http://api.mintshowapp.com/v1/users/get_search_hashtag"
 #define  SHOWROOM_LIST_API                   @"http://api.mintshowapp.com/v1/users/get_search_showroom"
 #define  ServerWebUrl                        @"http://api.mintshowapp.com"
 #define  ServerUrl                           @"http://api.mintshowapp.com/v1/"
 #define  VIDEO_UPLOAD_URL                    @"http://mintshowapp.com/ajax_video_mint.php"
 #define  IMAGE_UPLOAD_URL                    @"http://mintshowapp.com/ajax_image_mint.php"
 #define  ServerUrlSetting                    @"http://mintshowapp.com"
 #define  ZENCODER_API_KEY                    @"50adf0409e413209c7cdc0ca82c4ee2b"
 #define  CLOUD_UPLOAD                        @"http://api.mintshowapp.com/v1/customshowroom/cloud_upload_status"
 #define  VIDEO_DIRECT_URL                    @"http://0fdb9f32af97f2a587b5-12305243b19ae7f84223fc39bd885b15.r57.cf1.rackcdn.com"
 #define   MixPanelToken                       @"dea33d0ebe704f8132c9db970b26a994"
 #define   MixPanelKey                         @"659524708fcc75d31103b267b2aa0101"
 #define   MixpanelSecret                      @"571b83b7a7bb0f64b201a929e4b51957"


#pragma mark VIDEO URL

/*
 yaac_dev'  :http://0db2a9e454f033b23aa2-ea24f12bec0fa07dec74cf8d4195707d.r21.cf1.rackcdn.com/{$iFileId}.mp4";
 yaac_stage :http://0e4d430fb4098f4615fe-9aedea8abd2894134979c98a7fc0997b.r67.cf1.rackcdn.com/{$iFileId}.mp4";
 yaac_live' :http://2fd4c89ff62a0c7d68ba-d0d55a1a9e1cf781daedbf32577f7366.r26.cf1.rackcdn.com/{$iFileId}.mp4";
 */

#define ORANGECOLORCOACHINGTIPS              [UIColor colorWithRed:243./255.0 green:94./255.0 blue:27.0/255. alpha:1.0]
#define GREENCOLORCOACHINGTIPS               [UIColor colorWithRed:76./255.0 green:175./255.0 blue:80.0/255. alpha:1.0]
#define BLUECOLORCOACHINGTIPS                [UIColor colorWithRed:14./255.0 green:129./255.0 blue:196.0/255. alpha:1.0]

#define MINTSHOWAPPDELEGATE    (AppDelegate *) [UIApplication sharedApplication].delegate;

#pragma mark LIST OF API NAMES WITH ENDPOINT
//*******************************************

#define  API_GETALLCOMPLIMENTS               @"achievements/get_comment_list"
#define  API_ADDORUPDATESHOWROOM             @"customshowroom/save_update_showroom_details"
#define  API_ADDMINTTEXT                     @"mint/post_single_mint"
#define  API_HASHTAGMINTS                    @"customshowroom/get_mints_hashtag"
#define  API_JOINSHOWROOM                    @"customshowroom/join_showroom"
#define  API_REMINTMINT                      @"mint/remint"
#define  API_EDITREMINT                      @"mint/editremint"
#define  API_EDITMINT                        @"achievements/edit_mint_desc"
#define  API_ADDMINTMEDIA                    @"achievements/post_single_achievement"
#define  API_MYSHOW_MINTS                    @"customshowroom/my_show_mints"
#define  API_MYFOLLOWERS                     @"follow/followers_list"
#define  API_INCIRCLE                        @"follow/following_list"
#define  API_HASHTAG                         @"users/get_search_hashtag"
#define  API_FLAGMINT                        @"inappropriate/get_feedInappropriate"
#define  API_MUTEAMINT                       @"customshowroom/mute_unmute_mint"
#define  API_MUTEUSER                        @"customshowroom/mute_unmute_user"
#define  API_MYSHOW_MINTDETAIL               @"customshowroom/MYShow_Mint_Details"
#define  API_DELETE_MINT                     @"achievements/delete_single_achievement"
#define  API_GETPARTICULARSHOWROOM           @"customshowroom/get_particular_showroom"
#define  API_GETSHOWROOMDETAILS              @"customshowroom/get_showroom_details"
#define  API_GETMEMBERLIST                   @"customshowroom/get_member_list"
#define  API_FORGOTPASSWORD                  @"users/post_newforgetpassword"
#define  API_SHOWTAGCHECK                    @"customshowroom/showroom_tag_exits"
#define  API_MINTDETAIL                      @"achievements/get_achievement_details"
#define  API_GETSHOWROOMDETAILS              @"customshowroom/get_showroom_details"
#define  API_FIRSTTIMEVISIT                  @"customshowroom/update_first_time_visit"
#define  API_SIGNUP                          @"users/get_registeruser"
#define  API_GETUSERPROFILE                  @"successprofile/get_successprofile"
#define  API_PROFILEFOLLOWING                @"follow/following_list"
#define  API_PROFILEFOLLOWERS                @"follow/followers_list"
#define  API_FILTERSEARCH                    @"customshowroom/filter_search"
#define  API_GETACTIVITYMYCIRCLE             @"newsfeed/get_userfeed"
#define  API_SELECTINTEREST                  @"customshowroom/join_multiple_showrooms"
#define  API_USERLOGOUT                      @"users/post_logout"
#define  API_GETACTIVITYYOUANDREQUEST        @"newsfeed/activity"

#define  METHOD_PROFILESTATS                @"ProfileStats"
#define  API_GETPROFILESTATSDETAILS          @"successprofile/get_stats_details"
#define  KPROFILEEDITED                      @"profileEditedSuccesfully"
#define  KPROFILEINCIRCLE                     @"ProfileUserIncircleCheck"
#define  METHOD_EDITPROFILE                  @"ProfileEdit"
#define  API_PROFILEEDIT                     @"successprofile/get_professionsummary"
#define  API_SEARCHSUGGESTION                @"customshowroom/search_suggestion"

#pragma mark LIST OF METHODS
//***************************************************************
#define     METHOD_GLOBALSEARCH              @"GlobalSearch"
#define     METHOD_SEARCHSUGGETION           @"searchSuggetion"
#define     METHOD_SIGNUP                    @"SignUp"
#define     METHOD_DISCOVER_ALL              @"Discover_All_Tab"
#define     METHOD_MYSHOW_SHOWROOM           @"MyShowShowRoom"
#define     METHOD_ADDORUPDATESHOWROOM       @"addUpdateShowroom"
#define     METHOD_ADDMINT                   @"addMint"
#define     METHOD_GETHASHTAGMINTS           @"getHashTagMints"
#define     METHOD_REMINTMINT                @"remintMint"
#define     METHOD_EDITMINT                  @"EditMint"
#define     METHOD_LOGIN                     @"Login"
#define     METHOD_MYSHOW_MINT               @"MyShowMint"
#define     METHOD_HASGTAG                   @"Hashtag"
#define     METHOD_FLAGMINT                  @"FlagMint"
#define     METHOD_MUTEMINT                  @"muteMint"
#define     METHOD_MUTEUSER                  @"muteUser"
#define     METHOD_GETALLCOMPLIMINTS         @"getAllComplimints"
#define     METHOD_GETUSERPROFILE            @"getUserProfile"
#define     METHOD_GETPARTICULARSHOWROOM     @"getParticularShowroom"
#define     METHOD_TRENDING_SHOWROOM         @"TrendingShowroomsList"
#define     METHOD_FB_REGISTER               @"fbregister"
#define     METHOD_GPLUS_REGISTER            @"gPlusregister"
#define     METHOD_GPLUS_LOGIN               @"gPlusLogin"
#define     METHOD_SEND_MESSAGE              @"sendMessage"
#define     METHOD_REGISTER                  @"Register"
#define     METHOD_REFERRAL_ID               @"getRefferelID"
#define     METHOD_PROFILE                   @"profileDetails"
#define     METHOD_FB_TWT_ACCOUNTCHECK       @"fbtwtAccntcheck"
#define     METHOD_PROFILE_IMAGE             @"profileImage"
#define     METHOD_REGISTER_RSP              @"registerUserForRsp"
#define     METHOD_PROFESSION_SUMMARY        @"proffesionSummary"
#define     METHOD_EDIDPROFILELOCATION       @"editProfileLocation"
#define     METHOD_SINGLE_ACHIVMENT          @"GetSingleAchivment"
#define     METHOD_GET_SINGLEMINT            @"SingleMint"
#define     METHOD_FOLLOW_UNFOLLOW           @"followUnfollow"
#define     METHOD_HIFIVE_MINT               @"Hifivemint"
#define     METHOD_INSPIRE_MINT              @"inspiremint"
#define     METHOD_PLANTODO_MINT             @"planTodomint"
#define     METHOD_DELETE_MINT               @"deletemint"
#define     METHOD_GETIGHFIVEUSERS           @"hifiveusers"
#define     METHOD_DELETE_SINGLE_MINT        @"delete_single_mint"
#define     METHOD_COMPLI_MINT               @"complimint"
#define     METHOD_EDIT_SINGLRACHIVEMENT     @"edit_single_achievement"
#define     METHOD_FACEBOOK_EXISTING_USER    @"facebook_existing_usercheck"
#define     METHOD_USER_LIST                 @"UserList"
#define     METHOD_JOINSHOWROOM              @"JoinShowroom"
#define     METHOD_MEMBER_LIST               @"memberlist"
#define     METHOD_FORGOTPASSWORD            @"Forgotpassword"
#define     METHOD_MYSHOW_MINTDETAIL         @"MyshowmintDetails"
#define     METHOD_SHOWTAG_CHECK             @"ShowtagCheck"
#define     METHOD_MINTDETAIL                @"MintDetail"
#define     METHOD_DELETEMINT                @"DeleteMint"
#define     METHOD_SINGLE_MINTDETAIL         @"SingleMintDetails"
#define     METHOD_FOLLOWERLIST              @"FollowerList"
#define     METHOD_GETSHOWROOMDETAILS        @"getShowroomDetails"
#define     METHOD_RECEIVE_POWER_STATUS      @"RECIEVEPOWERSTREAK"
#define     METHOD_FIRSTTIMEVISIT            @"Firsttimevisit"
#define     METHOD_PROFILEFOLLOWING          @"FollowingList"
#define     METHOD_PROFILEFOLLOWERS          @"ProfileFollowers"
#define     METHOD_FILTERSEARCH              @"FilterSearch"
#define     METHOD_GETACTIVITYYOU            @"GetActivityYou"
#define     METHOD_GETACTIVITYREQUEST        @"GetActivityRequest"
#define     METHOD_GETACTIVITYMYCIRCLE       @"GetActivityMyCircle"

#pragma mark LIST OF CONSTANTS
//*************************************************************

#define     FACEBOOK_EXISTINGUSER_CHECK     @"facebookfriends/fb_google_check_exist_user"
#define     API_DISCOVER_ALL                @"customshowroom/get_discover_all"
#define     API_MYSHOW_SHOWROOM             @"users/get_showroom_list"
#define     KCOMMNETUPDATED                 @"CommntUpdatedSuccesfully"
#define     KSHOWSINGLEMINTDETAILS          @"ShowMintDetailsWithSingleRecord"
#define     KHIGHFIVEUPDATED                @"HighFiveUpdatedSuccesfully"
#define     KSELECTDROW                     @"SelectedRow"
#define     KSELECTDMINTID                  @"SelectdMintId"
#define     KMINTEDITED                     @"MintEditedSuccesfully"
#define     KPROFILEIMAGEUPDATED                     @"UserimageUpdated"

#define     DEVICE_TOKEN                    @"DeviceToken"
#define     KMINTADDED                      @"MintAddedSuccesfully"
#define     CONSTANT_RESULT                 @"result"
#define     ADDCOMMENT                      @"ADDCOMMENT"
#define     DELETECOMMENT                   @"DELETECOMMENT"
#define     EDITCOMMENT                     @"EDITCOMMENT"
#define     kEntityTypeNormalText           @"NormalText"
#define     kEntityTypeMention              @"MentionText"
#define     DummyAccessToken                @"389c7bd1dce4e5ef4f3b6a06ac9f9dadaba3149f" 
#define     kGOOGLE_API_KEY                 @"AIzaSyAmRWAMEs7jwyG0j0i1sgsFSZMFWS2wPyA"
#define PUSHMINTCOUNT                 @"mintcount"
#define APPINSTALLEDFIRSTTIME                @"App_Install"

#define     CEO_VersionApp                  @"appVersion"
#define     CEO_DeviceAccessToken           @"DeviceToken"
#define     CEO_AccessToken                 @"accessToken"
#define     CEO_LOGINSUCSSES                @"LoginSucsses"
#define     CEO_FacebookAccessToken         @"FBAcessToken"
#define     CEO_FacebookId                  @"FacebookId"
#define     CEO_DeviceToken                 @"DeviceToken"
#define     CEO_UserImage                   @"user_image_url"
#define     CEO_UserName                    @"user_name"
#define     CEO_UserId                      @"user_id"
#define     CEO_TempUserId                  @"Temp_user_id"
#define     CEO_UserFirstName               @"first_name"
#define     CEO_UserLastName                @"last_name"
#define     CEO_UserLocation                @"userLocName"
#define     CEO_FollowerCount               @"follower_list_count"
#define     CEO_FollowingCount              @"following_list_count"
#define     CEO_DEEPLINKINGINFO             @"DeepLinkingId"
#define     CEO_DEEPLINKINGUSERNAME         @"DeepLinkingUsername"
#define     CEO_ReferalID                   @"referalID"
#define     CEO_MintType_Image              @"mintTypeImage"
#define     CEO_MintType_Video              @"mintTypeVideo"
#define     CEO_Gender                      @"gender"
#define     CEO_PULLREQUESTTIME             @"pull_request_time"
#define     CEO_POSTMINTIMAGE               @"selectedImage"
#define     CEOMintPlacholderText           @"Add an Image or Video"
#define     CEOEncourageMintPlacholderText  @"Description..."
#define     CEO_DISCOVERALLFIRSTTIMEVISIT   @"discover_all_page_first_time_visit"
#define     CEO_PARTCULARSHOWROOMFIRSTTIMEVISIT      @"showroom_page_first_time_visit"
#define     CEO_Email                       @"UserEmail"
#define     CEO_Password                    @"UserPassword"
#define     CEO_RememberMe                  @"rememberMe"
#define     CEO_FBUSERIMAGE                 @"FacebookUserImage"
#define     CEO_ISMINTALREADYPOSTED         @"is_already_posted_mint"
#define     CEO_ISMINTDETAILFIRSTTIMEVISIT  @"mint_detail_page_first_time_visit"
#define     CEO_ISFBMINTSHOWFRIEND           @"is_fbandmintshowfriend"
#define     CEO_ISFACEBOOKLOGIN              @"is_fblogin"
#define     CEO_ISJOINEDSHOWROOM             @"is_joined_showrooms"
#define     CEO_ISACCOUNTDEACTIVATED         @"accountDeactivated"
#define     CEO_ISADDMINTFIRSTTIMEVISIT      @"addmintFirstTimeVisist"


#define     INNER_TAB_1_SELECTED_6P         @"myshow_ip6plus.png"
#define     INNER_TAB_2_SELECTED_6P         @"discover_ip6plus.png"
#define     INNER_TAB_3_SELECTED_6P         @"Mint_ip6plus.png"
#define     INNER_TAB_4_SELECTED_6P         @"activity_ip6plus.png"
#define     INNER_TAB_5_SELECTED_6P         @"profile_ip6plus.png"

#define     INNER_TAB_1_SELECTED_6          @"myshow_ip6.png"
#define     INNER_TAB_2_SELECTED_6          @"discover_ip6.png"
#define     INNER_TAB_3_SELECTED_6          @"Mint_ip6.png"
#define     INNER_TAB_4_SELECTED_6          @"activity_ip6.png"
#define     INNER_TAB_5_SELECTED_6          @"profile_ip6.png"

#define     INNER_TAB_1_SELECTED            @"myshow.png"
#define     INNER_TAB_2_SELECTED            @"discover.png"
#define     INNER_TAB_3_SELECTED            @"Mint.png"
#define     INNER_TAB_4_SELECTED            @"activity.png"
#define     INNER_TAB_5_SELECTED            @"profile.png"

#define kParticularShowroomMintAdded @"kParticularShowRoomAdded"

#define ORANGECOLOR                [UIColor colorWithRed:243./255.0 green:94./255.0 blue:27.0/255. alpha:1.0]
#define NAVYBLUECOLOR              [UIColor colorWithRed:9.0/255.0 green:26.0/255.0 blue:53.0/255. alpha:1.0]
#define BACKGROUNDCOLOR            [UIColor colorWithRed:246/255.0 green:246.0/255.0 blue:246.0/255. alpha:1.0]
#define SHOWROOMBACKGROUNDCOLOR    [UIColor colorWithRed:229/255.0 green:229.0/255.0 blue:229.0/255. alpha:1.0]

#define APPPUSHALLOWED                @"pushNotificationEnableOrDisable"




#define IOS7  ([[[[[UIDevice currentDevice] systemVersion]componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7)

//KEYS FOR COMMENT SCREEN ACTIONS




#define iPhone6     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone4     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define GETVALUE(keyname)  [[NSUserDefaults standardUserDefaults] valueForKey:keyname]
#define SETVALUE(value,keyname)  [[NSUserDefaults standardUserDefaults] setValue:value forKey:keyname]


#define USERNAMEARRAY @[@[@"Abdiel",@"user1"],@[@"Quontham",@"user2"],@[@"Candy",@"user3"],@[@"Alfreda",@"user4"],@[@"Trevion",@"user5"],@[@"Zechariah",@"user6"],@[@"Westley",@"user7"],@[@"Garrick",@"user8"],@[@"Pothiraj",@"user9"],@[@"Jitten",@"user10"]]

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define kBgQueue                    dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)




//**************************************************************************************
//               BELOW ARE THE NSUSERDEFAULTS FUNCTIONS
//**************************************************************************************

#define USERDEFAULTS                [NSUserDefaults standardUserDefaults]
#define SYNCHRONISE                 [[NSUserDefaults standardUserDefaults] synchronize]
#define GETVALUE(keyname)           [[NSUserDefaults standardUserDefaults] valueForKey:keyname]
#define SETVALUE(value,keyname)     [[NSUserDefaults standardUserDefaults] setValue:value forKey:keyname]


//#define NSLog(FORMAT, ...) nil

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif

//**************************************************************************************


#define showAlert(title,content,ok,adelegate) [[[UIAlertView alloc] initWithTitle:title message:content delegate:adelegate cancelButtonTitle:ok otherButtonTitles:nil] show];

#define ImagePostURL  @"%@/ajax_image.php"
#define ImageFilePath @"Content-Disposition: form-data; name=\"images\"; filename=\"achievementPic.jpg\"\r\n"
#define VideoPostURL  @"%@/ajax_video.php"
#define VideoFilePath @"Content-Disposition: form-data; name=\"file\"; filename=\"somefile.mov\"\r\n"

//#define  VIDEO_DIRECT_URL                    @"http://0e4d430fb4098f4615fe-9aedea8abd2894134979c98a7fc0997b.r67.cf1.rackcdn.com"



