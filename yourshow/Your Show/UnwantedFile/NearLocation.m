//
//  NearLocation.m
//  Your Show
//
//  Created by JITENDRA on 8/13/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "NearLocation.h"
#import "AppDelegate.h"
#import  <CoreLocation/CoreLocation.h>
#import "LocatioDetails.h"
#import "DBManager.h"
@interface NearLocation ()<CLLocationManagerDelegate,UISearchBarDelegate>
{
    BOOL isSearching;
    CLLocationCoordinate2D currentCentre;
    CLLocation *currentLocation;
    NSMutableArray *arrayAllLocations;
    UIActivityIndicatorView *indicator;
    UIButton *refreshLocation;
    NSMutableArray *searchResults;
    UISearchBar *search;
    NSArray *   customLocationArray;
}
@property (nonatomic, strong) DBManager *dbManager;
@property(nonatomic, strong) UITableView *table;
@property (nonatomic, strong) CLLocationManager *locationManager;
@end
@implementation NearLocation
@synthesize table;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)showFilterListWithFrame : (CGRect) frame{
    
    self.table = (UITableView *)[super init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"mintshow.sql"];

    if (self) {
        isSearching = NO;
        searchResults = [NSMutableArray array];

        UIView *viewTop = [[UIView alloc]initWithFrame:CGRectMake(0, 0,CGRectGetWidth(frame), 49)];
        [viewTop addSubview:[self addMyButton]];
        
        UILabel *labelFilters = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, [UIScreen mainScreen].bounds.size.width, 18)];
        [labelFilters setFont:[UIFont boldSystemFontOfSize:12]];
        labelFilters.textColor = [UIColor whiteColor];
        labelFilters.textAlignment = NSTextAlignmentCenter;
        labelFilters.font = [UIFont boldSystemFontOfSize:15];
        NSString *string =@"LOCATIONS";
        [labelFilters setText:string];
        [viewTop addSubview:labelFilters];
        
        search = [[UISearchBar alloc] init];
        [search setTintColor:[UIColor colorWithRed:233.0/255.0
                                             green:233.0/255.0
                                              blue:233.0/255.0
                                             alpha:1.0]];
        search.frame = CGRectMake(0, 50, CGRectGetWidth(frame), 40);
        search.placeholder = @"Find or create a location";
        search.delegate = self;
        search.showsBookmarkButton = NO;
        [self addSubview:search];
        
        
        UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        searchButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 60,12.0, 60.0, 27.0);
        [searchButton setTitle:@"Cancel" forState:UIControlStateNormal];
        searchButton.backgroundColor = [UIColor clearColor];
        searchButton.layer.cornerRadius = 4.0;
        [searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
        [searchButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [searchButton addTarget:self action:@selector(hideDropDown) forControlEvents:UIControlEventTouchUpInside];
        [viewTop addSubview:searchButton];
        
        
        /* Section header is in 0th index... */
        UIColor * Backcolor = [UIColor colorWithRed:9/255.0f green:19/255.0f blue:39/255.0f alpha:1.0f];
        [viewTop setBackgroundColor:Backcolor];
        self.backgroundColor = [UIColor whiteColor];
        self.layer.masksToBounds = NO;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 91, CGRectGetWidth(frame), CGRectGetHeight(frame)-91)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        table.backgroundColor = [UIColor whiteColor];
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        [appDell.window addSubview:self];
        self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
        [self setFrame:frame];
        [self addSubview:table];
        [self addSubview:viewTop];
        [self loadDataFromDataBase];

        [self getCurrentLocation];

        
    }
    return self;
}

-(void)hideDropDown
{
    [self removeFromSuperview];
}

- (UIButton *)addMyButton
{    // Method for creating button, with background image and other properties
    refreshLocation = [UIButton buttonWithType:UIButtonTypeCustom];
    refreshLocation.frame = CGRectMake(10.0,10.0, 30.0, 30.0);
    [refreshLocation setImage:[UIImage imageNamed:@"refresh-xxl"] forState:UIControlStateNormal];
    
    refreshLocation.backgroundColor = [UIColor clearColor];
    [refreshLocation setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
    [refreshLocation addTarget:self action:@selector(refressLocations:) forControlEvents:UIControlEventTouchUpInside];
    return refreshLocation;
}

-(void)refressLocations : (UIButton *) sender
{
    (![refreshLocation isSelected])?[refreshLocation setSelected:YES]:[refreshLocation setSelected:NO];
    [sender setImage:nil forState:UIControlStateNormal];

    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    CGFloat halfButtonHeight = sender.bounds.size.height / 2;
    CGFloat buttonWidth = sender.bounds.size.width;
    indicator.center = CGPointMake(buttonWidth - halfButtonHeight , halfButtonHeight);
    [sender addSubview:indicator];
    [indicator startAnimating];
    [arrayAllLocations removeAllObjects];
    [table reloadData];
    [self queryGooglePlaces:@"establishment"];

}
#pragma mark - UITableViewDelegate implementation


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    

    if (isSearching) {
        
//        return ([searchResults count])? [searchResults count]:1;
        return [searchResults count]+1;

    }
    else
        return [arrayAllLocations count];
    
 }

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    
    static NSString *CellIdentifier = @"newFriendCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    if ( isSearching ) {
        
        if((indexPath.row == searchResults.count) || searchResults.count==0)
        {
        cell.textLabel.text        =    [NSString stringWithFormat:@"Create \"%@\"",search.text];
        cell.detailTextLabel.text  =    @"Create a custom location";
            cell.tag = 9000;
        }
        
        else
        {
        cell.textLabel.text        =    [[searchResults objectAtIndex:indexPath.row]yaacPlaceName];
        cell.detailTextLabel.text  =    [[searchResults objectAtIndex:indexPath.row]yaccVicinity];
        }

    }
    else
    {
        cell.textLabel.text        =   [[arrayAllLocations objectAtIndex:indexPath.row]yaacPlaceName];
        cell.detailTextLabel.text  =   [[arrayAllLocations objectAtIndex:indexPath.row]yaccVicinity];
    }

    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [cell.detailTextLabel setTextColor:[UIColor grayColor]];

    
    return cell;
    
    }


-(void)getCurrentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDelegate:self];
    //In ViewDidLoad
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startMonitoringSignificantLocationChanges];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyKilometer];
    [_locationManager startUpdatingLocation];
    currentLocation = [ self.locationManager location];
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.tag == 9000) {
        [self.delegate NearLocationTableDelegateMethod:nil stringWithCustomText:search.text];
        [self addCustomLocation:search.text];
        
    }
    else
    {
        // Need to change this
        if (isSearching)
            [self.delegate NearLocationTableDelegateMethod:[searchResults objectAtIndex:indexPath.row] stringWithCustomText:nil];
        else
            [self.delegate NearLocationTableDelegateMethod:[arrayAllLocations objectAtIndex:indexPath.row] stringWithCustomText:nil];
    }
}




-(void) addCustomLocation : (NSString *) customLocation
{

    
    // Initialize the dbManager object.
    _dbManager = [[DBManager alloc] initWithDatabaseFilename:@"mintshow.sql"];
    //   create an insert query.
    NSString *query;
    
    query = [NSString stringWithFormat:@"insert into location values( '%@','%@','%@')",customLocation, [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude],[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude]];
    // Execute the query.
    [_dbManager executeQuery:query];
    // If the query was successfully executed then pop the view controller.
    if (_dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", _dbManager.affectedRows);
        
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    
    
   _dbManager = nil;



}

-(void)loadDataFromDataBase
{


        // Form the query.
        NSString *query = @"select * from location";
        customLocationArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
        NSLog(@"custom LocationArray. %@",customLocationArray);

    
    

}
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    currentLocation = newLocation;
//    // NSArray *currentLocation = @[ [[CLLocation alloc] initWithLatitude:19.0759 longitude:72.8776]];
//    if (currentLocation != nil) {
//        NSString *latitute = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
//        NSString *longitute = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
//        NSLog(@"Latitute %@ and longitute %@",latitute,longitute);
//        
//    }
//    
//    [_locationManager stopUpdatingLocation];
//    
//}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = [locations lastObject];
    currentLocation = newLocation;
    NSLog(@"userLat: %f ,userLon:%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    
    NSLog(@" reading horizontal accuracy :%f", newLocation.horizontalAccuracy);
    [_locationManager stopUpdatingLocation];
    [self queryGooglePlaces:@"establishment"];

}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [_locationManager stopUpdatingLocation];

    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    
}

#pragma mark --

#pragma mark Google Places
-(void) queryGooglePlaces: (NSString *) googleType
{
    
    
    // Build the url string we are going to sent to Google. NOTE: The kGOOGLE_API_KEY is a constant which should contain your own API key that you can obtain from Google. See this link for more info:
    // https://developers.google.com/maps/documentation/places/#Authentication
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=%@&types=%@&sensor=true&key=%@",currentLocation.coordinate.latitude, currentLocation.coordinate.longitude,[NSString stringWithFormat:@"%i", 1609], googleType, kGOOGLE_API_KEY];
    
    //Formulate the string as URL object.
    NSURL *googleRequestURL=[NSURL URLWithString:url];
    
    // Retrieve the results of the URL.
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
        
      
    });
}
- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          
                          options:kNilOptions
                          error:&error];
    
    //The results from Google will be an array obtained from the NSDictionary object with the key "results".
    NSArray* data = [json objectForKey:@"results"];
    
    //Write out the data to the console.
    NSLog(@"Google Data: %@", data);
    if (arrayAllLocations == nil)
    arrayAllLocations = [[NSMutableArray alloc]initWithCapacity:0];
    
    
    //Loop through the array of places returned from the Google API.
    for (int i=0; i<[data count]; i++)
    {
        
        //Retrieve the NSDictionary object in each index of the array.
        NSDictionary* place = [data objectAtIndex:i];
        
        //There is a specific NSDictionary object that gives us location info.
        NSDictionary *geo = [place objectForKey:@"geometry"];
        
        
        //Get our name and address info for adding to a pin.
        NSString *name=[place objectForKey:@"name"];
        // NSString *vicinity=[place objectForKey:@"vicinity"];
        
        //Get the lat and long for the location.
        NSDictionary *loc = [geo objectForKey:@"location"];
        
        
        
        
        LocatioDetails *placeNew = [[LocatioDetails alloc]init];
        placeNew.yaacPlaceName = name;
        placeNew.yaacLatitute  =  [loc objectForKey:@"lat"];
        placeNew.yaacLongitute = [loc objectForKey:@"lng"];
        placeNew.yaccVicinity =[place objectForKey:@"vicinity"];
        ;
        
        
        [arrayAllLocations addObject:placeNew];
        placeNew = nil;
    }
    
    if ([customLocationArray count]) {
        
        for (NSArray *arr in customLocationArray) {
            LocatioDetails *placeNew = [[LocatioDetails alloc]init];
            placeNew.yaacPlaceName = [arr objectAtIndex:0];
            placeNew.yaacLatitute  =  [arr objectAtIndex:1];
            placeNew.yaacLongitute = [arr objectAtIndex:2];
            placeNew.yaccVicinity =@"Custom Location";
            [arrayAllLocations addObject:placeNew];
            placeNew = nil;


        }
    }
    if ([arrayAllLocations count])
        [table reloadData];
    
    if ([refreshLocation isSelected]) {
        [indicator stopAnimating];
        [refreshLocation setImage:[UIImage imageNamed:@"refresh-xxl"] forState:UIControlStateNormal];
        [refreshLocation setSelected:NO];
    }

    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    NSLog(@"began");  // this executes as soon as i tap on the searchbar, so I'm guessing this is the place to put whatever solution is available
}

- (void)searchTableList {
    NSString *searchString = search.text;
    
    for (LocatioDetails *tempStr in arrayAllLocations) {
        NSComparisonResult result = [[tempStr  yaacPlaceName] compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
        
        // NSComparisonResult result=[[tempStr objectAtIndex:0] caseInsensitiveCompare:searchString];
        if (result == NSOrderedSame) {
            [searchResults addObject:tempStr];
            NSLog(@"foundddd");
            
        }
        else{
            NSLog(@"not  foundddd");
            
            [table reloadData];
        }
        
        
    }
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"Text change - %d",isSearching);
    NSLog(@"Text change - %@",searchText);
    
    //Remove all objects first.
    [searchResults removeAllObjects];
    
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else {
        isSearching = NO;
    }
    
    [table reloadData];
    NSLog(@"TtblList reloadData]");
    
}
@end
