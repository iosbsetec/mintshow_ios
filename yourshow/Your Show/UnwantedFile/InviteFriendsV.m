//
//  InviteFriendsV.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 17/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "InviteFriendsV.h"
#import "MBProgressHUD.h"
#import "FriendsInfo.h"



#import "CellInvite.h"
#import "AppDelegate.h"

@interface InviteFriendsV () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    BOOL isSearch,isLoading,isDataFinishedInServer;
    NSMutableArray *selectedIndexes;
    UILabel *labelFilters;
    NSMutableArray * arrayFriendList,*serachArray;
    NSMutableArray *selectedRows;NSInteger selectedrow ;
    NSMutableArray * arrayPriviouslyFriendList;
    UIView *viewTop;
    int pageNum;
    
}
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton       *searchButton;
@property(nonatomic, strong) UITextField *textField;

@end

@implementation InviteFriendsV

@synthesize table,searchButton,textField,arrayFriendList;



-(id)showFilterListWithFrame : (CGRect) frame withTitle : (NSString *)title isCateGoryView : (BOOL) isCategory selectdList:(NSArray *)selectedFriends
{
    
    self.table = (UITableView *)[super init];
    if (self) {
        pageNum = 1;
        selectedrow = 200;
        isLoading = NO;
        isDataFinishedInServer = NO;
        serachArray = [NSMutableArray array];

        arrayFriendList = [NSMutableArray array];
        selectedIndexes = [NSMutableArray array];
        selectedRows    = [NSMutableArray array];


        viewTop = [[UIView alloc]initWithFrame:CGRectMake(0, 0,CGRectGetWidth(frame), 45)];
        [viewTop addSubview:[self addMyButton]];
        
        labelFilters = [[UILabel alloc] initWithFrame:CGRectMake(0, 14, [UIScreen mainScreen].bounds.size.width, 23)];
        [labelFilters setFont:[UIFont boldSystemFontOfSize:12]];
        labelFilters.textColor = [UIColor whiteColor];
        labelFilters.textAlignment = NSTextAlignmentCenter;
        labelFilters.font = [UIFont systemFontOfSize:17];
        [labelFilters setText:@"Invite"];
        
        [viewTop addSubview:labelFilters];
        
        textField               = [[UITextField alloc] initWithFrame:CGRectMake(5, 50, CGRectGetWidth(frame)-10, 30)];
        textField.borderStyle   = UITextBorderStyleRoundedRect;
        textField.font          = [UIFont systemFontOfSize:12];
        textField.placeholder   = @"Search";
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.keyboardType  = UIKeyboardTypeDefault;
        textField.returnKeyType = UIReturnKeyDefault;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [textField addTarget:self action:@selector(searchFriends:) forControlEvents:UIControlEventEditingChanged];
        textField.delegate      = self;
        [self addSubview:textField];
        
        
        
        self.searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.searchButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 60,12.0, 50.0, 27.0)];
        [self.searchButton setTitle:@"Done" forState:UIControlStateNormal];
        self.searchButton.enabled = NO;
        self.searchButton.backgroundColor = ORANGECOLOR;
        self.searchButton.layer.cornerRadius = 4.0;
        [self.searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
        [self.searchButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
        
        [self.searchButton addTarget:self action:@selector(searchTapped) forControlEvents:UIControlEventTouchUpInside];
        [viewTop addSubview:self.searchButton];

     
        
//        /* Section header is in 0th index... */
        UIColor * Backcolor         = [UIColor colorWithRed:9/255.0f green:26/255.0f blue:53/255.0f alpha:1.0f];
        [viewTop setBackgroundColor:Backcolor];
        self.backgroundColor        = [UIColor lightGrayColor];
        self.layer.masksToBounds    = NO;
        //  self.layer.cornerRadius = 8;
        self.layer.shadowRadius     = 5;
        self.layer.shadowOpacity    = 0.5;
        
    
        
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(5, 85, CGRectGetWidth(frame)-10, CGRectGetHeight(frame)-110)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        table.backgroundColor = [UIColor whiteColor];
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        //  table.separatorColor = [UIColor grayColor];
        
        AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        [appDell.window addSubview:self];
        self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
        [self setFrame:frame];
        [self addSubview:table];
        [self addSubview:viewTop];
        [self friendsList:frame];

    }
    return self;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if ([arrayFriendList count] != [serachArray count]) {
        
        [arrayFriendList removeAllObjects];
        [arrayFriendList addObjectsFromArray:serachArray];
        [table reloadData];
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textFields
{

    [textFields resignFirstResponder];
    return YES;

}
-(void)createDoneButton
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
               });
    
}
-(void)searchFriends : (UITextField *) txtField
{

    if([[txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]>0){
        
        NSString *searchString = txtField.text;

        NSPredicate *filter = [NSPredicate predicateWithFormat:@"name BEGINSWITH[cd] %@", searchString];
//        NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", searchString];
      NSArray*  resultArray = [[arrayFriendList filteredArrayUsingPredicate:filter] mutableCopy];
        
        
            if (resultArray.count == 0) {
                NSLog(@"No data From Search");
            }
        else

        {
        
            [arrayFriendList removeAllObjects];
            [arrayFriendList addObjectsFromArray:resultArray];
            [table reloadData];

        }
        
    } else {
        if ([arrayFriendList count] != [serachArray count]) {
      
        [arrayFriendList removeAllObjects];
        [arrayFriendList addObjectsFromArray:serachArray];
        [table reloadData];
        }
    }

}
-(void)searchTapped
{
    //NSString *strList = nil;
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"selected CONTAINS[cd] %@", @"1"];
    //        NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", searchString];
    NSArray*  resultArray = [[arrayFriendList filteredArrayUsingPredicate:filter] mutableCopy];
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (FriendsInfo *obj in resultArray) {
        
        [array addObject:obj];
    }
    
   // strList = [YSSupport convertToCommaSeparatedFromArray:array];
   [self.delegate friendsSelectdForInvite:array];

}

-(void)friendsList : (CGRect) frame
{
    dispatch_queue_t queue ;
    queue= dispatch_queue_create("com.yacc.CategoryQueue", NULL);
    dispatch_async(queue, ^{
        // [AppDelegate serverCall];
        [self getAllFriendsInCircle:pageNum];
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            
//            
//        });

    }
                   );
}
-(void)hideDropDown
{
    
    [self removeFromSuperview];
}
- (UIButton *)addMyButton
{    // Method for creating button, with background image and other properties
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame = CGRectMake(0.0,10.0, 30.0, 30.0);
    [playButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    
    // [searchButton setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    playButton.backgroundColor = [UIColor clearColor];
    [playButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
    [playButton addTarget:self action:@selector(hideDropDown) forControlEvents:UIControlEventTouchUpInside];
    return playButton;
}
-(UIView *)creatorDevider
{
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 310, 1)];/// change size as you need.
    separatorLineView.backgroundColor = SHOWROOMBACKGROUNDCOLOR;// you can also put image here
    return separatorLineView;
}


#pragma mark - UITableViewDelegate implementation


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  arrayFriendList.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    static NSString *CellIdentifier = @"CellInvite";
    CellInvite *cell = (CellInvite*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    }

    [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
    [cell.btnFollow addTarget:self action:@selector(inviteTapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.btnFollow.tag   = indexPath.row;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell addSubview:[self creatorDevider]];
    FriendsInfo *obj = [arrayFriendList objectAtIndex:indexPath.row];
    [cell setInfo:obj.profile_image_url userName:obj.name];
    if ([obj.selected integerValue]) {
        
        [cell.imgVCheck setImage:[UIImage imageNamed:@"check_withcircle"] ];
    }
    else
        [cell.imgVCheck setImage:[UIImage imageNamed:@"greyCircle.png"] ];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    searchButton.enabled = YES;
    
    // check_withcircle
    CellInvite *cell = (CellInvite *)[table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
    FriendsInfo *obj = [arrayFriendList objectAtIndex:indexPath.row];

    if (![obj.selected isEqualToString:@"1"]) {
        [cell.btnFollow setSelected:YES];
        [cell.imgVCheck setImage:[UIImage imageNamed:@"check_withcircle"] ];
//        FriendsInfo *obj = [arrayFriendList objectAtIndex:indexPath.row];
        obj.selected = @"1";
        [arrayFriendList    replaceObjectAtIndex:indexPath.row  withObject:obj];
        
    }
    else
    {
        [cell.btnFollow  setSelected:NO];
        [cell.imgVCheck setImage:[UIImage imageNamed:@"greyCircle.png"] ];
//        FriendsInfo *obj = [arrayFriendList objectAtIndex:indexPath.row];
        obj.selected = @"0";
        [arrayFriendList    replaceObjectAtIndex:indexPath.row withObject:obj];
        
    }
    
}


-(void)inviteTapped : (UIButton *)sender
{
    searchButton.enabled = YES;

    // check_withcircle
      CellInvite *cell = (CellInvite *)[table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    FriendsInfo *obj = [arrayFriendList objectAtIndex:sender.tag];

    if (![obj.selected isEqualToString:@"1"]) {
        [sender setSelected:YES];
        [cell.imgVCheck setImage:[UIImage imageNamed:@"check_withcircle"] ];
        // FriendsInfo *obj = [arrayFriendList objectAtIndex:sender.tag];
         obj.selected = @"1";
         [arrayFriendList    replaceObjectAtIndex:sender.tag withObject:obj];
        
    }
    else
    {
        [sender setSelected:NO];
        [cell.imgVCheck setImage:[UIImage imageNamed:@"greyCircle.png"] ];
       // FriendsInfo *obj = [arrayFriendList objectAtIndex:sender.tag];
        obj.selected = @"0";
        [arrayFriendList    replaceObjectAtIndex:sender.tag withObject:obj];
        
    }
    
}

- (void)getAllFriendsInCircle :(int)pageNumber
{
    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&page_number=%d",GETVALUE(CEO_AccessToken),GETVALUE(CEO_UserId),pageNumber] dataUsingEncoding:NSUTF8StringEncoding];

    [YSAPICalls getAllFriendsInCircle:submitData methodName:METHOD_FOLLOWERLIST ForSuccessionBlock:^(id newResponse) {
       // [MBProgressHUD hideHUDForView:self animated:YES];
        Generalmodel *model = newResponse;
        NSLog(@"response = %@",model.status_code);
        
        if ([model.status_code integerValue]) {
            
            isLoading = NO;
            
            if ([model.tempArray count] > 1) {
                isDataFinishedInServer = NO;
                [arrayFriendList addObjectsFromArray:model.tempArray];
                [serachArray addObjectsFromArray:model.tempArray];
                [self.table reloadData];

            }
            else
            {
                isDataFinishedInServer = YES;

            
            }
                         }
        
    } andFailureBlock:^(NSError *error) {

        
    }];
    
  
}

static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.70;

static BOOL ShouldLoadNextPage(UITableView *tblView)
{
    CGFloat yOffset = tblView.contentOffset.y;
    CGFloat height = tblView.contentSize.height - CGRectGetHeight(tblView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self.table);
    if (shouldLoadNextPage)  {
       
        
        if(!isLoading && !isDataFinishedInServer)
        {
            pageNum      = pageNum +1;
            isLoading        = YES;
           
            [self getAllFriendsInCircle:pageNum];
            
        }
        
    }
  
    
}


@end
