//
//  CuisineListTable.m
//  GebeChat
//
//  Created by Siba Prasad Hota  on 6/13/15.
//

#define arrayRightTableData @[@"SEARCH BY",@"CATEGORY"]
#define arraySearchBy       @[@"All Mints",@"My Mint",@"My Csuite",@"Popular",@"Remint",@"Nomintated"]
//#define arrayCategory       @[@"BeingHappy",@"Career",@"EnterPrenure",@"ExerCise",@"Family",@"Leadership",@"Health"]

#import "FilterListTable.h"
#import "Cell2.h"
#import "Cell1.h"
#import "AppDelegate.h"

@interface FilterListTable ()
{
    BOOL isSearch;
    NSMutableArray *selectedIndexes;
    UILabel *labelFilters;
    NSMutableArray * arrayCategory;
    NSMutableArray *selectedRows;NSInteger selectedrow ;
    UIButton *searchButton;
    
    NSInteger circleIdentifier, typeIdentifier, sort_by_option, sort_by_feature,category_id;
    NSString *selectedCateGoryName;
    BOOL toShowCategoryList;
    
}
@property(nonatomic, strong) UITableView *table;
@end

@implementation FilterListTable

@synthesize table;



-(id)showFilterListWithFrame : (CGRect) frame withTitle : (NSString *)title isCateGoryView : (BOOL) isCategory withPreviousSelection:(NSString *)cateGoryName cateGoryId:(NSString *)cateGoryId showcategoryList:(BOOL)showcategorylist
{
    self.table = (UITableView *)[super init];
    if (self) {
        
        self.filtertabletype = (isCategory)?FilterTypeCategorySelector:FilterTypeMintFilter;
        selectedCateGoryName = cateGoryName;
        selectedrow = ([cateGoryId integerValue])?[cateGoryId integerValue]:200;
        arrayCategory = [NSMutableArray array];
        selectedIndexes = [NSMutableArray array];
        selectedRows = [NSMutableArray array];
        toShowCategoryList = showcategorylist;
      
        UIView *viewTop = [[UIView alloc]initWithFrame:CGRectMake(0, 0,CGRectGetWidth(frame), 49)];
        [viewTop addSubview:[self addMyButton]];
        
        labelFilters = [[UILabel alloc] initWithFrame:CGRectMake(0, 14, [UIScreen mainScreen].bounds.size.width, 23)];
        [labelFilters setFont:[UIFont boldSystemFontOfSize:12]];
        labelFilters.textColor = [UIColor whiteColor];
        labelFilters.textAlignment = NSTextAlignmentCenter;
        labelFilters.font = [UIFont systemFontOfSize:20];
        NSString *string = title;
        [labelFilters setText:string];

        [viewTop addSubview:labelFilters];

        if (self.filtertabletype == FilterTypeMintFilter) {
            searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
            searchButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 70,12.0, 60.0, 27.0);
            [searchButton setTitle:(![labelFilters.text isEqualToString:@"Filters" ])
             ?@"Done":@"Apply" forState:UIControlStateNormal];
            searchButton.enabled = YES;
            searchButton.backgroundColor = ORANGECOLOR;
            searchButton.layer.cornerRadius = 4.0;
            [searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
            [searchButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:13.0]];
            [searchButton addTarget:self action:@selector(applyTapped) forControlEvents:UIControlEventTouchUpInside];
            [viewTop addSubview:searchButton];
            
            circleIdentifier = 0;
            typeIdentifier = 10;
            sort_by_option = 0;
            sort_by_feature = 0;

        }
                /* Section header is in 0th index...*/
        UIColor * Backcolor = NAVYBLUECOLOR;
        [viewTop setBackgroundColor:Backcolor];
        self.backgroundColor = [UIColor lightGrayColor];
        self.layer.masksToBounds = NO;
    //  self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(10, 50, CGRectGetWidth(frame)-20, CGRectGetHeight(frame)-50)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        table.backgroundColor = [UIColor clearColor];
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
    //  table.separatorColor = [UIColor grayColor];
        
        AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        [appDell.window addSubview:self];
        self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
        [self setFrame:frame];
        [self addSubview:table];
        [self addSubview:viewTop];
        NSString *path = [[NSBundle mainBundle] pathForResource:((toShowCategoryList)?@"CategoryPlist":@"MintTypePlist") ofType:@"plist"];
        arrayCategory = [[NSMutableArray alloc] initWithContentsOfFile:path];
        
        if (![arrayCategory count]) {
                [self getCategoryInbackGround];


        }
        
        for (int i=0;i<arrayCategory.count;i++) {
            [selectedRows addObject:[NSNumber numberWithInt:0]];
        }
    }
    return self;
}
-(void)getCategoryInbackGround
{
    dispatch_queue_t queue ;
    queue= dispatch_queue_create("com.yacc.CategoryQueue", NULL);
    dispatch_async(queue, ^{
        // [AppDelegate serverCall];
        if (toShowCategoryList)
        [self getAchievementCategory];
            else
                [self getMintsType];
  
    }
      );
}
-(void)hideDropDown
{
    [self removeFromSuperview];
}
-(void)applyTapped
{
    if (self.filtertabletype == FilterTypeMintFilter)
    {
        NSString * comma          = @"";
        NSMutableString *ItemList = [[NSMutableString alloc]init];
       
        for (NSDictionary *obj  in selectedIndexes) {
                [ItemList appendString:[NSString stringWithFormat:@"%@%@", comma, [obj valueForKey:@"achievement_category_id"]]];
                comma = @",";
              }
            [self.delegate FilterListTableDelegateMethodCategoryId:ItemList sort_by_option:sort_by_option sort_by_feature:sort_by_feature];
    }
    else
    {
        NSString *selectedTypeInfo = (toShowCategoryList)?[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"achievement_category_name"]:[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"type_name"];
        NSString *selectdId = (toShowCategoryList)?[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"achievement_category_id"]:[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"type_id"];

    [self.delegate FilterListTableDelegateMethod:selectedTypeInfo selectedId:selectdId];
    }
    
}
- (UIButton *)addMyButton
{    // Method for creating button, with background image and other properties
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame = CGRectMake(0.0,10.0, 30.0, 30.0);
    [playButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];

   // [searchButton setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    playButton.backgroundColor = [UIColor clearColor];
    [playButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
    [playButton addTarget:self action:@selector(hideDropDown) forControlEvents:UIControlEventTouchUpInside];
    return playButton;
}


#pragma mark - UITableViewDelegate implementation


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return (!(self.filtertabletype == FilterTypeMintFilter))?1: 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.filtertabletype == FilterTypeMintFilter) {
        return  (section == 0) ? arraySearchBy.count:arrayCategory.count;
    }  else {
        return  arrayCategory.count;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 43, CGRectGetWidth([UIScreen mainScreen].bounds), 1)];
    separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    separatorView.alpha = 0.5;
    separatorView.layer.borderWidth = 0.5;
    
    static NSString *CellIdentifier = @"Cell2";
    Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    }
    
    [cell.btnCheckMarck setImage:[UIImage imageNamed:@"greyCircle.png"] forState:UIControlStateNormal];
    
    if (self.filtertabletype == FilterTypeMintFilter) {
        if (indexPath.row == 0) {
            UIBezierPath *maskPath;
            maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds
                                             byRoundingCorners:(UIRectCornerTopRight|UIRectCornerTopLeft)
                                                   cornerRadii:CGSizeMake(7.0, 7.0)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = cell.bounds;
            maskLayer.path = maskPath.CGPath;
            cell.layer.mask = maskLayer;
            [cell.contentView addSubview:separatorView];
            separatorView.hidden = NO;
        }
        else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1) {
            separatorView.hidden = YES;
            UIBezierPath *maskPath;
            maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds
                                             byRoundingCorners:(UIRectCornerBottomRight|UIRectCornerBottomLeft)
                                                   cornerRadii:CGSizeMake(7.0, 7.0)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = cell.bounds;
            maskLayer.path = maskPath.CGPath;
            cell.layer.mask = maskLayer;
        }
        else {
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            [cell.contentView addSubview:separatorView];
            separatorView.hidden = NO;

        }
        
        if (indexPath.section == 0) {
            cell.titleLabel.text = [arraySearchBy objectAtIndex:indexPath.row];
            cell.tag = 4000;
        }
        else if (indexPath.section == 1)  {
            cell.titleLabel.text = [[arrayCategory objectAtIndex:indexPath.row] valueForKey:@"achievement_category_name"] ;
            
        }
        
        
        if (indexPath.section==1&&([[selectedRows objectAtIndex:indexPath.row] integerValue] == 1)) {
           [cell.btnCheckMarck setImage:[UIImage imageNamed:@"check_withcircle"] forState:UIControlStateNormal];
        }else if (indexPath.section==0 && (indexPath.row == circleIdentifier || indexPath.row == typeIdentifier) ){
           [cell.btnCheckMarck setImage:[UIImage imageNamed:@"check_withcircle"] forState:UIControlStateNormal];
        }
        
    } else{


        NSString *cateGoryName     = (toShowCategoryList)?[[arrayCategory objectAtIndex:indexPath.row] valueForKey:@"achievement_category_name"]:[[arrayCategory objectAtIndex:indexPath.row] valueForKey:@"type_name"]; ;
        cell.titleLabel.text = cateGoryName;
        NSLog(@"selectedRow %ld",(long)selectedrow);
        if ([cateGoryName isEqualToString:selectedCateGoryName]) {
            [cell.btnCheckMarck setImage:[UIImage imageNamed:@"check_withcircle"] forState:UIControlStateNormal];
        }
    }
    cell.btnCheckMarck.tag   = indexPath.row;
    [cell.contentView addSubview:separatorView];

    //[cell.btnCheckMarck setTitle:[NSString stringWithFormat:@"%d",(int)indexPath.section] forState:UIControlStateNormal];
   // [cell.btnCheckMarck setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    cell.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    cell.titleLabel.textColor=[UIColor blackColor];
    [cell.btnCheckMarck addTarget:self action:@selector(checkTapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self changeButtonBackgroundforRow:indexPath.row  andSection:indexPath.section];
    [table deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)checkTapped : (UIButton *)sender{
    NSInteger mysection = [[sender titleForState:UIControlStateNormal] integerValue];
    NSInteger myrow = [sender tag];
    [self changeButtonBackgroundforRow:myrow andSection:mysection];
}

-(void)changeButtonBackgroundforRow:(NSInteger)myrow andSection:(NSInteger)mysection{
    if (self.filtertabletype == FilterTypeMintFilter){
        if (mysection == 0){
            if (myrow >2) {
                typeIdentifier = myrow;
                sort_by_feature = myrow -2;
            } else
            {
                circleIdentifier = myrow;
                sort_by_option = myrow;
            }
            NSRange range = NSMakeRange(mysection, 1);
            NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
            [self.table reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
        }else{
            int value = ([[selectedRows objectAtIndex:myrow] integerValue] == 1)?0:1;
            [selectedRows    replaceObjectAtIndex:myrow withObject:[NSNumber numberWithInteger:value]];
            if(!value)
                [selectedIndexes removeObject:[arrayCategory objectAtIndex:myrow]];
            else
                [selectedIndexes addObject:[arrayCategory objectAtIndex:myrow]];
            [self.table reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:myrow inSection:mysection], nil] withRowAnimation:UITableViewRowAnimationNone];
        }
    }else{
        selectedrow =myrow;
        NSString *selectedTypeInfo = (toShowCategoryList)?[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"achievement_category_name"]:[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"type_name"];
        NSString *selectdId = (toShowCategoryList)?[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"achievement_category_id"]:[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"type_id"];
        
        [self.delegate FilterListTableDelegateMethod:selectedTypeInfo selectedId:selectdId];
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
    
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, tableView.frame.size.width, 18)];
    [label setFont:[UIFont fontWithName:@"Helvetica Neue" size:14.0]];
    NSString *string =[arrayRightTableData objectAtIndex:section];
    
    /* Section header is in 0th index... */
    label.textColor=[UIColor darkGrayColor];
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:BACKGROUNDCOLOR]; //your background color...
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (self.filtertabletype == FilterTypeMintFilter)? 35.0:0.0;
}


-(void)listDelegateCalledAfterDelay {
    NSString *selectedTypeInfo = (toShowCategoryList)?[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"achievement_category_name"]:[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"type_name"];
    NSString *selectdId = (toShowCategoryList)?[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"achievement_category_id"]:[[arrayCategory objectAtIndex:selectedrow] valueForKey:@"type_id"];
    
    
    [self.delegate FilterListTableDelegateMethod:selectedTypeInfo selectedId:selectdId];
}








-(void)getPlistDta : (NSArray *)arrayCat
{
    NSLog(@"my plist  Clicked");
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    
    // get paths from root direcory
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    
    // get documents path
    
    NSString *documentsPath = [paths objectAtIndex:0];
    
    // get the path to our Data/plist file
    
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"CategoryPlist.plist"];
    
    
    //This copies objects of plist to array if there is one
    
    [array addObjectsFromArray:[NSArray arrayWithContentsOfFile:plistPath]];
    NSLog(@"catArray details %@",array);
    
    // [array addObject:searchLabel.text];
    
    // This writes the array to a plist file. If this file does not already exist, it creates a new one.
    
    [arrayCat writeToFile:plistPath atomically: TRUE];
    
}

- (void)getAchievementCategory
{
    NSString *url = [NSString stringWithFormat:@"/achievements/get_categories?access_token=%@",GETVALUE(CEO_AccessToken)];
    NSLog(@"My URL =%@",url);
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,url]]];
    [NSURLConnection sendAsynchronousRequest:submitrequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
             NSLog(@"json data=%@",json);
             if ([[NSString stringWithFormat:@"%@",[json objectForKey:@"status"]] isEqualToString:@"true"])
             {
                 //  [AllCategoryArray removeAllObjects];
                 NSArray *catArray= (NSArray*)[json objectForKey:@"data"];
                 NSArray *mintTypeArray = (NSArray*)[json objectForKey:@"mint_types"];
                 NSLog(@"catArray details %@",mintTypeArray);
                 
                 if ([catArray isKindOfClass:[NSArray class]]||[catArray isKindOfClass:[NSMutableArray class]])
                 {
                     [self getPlistDta:catArray];
                 }
                 if ([mintTypeArray isKindOfClass:[NSArray class]]||[mintTypeArray isKindOfClass:[NSMutableArray class]])
                 {
                    // [self getPlistMintType:mintTypeArray];
                 }
             }
         }
         else
         {
             
         }
     }];
}

- (void)getMintsType
{
  //  GETGet Mint type/customshowroom/get_mint_types
    
    
    NSString *url = [NSString stringWithFormat:@"/customshowroom/get_mint_types?access_token=%@",GETVALUE(CEO_AccessToken)];
    NSLog(@"My URL =%@",url);
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,url]]];
    [NSURLConnection sendAsynchronousRequest:submitrequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
             NSLog(@"json data=%@",json);
             if ([[NSString stringWithFormat:@"%@",[json objectForKey:@"status"]] isEqualToString:@"true"])
             {
                 //  [AllCategoryArray removeAllObjects];
                 NSArray *catArray= (NSArray*)[json objectForKey:@"data"];
                 NSArray *mintTypeArray = (NSArray*)[json objectForKey:@"mint_types"];
                 NSLog(@"catArray details %@",mintTypeArray);
                 
                 if ([catArray isKindOfClass:[NSArray class]]||[catArray isKindOfClass:[NSMutableArray class]])
                 {
                     [self getPlistDta:catArray];
                 }
                 if ([mintTypeArray isKindOfClass:[NSArray class]]||[mintTypeArray isKindOfClass:[NSMutableArray class]])
                 {
                     // [self getPlistMintType:mintTypeArray];
                 }
             }
         }
         else
         {
             
         }
     }];
}


@end
