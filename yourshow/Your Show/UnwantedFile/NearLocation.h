//
//  NearLocation.h
//  Your Show
//
//  Created by JITENDRA on 8/13/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LocatioDetails;
@class NearLocation;
@protocol NearLocationTableDelegate
- (void) NearLocationTableDelegateMethod: (LocatioDetails *) sender stringWithCustomText :(NSString *) customLocation;
@end

@interface NearLocation : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic, retain) id <NearLocationTableDelegate> delegate;

-(void)hideDropDown;
-(id)showFilterListWithFrame : (CGRect) frame;
@end
