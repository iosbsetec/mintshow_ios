
#import <Foundation/Foundation.h>
//#import "CatModel.h"




@class FilterListTable;

@protocol FilterTableViewDataSource <NSObject>

@optional

@required

- (NSInteger)rowsForBubbleTable:(FilterListTable *)tableView;
- (NSString *)bubbleTableView:(FilterListTable *)tableView dataForRow:(NSInteger)row;

@end
