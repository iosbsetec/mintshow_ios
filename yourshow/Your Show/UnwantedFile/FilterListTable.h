//
//  CuisineListTable.h
//  GebeChat
//
//  Created by Siba Prasad Hota  on 6/13/15.
//  Copyright (c) 2015 WemakeAppz. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    FilterTypeMintFilter,
    FilterTypeCategorySelector
} FilterType;

@class FilterListTable;
@protocol FilterListTableDelegate
- (void) FilterListTableDelegateMethod:(NSString *)sender selectedIndex:(NSInteger) selectedRowNo;
- (void) FilterListTableDelegateMethod:(NSString *)text selectedId :(NSString *) iddee;
- (void) FilterListTableDelegateMethodCategoryId:(NSString *)text sort_by_option:(NSInteger )sort_by_option sort_by_feature:(NSInteger)sort_by_feature;


@end

@interface FilterListTable : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic, retain) id <FilterListTableDelegate> delegate;
@property (nonatomic, assign) FilterType filtertabletype;

@property (assign) BOOL isCategoryView;

-(void)hideDropDown;
-(id)showFilterListWithFrame : (CGRect) frame withTitle : (NSString *)title isCateGoryView : (BOOL) isCategory withPreviousSelection:(NSString *)cateGoryName cateGoryId:(NSString *)cateGoryId showcategoryList:(BOOL)showcategorylist;

@end


