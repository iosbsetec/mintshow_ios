//
//  LocatioDetails.h
//  YACC
//
//  Created by JITEN on 12/15/14.
//  Copyright (c) 2014 BseTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocatioDetails : NSObject
{
    NSString                    *yaacPlaceName;
    NSString                    *yaacLatitute;
    NSString                    *yaacLongitute;
    NSString                    *yaccVicinity;
    
    
}

@property(nonatomic, copy)    NSString                     *yaacPlaceName;
@property(nonatomic, copy)    NSString                     *yaacLatitute;
@property(nonatomic, copy)    NSString                     *yaacLongitute;
@property(nonatomic, copy)    NSString                     *yaccVicinity;


@end
