//
//  InviteFriendsV.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 17/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol InviteFriendsVTableDelegate
- (void) friendsSelectdForInvite:(NSArray *)memberListVal;

@end


@interface InviteFriendsV : UIView
@property (nonatomic, assign) id <InviteFriendsVTableDelegate> delegate;
@property(nonatomic,strong) NSMutableArray * arrayFriendList;

-(void)hideDropDown;
-(id)showFilterListWithFrame : (CGRect) frame withTitle : (NSString *)title isCateGoryView : (BOOL) isCategory selectdList:(NSArray *)selectedFriends;
@end



@protocol InviteFriendsVTableDelegate
- (void) friendsSelectdForInvite:(NSArray *)memberListVal;
@end


