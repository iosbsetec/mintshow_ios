#import "AsyncImageView.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "YSProgressView.h"

#define TAG_PROGRESS_VIEW 149462


@implementation UIImageView (ProgressView)

- (void)addProgressView:(YSProgressView *)progressView {
    YSProgressView *existingProgressView = (YSProgressView *)[self viewWithTag:TAG_PROGRESS_VIEW];
    if (!existingProgressView) {
        if (!progressView) {
            progressView = [[YSProgressView alloc] init];
        }
        
        progressView.tag = TAG_PROGRESS_VIEW;
        progressView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        
        float width = 52;
        float height = 52;
        float x = (self.frame.size.width / 2.0) - width/2;
        float y = (self.frame.size.height / 2.0) - height/2;
        progressView.frame = CGRectMake(x, y, width, height);
        
        [self addSubview:progressView];
    }
}

- (void)updateProgress:(CGFloat)progress {
    YSProgressView *progressView = (YSProgressView *)[self viewWithTag:TAG_PROGRESS_VIEW];
    if (progressView) {
        [progressView setProgress:progress animated:YES];
    }
}

- (void)removeProgressView {
    YSProgressView *progressView = (YSProgressView *)[self viewWithTag:TAG_PROGRESS_VIEW];
    if (progressView) {
        [progressView removeFromSuperview];
    }
}

- (void)sd_setImageWithURL:(NSURL *)url usingProgressView:(YSProgressView *)progressView {
    [self sd_setImageWithURL:url placeholderImage:nil options:0 progress:nil completed:nil usingProgressView:progressView];
}

- (void)sd_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder usingProgressView:(YSProgressView *)progressView {
    [self sd_setImageWithURL:url placeholderImage:placeholder options:0 progress:nil completed:nil usingProgressView:progressView];
}

- (void)sd_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options usingProgressView:(YSProgressView *)progressView{
    [self sd_setImageWithURL:url placeholderImage:placeholder options:options progress:nil completed:nil usingProgressView:progressView];
}

- (void)sd_setImageWithURL:(NSURL *)url completed:(SDWebImageCompletionBlock)completedBlock usingProgressView:(YSProgressView *)progressView {
    [self sd_setImageWithURL:url placeholderImage:nil options:0 progress:nil completed:completedBlock usingProgressView:progressView];
}

- (void)sd_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder completed:(SDWebImageCompletionBlock)completedBlock usingProgressView:(YSProgressView *)progressView {
    [self sd_setImageWithURL:url placeholderImage:placeholder options:0 progress:nil completed:completedBlock usingProgressView:progressView];
}

- (void)sd_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options completed:(SDWebImageCompletionBlock)completedBlock usingProgressView:(YSProgressView *)progressView {
    [self sd_setImageWithURL:url placeholderImage:placeholder options:options progress:nil completed:completedBlock usingProgressView:progressView];
}

- (void)sd_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options progress:(SDWebImageDownloaderProgressBlock)progressBlock completed:(SDWebImageCompletionBlock)completedBlock usingProgressView:(YSProgressView *)progressView {
    [self addProgressView:progressView];
    
    __weak typeof(self) weakSelf = self;
    
    [self sd_setImageWithURL:url placeholderImage:placeholder options:options progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        CGFloat progress = ((CGFloat)receivedSize / (CGFloat)expectedSize);
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf updateProgress:progress];
        });
        
        if (progressBlock) {
            progressBlock(receivedSize, expectedSize);
        }
    }
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                       [weakSelf removeProgressView];
                       if (completedBlock) {
                           completedBlock(image, error, cacheType, imageURL);
                       }
                   }];
}

@end



@implementation UIImageView(AsyncImageView)

- (void)setNewimageURL:(NSURL *)imageURL {

    CGRect progressBarFrame = CGRectMake(0, 0, 60, 60);
    YSProgressView *ysprogressView = [[YSProgressView alloc] initWithFrame:progressBarFrame];
    [ysprogressView setCenter:self.center];
    self.alpha = 1.0;
    [self sd_setImageWithURL:imageURL usingProgressView:ysprogressView];
}

- (NSURL *)newimageURL {
    return self.newimageURL;
}

- (NSURL *)mintImageURL {
    return self.mintImageURL;
}

- (void)setMintImageURL:(NSURL *)imageURL {
    // Here we use the new provided sd_setImageWithURL: method to load the web image
    [self sd_setImageWithURL:imageURL
            placeholderImage:nil
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                       // self.image = image;
                       self.alpha = 1.0;
                   }];
}


@end


@interface AsyncImageView ()


@end


@implementation AsyncImageView
@end


/*
 [[PINCache sharedCache] objectForKey:[imageURL absoluteString]
 block:^(PINCache *cache, NSString *key, id object) {
 if (object) {
 [self setImageOnMainThread:(UIImage *)object];
 return;
 }
 
 UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:imageURL]];
 [self setImageOnMainThread:image];
 [[PINCache sharedCache] setObject:image forKey:[imageURL absoluteString]];
 }];
 
 - (void)setImageOnMainThread:(UIImage *)image
 {
 if (!image)
 return;
 dispatch_async(dispatch_get_main_queue(), ^{
 self.image = image;
 self.alpha = 1.0;
 });
 }
 
 */


