//
//  SPHCacheImageView.h
//  Your Show
//
//  Created by Siba Prasad Hota on 02/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImageView(SPHCacheImageView)

@property (nonatomic, strong) NSURL *mintImageURL;

@end

@interface SPHCacheImageView : UIImageView

@end
