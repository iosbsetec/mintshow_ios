//
//  SPHCacheImageView.m
//  Your Show
//
//  Created by Siba Prasad Hota on 02/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "SPHCacheImageView.h"
#import <objc/message.h>
#import <QuartzCore/QuartzCore.h>
#import <Availability.h>
#import "Haneke.h"
#import "PINCache.h"


#if !__has_feature(objc_arc)
#error This class requires automatic reference counting
#endif

@implementation UIImageView(AsyncImageView)

- (void)setMintImageURL:(NSURL *)imageURL
{
    [self hnk_setImageFromURL:imageURL];
}

- (void)setPNImageURL:(NSURL *)url{
    
    [[PINCache sharedCache] objectForKey:[url absoluteString]
                                   block:^(PINCache *cache, NSString *key, id object) {
                                       if (object) {
                                           [self setImageOnMainThread:(UIImage *)object];
                                           return;
                                       }
                                       
                                       NSLog(@"cache miss, requesting %@", url);
                                       
                                       NSURLResponse *response = nil;
                                       NSURLRequest *request = [NSURLRequest requestWithURL:url];
                                       NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
                                       
                                       UIImage *image = [[UIImage alloc] initWithData:data scale:[[UIScreen mainScreen] scale]];
                                       [self setImageOnMainThread:image];
                                       
                                       [[PINCache sharedCache] setObject:image forKey:[url absoluteString]];
                                   }];
}


- (void)setImageOnMainThread:(UIImage *)image
{
    if (!image)
        return;
    NSLog(@"setting view image %@", NSStringFromCGSize(image.size));
    dispatch_async(dispatch_get_main_queue(), ^{
        self.image = image;
    });
}



@end


@implementation SPHCacheImageView



- (id)initWithFrame:(CGRect)frame{
    if ((self = [super initWithFrame:frame]))  {
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder]))  {
    }
    return self;
}

- (void)setMintImageURL:(NSURL *)imageURL{
    [self hnk_setImageFromURL:imageURL];
}

- (void)setImage:(UIImage *)image {
    super.image = image;
}

- (void)dealloc
{
}

@end
