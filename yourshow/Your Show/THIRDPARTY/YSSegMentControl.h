//
//  YSSegMentControl.h
//  Your Show
//
//  Created by JITENDRA on 8/4/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YSSegMentControl;

typedef void (^IndexChangeBlock)(NSInteger index);
typedef NSAttributedString *(^YSTitleFormatterBlock)(YSSegMentControl *segmentedControl, NSString *title, NSUInteger index, BOOL selected);




typedef enum {
    YSSegmentedControlSelectionIndicatorLocationUp,
    YSSegmentedControlSelectionIndicatorLocationDown,
    YSSegmentedControlSelectionIndicatorLocationNone // No selection indicator
} YSSegmentedControlSelectionIndicatorLocation;

typedef enum {
    YSSegmentedControlSegmentWidthStyleFixed, // Segment width is fixed
    YSSegmentedControlSegmentWidthStyleDynamic, // Segment width will only be as big as the text width (including inset)
} YSSegmentedControlSegmentWidthStyle;


typedef NS_OPTIONS(NSInteger, YSSegmentedControlBorderType) {
    YSSegmentedControlBorderTypeNone = 0,
    YSSegmentedControlBorderTypeTop = (1 << 0),
    YSSegmentedControlBorderTypeLeft = (1 << 1),
    YSSegmentedControlBorderTypeBottom = (1 << 2),
    YSSegmentedControlBorderTypeRight = (1 << 3)
};


enum {
    YSSegmentedControlNoSegment = -1   // Segment index for no selected segment
};


@interface YSSegMentControl : UIControl

@end
