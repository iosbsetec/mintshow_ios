//
//  MSGlobalSearchVCViewController.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 19/04/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//
#define SECTIONARRAY       @[@"",@"PEOPLE"]


#import "MSGlobalSearchVCViewController.h"
#import "CellFollowers.h"
#import "CellGlobalSearchPeoples.h"
#import "SerchResultQueryVC.h"

@interface MSGlobalSearchVCViewController () <SerchResultQueryDelegate>
{
    NSMutableArray * tempArrayFriendList,*tempArrayPeopleList;

    NSMutableArray * arrayFriendList,*serachArray;
    NSMutableArray * arrayPeopleList;
    NSString       * searchTxt;
    BOOL             isPeopelMore,isDefaultKeywordMore, isSeeMoreClicked,isDataFinishedInServer;
    UILabel        * ttitle ;
    NSString       * strMorelinkType;
    NSString       * searchString;
    NSInteger        pageNumber;
}
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, strong) NSTimer *searchCountdownTimer;
@property (strong, nonatomic) NSArray *arrayList;
-(id)showFilterListWithFrame : (CGRect) frame withTextToSearch : (NSString *)serachText;
-(void)serachText: (NSString *)searchString;

@end



@implementation MSGlobalSearchVCViewController
@synthesize searchCountdownTimer = _searchCountdownTimer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    arrayFriendList = [NSMutableArray array];
    arrayPeopleList = [NSMutableArray array];
    isDefaultKeywordMore = NO;
    isPeopelMore = NO;
    isDataFinishedInServer = NO;
    [_searchTextField becomeFirstResponder];
    
    _table.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
  
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    AppDelegate *del = MINTSHOWAPPDELEGATE;
    del.isGlobalSearch = YES;

}
-(void)viewDidDisappear:(BOOL)animated
{

    [super viewDidAppear:animated];
    AppDelegate *del = MINTSHOWAPPDELEGATE;
    del.isGlobalSearch = NO;
}
- (IBAction)backTapped:(UIButton *)sender {
    [self.view endEditing:YES];
    
    if ([_searchTextField.text length] == 0) {
        [self.navigationController popViewControllerAnimated:NO];
        return;
    }


    if (isSeeMoreClicked) {
        isSeeMoreClicked = NO;
        [arrayFriendList removeAllObjects];
        [arrayPeopleList removeAllObjects];
       
        if ([tempArrayFriendList count])
        [arrayFriendList addObjectsFromArray:tempArrayFriendList];
        
        if ([tempArrayPeopleList count])
        [arrayPeopleList addObjectsFromArray:tempArrayPeopleList];
        [tempArrayPeopleList removeAllObjects];
        [tempArrayFriendList removeAllObjects];
        [_table reloadData];
        
    }
    else
        [self.navigationController popViewControllerAnimated:NO];
}
-(void)cancelSearchResultQuery
{
    searchTxt = nil;
    isDefaultKeywordMore = NO;
    isPeopelMore = NO;
    isSeeMoreClicked = NO;
    [tempArrayPeopleList removeAllObjects];
    [tempArrayFriendList removeAllObjects];
    [arrayFriendList removeAllObjects];
    [arrayPeopleList removeAllObjects];
    [_table reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)globalSearch:(UITextField *)textField{
    NSString *serach = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (textField.text.length == 0) {
        _table.hidden  = YES;
    }
    else
    {
        [self serachText:serach];
    }
}
-(NSMutableAttributedString  *)myfilteredStringWithFullString:(NSString  *)originalText{
    
    NSMutableAttributedString *mainString = [[NSMutableAttributedString alloc] initWithString:[originalText lowercaseString]];
    if (originalText.length == 0 || searchTxt.length == 0) {
        return mainString;
    }
    
    NSUInteger length = [originalText length];
    NSRange range = NSMakeRange(0, length);
    while(range.location != NSNotFound)
    {
        //range = [originalText rangeOfString:[searchTxt lowercaseString] options:0 range:range];
        range = [[originalText lowercaseString] rangeOfString:[searchTxt lowercaseString] options:0 range:range];
        if(range.location != NSNotFound)
        {
            range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
           // NSLog(@"the first range location = %lu",(unsigned long)range.location);
            [mainString addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(range.location-[searchTxt length],[searchTxt length])];
        }
    }
    // do something with substrings
    return mainString;
}

-(void)serachText: (NSString *)searchStringVal
{
    //stop the current countdown
    if (self.searchCountdownTimer) {
        [self.searchCountdownTimer invalidate];
    }
    searchTxt = searchStringVal;
    NSDate *fireDate = [NSDate dateWithTimeIntervalSinceNow:1.0];
    // add search data to the userinfo dictionary so it can be retrieved when the timer fires
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          searchStringVal, @"searchString",
                          nil];
    self.searchCountdownTimer = [[NSTimer alloc] initWithFireDate:fireDate
                                                         interval:0
                                                           target:self
                                                         selector:@selector(searchCountdownTimerFired:)
                                                         userInfo:info
                                                          repeats:NO];
    
    [[NSRunLoop mainRunLoop] addTimer:self.searchCountdownTimer forMode:NSDefaultRunLoopMode];
}
- (void)searchCountdownTimerFired:(NSTimer *)countdownTimer {
    
    searchString = [countdownTimer.userInfo objectForKey:@"searchString"];
   // NSLog(@"searchString %@",[countdownTimer.userInfo objectForKey:@"searchString"]);
    
    
    if ( ([searchString rangeOfString:@"*"].location!=NSNotFound) || ([searchString rangeOfString:@"#"].location!=NSNotFound) || ([searchString rangeOfString:@"@"].location!=NSNotFound)) {
        [arrayFriendList removeAllObjects];
        [arrayPeopleList removeAllObjects];
        [self showNoMoreMints];
        isDefaultKeywordMore = NO;
        isPeopelMore = NO;
        isSeeMoreClicked = NO;
        [_table reloadData];
        return;
    }
    
    

    [self getSearchSuggestion:searchString delete:YES];
    
}
-(void)getSearchSuggestion:(NSString *)text delete:(BOOL)needTodelete
{
    //more_link_type..page_number//'default_words' / 'showrooms' / 'people' / 'hashtags'

    if ([_searchTextField.text length]== 0) {
        return;
    }
    self.table.tableHeaderView = nil;

    NSData*  submitData  = nil;
    if (isSeeMoreClicked) {
         submitData    = [[NSString stringWithFormat:@"access_token=%@&search_text=%@&more_link_type=%@&page_number=%d",GETVALUE(CEO_AccessToken),[YSSupport escapedValue:text],strMorelinkType,pageNumber] dataUsingEncoding:NSUTF8StringEncoding];
    }
    else
    submitData    = [[NSString stringWithFormat:@"access_token=%@&search_text=%@",GETVALUE(CEO_AccessToken),[YSSupport escapedValue:text]] dataUsingEncoding:NSUTF8StringEncoding];
    
    [MBProgressHUD hideHUDForView:self.table animated:YES];
    [MBProgressHUD showHUDAddedTo:self.table animated:YES];
    [YSAPICalls getSuggestionList:submitData ForSuccessionBlock:^(id newResponse) {
        [MBProgressHUD hideHUDForView:self.table animated:YES];
        Generalmodel *gmodel = newResponse;
        
        if ([gmodel.status_code integerValue])  {
            
            isDataFinishedInServer = ([gmodel.status_Msg isEqualToString:@"No result Found"])?YES:NO;
            self.table.hidden  = NO;

            if (needTodelete) {
            [arrayFriendList removeAllObjects];
            [arrayPeopleList removeAllObjects];
            }
            if (!isSeeMoreClicked) {
               isDefaultKeywordMore = ([[gmodel.serchDefaultWords valueForKey:@"is_more_link"] integerValue])?YES:NO;
               isPeopelMore         = ([[gmodel.searchPeople valueForKey:@"is_more_link"] integerValue])?YES:NO;
            }

            if ([gmodel.status_Msg isEqualToString:@"No result Found"] && !isSeeMoreClicked) {
                [_table reloadData];
                [self showNoMoreMints];
                return;
            }
            
            if ([[gmodel.serchDefaultWords valueForKey:@"data"] count]) {
              //  isDefaultKeywordMore = ([[gmodel.serchDefaultWords valueForKey:@"is_more_link"] integerValue])?YES:NO;
                [arrayFriendList addObjectsFromArray:[gmodel.serchDefaultWords valueForKey:@"data"]];
            }
            
            if ([[gmodel.searchPeople valueForKey:@"data"] count]) {
               // isPeopelMore = ([[gmodel.searchPeople valueForKey:@"is_more_link"] integerValue])?YES:NO;
                [arrayPeopleList addObjectsFromArray:[gmodel.searchPeople valueForKey:@"data"]];
            }
            
            [_table reloadData];
        }
        
    } andFailureBlock:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.table animated:YES];
    }];
    
}
#pragma mark - UITableViewDataSource/Delegate implementation


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSeeMoreClicked) {
         return (section == 0)? [arrayFriendList count]:[arrayPeopleList count];
    }
    return (section == 0)? ((isDefaultKeywordMore)?[arrayFriendList count]+1:[arrayFriendList count]):((isPeopelMore)?[arrayPeopleList count]+1:[arrayPeopleList count]);
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-20, 20)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 1, tableView.frame.size.width, 18)];
    [label setFont:[UIFont fontWithName:@"Helvetica Neue" size:13.0]];
    NSString *string =[SECTIONARRAY objectAtIndex:section];
    
    /* Section header is in 0th index... */
    label.textColor =[UIColor darkGrayColor];
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]]; //your background color...
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return (section == 0)? 0:(([arrayPeopleList count])?20:0);
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(10, 40, CGRectGetWidth([UIScreen mainScreen].bounds)-20, 1)];
    separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    separatorView.alpha = 0.5;
    separatorView.layer.borderWidth = 0.5;
    
    if (indexPath.section == 0 && [arrayFriendList count])
    {
        if (arrayFriendList.count == indexPath.row && isDefaultKeywordMore && !isSeeMoreClicked) {
            
            static NSString *CellSeeMore = @"SeeMore";
            CellGlobalSearchPeoples *cell  = (CellGlobalSearchPeoples *)[self.table dequeueReusableCellWithIdentifier:CellSeeMore];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellSeeMore owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            [cell.btnSeeMore setTitle:@"See more" forState:UIControlStateNormal];
            cell.btnSeeMore.tag = indexPath.section;
            [cell.btnSeeMore addTarget:self action:@selector(seeMore:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        
        static NSString *CellIdentifierNormal = @"CellIdentifierNormal";
        UITableViewCell *cell  = (UITableViewCell *)[self.table dequeueReusableCellWithIdentifier:CellIdentifierNormal];
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierNormal];
        }
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:14.0]];
        [cell.textLabel setTextColor:[UIColor darkGrayColor]];
        
        
        NSDictionary *resultDict = [arrayFriendList objectAtIndex:indexPath.row] ;
        NSMutableAttributedString *colouredString = [self myfilteredStringWithFullString:[resultDict valueForKey:@"text"]] ;
        cell.textLabel.attributedText =  colouredString;
        [cell.contentView addSubview:separatorView];
        return cell;
    }
    else  {
        if (arrayPeopleList.count == indexPath.row && isPeopelMore && !isSeeMoreClicked) {
            
            static NSString *CellSeeMore = @"SeeMore";
            CellGlobalSearchPeoples *cell  = (CellGlobalSearchPeoples *)[self.table dequeueReusableCellWithIdentifier:CellSeeMore];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellSeeMore owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            [cell.btnSeeMore setTitle:@"See more People" forState:UIControlStateNormal];
            cell.btnSeeMore.tag = indexPath.section;
            [cell.btnSeeMore addTarget:self action:@selector(seeMore:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        
        static NSString *CellIdentifierNormal = @"CellGlobalSearchPeoples";
        CellGlobalSearchPeoples *cell  = (CellGlobalSearchPeoples *)[self.table dequeueReusableCellWithIdentifier:CellIdentifierNormal];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifierNormal owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        if ([arrayPeopleList count]) {

        NSDictionary *resultDict = [arrayPeopleList objectAtIndex:indexPath.row] ;
        NSMutableAttributedString *colouredString = [self myfilteredStringWithFullString:[resultDict valueForKey:@"user_name"]] ;
        cell.lblfllw.attributedText = colouredString;
        cell.fllwimgV.mintImageURL = [NSURL URLWithString:[resultDict valueForKey:@"user_image"]];
        [cell.contentView addSubview:separatorView];
        }
        return cell;
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.table deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
    
    // here we get the cell from the selected row.
    if (indexPath.section == 0) {
        NSString *string = [[[arrayFriendList objectAtIndex:indexPath.row] valueForKey:@"text"] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
        NSArray *names = [string componentsSeparatedByString: @" "];
        NSLog(@"Final screen is %@",names);
        [self gotoQueryResultPage:names serchText:string];
    }
    else
    {
        
            NSDictionary *resultDict = [arrayPeopleList objectAtIndex:indexPath.row] ;
            [self.delegate serverCallForTheSearch:[resultDict valueForKey:@"user_id"] GlobalsearchApi:NO];
    }
    [self cancelSearchResultQuery];
    _searchTextField.text = nil;

}
#pragma mark----
-(void)gotoQueryResultPage : (NSArray *)arrayVal serchText :(NSString *)searchTextVal
{
    SerchResultQueryVC *obj = [[SerchResultQueryVC alloc]initWithNibName:@"SerchResultQueryVC" bundle:nil];
    obj.delegate    = self;
    obj.searchArray = arrayVal;
    obj.searchText  = searchTextVal;
    [self.navigationController pushViewController:obj animated:YES];
}
// called when "Next" is pressed
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
   
    _table.tableHeaderView = nil;
    if ([_searchTextField.text length]) {
        
        NSString *theString = _searchTextField.text;
        NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
        NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
        
        NSArray *parts = [theString componentsSeparatedByCharactersInSet:whitespaces];
        NSArray *names = [parts filteredArrayUsingPredicate:noEmptyStrings];
        theString = [names componentsJoinedByString:@" "];
         NSLog(@"Final screen is %@",names);
        [self gotoQueryResultPage:names serchText:theString];
        _searchTextField.text= nil;
    }
    return YES;
}

#pragma mark For Global Search

-(void)getGlobalSearchResult:(NSString *) text
{
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),text,1] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"/customshowroom/global_search_result",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    NSLog(@"searchRequest data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"SearchResult list ****** = %@",showroomDict);
         
     }];
}

-(void)showNoMoreMints
{
    ttitle               = [[UILabel alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,40)];
    ttitle.font          = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
    ttitle.textColor     = [UIColor blackColor];
    ttitle.textAlignment = NSTextAlignmentCenter;
    [ttitle setText:@"No Result Found"];
    [ttitle setBackgroundColor:[UIColor whiteColor]];
    ttitle.tag = 200;
    self.table.tableHeaderView = ttitle;
    
}
-(void)seeMore:(UIButton *)button
{
    pageNumber = 1;
    isSeeMoreClicked = YES;

    if ([arrayFriendList count]) {
        tempArrayFriendList = [NSMutableArray array];
        [tempArrayFriendList addObjectsFromArray:arrayFriendList];

    }
    if ([arrayPeopleList count]) {
        tempArrayPeopleList = [NSMutableArray array];
        [tempArrayPeopleList addObjectsFromArray:arrayPeopleList];
    }
    
    if ((button.tag == 0)) {
        [arrayPeopleList removeAllObjects];
    }
    else
    [arrayFriendList removeAllObjects];
    
    
    [_table reloadData];
    strMorelinkType = (button.tag == 0)?@"default_words":@"people";
    NSLog(@"See more tapped");
    [self getSearchSuggestion:searchString delete:YES];


}
//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.70;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self.table);
    if (shouldLoadNextPage && isSeeMoreClicked && !isDataFinishedInServer)  {
        pageNumber      = pageNumber +1;
        [self getSearchSuggestion:searchString delete:NO];
    }
  
}
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect visibleRect = self.table.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    self.table.frame =  visibleRect;
//    [self.table scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.table.contentInset = UIEdgeInsetsZero;
    self.table.scrollIndicatorInsets = UIEdgeInsetsZero;
}
//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    [self registerForKeyboardNotifications];
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    
//    [self deregisterFromKeyboardNotifications];
//    [super viewWillDisappear:animated];
//}
//- (void)deregisterFromKeyboardNotifications {
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillShowNotification
//                                                  object:nil];
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
//}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    if ([searchTxt length]) {
        _searchTextField.text = searchTxt;
    }

}// became first responder
-(NSString *)collapseString :(NSString *)serachString
{

    NSString *theString = searchString;
    
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    
    NSArray *parts = [theString componentsSeparatedByCharactersInSet:whitespaces];
    NSArray *filteredArray = [parts filteredArrayUsingPredicate:noEmptyStrings];
    theString = [filteredArray componentsJoinedByString:@" "];
    return theString;
    
}
@end
