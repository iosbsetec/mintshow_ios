//
//  MSGlobalSearchVCViewController.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 19/04/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MSGlobalSearchDelegate <NSObject>
-(void)cancelGlobalSearch;
-(void)tappedGlobalSerchForRemove;
-(void)serverCallForTheSearch:(NSString *)textToSearch GlobalsearchApi:(BOOL)GlobalSearch;
@end

@interface MSGlobalSearchVCViewController : UIViewController
@property (nonatomic,assign)  id  <MSGlobalSearchDelegate> delegate;
@end
