
#import <UIKit/UIKit.h>

@class MSTagsControl;

@protocol MSTagsControlDelegate <NSObject>

- (void)tagsControl:(MSTagsControl *)tagsControl tappedAtIndex:(NSInteger)index keywordAdded:(BOOL)added;

@end

typedef NS_ENUM(NSUInteger, MSTagsControlMode) {
    MSTagsControlModeEdit,
    MSTagsControlModeList,
};

@interface MSTagsControl : UIScrollView

@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) UIColor *tagsBackgroundColor;
@property (nonatomic, strong) UIColor *tagsTextColor;
@property (nonatomic, strong) UIColor *tagsDeleteButtonColor;
@property (nonatomic, strong) NSString *tagPlaceholder;
@property (nonatomic) MSTagsControlMode mode;

@property (assign, nonatomic) id<MSTagsControlDelegate> tapDelegate;

- (id)initWithFrame:(CGRect)frame andTags:(NSArray *)tags withTagsControlMode:(MSTagsControlMode)mode;

- (void)addTag:(NSString *)tag;
- (void)reloadTagSubviews;

@end