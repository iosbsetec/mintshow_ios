//
//  SerchResultQueryVC.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 20/04/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "SerchResultQueryVC.h"
#import "YSCollectionView.h"
#import "MSTagsControl.h"
#import "UsersListView.h"
#import "MSMintDetails.h"
#import "MintShowroomViewController.h"
#import "MSProfileVC.h"
#import "MSRemintVC.h"
#import "CommentsViewController.h"


@interface SerchResultQueryVC ()<MSTagsControlDelegate,YSCollectionViewDataSource,YSCollectionViewDelegate,MSMintDetailsDelegate,RemintDelegate,UsersListDelegate>

@property (nonatomic, strong) IBOutlet MSTagsControl *blueEditingTagControl;
@property (retain,nonatomic)  NSMutableArray *allMintsdata;
@property (strong, nonatomic) IBOutlet YSCollectionView *aView;

- (IBAction)backTapped:(UIButton *)sender ;

@end

@implementation SerchResultQueryVC {

    MSTagsControl *demoTagsControl;
    BOOL                    isLoading,isPullToreFresh;
    BOOL                    isfilterSearchApply;
    BOOL                    isDataFinishedInServer;
    int                     pagenumber;
    __weak IBOutlet UILabel *lblMintCount;
    NSMutableString *removedString;
    NSInteger mintCount;

}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.aView.backgroundColor                  = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    self.view.backgroundColor                   = [UIColor colorWithRed:250.0/255 green:246.0/255  blue:246.0/255  alpha:1.0];
    
    self.aView.showsHorizontalScrollIndicator   = NO;
    self.aView.scrollEnabled                    = YES;
    self.aView.ysDataSource                     = self;
    self.aView.ysDelegate                       = self;
    self.aView.bounces                          = YES;
    self.allMintsdata = [[NSMutableArray alloc]init];
    
    
    isLoading = NO;
    isDataFinishedInServer = NO;
    
    pagenumber = 1;
    
    
    // Do any additional setup after loading the view, typically from a nib.
    NSMutableArray *tags = [NSMutableArray arrayWithArray:self.searchArray];
    _blueEditingTagControl.tags = [tags mutableCopy];
    // _defaultEditingTagControl.tagPlaceholder = @"Placeholder";
    _blueEditingTagControl.tapDelegate=self;
    
    
    UIColor *blueBackgroundColor = [UIColor colorWithRed:243./255.0 green:94./255.0 blue:27.0/255. alpha:1.0];
    UIColor *whiteTextColor = [UIColor whiteColor];
    _blueEditingTagControl.tagsBackgroundColor = blueBackgroundColor;
    _blueEditingTagControl.tagsDeleteButtonColor = whiteTextColor;
    _blueEditingTagControl.tagsTextColor = whiteTextColor;
    
    
    
    [_blueEditingTagControl reloadTagSubviews];
    removedString = [[NSMutableString alloc]initWithString:_searchText];
    [self getGlobalSearchResult:removedString isDelete:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TLTagsControlDelegate

- (void)tagsControl:(MSTagsControl *)tagsControl tappedAtIndex:(NSInteger)index keywordAdded:(BOOL)added
{
    NSLog(@"Tag \"%@\" was tapped", tagsControl.tags[index]);
    NSLog(@"Tag count is  %d", [tagsControl.tags count]);

    pagenumber = 1;

    if (added) {
        [removedString appendString:[NSString stringWithFormat:@" %@",tagsControl.tags[index]]];
        [self getGlobalSearchResult:removedString isDelete:YES];
        return;
    }
    
    if (!added && [tagsControl.tags count] == 1 ) {
        _allMintsdata = nil;
        removedString = nil;
        _blueEditingTagControl = nil;
        _aView = nil;
        [self.delegate cancelSearchResultQuery];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }


    NSString *newString1 = [[removedString stringByReplacingOccurrencesOfString:tagsControl.tags[index] withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *trimmedString = [regex stringByReplacingMatchesInString:newString1 options:0 range:NSMakeRange(0, [newString1 length]) withTemplate:@" "];
    
    [removedString setString:trimmedString];
//    if ([tagsControl.tags count] == 0) {
//        _allMintsdata = nil;
//        removedString = nil;
//        _blueEditingTagControl = nil;
//        _aView = nil;
//        [self.delegate cancelSearchResultQuery];
//        [self.navigationController popViewControllerAnimated:YES];
//        return;
//    }
    [self getGlobalSearchResult:removedString isDelete:YES];
    
}
- (IBAction)backTapped:(UIButton *)sender {
    [self.delegate cancelSearchResultQuery];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark- Global search

-(void)getGlobalSearchResult:(NSString *)text isDelete:(BOOL)delete
{
    
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),[YSSupport escapedValue:text],pagenumber] ;

    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"/customshowroom/global_search_result",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    NSLog(@"searchRequest data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[YSSupport dictionaryFromResponseData:data] methodName:METHOD_MYSHOW_MINT];
         
         if (pagenumber == 1) {
             lblMintCount.text = ([gmodel.status_Msg isEqualToString:@"No more mints"])?@"No Mints to Display":[NSString stringWithFormat:@"SEARCH RESULT: %@ MINTS",gmodel.total_count];
             mintCount = [gmodel.total_count integerValue];
         }
         
         
         if ([gmodel.status_code integerValue]){
             isLoading = NO;
             if (delete) {
                 [self.allMintsdata removeAllObjects];
             }
             if([gmodel.tempArray count]>0){
            
                 isDataFinishedInServer = NO;
                 [self.allMintsdata addObjectsFromArray:gmodel.tempArray];
                 
             }
             else{
                 isDataFinishedInServer = YES;
             }
             [self batchreloadCollectionView];

         }
     }];
}

-(void)batchreloadCollectionView{
    
    BOOL issuesCame = NO;
    @try {
        BOOL animationsEnabled = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        [self.aView performBatchUpdates:^{
            [self.aView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        } completion:^(BOOL finished) {
            if (finished) {
                [UIView setAnimationsEnabled:animationsEnabled];
            }
        }];
        issuesCame = NO;
    }
    @catch (NSException *theException) {
        issuesCame = YES;
        //  showAlert(@"try", @"load tableview again", @"OKAY", nil);
    }
    @finally  {
        issuesCame = NO;
        NSLog(@"Executing finally block");
    }
}
#pragma mark - Yscollection view delegate and data source

-(void)multiMintCollectionViewClicked:(YSCollectionView *)sender AtRow:(NSInteger)row {
    
}

- (BaseFeedModel *)multiMintCollectionViewView:(YSCollectionView *)collectionView DataForItemAtRow:(NSInteger)row{
    return [self.allMintsdata objectAtIndex:row];
}

- (NSInteger)NumberOFSectionsMultiMintView:(YSCollectionView *)collectionView{
    return 1;
}

- (NSInteger)NumberOFItemsForMultiMintView:(YSCollectionView *)collectionView forSection:(NSInteger)section{
    return  _allMintsdata.count;
}


- (void)didLoadNextTriggered{
    if(!isLoading && !isDataFinishedInServer){
        isLoading = YES;
        pagenumber = pagenumber+1;
        [self getGlobalSearchResult:removedString isDelete:NO];
    }
}


- (void)selectedFeatureType:(int )featureType withString:(NSString *)selectedEnitity isPrivateShowroomAccess:(BOOL)access{
    switch (featureType) {
        case 0:
            [self gotoHashTagView:selectedEnitity];
            break;
        case 1:
        {
            if(access) {
                showAlert(@"It's a private showroom", nil, @"Ok", nil);
            }
            else
                [self gotoParticularShowroom:selectedEnitity];
        }
            break;
        case 2:case 44:
            [self gotoProfileView:selectedEnitity];
            break;
        default:
            break;
    }
    
}


- (void)buttonActionclickedFortype :(YSButtonType )buttonType forRow:(NSInteger)row feedModelData : (BaseFeedModel *)data forCell:(YSMintCollectionViewCell*)cell{
    BaseFeedModel *existingFeedModeldata = [ self.allMintsdata objectAtIndex:row];
    
    if(buttonType == YSButtonTypeComplimint)  {
        [self commentScreen:existingFeedModeldata withSelectedMintTag:row];
    }
    else if (buttonType == YSButtonTypeHifiveCount)  {
        NSLog(@"created more time.....");
        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeHighFive numberofPeoples:[existingFeedModeldata.feed_like_count intValue] feedId:existingFeedModeldata.feed_id delegate:self];
    }
    else if (buttonType == YSButtonTypeInspire)  {
        [self remintClicked:existingFeedModeldata];
    }
    else if (buttonType == YSButtonTypedetails){
        [self gotoMintDetailsScreen:existingFeedModeldata forRow:row];
    } else if (buttonType == YSButtonTypeUserImage) {
        [self showPopupWithModeldata:data withrow:row isRemint:NO];
    } else if (buttonType == YSButtonTypeRemintUserImage) {
        [self showPopupWithModeldata:data withrow:row isRemint:YES];
    }else if (buttonType == YSButtonTypeHifive){
        [ self.allMintsdata replaceObjectAtIndex:row withObject:data];
    }
}
-(void)selectedUserIdFromHighfiveList:(NSString *)selectdUserId
{
    
    [self gotoProfileView:selectdUserId];
    
}
-(void)gotoHashTagView:(NSString *)selectedEnitity{
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.delegate = self;
    mintDetailsVC.isToshowSingleRecord = NO;
    mintDetailsVC.isFromTrendingTags = YES;
    mintDetailsVC.isMintOwner = NO;
    mintDetailsVC.navigationHeaderText = selectedEnitity;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}

-(void)gotoProfileView:(NSString *)selectedEnitity{
    
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    [self.navigationController pushViewController:prof animated:YES];
}
-(void)gotoParticularShowroom:(NSString *)selectedEnitity{

    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [selectedEnitity substringFromIndex:1];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}

- (void)complimintClicked :(BaseFeedModel *)model {
    BaseFeedModel *someModel    = model;
    NSString *feedId        = someModel.feed_id;
    SPHServiceRequest *req  = [[SPHServiceRequest alloc]init];
    NSString *url = [NSString stringWithFormat:@"%@%@?access_token=%@&%@=%d",ServerUrl,API_GETALLCOMPLIMENTS,
                     GETVALUE(CEO_AccessToken),@"mint_id",[feedId intValue]];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [req GETRequestWithUrlAndData:nil mathod:METHOD_GET_SINGLEMINT link:url withSuccessionBlock:^(id response)
     {
         Generalmodel *list_param  = response;
         if ([list_param.status isEqualToString:@"true"]||[list_param.status isEqualToString:@"True"]) {
             BaseFeedModel *model                   = [list_param.tempArray lastObject];
             CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
             comentVc.achievementdetails = model ;
             //             comentVc.hidesBottomBarWhenPushed = YES;
             
             [self.navigationController pushViewController:comentVc animated:YES];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
         }else {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     } andFailureBlock:^(NSError *error) {
         //NSLog(@"%@",error);
     }];
}


- (void)remintClicked :(BaseFeedModel *)feedData{
    MSRemintVC *reminytVW               = [[MSRemintVC alloc]initWithNibName:@"MSRemintVC" bundle:nil];
    reminytVW.aDelegate = self;
    reminytVW.feedData                  = feedData;
    reminytVW.isFromInstagramTableView  =   @"0";
    [self presentViewController:reminytVW animated:NO completion:^{
    }];
}

-(void)showPopupWithModeldata:(BaseFeedModel *)feeddetails withrow:(NSInteger)row isRemint:(BOOL)remint {
    [self gotoProfileView:[NSString stringWithFormat:@"%@",(remint)?feeddetails.remint_user_id:feeddetails.feed_user_id]];
}


-(void)gotoMintDetailsScreen: (BaseFeedModel *)model forRow:(NSInteger)row{
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mint_row_id = model.mint_row_id;
    mintDetailsVC.Selected_row_value = row;
    mintDetailsVC.delegate    = self;
    mintDetailsVC.isFromQueryResultPage = YES;
    mintDetailsVC.globalSearchText = removedString;
    mintDetailsVC.mintArrayDetails = _allMintsdata;
    mintDetailsVC.isMintOwner = ([model.feed_user_id isEqualToString:GETVALUE(CEO_UserId)])?YES:NO;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}

-(void)gotoCommentScreen : (BaseFeedModel *)model{
    CommentsViewController *comentVc = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.achievementdetails = model ;
    [self.navigationController pushViewController:comentVc animated:YES];
}

-(void)commentScreen : (BaseFeedModel *)model withSelectedMintTag :(NSInteger) tag{
    AppDelegate *aDelegate  =  (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndex =  [NSString stringWithFormat:@"%d",(int)tag];
    CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.commentCount              = [model.feed_comment_count intValue];
    comentVc.achievementdetails        = model ;
    comentVc.feedId                    = model.feed_id ;
    [self.navigationController pushViewController:comentVc animated:YES];
}

//For Mute mint, Delete Mint. FlagInappropriate
-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId
{
    int mintIndex = 0;
    BOOL isMintFoundFromList = NO;
    
    for (int i = 0; i<_allMintsdata.count; i++) {
        
        
        if ([[[_allMintsdata objectAtIndex:i]feed_id] isEqualToString:mintId]) {
            mintIndex = i;
            isMintFoundFromList = YES;
            break;
        }
        
    }
    
    if (isMintFoundFromList) {
        
        [_allMintsdata removeObjectAtIndex:mintIndex];
        mintCount = (mintCount)?mintCount-1:0;
        lblMintCount.text = (mintCount)?[NSString stringWithFormat:@"SEARCH RESULT: %d MINTS",mintCount]:@"No Mints to Display";
        [self batchreloadCollectionView];
    }
    
    
}
//Update the tableview if the MintUser Mint exist
-(void)muteUserOfmintDetails:(NSString *)muteUser{
    
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[_allMintsdata count]; currentIndex++){
        //do stuff with obj
        BaseFeedModel *feedlist = [_allMintsdata objectAtIndex:currentIndex];
        if([muteUser isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [_allMintsdata removeObjectsAtIndexes:indexesToDelete];
        mintCount = (mintCount)?mintCount-1:0;

 lblMintCount.text = (mintCount)?[NSString stringWithFormat:@"SEARCH RESULT: %d MINTS",mintCount]:@"No Mints to Display";
        [self batchreloadCollectionView];
    }
    
    
}

@end
