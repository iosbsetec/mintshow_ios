//
//  SerchResultQueryVC.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 20/04/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SerchResultQueryDelegate <NSObject>
-(void)cancelSearchResultQuery;
@end


@interface SerchResultQueryVC : UIViewController
@property (nonatomic,retain)  NSArray *searchArray;
@property (nonatomic,retain)  NSString *searchText;
@property (nonatomic,assign)  id  <SerchResultQueryDelegate> delegate;

@end
