//
//  UIImageView+LoadAsync.h
//  Your Show
//
//  Created by Siba Prasad Hota on 08/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (LoadAsync)

@property (nonatomic, strong) NSURL *mintImageURL;

- (void)setMintImageURL:(NSURL *)imageURL;

@end
