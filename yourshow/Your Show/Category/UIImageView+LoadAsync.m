//
//  UIImageView+LoadAsync.m
//  Your Show
//
//  Created by Siba Prasad Hota on 08/03/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "UIImageView+LoadAsync.h"
#import "PINCache.h"


@implementation UIImageView (LoadAsync)



- (void)setMintImageURL:(NSURL *)imageURL
{
    
    [[PINCache sharedCache] objectForKey:[imageURL absoluteString]
                                   block:^(PINCache *cache, NSString *key, id object) {
                                       if (object) {
                                           [self setImageOnMainThread:(UIImage *)object];
                                           return;
                                       }
                                       
                                       [self startDownloadingImageWithUrl:imageURL withSuccessionBlock:^(UIImage *imageToshow){
                                           
                                           [self setImageOnMainThread:imageToshow];
                                           [[PINCache sharedCache] setObject:imageToshow forKey:[imageURL absoluteString]];
                                           
                                       } andFailureBlock:^(NSError *error){
                                           NSLog(@"image cant be loaded %@" , error);
                                           
                                       }];
                                   }];
}

-(void)startDownloadingImageWithUrl:(NSURL*)iamgeURL
                withSuccessionBlock:(void(^)(UIImage *imageToshow))successBlock
                    andFailureBlock:(void(^)(NSError *error))failureBlock{
    NSURLRequest *request = [NSURLRequest requestWithURL:iamgeURL
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:60];
    // create an session data task to obtain and download the app icon
    NSURLSessionDataTask *sessionTask = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                         {
                                             if (error != nil) {
                                                 if ([error code] == NSURLErrorAppTransportSecurityRequiresSecureConnection) {
                                                     abort();
                                                 }
                                             }
                                             [[NSOperationQueue mainQueue] addOperationWithBlock: ^{
                                                 UIImage *image = [[UIImage alloc] initWithData:data];
                                                 successBlock(image);
                                             }];
                                         }];
    
    [sessionTask resume];
    
}



- (void)setImageOnMainThread:(UIImage *)image
{
    if (!image)
        return;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.image = image;
        self.alpha = 1.0;
    });
}


@end
