//
//  NSNull+JSON.m
//  GebeChat
//
//  Created by Siba Prasad Hota on 15/02/15.
//  Copyright (c) 2015 WemakeAppz. All rights reserved.
//

#import "NSNull+JSON.h"

@implementation NSNull (JSON)

- (NSUInteger)length { return 0; }

- (NSInteger)integerValue { return 0; };

- (int)intValue { return 0; };

- (float)floatValue { return 0; };

- (NSString *)description { return @"0(NSNull)"; }

- (NSString *)stringByReplacingOccurrencesOfString:(NSString *)withString { return @""; }

- (NSArray *)componentsSeparatedByString:(NSString *)separator { return @[]; }

- (id)objectForKey:(id)key { return nil; }

- (BOOL)boolValue { return NO; }

- (BOOL)isEqualToString { return NO; }

@end
