

#import "NSString+Emojize.h"
#import "emojis.h"
#import "iOSEmoji.h"

@implementation NSString (Emojize)

- (NSString *)emojizedString
{
    return [NSString emojizedStringWithString:self];
}

- (NSString *)reverseEmojizedString
{
    return [NSString reverseEmojizedStringWithString:self];
}
+ (NSString *)reverseEmojizedStringWithString:(NSString *)text
{
    static dispatch_once_t onceToken2;
    static NSRegularExpression *regex2 = nil;
    dispatch_once(&onceToken2, ^{
        regex2 = [[NSRegularExpression alloc] initWithPattern:@"[0-9a-zA-Z'\\-\n ]" options:0 error:NULL];
    });
    
    __block NSString *resultText = text;
    NSRange matchingRange = NSMakeRange(0, [resultText length]);
    [regex2 enumerateMatchesInString:resultText options:NSMatchingReportCompletion range:matchingRange usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        if (result && ([result resultType] == NSTextCheckingTypeRegularExpression) && !(flags & NSMatchingInternalError)) {
            NSRange range = result.range;
            if (range.location != NSNotFound) {
                NSString *code = [text substringWithRange:range];
                NSString *unicode = self.reverseEmojiAliases[code];
                if (unicode) {
                    resultText = [resultText stringByReplacingOccurrencesOfString:code withString:unicode];
                }
            }
        }
    }];
    
    return resultText;
}


+ (NSString *)emojizedStringWithString:(NSString *)text
{
    static dispatch_once_t onceToken5;
    static NSRegularExpression *regex5 = nil;
    dispatch_once(&onceToken5, ^{
        regex5 = [[NSRegularExpression alloc] initWithPattern:@"(:[a-z0-9-+_]+:)" options:NSRegularExpressionCaseInsensitive error:NULL];
    });
    __block NSString *resultText = text;
    NSRange matchingRange = NSMakeRange(0, [resultText length]);
    [regex5 enumerateMatchesInString:resultText options:NSMatchingReportCompletion range:matchingRange usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         if (result && ([result resultType] == NSTextCheckingTypeRegularExpression) && !(flags & NSMatchingInternalError)) {
             NSRange range = result.range;
             if (range.location != NSNotFound) {
                 NSString *code = [text substringWithRange:range];
                 NSString *unicode = self.emojiAliases[code];
                 if (unicode) {
                     resultText = [resultText stringByReplacingOccurrencesOfString:code withString:unicode];
                 }
             }
         }
     }];
    return resultText;
}

+ (NSDictionary *)emojiAliases {
    static NSDictionary *_emojiAliases;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _emojiAliases = EMOJI_HASH;
    });
    return _emojiAliases;
}

+ (NSDictionary *)reverseEmojiAliases {
    NSMutableDictionary *_revEmojiAliases = [NSMutableDictionary new];
    static dispatch_once_t onceToken3;
    dispatch_once(&onceToken3, ^{
        for (NSString *key in [EMOJI_HASH allKeys]) {
            _revEmojiAliases[EMOJI_HASH[key]] = key;
        }
    });
    return _revEmojiAliases;
}

#pragma mark - Methods for coverting the user entry text to server recomened format
#pragma mark --

-(NSString *)getFinalFormatStringForServer
{
    
    return [self stringOperation:self];
    
    
}


-(NSString *) stringOperation:(NSString *)newString
{
    NSArray *compareArray = [[NSArray alloc]initWithObjects:@"ud83d",@"ud83c",@"u2",@"u3",@"u0", nil];
    //    NSString *newString=@"Hiiiii ud83dude13Jeyaud83dude1cKumarud83dude07Maniud83dude23Jitenud83dude18u2764 u2652u3297u00a9";
    
    for(int j=0; j<[compareArray count]; j++)
    {
        if(j==0 || j==1)
        {
            newString=[self stringRegex:[compareArray objectAtIndex:j] andActualstr:newString andLength:10];
        }
        else
        {
            newString=[self stringRegex:[compareArray objectAtIndex:j] andActualstr:newString andLength:5];
        }
    }
    
    NSString *squashed = [newString stringByReplacingOccurrencesOfString:@"[ ]+" withString:@" " options:NSRegularExpressionSearch range:NSMakeRange(0, newString.length)];
    
    return [squashed stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString *) stringRegex: (NSString *)replacer andActualstr: (NSString *)str andLength: (int) i {
    //    NSMutableString *string = [str mutableCopy];
    NSString *string = str;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:replacer options:NSRegularExpressionCaseInsensitive error:&error];
    NSArray *matches1 = [regex matchesInString:string options:0 range:NSMakeRange(0, string.length)];
    NSString *strMain= string;
    if([matches1 count]>0)
    {
        for (NSTextCheckingResult *match in matches1)
        {
            NSRange matchRange = [match rangeAtIndex:0];
            
            NSString *strNow;
            if ([replacer isEqualToString:@"u2"] || [replacer isEqualToString:@"u3"] || [replacer isEqualToString:@"u0"]) {
                strNow = [string substringWithRange:NSMakeRange(matchRange.location, 5)];
                strMain = [strMain stringByReplacingOccurrencesOfString:strNow withString:[EMOJIIOS_HASH valueForKey:strNow]];
            }
            else
            {
              
                strNow = [string substringWithRange:NSMakeRange(matchRange.location, 10)];
                strMain = [strMain stringByReplacingOccurrencesOfString:strNow withString:[EMOJIIOS_HASH valueForKey:strNow]];
            }
            
            NSLog(@"strMain==> %@",strMain);
            
        }
    }
    return strMain;
    
}


@end
