

#import <Foundation/Foundation.h>

@interface NSString (Emojize)

- (NSString *)reverseEmojizedString;
- (NSString *)emojizedString;
+ (NSString *)emojizedStringWithString:(NSString *)text;
+ (NSDictionary *)emojiAliases;


//Methos for converting the Emoji (imnages) to the format for server
-(NSString *)getFinalFormatStringForServer;
@end
