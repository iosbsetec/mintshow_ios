//
//  MintDetailTableview.h
//  Your Show
//
//  Created by Siba Prasad Hota on 02/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MintDetailsTableViewDataSource.h"


@class MintDetailTableview;

@protocol mintTableDelegate

- (void)didSelectSelectRow:(NSInteger)row inSection:(NSInteger)section;
- (void)didSelectSelectAllButtonClicked: (MintDetailTableview *) sender forSection :(NSInteger) section;

@end

@interface MintDetailTableview : UITableView< UITableViewDelegate, UITableViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate>


@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;

@property (nonatomic, assign)IBOutlet id <mintTableDelegate> mintdelegate;
@property (nonatomic, assign) BOOL isLoadingFinished;
@property (nonatomic, assign) IBOutlet id<MintDetailsTableViewDataSource> mintDataSource;

@end








