//
//  MintDetailsTableViewCell.h
//  Your Show
//
//  Created by Siba Prasad Hota on 02/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPHMintModel.h"

@interface MintDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreTapped;
@property (weak, nonatomic) IBOutlet UIImageView *imageScreen;
-(void)setCellData:(SPHMintModel*)mintmodel;
@end
