//
//  JitenCollectionView.h
//  Your Show
//
//  Created by Siba Prasad Hota  on 7/30/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JitenCollectionViewDataSource.h"


@class JitenCollectionView;

@protocol JitenCollectionTableDelegate
- (void) PinterestTableDelegateMethod: (JitenCollectionView *) sender selectedIndex :(NSInteger) selectedRowNo;
@end

@interface JitenCollectionView : UICollectionView <UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSArray *arrimages;

}
@property (nonatomic, retain) id <JitenCollectionTableDelegate> collectiondelegate;
@property (nonatomic, assign) IBOutlet id<JitenCollectionViewDataSource> CollectionDataSource;
@end


