//
//  MintDetailsTableViewDataSource.h
//  Your Show
//
//  Created by Siba Prasad Hota on 02/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SPHMintModel.h"

@class MintDetailTableview;

@protocol MintDetailsTableViewDataSource <NSObject>

@optional

@required

- (SPHMintModel *)mintTableView:(MintDetailTableview *)tableView DataForSection:(NSInteger)section;
//- (NSInteger)NumberOFrowsForMintTableView:(MintDetailTableview *)mintTableView;

@end
