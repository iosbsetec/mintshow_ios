//
//  MintDetailTableview.m
//  Your Show
//
//  Created by Siba Prasad Hota on 02/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MintDetailTableview.h"
#import "ContactSectionHeaderView.h"
#import "LoadingDataTableViewCell.h"
#import "MintDetailsTableViewCell.h"
#import "CollectionViewTableViewCell.h"

#define HEADER_HEIGHT 48

static NSString *ContactSectionHeaderViewIdentifier = @"ContactSectionHeaderViewIdentifier";

@implementation MintDetailTableview

@synthesize mintDataSource = _mintDataSource;
@synthesize mintdelegate = _mintdelegate;
@synthesize isLoadingFinished=_isLoadingFinished;

- (void)initializator{
    self.delegate = self;
    self.dataSource = self;
    _isLoadingFinished=YES;
    
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
    self.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin  |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth |
                                  UIViewAutoresizingFlexibleLeftMargin |
                                  UIViewAutoresizingFlexibleRightMargin);

//    self.sectionHeaderHeight = HEADER_HEIGHT;
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"ContactSectionHeaderView" bundle:nil];
    [self registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:ContactSectionHeaderViewIdentifier];
}

- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) [self initializator];
    return self;
}



#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0)
    {
        return 0.0;
    }
    else
        return 44.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ContactSectionHeaderView *sectionHeaderView = [self dequeueReusableHeaderFooterViewWithIdentifier:ContactSectionHeaderViewIdentifier];
    
    if (section==1)
    {
        sectionHeaderView.titleLabel.text = @"RELATED MINTS";
        sectionHeaderView.selectAllBtn.alpha = 1.0;
        sectionHeaderView.lblRightArrow.alpha=1.0;

    }
    return sectionHeaderView;
}

-(IBAction)ClickedSelectAll:(id)sender
{
    [self.mintdelegate didSelectSelectAllButtonClicked:self forSection:[sender tag]];
}



#pragma mark - UITableViewDelegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!_isLoadingFinished)
        return 70;
    else  if (indexPath.section==0)
    {
        return 531;
     }
    else
        return 250;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!_isLoadingFinished)
    {
        static NSString *lCellIdentifier = @"LoadingDataTableViewCell";
        
        
        LoadingDataTableViewCell  *newcell = (LoadingDataTableViewCell *)[self dequeueReusableCellWithIdentifier:lCellIdentifier];
        if (newcell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LoadingDataTableViewCell" owner:self options:nil];
            newcell = [topLevelObjects objectAtIndex:0];
        }
        return newcell;
    }
    
    if (indexPath.section ==0)
    {
        static NSString *CellIdentifier = @"MintDetailsTableViewCell";
        
        MintDetailsTableViewCell  *cell = (MintDetailsTableViewCell *)[self dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        [cell setCellData:[self.mintDataSource mintTableView:self DataForSection:indexPath.section]];
        
        return cell;
    }
    
    
    static NSString *CellIdentifier = @"CellIdentifier";
    
    CollectionViewTableViewCell *cell = (CollectionViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CollectionViewTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(CollectionViewTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselectRowAtIndexPath:indexPath animated:NO];
    [self.mintdelegate didSelectSelectRow:indexPath.row inSection:indexPath.section];
}

@end
