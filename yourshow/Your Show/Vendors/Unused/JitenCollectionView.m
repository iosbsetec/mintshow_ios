//
//  JitenCollectionView.m
//  Your Show
//
//  Created by Siba Prasad Hota  on 7/30/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "JitenCollectionView.h"
#import "CellDiscoverShowrooms.h"

@implementation JitenCollectionView

@synthesize collectiondelegate = _collectiondelegate;
@synthesize CollectionDataSource = _CollectionDataSource;

- (void)initializator
{
    self.delegate = self;
    self.dataSource = self;
    
    arrimages = [NSArray arrayWithObjects:@"nature1.png", @"nature2.jpeg",@"nature3.png", @"nature4.jpeg", @"nature5.jpeg", @"nature2.jpeg",nil];
 
    
    UINib *nib = [UINib nibWithNibName:@"CellDiscoverShowrooms" bundle: nil];
    [self registerNib:nib forCellWithReuseIdentifier:@"CellDiscoverShowrooms"];

}
- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.CollectionDataSource NumberOFrowsForCollectionViewView:self];
}

- (CGSize)collectionViewContentSize
{
    return CGSizeMake(320, 225);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    NSString *identifier = @"CellDiscoverShowrooms";
    CellDiscoverShowrooms *cell = (CellDiscoverShowrooms*)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *productImageView = (UIImageView *)[cell viewWithTag:100];
    productImageView.image = [UIImage imageNamed:[arrimages objectAtIndex:indexPath.row]];
    
    ShowRoomModel *data = [self.CollectionDataSource JitenCollectionViewView:self DataForItemAtRow:indexPath.row];
    
    UILabel *producttitle = (UILabel *)[cell viewWithTag:101];
    producttitle.text = data.showroom_name;
    producttitle.tintColor = [UIColor blackColor];
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGSize mElementSize = CGSizeMake(150, 190);
    return mElementSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5,4,4,5);
    
    
}


@end
