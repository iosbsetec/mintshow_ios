//
//  JitenCollectionViewDataSource.h
//  Your Show
//
//  Created by Siba Prasad Hota  on 7/30/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ShowRoomModel.h"





@class JitenCollectionView;

@protocol JitenCollectionViewDataSource <NSObject>

@optional

@required

- (ShowRoomModel *)JitenCollectionViewView:(JitenCollectionView *)collectionView DataForItemAtRow:(NSInteger)row;
- (NSInteger)NumberOFrowsForCollectionViewView:(JitenCollectionView *)collectionView;

@end


