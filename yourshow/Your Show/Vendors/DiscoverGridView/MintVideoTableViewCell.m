//
//  MintVideoTableViewCell.m
//  Your Show
//
//  Created by Siba Prasad Hota on 27/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MintVideoTableViewCell.h"
#import <AVFoundation/AVFoundation.h>


@implementation MintVideoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}


-(void)setCustomVideoDelegate:(id)sender
{
    self.videoDelegate=sender;
}

- (void)awakeFromNib
{
    self.frontView.alpha=0.0;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.mintImageView.layer.cornerRadius = 10.0;
    self.mintImageView.clipsToBounds = YES;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

-(void)setCelldataForVideoCell:(VideoModel *)someModel{
    self.videoUrl = someModel.VideoUrl;
    self.vcellType = someModel.vcellType;
    if (someModel.vcellType == VideoCellTypeLocalPath)
    {
        [self.mintImageView setImage:someModel.thumbImage];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            AVAsset *asset = [AVAsset assetWithURL:someModel.VideoUrl];
            AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
            CMTime time = CMTimeMake(0, 1);
            CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
            UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.mintImageView setImage:thumbnail];
                CGImageRelease(imageRef);
                
            });
        });
    }else   if (someModel.vcellType == VideoCellTypeYoutube && someModel.thumbImage) {
        [self.mintImageView setImage:someModel.thumbImage];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (IBAction)playbtnClicked:(id)sender
{
    [self.videoDelegate didClickedVideoBtn:self AndvcelType:self.vcellType];
}

@end
