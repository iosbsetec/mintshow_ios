//
//  DiscoverTableview.m
//  Your Show
//
//  Created by Siba Prasad Hota on 05/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "DiscoverTableview.h"
#import "ContactSectionHeaderView.h"
#import "PPImageScrollingTableViewCell.h"
#import "LoadingDataTableViewCell.h"
#import "CollectionViewTableViewCell.h"
#import "EmptyTableViewCell.h"
#import "MSPSignatureShowroomVC.h"
#import "SignShowImageScrollingTableViewCell.h"


static NSString *SectionHeaderViewIdentifier = @"DiscoverHeaderViewIdentifier";

@interface DiscoverTableview ()

@end



@implementation DiscoverTableview 

@synthesize DiscoverDataSource = _DiscoverDataSource;
@synthesize Discoverdelegate = _Discoverdelegate;


- (void)initializator
{
    self.delegate = self;
    self.dataSource = self;

    UINib *sectionHeaderNib = [UINib nibWithNibName:@"ContactSectionHeaderView" bundle:nil];
    [self registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:SectionHeaderViewIdentifier];
    static NSString *CellIdentifier = @"Cell";
    [self registerClass:[PPImageScrollingTableViewCell class] forCellReuseIdentifier:CellIdentifier];
    self.ArrayTobepassed = [[NSMutableArray alloc]init];
    
    static NSString *CellIdentifierForSign = @"Cella";
    [self registerClass:[SignShowImageScrollingTableViewCell class] forCellReuseIdentifier:CellIdentifierForSign];

    self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
}

- (id)init
{
    self = [super init];
    
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) [self initializator];
    return self;
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return   [self.DiscoverDataSource DiscoverTableViewNumberOfsections:self];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self.DiscoverDataSource DiscovertableView:self heightForHeaderInSection:section];
}


-(IBAction)ClickedSelectAll:(id)sender
{
    [self.Discoverdelegate didSelectSelectAllButtonClickedforSection:[sender tag]];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ContactSectionHeaderView *sectionHeaderView = [self dequeueReusableHeaderFooterViewWithIdentifier:SectionHeaderViewIdentifier];
    sectionHeaderView.titleLabel.text =[self.DiscoverDataSource DiscoverTableView:self TitleOfHeaderForSection:section];
//    sectionHeaderView.titleLabel.font =  [UIFont fontWithName:@"Gotham-Book" size:5];

    DiscoverViewCellType celltype = [self.DiscoverDataSource DiscoverTableView:self cellTypeForSection:section];
    
    if (celltype == DiscoverViewCellTypeMultiShowroom ) {
        //sectionHeaderView.titleLabel.textAlignment = NSTextAlignmentCenter;
        sectionHeaderView.titleLabel.textColor = [UIColor colorWithRed:(177/255.f) green:(177/255.f) blue:(177/255.f) alpha:1.0f];
    }
    //PinterestViewCellTypeImageScroller
    if (celltype == DiscoverViewCellTypeImageScroller && self.showArrowButton ) {
        sectionHeaderView.selectAllBtn.alpha=0.0;
        sectionHeaderView.lblRightArrow.alpha=1.0;
    }
    else {
        sectionHeaderView.selectAllBtn.alpha  = 0.0;
        sectionHeaderView.lblRightArrow.alpha = 0.0;

    }
    
    sectionHeaderView.section = section;
    sectionHeaderView.selectAllBtn.tag = section;
    [sectionHeaderView.selectAllBtn addTarget:self action:@selector(ClickedSelectAll:) forControlEvents:UIControlEventTouchUpInside];
    
    return sectionHeaderView;
}


#pragma mark - UITableViewDelegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.DiscoverDataSource DiscoverTableView:self heightForRowAtIndexPath:indexPath];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     DiscoverViewCellType celltype = [self.DiscoverDataSource DiscoverTableView:self cellTypeForSection:indexPath.section];
    
    if(celltype == DiscoverViewCellTypeLoadingData)
    {
        static NSString *lCellIdentifier = @"LoadingDataTableViewCell";
        
        LoadingDataTableViewCell  *newcell = (LoadingDataTableViewCell *)[self dequeueReusableCellWithIdentifier:lCellIdentifier];
        if (newcell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LoadingDataTableViewCell" owner:self options:nil];
            newcell = [topLevelObjects objectAtIndex:0];
        }
        return newcell;
    }
    if (celltype == DiscoverViewCellTypeImageScroller)
    {
        static NSString *CellIdentifier = @"Cell";
        
        PPImageScrollingTableViewCell *customCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [customCell setBackgroundColor: [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1]];
        [customCell setCustomDelegate:self.Discoverdelegate];
        [customCell setImageData:[self.DiscoverDataSource DiscoverTableView:self DataForSection:indexPath.section]];
        
        [customCell setTag:[indexPath section]];
        [customCell setImageTitleTextColor:[UIColor grayColor] withBackgroundColor:[UIColor clearColor]];
        [customCell setImageTitleLabelWitdh:90 withHeight:30];
        [customCell setCollectionViewBackgroundColor:[UIColor darkGrayColor]];
        return customCell;
        
    }
    
    
    if (celltype == DiscoverViewCellTypeSignatureShowroom)
    {
        static NSString *CellIdentifier = @"Cella";
        
        SignShowImageScrollingTableViewCell *customCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [customCell setBackgroundColor: [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1]];
        [customCell setCustomDelegate:self.Discoverdelegate];
        [customCell setImageData:[self.DiscoverDataSource DiscoverTableView:self DataForSection:indexPath.section]];
        
        [customCell setTag:[indexPath section]];
        [customCell setImageTitleTextColor:[UIColor grayColor] withBackgroundColor:[UIColor clearColor]];
        [customCell setImageTitleLabelWitdh:100 withHeight:30];
        [customCell setCollectionViewBackgroundColor:[UIColor darkGrayColor]];
        return customCell;
        
    }
    if (celltype == DiscoverViewCellTypeMultiMint)
    {
        static NSString *CellIdentifier = @"MultiMintCollectionTableViewCell";
        
        EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableViewCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        for(id aview in [cell.contentView subviews])
        {
            [aview removeFromSuperview];
        }
        [cell.contentView addSubview:[self.DiscoverDataSource discoverCollectionViewForRowAtIndexPath:indexPath]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if (celltype == DiscoverViewCellTypeMultiShowroom)
    {
        static NSString *CellIdentifier = @"SCollectionViewTableViewCell";
        EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableViewCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        for(id aview in [cell.contentView subviews])
        {
            [aview removeFromSuperview];
        }
        [cell.contentView addSubview:[self.DiscoverDataSource showroomCollectionViewForRowAtIndexPath:indexPath]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    static NSString *noCellIdentifier = @"NoCell";
    CollectionViewTableViewCell *cell = (CollectionViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:noCellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CollectionViewTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    return cell;
}


//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self);
    if (shouldLoadNextPage)
    {
        [self.Discoverdelegate LoadNextPagetriggeredForSection:3];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self);
    if (shouldLoadNextPage)
    {
        [self.Discoverdelegate LoadNextPagetriggeredForSection:3];
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    

    if ([self.Discoverdelegate isKindOfClass:[MSPSignatureShowroomVC class]])
    {
        [self.Discoverdelegate scrollViewSignatureShowroomDidScroll:scrollView];
    }
}
@end
