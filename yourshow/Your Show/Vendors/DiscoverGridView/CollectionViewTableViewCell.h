//
//  CollectionViewTableViewCell.h
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JitenCollectionView.h"

static NSString *CollectionViewCellIdentifier = @"CollectionViewCellIdentifier";


@interface CollectionViewTableViewCell : UITableViewCell


@property (nonatomic, strong)IBOutlet JitenCollectionView *discovershowroomcollectionview;

- (void)setCollectionViewDataSourceDelegate:(id)sender;

@end
