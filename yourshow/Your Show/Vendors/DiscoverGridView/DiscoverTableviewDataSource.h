//
//  DiscoverTableviewDataSource.h
//  Your Show
//
//  Created by Siba Prasad Hota on 05/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//



#import <Foundation/Foundation.h>
#import "SPHMintModel.h"
#import "Feedmodel.h"
#import "MultiMintCollectionView.h"
#import "JitenCollectionView.h"


typedef enum {
    DiscoverViewCellTypeImageScroller,
    DiscoverViewCellTypeMintScroller,
    DiscoverViewCellTypeMultiMint,
    DiscoverViewCellTypeMultiShowroom,
    DiscoverViewCellTypeLoadingData,
    DiscoverViewCellTypeMintDetail,
    DiscoverViewCellTypeSignatureShowroom,
} DiscoverViewCellType;


@class DiscoverTableview;

@protocol DiscoverTableViewDataSource <NSObject>

@optional

@required


- (DiscoverViewCellType)DiscoverTableView:(DiscoverTableview *)tableView cellTypeForSection:(NSInteger)section;
- ( NSInteger)DiscoverTableViewNumberOfsections:(DiscoverTableview *)tableView;
-(CGFloat)DiscovertableView:(DiscoverTableview *)tableView heightForHeaderInSection:(NSInteger)section;

@optional

-(CGFloat)DiscoverTableView:(DiscoverTableview *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (SPHMintModel *)DiscoverTableView:(DiscoverTableview *)tableView MintDataForSection:(NSInteger)section;
- (NSDictionary *)DiscoverTableView:(DiscoverTableview *)tableView DataForSection:(NSInteger)section;
- (NSString *)DiscoverTableView:(DiscoverTableview *)tableView TitleOfHeaderForSection:(NSInteger)section;
- (Feedmodel *)multiMintCollectionViewView:(MultiMintCollectionView *)collectionView DataForItemAtRow:(NSInteger)row;
- (NSInteger)NumberOFrowsForMultiMintView:(MultiMintCollectionView *)collectionView;
-(MultiMintCollectionView *)discoverCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath;
-(JitenCollectionView *)showroomCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath;


@end