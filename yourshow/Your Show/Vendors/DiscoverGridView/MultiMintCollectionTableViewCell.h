//
//  MultiMintCollectionTableViewCell.h
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MultimintViewDataSource.h"
#import "MultiMintCollectionView.h"


@interface MultiMintCollectionTableViewCell : UITableViewCell

@property(nonatomic,strong) id mintDelegate;

@property (nonatomic, strong)IBOutlet MultiMintCollectionView *discovershowroomcollectionview;



-(CGFloat)heightOfCollectionView;

@end
