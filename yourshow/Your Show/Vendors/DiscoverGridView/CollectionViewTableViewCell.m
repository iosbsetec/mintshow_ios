//
//  CollectionViewTableViewCell.m
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "CollectionViewTableViewCell.h"

@implementation CollectionViewTableViewCell


- (void)awakeFromNib
{
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.discovershowroomcollectionview.backgroundColor = [UIColor whiteColor];
    self.discovershowroomcollectionview.showsHorizontalScrollIndicator = NO;
    self.discovershowroomcollectionview.Addshowroom = NO;
    self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
   // self.discovershowroomcollectionview.frame = self.contentView.bounds;
}

- (void)setCollectionViewDataSourceDelegate:(id)sender
{
    self.discovershowroomcollectionview.CollectionDataSource = sender;
    self.discovershowroomcollectionview.collectiondelegate = sender;
    self.discovershowroomcollectionview.Addshowroom = NO;
    [self.discovershowroomcollectionview reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
