
#import <Foundation/Foundation.h>
#import "SPHMintModel.h"
#import "addmintModel.h"
#import "Feedmodel.h"
#import "MultiMintCollectionView.h"
#import "YSCommentView.h"
#import "JitenCollectionView.h"
#import "YSVideoPlayerView.h"
#import "VideoModel.h"

typedef enum {
    PinterestViewCellTypeMultiShowroom,
    PinterestViewCellTypeMintDetails,
    PinterestViewCellTypeAddMintDescription,
    PinterestViewCellTypeAddMintOptions,
    PinterestViewCellTypeAddMintImage,
    PinterestViewCellTypeVideo,
    PinterestViewCellTypeDefault // No selection indicator
} PinterestViewCellType;


@class PrinterestGridTable;

@protocol PinterestTableViewDataSource <NSObject>

@optional

@required

- (PinterestViewCellType)DiscoverTableView:(PrinterestGridTable *)tableView cellTypeForSection:(NSInteger)section;
- ( NSInteger)DiscoverTableViewNumberOfsections:(PrinterestGridTable *)tableView;

@optional
-(JitenCollectionView *)showroomCollectionViewForRowAtIndexPath:(NSIndexPath *)indexPath;

-(CGFloat)DiscoverTableView:(PrinterestGridTable *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSDictionary *)DiscoverTableView:(PrinterestGridTable *)tableView DataForSection:(NSInteger)section;
- (SPHMintModel *)DiscoverTableView:(PrinterestGridTable *)tableView MintDataForSection:(NSInteger)section;
- (NSString *)DiscoverTableView:(PrinterestGridTable *)tableView TitleOfHeaderForSection:(NSInteger)section;
- (YSCommentView *)addMintDescriptionTextViewforRow:(NSInteger)row;
- (UILabel *)addMintCounterLabelforRow:(NSInteger)row;

- (YSVideoPlayerView *)videoPlayerForDiscoverTableView:(PrinterestGridTable *)tableView ForRowAtIndexPath:(NSIndexPath *)indexPath;
- (VideoModel *)VideoTableViewViewDataForRow:(NSInteger)row;
-(NSInteger)DiscoverTableView:(PrinterestGridTable *)tableView selectedRowAtIndexPath:(NSIndexPath *)indexPath;
@end








