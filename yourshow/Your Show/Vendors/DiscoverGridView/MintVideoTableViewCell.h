//
//  MintVideoTableViewCell.h
//  Your Show
//
//  Created by Siba Prasad Hota on 27/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"




@class MintVideoTableViewCell;

@protocol videoCellDelegate

@optional

- (void)didClickedVideoBtn:(MintVideoTableViewCell *)videoCell AndvcelType:(VideoCellType)celltype;

@end


@interface MintVideoTableViewCell : UITableViewCell

@property (weak, nonatomic)   IBOutlet UIImageView *mintImageView;
@property (strong, nonatomic) IBOutlet UIButton *videoPlaybtn;
@property (assign, nonatomic) VideoCellType vcellType;
@property (strong, nonatomic) IBOutlet UIView *frontView;

- (IBAction)playbtnClicked:(id)sender;
-(void)setCustomVideoDelegate:(id)sender;

@property (nonatomic, assign)IBOutlet id <videoCellDelegate> videoDelegate;
@property (nonatomic, strong) NSURL *videoUrl;

-(void)setCelldataForVideoCell:(VideoModel *)someModel;

@end





