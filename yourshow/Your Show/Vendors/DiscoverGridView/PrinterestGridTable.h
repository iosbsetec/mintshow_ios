//
//  CuisineListTable.h
//  GebeChat
//
//  Created by Siba Prasad Hota  on 6/13/15.
//  Copyright (c) 2015 WemakeAppz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PinterestTableViewDataSource.h"
#import "MintVideoTableViewCell.h"

@class PrinterestGridTable;
@class ACAccountStore;
@class ACAccount;
@class ShowRoomModel;

@protocol PinterestTableDelegate

@optional

- (void)didSelectSelectOptionButtonClicked: (PrinterestGridTable *) sender forButton :(NSInteger) section;
- (void)didPostMintButtonClicked: (PrinterestGridTable *) sender;
- (void)didLocationButtonClicked: (PrinterestGridTable *) sender;
- (void)didCategoryButtonClicked: (PrinterestGridTable *) sender;
- (void)LoadNextPagetriggered;
-(void)onEditMediaTapped;
-(void)onClearMediaTapped;

@end

@interface PrinterestGridTable : UITableView < UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) BOOL selectedButtonTag;

@property (nonatomic, assign) BOOL isLoadingFinished;
@property (nonatomic,strong)  NSMutableArray  *ArrayTobepassed;
@property (nonatomic, assign) BOOL isFromMintDetail;
@property (nonatomic,strong)  UIView *viewEditMediaHolder;

@property (nonatomic, assign) IBOutlet id <PinterestTableDelegate> Pinterestdelegate;
@property (nonatomic, assign) IBOutlet id<PinterestTableViewDataSource> PinterestDataSource;
@property (nonatomic,retain) ACAccountStore *accountStore;
@property (nonatomic,retain) ACAccount *account;

// All options regarding Addmint
-(IBAction)addmintOptionButtonClicked:(id)sender;
-(IBAction)addmintPostButtonClicked:(id)sender;
-(void)setLocation:(NSString*)locationData withSection : (NSUInteger )sec;
-(void)setCategoryText:(NSString*)categoryData withSection : (NSUInteger )sec;
-(void)setAllHeaderButtonNormal;
-(void)setButtonSelectdWithTag : (NSInteger ) tagValue;

@end


