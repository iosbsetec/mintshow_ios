//
//  JitenCollectionView.h
//  Your Show
//
//  Created by Siba Prasad Hota  on 7/30/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JitenCollectionViewDataSource.h"


@class JitenCollectionView;





@protocol JitenCollectionTableDelegate <NSObject>
- (void) JitenCollectionViewJoinClicked: (NSInteger) selectedRow;
- (void) JitenCollectionViewAddClicked: (JitenCollectionView *) sender;
- (void) JitenCollectionViewDelegateMethod: (ShowRoomModel *) selectedData selectedIndex :(NSInteger) selectedRowNo;
-(void)loadNextShowrooms;

@optional
-(void)didScrolledUp:(CGFloat)yPosition;
-(void)didScrolledDown:(CGFloat)yPosition;

@end

@interface JitenCollectionView : UICollectionView <UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic, assign) BOOL Addshowroom,relatedShowRoom;
@property (nonatomic, assign) BOOL isCellSelectable;
@property (nonatomic, assign) BOOL showJoinButton;
@property (nonatomic) CGFloat lastContentOffset;

@property (nonatomic, retain) id <JitenCollectionTableDelegate> collectiondelegate;
@property (nonatomic, assign) IBOutlet id<JitenCollectionViewDataSource> CollectionDataSource;
-(void)batchinsertCollectionView:(NSIndexPath *)indexPath
;
@end


