//
//  JitenCollectionView.m
//  Your Show
//
//  Created by Siba Prasad Hota  on 7/30/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "JitenCollectionView.h"
#import "CellDiscoverShowrooms.h"
#import "CellAddShowroom.h"
#import "YSAPICalls.h"
#import "MSPShowroomVC.h"

@implementation JitenCollectionView

@synthesize collectiondelegate = _collectiondelegate;
@synthesize CollectionDataSource = _CollectionDataSource;


//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    
//    if ([self.collectiondelegate isKindOfClass:[MSPShowroomVC class]])
//    {
//        int yPos =  scrollView.contentOffset.y;
//        if (self.lastContentOffset > scrollView.contentOffset.y)
//        {
//            //            NSLog(@"Current YPos Up: %d",yPos);
//            
//            [self.collectiondelegate didScrolledUp:(scrollView.contentOffset.y<0)?0:(yPos>86)?86:scrollView.contentOffset.y];
//            //            NSLog(@"Scrolling Up");
//        }
//        else if (self.lastContentOffset < scrollView.contentOffset.y)
//        {
//            //            NSLog(@"Current YPos Down: %f",(scrollView.contentOffset.y>86)?86:scrollView.contentOffset.y);
//            if(yPos<0||yPos==0)
//            {
//            }
//            else
//                [self.collectiondelegate didScrolledDown:(scrollView.contentOffset.y>86)?86:scrollView.contentOffset.y];
//        }
//        self.lastContentOffset = scrollView.contentOffset.y;
//    }
//}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([self.collectiondelegate isKindOfClass:[MSPShowroomVC class]])
    {
        int yPos =  scrollView.contentOffset.y;
        if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            //            NSLog(@"Current YPos Up: %d",yPos);
            
            [self.collectiondelegate didScrolledUp:(scrollView.contentOffset.y<0)?0:scrollView.contentOffset.y];
            //            NSLog(@"Scrolling Up");
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            if(yPos<0||yPos==0)
            {
            }
            else
                [self.collectiondelegate didScrolledDown:scrollView.contentOffset.y];
        }
        self.lastContentOffset = scrollView.contentOffset.y;
    }
}





- (void)initializator
{
    self.delegate = self;
    self.dataSource = self;
 
    self.isCellSelectable = NO;
    self.Addshowroom = YES;
    
    UINib *nibCellDiscoverShowrooms = [UINib nibWithNibName:@"CellDiscoverShowrooms" bundle: nil];
    [self registerNib:nibCellDiscoverShowrooms forCellWithReuseIdentifier:@"CellDiscoverShowrooms"];
    
    UINib *nibCellAddShowroom = [UINib nibWithNibName:@"CellAddShowroom" bundle: nil];
    [self registerNib:nibCellAddShowroom forCellWithReuseIdentifier:@"CellAddShowroom"];


}
- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  (_Addshowroom)?([self.CollectionDataSource NumberOFrowsForCollectionViewView:self]+1):[self.CollectionDataSource NumberOFrowsForCollectionViewView:self];
  //  return (_Addshowroom)?7:;

}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.collectiondelegate JitenCollectionViewDelegateMethod:[self.CollectionDataSource JitenCollectionViewView:self DataForItemAtRow:(!_Addshowroom)?indexPath.row:(indexPath.row-1)] selectedIndex:indexPath.row];
    
    [self batchinsertCollectionView:indexPath];
}
- (CGSize)collectionViewContentSize
{
    return CGSizeMake(320, 225);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    if (indexPath.row == 0  && _Addshowroom )
    {
        NSString *identifier = @"CellAddShowroom";
        
        CellAddShowroom *cell = (CellAddShowroom*)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
       [cell setBackgroundColor:SHOWROOMBACKGROUNDCOLOR];
        
        cell.layer.cornerRadius = 4.0;
        [cell.btnAddshowroom addTarget:self action:@selector(presentAddShowroomView:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else
    {
        NSString *identifier = @"CellDiscoverShowrooms";
        
        CellDiscoverShowrooms *cell = (CellDiscoverShowrooms*)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        ShowRoomModel *data = [self.CollectionDataSource JitenCollectionViewView:self DataForItemAtRow:(!_Addshowroom)?indexPath.row:(indexPath.row-1)];
        cell.Cellimgview.backgroundColor =[YSSupport randomColor];
        cell.Cellimgview.image=nil;
        if (cell.Cellimgview.image == nil) {
            cell.Cellimgview.mintImageURL = [NSURL URLWithString:data.showroom_imageSmall];
        }

        UILabel *producttitle = (UILabel *)[cell viewWithTag:101];
       
        // Create the path (with only the top-left corner rounded)
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.Cellimgview.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight                                                         cornerRadii:CGSizeMake(6.0, 6.0)];
        // Create the shape layer and set its path
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = cell.Cellimgview.bounds;
        maskLayer.path = maskPath.CGPath;
        // Set the newly created shape layer as the mask for the image view's layer
        cell.Cellimgview.layer.mask      = maskLayer;
        cell.Cellimgview.backgroundColor = BACKGROUNDCOLOR;
        producttitle.text        = data.show_tag;
        cell.lblMemberCount.text = [NSString stringWithFormat:@"%@",data.member_count];
        cell.lblMintCount.text   = [NSString stringWithFormat:@"%@",data.mint_count]; ;
        if (_Addshowroom) {

            if (![data.show_option isEqualToString:@"owner"]) {
                [cell.btnJoin setBackgroundColor:([data.show_option isEqualToString:@""])?ORANGECOLOR:[UIColor whiteColor]];
                [cell.btnJoin setTitle:([data.show_option isEqualToString:@""])?@"JOIN":@"JOINED" forState:UIControlStateNormal];
                [cell.btnJoin setTitleColor:([data.show_option isEqualToString:@""])?[UIColor whiteColor]:ORANGECOLOR forState:UIControlStateNormal];
                cell.btnJoin.layer.borderColor = ([data.show_option isEqualToString:@""])?[UIColor clearColor].CGColor:[[UIColor colorWithRed:(243/255.0f) green:(94/255.0f) blue:(27/255.0f) alpha:1.0]CGColor];
                cell.btnJoin.layer.borderWidth = 1.3f;
                cell.btnJoin.layer.masksToBounds = YES;

            }
           else
           {
               [cell.btnJoin setBackgroundColor:ORANGECOLOR];
               [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
               [cell.btnJoin setTitle:@"EDIT" forState:UIControlStateNormal];

           }
            [cell.btnJoin addTarget:self action:@selector(joinTapped:) forControlEvents:UIControlEventTouchUpInside];

                cell.btnJoin.tag = indexPath.row-1;
                [cell setBackgroundColor:[UIColor whiteColor]];
        }
        
        else
        {
//            if (_showJoinButton) {
//                    cell.btnJoin.hidden = NO;
//                    [cell.btnJoin setBackgroundColor:([data.show_option isEqualToString:@""])?ORANGECOLOR:[UIColor whiteColor]];
//                    [cell.btnJoin setTitle:([data.show_option isEqualToString:@""])?@"JOIN":@"JOINED" forState:UIControlStateNormal];
//                    [cell.btnJoin setTitleColor:([data.show_option isEqualToString:@""])?[UIColor whiteColor]:ORANGECOLOR forState:UIControlStateNormal];
//                    cell.btnJoin.layer.borderColor = ([data.show_option isEqualToString:@""])?[UIColor clearColor].CGColor:[[UIColor colorWithRed:(243/255.0f) green:(94/255.0f) blue:(27/255.0f) alpha:1.0]CGColor];
//                    cell.btnJoin.layer.borderWidth = 1.3f;
//                    cell.btnJoin.layer.masksToBounds = YES;
//                    cell.btnJoin.tag = indexPath.row;
//                   [cell.btnJoin addTarget:self action:@selector(joinTapped:) forControlEvents:UIControlEventTouchUpInside];
//
//                
//
//            }
//            else
//                cell.btnJoin.hidden = YES;
//               (_relatedShowRoom)?[cell setBackgroundColor:[UIColor whiteColor]]:[cell setBackgroundColor:BACKGROUNDCOLOR];
//            
            
            
            if (_showJoinButton) {
                cell.btnJoin.hidden = NO;
                if (![data.show_option isEqualToString:@"owner"])
                {
                    [cell.btnJoin setBackgroundColor:([data.show_option isEqualToString:@""])?ORANGECOLOR:[UIColor whiteColor]];
                    [cell.btnJoin setTitle:([data.show_option isEqualToString:@""])?@"JOIN":@"JOINED" forState:UIControlStateNormal];
                    [cell.btnJoin setTitleColor:([data.show_option isEqualToString:@""])?[UIColor whiteColor]:ORANGECOLOR forState:UIControlStateNormal];
                }
                else
                {
                    [cell.btnJoin setBackgroundColor:ORANGECOLOR];
                    [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [cell.btnJoin setTitle:@"EDIT" forState:UIControlStateNormal];
                }
                
                cell.btnJoin.layer.borderColor = ([data.show_option isEqualToString:@""])?[UIColor clearColor].CGColor:[[UIColor colorWithRed:(243/255.0f) green:(94/255.0f) blue:(27/255.0f) alpha:1.0]CGColor];
                cell.btnJoin.layer.borderWidth = 1.3f;
                cell.btnJoin.layer.masksToBounds = YES;
                cell.btnJoin.tag = indexPath.row;
                [cell.btnJoin addTarget:self action:@selector(joinTapped:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
                cell.btnJoin.hidden = YES;
            (_relatedShowRoom)?[cell setBackgroundColor:[UIColor whiteColor]]:[cell setBackgroundColor:BACKGROUNDCOLOR];
        }
        

        if (_isCellSelectable && [[data selected] integerValue] == 1) {
                cell.checkMarck.hidden = NO;
                cell.checkMarck.layer.mask = maskLayer;
        }
        else
                cell.checkMarck.hidden = YES;


  
//      producttitle.text = data.showroom_name;
        producttitle.tintColor = [UIColor blackColor];
        return cell;

    }
}


-(UIColor *)randomColor
{
    CGFloat red = arc4random_uniform(255) / 255.0;
    CGFloat green = arc4random_uniform(255) / 255.0;
    CGFloat blue = arc4random_uniform(255) / 255.0;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    return color;
}


-(void)joinTapped : (UIButton *) button {
    NSLog(@"%ld",(long)button.tag);
    ShowRoomModel *data = [self.CollectionDataSource JitenCollectionViewView:self DataForItemAtRow:(button.tag)];
    if ([button.titleLabel.text isEqualToString:@"EDIT"]) {
        [self.collectiondelegate JitenCollectionViewDelegateMethod:data selectedIndex:button.tag];
    } else {
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&inviter_id=0&showroom_tag=%@",GETVALUE(CEO_AccessToken),data.show_tag] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding ];
    
    NSLog(@"request %@",str);
    NSLog(@"URL %@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_JOINSHOWROOM]]);

    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_JOINSHOWROOM]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSDictionary *showroomDict = [self dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"StatusMsg"]);
         
         AppDelegate *delegateApp = MINTSHOWAPPDELEGATE
         delegateApp.showroomJoienedCount = [[showroomDict valueForKey:@"count"] integerValue];
         (delegateApp.showroomJoienedCount == 0)?SETVALUE(@"no", CEO_ISJOINEDSHOWROOM):SETVALUE(@"yes", CEO_ISJOINEDSHOWROOM);
         SYNCHRONISE;
         
         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             [_collectiondelegate JitenCollectionViewJoinClicked:button.tag];
         }
         else
         {
             
         }
     }];
    }
}


- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            return object;
        }
        else
        {
            return (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    return dictionary;
}


-(IBAction)presentAddShowroomView:(id)sender
{
    [_collectiondelegate JitenCollectionViewAddClicked:self];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize mElementSize;
//    if (iPhone4 || iPhone5) {
//        mElementSize = CGSizeMake(150, 150);
//    }
//    else
//    mElementSize = CGSizeMake(177.5, 150);

    mElementSize = CGSizeMake(150, 150);

    return mElementSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(2,4,2,5);
    
    
}
-(void)batchinsertCollectionView:(NSIndexPath *)indexPath
{
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self performBatchUpdates:^{
        
        [self reloadItemsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil]];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView setAnimationsEnabled:animationsEnabled];
  
        }
    }];
}

static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.75;

static BOOL ShouldLoadNextPage(UICollectionView *collectionView){
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self);
    if (shouldLoadNextPage)  {
        [self.collectiondelegate loadNextShowrooms];
    }
}

@end
