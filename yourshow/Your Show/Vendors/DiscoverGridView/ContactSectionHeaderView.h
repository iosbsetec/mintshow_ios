

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol SectionHeaderViewDelegate;

@interface ContactSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic) NSInteger section;
@property (weak, nonatomic) IBOutlet UIButton *selectAllBtn;
@property (nonatomic, weak) IBOutlet UILabel *lblRightArrow;

@end



