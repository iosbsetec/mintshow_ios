//
//  PPImageScrollingCellView.m
//  PPImageScrollingTableViewDemo
//
//  Created by popochess on 13/8/9.
//  Copyright (c) 2013年 popochess. All rights reserved.
//

#import "PPImageScrollingCellView.h"
#import "PPCollectionViewCell.h"
#import "DiscoverDataModel.h"
#import "STTweetLabel.H"
#import "NSString+Emojize.h"
#import "PPCollectionTextCell.h"
@interface  PPImageScrollingCellView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) PPCollectionViewCell *myCollectionViewCell;
@property (strong, nonatomic) UICollectionView *myCollectionView;
@property (strong, nonatomic) NSArray *collectionImageData;
@property (nonatomic) CGFloat imagetitleWidth;
@property (nonatomic) CGFloat imagetitleHeight;
@property (strong, nonatomic) UIColor *imageTilteBackgroundColor;
@property (strong, nonatomic) UIColor *imageTilteTextColor;


@end

@implementation PPImageScrollingCellView

- (id)initWithFrame:(CGRect)frame
{
     self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code

        /* Set flowLayout for CollectionView*/
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.itemSize = CGSizeMake(100.0, 140.0);
        flowLayout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
        flowLayout.minimumLineSpacing = 10;

        /* Init and Set CollectionView */
        self.myCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
        self.myCollectionView.delegate = self;
        self.myCollectionView.dataSource = self;
        self.myCollectionView.showsHorizontalScrollIndicator = NO;

        [self.myCollectionView registerClass:[PPCollectionViewCell class] forCellWithReuseIdentifier:@"PPCollectionCell"];
        [self.myCollectionView registerClass:[PPCollectionTextCell class] forCellWithReuseIdentifier:@"PPCollectionTextCell"];

         self.myCollectionView.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
        [self addSubview:_myCollectionView];
        
    }
    return self;
}

- (void) setImageData:(NSArray*)collectionImageData{

    _collectionImageData = collectionImageData;
    [_myCollectionView reloadData];
}

- (void) setBackgroundColor:(UIColor*)color{

   
    [_myCollectionView reloadData];
}

- (void) setImageTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height{
    _imagetitleWidth = width;
    _imagetitleHeight = height;
}
- (void) setImageTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor{
    
    _imageTilteTextColor = textColor;
    _imageTilteBackgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.collectionImageData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    DiscoverDataModel *model = [self.collectionImageData objectAtIndex:[indexPath row]];

    
    if ([model.discoverType isEqualToString:@"TEXT"]) {
        PPCollectionTextCell *cell = (PPCollectionTextCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"PPCollectionTextCell" forIndexPath:indexPath];
        
        [cell setTextTitleLabelWitdh:_imagetitleWidth withHeight:_imagetitleHeight];
        [cell setTextTitleTextColor:_imageTilteTextColor withBackgroundColor:_imageTilteBackgroundColor];
        [cell setTitle:model.discoverName];
        [cell setMintDescription:model.discoverText];
        cell.descriptionTextView.tag = indexPath.item;

        cell.descriptionTextView.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
        {
            if (hotWord == STTweetHashtag)
            {
                [self onHashTagTapped:indexPath];
            }
            else if (hotWord == STTweetStartag)
            {
                [self gotoShowroomPage:string];
            }
            else if(hotWord==STTweetMention)
            {
                [self gotoProfilePage:string index:indexPath];

            }
        };
        
        return  cell;

    }
    else
    {
    PPCollectionViewCell *cell = (PPCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"PPCollectionCell" forIndexPath:indexPath];
    
    [cell setImageTitleLabelWitdh:_imagetitleWidth withHeight:_imagetitleHeight];
    [cell setImageTitleTextColor:_imageTilteTextColor withBackgroundColor:_imageTilteBackgroundColor];
    [cell setTitle:model.discoverName];
    [cell setImageUrl:[NSURL URLWithString:model.discoverImg]];
        return cell;
    }
        model = nil;
    
}
-(void)gotoProfilePage:(NSString *)selectedEnitity index:(NSIndexPath *)indexPath
{
    DiscoverDataModel *model = [self.collectionImageData objectAtIndex:[indexPath row]];
    NSString *strUserId = [self checkUserNameExistWithName:[selectedEnitity substringFromIndex:1] model:model];
    [self.delegate scollingCellAtmentionElementTapped:strUserId];
}

-(NSString *)checkUserNameExistWithName:(NSString *)selectUserName model:(DiscoverDataModel *)disModel
{
    if(disModel.at_mentioned_users.count>0)
    {
        for (int i=0; i<disModel.at_mentioned_users.count; i++) {
            NSString *strName = [[disModel.at_mentioned_users objectAtIndex:i]valueForKey:@"name"];
            strName = [strName stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([[strName lowercaseString] isEqualToString:[selectUserName lowercaseString]]) {
                return [[disModel.at_mentioned_users objectAtIndex:i]valueForKey:@"id"];
            }
        }
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    [self.delegate collectionView:self didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath];
    
    DiscoverDataModel *model = [self.collectionImageData objectAtIndex:[indexPath row]];
    
    [self.delegate collectionView:self Withdata:model didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath];
}
-(void)onHashTagTapped:(NSIndexPath *)indexPath
{
    DiscoverDataModel *model = [self.collectionImageData objectAtIndex:[indexPath row]];
    [self.delegate collectionView:self Withdata:model didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath];
}

-(void)gotoShowroomPage:(NSString *)selectedEnitity
{
    [self.delegate scollingCellStarElementTapped:selectedEnitity];
}

-(void)gotoProfilePage:(NSString *)selectedEnitity
{
    [self.delegate scollingCellAtmentionElementTapped:selectedEnitity];
}



#pragma mark CHECK IF NEED TO LOAD MORE

static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.89;

static BOOL ShouldLoadNextPage(UICollectionView *collectionView){
    CGFloat xOffset = collectionView.contentOffset.x;
    CGFloat weidth = collectionView.contentSize.width - CGRectGetWidth(collectionView.frame);
    return xOffset / weidth > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UICollectionView *)scrollView {
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self.myCollectionView);
    if (shouldLoadNextPage)  {
        [self.delegate didLoadNextPgeTriggered];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
 if ([model.discoverType isEqualToString:@"TEXT"]) {
 [cell setTitle:@"JITENDRA"];
 [cell setImageTitleTextColor:[UIColor greenColor] withBackgroundColor:[UIColor whiteColor]];
 
 }
 else
 {
 [cell setTitle:model.discoverName];
 [cell setImageTitleTextColor:_imageTilteTextColor withBackgroundColor:_imageTilteBackgroundColor];
 
 }

*/

@end
