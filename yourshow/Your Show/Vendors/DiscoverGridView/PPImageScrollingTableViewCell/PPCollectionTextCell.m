//
//  PPCollectionTextCell.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 09/02/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//
//UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:9];
//CGFloat commentTextHeight  = [[@"#mintshow wow mintshow" emojizedString] boundingRectWithSize:CGSizeMake(90, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
//commentTextHeight = (commentTextHeight>100)?100:commentTextHeight;
//STTweetLabel *remint_message    = [[STTweetLabel alloc]initWithFrame:CGRectMake(5, 10+((100-commentTextHeight)/2),90, commentTextHeight)];
//[remint_message setBackgroundColor:[UIColor clearColor]];
//remint_message.text             =  [@"#mintshow wow mintshow" emojizedString];
//remint_message.font             =font;
//remint_message.textAlignment    = NSTextAlignmentCenter;
//[cell addSubview:remint_message];
//remint_message.tag = 123;




#import "PPCollectionTextCell.h"
#import "NSString+Emojize.h"
@interface PPCollectionTextCell ()

@property (strong, nonatomic) UITextView *imageTitle;
@property (strong, nonatomic) UIView *viewBg;


@end

//        ;

@implementation PPCollectionTextCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

        self.viewBg = [[UIView alloc]initWithFrame:CGRectMake(0,10.0, 100, 100)];
        self.viewBg.backgroundColor = [UIColor whiteColor];
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:9];
        self.descriptionTextView   = [[STTweetLabel alloc]init];
        self.descriptionTextView.font             =font;
        self.viewBg.layer.cornerRadius = 4.0;
        self.viewBg.clipsToBounds = YES;
        [self.descriptionTextView  setBackgroundColor:[UIColor clearColor]];

        
    }
    return self;
}
- (void) setMintDescription:(NSString*) title
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:9];

   self.descriptionTextView.text             =  [title emojizedString];
    self.descriptionTextView.textAlignment    = NSTextAlignmentCenter;
    CGFloat commentTextHeight  = [[title emojizedString] boundingRectWithSize:CGSizeMake(100, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    commentTextHeight = (commentTextHeight>80)?100:commentTextHeight+30;
    [self.descriptionTextView setFrame:CGRectMake(0, ((100-commentTextHeight)/2),100, commentTextHeight)];
    self.descriptionTextView.center = self.viewBg.center;

}


- (void) setTextTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height
{
    self.imageTitle = [[UITextView alloc] initWithFrame:CGRectMake(0,105, width,30)];
    self.imageTitle.textAlignment= NSTextAlignmentCenter;
    self.imageTitle.contentInset = UIEdgeInsetsMake(1,1,1,1);
    self.imageTitle.userInteractionEnabled = NO;
}

- (void) setTextTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor
{
    self.imageTitle.textColor = [UIColor darkGrayColor];
    self.imageTitle.backgroundColor = [UIColor clearColor];

}

-(void)setTitle:(NSString*)title
{
    if ([self.contentView subviews]){
        for (UILabel *subview in [self.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    [self.contentView addSubview:self.viewBg];
    [self.contentView addSubview:self.descriptionTextView];
    self.imageTitle.text = title;
    [self.contentView addSubview:self.imageTitle];
    
}




@end
