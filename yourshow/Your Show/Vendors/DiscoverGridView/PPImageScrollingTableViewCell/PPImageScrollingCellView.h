//
//  PPImageScrollingCellView.h
//  PPImageScrollingTableViewDemo
//

#import <UIKit/UIKit.h>
#import "DiscoverDataModel.h"

@class PPImageScrollingCellView;

@protocol PPImageScrollingViewDelegate <NSObject>

- (void)collectionView:(PPImageScrollingCellView *)collectionView  Withdata:(DiscoverDataModel*)discoverdata didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath;
-(void)didLoadNextPgeTriggered;

@optional
-(void)scollingCellStarElementTapped:(NSString *)selectedEnitity;
-(void)scollingCellAtmentionElementTapped:(NSString *)selectedEnitity;


@end


@interface PPImageScrollingCellView : UIView

@property (weak, nonatomic) id<PPImageScrollingViewDelegate> delegate;

- (void) setImageTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height;
- (void) setImageTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor;
- (void) setImageData:(NSArray*)collectionImageData;
- (void) setBackgroundColor:(UIColor*)color;

@end
