//
//  PPCollectionViewCell.m
//  PPImageScrollingTableViewControllerDemo
//
//  Created by popochess on 13/8/10.
//  Copyright (c) 2013年 popochess. All rights reserved.
//

#import "PPCollectionViewCell.h"

@interface PPCollectionViewCell ()

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UITextView *imageTitle;

@end

@implementation PPCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,10.0, 100, 100)];
        self.imageView.layer.cornerRadius = 4.0;
        self.imageView.clipsToBounds = YES;
        self.imageView.backgroundColor = [YSSupport randomColor];
        self.imageView.image =[UIImage imageNamed:@"imgPlaceholder"];
    }
    return self;
}



-(void)setImage:(UIImage *)image
{
    self.imageView.image = image;
}

-(void)setImageUrl:(NSURL *)imageUrl {
    self.imageView.alpha = 0.0;
    self.imageView.image = nil;
    self.imageView.mintImageURL = imageUrl;
}


- (void)setImageTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height
{
    self.imageTitle = [[UITextView alloc] initWithFrame:CGRectMake(0,105, width,30)];
    self.imageTitle.textAlignment= NSTextAlignmentCenter;
    self.imageTitle.contentInset = UIEdgeInsetsMake(1,1,1,1);
    self.imageTitle.userInteractionEnabled = NO;
}

- (void)setImageTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor
{
    self.imageTitle.textColor = [UIColor colorWithRed:(74/255.f) green:(74/255.f) blue:(74/255.f) alpha:1.0f];
    self.imageTitle.backgroundColor = [UIColor clearColor];
}

- (void)setTitle:(NSString*)title ;
{
    if ([self.contentView subviews]){
        for (UILabel *subview in [self.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    [self.contentView addSubview:self.imageView];
    self.imageTitle.font =  [UIFont fontWithName:@"HelveticaNeueLTStd-Md/65 Medium" size:12];
    self.imageTitle.text = title;
    [self.contentView addSubview:self.imageTitle];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
