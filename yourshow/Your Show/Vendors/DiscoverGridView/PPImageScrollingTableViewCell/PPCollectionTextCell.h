//
//  PPCollectionTextCell.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 09/02/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTweetLabel.h"
@interface PPCollectionTextCell : UICollectionViewCell

@property (strong, nonatomic) STTweetLabel *descriptionTextView;

- (void) setTitle:(NSString*) title;
- (void) setTextTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height;
- (void) setTextTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor;
- (void) setMintDescription:(NSString*) title;

@end
