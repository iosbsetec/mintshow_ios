//
//  DiscoverTableview.h
//  Your Show
//
//  Created by Siba Prasad Hota on 05/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscoverTableviewDataSource.h"
#import "ShowRoomModel.h"


@protocol DiscoverTableDelegate <NSObject>

@optional

- (void)didSelectSelectRow:(NSInteger)row inSection:(NSInteger)section;
- (void)didSelectSelectAllButtonClickedforSection :(NSInteger) section;
- (void)didSelectSelectOptionButtonClickedForSection :(NSInteger) section;
- (void)didSelectSelectShowroomData:(ShowRoomModel *)modelData row:(NSInteger)row inSection:(NSInteger)section;
- (void)LoadNextPagetriggeredForSection:(NSInteger)section;
- (void)scrollViewSignatureShowroomDidScroll:(UIScrollView *)scrollView;
@end




@interface DiscoverTableview : UITableView< UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) CGFloat lastContentOffset;

@property (nonatomic,strong)  NSMutableArray  *ArrayTobepassed;
@property (nonatomic, assign) IBOutlet id <DiscoverTableDelegate> Discoverdelegate;
@property (nonatomic, assign) IBOutlet id<DiscoverTableViewDataSource> DiscoverDataSource;
@property (nonatomic,assign)   BOOL  showArrowButton;
@property (nonatomic,assign)   BOOL  isSignatureShowroom;


@end
