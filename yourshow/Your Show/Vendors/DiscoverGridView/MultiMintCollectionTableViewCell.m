//
//  MultiMintCollectionTableViewCell.m
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "MultiMintCollectionTableViewCell.h"
#import "Feedmodel.h"


@implementation MultiMintCollectionTableViewCell

- (void)awakeFromNib
{
    
    self.discovershowroomcollectionview.backgroundColor = [UIColor whiteColor];
    self.discovershowroomcollectionview.showsHorizontalScrollIndicator = NO;
    self.discovershowroomcollectionview.scrollEnabled = NO;
    
    self.discovershowroomcollectionview.multiMintDataSource = self.mintDelegate;
    self.discovershowroomcollectionview.multiMintdelegate = self.mintDelegate;
    
    self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
    // Initialization code
}

-(CGFloat)heightOfCollectionView
{
    CGFloat height = self.discovershowroomcollectionview.collectionViewLayout.collectionViewContentSize.height;
    return height;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    // self.discovershowroomcollectionview.frame = self.contentView.bounds;
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
