 //
//  CuisineListTable.m
//  GebeChat
//
//  Created by Siba Prasad Hota  on 6/13/15.
//
#define twitterError @"There are no Twitter accounts configured. You can add or create a Twitter account in Settings."
#define twitterPermission @"Permission not granted"

#define Youtube @"https://www.youtube.com/watch?v=2PPC_2m9xFs"
#define arrayRightTableData @[@"Search By",@"Search Category"]
#define arraySearchBy       @[@"All Members",@"My Mints",@"My C-suite",@"Planned to do",@"Nomintated"]
#define arrayCategory       @[@"BeingHappy",@"Career",@"EnterPrenure",@"ExerCise"]

#import "PrinterestGridTable.h"
#import "ContactSectionHeaderView.h"
#import "SPHSingletonClass.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import  <UIKit/UIKit.h>
#import "NSString+YaacMention.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "SocialVideoHelper.h"


// All Table view Cell List
#import "PPImageScrollingTableViewCell.h"
#import "LoadingDataTableViewCell.h"
#import "CollectionViewTableViewCell.h"
#import "MintDescriptionTableViewCell.h"
#import "MintSelectOptionTableViewCell.h"
#import "MintImageTableViewCell.h"
#import "EmptyTableViewCell.h"

#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import <Social/Social.h>


#import "MultimintViewDataSource.h"
#import "UIImage+UIImagescaling.h"

static NSString *ContactSectionHeaderViewIdentifier = @"ContactSectionHeaderViewIdentifier";
@interface PrinterestGridTable ()<FBSDKSharingDelegate,videoCellDelegate>
{

    NSString *searchedvalue;
    BOOL isPlayStarted;

}
@property (nonatomic, strong) ACAccount* facebookAccount;
@property (nonatomic, strong) NSArray *colorArray;
@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;

@end

@implementation PrinterestGridTable

@synthesize PinterestDataSource = _PinterestDataSource;
@synthesize Pinterestdelegate = _Pinterestdelegate;
@synthesize isLoadingFinished=_isLoadingFinished;





- (void)initializator
{
    self.selectedButtonTag = 0;
    self.delegate = self;
    self.dataSource = self;
    _isLoadingFinished=NO;
    
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"ContactSectionHeaderView" bundle:nil];
    [self registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:ContactSectionHeaderViewIdentifier];
    
    static NSString *CellIdentifier = @"Cell";
    [self registerClass:[PPImageScrollingTableViewCell class] forCellReuseIdentifier:CellIdentifier];
    self.ArrayTobepassed = [[NSMutableArray alloc]init];

    
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
   

    self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
   
}

- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) [self initializator];
    return self;
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.PinterestDataSource DiscoverTableViewNumberOfsections:self];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    PinterestViewCellType celltype = [self.PinterestDataSource DiscoverTableView:self cellTypeForSection:section];
    
    if(celltype == PinterestViewCellTypeMintDetails||celltype == PinterestViewCellTypeAddMintImage||celltype == PinterestViewCellTypeAddMintDescription||celltype == PinterestViewCellTypeAddMintOptions||celltype ==PinterestViewCellTypeVideo)
    {
        return 0.0;
    }
    else if (celltype == PinterestViewCellTypeMultiShowroom)
             return 25.0;
    else
        return 40.0;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ContactSectionHeaderView *sectionHeaderView = [self dequeueReusableHeaderFooterViewWithIdentifier:ContactSectionHeaderViewIdentifier];
    sectionHeaderView.titleLabel.text =[self.PinterestDataSource DiscoverTableView:self TitleOfHeaderForSection:section];
    
   PinterestViewCellType celltype = [self.PinterestDataSource DiscoverTableView:self cellTypeForSection:section];
    
    if (celltype == PinterestViewCellTypeMultiShowroom )
    {
        sectionHeaderView.titleLabel.textAlignment = NSTextAlignmentCenter;
        sectionHeaderView.titleLabel.textColor = [UIColor blackColor];

    }
    sectionHeaderView.selectAllBtn.alpha=0.0;
    sectionHeaderView.lblRightArrow.alpha=0.0;

    sectionHeaderView.section = section;
    return sectionHeaderView;
}


#pragma mark - UITableViewDelegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!_isLoadingFinished)
        return 70;
   
    else
        return [self.PinterestDataSource DiscoverTableView:self heightForRowAtIndexPath:indexPath];
    
    return 230;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if(!_isLoadingFinished)
    {
        static NSString *lCellIdentifier = @"LoadingDataTableViewCell";
        
        LoadingDataTableViewCell  *newcell = (LoadingDataTableViewCell *)[self dequeueReusableCellWithIdentifier:lCellIdentifier];
        if (newcell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LoadingDataTableViewCell" owner:self options:nil];
            newcell = [topLevelObjects objectAtIndex:0];
        }
        return newcell;
    }
    
    
    PinterestViewCellType celltype = [self.PinterestDataSource DiscoverTableView:self cellTypeForSection:indexPath.section];
   
   
     if (celltype == PinterestViewCellTypeMultiShowroom)
     {
         static NSString *CellIdentifier = @"SCollectionViewTableViewCell";
         EmptyTableViewCell *cell = (EmptyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
         
         if (cell == nil) {
             NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableViewCell" owner:self options:nil];
             cell = [topLevelObjects objectAtIndex:0];
         }
         for(id aview in [cell.contentView subviews])
         {
             [aview removeFromSuperview];
         }
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
         [cell.contentView addSubview:[self.PinterestDataSource showroomCollectionViewForRowAtIndexPath:indexPath]];
         
         return cell;
         
     }
    
    if (celltype == PinterestViewCellTypeAddMintDescription)
    {
        static NSString *CellIdentifier = @"MintDescriptionTableViewCell";
        
        MintDescriptionTableViewCell *cell = (MintDescriptionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MintDescriptionTableViewCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        cell.tag = 9999;
        
        for (id asubview in [cell.DescriptionContainerView subviews])
        {
            [asubview removeFromSuperview];
        }
        
        [cell.DescriptionContainerView addSubview:[self.PinterestDataSource addMintDescriptionTextViewforRow:indexPath.row]];
        
        //- (UILabel *)addMintCounterLabelforRow:(NSInteger)row

        UILabel *lblCount = [self.PinterestDataSource addMintCounterLabelforRow:indexPath.row];
        
        lblCount.frame = CGRectMake(CGRectGetWidth(cell.DescriptionContainerView.frame)-40, CGRectGetHeight(cell.DescriptionContainerView.frame)-21, 100, 20);
        
        lblCount.textColor = [UIColor grayColor];
        
        [cell.DescriptionContainerView addSubview:lblCount];
        
        
        SPHSingletonClass *sharedClass = [SPHSingletonClass sharedSingletonClass];
        cell.lblLocation.text = sharedClass.selectedLocation.yaacPlaceName;
        cell.lblCategory.text = sharedClass.categoryname;
        
        
        if (_isFromMintDetail)
        {
            cell.btnCategory.userInteractionEnabled = NO;
            cell.btnLocation.userInteractionEnabled = NO;
        }

        
        
        cell.btnFacebook.tag = indexPath.section;
        cell.btnTwitter.tag = indexPath.section;

        cell.lblFacebook.textColor = (sharedClass.isFacebookSelected)?ORANGECOLOR:[UIColor colorWithRed:57.0/255 green:57.0/255 blue:57.0/255 alpha:1.0];
        cell.lblTwitter.textColor = (sharedClass.isTwitterSelected)?ORANGECOLOR:[UIColor colorWithRed:57.0/255 green:57.0/255 blue:57.0/255 alpha:1.0];

        [cell.btnLocation addTarget:self action:@selector(locationFinder:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnCategory addTarget:self action:@selector(caregoryFinder:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnFacebook addTarget:self action:@selector(facebookClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnTwitter addTarget:self action:@selector(checkTwitterAccountExist:) forControlEvents:UIControlEventTouchUpInside];

        return cell;
        
    }
    
    if (celltype == PinterestViewCellTypeAddMintOptions)
    {
        static NSString *CellIdentifier = @"MintSelectOptionTableViewCell";
        
        MintSelectOptionTableViewCell *cell = (MintSelectOptionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MintSelectOptionTableViewCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        cell.selectedButtonTag = [self.PinterestDataSource DiscoverTableView:self selectedRowAtIndexPath:indexPath];
        [cell setCustomDelegate:self.Pinterestdelegate];
        return cell;
        
    }
    if (celltype == PinterestViewCellTypeVideo)
    {
        static NSString *CellIdentifier = @"MintNormalVideoTableViewCell";
        MintVideoTableViewCell *cell = (MintVideoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MintVideoTableViewCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        [cell setCustomVideoDelegate:self.Pinterestdelegate];
        VideoModel *someModel = [self.PinterestDataSource VideoTableViewViewDataForRow:indexPath.row];
        if (someModel.isVideoPlaying) {
            YSVideoPlayerView *ysPlayer = [self.PinterestDataSource videoPlayerForDiscoverTableView:self ForRowAtIndexPath:indexPath];
            [ysPlayer setFrame:cell.bounds];
            [cell.contentView addSubview:ysPlayer];
            cell.frontView.alpha=1.0;

        }
        else
            cell.frontView.alpha=0.0;
        [cell setCelldataForVideoCell:someModel];
         if (!_isFromMintDetail)
             [cell addSubview:[self onAddEditMediaBtn:cell.frame]];

        return cell;
    }

       if (celltype == PinterestViewCellTypeAddMintImage)
    {
        SPHSingletonClass *sharedClass = [SPHSingletonClass sharedSingletonClass];
        static NSString *CellIdentifier = @"MintImageTableViewCell";
        MintImageTableViewCell *cell = (MintImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MintImageTableViewCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
        }
        if (_isFromMintDetail) {
            cell.mintImageView.mintImageURL = sharedClass.mintDetailImgURL;
        }
        else{
            [cell addSubview:[self onAddEditMediaBtn:cell.frame]];  // need to add
            cell.mintImageView.image = [sharedClass.selectedImage imageByScalingAndCroppingForSize:cell.mintImageView.frame.size];
        }

        return cell;
        
    }
  static NSString *noCellIdentifier = @"NoCell";
    CollectionViewTableViewCell *cell = (CollectionViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:noCellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CollectionViewTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    return cell;
}

- (void)LoadNextPagetriggered
{
    [self.Pinterestdelegate LoadNextPagetriggered];
}

//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.66;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self);
    if (shouldLoadNextPage)
    {
        [self.Pinterestdelegate LoadNextPagetriggered];
    }
}
-(void)setAllHeaderButtonNormal
{
    self.selectedButtonTag = 0;
    MintSelectOptionTableViewCell *cell = (MintSelectOptionTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell deselectAll];


}
-(void)setButtonSelectdWithTag : (NSInteger ) tagValue
{
    self.selectedButtonTag = tagValue;
    MintSelectOptionTableViewCell *cell = (MintSelectOptionTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell setButtonSelectdWithTag:tagValue];

}
// Add mint Functions
-(IBAction)addmintOptionButtonClicked:(id)sender
{
    [self.Pinterestdelegate didSelectSelectOptionButtonClicked:self forButton:[sender tag]];
}
-(IBAction)addmintPostButtonClicked:(id)sender
{
    [self.Pinterestdelegate didPostMintButtonClicked:self];
}
-(IBAction)locationFinder:(UIButton *)sender
{
    [self.Pinterestdelegate didLocationButtonClicked:self];
}

-(IBAction)caregoryFinder:(UIButton *)sender
{
   [self.Pinterestdelegate didCategoryButtonClicked:self];
}

-(void)setLocation:(NSString*)locationData withSection:(NSUInteger )sec
{
    MintDescriptionTableViewCell *cell = (MintDescriptionTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:sec]];
    if (![cell isKindOfClass:[MintDescriptionTableViewCell class]]) {
        return;
    }
    cell.lblLocation.text = locationData;
}

-(void)setCategoryText:(NSString*)categoryData withSection : (NSUInteger )sec
{
    MintDescriptionTableViewCell *cell = (MintDescriptionTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:sec]];
    if (![cell isKindOfClass:[MintDescriptionTableViewCell class]]) {
        return;
    }
    cell.lblLocation.text = categoryData;
}


-(void)alertViewShow
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"To access Twitter, go to device 'Settings', choose 'Twitter' and allow Your Show access" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert setTag:5555];
    [alert show];
    
}



#pragma  mark - Facebook Methods
- (void)facebookClicked : (UIButton *)sender
{
    SPHSingletonClass *sharedClass = [SPHSingletonClass sharedSingletonClass];
    
    if (sharedClass.isFacebookSelected) {
        MintDescriptionTableViewCell *cell = (MintDescriptionTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:sender.tag]];
        cell.lblFacebook.textColor = [UIColor colorWithRed:57.0/255 green:57.0/255 blue:57.0/255 alpha:1.0];
        sharedClass.isFacebookSelected = NO;
    }
    
    
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logInWithPublishPermissions: @[@"publish_actions"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            NSLog(@"Process error");
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
        }
        else {
            NSLog(@"Logged in");
            MintDescriptionTableViewCell *cell = (MintDescriptionTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:sender.tag]];
            cell.lblFacebook.textColor = ORANGECOLOR;
            sharedClass.isFacebookSelected = YES;
            //TODO: process error or result.
        }
    }];
}

-(void)checkTwitterAccountExist:(UIButton *)sender
{
    
    if ([SocialVideoHelper userHasAccessToTwitter]) {
        SPHSingletonClass *sharedClass = [SPHSingletonClass sharedSingletonClass];

        if (sharedClass.isTwitterSelected) {
            MintDescriptionTableViewCell *cell = (MintDescriptionTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:sender.tag]];
            cell.lblTwitter.textColor = [UIColor colorWithRed:57.0/255 green:57.0/255 blue:57.0/255 alpha:1.0];
            sharedClass.isTwitterSelected = NO;
            [sender setSelected:NO];
        }
        
        else
        {
        
        MintDescriptionTableViewCell *cell = (MintDescriptionTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:sender.tag]];
        cell.lblTwitter.textColor = ORANGECOLOR;
        sharedClass.isTwitterSelected = YES;
            [sender setSelected:YES];

        }
        
    }
    else
        
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Twitter" message:twitterError delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    
    


}
#pragma Mark---
#pragma Mark -  Video/Image/Youtube video Edit and cancel Mechanism
 -(UIView *)onAddEditMediaBtn:(CGRect)Cellframe
{
    _viewEditMediaHolder = [[UIView alloc]initWithFrame:CGRectMake(Cellframe.size.width-40, 10, 30, 60)];
    _viewEditMediaHolder.backgroundColor = [UIColor clearColor];
//    UIButton *btnClearMedia = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnClearMedia.frame = CGRectMake(0, 0, 30, 30);
//    [btnClearMedia setImage:[UIImage imageNamed:@"Close.png"] forState:UIControlStateNormal];
//    [btnClearMedia addTarget:self action:@selector(onClearMedia) forControlEvents:UIControlEventTouchUpInside];
//    [_viewEditMediaHolder addSubview:btnClearMedia];
    UIButton *btnEditMedia = [UIButton buttonWithType:UIButtonTypeCustom];
    btnEditMedia.frame = CGRectMake(0, 0, 30, 30);
    [btnEditMedia setImage:[UIImage imageNamed:@"Edit_pencil.png"] forState:UIControlStateNormal];
    [btnEditMedia addTarget:self action:@selector(onEditMedia) forControlEvents:UIControlEventTouchUpInside];
    [_viewEditMediaHolder addSubview:btnEditMedia];
    return _viewEditMediaHolder;
}

-(void)onEditMedia
{
    [self.Pinterestdelegate onEditMediaTapped];
}

-(void)onClearMedia
{
    [self.Pinterestdelegate onClearMediaTapped];
}

-(void)showAlert :(NSString *)alertViewTag
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Twitter" message:([alertViewTag integerValue] == 1)?twitterError:twitterPermission delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
