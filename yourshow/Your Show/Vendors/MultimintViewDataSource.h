//
//  MultimintViewDataSource.h
//  Your Show
//
//  Created by Siba Prasad Hota on 12/08/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Feedmodel.h"

@class MultiMintCollectionView;

@protocol MultimintViewDataSource <NSObject>

@optional

@required

- (Feedmodel *)multiMintCollectionViewView:(MultiMintCollectionView *)collectionView DataForItemAtRow:(NSInteger)row;
- (NSInteger)NumberOFrowsForMultiMintView:(MultiMintCollectionView *)collectionView;

@end