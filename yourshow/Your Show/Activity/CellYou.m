//
//  CellYou.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 22/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "CellYou.h"
#import "UIImage+animatedGIF.h"
#import "NSString+Emojize.h"
@implementation CellYou
{

}

- (void)awakeFromNib {
    // Initialization code
     _lblActivityText.isActivityScreen = YES;
    _imgVuserImage.layer.cornerRadius = 15;
    _viewContainer.layer.cornerRadius = 5.;
    [_viewContainer setClipsToBounds:YES];
    self.layer.cornerRadius = 4;

    _lblMintText.layer.cornerRadius = 4.0;
    [_lblMintText setClipsToBounds:YES];
    
    [_imgVuserImage setClipsToBounds:YES];
    _imgVMintThumbImage.layer.cornerRadius = 2;
    
    [_imgVMintThumbImage setClipsToBounds:YES];
    
    
    _btnJoin.layer.cornerRadius = 5;
    _btnDeclined.layer.cornerRadius = 5;
    

    [self showloader];

}

-(void)showloader
{
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"page_loader" withExtension:@"gif"];
    imgLoader.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
//    [loader setFrame:CGRectMake(0, 0, 35, 35)];
    
//    [webVLoader setOpaque:NO];
//    NSString *pathForFile = [[NSBundle mainBundle] pathForResource: @"page_loader_small" ofType: @"gif"];
//    NSData *dataOfGif = [NSData dataWithContentsOfFile: pathForFile];
//    webVLoader.scalesPageToFit = YES;
//    webVLoader.contentMode = UIViewContentModeScaleAspectFit;
//    [webVLoader loadData:dataOfGif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setScreenDetails:(ActivityModel *)actModel
{
    NSURL *strImageName = ([actModel.activity_type isEqualToString:@"showroom"])?[NSURL URLWithString:actModel.activity_showroom_thumb_image]:[NSURL URLWithString:actModel.activity_thumb_image];
    
    NSString *strText = ([[strImageName absoluteString] length])?@"":([[actModel.activity_mint_desc emojizedString] length])?[NSString stringWithFormat: @": %@",[actModel.activity_mint_desc emojizedString]]: @"";
    strText = [strText stringByReplacingOccurrencesOfString:@": (null)" withString:@""];
    NSString *strActText;
    if([actModel.activity_type isEqualToString:@"follow"])
    {
        strActText = [NSString stringWithFormat:@"%@ %@",actModel.activity_text,actModel.activity_time];
    }
    else
        strActText = [self getActivityString:[NSString stringWithFormat:@"%@%@",actModel.activity_text,([strText length])?strText:@""] date:(![strText length])?actModel.activity_time:@"" width:_lblActivityText.frame.size.width];
    
    [_lblTime setHidden:YES];
    _lblMintText.hidden = ([[strImageName absoluteString] length])?YES:NO;
    
    CGFloat strHeight = [self getStringHeight:strActText width:_lblActivityText.frame.size.width];
    CGRect rectLbl = _lblActivityText.frame;
    
    
    if (![[strImageName absoluteString] length])
    {
        CGFloat strHeight   =[self getStringHeight:[NSString stringWithFormat:@"%@%@",actModel.activity_text,([strText length])?strText:@""] width:214];
        strHeight = (strHeight<40)?strHeight:45;
        rectLbl.origin.y = 2;
        CGRect rectCell = _lblTime.frame;
        rectCell.origin.y = strHeight-2;
        [_lblTime setFrame:rectCell];
        [_lblTime setHidden:NO];
        [_lblTime setText:actModel.activity_time];
    }
    else
    {
        rectLbl.origin.y = (strHeight>28)?2:10;
        _lblActivityText.strDateTime = actModel.activity_time;
    }
    
    rectLbl.size.height = strHeight+3;
    
    if([actModel.activity_type isEqualToString:@"follow"])
    {
        _lblActivityText.strDateTime = actModel.activity_time;
        rectLbl.origin.y = (50-strHeight)/2;
        [_lblTime setHidden:YES];
        [_lblMintText setHidden:YES];
    }
    
    if([actModel.activity isEqualToString:@"mentioned"])
    {
        _lblActivityText.strDateTime = actModel.activity_time;
        rectLbl.origin.y = (50-strHeight)/2;
        [_lblTime setHidden:YES];
    }
    
    
    if ([actModel.activity isEqualToString:@"mint_milestone"])
    {
        _imgVMintThumbImage.image = [UIImage imageNamed:@"milestoneCelebration"];
        _lblMintText.hidden = YES;
        rectLbl.origin.y = (50-strHeight)/2;
        [_lblTime setHidden:YES];
        _lblActivityText.strDateTime = actModel.activity_time;
    }
    else
        _imgVMintThumbImage.mintImageURL = strImageName;
    
    [_lblActivityText setFrame:rectLbl];
    
    _imgVuserImage.mintImageURL = [NSURL URLWithString:actModel.activity_image];
    _lblActivityText.strFirstName = actModel.first_user_name;
    _lblActivityText.strSecondName = actModel.second_user_name;
    _lblActivityText.strUserName = actModel.activity_user_name;
    _lblActivityText.text = strActText;
    _lblActivityText.textView.textContainer.maximumNumberOfLines = ([strText length])?3:0;
    _lblActivityText.textView.textContainer.lineBreakMode       = NSLineBreakByTruncatingTail;
    
    _imgVMediaPlay.hidden = ([actModel.activity_isVideo isEqualToString:@"1"])?NO:YES;
}

-(CGFloat)getStringHeight:(NSString *)strText  width:(CGFloat)width
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGFloat textHeight  = [strText boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    
    return textHeight;
}

//-(NSString *)getActivityString:(NSString *)strText date:(NSString *)strDate width:(CGFloat)width
//{
//    NSString *strActText;
//    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
//    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
//    
//    CGSize stringTextSize = [strText sizeWithAttributes:@{NSFontAttributeName:font}];
//    CGSize stringDateSize = [strDate sizeWithAttributes:@{NSFontAttributeName:font1}];
//    
//    CGFloat totalWidth = stringTextSize.width + stringDateSize.width;
//    if (stringTextSize.width<width)
//    {
//        strActText = [NSString stringWithFormat:@"%@\n%@",strText,strDate];
//    }
//    else
//    {
//        if(totalWidth>((width*2)-20)) // More than one line
//            strActText = [NSString stringWithFormat:@"%@\n%@",strText,strDate];
//        else
//            strActText = [NSString stringWithFormat:@"%@ %@",strText,strDate];
//    }
//    
//    
//    return strActText;
//}

-(NSString *)getActivityString:(NSString *)strText date:(NSString *)strDate width:(CGFloat)width
{
    NSString *strActText;
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    
    CGSize stringTextSize = [strText sizeWithAttributes:@{NSFontAttributeName:font}];
    CGSize stringDateSize = [strDate sizeWithAttributes:@{NSFontAttributeName:font1}];
    
    CGFloat totalWidth = stringTextSize.width + stringDateSize.width;
    if (stringTextSize.width<width)
    {
        strActText = [NSString stringWithFormat:@"%@\n%@",strText,strDate];
    }
    else
    {
        if(totalWidth>((width*2)-20)) // More than one line
            strActText = [NSString stringWithFormat:@"%@ %@",strText,strDate];
        else
            strActText = [NSString stringWithFormat:@"%@ %@",strText,strDate];
    }
    
    return strActText;
}

@end
