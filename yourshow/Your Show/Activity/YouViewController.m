//
//  YouViewController.m
//  ICViewPager
//
//  Created by JITENDRA KUMAR PRADHAN on 29/02/16.
//  Copyright © 2016 Ilter Cengiz. All rights reserved.
//

#import "YouViewController.h"
#import "ActivityModel.h"
#import "CellYou.h"
#import "UserAccessSession.h"
#import "MintShowroomViewController.h"
#import "MSProfileVC.h"
#import "MSMintDetails.h"
#import "CarbonKit.h"
#import "NSString+Emojize.h"

@interface YouViewController ()<UITableViewDataSource,UITableViewDelegate,MSMintDetailsDelegate>
{
    int pagenumber;
    BOOL isDataFinishedInServer;
    NSMutableArray *arrYouActivityList;
    IBOutlet UITableView *tblYouActivity;
    BOOL isAPICalling;
    CarbonSwipeRefresh *refreshCarbon;

    BOOL isDataFinisedFromServer;
}
@end

@implementation YouViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"You VC Called");
    isAPICalling = NO;
    isDataFinisedFromServer = NO;
    tblYouActivity.separatorStyle = UITableViewCellSeparatorStyleNone;
    arrYouActivityList = [[NSMutableArray alloc]init];
    pagenumber = 1;
    [self createRefreshView];
    arrYouActivityList = [UserAccessSession getAllActivityYouList];
//
    [self getYouActivityList:pagenumber];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(autoRefreshScreen:) name:@"AutoRefreshYouTab" object:nil];
    
    // Changes for iPad done by mani
    if (isSmallPad) {
        CGRect rctTransparent = tblYouActivity.frame;
        rctTransparent.size.height = self.view.frame.size.height-100;
        [tblYouActivity setFrame:rctTransparent];
    }
    
}
-(void)viewDidDisappear:(BOOL)animated
{

  

}
-(void)autoRefreshScreen :(NSNotification *)notify
{
    pagenumber = 1;
    [self getYouActivityList:pagenumber];
}

-(void)getYouActivityList:(int)pageNo
{
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pagenumber] forKey:@"page_number"];
    [dicts setValue:@"2" forKey:@"type"];
    
    [YSAPICalls getActivitySegmentInfo:dicts methodName:METHOD_GETACTIVITYYOU  SuccessionBlock:^(id newResponse) {
        Generalmodel *gModel = newResponse;
        if ([gModel.status_code integerValue])
        {
            isAPICalling = NO;

            if(pagenumber==1)
                [arrYouActivityList removeAllObjects];
            
            if([gModel.tempArray2 count]>0)
            {
                isDataFinisedFromServer = NO;
                [arrYouActivityList addObjectsFromArray:gModel.tempArray2];

                if(pagenumber==1)
                    [UserAccessSession InsertActivityYouDataBase:arrYouActivityList];
            }
            else
            {
                isDataFinisedFromServer = YES;
            }
            [tblYouActivity reloadData];
        }
    }andFailureBlock:^(NSError *error)
     {
         isAPICalling = NO;
     }];
}

-(CGFloat)getStringHeight:(NSString *)strText  width:(CGFloat)width
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGFloat textHeight  = [strText boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    
    return textHeight;
}

#pragma mark - UITableView delegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![YSSupport isNetworkRechable]) {
        return ([arrYouActivityList count])?[arrYouActivityList count]:0;
    }
    
    if ([arrYouActivityList count]>=8) {
        return ([arrYouActivityList count])?((isDataFinisedFromServer)?[arrYouActivityList count]:[arrYouActivityList count]+1):0;
    }
    
    return ([arrYouActivityList count])?((isDataFinisedFromServer)?[arrYouActivityList count]:[arrYouActivityList count]):0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if((indexPath.row>=(arrYouActivityList.count)))
    {
        return 50;
    }
    
    ActivityModel *actModel        = [arrYouActivityList objectAtIndex:indexPath.row];
    NSURL *strImageName = ([actModel.activity_type isEqualToString:@"showroom"])?[NSURL URLWithString:actModel.activity_showroom_thumb_image]:[NSURL URLWithString:actModel.activity_thumb_image];
    
    if([actModel.activity_type isEqualToString:@"follow"])
    {
        return 56;
    }
    
    if (![[strImageName absoluteString] length])
    {
        NSString *strText = ([[strImageName absoluteString] length])?@"":[NSString stringWithFormat:@": %@",[actModel.activity_mint_desc emojizedString]];
        CGFloat strHeight   =[self getStringHeight:[NSString stringWithFormat:@"%@%@",actModel.activity_text,([strText length])?strText:@""] width:214];
        strHeight = (strHeight<40)?strHeight:45;
        return  ((strHeight+23)<56)?56:(strHeight+23);
    }
    
    if([actModel.activity_type isEqualToString:@"showroom"] && ![actModel.activity isEqualToString:@"joined_showroom"])
    {
        ActivityModel *actModel   = [arrYouActivityList objectAtIndex:indexPath.row];
        CGFloat strHeight   =[self getStringHeight:[NSString stringWithFormat:@"%@ %@",actModel.activity_text,actModel.activity_time] width:214];
        int yPos = (strHeight<15)?15:(strHeight>28)?2:10;
        return  yPos+strHeight+46;
    }
    
    return 56;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellYou";
    CellYou  *cell = (CellYou *)[tblYouActivity dequeueReusableCellWithIdentifier:cellIdentifier];
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    tblYouActivity.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if((indexPath.row>=(arrYouActivityList.count)))
    {
        [cell.lblTime   setHidden:YES];              // Need to add
        [cell.lblMintText   setHidden:YES];              // Need to add
        [cell.imgVuserImage setHidden:YES];       // Need to add
        [cell.viewContainer setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:cell.viewNoMoreMints];
        return cell;
    }
    
    ActivityModel *actModel        = [arrYouActivityList objectAtIndex:indexPath.row];
    [cell setScreenDetails:actModel];
    cell.btnUserThumb.tag = indexPath.row;
    cell.btnMintThumb.tag = indexPath.row;
    cell.lblActivityText.tag = indexPath.row;

    __weak STTweetLabel *lblActivityTemp = cell.lblActivityText;
    cell.lblActivityText.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
    {
        if (hotWord == STTweetStartag)
        {
            [self gotoShowroomViewWithTag:(int)lblActivityTemp.tag showTag:string];
        }
        else if(hotWord == TweetActivityUserName)
        {
            [self gotoProfileUserWithTag:(int)lblActivityTemp.tag userName:string];
        }
    };
    
    [cell.btnUserThumb addTarget:self action:@selector(onGotoProfilePage:) forControlEvents:UIControlEventTouchUpInside];

    if([actModel.activity isEqualToString:@"mint_milestone"])
    {
        [cell.btnMintThumb addTarget:self action:@selector(showCelebrationPopUp:) forControlEvents:UIControlEventTouchUpInside];
    }
    
   else if([actModel.activity_type isEqualToString:@"showroom"])
    {
        [cell.btnMintThumb addTarget:self action:@selector(gotoShowroomDetailsPage:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if([actModel.activity_type isEqualToString:@"mint"])
    {
        [cell.btnMintThumb addTarget:self action:@selector(gotoMintDetailsScreen:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self setJoinAndUnJoinComponents:cell model:actModel index:indexPath];
    return cell;
}


-(void)showCelebrationPopUp:(UIButton *)sender
{
    [self showPopUp:(int)sender.tag];
    
}
-(void)showPopUp:(int)tag
{
    ActivityModel *actModel = [arrYouActivityList objectAtIndex:tag];
    
    if([actModel.activity isEqualToString:@"mint_milestone"])
    {
//        [AppDelegate createCelebrationPopup:actModel.mint_count_gamification withUsername:[NSString stringWithFormat:@"%@ %@",GETVALUE(CEO_UserFirstName),GETVALUE(CEO_UserLastName)] userimageUrl:actModel.activity_image];
        
        AppDelegate *obj = MINTSHOWAPPDELEGATE;
        [obj createCelebrationPopup:actModel.mint_count_gamification withUsername:[NSString stringWithFormat:@"%@ %@",GETVALUE(CEO_UserFirstName),GETVALUE(CEO_UserLastName)] userimageUrl:actModel.activity_image];
        
    }
}

-(void)setJoinAndUnJoinComponents:(CellYou *)cell model:(ActivityModel *)actModel index:(NSIndexPath *)indexPath
{
    if([actModel.activity isEqualToString:@"you_joined_showroom"])
    {
        [cell.btnJoin setTitle:@"JOINED" forState:UIControlStateNormal];
        [cell.btnJoin setBackgroundColor:[UIColor clearColor]];
        [cell.btnJoin setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
        cell.btnJoin.layer.borderColor = ORANGECOLOR.CGColor;
        cell.btnJoin.layer.borderWidth = 2.0;
        cell.btnJoin.tag = indexPath.row;
        [cell.btnJoin addTarget:self action:@selector(onJoinOrUnjoinShowRoom:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if([actModel.activity isEqualToString:@"shared_join_showroom"])
    {
        [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnJoin setTitle:@"JOIN" forState:UIControlStateNormal];
        [cell.btnJoin setBackgroundColor:ORANGECOLOR];
        cell.btnJoin.tag = indexPath.row;
        
        if([actModel.is_userjoined isEqualToString:@"yes"])
        {
            [cell.btnJoin setTitle:@"JOINED" forState:UIControlStateNormal];
            [cell.btnJoin setBackgroundColor:[UIColor clearColor]];
            [cell.btnJoin setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
            cell.btnJoin.layer.borderColor = ORANGECOLOR.CGColor;
            cell.btnJoin.layer.borderWidth = 2.0;
        }
        else
        {
            [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cell.btnJoin setTitle:@"JOIN" forState:UIControlStateNormal];
            [cell.btnJoin setBackgroundColor:ORANGECOLOR];
        }
        
        cell.btnJoin.tag = indexPath.row;
        [cell.btnJoin addTarget:self action:@selector(onJoinOrUnjoinShowRoom:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if([actModel.activity isEqualToString:@"request_follow_showroom"])
    {
        [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnJoin setTitle:@"ACCEPT" forState:UIControlStateNormal];
        [cell.btnJoin setBackgroundColor:ORANGECOLOR];
        
        [cell.btnDeclined setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnDeclined setTitle:@"DECLINE" forState:UIControlStateNormal];
        [cell.btnDeclined setBackgroundColor:ORANGECOLOR];
        
        cell.btnJoin.tag = indexPath.row;
        [cell.btnJoin addTarget:self action:@selector(onAcceptShowRoom:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnDeclined.tag = indexPath.row;
        [cell.btnDeclined addTarget:self action:@selector(onAcceptShowRoom:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [cell.btnActvityFollower setHidden:YES];

    if([actModel.activity_type isEqualToString:@"showroom"] && ![actModel.activity isEqualToString:@"joined_showroom"])
    {
        CGFloat strHeight = [self getStringHeight:cell.lblActivityText.text width:cell.lblActivityText.frame.size.width];
        CGRect rectDecbtn = cell.btnDeclined.frame;
        rectDecbtn.origin.y = cell.btnJoin.frame.origin.y;
        [cell.btnDeclined setFrame:rectDecbtn];

        CGRect rectCell = cell.viewContainer.frame;
        rectCell.size.height = [self getStringHeight:cell.lblActivityText.text width:cell.lblActivityText.frame.size.width]+((strHeight>28)?28:45)	;
        [cell.viewContainer setFrame:rectCell];
        CGRect rectbtn = cell.btnJoin.frame;
        rectbtn.origin.y = cell.viewContainer.frame.size.height-cell.btnJoin.frame.size.height-5;
        [cell.btnJoin setFrame:rectbtn];
    }
    else if([actModel.activity_type isEqualToString:@"follow"])
    {
        CGRect rectCell = cell.viewContainer.frame;
        rectCell.size.height = 50;
        [cell.viewContainer setFrame:rectCell];
        [cell.btnActvityFollower setHidden:NO];
        [cell.btnActvityFollower setImage:[UIImage imageNamed:([actModel.is_follow_status isEqualToString:@"1"])?@"Btn_ActivityFollowing_1x.png":@"Btn_ActivityFollow_1x.png"] forState:UIControlStateNormal];
        [cell.btnActvityFollower addTarget:self action:@selector(onFollowUnfollowUser:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnActvityFollower.tag = indexPath.row;
    }

    else
    {
        NSURL *strImageName = ([actModel.activity_type isEqualToString:@"showroom"])?[NSURL URLWithString:actModel.activity_showroom_thumb_image]:[NSURL URLWithString:actModel.activity_thumb_image];
        CGRect rectCell = cell.viewContainer.frame;
        
        if (![[strImageName absoluteString] length])
        {
            NSString *strText = ([[strImageName absoluteString] length])?@"":[NSString stringWithFormat:@": %@",[actModel.activity_mint_desc emojizedString]];
            CGFloat strHeight   =[self getStringHeight:[NSString stringWithFormat:@"%@%@",actModel.activity_text,([strText length])?strText:@""] width:214];
            strHeight = (strHeight<40)?strHeight:45;
            rectCell.size.height = (strHeight+18<50)?50:strHeight+18;
        }
        else
            rectCell.size.height = 50;
        [cell.viewContainer setFrame:rectCell];
    }
}

-(void)onFollowUnfollowUser:(UIButton *)sender
{
    ActivityModel *actModel        = [arrYouActivityList objectAtIndex:sender.tag];
    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&user_id=%@",GETVALUE(CEO_AccessToken),actModel.activity_user_id] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@follow/follow",ServerUrl]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:url];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"My Url %@",url);
    NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"status"] isEqualToString:@"true"]) {
             if ([[showroomDict valueForKey:@"success_message"] isEqualToString:@"followed"])
             {
                 [sender setImage:[UIImage imageNamed:@"Btn_ActivityFollowing_1x.png"] forState:UIControlStateNormal];
                 actModel.is_follow_status = @"1";
             }
             else
             {
                 [sender setImage:[UIImage imageNamed:@"Btn_ActivityFollow_1x.png"] forState:UIControlStateNormal];
                 actModel.is_follow_status = @"0";
             }
             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
             
             [tblYouActivity beginUpdates];
             [tblYouActivity reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             [tblYouActivity endUpdates];
         }
     }];
}

-(void)onJoinOrUnjoinShowRoom:(UIButton *)sender
{
    ActivityModel *actModel        = [arrYouActivityList objectAtIndex:sender.tag];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&inviter_id=0&showroom_tag=%@",GETVALUE(CEO_AccessToken),actModel.activity_showroom_tag] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_JOINSHOWROOM]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitVal = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submitData val %@",submitVal);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"StatusMsg"]);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
             aDelegate.isAnyShowroomJoinUnjoin = YES;
             
             NSLog(@"Joiend properly");
             if ([[showroomDict valueForKey:@"follow_message"] isEqualToString:@"true"])
             {
                 actModel.activity = @"you_joined_showroom";
                 actModel.activity_text = [NSString stringWithFormat:@"You have joined successfully in this showroom, *%@",actModel.activity_showroom_tag];
                 actModel.is_userjoined = @"yes";
             }
             else
             {
                 actModel.activity_text = [NSString stringWithFormat:@"You have unjoin in this showroom, *%@",actModel.activity_showroom_tag];
                 actModel.activity = @"shared_join_showroom";
                 actModel.is_userjoined = @"no";
             }
             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
             [tblYouActivity beginUpdates];
             [tblYouActivity reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             [tblYouActivity endUpdates];
         }
         else
         {
             
         }
     }];
}

-(void)onAcceptShowRoom:(UIButton *)sender
{
    [self callApiForShowroomOrCollaborater:sender requestType:@"0"];
}

-(void)callApiForShowroomOrCollaborater:(UIButton *)sender requestType:(NSString *)reqType
{
    ActivityModel *actModel        = [arrYouActivityList objectAtIndex:sender.tag];
    NSString *strAcceptType;
    if ([sender.titleLabel.text isEqualToString:@"ACCEPT"])
    {
        strAcceptType = @"0";
    }
    else
    {
        strAcceptType = @"1";
    }
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&requester_id=%@&showroom_tag=%@&request_type=%@&action_type=%@",GETVALUE(CEO_AccessToken),actModel.activity_user_id,actModel.activity_showroom_tag,reqType,strAcceptType] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submit data %@",submitDta123);
    
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@customshowroom/request_accept_decline",ServerUrl]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitVal = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submitData val %@",submitVal);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"StatusMsg"]);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             NSLog(@"Joiend properly");
             if ([[showroomDict valueForKey:@"Status"] isEqualToString:@"Success"])
             {
                 if([[showroomDict valueForKey:@"StatusMsg"] isEqualToString:@"Declined Successfully"])
                 {
                     [arrYouActivityList removeObjectAtIndex:sender.tag];
                     [tblYouActivity reloadData];
                 }
                 else
                 {
                     actModel.activity = @"you_joined_showroom";
                     actModel.activity_text = [NSString stringWithFormat:@"You have joined successfully in this showroom, *%@",actModel.activity_showroom_tag];
                     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
                     [tblYouActivity beginUpdates];
                     [tblYouActivity reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                     [tblYouActivity endUpdates];
                 }
             }
             else
             {
             }
         }
         else
         {
             
         }
     }];
}

-(void)gotoShowroomDetailsPage: (UIButton *)btnShowroom
{
    ActivityModel *actModel        = [arrYouActivityList objectAtIndex:[btnShowroom tag]];
    [self gotoShowroomViewWithTag:(int)[btnShowroom tag] showTag:[NSString stringWithFormat:@"*%@",actModel.activity_showroom_tag]];
}

-(void)gotoMintDetailsScreen: (UIButton *)btnMint {
    
    ActivityModel *actModel        = [arrYouActivityList objectAtIndex:[btnMint tag]];

    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mint_id = actModel.feed_id;
    mintDetailsVC.delegate = self;
    mintDetailsVC.isToshowSingleRecord = YES;
    mintDetailsVC.isFromActivity = YES;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}

-(void)onGotoProfilePage:(UIButton *)btnTag
{
    ActivityModel *actModel        = [arrYouActivityList objectAtIndex:[btnTag tag]];
    if ([actModel.activity_user_id length])
    {
        [self gotoProfileView:actModel.activity_user_id];
    }
    else
    {
        [self gotoProfileView:actModel.first_user_id];
    }
}

-(void)gotoShowroomViewWithTag:(int)tag showTag:(NSString *)strShowroomName
{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.lbl.hidden   = YES;
    
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [strShowroomName stringByReplacingCharactersInRange:NSMakeRange(0, 1)  withString:@""];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}

-(void)gotoProfileUserWithTag:(int)tag userName:(NSString *)strName
{
    NSString *strSeletedUserId;

    if ([strName isEqualToString:@"Your's"] || [strName isEqualToString:@"Your"] || [strName isEqualToString:@"You"])
    {
        strSeletedUserId = GETVALUE(CEO_UserId);
    }
    else
    {
        ActivityModel *actModel        = [arrYouActivityList objectAtIndex:tag];
        if ([actModel.activity_user_name length]) {
            if([actModel.activity_user_name isEqualToString:strName])
            {
                strSeletedUserId = actModel.activity_user_id;
            }
        }
        else
        {
            if([actModel.first_user_name isEqualToString:strName])
            {
                strSeletedUserId = actModel.first_user_id;
            }
            else if([actModel.second_user_name isEqualToString:strName])
            {
                strSeletedUserId = actModel.second_user_id;
            }
        }
    }
    [self gotoProfileView:strSeletedUserId];
}

-(void)gotoProfileView:(NSString *)selectedEnitity{
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    // prof.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:prof animated:YES];
}

-(void)setActivityLabelText:(CellYou *)cell andModel:(ActivityModel *)actModel
{
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showPopUp:(int)indexPath.row];
}





#pragma mark - UIScrollViewDelegate
//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    NSLog(@"%d",(yOffset / height > kNewPageLoadScrollPercentageThreshold));
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    
    if (isAPICalling || ![YSSupport isNetworkRechable]) {
        return;
    }
    
    if (isDataFinisedFromServer) {
        return;
    }
    
    BOOL shouldLoadNextPage = ShouldLoadNextPage(tblYouActivity);
    if (shouldLoadNextPage)
    {
        pagenumber = pagenumber +1;
        NSLog(@"*** Scroll End ****");
        isAPICalling = YES;
        [self getYouActivityList:pagenumber];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createRefreshView
{
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:tblYouActivity withYPos:10];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
}

- (void)refresh:(id)sender {
    
    pagenumber = 1;
    [self getYouActivityList:pagenumber];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}
//For Mute mint, Delete Mint. FlagInappropriate
-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId
{
    int mintIndex = 0;
    BOOL isMintFoundFromList = NO;
    
    for (int i = 0; i<arrYouActivityList.count; i++) {
        
        
        if ([[[arrYouActivityList objectAtIndex:i]feed_id] isEqualToString:mintId]) {
            mintIndex = i;
            isMintFoundFromList = YES;
            break;
        }
        
    }
    
    if (isMintFoundFromList) {
        
        [arrYouActivityList removeObjectAtIndex:mintIndex];
        
        [tblYouActivity reloadData];
    }
    
    
}
//Update the tableview if the MintUser Mint exist
-(void)muteUserOfmintDetails:(NSString *)muteUser{
    
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[arrYouActivityList count]; currentIndex++){
        //do stuff with obj
        ActivityModel *feedlist = [arrYouActivityList objectAtIndex:currentIndex];
        if([muteUser isEqualToString:feedlist.activity_user_id]){
            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [arrYouActivityList removeObjectsAtIndexes:indexesToDelete];
        [tblYouActivity reloadData];
    }
    
    
}

@end
