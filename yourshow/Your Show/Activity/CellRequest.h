//
//  CellRequest.h
//  Your Show
//
//  Created by bsetec on 7/31/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTweetLabel.h"

@interface CellRequest : UITableViewCell
{
    IBOutlet UIView *viewContainer;
    IBOutlet UIImageView *imgVLoader;
    IBOutlet UIView *viewAcceptBtnHolder;
}
@property(nonatomic ,strong) IBOutlet UIView *viewNoMoreMints;
@property(nonatomic ,strong) IBOutlet UIView *viewContainer;
@property(nonatomic ,strong) IBOutlet STTweetLabel *lblActivityText;
@property(nonatomic ,strong) IBOutlet UIImageView *imgVMintThumbImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgVuserImage;
@property (strong, nonatomic) IBOutlet UIButton *btnreply;
@property (strong, nonatomic) IBOutlet UIButton *btnActvityFollower;
@property (strong, nonatomic) IBOutlet UIButton *btnShowroomThumb;
@property (strong, nonatomic) IBOutlet UIButton *btnUserThumb;
@property (strong, nonatomic) IBOutlet UIButton *btnJoin;
@property (strong, nonatomic) IBOutlet UIButton *btnDeclined;
@end
