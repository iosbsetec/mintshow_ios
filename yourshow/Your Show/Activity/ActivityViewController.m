//
//  ActivityViewController.m
//  Your Show
//
//  Created by Siba Prasad Hota on 16/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "ActivityViewController.h"


#import "MyCircleViewController.h"
#import "YouViewController.h"
#import "RequestViewController.h"

@interface ActivityViewController ()<ViewPagerDataSource, ViewPagerDelegate,YouActivityDelegate,RequestActivityDelegate,ReloadViewControllerDelegate>


{
    __weak IBOutlet UITableView *tblVActivity;
    __weak IBOutlet UIView *viewSegment;
    __weak IBOutlet UIButton *btnMyCircle;
    IBOutlet UILabel *lblYouActivityCounter;
    IBOutlet UILabel *lblRequestActivityCounter;
    
    NSInteger selectedIndex;

}
@property (nonatomic) NSUInteger numberOfTabs;

@end

@implementation ActivityViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self performSelector:@selector(loadContent) withObject:nil afterDelay:0.1];

    self.dataSource = self;
    self.delegate   = self;
   // 
    self.toplayoutVal = 48.0;
    
    lblRequestActivityCounter.layer.cornerRadius = 10.0;
    lblYouActivityCounter.layer.cornerRadius     = 10.0;
    [lblYouActivityCounter     setClipsToBounds:YES];
    [lblRequestActivityCounter setClipsToBounds:YES];
    
    lblRequestActivityCounter.hidden  = YES;
    lblYouActivityCounter.hidden      = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAllCounterInfo:) name:@"TestNotification" object:nil];
    
    selectedIndex = 0;
    
    
    AppDelegate *obj = MINTSHOWAPPDELEGATE;
    obj.refreshViewControllerDelegate = self;
    
}

- (void)viewDidAppear:(BOOL)animated {
    [self updateAllCounterInfo:nil];
    [super viewDidAppear:animated];
}
-(void)refreshActivityViewController

{
    SPHSingletonClass *singleTone     = [SPHSingletonClass sharedSingletonClass];
    selectedIndex = singleTone.glblactivitySelectedSegmentIndex;
    [self updateCounterTappingOtherPlace];
    
}
-(void)updateCounterTappingOtherPlace
{
    if (selectedIndex == 1 || selectedIndex == 2 || selectedIndex == 0) {
        SPHSingletonClass *singleTone     = [SPHSingletonClass sharedSingletonClass];
        if(selectedIndex == 1 && [lblYouActivityCounter.text integerValue])
        {
            [self activityUpdateReadStatus:2];
            lblYouActivityCounter.hidden = YES;
            singleTone.glblActivityYou_count = @"0";
            
        }
        else if(selectedIndex ==2  && [lblRequestActivityCounter.text integerValue])
        {
            [self activityUpdateReadStatus:1];
            lblRequestActivityCounter.hidden = YES;
            singleTone.glblActivityRequest_count = @"0";
        }
        
        [self updateAllCounterInfo:nil];
        singleTone.glblActivityTotal_count = [NSString stringWithFormat:@"%d",[singleTone.glblActivityYou_count intValue] + [singleTone.glblActivityRequest_count intValue]];
        AppDelegate *delegate =  MINTSHOWAPPDELEGATE
        [delegate updateCounterValue:[singleTone.glblActivityTotal_count integerValue]];
    }
    
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSLog(@"selecther tab index %d",(int)selectedIndex);
    
    if (selectedIndex == 1 || selectedIndex == 2) {
        SPHSingletonClass *singleTone     = [SPHSingletonClass sharedSingletonClass];

        
         if(selectedIndex == 1 && [lblYouActivityCounter.text integerValue])
         {
             [self activityUpdateReadStatus:2];
             lblYouActivityCounter.hidden = YES;
             singleTone.glblActivityYou_count = @"0";

         }
         else if(selectedIndex ==2  && [lblRequestActivityCounter.text integerValue])
         {
              [self activityUpdateReadStatus:1];
              lblRequestActivityCounter.hidden = YES;
              singleTone.glblActivityRequest_count = @"0";
         }

        
        singleTone.glblActivityTotal_count = [NSString stringWithFormat:@"%d",[singleTone.glblActivityYou_count intValue] + [singleTone.glblActivityRequest_count intValue]];
        AppDelegate *delegate =  MINTSHOWAPPDELEGATE
        [delegate updateCounterValue:[singleTone.glblActivityTotal_count integerValue]];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - Setters
- (void)setNumberOfTabs:(NSUInteger)numberOfTabs {
    // Set numberOfTabs
    _numberOfTabs = numberOfTabs;
    // Reload data
    [self reloadData];
}
#pragma mark - Helpers
- (void)selectTabWithNumberFive {
    [self selectTabAtIndex:1];
}
- (void)loadContent {
    self.numberOfTabs = 3;
    selectedIndex = 1;
    [self performSelector:@selector(selectTabWithNumberFive) withObject:nil];
}

#pragma mark - Interface Orientation Changes
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    // Update changes after screen rotates
    [self performSelector:@selector(setNeedsReloadOptions) withObject:nil afterDelay:duration];
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.numberOfTabs;
}
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    UIFont *fontVal = [UIFont fontWithName:@"Helvetica Neue" size:12];
    label.font = fontVal;

    if (index == 0) {
        label.text = @"MY CIRCLE";
    }
   else if (index == 1) {
        label.text = @"YOU";
    }
   else
       label.text = @"REQUESTS";

    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    label.backgroundColor = [UIColor clearColor];
    [label sizeToFit];
    return label;
}
-(void)haltForSomeSeconds{

    [self.view setUserInteractionEnabled:YES];
}
- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    [lblYouActivityCounter bringSubviewToFront:self.view];
    [lblRequestActivityCounter bringSubviewToFront:self.view];
    [self.view setUserInteractionEnabled:NO];
    [self performSelector:@selector(haltForSomeSeconds) withObject:nil afterDelay:2];
    if (index == 0) {
        MyCircleViewController *cvc = [[MyCircleViewController alloc]initWithNibName:@"MyCircleViewController" bundle:nil];
        return cvc;
    }
    else if (index == 1)
    {
        
        YouViewController *cvc = [[YouViewController alloc]initWithNibName:@"YouViewController" bundle:nil];
        cvc.aDelegate = self;
        return cvc;
        
    }
    else{
        RequestViewController *cvc = [[RequestViewController alloc]initWithNibName:@"RequestViewController" bundle:nil];
        cvc.aDelegate = self;
        return cvc;
    }


}
- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index
{
    SPHSingletonClass *singleTone = [SPHSingletonClass sharedSingletonClass];
    singleTone.glblactivitySelectedSegmentIndex = index;

    if (selectedIndex!=index && selectedIndex==1) {
        // you tab count 0
        singleTone.glblActivityTotal_count = [NSString stringWithFormat:@"%d",(int)([singleTone.glblActivityTotal_count integerValue]-[singleTone.glblActivityYou_count integerValue])];
        singleTone.glblActivityYou_count = @"0";

        [self updateAllCounterInfo:nil];

    }else  if (selectedIndex!=index && selectedIndex==2) {
        // Req tab count 0
        singleTone.glblActivityTotal_count = [NSString stringWithFormat:@"%d",(int)([singleTone.glblActivityTotal_count integerValue]-[singleTone.glblActivityRequest_count integerValue])];
        singleTone.glblActivityRequest_count = @"0";
        [self updateAllCounterInfo:nil];
    }
    
    selectedIndex = index;
    if (index != 0) {
        [self activityUpdateReadStatus:(index == 1)?2:1];
    }
}


#pragma mark - ViewPagerDelegate
- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            return 38.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            return UIInterfaceOrientationIsLandscape(self.interfaceOrientation) ? 128.0 : CGRectGetWidth([UIScreen mainScreen].bounds)/3;
        case ViewPagerOptionFixFormerTabsPositions:
            return 0.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 0.0;
        default:
            return value;
    }
}
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return ORANGECOLOR;
        case ViewPagerTabsView:
            return [[UIColor whiteColor] colorWithAlphaComponent:0.32];
        case ViewPagerContent:
            return [[UIColor darkGrayColor] colorWithAlphaComponent:0.32];
        default:
            return color;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateAllCounterInfo:(NSNotification*)notification
{
    lblRequestActivityCounter.hidden  = NO;
    lblYouActivityCounter.hidden      = NO;
    
    
    SPHSingletonClass *singleTone     = [SPHSingletonClass sharedSingletonClass];
    lblRequestActivityCounter.text    = [NSString stringWithFormat:@"%@",singleTone.glblActivityRequest_count];
    lblYouActivityCounter.text        = [NSString stringWithFormat:@"%@",singleTone.glblActivityYou_count];
    
    NSLog(@"counter you %@ counter request %@",singleTone.glblActivityYou_count,singleTone.glblActivityRequest_count);

    if ([notification.name isEqualToString:@"TestNotification"])
    {
        NSDictionary* userInfo = notification.userInfo;
        NSNumber* notiType = (NSNumber*)userInfo[@"activity_type"];
        NSLog (@"Successfully received test notification! %i", notiType.intValue);
        
        switch (notiType.intValue) {
            case 1:
                if (selectedIndex == 1)
                [[NSNotificationCenter defaultCenter]postNotificationName:@"AutoRefreshYouTab" object:nil];
                break;
                
            case 2:
                if (selectedIndex == 2)
                [[NSNotificationCenter defaultCenter]postNotificationName:@"AutoRefreshRequesTab" object:nil];
                break;
                
            default:
                break;
        }
        
    }
    
    if (![singleTone.glblActivityRequest_count integerValue]) {
        lblRequestActivityCounter.hidden = YES;
    }
    if (![singleTone.glblActivityYou_count integerValue]) {
        lblYouActivityCounter.hidden = YES;
    }
 
    AppDelegate *delegate =  MINTSHOWAPPDELEGATE
    [delegate updateCounterValue:[singleTone.glblActivityTotal_count integerValue]];
}
//POSTActivity YOU/REQUEST notification read status update/newsfeed/activity_update_read_status

-(void)activityUpdateReadStatus:(NSInteger)activityType
{
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&type=%ld",GETVALUE(CEO_AccessToken),(long)activityType] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@newsfeed/activity_update_read_status",ServerUrl]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:url];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"My Url %@",url);
    NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"StatusCode"] integerValue]) {

         }
     }];
}

@end
