//
//  CommentModel.m
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 22/09/14.
//
//

#import "ActivityModel.h"

@implementation ActivityModel


-(ActivityModel *)initWithActivity : (NSDictionary *) activityInfo
{
    self.activity                   = [activityInfo valueForKey:@"activity_type"];
    self.activity_type              = [activityInfo valueForKey:@"type"];
    self.activity_time              = [activityInfo valueForKey:@"activity_time"];
    self.activity_text              = [activityInfo valueForKey:@"activity_text"];
    self.activity_image             = [activityInfo valueForKey:@"activity_image"];
    self.activity_total_pages       = [activityInfo valueForKey:@"total_pages"];
    self.is_seen                    = [[activityInfo valueForKey:@"is_seen"] integerValue];
    
    NSDictionary *dataDict = [activityInfo   valueForKey:@"data"];

    if ([dataDict count])
    {
        if (![dataDict valueForKey:@"user_name"])
        {
            self.first_user_name            = [dataDict valueForKey:@"first_user_name"];
            self.second_user_name           = [dataDict valueForKey:@"second_user_name"];
            self.first_user_id              = [dataDict valueForKey:@"first_user_id"];
            self.second_user_id             = [dataDict valueForKey:@"second_user_id"];
        }
        else
        {
            self.activity_user_id           = [dataDict valueForKey:@"user_id"];
            self.activity_user_name         = [dataDict valueForKey:@"user_name"];
        }
        //User action on some more mints
        self.is_follow_status               = [dataDict valueForKey:@"is_follow_status"];
        self.activity_isVideo               = [dataDict valueForKey:@"is_video"];
        self.activity_thumb_image           = [dataDict valueForKey:@"feed_thumb_image"];
        self.activity_showroom_id           = [dataDict valueForKey:@"showroom_id"];
        self.activity_showroom_name         = [dataDict valueForKey:@"showroom_name"];
        self.activity_showroom_tag          = [dataDict valueForKey:@"showroom_tag"];
        self.activity_showroom_thumb_image  = [dataDict valueForKey:@"showroom_thumb_image"];
        self.is_userjoined                  = [dataDict valueForKey:@"is_user_joined"];
        self.activity_mint_desc             = [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"feed_text"]];
        self.feed_id                        = [dataDict valueForKey:@"feed_id"];
        self.mint_count_gamification        = [dataDict valueForKey:@"mint_count"];
    }

    
    return  self;
}

-(ActivityModel *)initWithMyCircleActivity : (NSDictionary *) activityInfo
{
    self.activity                   = [activityInfo valueForKey:@"activity_type"];
    self.activity_type              = [activityInfo valueForKey:@"type"];
    self.activity_time              = [activityInfo valueForKey:@"activity_time"];
    self.activity_text              = [activityInfo valueForKey:@"activity_text"];
    self.activity_image             = [activityInfo valueForKey:@"activity_image"];
    self.activity_total_pages       = [activityInfo valueForKey:@"total_pages"];;
    
    
    NSDictionary *dataDict = [activityInfo   valueForKey:@"data"];

    
    if ([dataDict count])
    {
        self.activity_user_id                = [dataDict valueForKey:@"user_id"];
        self.activity_user_name              = [dataDict valueForKey:@"user_name"];
        
        if ([dataDict valueForKey:@"details"])
        {
            _arrFeedDetails = [[NSMutableArray alloc]init];
            NSArray *arrDetails = [dataDict valueForKey:@"details"];
            if (arrDetails.count)
            {
                for (NSDictionary *dict in arrDetails) {
                    [_arrFeedDetails addObject:dict];
                }
            }
            self.mint_count             = [dataDict valueForKey:@"mints_count"];
        }
        
        if ([dataDict valueForKey:@"mintlist"])
        {
            _arrMintListDetails = [[NSMutableArray alloc]init];
            NSArray *arrDetails = [dataDict valueForKey:@"mintlist"];
            if (arrDetails.count)
            {
                for (NSDictionary *dict in arrDetails) {
                    [_arrMintListDetails addObject:dict];
                }
            }
        }
        
        if ([dataDict valueForKey:@"showroomlist"])
        {
            if ([[dataDict valueForKey:@"showroomlist"] isKindOfClass:[NSArray class]]) {
                
                _arrShowroomDetails = [[NSMutableArray alloc]init];
                NSArray *arrDetails = [dataDict valueForKey:@"showroomlist"];
                if (arrDetails.count)
                {
                    for (NSDictionary *dict in arrDetails)
                    {
                        [_arrShowroomDetails addObject:dict];
                    }
                }
            }
        }
        
        if ([dataDict valueForKey:@"users_list"])
        {
            if ([[dataDict valueForKey:@"users_list"] isKindOfClass:[NSArray class]])
            {
                _arrFollowerDetails = [[NSMutableArray alloc]init];
                NSArray *arrDetails = [dataDict valueForKey:@"users_list"];
                if (arrDetails.count)
                {
                    for (NSDictionary *dict in arrDetails)
                    {
                        [_arrFollowerDetails addObject:dict];
                    }
                }
            }
            
            @try {
                if ([_arrFollowerDetails count]>1)
                {
                    self.first_user_name                = [[_arrFollowerDetails objectAtIndex:0] valueForKey:@"user_name"];
                    self.second_user_name               = [[_arrFollowerDetails objectAtIndex:1] valueForKey:@"user_name"];
                    self.first_user_id                  = [[_arrFollowerDetails objectAtIndex:0] valueForKey:@"user_id"];
                    self.second_user_id                 = [[_arrFollowerDetails objectAtIndex:1] valueForKey:@"user_id"];
                }
                else if ([_arrFollowerDetails count]==1)
                {
                    self.first_user_name                = [[_arrFollowerDetails objectAtIndex:0] valueForKey:@"user_name"];
                    self.first_user_id                  = [[_arrFollowerDetails objectAtIndex:0] valueForKey:@"user_id"];
                }
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
        }
        
        if((![dataDict valueForKey:@"mintlist"]) &&  (![dataDict valueForKey:@"showroomlist"]))
        {
            if (![dataDict valueForKey:@"user_name"])
            {
                self.first_user_name                = [dataDict valueForKey:@"first_user_name"];
                self.second_user_name               = [dataDict valueForKey:@"second_user_name"];
                self.first_user_id                  = [dataDict valueForKey:@"first_user_id"];
                self.second_user_id                 = [dataDict valueForKey:@"second_user_id"];
            }
            
            if ([self.activity_type isEqualToString:@"showroom"])
            {
                self.activity_showroom_id           = [dataDict valueForKey:@"showroom_id"];
                self.activity_showroom_name         = [dataDict valueForKey:@"showroom_name"];
                self.activity_showroom_tag          = [dataDict valueForKey:@"showroom_tag"];
                self.activity_showroom_thumb_image  = [dataDict valueForKey:@"showroom_thumb_image"];
            }
            self.activity_thumb_image               = [dataDict valueForKey:@"feed_thumb_image"];
            self.is_follow_status                   = [dataDict valueForKey:@"is_follow_status"];
            self.feed_id                            = [dataDict valueForKey:@"feed_id"];
            self.activity_isVideo                   = [dataDict valueForKey:@"is_video"];
            
        }
    }
    
    return  self;
}

@end
