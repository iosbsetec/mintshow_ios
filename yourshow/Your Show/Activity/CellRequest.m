//
//  CellRequest.m
//  Your Show
//
//  Created by bsetec on 7/31/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "CellRequest.h"
#import "UIImage+animatedGIF.h"

@implementation CellRequest
{
    
}


- (void)awakeFromNib {
    _lblActivityText.isActivityScreen= YES;
    _viewContainer.layer.cornerRadius = 5.;
    
    [_viewContainer setClipsToBounds:YES];
    
    _imgVuserImage.layer.cornerRadius = 15;
    
    [_imgVuserImage setClipsToBounds:YES];
    _imgVMintThumbImage.layer.cornerRadius = 2;
    
    [_imgVMintThumbImage setClipsToBounds:YES];

    _btnActvityFollower.layer.cornerRadius = 5;
    _btnJoin.layer.cornerRadius = 5;
    _btnDeclined.layer.cornerRadius = 5;
    // Initialization code
    [self showloader];

}

-(void)showloader
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"page_loader" withExtension:@"gif"];
    imgVLoader.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
