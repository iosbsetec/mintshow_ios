//
//  RequestViewController.h
//  ICViewPager
//
//  Created by JITENDRA KUMAR PRADHAN on 29/02/16.
//  Copyright © 2016 Ilter Cengiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RequestActivityDelegate <NSObject>

-(void)updateAllCounterInfo;
@end


@interface RequestViewController : UIViewController
@property (nonatomic,assign) id <RequestActivityDelegate> aDelegate;

@end
