//
//  CellYou.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 22/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTweetLabel.h"
#import "ActivityModel.h"
@interface CellYou : UITableViewCell
{
    IBOutlet UIImageView *imgLoader;
    IBOutlet UIWebView *webVLoader;

}
@property(nonatomic ,strong) IBOutlet UIView *viewNoMoreMints;

@property(nonatomic ,strong) IBOutlet UIView *viewContainer;
@property(nonatomic ,strong) IBOutlet STTweetLabel *lblActivityText;
@property(nonatomic ,strong) IBOutlet UIImageView *imgVMintThumbImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgVuserImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgVMediaPlay;
@property (strong, nonatomic) IBOutlet UIButton *btnUserThumb;
@property (strong, nonatomic) IBOutlet UIButton *btnMintThumb;
@property(nonatomic ,strong) IBOutlet UILabel *lblMintText;
@property(nonatomic ,strong) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIButton *btnActvityFollower;
@property (strong, nonatomic) IBOutlet UIButton *btnJoin;
@property (strong, nonatomic) IBOutlet UIButton *btnDeclined;

-(void)setScreenDetails:(ActivityModel *)actModel;

@end

