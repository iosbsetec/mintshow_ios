//
//  RequestViewController.m
//  ICViewPager
//
//  Created by JITENDRA KUMAR PRADHAN on 29/02/16.
//  Copyright © 2016 Ilter Cengiz. All rights reserved.
//

#import "RequestViewController.h"
#import "RequestActivityModel.h"
#import "CellRequest.h"
#import "MSProfileVC.h"
#import "MintShowroomViewController.h"
#import "CarbonKit.h"
#import "UserAccessSession.h"

@interface RequestViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    int pagenumber;
    BOOL isDataFinishedInServer;
    NSMutableArray *arrRequestActivityList;
    IBOutlet UITableView *tblRequestActivity;
    BOOL isAPICalling;
    BOOL isDataFinisedFromServer;
    CarbonSwipeRefresh *refreshCarbon;

}
@end

@implementation RequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"You VC Called");
    isAPICalling = NO;
    isDataFinisedFromServer = NO;
    tblRequestActivity.separatorStyle = UITableViewCellSeparatorStyleNone;
    arrRequestActivityList = [[NSMutableArray alloc]init];
    pagenumber = 1;
    [self createRefreshView];
    [self getRequestActivityList:pagenumber];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(autoRefreshScreen:) name:@"AutoRefreshRequesTab" object:nil];
    arrRequestActivityList = [UserAccessSession getAllActivityrequestList];
    
    // Changes for iPad done by mani
    if (isSmallPad) {
        CGRect rctTransparent = tblRequestActivity.frame;
        rctTransparent.size.height = self.view.frame.size.height-100;
        [tblRequestActivity setFrame:rctTransparent];
    }

}
-(void)autoRefreshScreen :(NSNotification *)notify
{
    pagenumber  =1;
    [self getRequestActivityList:pagenumber];

}
-(void)getRequestActivityList:(int)pageNo
{
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pagenumber] forKey:@"page_number"];
    [dicts setValue:@"1" forKey:@"type"];
    
    [YSAPICalls getActivitySegmentInfo:dicts methodName:METHOD_GETACTIVITYREQUEST  SuccessionBlock:^(id newResponse) {
        Generalmodel *gModel = newResponse;
        if ([gModel.status_code integerValue])
        {
               isAPICalling = NO;

            if(pagenumber==1)
            {
                     if ([arrRequestActivityList count])
                        [arrRequestActivityList removeAllObjects];
            }
            
            if([gModel.tempArray3 count]>0)
            {
                    isDataFinisedFromServer = NO;
                    [arrRequestActivityList addObjectsFromArray:gModel.tempArray3];
                    if(pagenumber==1)
                    [UserAccessSession InsertActivityRequestDataBase:arrRequestActivityList];
            }
            else
            {
                    isDataFinisedFromServer = YES;
            }
            [tblRequestActivity reloadData];

        }
    }andFailureBlock:^(NSError *error)
     {
                    isAPICalling = NO;
     }];
}

- (NSMutableAttributedString *)buildAgreeTextViewFromString:(NSString *)localizedString AndUserName:(NSString*)userName date:(NSString *)dateAndTime andType:(NSString *)strType showTag:(NSString *)showRoomTag
{
    
    UIFont *font  = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    NSMutableAttributedString * finalString      = [[NSMutableAttributedString alloc] initWithString:localizedString];
    
    NSDictionary *attributesUserName = @{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName:font};
    NSDictionary *attributesShowroomName = @{NSForegroundColorAttributeName:ORANGECOLOR, NSFontAttributeName:font};
    NSDictionary *attributesDateTime = @{NSForegroundColorAttributeName:[UIColor lightGrayColor], NSFontAttributeName:font1};
    
    
    if ([userName length]) {
        [finalString addAttributes:attributesUserName range:[localizedString rangeOfString:userName]];
    }
    
    if ([dateAndTime length])
    {
        [finalString addAttributes:attributesDateTime range:NSMakeRange([localizedString length]-[dateAndTime length],[dateAndTime length])];
    }
    
    NSRange rangeYours = [localizedString rangeOfString:@"Your's"];
    if (rangeYours.location != NSNotFound) {
        [finalString addAttributes:attributesUserName range:NSMakeRange(rangeYours.location,[@"Your's" length])];
    }
    
    NSRange rangeYour = [localizedString rangeOfString:@"Your"];
    if (rangeYour.location != NSNotFound) {
        [finalString addAttributes:attributesUserName range:NSMakeRange(rangeYour.location,[@"Your" length])];
    }
    
    NSRange rangeYou = [localizedString rangeOfString:@"You"];
    if (rangeYou.location != NSNotFound) {
        [finalString addAttributes:attributesUserName range:NSMakeRange(rangeYou.location,[@"You" length])];
    }
    
    if([strType isEqualToString:@"showroom"])
    {
        NSRange rangeYou = [localizedString rangeOfString:showRoomTag];
        if (rangeYou.location != NSNotFound) {
            [finalString addAttributes:attributesShowroomName range:NSMakeRange(rangeYou.location-1,[showRoomTag length]+1)];
        }
    }
    //    [finalString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange([userName length],[localizedString length]-[userName length])];
    
    return finalString;
}

#pragma mark - UITableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![YSSupport isNetworkRechable]) {
        return ([arrRequestActivityList count])?[arrRequestActivityList count]:0;
    }
    
    if ([arrRequestActivityList count]>=8) {
        return ([arrRequestActivityList count])?((isDataFinisedFromServer)?[arrRequestActivityList count]:[arrRequestActivityList count]+1):0;
    }
    
    return ([arrRequestActivityList count])?((isDataFinisedFromServer)?[arrRequestActivityList count]:[arrRequestActivityList count]):0;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if((indexPath.row>=(arrRequestActivityList.count)))
    {
        return 50;
    }
    RequestActivityModel *actModel        = [arrRequestActivityList objectAtIndex:indexPath.row];
    if([actModel.activity_type isEqualToString:@"showroom"])
    {
        RequestActivityModel *actModel   = [arrRequestActivityList objectAtIndex:indexPath.row];
        CGFloat strHeight   =[self getStringHeight:[NSString stringWithFormat:@"%@ %@",actModel.activity_text,actModel.activity_time] width:214];
        int yPos = (strHeight<15)?15:(strHeight>28)?2:10;
        return  yPos+strHeight+40;
        
        //return 86;
    }
    else if([actModel.activity_type isEqualToString:@"follow"])
    {
        return 56;
        
    }
    return 56;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"CellRequest";
    CellRequest  *cell = (CellRequest *)[tblRequestActivity dequeueReusableCellWithIdentifier:cellIdentifier];
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    tblRequestActivity.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row>=arrRequestActivityList.count)
    {
        [cell.viewContainer setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:cell.viewNoMoreMints];
        return cell;
    }
    RequestActivityModel *actModel   = [arrRequestActivityList objectAtIndex:indexPath.row];
    cell.lblActivityText.strUserName = actModel.activity_user_name;
    cell.lblActivityText.strDateTime = actModel.activity_time;
    cell.lblActivityText.text        = [NSString stringWithFormat:@"%@ %@",actModel.activity_text,actModel.activity_time];
    
    CGFloat strHeight   = [self getStringHeight:cell.lblActivityText.text width:cell.lblActivityText.frame.size.width];
    CGRect rectLbl      = cell.lblActivityText.frame;
    rectLbl.origin.y    = (strHeight<15)?15:(strHeight>28)?2:10;
    rectLbl.size.height = strHeight+5;
    [cell.lblActivityText setFrame:rectLbl];
    cell.lblActivityText.tag = indexPath.row;

    __weak STTweetLabel *lblActivityTemp = cell.lblActivityText;
    cell.lblActivityText.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
    {
        if (hotWord == STTweetStartag)
        {
            [self gotoShowroomViewWithTag:(int)lblActivityTemp.tag showTag:string];
        }
        else if(hotWord == TweetActivityUserName)
        {
            [self gotoProfileUserWithTag:(int)lblActivityTemp.tag userName:string];
        }
    };
    
    cell.imgVMintThumbImage.mintImageURL = ([actModel.activity_type isEqualToString:@"showroom"])?[NSURL URLWithString:actModel.activity_showroom_thumb_image]:[NSURL URLWithString:@""];
    [cell.imgVMintThumbImage setHidden:([actModel.activity_type isEqualToString:@"showroom"])?NO:YES];
    [cell.btnShowroomThumb   setHidden:([actModel.activity_type isEqualToString:@"showroom"])?NO:YES];
    [cell.btnShowroomThumb   addTarget:self action:@selector(onGotoShowroomDetailsPage:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnShowroomThumb.tag = indexPath.row;

    [cell.btnUserThumb addTarget:self action:@selector(gotoProfilePage:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnUserThumb.tag = indexPath.row;

    
    cell.imgVuserImage.mintImageURL = [NSURL URLWithString:actModel.activity_image];
    
    if([actModel.activity isEqualToString:@"you_joined_showroom"])
    {

        [cell.btnJoin setTitle:@"JOINED" forState:UIControlStateNormal];
        [cell.btnJoin setBackgroundColor:[UIColor clearColor]];
        [cell.btnJoin setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
        cell.btnJoin.layer.borderColor = ORANGECOLOR.CGColor;
        cell.btnJoin.layer.borderWidth = 2.0;
        cell.btnJoin.tag = indexPath.row;
        [cell.btnJoin addTarget:self action:@selector(onJoinOrUnjoinShowRoom:) forControlEvents:UIControlEventTouchUpInside];

    }
    else if([actModel.activity isEqualToString:@"shared_join_showroom"])
    {

        [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnJoin setTitle:@"JOIN" forState:UIControlStateNormal];
        [cell.btnJoin setBackgroundColor:ORANGECOLOR];
        cell.btnJoin.tag = indexPath.row;
        [cell.btnJoin addTarget:self action:@selector(onJoinOrUnjoinShowRoom:) forControlEvents:UIControlEventTouchUpInside];
        
        if([actModel.is_userjoined isEqualToString:@"yes"])
        {
            [cell.btnJoin setTitle:@"JOINED" forState:UIControlStateNormal];
            [cell.btnJoin setBackgroundColor:[UIColor clearColor]];
            [cell.btnJoin setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
            cell.btnJoin.layer.borderColor = ORANGECOLOR.CGColor;
            cell.btnJoin.layer.borderWidth = 2.0;
        }
        else
        {
            [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cell.btnJoin setTitle:@"JOIN" forState:UIControlStateNormal];
            [cell.btnJoin setBackgroundColor:ORANGECOLOR];
        }
        
        cell.btnJoin.tag = indexPath.row;
        [cell.btnJoin addTarget:self action:@selector(onJoinOrUnjoinShowRoom:) forControlEvents:UIControlEventTouchUpInside];
        
        
        

    }
    else if([actModel.activity isEqualToString:@"request_follow_showroom"])
    {
        [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnJoin setTitle:@"ACCEPT" forState:UIControlStateNormal];
        [cell.btnJoin setBackgroundColor:ORANGECOLOR];
        
        [cell.btnDeclined setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnDeclined setTitle:@"DECLINE" forState:UIControlStateNormal];
        [cell.btnDeclined setBackgroundColor:ORANGECOLOR];
        
        cell.btnJoin.tag = indexPath.row;
        [cell.btnJoin addTarget:self action:@selector(onAcceptShowRoom:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnDeclined.tag = indexPath.row;
        [cell.btnDeclined addTarget:self action:@selector(onAcceptShowRoom:) forControlEvents:UIControlEventTouchUpInside];

    }
    else if([actModel.activity isEqualToString:@"invite_collaborator"])
    {
        if([actModel.is_userjoined isEqualToString:@"yes"])
        {
            [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cell.btnJoin setTitle:@"ACCEPTED" forState:UIControlStateNormal];
            [cell.btnJoin setBackgroundColor:ORANGECOLOR];
        }
        else{

        [cell.btnJoin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnJoin setTitle:@"ACCEPT" forState:UIControlStateNormal];
        [cell.btnJoin setBackgroundColor:ORANGECOLOR];
        [cell.btnDeclined setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cell.btnDeclined setTitle:@"CANCEL" forState:UIControlStateNormal];
        [cell.btnDeclined setBackgroundColor:ORANGECOLOR];
        
        cell.btnJoin.tag = indexPath.row;
        [cell.btnJoin addTarget:self action:@selector(onAcceptCollaborator:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnDeclined.tag = indexPath.row;
        [cell.btnDeclined addTarget:self action:@selector(onAcceptCollaborator:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    
    if([actModel.activity_type isEqualToString:@"showroom"])
    {
        CGRect rectbtn = cell.btnJoin.frame;
        rectbtn.origin.y = cell.lblActivityText.frame.origin.y+cell.lblActivityText.frame.size.height;
        [cell.btnJoin setFrame:rectbtn];
        
        CGRect rectDecbtn = cell.btnDeclined.frame;
        rectDecbtn.origin.y = cell.btnJoin.frame.origin.y;
        [cell.btnDeclined setFrame:rectDecbtn];

        
        CGRect rectCell = cell.viewContainer.frame;
        
        rectCell.size.height = [self getStringHeight:cell.lblActivityText.text width:cell.lblActivityText.frame.size.width]+((rectLbl.origin.y==2)?35:45)	;
        [cell.viewContainer setFrame:rectCell];
        
        [cell.btnActvityFollower setHidden:YES];
        
    }
    else if([actModel.activity_type isEqualToString:@"follow"])
    {
        CGRect rectCell = cell.viewContainer.frame;
        rectCell.size.height = 46;
        [cell.viewContainer setFrame:rectCell];
        [cell.btnActvityFollower setHidden:NO];
        [cell.btnActvityFollower setImage:[UIImage imageNamed:([actModel.is_follow_status isEqualToString:@"1"])?@"Btn_ActivityFollowing_1x.png":@"Btn_ActivityFollow_1x.png"] forState:UIControlStateNormal];
        [cell.btnActvityFollower addTarget:self action:@selector(onFollowUnfollow:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnActvityFollower.tag = indexPath.row;
    }
  
    
    cell.tag = indexPath.row;
    
    return cell;
}
-(void)onAcceptCollaborator:(UIButton *)sender
{
    [self callApiForShowroomOrCollaborater:sender requestType:@"1"];
}

-(void)onAcceptShowRoom:(UIButton *)sender
{
    [self callApiForShowroomOrCollaborater:sender requestType:@"0"];
}

-(void)callApiForShowroomOrCollaborater:(UIButton *)sender requestType:(NSString *)reqType
{
    RequestActivityModel *actModel        = [arrRequestActivityList objectAtIndex:sender.tag];
    NSString *strAcceptType;
    if ([sender.titleLabel.text isEqualToString:@"ACCEPT"])
    {
        strAcceptType = @"0";
    }
    else
    {
        strAcceptType = @"1";
    }
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&requester_id=%@&showroom_tag=%@&request_type=%@&action_type=%@",GETVALUE(CEO_AccessToken),actModel.activity_user_id,actModel.activity_showroom_tag,reqType,strAcceptType] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submit data %@",submitDta123);
    
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@customshowroom/request_accept_decline",ServerUrl]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitVal = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submitData val %@",submitVal);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"StatusMsg"]);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             NSLog(@"Joiend properly");
             if ([[showroomDict valueForKey:@"Status"] isEqualToString:@"Success"])
             {
                 if([[showroomDict valueForKey:@"StatusMsg"] isEqualToString:@"Declined Successfully"])
                 {
                     [arrRequestActivityList removeObjectAtIndex:sender.tag];
                     [tblRequestActivity reloadData];
                 }
                 else
                 {
                     actModel.activity = @"you_joined_showroom";
                     actModel.activity_text = [NSString stringWithFormat:@"You have joined successfully in this showroom, *%@",actModel.activity_showroom_tag];
                     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
                     [tblRequestActivity beginUpdates];
                     [tblRequestActivity reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                     [tblRequestActivity endUpdates];
                 }
             }
             else
             {
             }
         }
         else
         {
             
         }
     }];
}
-(CGFloat)getStringHeight:(NSString *)strText  width:(CGFloat)width
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGFloat textHeight  = [strText boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    
    return textHeight;
}


-(void)onGotoShowroomDetailsPage: (UIButton *)btnShowroom
{
    RequestActivityModel *actModel        = [arrRequestActivityList objectAtIndex:[btnShowroom tag]];
    [self gotoShowroomViewWithTag:(int)[btnShowroom tag] showTag:[NSString stringWithFormat:@"*%@",actModel.activity_showroom_tag]];
}



-(void)gotoProfilePage:(UIButton *)btnTag
{
    RequestActivityModel *actModel        = [arrRequestActivityList objectAtIndex:[btnTag tag]];
    if ([actModel.activity_user_id length])
    {
        [self gotoProfileView:actModel.activity_user_id];
    }
}

-(void)gotoShowroomViewWithTag:(int)tag showTag:(NSString *)strShowroomName
{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.lbl.hidden   = YES;
    
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [strShowroomName stringByReplacingCharactersInRange:NSMakeRange(0, 1)  withString:@""];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}

-(void)gotoProfileUserWithTag:(int)tag userName:(NSString *)strName
{
    NSString *strSeletedUserId;
    
    if ([strName isEqualToString:@"Your's"] || [strName isEqualToString:@"Your"] || [strName isEqualToString:@"You"])
    {
        strSeletedUserId = GETVALUE(CEO_UserId);
    }
    else
    {
        RequestActivityModel *actModel        = [arrRequestActivityList objectAtIndex:tag];
        if ([actModel.activity_user_name length]) {
            if([actModel.activity_user_name isEqualToString:strName])
            {
                strSeletedUserId = actModel.activity_user_id;
            }
        }
    }
    [self gotoProfileView:strSeletedUserId];
}

-(void)gotoProfileView:(NSString *)selectedEnitity{

    
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity ;
    [self.navigationController pushViewController:prof animated:YES];
}



-(void)onJoinOrUnjoinShowRoom:(UIButton *)sender
{
    RequestActivityModel *actModel        = [arrRequestActivityList objectAtIndex:sender.tag];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&inviter_id=0&showroom_tag=%@",GETVALUE(CEO_AccessToken),actModel.activity_showroom_tag] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_JOINSHOWROOM]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitVal = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submitData val %@",submitVal);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"SHOWROOMS ****** = %@",[showroomDict valueForKey:@"StatusMsg"]);
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
             aDelegate.isAnyShowroomJoinUnjoin = YES;
             
             NSLog(@"Joiend properly");
             if ([[showroomDict valueForKey:@"follow_message"] isEqualToString:@"true"])
             {
                 actModel.activity = @"you_joined_showroom";
                 actModel.activity_text = [NSString stringWithFormat:@"You have joined successfully in this showroom, *%@",actModel.activity_showroom_tag];
                 actModel.is_userjoined = @"yes";
             }
             else
             {
                 actModel.activity_text = [NSString stringWithFormat:@"You have unjoin in this showroom, *%@",actModel.activity_showroom_tag];
                 actModel.activity = @"shared_join_showroom";
                 actModel.is_userjoined = @"no";
             }
             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
             [tblRequestActivity beginUpdates];
             [tblRequestActivity reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             [tblRequestActivity endUpdates];
         }
         else
         {
             
         }
     }];
}


-(void)setJoinButtonBGColor:(UIButton *)joinBtn filledBG:(BOOL)filled
{
    if(filled)
    {
        [joinBtn setBackgroundColor:ORANGECOLOR];
        [joinBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        [joinBtn setBackgroundColor:[UIColor clearColor]];
        [joinBtn setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
        joinBtn.layer.borderColor = [[UIColor colorWithRed:(243/255.0f) green:(94/255.0f) blue:(27/255.0f) alpha:1.0]CGColor];
        joinBtn.layer.borderWidth = 1.3f;
        joinBtn.layer.masksToBounds = YES;
    }
}

-(void)onFollowUnfollow:(UIButton *)sender
{
    RequestActivityModel *actModel        = [arrRequestActivityList objectAtIndex:sender.tag];
    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&user_id=%@",GETVALUE(CEO_AccessToken),actModel.activity_user_id] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@follow/follow",ServerUrl]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:url];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"My Url %@",url);
    NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"status"] isEqualToString:@"true"]) {
             if ([[showroomDict valueForKey:@"success_message"] isEqualToString:@"followed"])
             {
                 [sender setImage:[UIImage imageNamed:@"Btn_ActivityFollowing_1x.png"] forState:UIControlStateNormal];
                 actModel.is_follow_status = @"1";
             }
             else
             {
                 [sender setImage:[UIImage imageNamed:@"Btn_ActivityFollow_1x.png"] forState:UIControlStateNormal];
                 actModel.is_follow_status = @"0";
             }
             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];

             [tblRequestActivity beginUpdates];
             [tblRequestActivity reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             [tblRequestActivity endUpdates];
         }
     }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UIScrollViewDelegate
//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    NSLog(@"%d",(yOffset / height > kNewPageLoadScrollPercentageThreshold));
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (isAPICalling || ![YSSupport isNetworkRechable]) {
        return;
    }
    if (isDataFinisedFromServer) {
        return;
    }
    
    BOOL shouldLoadNextPage = ShouldLoadNextPage(tblRequestActivity);
    if (shouldLoadNextPage)
    {
        pagenumber = pagenumber +1;
        NSLog(@"*** Scroll End ****");
        isAPICalling = YES;
        [self getRequestActivityList:pagenumber];
    }
}
- (void)createRefreshView
{
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:tblRequestActivity withYPos:10];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

- (void)refresh:(id)sender {
    pagenumber = 1;
    [self getRequestActivityList:pagenumber];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}


@end
