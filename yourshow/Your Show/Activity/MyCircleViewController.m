//
//  ContentViewController.m
//  ICViewPager
//
//  Created by Ilter Cengiz on 28/08/2013.
//  Copyright (c) 2013 Ilter Cengiz. All rights reserved.
//

#import "MyCircleViewController.h"
#import "ActivityModel.h"
#import "CellMyCircle.h"
#import "MSProfileVC.h"
#import "MintShowroomViewController.h"
#import "MSMintDetails.h"
#import "CarbonKit.h"
#import "UserAccessSession.h"

@interface MyCircleViewController ()<UITableViewDataSource,UITableViewDelegate,MyCircleCellDelegate,MSMintDetailsDelegate>
{
    int pagenumber;
    BOOL isDataFinishedInServer;
    NSMutableArray *arrMyCircleList;
   
    BOOL isAPICalling;
    BOOL isDataFinisedFromServer;
    CarbonSwipeRefresh *refreshCarbon;

}
@property (nonatomic, strong)  IBOutlet UITableView *tblMyCircleActivity;
@end

@implementation MyCircleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"You VC Called");
    isAPICalling = NO;
    isDataFinisedFromServer = NO;
    self.tblMyCircleActivity.separatorStyle = UITableViewCellSeparatorStyleNone;
    arrMyCircleList = [[NSMutableArray alloc]init];
    pagenumber = 1;
    [self createRefreshView];
    arrMyCircleList = [UserAccessSession getAllActivityMyCircleList];
    [self getMycircleList:pagenumber];

    // Changes for iPad done by mani
    if (isSmallPad) {
        CGRect rctTransparent = _tblMyCircleActivity.frame;
        rctTransparent.size.height = self.view.frame.size.height-100;
        [_tblMyCircleActivity setFrame:rctTransparent];
    }
}

-(void)getMycircleList:(int)pageNo
{
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pageNo] forKey:@"page_number"];
    [dicts setValue:@"2" forKey:@"tab"];
    
    [YSAPICalls getActivitySegmentInfo:dicts methodName:METHOD_GETACTIVITYMYCIRCLE  SuccessionBlock:^(id newResponse) {
        Generalmodel *gModel = newResponse;
        if ([gModel.status_code integerValue])
        {
            isAPICalling = NO;
            
            if (pagenumber == 1)
            {
                if ([arrMyCircleList count])
                    [arrMyCircleList removeAllObjects];
            }
            if([gModel.tempArray count]>0)
            {
                isDataFinisedFromServer = NO;
                [arrMyCircleList addObjectsFromArray:gModel.tempArray];
                
                if(pagenumber==1)
                    [UserAccessSession InsertActivityMyCircleDataBase:arrMyCircleList];
            }
            else
            {
                isDataFinisedFromServer = YES;
            }
            [self.tblMyCircleActivity reloadData];
        }
    }andFailureBlock:^(NSError *error)
     {
         isAPICalling = NO;
     }];
}


#pragma mark - UITableView delegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (![YSSupport isNetworkRechable]) {
        return ([arrMyCircleList count])?[arrMyCircleList count]:0;
    }
    if ([arrMyCircleList count]>=8) {
        return ([arrMyCircleList count])?((isDataFinisedFromServer)?[arrMyCircleList count]:[arrMyCircleList count]+1):0;
    }
    
    return ([arrMyCircleList count])?((isDataFinisedFromServer)?[arrMyCircleList count]:[arrMyCircleList count]):0;
}



// need to change
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if((indexPath.row>=(arrMyCircleList.count)))
    {
        return 59;
    }
    
    ActivityModel *actModel        = [arrMyCircleList objectAtIndex:indexPath.row];
    
    CGFloat getRowHeight = [self calculateCellHeight:actModel index:indexPath.row];
    return getRowHeight;
}
-(CGFloat)calculateCellHeight:(ActivityModel *)actModel index:(NSInteger)nIndex
{
    NSString *strActText = [self getActivityString:actModel.activity_text date:actModel.activity_time width:214];
    
    CGFloat strHeight = [self getStringHeight:strActText width:214];
    int yPos = (strHeight>28)?2:10;
    
    if ([actModel.activity_type isEqualToString:@"follow"])
    {
        return strHeight+yPos+50;
    }
    
    if (actModel.arrFeedDetails.count>1)
    {
        NSString *strProfileImage = [[actModel.arrFeedDetails objectAtIndex:0]valueForKey:@"feed_thumb_image"];
        if ([strProfileImage length]>0)
        {
            return strHeight+yPos+50;
        }
        else
        {
            return strHeight+yPos+50;
        }
    }
    
    if (actModel.arrShowroomDetails.count>1 && [actModel.activity_type isEqualToString:@"showroom"])
    {
        NSString *strProfileImage = [[actModel.arrShowroomDetails objectAtIndex:0]valueForKey:@"showroom_thumb_image"];
        if ([strProfileImage length]>0)
        {
            return strHeight+yPos+50;
        }
    }
    
    if (actModel.arrMintListDetails.count>1)
    {
        NSString *strProfileImage = [[actModel.arrMintListDetails objectAtIndex:0]valueForKey:@"feed_thumb_image"];
        if ([strProfileImage length]>0)
        {
            return strHeight+yPos+50;
        }
        else
        {
            return strHeight+yPos+50;
        }

    }
    
    return ((strHeight>50)?strHeight+5:strHeight)+yPos+17;
}

-(NSString *)getActivityString:(NSString *)strText date:(NSString *)strDate width:(CGFloat)width
{
    NSString *strActText;
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
    UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    
    CGSize stringTextSize = [strText sizeWithAttributes:@{NSFontAttributeName:font}];
    CGSize stringDateSize = [strDate sizeWithAttributes:@{NSFontAttributeName:font1}];
    
    CGFloat totalWidth = stringTextSize.width + stringDateSize.width;
    
    
    if (stringTextSize.width<width)
    {
        strActText = [NSString stringWithFormat:@"%@\n%@",strText,strDate];
    }
    else
    {
        if(totalWidth>((width*3))) // More than two line
            strActText = [NSString stringWithFormat:@"%@ %@",strText,strDate];
        if(totalWidth>((width*2)-20)) // More than one line
            strActText = [NSString stringWithFormat:@"%@\n%@",strText,strDate];
        else
            strActText = [NSString stringWithFormat:@"%@ %@",strText,strDate];
    }
    return strActText;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _tblMyCircleActivity.separatorStyle = UITableViewCellSeparatorStyleNone;

    static NSString *cellIdentifier = @"CellMyCircle";
    
    CellMyCircle  *cell = (CellMyCircle *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    cell.cellDelegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.arrModelActivity = arrMyCircleList;
    if((indexPath.row>=arrMyCircleList.count))
    {
        [cell.viewContainer setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:cell.viewNoMoreMints];
        return cell;
    }
    
    ActivityModel *actModel        = [arrMyCircleList objectAtIndex:indexPath.row];
    [cell setCellData:actModel index:(int)indexPath.row];

    if ([actModel.activity_type isEqualToString:@"showroom"])
        [cell.btnMintThumb addTarget:self action:@selector(gotoShowroomPage:) forControlEvents:UIControlEventTouchUpInside];
    else
        [cell.btnMintThumb addTarget:self action:@selector(gotoMintDetailsPage:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.lblActivityText.tag = indexPath.row;
    cell.btnUserThumb.tag = indexPath.row;
    cell.btnMintThumb.tag = indexPath.row;
    [cell.btnUserThumb addTarget:self action:@selector(onGotoProfilePageFromMycircle:) forControlEvents:UIControlEventTouchUpInside];

    __weak STTweetLabel *lblActivityTemp = cell.lblActivityText;
    cell.lblActivityText.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
    {
        if (hotWord == STTweetStartag)
        {
            [self gotoShowroomViewWithTag:(int)lblActivityTemp.tag showTag:string];
        }
        else if(hotWord == TweetActivityUserName)
        {
            [self gotoProfileUserWithTag:(int)lblActivityTemp.tag userName:string];
        }
    };
    
    return cell;
}

-(void)onGotoProfilePageFromMycircle:(UIButton *)btnTag
{
    ActivityModel *actModel        = [arrMyCircleList objectAtIndex:[btnTag tag]];
    if ([actModel.activity_user_id length])
    {
        [self gotoProfilePageView:actModel.activity_user_id];
    }
    else
    {
        [self gotoProfilePageView:actModel.first_user_id];
    }
}
-(void)gotoMintDetailsPage: (UIButton *)btnMint
{
    ActivityModel *actModel        = [arrMyCircleList objectAtIndex:[btnMint tag]];
    if (actModel.arrMintListDetails.count) {
        [self gotoMintDetailsView:[[actModel.arrMintListDetails objectAtIndex:0] valueForKey:@"feed_id"]];
    }
    else if (actModel.arrFeedDetails.count) {
        [self gotoMintDetailsView:[[actModel.arrFeedDetails objectAtIndex:0] valueForKey:@"feed_id"]];
    }
    else
        [self gotoMintDetailsView:actModel.feed_id];
    
}
-(void)gotoMintDetailsView: (NSString *)feedid
{
    MSMintDetails  *mintDetailsVC =  [[MSMintDetails alloc] initWithNibName:@"MSMintDetails" bundle:nil];
    mintDetailsVC.mint_id = feedid;
    mintDetailsVC.delegate = self;
    mintDetailsVC.isToshowSingleRecord = YES;
    mintDetailsVC.isFromActivity = YES;
    [self.navigationController pushViewController:mintDetailsVC animated:YES];
}

-(void)gotoShowroomPage: (UIButton *)btnShowroom
{
    ActivityModel *actModel        = [arrMyCircleList objectAtIndex:[btnShowroom tag]];
    if ([actModel.arrShowroomDetails count]>0) {
        [self gotoShowroomViewWithTag:(int)[btnShowroom tag] showTag:[NSString stringWithFormat:@"*%@",[[actModel.arrShowroomDetails objectAtIndex:0] valueForKey:@"showroom_tag"]]];
    }
    else
        [self gotoShowroomViewWithTag:(int)[btnShowroom tag] showTag:[NSString stringWithFormat:@"*%@",actModel.activity_showroom_tag]];
}
-(void)gotoShowroomViewWithTag:(int)tag showTag:(NSString *)strShowroomName
{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.lbl.hidden   = YES;
    
    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [strShowroomName stringByReplacingCharactersInRange:NSMakeRange(0, 1)  withString:@""];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}

-(void)gotoProfileUserWithTag:(int)tag userName:(NSString *)strName
{
    NSString *strSeletedUserId;
    
    if ([strName isEqualToString:@"Your's"] || [strName isEqualToString:@"Your"] || [strName isEqualToString:@"You"])
    {
        strSeletedUserId = GETVALUE(CEO_UserId);
    }
    else
    {
        ActivityModel *actModel        = [arrMyCircleList objectAtIndex:tag];
        if ([actModel.first_user_name length])
        {
            if([actModel.first_user_name isEqualToString:strName])
            {
                strSeletedUserId = actModel.first_user_id;
            }
            else if([actModel.second_user_name isEqualToString:strName])
            {
                strSeletedUserId = actModel.second_user_id;
            }
        }
        else if ([actModel.activity_user_name length])
        {
            if([actModel.activity_user_name isEqualToString:strName])
            {
                strSeletedUserId = actModel.activity_user_id;
            }
        }
        
    }
    [self gotoProfilePageView:strSeletedUserId];
}

-(void)gotoProfilePageView:(NSString *)selectedEnitity{

    
    
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    // prof.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:prof animated:YES];
}

-(CGFloat)getStringHeight:(NSString *)strText  width:(CGFloat)width
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGFloat textHeight  = [strText boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    
    return textHeight;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UIScrollViewDelegate
//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.95;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    NSLog(@"%d",(yOffset / height > kNewPageLoadScrollPercentageThreshold));
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (isAPICalling || ![YSSupport isNetworkRechable]) {
        return;
    }
    
    if (isDataFinisedFromServer) {
        return;
    }
    
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self.tblMyCircleActivity);
    if (shouldLoadNextPage)
    {
        pagenumber = pagenumber +1;
        NSLog(@"*** Scroll End ****");
        isAPICalling = YES;
        [self.tblMyCircleActivity reloadData];
        [self getMycircleList:pagenumber];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createRefreshView
{
    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:_tblMyCircleActivity withYPos:10];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    
    [refreshCarbon addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
}

- (void)refresh:(id)sender {
    
    pagenumber = 1;
    [self getMycircleList:pagenumber];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [refreshCarbon endRefreshing];
    });
}

//For Mute mint, Delete Mint. FlagInappropriate
-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId
{
    int mintIndex = 0;
    BOOL isMintFoundFromList = NO;
    
    for (int i = 0; i<arrMyCircleList.count; i++) {
        
        
        if ([[[arrMyCircleList objectAtIndex:i]feed_id] isEqualToString:mintId]) {
            mintIndex = i;
            isMintFoundFromList = YES;
            break;
        }
        
    }
    
    if (isMintFoundFromList) {
        
        [arrMyCircleList removeObjectAtIndex:mintIndex];
        
        [self.tblMyCircleActivity reloadData];
    }
    
    
}
//Update the tableview if the MintUser Mint exist
-(void)muteUserOfmintDetails:(NSString *)muteUser{
    
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
    for (int currentIndex = 0;currentIndex<[arrMyCircleList count]; currentIndex++){
        //do stuff with obj
        ActivityModel *feedlist = [arrMyCircleList objectAtIndex:currentIndex];
        if([muteUser isEqualToString:feedlist.activity_user_id]){
            [indexesToDelete addIndex:currentIndex];
        }
    }
    
    if ([indexesToDelete count]) {
        [arrMyCircleList removeObjectsAtIndexes:indexesToDelete];
        [self.tblMyCircleActivity reloadData];
    }
    
    
}

@end
