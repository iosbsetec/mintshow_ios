//
//  YouViewController.h
//  ICViewPager
//
//  Created by JITENDRA KUMAR PRADHAN on 29/02/16.
//  Copyright © 2016 Ilter Cengiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YouActivityDelegate <NSObject>

-(void)updateAllCounterInfo;
@end

@interface YouViewController : UIViewController
@property (nonatomic,assign) id <YouActivityDelegate> aDelegate;

@end
