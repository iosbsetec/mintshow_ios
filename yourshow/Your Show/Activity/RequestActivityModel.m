//
//  CommentModel.m
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 22/09/14.
//
//

#import "RequestActivityModel.h"

@implementation RequestActivityModel

-(RequestActivityModel *)initWithRequestActivity : (NSDictionary *) activityInfo
{
    self.activity                   = [activityInfo valueForKey:@"activity_type"];
    self.activity_type              = [activityInfo valueForKey:@"type"];
    self.activity_time              = [activityInfo valueForKey:@"activity_time"];
    self.activity_text              = [activityInfo valueForKey:@"activity_text"];
    self.activity_image             = [activityInfo valueForKey:@"activity_image"];
    self.activity_total_pages       = [activityInfo valueForKey:@"total_pages"];;

    if ([[activityInfo   valueForKey:@"data"] count])
    {
        self.is_userjoined                  = ([self.activity isEqualToString:@"invite_collaborator"])?[[activityInfo   valueForKey:@"data"] valueForKey:@"is_user_accepted"]:[[activityInfo   valueForKey:@"data"] valueForKey:@"is_user_joined"];
        self.is_follow_status               = [[activityInfo   valueForKey:@"data"] valueForKey:@"is_follow_status"];
        self.activity_user_id               = [[activityInfo   valueForKey:@"data"] valueForKey:@"user_id"];
        self.activity_user_name             = [[activityInfo   valueForKey:@"data"] valueForKey:@"user_name"];
        self.activity_showroom_id           = [[activityInfo   valueForKey:@"data"] valueForKey:@"showroom_id"];
        self.activity_showroom_name         = [[activityInfo   valueForKey:@"data"] valueForKey:@"showroom_name"];
        self.activity_showroom_tag          = [[activityInfo   valueForKey:@"data"] valueForKey:@"showroom_tag"];
        self.activity_showroom_thumb_image  = [[activityInfo   valueForKey:@"data"] valueForKey:@"showroom_thumb_image"];
    }
    
    return  self;
}
@end
