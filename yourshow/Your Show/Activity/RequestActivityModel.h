//
//  CommentModel.h
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 22/09/14.
//
//

#import <Foundation/Foundation.h>

@interface RequestActivityModel : NSObject


@property(nonatomic,strong)NSString *activity;
@property(nonatomic,strong)NSString *activity_type;
@property(nonatomic,strong)NSString *activity_text;
@property(nonatomic,strong)NSString *activity_time;
@property(nonatomic,strong)NSString *activity_image;
@property(nonatomic,strong)NSString *activity_id;
@property(nonatomic,strong)NSString *activity_thumb_image;
@property(nonatomic,strong)NSString *activity_user_id;
@property(nonatomic,strong)NSString *activity_user_name;
@property(nonatomic,strong)NSString *activity_showroom_id;
@property(nonatomic,strong)NSString *activity_showroom_name;
@property(nonatomic,strong)NSString *activity_showroom_tag;
@property(nonatomic,strong)NSString *activity_showroom_thumb_image;
@property(nonatomic,strong)NSString *activity_total_pages;
@property(nonatomic,strong)NSString *activity_feed_thumb_image;
@property(nonatomic,strong)NSString *is_userjoined;

@property(nonatomic,strong)NSString *is_follow_status;

@property(nonatomic,strong)  NSString *status;
@property(nonatomic,strong)  NSString *status_Msg;
@property(nonatomic,strong)  NSString *status_code;
@property(nonatomic,strong)  NSString *status_commnetEdited;

@property (nonatomic,assign) BOOL postingStatus;


-(RequestActivityModel *)initWithRequestActivity : (NSDictionary *) activityInfo;

@end



                                             