
//
//  AppDelegate.m
//  Your Show
//
//  Created by Siba Prasad Hota on 16/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#define DEACTIVATE_ACCOUNT_MESSAGE @"Your account has been deactivated.", @"To reactivate your account, log in using your old login email address and password. You will then be able to use the site as before. We hope you come back soon."
#import "AppDelegate.h"
#import "MyShowViewController.h"
#import "MintViewController.h"
#import "ActivityViewController.h"
#import "MSProfileVC.h"
#import "AddMintViewController.h"
#import "SPHSingletonClass.h"
#import "DiscoverViewController.h"
#import <HockeySDK/HockeySDK.h>
#import "DummyViewController.h"
#import "MSLandingPage.h"
#import "Keys.h"
#import "LNNotificationsUI.h"
#import "UserAccessSession.h"
#import "MSSettingVC.h"
#import "UIImage+animatedGIF.h"
#import  <Instabug/Instabug.h>



@interface AppDelegate ()
{

    UIView *view;
}

@end

@implementation AppDelegate
@synthesize lbl;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
 
    //[self registerForRemoteNotification];
    // In application:didFinishLaunchingWithOptions:
    Mixpanel *mixpanel = [Mixpanel sharedInstanceWithToken:
                          @"AA5c4e83e8f167e82ae433171e3d54cbeda879b5cf"];
    // Turn this off so the notification doesn't pop up automatically.
    mixpanel.showNotificationOnActive = NO;
    
    
    [MSMixPanelTrackings trackAppOpenWithCount:0 WithAccessToken:@""];
    [NewRelicAgent startWithApplicationToken:@"AA5c4e83e8f167e82ae433171e3d54cbeda879b5cf"];
    
    [Instabug startWithToken:@"15bda8ce39772667f3494448dc295f9c" invocationEvent:IBGInvocationEventShake];
    [Instabug setColorTheme:IBGColorThemeLight];
    

    if (![GETVALUE(APPINSTALLEDFIRSTTIME) isEqualToString:@"1"]) {
        [MSMixPanelTrackings trackAppInstallWithData];
        SETVALUE(@"1",APPINSTALLEDFIRSTTIME);
        SYNCHRONISE;
        [MSMixPanelTrackings trackAppOpen:@"1" lastAppOpenDate:nil];
        
        SETVALUE(@"1",@"APPOPENCOUNT");
        SYNCHRONISE;
        
    }

    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0 ;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    MSLandingPage *splsh = [[MSLandingPage alloc]initWithNibName:@"MSLandingPage" bundle:nil];
    UINavigationController *newVc = [[UINavigationController alloc]initWithRootViewController:splsh];
    newVc.navigationBarHidden = YES;
    self.window.rootViewController = newVc;
    [self.window makeKeyAndVisible];
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [[LNNotificationCenter defaultCenter] registerApplicationWithIdentifier:@"123" name:@"Notifications" icon:[UIImage imageNamed:@"Icon.png"] defaultSettings:[LNNotificationAppSettings defaultNotificationAppSettings]];
     [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"20e4a496a06740c3bdedad14f6831907 "];
//     [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"62e43ce1fcac4c578f15c30aee842801"];
    //[[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"20e4a496a06740c3bdedad14f6831907"];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    
    sleep(3.0);
    return YES;
}

-(void)checkForPushNotificationEnableorDisable
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        UIUserNotificationType type = [[[UIApplication sharedApplication] currentUserNotificationSettings] types];
        if (type == UIUserNotificationTypeNone){
            if ([[self getDeviceTkenFromKeyChain] length]) {
                [self updateDeviceInfo:0];
            }
        }
    }
    else {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types == UIRemoteNotificationTypeNone) {
            if ([[self getDeviceTkenFromKeyChain] length]) {
                [self updateDeviceInfo:0];
            }
        }
    }

}

-(void)createStreakPopup:(NSString *)strikDays
{
    
    /* GET ALL UI OBJECTS FROM THE NIB FILE
      // Getting background view which contain
      // Getting the dayscount for mintstreak
     */
    if (view) {
        [view removeFromSuperview];
    }
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"MintStreakPopup" owner:nil options:nil];
    view = nibContents[0];
    view.tag = 1000;
    
    UIView *viewMain = (UIView *)[view viewWithTag:9000];
    viewMain.layer.cornerRadius = 10.0;
    [viewMain setClipsToBounds:YES];
    
    UILabel *lableDaysCount = (UILabel *)[view viewWithTag:300];
    [lableDaysCount setText:strikDays];

    UILabel *labelDaysInfo = (UILabel *)[view viewWithTag:400];
    [labelDaysInfo setText:[strikDays stringByAppendingString:@" day streak"]];

    
    /* GET ALL GIF FILES FOR SHOWING THE ANIMATIONS
       // APPLYED COLOR FOR ALL UI ACCORDING TO MINTSTREAK DAYS
       // Getting the dayscount for mintstreak
     */

    NSString *pathForFile =  [[NSBundle mainBundle] pathForResource:([strikDays integerValue]<=7)?@"Orange_mintStreak_popUp":(([strikDays integerValue]<=14)?@"Green_mintStreak_popUp":@"Blue_mintStreak_popUp") ofType: @"gif"];
    
    UIColor *applyColor = ([strikDays integerValue]<=7)?ORANGECOLORCOACHINGTIPS:(([strikDays integerValue]<=14)?GREENCOLORCOACHINGTIPS:BLUECOLORCOACHINGTIPS);
    
    lableDaysCount.textColor  = applyColor;
    labelDaysInfo.textColor   = applyColor;


    
    /* APPLYING THE ANIMATIONS */

    NSData *dataOfGif = [NSData dataWithContentsOfFile: pathForFile];
    UIImage *testImage = [UIImage animatedImageWithAnimatedGIFData:dataOfGif];

    UIImageView *loader = (UIImageView *)[view viewWithTag:7000];
    [loader setImage:[UIImage animatedImageWithAnimatedGIFData:dataOfGif]];
    loader.animationImages = testImage.images;
    loader.animationDuration = testImage.duration;
    loader.animationRepeatCount = 1;
    loader.image = testImage.images.lastObject;
    [loader startAnimating];

    
    UIButton *bgButton = (UIButton *)[view viewWithTag:11];
    [bgButton addTarget:self action:@selector(removeAppdelegatePopupview:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *crossButton = (UIButton *)[view viewWithTag:8000];
    [crossButton addTarget:self action:@selector(removeAppdelegatePopupview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *gotItButton = (UIButton *)[view viewWithTag:500];
    [gotItButton addTarget:self action:@selector(removeAppdelegatePopupview:) forControlEvents:UIControlEventTouchUpInside];
    [gotItButton setTitleColor:applyColor forState:UIControlStateNormal];

    
    if ([view superview] == nil) {
        [[UIApplication sharedApplication].keyWindow addSubview:view];
    }
    [AppDelegate updateStreakPopup];
}

-(void)createCelebrationPopup:(NSString *)mintCountDays withUsername:(NSString *)username userimageUrl:(NSString *)userimageUrl
{
    //    AppDelegate *app = MINTSHOWAPPDELEGATE;
    //    [MBProgressHUD showHUDAddedTo:app.window animated:YES];
    /* GET ALL UI OBJECTS FROM THE NIB FILE
     // Getting background view which contain
     // Getting the dayscount for mintstreak
     */
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CelebrationPopup" owner:nil options:nil];
    view = nibContents[0];
    UIView *viewMain = (UIView *)[view viewWithTag:9000];
    viewMain.layer.cornerRadius = 10.0;
    [viewMain setClipsToBounds:YES];
    
    UILabel *lableUserName = (UILabel *)[view viewWithTag:20];
    [lableUserName setText:username];
    
    UILabel *labelmintCount = (UILabel *)[view viewWithTag:30];
    
    
    
    [labelmintCount setText:[[AppDelegate addSuffixToNumber:(int)[mintCountDays integerValue]] stringByAppendingString:@" Mint!"]];
    
    
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        // Background work
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            /* GET ALL GIF FILES FOR SHOWING THE ANIMATIONS
             // APPLYED COLOR FOR ALL UI ACCORDING TO MINTSTREAK DAYS
             // Getting the dayscount for mintstreak
             */
            NSString *pathForFile =  [[NSBundle mainBundle] pathForResource:@"Mint_Celebration_popup"ofType: @"gif"];
            
            /* APPLYING THE ANIMATIONS */
            
            NSData *dataOfGif = [NSData dataWithContentsOfFile: pathForFile];
            UIImage *testImage = [UIImage animatedImageWithAnimatedGIFData:dataOfGif];
            
            UIImageView *loader = (UIImageView *)[view viewWithTag:7000];
            [loader setImage:[UIImage animatedImageWithAnimatedGIFData:dataOfGif]];
            loader.animationImages = testImage.images;
            loader.animationDuration = testImage.duration;
            loader.animationRepeatCount = 1;
            loader.image = testImage.images.lastObject;
            [loader startAnimating];
            
            UIImageView *userImage = (UIImageView *)[view viewWithTag:10];
            userImage.mintImageURL = [NSURL URLWithString:userimageUrl];
            [userImage setClipsToBounds:YES];
            
            [AppDelegate makeUserImageAnimation:userImage withOldRect:userImage.frame];

            // Main thread work (UI usually)
        }];
    }];
    
    
    
    UIButton *bgButton = (UIButton *)[view viewWithTag:11];
    [bgButton addTarget:self action:@selector(removeAppdelegatePopupview:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *crossButton = (UIButton *)[view viewWithTag:8000];
    [crossButton addTarget:self action:@selector(removeAppdelegatePopupview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *gotItButton = (UIButton *)[view viewWithTag:40];
    [gotItButton addTarget:self action:@selector(removeAppdelegatePopupview:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (![view superview]) {
        [[UIApplication sharedApplication].keyWindow addSubview:view];
        
    }
    
    //    [MBProgressHUD hideHUDForView:app.window animated:YES];
    
}

+(NSString *) addSuffixToNumber:(int) number
{
    NSString *suffix;
    int ones = number % 10;
    int tens = (number/10) % 10;
    
    if (tens ==1) {
        suffix = @"th";
    } else if (ones ==1){
        suffix = @"st";
    } else if (ones ==2){
        suffix = @"nd";
    } else if (ones ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    
    NSString * completeAsString = [NSString stringWithFormat:@"%d%@", number, suffix];
    return completeAsString;
}

+(void)makeUserImageAnimation:(UIView *)viewTips withOldRect:(CGRect)oldFrame
{
    CGRect rectLbl = viewTips.frame;
    rectLbl.origin.y = oldFrame.origin.y-150;
    viewTips.frame = rectLbl;
    
    [UIView animateWithDuration:0.16
                          delay:1.5f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         viewTips.frame = oldFrame;
                     } completion:^(BOOL finished)
     { }];
}



+(void)removeGifImage:(UIImageView *)img
{
    [img removeFromSuperview];


}


-(void)createStreakCoachingTips:(NSString *)strikDays
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"StreakCoachingTipsPopUp" owner:nil options:nil];
    view = nibContents[0];
    
    UIView *viewMain = (UIView *)[view viewWithTag:9000];
    viewMain.layer.cornerRadius = 10.0;
    viewMain.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    viewMain.layer.shadowOpacity = 1.0f;
    viewMain.layer.shadowRadius = 8.0f;
    [viewMain setClipsToBounds:YES];
    
    UIImageView *topArrow = (UIImageView *)[view viewWithTag:10];

    UIButton *crossButton = (UIButton *)[view viewWithTag:7000];
    [crossButton addTarget:self action:@selector(removeAppdelegatePopupview:) forControlEvents:UIControlEventTouchUpInside];
    
   
    if ([strikDays integerValue]<=7)
    {
        viewMain.backgroundColor = ORANGECOLORCOACHINGTIPS;
        [crossButton setTitleColor:ORANGECOLORCOACHINGTIPS forState:UIControlStateNormal];

        [topArrow setImage:[UIImage imageNamed:@"top_orange"]];
    }
    
    else if ([strikDays integerValue]<=14)
    {
        viewMain.backgroundColor = GREENCOLORCOACHINGTIPS;
        [crossButton setTitleColor:GREENCOLORCOACHINGTIPS forState:UIControlStateNormal];

        [topArrow setImage:[UIImage imageNamed:@"top_green"]];
    }
    
    else
    {
        if ([strikDays integerValue] > 21) {
            UIImageView *mintImage = (UIImageView *)[view viewWithTag:20];
            [mintImage setImage:[UIImage imageNamed:@"streak_fire_white"]];


        }
        viewMain.backgroundColor = BLUECOLORCOACHINGTIPS;
        [crossButton setTitleColor:BLUECOLORCOACHINGTIPS forState:UIControlStateNormal];
        [topArrow setImage:[UIImage imageNamed:@"top_blue"]];
    }

    
    
    UIButton *bgButton = (UIButton *)[view viewWithTag:11];
    [bgButton addTarget:self action:@selector(removeAppdelegatePopupview:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UILabel *labelDaysInfo = (UILabel *)[view viewWithTag:8000];
    [labelDaysInfo setText:[strikDays stringByAppendingString:@" day streak"]];
    
    UIView *label1 = (UIView *)[view viewWithTag:1234];
    label1.layer.cornerRadius = 70.0;
    [label1 setClipsToBounds:YES];
    
    [[UIApplication sharedApplication].keyWindow addSubview:view];
}

+(void)removeview :(UIButton *)crossButton
{
    
    [[crossButton superview]removeFromSuperview];
    
}
-(void)removeAppdelegatePopupview :(UIButton *)crossButton
{
   // openedView
    [view removeFromSuperview];
    view = nil;
    
}
+(void)removeCoachingTip :(UIButton *)crossButton
{
    [[[[crossButton superview] superview] superview]removeFromSuperview];
    
    
}
+(void)removeStreakView :(UIButton *)crossButton
{
    [[[crossButton superview] superview]removeFromSuperview];


}
-(void)registerForRemoteNotification{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert|UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }else  {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert |UIUserNotificationTypeBadge)];
    }
}
void uncaughtExceptionHandler(NSException *exception)
{
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
    // Internal error reporting
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString* deviceTokenData = [[[[deviceToken description]
                                   stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                   stringByReplacingOccurrencesOfString: @">" withString: @""]
                                   stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"Device_Token     -----> %@\n",deviceTokenData);
    [self updateDeviceTkenInKeyChain:deviceTokenData];
    if ([GETVALUE(CEO_AccessToken) length]) {
        [self updateDeviceInfo:1];
    }
   // [self CallAPIFornDevice:deviceTokenData];
    NSLog(@"Device_Token from data base     -----> %@\n",[[UserAccessSession getUserSession] User_deviceToken]);
    SETVALUE(@"yes", APPPUSHALLOWED);
    SYNCHRONISE;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"User info %@",userInfo);
    [self applicationDidOpenFromRemoteNotification:[userInfo valueForKey:@"aps"]];
    
    if ([GETVALUE(CEO_AccessToken) length]) {
        
//        [{action=, activity_type=, from=580807782583, link=, category=7, badge=, title=deactivate, collapse_key=do_not_collapse}]
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {

//        
//        if (([[[userInfo valueForKey:@"aps"] valueForKey:@"category"] integerValue]==7)  || ([[[userInfo valueForKey:@"aps"] valueForKey:@"category"] integerValue] == 6)){
//            [self updateLogoutInfo];
//
//            if ([[[userInfo valueForKey:@"aps"] valueForKey:@"category"] integerValue]==7) {
//                showAlert(@"Your account has been deactivated.", @"To reactivate your account, log in using your old login email address and password. You will then be able to use the site as before. We hope you come back soon.", @"ok", nil);
//            }
//            else
//                showAlert(@"Your account has been deleted.", nil, @"ok", nil);
//            return;
//        
//        }
//        
//       else
        if ([[[userInfo valueForKey:@"aps"] valueForKey:@"category"] integerValue]==21)
        {
            if ([[[userInfo valueForKey:@"aps"] valueForKey:@"is_mint_streak"] isEqualToString:@"yes"])
                [self createStreakPopup:[[userInfo valueForKey:@"aps"] valueForKey:@"count"]];
            
                
                if (self.ysTabBarController.selectedIndex == 4)
                {
                    NSDictionary* notiUserInfo = @{@"streak_info": [[userInfo valueForKey:@"aps"] valueForKey:@"count"]};
                    NSNotificationCenter* nc   = [NSNotificationCenter defaultCenter];
                    [nc postNotificationName:@"StreakNotification" object:self userInfo:notiUserInfo];
                }
            
            return;
        }

        else  if ([[userInfo valueForKey:@"aps"] valueForKey:@"badge"] && ([[[userInfo valueForKey:@"aps"] valueForKey:@"category"] integerValue] != 6)) {
                
                NSLog(@"controller info %@",[((UITabBarController*)self.window.rootViewController).selectedViewController class]);
                
                [self updateSinletonValue:[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] you_count:[[userInfo valueForKey:@"aps"] valueForKey:@"you_count"] request_count:[[userInfo valueForKey:@"aps"] valueForKey:@"request_count"]];
                
                [self updateCounterValue:[[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] integerValue]];
                
              
                if (self.ysTabBarController.selectedIndex == 3) {
                    NSDictionary* notiUserInfo = @{@"activity_type": [[userInfo valueForKey:@"aps"] valueForKey:@"activity_type"]};
                    NSNotificationCenter* nc   = [NSNotificationCenter defaultCenter];
                    [nc postNotificationName:@"TestNotification" object:self userInfo:notiUserInfo];
                }
            }
        }
        else{
        
            [self.ysTabBarController setSelectedIndex:3];
            [self changeBackgroundOfTabBarForIndex:3];
        }
    }
}




- (void)applicationDidOpenFromRemoteNotification:(NSDictionary *)userInfo {
    if ([[userInfo valueForKey:@"alert"] length]<5) {
        return;
    }
    LNNotification* notification = [LNNotification notificationWithMessage:[userInfo valueForKey:@"alert"]];
    notification.soundName = @"demo.aiff";
    notification.title = @"Mint Show";
    notification.defaultAction = [LNNotificationAction actionWithTitle:@"View" handler:^(LNNotificationAction *action) {
    }];
    // go to profile
    [[LNNotificationCenter defaultCenter] presentNotification:notification forApplicationIdentifier:@"123"];
}


-(void)showApplicationNotification:(NSDictionary*)appsDict{
    
    LNNotification* notification = [LNNotification notificationWithMessage:[appsDict valueForKey:@"alert"]];
    notification.soundName = @"demo.aiff";
    
    
    UINavigationController *nvc= (UINavigationController *)self.window.rootViewController;
    
    if ([[appsDict valueForKey:@"type"] isEqualToString:@"AAD"]) {
        notification.title = @"New activity!";
        if ([[[nvc viewControllers] lastObject] isKindOfClass:[DummyViewController class]]) {
            notification.defaultAction = [LNNotificationAction actionWithTitle:@"View" handler:^(LNNotificationAction *action) {
            }];
            // go to profile
        }else{
            
        }
        // Appointment accepted
    }else if ([[appsDict valueForKey:@"type"] isEqualToString:@"DAC"]||[[appsDict valueForKey:@"type"] isEqualToString:@"SMS"]||[[appsDict valueForKey:@"type"] isEqualToString:@"NCM"]) {
        
    }else if ([[appsDict valueForKey:@"type"] isEqualToString:@"XYZ"]) {
        // Appointment accepted
    }else if ([[appsDict valueForKey:@"type"] isEqualToString:@"ABC"]) {
        // Appointment accepted
    }
    
    [[LNNotificationCenter defaultCenter] presentNotification:notification forApplicationIdentifier:@"123"];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"error is %@",error);
}

-(NSString*)getDeviceID {
    id getvalue=[SPHKeyChain load:@"SavedKeyChainData"];
    if ([getvalue respondsToSelector:@selector(objectForKey:)])  {
        return ([[getvalue valueForKey:@"ConstantUDID"] length]<1)?[self generateUUID]:[getvalue valueForKey:@"ConstantUDID"];
    }
    else{
        return [self generateUUID];
    }
}

-(NSString*)getDeviceTkenFromKeyChain {
    id getvalue=[SPHKeyChain load:@"SavedKeyChainData"];
    if ([getvalue respondsToSelector:@selector(objectForKey:)])  {
        return [getvalue valueForKey:CEO_DeviceToken];
    }
    return  @"";
}
-(void)updateDeviceTkenInKeyChain:(NSString *)token {
    
    NSMutableDictionary *firstNameDict = [SPHKeyChain load:@"SavedKeyChainData"];
    if ([firstNameDict respondsToSelector:@selector(objectForKey:)])  {
            [firstNameDict setObject:token forKey:CEO_DeviceToken];
            [SPHKeyChain save:@"SavedKeyChainData" data:firstNameDict];
    }
}

-(NSString*)generateUUID {
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    NSString *UniqueID = [oNSUUID UUIDString];
    NSMutableDictionary *firstNameDict=[NSMutableDictionary new];
    [firstNameDict setObject:UniqueID forKey:@"ConstantUDID"];
    [SPHKeyChain save:@"SavedKeyChainData" data:firstNameDict];
    return UniqueID;
}

-(void)logOutDidFinish:(UITabBarController *)susTabbar {
    [self.myPointView removeFromSuperview];
    NSMutableArray *controllersArray = [self.ysTabBarController.viewControllers mutableCopy];
    for(UIViewController *tempVC in controllersArray) {
        if([tempVC isKindOfClass:[UIViewController class]]) {
            [tempVC removeFromParentViewController];
        }
    }
    self.ysTabBarController         = nil;
    MSLandingPage *splsh     = [[MSLandingPage alloc]initWithNibName:@"MSLandingPage" bundle:nil];
    UINavigationController *newVc   = [[UINavigationController alloc]initWithRootViewController:splsh];
    newVc.navigationBarHidden = YES;
    self.window.rootViewController  = newVc;
    [self.window makeKeyAndVisible];
}



-(void)authenticationDidFinish:(UIViewController *)suslogin selectedInde:(NSInteger)selectdTabVal{
    UINavigationController *nvc = (UINavigationController *)self.window.rootViewController;
    NSArray* tempVCA = [nvc viewControllers];
    for(UIViewController *tempVC in tempVCA) {
        if([tempVC isKindOfClass:[UIViewController class]]) {
            [tempVC removeFromParentViewController];
        }
    }
    if(suslogin.view)[suslogin.view removeFromSuperview];
    self.window.rootViewController = [self generateTabbarForThis:selectdTabVal];
  //  [self setupPointView];
    [self.window makeKeyAndVisible];
}

-(void)setupPointView {
    self.myPointView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.window.bounds.size.width, 50)];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self.myPointView];
    self.myPointView.alpha=1.0;
    self.myPointView.backgroundColor = [UIColor blackColor];
    
}


-(UITabBarController*)generateTabbarForThis : (NSInteger) selectedIndexVal {
    
    MyShowViewController *sphome   = [[MyShowViewController alloc]initWithNibName:@"MyShowViewController" bundle:nil];
    UINavigationController *nvc1   = [[UINavigationController alloc]initWithRootViewController:sphome];
    
    DiscoverViewController *spview = [[DiscoverViewController alloc]initWithNibName:@"DiscoverViewController" bundle:nil];
    UINavigationController *nvc2   = [[UINavigationController alloc]initWithRootViewController:spview];
    
    DummyViewController *sphcart       = [[DummyViewController alloc]initWithNibName:@"DummyViewController" bundle:nil];
    sphcart.myshow                     = sphome;
    UINavigationController *nvc3       = [[UINavigationController alloc]initWithRootViewController:sphcart];
    
    ActivityViewController *spActivity = [[ActivityViewController alloc]initWithNibName:@"ActivityViewController" bundle:nil];
    UINavigationController *nvc4       = [[UINavigationController alloc]initWithRootViewController:spActivity];
 
//    /MSProfileVC
    MSProfileVC *sprofile         = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    sprofile.btnProfileback.hidden = YES;
    UINavigationController *nvc5  = [[UINavigationController alloc]initWithRootViewController:sprofile];
   
    NSInteger nLength             = [self getDigitCount:[[UIApplication sharedApplication]  applicationIconBadgeNumber]];
    self.lbl                           = [[UILabel alloc] initWithFrame:CGRectMake(self.window.bounds.size.width-89,  4, (nLength>2)?28:18, (nLength>2)?20:18)];
    
   
    self.lbl.font                = [UIFont boldSystemFontOfSize:12];
    self.lbl.textAlignment       =  NSTextAlignmentCenter;
    self.lbl.textColor           =  [UIColor whiteColor];
    self. lbl.layer.cornerRadius  = 9;
    [self.lbl setClipsToBounds:YES];
    self. lbl.backgroundColor     = ORANGECOLOR;
    self.lbl.hidden                = YES;


    nvc1.navigationBarHidden = YES;
    nvc2.navigationBarHidden = YES;
    nvc3.navigationBarHidden = YES;
    nvc4.navigationBarHidden = YES;
    nvc5.navigationBarHidden = YES;
    
    NSArray *ctrlArr = [NSArray arrayWithObjects:nvc1,nvc2,nvc3,nvc4,nvc5,nil];
    self.ysTabBarController = [[UITabBarController alloc]init];
    
    [self.ysTabBarController.tabBar setBackgroundImage:[UIImage imageNamed:[self iPhoneimagenamefromIndex:selectedIndexVal]]];
    self.ysTabBarController.delegate  =self;
    [self.ysTabBarController setViewControllers:ctrlArr];
    self.window.rootViewController    =self.ysTabBarController;
    [self.ysTabBarController.tabBar addSubview:lbl];
    [lbl bringSubviewToFront:self.window];
    self.ysTabBarController.selectedIndex= selectedIndexVal;
    [self.window makeKeyAndVisible];
    
    
    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:self.myPointView];
    return self.ysTabBarController;
}
-(void)updateCounterValue :(NSInteger) totalCount
{
    [[UIApplication sharedApplication]  setApplicationIconBadgeNumber:totalCount];

    if (totalCount == 0) {
        self.lbl.hidden = YES;
        return;
    }
    self.self.lbl.hidden = NO;
    int nLength            = [self getDigitCount:totalCount];
    if (totalCount >0) {
        lbl.text = [NSString stringWithFormat:@"%d",(int)totalCount];
        self.self.lbl.hidden = NO;

    }
    else
    {
            self.self.lbl.hidden = YES;
            
    }
    self.lbl.frame = CGRectMake(self.window.bounds.size.width-89,  4, (nLength>2)?28:18, (nLength>2)?20:18);
}
-(int)getDigitCount :(NSInteger)badgeCount
{

    int n = 1;
    int i = (int)badgeCount;
    if (i >= 100000000){i /= 100000000; n += 8;}
    if (i >= 10000){i /= 10000; n += 4;}
    if (i >= 100){i /= 100; n += 2;}
    if (i >= 10){i /= 10; n += 1;}
    
    return n;

}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewControllers
{
    
    NSUInteger index=[[tabBarController viewControllers] indexOfObject:viewControllers];
    NSLog(@"Index of Tab =%d",(int)index);

    if (index != 3) {
        if (_ysTabBarControllerSelectdIndex == 3) {
            
        }
        NSLog(@"Activity Tab not Selected =%d",(int)index);
        
    }
    
    
    _ysTabBarControllerSelectdIndex =index;
    
  
    if (index ==2 ) {
        self.myPointView.alpha=0.0;
    }
    else
        self.myPointView.alpha=1.0;
    [self.ysTabBarController.tabBar setBackgroundImage:[UIImage imageNamed:[self iPhoneimagenamefromIndex:index]]];
}
//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
//    return viewController != tabBarController.selectedViewController;
//}
-(void)changeBackgroundOfTabBarForIndex : (NSUInteger) index

{
    [self.ysTabBarController.tabBar setBackgroundImage:[UIImage imageNamed:[self iPhoneimagenamefromIndex:index]]];
}
-(NSString*)iPhoneimagenamefromIndex:(NSUInteger)index
{
    NSString *imageame=@"";
    switch (index)
    {
        case 0:
        {
            if (iPhone6)
                
                return imageame = INNER_TAB_1_SELECTED_6;
           
            else if (iPhone5 || iPhone4)
                return imageame = INNER_TAB_1_SELECTED;

            else
                return imageame =INNER_TAB_1_SELECTED;
        }
            break;
        case 1:
        {
            if (iPhone6)
                
                return imageame = INNER_TAB_2_SELECTED_6;
            
            else if (iPhone5 || iPhone4)
                return imageame = INNER_TAB_2_SELECTED;
            
            else
                return imageame =INNER_TAB_2_SELECTED;
        }
            break;
        case 2:
        {
            if (iPhone6)
                
                return imageame = INNER_TAB_3_SELECTED_6;
            
            else if (iPhone5 || iPhone4)
                return imageame = INNER_TAB_3_SELECTED;
            
            else
                return imageame =INNER_TAB_3_SELECTED;
        }            break;
        case 3:
        {
            if (iPhone6)
                
                return imageame = INNER_TAB_4_SELECTED_6;
            
            else if (iPhone5 || iPhone4)
                return imageame = INNER_TAB_4_SELECTED;
            
            else
                return imageame =INNER_TAB_4_SELECTED;
        }            break;
        case 4:
        {
            if (iPhone6)
                
                return imageame = INNER_TAB_5_SELECTED_6;
            
            else if (iPhone5 || iPhone4)
                return imageame = INNER_TAB_5_SELECTED;
            
            else
                return imageame =INNER_TAB_5_SELECTED;
        }            break;
        default:
            break;
    }
    return imageame;
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    if (_ysTabBarControllerSelectdIndex == 3) {
        
        [self.refreshViewControllerDelegate refreshActivityViewController];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
    if ([GETVALUE(APPINSTALLEDFIRSTTIME) isEqualToString:@"1"]) {
        
        NSString *openTime = [NSString stringWithFormat:@"%d",[GETVALUE(@"APPOPENCOUNT") intValue]+1];
        [MSMixPanelTrackings trackAppOpen:openTime lastAppOpenDate:nil];
        SETVALUE(openTime,@"APPOPENCOUNT");
        SYNCHRONISE;
        
    }

    if ([GETVALUE(CEO_AccessToken) length]) {
        [self getCurrentActivityCountInfo];
    }
        [self checkForPushNotificationEnableorDisable];
    
    if (self.ysTabBarController.selectedIndex == 2 || self.ysTabBarController.selectedIndex == 0) {
        //BottomViewFrame
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BottomViewFrame" object:nil];
    }
    if (_ysTabBarControllerSelectdIndex == 3) {
        
        [NSTimer scheduledTimerWithTimeInterval:3.0f
                                         target:self
                                       selector:@selector(counterDidActive:)
                                       userInfo:nil
                                        repeats:NO];
        
        
    }
    
        [FBSDKAppEvents activateApp];
}
-(void)counterDidActive :(NSTimer *) time
{
    
    [self.refreshViewControllerDelegate refreshActivityViewController];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

}


#pragma mark -
-(void)onCreateToastMsgInWindow:(NSString *)strMsg
{

        if ([[self.window viewWithTag:123]superview])
        {
            [[self.window viewWithTag:123]removeFromSuperview];
        }
    
    UILabel *lblMessage        = [[UILabel alloc]initWithFrame:CGRectMake(0, -50, self.window.frame.size.width, 50)];
    lblMessage.backgroundColor = ORANGECOLOR;
    lblMessage.text            = strMsg;
    lblMessage.textColor       = [UIColor whiteColor];
    lblMessage.textAlignment   = NSTextAlignmentCenter;
    lblMessage.numberOfLines   = 3;
    lblMessage.font            = [UIFont fontWithName:@"OPENSANS" size:10];
    lblMessage.tag             = 123;
    [lblMessage setClipsToBounds:YES];
    [self.window addSubview:lblMessage];
    [self performSelector:@selector(showViewAlertText) withObject:nil afterDelay:0.0];
    
}

-(void)showViewAlertText
{
    
    [UIView animateWithDuration:0.8 animations:^{
        CGRect rect = [self.window viewWithTag:123].frame;
        rect.origin.y = rect.origin.y + [self.window viewWithTag:123].frame.size.height;
        [[self.window viewWithTag:123] setFrame:rect];
        [self performSelector:@selector(viewRemoveFromSuperView) withObject:nil afterDelay:2.0];
    }];
    
}

-(void)viewRemoveFromSuperView
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect rect = [self.window viewWithTag:123].frame;
        rect.origin.y = rect.origin.y - [self.window viewWithTag:123].frame.size.height;
        [[self.window viewWithTag:123] setFrame:rect];
        // [[self.window viewWithTag:123]removeFromSuperview];
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window.rootViewController.view setUserInteractionEnabled:YES];
        
    }];
}


#pragma mark -Deeplinking
- (BOOL)application:(UIApplication *)application  openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication  annotation:(id)annotation {
    if (url == nil)  return YES;
    NSString* URLstring = [url absoluteString];
    if ([URLstring rangeOfString:@"mintshowios"].location == NSNotFound) {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    
    /*
     The URL should contain something like:
     
     mintshowiOS://?type=0&id=123 ->mint
     mintshowiOS://?type=1&id=123 ->showroom
     mintshowiOS://?type=2&id=123 ->profile
     */
    
    // First, we'll extract the list of parameters from the URL

    NSArray *strURLParse = [URLstring componentsSeparatedByString:@"//"];
    if ([strURLParse count] == 2)  {
        // Then, we'll turn this list of parameters into an array of parameter names+values
        // eg: paramNamesAndValues = { "type=0", "id=123" ]
        NSString *paramNamesAndValuesString = [[strURLParse objectAtIndex:1] stringByReplacingOccurrencesOfString:@"?" withString:@""];
        NSArray *paramNamesAndValues = [paramNamesAndValuesString componentsSeparatedByString:@"&"];
        
        // Then, we'll split each into a Parameter name and it's value, and
        // see if one of the names is the one we're interested in.
        if (paramNamesAndValues.count == 4) {
            NSArray *paramTypeParts = [[paramNamesAndValues objectAtIndex:0]  componentsSeparatedByString:@"="];
            NSArray *paramIdParts   = [[paramNamesAndValues objectAtIndex:1]  componentsSeparatedByString:@"="];
            NSArray *paramUserNameParts   = [[paramNamesAndValues objectAtIndex:3]  componentsSeparatedByString:@"="];


            NSString* redirectType = (paramTypeParts.count == 2)?[paramTypeParts objectAtIndex:1]:@"";
            NSString* redirectID   = (paramIdParts.count == 2)?[paramIdParts objectAtIndex:1]:@"";
            NSString* userName   = (paramUserNameParts.count == 2)?[paramUserNameParts objectAtIndex:1]:@"";

            
            if ([GETVALUE(CEO_AccessToken) length]) {
                return [self RedirectToSpoecificScreenAfterLogin:redirectID andRedirectType:[redirectType integerValue]name:userName];
            }
        }
    }
    
//    [[SPHSingletonClass sharedSingletonClass] ste]  = @"yes";
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

-(BOOL)RedirectToSpoecificScreenBeforeLogin:(NSString*)redirectID andRedirectType:(NSInteger )type {
    
    switch (type) {
        case 0:
            NSLog(@"Open Multimint/ Mint details view with mint id =%@",redirectID);
            break;
        case 1:
            NSLog(@"Open Particular showroom  view with showroom id =%@",redirectID);
            break;
        case 2:
            NSLog(@"Open Profile view  view user id =%@",redirectID);
            break;
        default:
            NSLog(@"Please contact API team");
            break;
    }
    return YES;
}

-(BOOL)RedirectToSpoecificScreenAfterLogin:(NSString*)redirectID andRedirectType:(NSInteger )type name : (NSString *)userName {

    switch (type) {
        case 0:
            SETVALUE(redirectID, CEO_DEEPLINKINGINFO);
            SYNCHRONISE;
            [[NSNotificationCenter defaultCenter] postNotificationName:KSHOWSINGLEMINTDETAILS object:nil];
            NSLog(@"Open Multimint/ Mint details view with mint id =%@",redirectID);
            break;
        case 1:
            SETVALUE(redirectID, CEO_DEEPLINKINGINFO);
            SYNCHRONISE;
            SETVALUE([userName stringByReplacingOccurrencesOfString:@"%20" withString:@" "], CEO_DEEPLINKINGUSERNAME);
            SYNCHRONISE;
            [[NSNotificationCenter defaultCenter] postNotificationName:KSHOWSINGLEMINTDETAILS object:nil];
            NSLog(@"Open Particular showroom  view with showroom id =%@",redirectID);
            
            break;
        case 2:
            NSLog(@"Open Profile view  view user id =%@",redirectID);
            break;
        default:
            NSLog(@"Please contact API team");
            break;
    }
    return YES;
}
#pragma mark --------

-(void)updateDeviceInfo:(int)allowStatus{
    NSLog(@"accesstoken %@",GETVALUE(CEO_AccessToken));

    if (!GETVALUE(CEO_AccessToken)) {
        return;
    }
  NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&device_token=%@&device_id=%@&device_type=%@&allow_status=%d",GETVALUE(CEO_AccessToken),[self getDeviceTkenFromKeyChain],[self getDeviceID],@"iphone",allowStatus] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,@"users/update_device_details"]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         NSDictionary *update_device_details = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"update_device_details --- ---> %@",update_device_details);

     }];
}



-(void)updateSinletonValue:(NSString *)totalAccount you_count:(NSString *)you_count request_count:(NSString *)request_count
{
    SPHSingletonClass *singleTone        = [SPHSingletonClass sharedSingletonClass];
    singleTone.glblActivityTotal_count   = totalAccount;
    singleTone.glblActivityYou_count     = you_count;
    singleTone.glblActivityRequest_count = request_count;

}

-(void)getCurrentActivityCountInfo
{
    
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@",GETVALUE(CEO_AccessToken)];
    [self CallAPIFornUnreadActivity:[NSURL URLWithString:[NSString stringWithFormat:@"%@newsfeed/unread_activity_count?%@",ServerUrl,submitData]]];
}

//This method is for Checking how many Activity have been left to see yet in both (You/Request tab)
-(void)CallAPIFornUnreadActivity:(NSURL *)myUrl
{
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myUrl];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSDictionary *activity = [YSSupport dictionaryFromResponseData:data];
        NSLog(@"Activity ****** = %@",[activity valueForKey:@"Status"]);
        if ([[activity valueForKey:@"Status"]isEqualToString:@"Success"])  {
            
//            "total_count":0,"you_count":0,"request_count":0
            
            if ([activity valueForKey:@"total_count"]) {
                
                [self updateSinletonValue:[activity valueForKey:@"total_count"] you_count:[activity valueForKey:@"you_count"] request_count:[activity valueForKey:@"request_count"]];
                [self updateCounterValue:[[activity valueForKey:@"total_count"] integerValue]];
               // NSDictionary* notiUserInfo = @{@"activity_type": @"you"};

              /*  NSNotificationCenter* nc   = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"TestNotification" object:self userInfo:notiUserInfo];*/

                
            }
            
            
        }
    }];
    
}

//This method is for Checking how many Activity have been left to see yet in both (You/Request tab)
-(void)CallAPIFornDevice:(NSString *)deviceToken
{
  //  http://imarstech.com/Siba/chatapp/user/UpdateDevice.php?devicetoken=8d8289fa34710e200b340dbe96fbef7b16f467b1d8d574436a3185ce2fe91683&devicetype=IOS&deviceid=ABCXYZ&access=FAZJCXCAD9118WF17PII7D0DGBLVH7
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://imarstech.com/Siba/chatapp/user/UpdateDevice.php?devicetoken=%@&devicetype=IOS&deviceid=ABCXYZ&access=FAZJCXCAD9118WF17PII7D0DGBLVH7",deviceToken]]];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSDictionary *activity = [YSSupport dictionaryFromResponseData:data];
        NSLog(@"info ****** = %@",activity);
    }];
    
}

#pragma mark - More Mechanisms

-(void)createMoreSreen
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"MSMore" owner:nil options:nil];
    view = nibContents[0];
    view.frame = [UIScreen mainScreen].bounds;
    view.tag = 123456;

    UIButton *btnLogout = (UIButton *)[view viewWithTag:20];
    [btnLogout addTarget:self action:@selector(logoutClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *settingButton = (UIButton *)[view viewWithTag:10];
    [settingButton addTarget:self action:@selector(settingClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton *tapButton = (UIButton *)[view viewWithTag:30];
    [tapButton addTarget:self action:@selector(tapButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    tapButton.frame = [UIScreen mainScreen].bounds;

    
    [[UIApplication sharedApplication].keyWindow addSubview:view];
}
-(void)updateLogoutInfo{

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0 ;
    [self logoutComplete];
    
    SPHSingletonClass *objectShareClass = [SPHSingletonClass sharedSingletonClass];
    objectShareClass.glblActivityRequest_count = 0;
    objectShareClass.glblActivityTotal_count = 0;
    objectShareClass.glblActivityYou_count = 0;
    
    [YSSupport resetDefaultsLogout:NO];
    [UserAccessSession clearAllSession];
    
    AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appDell logOutDidFinish:appDell.ysTabBarController];

}
-(void)logoutClicked: (UIButton *)sender
{
    
    [view removeFromSuperview];
    view = nil;

    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Are you sure?"
                                  message:nil
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert.view setTintColor:[UIColor blackColor]];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Log out"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             AppDelegate *obj = MINTSHOWAPPDELEGATE
                             [obj updateLogoutInfo];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    
    
    UITabBarController *nvc = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController ;
    UINavigationController *vc = [[nvc viewControllers] objectAtIndex:0];
    [[[vc viewControllers] firstObject] presentViewController:alert animated:YES completion:nil];
    
}
-(void)settingClicked: (UIButton *)sender
{
    [view removeFromSuperview];
    view = nil;

    MSSettingVC *settingVC =  [[MSSettingVC alloc]initWithNibName:@"MSSettingVC" bundle:nil];
    //settingVC.hidesBottomBarWhenPushed = YES;
   // [settingVC.view pushViewController:settingVC animated:YES];

    
    UITabBarController *nvc = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController ;
    UINavigationController *vc = [[nvc viewControllers] objectAtIndex:0];
    [[[vc viewControllers] firstObject] presentViewController:settingVC animated:YES completion:nil];
    
    
}

-(void)tapButtonClicked: (UIButton *)sender
{
    
    [view removeFromSuperview];
    view = nil;

}


-(NSMutableArray *)getArray:(NSArray *)arrReq
{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    
    
    
    //Process one .. adding all array into a array
    
    NSArray *germanMakes = @[@"Mercedes-Benz", @"BMW", @"Porsche",
                             @"Opel", @"Volkswagen", @"Audi"];
    NSArray *ukMakes = @[@"Aston Martin", @"Lotus", @"Jaguar", @"Bentley"];
    NSArray *newArray = @[germanMakes,ukMakes];

    
    //Process 2 : for getting Max count of Indivisual array
    
    NSMutableArray *newArray1 = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<[newArray count]; i++) {
        [ newArray1 addObject:[NSNumber numberWithInteger:[[newArray objectAtIndex:i] count]]];
        
    }
    
    NSNumber * max = [newArray1 valueForKeyPath:@"@max.intValue"];
    NSLog(@"Max value is %@",max);
    

    //Process 3: Adding object into a final array
    
    for (int i = 0; i< [max intValue]; i++) {
        
        
        for (int j =0; j<[newArray count]; j++) {
            
            if (i<[[newArray objectAtIndex:j] count]) {
                
                [arr addObject:[[newArray objectAtIndex:j] objectAtIndex:i]];
            }
        }
    }
    NSLog(@"newformed array is %@",arr);
    return arr;
}

#pragma mark - SERVER call
- (void)logoutComplete
{
    
    AppDelegate *appDell = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&device_id=%@",GETVALUE(CEO_AccessToken),[appDell getDeviceID]] dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_USERLOGOUT]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"Status"] isEqualToString:@"Success"])
         {
             NSLog(@"%@",[showroomDict valueForKey:@"StatusMsg"]);
         }
         else
         {
             NSLog(@"%@",[showroomDict valueForKey:@"StatusMsg"]);
         }
     }];
}


//POSTUpdate viewed status for Mint Streak Popup - automatic trigger/successprofile/update_streak_popup_status
+ (void)updateStreakPopup
{
    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@",GETVALUE(CEO_AccessToken)] dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,@"successprofile/update_streak_popup_status"]]];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"submit data %@",submitDta123);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         NSDictionary *showroomDict = [YSSupport dictionaryFromResponseData:data];
         NSLog(@"%@",showroomDict);
         if ([[showroomDict valueForKey:@"Status"] isEqualToString:@"Success"])
         {
             NSLog(@"%@",[showroomDict valueForKey:@"StatusMsg"]);
         }
         else
         {
             NSLog(@"%@",[showroomDict valueForKey:@"StatusMsg"]);
         }
     }];
}

@end
