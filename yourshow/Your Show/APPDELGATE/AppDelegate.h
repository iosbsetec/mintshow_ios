//
//  AppDelegate.h
//  Your Show
//


#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "SPHKeyChain.h"

@protocol ReloadViewControllerDelegate <NSObject>

@optional
-(void)refreshActivityViewController;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIView   *myPointView;
@property (strong, nonatomic) UILabel  *lbl;
@property (strong, nonatomic) UITabBarController *ysTabBarController;
@property (strong, nonatomic) NSString *selectedIndex;
@property (strong, nonatomic) NSString *selectedMintIndexFor;
@property (strong, nonatomic) NSString *mintShowroomName;

@property (assign) NSInteger  showroomJoienedCount;
@property (assign, nonatomic) NSInteger selectedIndexForTrendingMint;
@property (nonatomic, assign) id <ReloadViewControllerDelegate> refreshViewControllerDelegate;

@property (assign, nonatomic) BOOL isCommentActionUpdate;
@property (assign, nonatomic) BOOL isAnyShowroomJoinUnjoin;
@property (assign, nonatomic) BOOL isShowroomDeleted;
@property (assign, nonatomic) BOOL isMintDetailsChanged;
@property (assign, nonatomic) BOOL isAddMintContainShowroom;
@property (assign, nonatomic) BOOL isAppFirstTimeLaunched;
@property (assign, nonatomic) BOOL isGlobalSearch;
@property (assign, nonatomic) BOOL isAnyUserMuted;

@property (assign) NSInteger  ysTabBarControllerSelectdIndex;

-(void)authenticationDidFinish:(UIViewController *)suslogin selectedInde:(NSInteger)selectdTabVal;
-(void)logOutDidFinish:(UITabBarController *)susTabbar;
-(void)changeBackgroundOfTabBarForIndex : (NSUInteger) index;
-(void)onCreateToastMsgInWindow:(NSString *)strMsg;
-(NSString*)getDeviceID;
-(NSString*)getDeviceTkenFromKeyChain ;
-(void)createStreakPopup:(NSString *)strikDays;

-(void)registerForRemoteNotification;
-(void)updateCounterValue :(NSInteger) totalCount;
-(void)updateSinletonValue:(NSString *)totalAccount you_count:(NSString *)you_count request_count:(NSString *)request_count;
-(void)updateDeviceInfo:(int)allowStatus;
-(void)createStreakCoachingTips:(NSString *)strikDays;
-(void)createMoreSreen;
-(void)createCelebrationPopup:(NSString *)mintCountDays withUsername:(NSString *)username userimageUrl:(NSString *)userimageUrl;
@end

