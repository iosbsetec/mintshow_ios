//
//  SignShowImageScrollingCellView.h
//  SignShowImageScrollingTableViewDemo
//

#import <UIKit/UIKit.h>
#import "DiscoverDataModel.h"

@class SignShowImageScrollingCellView;

@protocol SignShowImageScrollingViewDelegate <NSObject>

- (void)collectionView:(SignShowImageScrollingCellView *)collectionView  Withdata:(DiscoverDataModel*)discoverdata didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath;
-(void)didLoadNextPgeTriggered;

@optional
-(void)scollingCellStarElementTapped:(NSString *)selectedEnitity;
-(void)scollingCellAtmentionElementTapped:(NSString *)selectedEnitity;


@end


@interface SignShowImageScrollingCellView : UIView

@property (weak, nonatomic) id<SignShowImageScrollingViewDelegate> delegate;

- (void) setImageTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height;
- (void) setImageTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor;
- (void) setImageData:(NSArray*)collectionImageData;
- (void) setBackgroundColor:(UIColor*)color;

@end
