//
//  PPScrollingTableViewCell.m
//  PPImageScrollingTableViewControllerDemo
//
//  Created by popochess on 13/8/10.
//  Copyright (c) 2013年 popochess. All rights reserved.
//

#import "SignShowImageScrollingTableViewCell.h"
#import "SignShowImageScrollingCellView.h"
#import "MintShowroomViewController.h"

#define kScrollingViewHieght 120
#define kCategoryLabelWidth 200
#define kCategoryLabelHieght 30
#define kStartPointY 5

@interface SignShowImageScrollingTableViewCell() <SignShowImageScrollingViewDelegate>

@property (strong,nonatomic) UIColor *categoryTitleColor;
@property(strong, nonatomic) SignShowImageScrollingCellView *imageScrollingView;
@property (strong, nonatomic) NSString *categoryLabelText;

@end

@implementation SignShowImageScrollingTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        [self initialize];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initialize];
}

-(void)setCustomDelegate:(id)sender
{
    self.delegate =sender;
}
- (void)initialize
{
    // Set ScrollImageTableCellView
    CGRect boundsize = [[UIScreen mainScreen] bounds];
    NSLog(@"width= %f & heigt= %f", boundsize.size.width,boundsize.size.height);
    _imageScrollingView = [[SignShowImageScrollingCellView alloc] initWithFrame:CGRectMake(0., kStartPointY, boundsize.size.width, kScrollingViewHieght)];
    _imageScrollingView.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];    
    // Configure the view for the selected state
}

- (void)setImageData:(NSDictionary*)collectionImageData
{
    [_imageScrollingView setImageData:[collectionImageData objectForKey:@"images"]];
   
}

- (void)setCategoryLabelText:(NSString*)text withColor:(UIColor*)color{
    
    if ([self.contentView subviews]){
        for (UIView *subview in [self.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
   }
    
- (void) setImageTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height {

    [_imageScrollingView setImageTitleLabelWitdh:width withHeight:height];
}

- (void) setImageTitleTextColor:(UIColor *)textColor withBackgroundColor:(UIColor *)bgColor{

    [_imageScrollingView setImageTitleTextColor:textColor withBackgroundColor:bgColor];
}

- (void)setCollectionViewBackgroundColor:(UIColor *)color{
    
    _imageScrollingView.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
    [self.contentView addSubview:_imageScrollingView];
}

#pragma mark - PPImageScrollingViewDelegate

//- (void)collectionView:(PPImageScrollingCellView *)collectionView didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath {
// 
//        [self.delegate scrollingTableViewDidSelectImageAtIndexPath:indexPath atCategoryRowIndex:self.tag];
//}

- (void)collectionView:(SignShowImageScrollingCellView *)collectionView  Withdata:(DiscoverDataModel*)discoverdata didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath
{
    [self.delegate scrollingTableViewDidSelectImageAtIndexPath:indexPath Withdata:discoverdata atCategoryRowIndex:self.tag];
}

- (void)didLoadNextPgeTriggered{
//    if(!isLoading && !isDataFinishedInServer){
//        pagenumberShowroom = pagenumberShowroom+1;
//        [self getTrendingsShowroom:pagenumber];
//    }
    
    [self.delegate loadNextShowroomPages];
}


#pragma mark -CellScrolling delagates
-(void)scollingCellStarElementTapped:(NSString *)selectedEnitity
{

    [self.delegate gotoShowroomPageFromPPImageScrollingTableViewCell:selectedEnitity];
}
-(void)scollingCellAtmentionElementTapped:(NSString *)selectedEnitity
{
    [self.delegate gotoProfilePageFromPPImageScrollingTableViewCell:selectedEnitity];


}

@end
