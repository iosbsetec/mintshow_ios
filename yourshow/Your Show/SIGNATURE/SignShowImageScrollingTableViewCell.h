//
//  PPScrollingTableViewCell.h
//  PPImageScrollingTableViewControllerDemo
//
//  Created by popochess on 13/8/10.
//  Copyright (c) 2013年 popochess. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscoverDataModel.h"
@class SignShowImageScrollingTableViewCell;

@protocol SignShowImageScrollingTableViewCellDelegate <NSObject>

// Notifies the delegate when user click image
//- (void)scrollingTableViewDidSelectImageAtIndexPath:(NSIndexPath*)indexPathOfImage atCategoryRowIndex:(NSInteger)categoryRowIndex;
- (void)scrollingTableViewDidSelectImageAtIndexPath:(NSIndexPath*)indexPathOfImage Withdata:(DiscoverDataModel*)data atCategoryRowIndex:(NSInteger)categoryRowIndex;
-(void)loadNextShowroomPages;
-(void)gotoShowroomPageFromPPImageScrollingTableViewCell:(NSString *)selectedEnitity;
-(void)gotoProfilePageFromPPImageScrollingTableViewCell:(NSString *)selectedEnitity;


@end

@interface SignShowImageScrollingTableViewCell : UITableViewCell

@property (weak, nonatomic) id<SignShowImageScrollingTableViewCellDelegate> delegate;
@property (nonatomic) CGFloat height;

-(void)setCustomDelegate:(id)sender;
- (void) setImageData:(NSDictionary*) image;
- (void) setCollectionViewBackgroundColor:(UIColor*) color;
- (void) setCategoryLabelText:(NSString*)text withColor:(UIColor*)color;
- (void) setImageTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height;
- (void) setImageTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor;

@end