//
//  PPCollectionViewCell.m
//  PPImageScrollingTableViewControllerDemo

//

#import "SignShowCollectionViewCell.h"

@interface SignShowCollectionViewCell ()

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *imgMint;
@property (strong, nonatomic) UIImageView *imgSign;
@property (strong, nonatomic) UITextView *imageTitle;
@property (strong, nonatomic) UILabel    *mintCount;

@end

@implementation SignShowCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,5.0, 100, 100)];
        self.imageView.layer.cornerRadius = 4.0;
        self.imageView.clipsToBounds = YES;
        self.imageView.backgroundColor = [YSSupport randomColor];
        self.imageView.image =[UIImage imageNamed:@"imgPlaceholder"];
        
        self.imgSign = [[UIImageView alloc] initWithFrame:CGRectMake(80,130, 15, 15)];
        self.imgSign.image =[UIImage imageNamed:@"Signature_Icon.png"];
        
        self.imgMint = [[UIImageView alloc] initWithFrame:CGRectMake(5,130, 15, 15)];
        self.imgMint.image =[UIImage imageNamed:@"mint grey.png"];


    }
    return self;
}



-(void)setImage:(UIImage *)image
{
    self.imageView.image = image;
}

-(void)setImageUrl:(NSURL *)imageUrl {
    self.imageView.alpha = 0.0;
    self.imageView.image = nil;
    self.imageView.mintImageURL = imageUrl;
}


- (void)setImageTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height
{
    self.imageTitle = [[UITextView alloc] initWithFrame:CGRectMake(0,105, width,25)];
    self.imageTitle.textAlignment= NSTextAlignmentCenter;
    self.imageTitle.contentInset = UIEdgeInsetsMake(1,1,1,1);
    self.imageTitle.userInteractionEnabled = NO;
    
    self.mintCount = [[UILabel alloc] initWithFrame:CGRectMake(23,130, 60,15)];
    self.mintCount.textAlignment= NSTextAlignmentLeft;
}

- (void)setImageTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor
{
    self.imageTitle.textColor = [UIColor colorWithRed:(74/255.f) green:(74/255.f) blue:(74/255.f) alpha:1.0f];
    self.imageTitle.backgroundColor = [UIColor clearColor];
    
    self.mintCount.textColor = [UIColor lightGrayColor];
    self.mintCount.backgroundColor = [UIColor clearColor];
}

- (void)setTitle:(NSString*)title andMintCount:(NSString *)mintcount
{
    if ([self.contentView subviews]){
        for (UILabel *subview in [self.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    [self.contentView addSubview:self.imgMint];
    [self.contentView addSubview:self.imgSign];
    [self.contentView addSubview:self.imageView];
    self.imageTitle.font =  [UIFont fontWithName:@"HelveticaNeueLTStd-Md/65 Medium" size:12];
    self.imageTitle.text = title;
    [self.contentView addSubview:self.imageTitle];
    self.mintCount.text = mintcount;
    self.mintCount.font =  [UIFont fontWithName:@"Helvetica" size:10];
    [self.contentView addSubview:self.mintCount];
}



@end
