//
//  PPImageScrollingCellView.m
//  PPImageScrollingTableViewDemo
//
//  Created by popochess on 13/8/9.
//  Copyright (c) 2013年 popochess. All rights reserved.
//

#import "SignShowImageScrollingCellView.h"
#import "SignShowCollectionViewCell.h"
#import "DiscoverDataModel.h"
@interface  SignShowImageScrollingCellView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) SignShowCollectionViewCell *myCollectionViewCell;
@property (strong, nonatomic) UICollectionView *myCollectionView;
@property (strong, nonatomic) NSArray *collectionImageData;
@property (nonatomic) CGFloat imagetitleWidth;
@property (nonatomic) CGFloat imagetitleHeight;
@property (strong, nonatomic) UIColor *imageTilteBackgroundColor;
@property (strong, nonatomic) UIColor *imageTilteTextColor;


@end

@implementation SignShowImageScrollingCellView

- (id)initWithFrame:(CGRect)frame
{
     self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code

        /* Set flowLayout for CollectionView*/
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.itemSize = CGSizeMake(100.0, 150.0);
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 10, 5, 10);
        flowLayout.minimumLineSpacing = 10;
   
        /* Init and Set CollectionView */
        
        
        NSLog(@"%@",NSStringFromCGRect(self.bounds));
        self.myCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 320, 150) collectionViewLayout:flowLayout];
        self.myCollectionView.delegate = self;
        self.myCollectionView.dataSource = self;
        self.myCollectionView.showsHorizontalScrollIndicator = NO;

//        self.myCollectionView.layer.borderColor = [UIColor redColor].CGColor;
//        self.myCollectionView.layer.borderWidth = 2.0;
        
        [self.myCollectionView registerClass:[SignShowCollectionViewCell class] forCellWithReuseIdentifier:@"SignShowCollectionCell"];

         self.myCollectionView.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
        [self addSubview:_myCollectionView];
        
    }
    return self;
}

- (void) setImageData:(NSArray*)collectionImageData{

    _collectionImageData = collectionImageData;
    [_myCollectionView reloadData];
}

- (void) setBackgroundColor:(UIColor*)color{
    [_myCollectionView reloadData];
}

- (void) setImageTitleLabelWitdh:(CGFloat)width withHeight:(CGFloat)height{
    _imagetitleWidth = width;
    _imagetitleHeight = height;
}
- (void) setImageTitleTextColor:(UIColor*)textColor withBackgroundColor:(UIColor*)bgColor{
    
    _imageTilteTextColor = textColor;
    _imageTilteBackgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
//    _myCollectionView.contentInset =  UIEdgeInsetsMake(25, 5, 5, 10);
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.collectionImageData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DiscoverDataModel *model = [self.collectionImageData objectAtIndex:[indexPath row]];
    SignShowCollectionViewCell *cell = (SignShowCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"SignShowCollectionCell" forIndexPath:indexPath];
    
    [cell setImageTitleLabelWitdh:_imagetitleWidth withHeight:_imagetitleHeight];
    [cell setImageTitleTextColor:_imageTilteTextColor withBackgroundColor:_imageTilteBackgroundColor];
    [cell setTitle:model.discover_showroom_name andMintCount:model.mint_count];
    [cell setImageUrl:[NSURL URLWithString:model.discoverImg]];
    return cell;
}
-(void)gotoProfilePage:(NSString *)selectedEnitity index:(NSIndexPath *)indexPath
{
    DiscoverDataModel *model = [self.collectionImageData objectAtIndex:[indexPath row]];
    NSString *strUserId = [self checkUserNameExistWithName:[selectedEnitity substringFromIndex:1] model:model];
    [self.delegate scollingCellAtmentionElementTapped:strUserId];
}

-(NSString *)checkUserNameExistWithName:(NSString *)selectUserName model:(DiscoverDataModel *)disModel
{
    if(disModel.at_mentioned_users.count>0)
    {
        for (int i=0; i<disModel.at_mentioned_users.count; i++) {
            NSString *strName = [[disModel.at_mentioned_users objectAtIndex:i]valueForKey:@"name"];
            strName = [strName stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([[strName lowercaseString] isEqualToString:[selectUserName lowercaseString]]) {
                return [[disModel.at_mentioned_users objectAtIndex:i]valueForKey:@"id"];
            }
        }
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    [self.delegate collectionView:self didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath];
    
    DiscoverDataModel *model = [self.collectionImageData objectAtIndex:[indexPath row]];
    
    [self.delegate collectionView:self Withdata:model didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath];
}
-(void)onHashTagTapped:(NSIndexPath *)indexPath
{
    DiscoverDataModel *model = [self.collectionImageData objectAtIndex:[indexPath row]];
    [self.delegate collectionView:self Withdata:model didSelectImageItemAtIndexPath:(NSIndexPath*)indexPath];
}

-(void)gotoShowroomPage:(NSString *)selectedEnitity
{
    [self.delegate scollingCellStarElementTapped:selectedEnitity];
}

-(void)gotoProfilePage:(NSString *)selectedEnitity
{
    [self.delegate scollingCellAtmentionElementTapped:selectedEnitity];
}



#pragma mark CHECK IF NEED TO LOAD MORE

static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.89;

static BOOL ShouldLoadNextPage(UICollectionView *collectionView){
    CGFloat xOffset = collectionView.contentOffset.x;
    CGFloat weidth = collectionView.contentSize.width - CGRectGetWidth(collectionView.frame);
    return xOffset / weidth > kNewPageLoadScrollPercentageThreshold;
}

-(void)scrollViewDidEndDecelerating:(UICollectionView *)scrollView {
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self.myCollectionView);
    if (shouldLoadNextPage)  {
        [self.delegate didLoadNextPgeTriggered];
    }
}

@end
