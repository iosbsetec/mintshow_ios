//
//  ViewingMembersVC.h
//  Your Show
//
//  Created by bsetec on 12/21/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ViewingMembersDelegate
- (void)selectedFriendsUserId:(NSString *)userId;
@end


@interface ViewingMembersVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
@property (strong, nonatomic) NSString  *showroomName,*privacy_settings,*user_name,*is_user_joined;
@property (nonatomic, assign) id <ViewingMembersDelegate> delegate;
@end
