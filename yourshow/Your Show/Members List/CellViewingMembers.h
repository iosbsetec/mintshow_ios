//
//  CellViewingMembers.h
//  Your Show
//
//  Created by bsetec on 12/21/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemberList.h"
@interface CellViewingMembers : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgViewMember;
@property (strong, nonatomic) IBOutlet UILabel *lblMemberName;
@property (strong, nonatomic) IBOutlet UILabel *lblMemberStatus;

-(void)setMemberListData:(MemberList *)listMembers;
@end
