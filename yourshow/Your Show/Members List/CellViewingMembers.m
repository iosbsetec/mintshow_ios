//
//  CellViewingMembers.m
//  Your Show
//
//  Created by bsetec on 12/21/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "CellViewingMembers.h"

@implementation CellViewingMembers

- (void)awakeFromNib {
    self.imgViewMember.layer.cornerRadius  = self.imgViewMember.frame.size.width/2;
    [self.imgViewMember setClipsToBounds:YES];
    self.imgViewMember.layer.borderWidth = 1.5f;
    self.imgViewMember.layer.borderColor = [UIColor colorWithRed:135.0/255 green:135.0/255  blue:135.0/255  alpha:0.8].CGColor;
    
    // Initialization code
}

-(void)setMemberListData:(MemberList *)listMembers
{
    UIFont *font       =    [UIFont fontWithName:@"Helvetica" size:10];
    CGRect recttxtLbl  = [listMembers.member_name boundingRectWithSize:CGSizeMake(self.lblMemberName.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:10]} context:nil];
    ;
    CGRect rectlblMemberName = self.lblMemberName.frame;
    rectlblMemberName.size.height = recttxtLbl.size.height;
    [self.lblMemberName setFrame:rectlblMemberName];
    
    CGRect rectlblMemberStatus = self.lblMemberStatus.frame;
    rectlblMemberStatus.origin.y = self.lblMemberName.frame.origin.y + recttxtLbl.size.height-3;
    [self.lblMemberStatus setFrame:rectlblMemberStatus];
    
    self.lblMemberName.text       =    listMembers.member_name;
    self.lblMemberName.font       =    font;
    self.lblMemberStatus.font     =     font;
    self.imgViewMember.mintImageURL   =    [NSURL URLWithString:listMembers.member_img_url];
    
    if ([listMembers.show_option isEqualToString:@"owner"])
    {
        self.lblMemberStatus.text = @"Creator";
    }
    else  if ([listMembers.is_collaborator isEqualToString:@"Yes"])
    {
        self.lblMemberStatus.text = @"Colaborator";
    }
   else  if ([listMembers.follow_status isEqualToString:@"Yes"])
    {
        self.lblMemberStatus.text = @"IN CIRCLE";
    }
   else
       self.lblMemberStatus.text = @"";
}

@end
