//
//  MemberList.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 23/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "MemberList.h"

@implementation MemberList

-(MemberList *)initWithMembersListDict:(NSDictionary*)list

{
    self.member_name         =   [list valueForKey:@"user_name"];
    self.member_img_url      =   [list valueForKey:@"user_thumb_image"];
    self.showroomowner_id    =   [list valueForKey:@"showroom_owner"];
    self.user_id             =   [list valueForKey:@"user_id"];
    self.follow_status       =   [list valueForKey:@"follow_status"];
    self.show_option       =   [list valueForKey:@"show_option"];
    self.is_collaborator       =   [list valueForKey:@"is_collaborator"];

    return self;
}
@end
