//
//  MemberList.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 23/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MemberList : NSObject

@property (strong,nonatomic) NSString* member_name,*member_img_url,*showroomowner_id,*user_id,*follow_status,*show_option,*is_collaborator;

-(MemberList *)initWithMembersListDict:(NSDictionary*)list;

@end
