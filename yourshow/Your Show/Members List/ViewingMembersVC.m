//
//  ViewingMembersVC.m
//  Your Show
//
//  Created by bsetec on 12/21/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "ViewingMembersVC.h"
#import "UIViewController+CWPopup.h"
#import "YSAPICalls.h"
#import "CellViewingMembers.h"
#import "MemberList.h"
#import "MSProfileVC.h"
@interface ViewingMembersVC ()<UITextFieldDelegate>
{
    
    IBOutlet UICollectionView *cllctnViewViewMembers;
    IBOutlet UILabel *lblMembersCount;
    NSMutableArray  *arrMembersList;
    int  pagenumbers,RowId;
    BOOL scrollVal;
    BOOL isLoading;
    NSInteger currentpage,totalNoofpages;
    AppDelegate *delegateApp;
    
    
    IBOutlet UITextField *txtSearchFiled;
    NSString *searchKeyword;
    BOOL isSearching;
    
    IBOutlet UIButton *btnInvite;
}
@end

@implementation ViewingMembersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pagenumbers =  1;
    totalNoofpages = 0;
    scrollVal  =  NO;
    isLoading  =  NO;
    searchKeyword = @"";
    isSearching = NO;
    
    UINib *cellNib = [UINib nibWithNibName:@"CellViewingMembers" bundle:nil];
    [cllctnViewViewMembers registerNib:cellNib forCellWithReuseIdentifier:@"cellidentifier"];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [cllctnViewViewMembers setCollectionViewLayout:flowLayout];
     cllctnViewViewMembers.alwaysBounceVertical = YES;
    cllctnViewViewMembers.layer.cornerRadius = 4.0f;
    [cllctnViewViewMembers setClipsToBounds:YES];

     arrMembersList  = [NSMutableArray array];
    delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    if ([_privacy_settings isEqualToString:@"on" ] && ![_user_name isEqualToString: GETVALUE(CEO_UserName)])
    {
        btnInvite.hidden = YES;
        
    }
    
    else if([_is_user_joined isEqualToString:@"NO"])
    {
        btnInvite.hidden = YES;
        
    }
    

    
    
    
    
    [self getMembersListForParticularShowroom:pagenumbers searchText:searchKeyword];
}

-(IBAction)textFieldDidChange:(UITextField *)txtFld
{
    arrMembersList = [[NSMutableArray alloc]init];
    [cllctnViewViewMembers reloadData];

    [self getMembersListForParticularShowroom:pagenumbers searchText:txtFld.text];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrMembersList.count;
}

// The cell that is returned must be retrieved from a call to - dequeueReusableCellWithReuseIdentifier   :forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CellViewingMembers  *cell     =    [collectionView dequeueReusableCellWithReuseIdentifier:@"cellidentifier" forIndexPath:indexPath];
    MemberList *memberList             =    [arrMembersList objectAtIndex:indexPath.row];
    [cell setMemberListData:memberList];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        MemberList *memberList             =    [arrMembersList objectAtIndex:indexPath.row];
//        MSProfileVC *prof  = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
//        prof.profileUserId = memberList.user_id;
//        prof.hidesBottomBarWhenPushed = NO;
//        [self.navigationController pushViewController:prof animated:YES];
    [self.delegate selectedFriendsUserId:memberList.user_id];
    
 }
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(59, 85);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 20.0;  // right side space
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0; // bottom space
}

- (IBAction)dismissThisPopup:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CLosePopupNow" object:nil];
}
- (IBAction)backTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getMembersListForParticularShowroom:(int)pageNo searchText:(NSString *)text
{
    isLoading = YES;
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&showroom_tag=%@&search_text=%@&page_no=%@",GETVALUE(CEO_AccessToken),_showroomName,text,[NSString stringWithFormat:@"%d",pagenumbers]] dataUsingEncoding:NSUTF8StringEncoding];
    
    [YSAPICalls getMemberList:submitData ForSuccessionBlock:^(id newResponse)
    {
        isLoading  = NO;
        Generalmodel *gmodel = newResponse;
        if ([gmodel.status_code isEqualToString:@"1"]) {
            NSString *strTotal = gmodel.totalpages;
            totalNoofpages        =  [strTotal integerValue];
            lblMembersCount.text  = [NSString stringWithFormat:@"%@ Members",gmodel.member_count];
            if (gmodel.tempArray2.count)
            {
                [arrMembersList addObjectsFromArray:gmodel.tempArray2];
                [cllctnViewViewMembers reloadData];
            }
        }
        
        if (pagenumbers<totalNoofpages)
            [self getAllMembers];
    }
              andFailureBlock:^(NSError *error) {
        NSLog(@"error");
    }];
}

-(void)getAllMembers
{
    pagenumbers = pagenumbers +1;
    [self getMembersListForParticularShowroom:pagenumbers searchText:searchKeyword];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end