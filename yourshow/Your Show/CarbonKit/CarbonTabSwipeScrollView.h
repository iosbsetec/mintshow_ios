

#import <UIKit/UIKit.h>
#import "CarbonTabSwipeSegmentedControl.h"

NS_ASSUME_NONNULL_BEGIN

@interface CarbonTabSwipeScrollView : UIScrollView

/**
 *  Custimized segmented control without tint color and divider image
 */
@property (nonatomic, strong, readonly) CarbonTabSwipeSegmentedControl *carbonSegmentedControl;

/**
 *  Create CarbonTabScrollView with items. Items can be NSString or UIImage like
 *  creating a SegmentedControl. ScrollView content size is equal to
 *  SegmentedControl width.
 *
 *  @param items Array of segment titles or images
 *
 *  @return CarbonTabSwipeScrollView that contains CarbonTabSwipeSegmentedControl
 */
- (instancetype)initWithItems:(nullable NSArray *)items;

/**
 *  Add items to CarbonTabScrollView. Items can be NSString or UIImage like
 *  creating a SegmentedControl. ScrollView content size is equal to
 *  SegmentedControl width.
 *
 *  @param items Array of segment titles or images
 *
 *  @return CarbonTabSwipeScrollView that contains CarbonTabSwipeSegmentedControl
 */
- (void)setItems:(nullable NSArray *)items;

NS_ASSUME_NONNULL_END

@end
