//
//  CropImageVC.h
//  VPolor
//
//  Created by Vinson.D.Warm on 12/30/13.
//  Copyright (c) 2013 Huang Vinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CropImageVC;

@protocol CropImageDelegate <NSObject>

- (void)imageCropper:(CropImageVC *)cropperViewController didFinished:(UIImage *)editedImage;
- (void)imageCropperDidCancel:(CropImageVC *)cropperViewController;
- (void)imageCropperDidCancel:(CropImageVC *)cropperViewController btnTag:(UIButton *)btnTag;   //  changed delegate method


@end

@interface CropImageVC : UIViewController

@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, assign) id<CropImageDelegate> delegate;
@property (nonatomic, assign) CGRect cropFrame;

- (id)initWithImage:(UIImage *)originalImage cropFrame:(CGRect)cropFrame limitScaleRatio:(NSInteger)limitRatio;

@end
