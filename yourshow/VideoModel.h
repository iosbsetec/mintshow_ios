//
//  VideoModel.h
//  TableviewVideo
//
//  Created by Siba Prasad Hota on 17/10/15.
//  Copyright © 2015 Power Electronics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>


typedef enum {
    VideoCellTypeYoutube,
    VideoCellTypeCameraRoll,
    VideoCellTypeRemoteUrl,
    VideoCellTypeLocalPath
} VideoCellType;



@interface VideoModel : NSObject

@property(nonatomic,assign)     NSInteger        mintType;
@property(nonatomic,assign)     BOOL             isVideoPlaying;
@property (assign, nonatomic)   VideoCellType    vcellType;

@property(nonatomic,strong)     NSURL           *VideoUrl;
@property(nonatomic,strong)     UIImage         *thumbImage;
@property(nonatomic,strong)     NSURL           *thumbUrl;
@property(nonatomic,strong)     NSString        *videoDuration;
@property(nonatomic,strong)     NSString        *videoLocalUrlAsset;
@property(strong,nonatomic)     NSString        *mintDataStr;

@property(nonatomic,strong)     NSData          *dataGaleryVideo;

@property(nonatomic,strong)     ALAsset *result;

@end


