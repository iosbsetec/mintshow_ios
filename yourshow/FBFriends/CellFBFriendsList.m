//
//  CellFBFriendsList.m
//  Your Show
//
//  Created by bsetec on 12/15/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "CellFBFriendsList.h"

@implementation CellFBFriendsList

- (void)awakeFromNib {
    self.imgFBFriend.layer.cornerRadius = self.imgFBFriend.frame.size.width / 2;
    [self.imgFBFriend setClipsToBounds:YES];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
