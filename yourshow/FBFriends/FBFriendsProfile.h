//
//  FBFriendsProfile.h
//  Your Show
//
//  Created by bsetec on 2/6/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

@protocol FBProfileDelegate <NSObject>


@end


#import <UIKit/UIKit.h>

@interface FBFriendsProfile : UIView
@property(nonatomic,retain)id<FBProfileDelegate>delegate;
- (id)initWithFrame:(CGRect)frame
;

//-(void)showProfileViewWithFrame : (CGRect) frame;




@end
