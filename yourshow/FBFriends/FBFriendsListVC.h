//
//  FBFriendsListVC.h
//  Your Show
//
//  Created by bsetec on 12/15/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBFriendsListVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end
