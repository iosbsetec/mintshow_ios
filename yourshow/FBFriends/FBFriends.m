//
//  FBFriends.m
//  Your Show
//
//  Created by bsetec on 12/15/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "FBFriends.h"

@implementation FBFriends

-(id)initWithFriendInfo : (NSDictionary *) friendInfo
{
   
    self.friendId = [friendInfo valueForKey:@"id"];
    self.friendName = [friendInfo valueForKey:@"name"];
    self.friendPicture = [friendInfo valueForKeyPath:@"picture.data.url"];


    return self;
}
@end
