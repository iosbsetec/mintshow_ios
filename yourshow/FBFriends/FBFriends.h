//
//  FBFriends.h
//  Your Show
//
//  Created by bsetec on 12/15/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBFriends : NSObject
@property (nonatomic,strong) NSString *friendName;
@property (nonatomic,strong) NSString *friendId;
@property (nonatomic,strong) NSString *friendPicture;

-(id)initWithFriendInfo : (NSDictionary *) friendInfo
;
@end
