//
//  CellFBFriendsList.h
//  Your Show
//
//  Created by bsetec on 12/15/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellFBFriendsList : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgFBFriend;
@property (weak, nonatomic) IBOutlet UILabel *lblFBFriend;
@property (strong, nonatomic) IBOutlet UIButton *btnFollow;

@end
