//
//  FBFriendsProfile.m
//  Your Show
//
//  Created by bsetec on 2/6/16.
//  Copyright © 2016 Siba Prasad Hota. All rights reserved.
//

#import "FBFriendsProfile.h"
#import "FBFriends.h"


@implementation FBFriendsProfile
{
    NSMutableArray *friendslist;
    FBFriends *FBProfileImages;
    NSMutableArray  *arrFBFriends;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(0, 5, frame.size.width, 60)];
    if (self)
    {
        friendslist = [[NSMutableArray alloc]init];
        [self getfriendslist];
    }
    return self;
}

-(void)showProfileViewWithFriendsInfo:(NSMutableArray*)friendsArray
{
    CGFloat xAxis = 10;
    NSString *strProfileImage;
    int i = 0;
    int nWidthHeight = 50;
    for (strProfileImage in friendslist)
    {
        UIView *viewPhotoHolder = [[UIView alloc]init];
        UIImageView *imgViewProfile = [[UIImageView alloc]init] ;
        NSLog(@"xPos : %lu", i+(i%[friendslist count])*nWidthHeight);
        
        imgViewProfile.frame = CGRectMake(i+(i%[friendslist count])*55, 0, nWidthHeight, nWidthHeight);
        imgViewProfile.backgroundColor = [UIColor redColor];
        imgViewProfile.mintImageURL   = [NSURL URLWithString:strProfileImage];
        imgViewProfile.layer.cornerRadius = imgViewProfile.frame.size.width /2;
        [imgViewProfile setClipsToBounds:YES];
        
        UIButton *btnProfile          = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnProfile setFrame:imgViewProfile.frame];
        NSLog(@" btnProfile xPos : %f", btnProfile.frame.size.width);


        [btnProfile addTarget:self action:@selector(profileselected:) forControlEvents:UIControlEventTouchUpInside];
        [viewPhotoHolder addSubview:imgViewProfile];
        [viewPhotoHolder addSubview:btnProfile];
        viewPhotoHolder.frame = CGRectMake((self.frame.size.width-(friendslist.count*55))/2, 0, (friendslist.count*55), 50);
        
        viewPhotoHolder.layer.borderColor = [UIColor redColor].CGColor;
        viewPhotoHolder.layer.borderWidth = 2.0;

        
        [self addSubview:viewPhotoHolder];
        xAxis = xAxis + self.frame.size.width + 8;
        i++;
    }
}

-(void)profileselected:(UIButton*)sender
{
    NSLog(@"profile pic %ld tapped.", (long int)[sender tag]);

}


-(void)getfriendslist

{
    NSDictionary *limitDictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"200",@"limit", @"id,picture,name",@"fields", nil];
    
    
     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:limitDictionary tokenString:GETVALUE(CEO_FacebookAccessToken) version:nil HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             for (NSDictionary *dict in [result objectForKey:@"data"])
             {
                 FBProfileImages = [[FBFriends alloc]initWithFriendInfo:dict];
                 [friendslist addObject:FBProfileImages.friendPicture];
                
             }
             
             [self showProfileViewWithFriendsInfo:friendslist];
         }
         else
         {
             NSLog(@"error == %@",error.description);
         }
     }];
}




@end
