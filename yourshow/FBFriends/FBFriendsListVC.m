//
//  FBFriendsListVC.m
//  Your Show
//
//  Created by bsetec on 12/15/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "FBFriendsListVC.h"
#import "CellFBFriendsList.h"
#import "FBFriends.h"

@interface FBFriendsListVC ()
{
    
    __weak IBOutlet UITableView *tblViewFBFriends;
    NSMutableArray *friendslist;
   
}

@end

@implementation FBFriendsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    tblViewFBFriends.delegate  =self;
    tblViewFBFriends.dataSource = self;
    friendslist = [[NSMutableArray alloc]init];
    [self getfriendslist];
    tblViewFBFriends.separatorColor = [UIColor clearColor];
            
//            //TODO: process error or result.
//        }
//    }];
    
    
//    AppDelegate *appdelagate  = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [appdelagate.ysTabBarController .tabBar setHidden:YES];
//    appdelagate.lbl.hidden = YES;

//
    // Do any additional setup after loading the view from its nib.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return friendslist.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier = @"CellFBFriendsList";
    
    CellFBFriendsList *cell = (CellFBFriendsList*)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CellFBFriendsList" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.btnFollow.tag = indexPath.row;
   [ cell.btnFollow.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0]];
    FBFriends *friendsdetails = [friendslist objectAtIndex:indexPath.row];
    cell.lblFBFriend.text = friendsdetails.friendName;
    cell.imgFBFriend.mintImageURL = [NSURL URLWithString:friendsdetails.friendPicture];
    
    [cell.btnFollow addTarget:self action:@selector(btnFollowClicked:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48;
}


- (IBAction)CancelTapped:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CLosePopupNow" object:nil];
}

-(void)getfriendslist

{
    NSDictionary *limitDictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"200",@"limit", @"id,picture,name",@"fields", nil];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:limitDictionary HTTPMethod:@"GET"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             NSLog(@"friends are == %@",result);
            for (NSDictionary *dict in [result objectForKey:@"data"]) {
                
                FBFriends *obj = [[FBFriends alloc]initWithFriendInfo:dict];
                [friendslist addObject:obj];
                [tblViewFBFriends reloadData];

            }
             
         }

     }];

}


-(IBAction)btnFollowClicked:(id)sender{
//    NSIndexPath *path = [NSIndexPath indexPathForRow:sender.tag inSection:0];
//    CellFBFriendsList *cell = (CellFBFriendsList *)[tblViewFBFriends cellForRowAtIndexPath:sender];

    FBFriends *fbfriendslist = [friendslist objectAtIndex:[sender tag]];
    [YSAPICalls FBFriendfollowUnfollowMint:fbfriendslist withSuccessionBlock:^(id newResponse)  {
        Generalmodel *gModel = newResponse;
        if ([gModel.status_code isEqualToString:@"1"]) {
            if ([gModel.status_Msg isEqualToString:@"followed"])  {
                [sender setTitle:@"IN CIRCLE" forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"Checkmark.png"] forState:UIControlStateNormal];
                [sender setBackgroundColor:[UIColor whiteColor]];
                [sender setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
            } else  {
                
                [sender setTitle:@"FOLLOW" forState:UIControlStateNormal];
                [sender setBackgroundColor:ORANGECOLOR];
                [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [sender setImage:nil forState:UIControlStateNormal];
            }
        }
        else {
            NSLog(@"Failure Msg=%@",gModel.status_Msg);
        }
    } andFailureBlock:^(NSError *error){
        NSLog(@"Error = %@",error);
    }];
}



//-(void)getfriendslist
//{
//
//NSMutableArray *appFriendUsers = [[NSMutableArray alloc] init];
//NSDictionary *limitDictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"200",@"limit", nil];
//    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends?fields=installed" parameters:limitDictionary HTTPMethod:@"GET"]
//          startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//     
//     //if result, no errors
//     if (!error && result)
//     {
//        //result dictionary in key "data"
//         NSArray *allFriendsList = [result objectForKey:@"data"];
//         
//         if ([allFriendsList count] > 0)
//         {
//             // Loop
//             for (NSDictionary *aFriendData in allFriendsList) {
//                 // Friend installed app?
//                 if ([aFriendData objectForKey:@"installed"]) {
//                     
//                     [appFriendUsers addObject: [aFriendData objectForKey:@"id"]];
//                 }
//             }
//         }
//     }
//              
//              else
//              {
//                  NSLog(@"error == %@",error.description);
//              }
// }];
//    
//    
//    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
