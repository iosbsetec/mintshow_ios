//
//  RoundedRect.h
//  Your Show
//
//  Created by MANIKANDAN on 27/07/15.
//  Copyright (c) 2015 Mani kandan. All rights reserved.
//

#import <UIKit/UIKit.h>


// * Class defines progressive circle border
@interface StreakCircularProgress : UIView
{
    UIView *viewSupport;
}
@property (nonatomic) CGFloat progress;  // From 0 to 1

//Customization
@property (nonatomic) CGFloat progressLineWidth;
@property (nonatomic) UIColor *progressLineColor;
@property (nonatomic, strong) CAShapeLayer *circlePathLayer;

-(void)setGradiantColorForStreak:(CGFloat)progress withStreakColor:(NSString *)colorName withDays:(NSInteger)days;

@end
