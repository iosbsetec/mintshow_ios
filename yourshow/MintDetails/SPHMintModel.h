//
//  SPHMintModel.h
//  Demo
//
//  Created by Siba Prasad Hota  on 6/24/15.
//  Copyright (c) 2015 Sebastián Gómez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPHMintModel : NSObject

@property(nonatomic,strong)  NSString       *mintID;
@property(nonatomic,strong)  NSString       *mintTitle;
@property(nonatomic,strong)  NSString       *Mint_Description;
@property(nonatomic,strong)  NSString       *Mint_image;
@property(nonatomic,strong)  NSString       *Mint_video;
@property(nonatomic,strong)  NSString       *Mint_type;

@end