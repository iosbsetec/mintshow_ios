//
//  CommentsCell.m

//  Created by developer on 2/13/13.
//

#import "CommentsCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+animatedGIF.h"

@implementation CommentsCell

{
    CAGradientLayer *_gradientLayer;
    CGPoint _originalCenter;
    BOOL _deletedonDragfromPoint;
    
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _gradientLayer=[CAGradientLayer layer];
        
        _gradientLayer.frame=self.bounds;
        _gradientLayer.colors=@[(id)[[UIColor colorWithWhite:0.2f alpha:0.02f] CGColor],(id)[[UIColor colorWithWhite:0.01f alpha:0.1f] CGColor],(id)[[UIColor clearColor] CGColor],(id)[[UIColor colorWithWhite:0.1 alpha:0.2] CGColor]];
        _gradientLayer.locations=@[@"0.00f",@"0.01f",@"0.95f",@"1.00f"];
        [self.layer addSublayer:_gradientLayer];
        
        
        
        UIGestureRecognizer *_guestureRecogniser=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
        _guestureRecogniser.delegate=self;
        [self addGestureRecognizer:_guestureRecogniser];
        

    }
    return self;
}

-(void)setGestureForCell
{
    UIGestureRecognizer *_guestureRecogniser=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    _guestureRecogniser.delegate=self;
    [self addGestureRecognizer:_guestureRecogniser];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    _gradientLayer.frame=self.bounds;
    
}



-(void)setLayer
{

    
    self.imageUser.layer.cornerRadius = self.imageUser.frame.size.width / 2;
    self.imageUser.layer.masksToBounds = YES;
    [self.imageUser.layer  setBorderWidth:0.5f];
    [self.imageUser.layer  setBorderColor:[UIColor grayColor].CGColor];



}


#pragma mark - horizontal pan gesture methods
-(BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    
    if ([gestureRecognizer class] == [UIPanGestureRecognizer class]) {
        CGPoint translation = [gestureRecognizer translationInView:[self superview]];
        // Check for horizontal gesture
        if (fabs(translation.x) > fabs(translation.y)) {
            return YES;
        }
    }
   
    return NO;
}

-(void)handlePan:(UIPanGestureRecognizer *)recognizer {
    // 1
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // if the gesture has just started, record the current centre location
        _originalCenter = self.center;
    }
    
    // 2
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        // translate the center
        CGPoint translation = [recognizer translationInView:self];
        self.center = CGPointMake(_originalCenter.x + translation.x, _originalCenter.y);
        // determine whether the item has been dragged far enough to initiate a delete / complete
        _deletedonDragfromPoint = self.frame.origin.x < -self.frame.size.width / 2;
        
    }
    
    // 3
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        // the frame this cell would have had before being dragged
        CGRect originalFrame = CGRectMake(0, self.frame.origin.y,
                                          self.bounds.size.width, self.bounds.size.height);
        if (!_deletedonDragfromPoint) {
            // if the item is not being deleted, snap back to the original location
            [UIView animateWithDuration:0.2
                             animations:^{
                                 self.frame = originalFrame;
                             }
             ];
        }
    }
}

 -(void)showLoadMore
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"page_loader" withExtension:@"gif"];
    imgVLoader.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
}
@end
