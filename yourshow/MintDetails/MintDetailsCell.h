//
//  VideoTableViewCell.h
//  Demo
//
//  Created by JITEN on 6/29/15.
//  Copyright (c) 2015 Sebastián Gómez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "YSVideoPlayerView.h"
#import "STTweetLabel.h"
#import "YSCommentUserLable.h"
#import "CommentedUserModel.h"
#import "StreakCircleView.h"



@protocol MintDetailsCellDelegate <NSObject>
-(void)hashTagSelected : (NSString *)selectedEnitity;
-(void)selectdFeature :(NSString *)selectedEnitity  isPrivateShowroomAccess : (BOOL) access;
-(void)gotoProfileView:(NSString *)selectedEnitity;     // need to add
-(void)commentTextClickedwithComment:(NSInteger)selectedEnitity feedId:(NSInteger)feedId;     // need to add
-(void)createMintstreakInfo:(NSString *)days lblView:(UILabel *)lblStreak streakText:(NSString *)streakText isMintRemint:(BOOL)isMintRemint;
-(void)mintTypeSelecedFromHeader:(UITapGestureRecognizer *)guesture;
@end

@interface MintDetailsCell : UITableViewCell<ysPlayerViewDelegate>
{
    NSMutableArray *cmtArray;
}

@property (strong, nonatomic) YSVideoPlayerView     *videoPlayer;
@property (weak, nonatomic) IBOutlet UILabel        *lblLineLocation;


@property (weak, nonatomic) IBOutlet STTweetLabel   *descriptionView;
@property (weak, nonatomic) IBOutlet UIView         *viewComplimentSection;
@property (weak, nonatomic) IBOutlet UIView         *viewButtonComponent;
@property (weak, nonatomic) IBOutlet UIView         *viewRemint;
@property (weak, nonatomic) IBOutlet UIView         *viewMintPostedUser;

@property (weak, nonatomic) IBOutlet UILabel        *lblMintPostedLocation;
@property (weak, nonatomic) IBOutlet UILabel        *lblMintPostedUser;
@property (weak, nonatomic) IBOutlet UILabel        *lblMintPostedTime;

@property (weak, nonatomic)   IBOutlet UIButton     *btnMintPostedUser;
@property (weak, nonatomic)   IBOutlet UIButton     *btnAddcomplement;
@property (strong, nonatomic) IBOutlet UIButton     *btnHighfive;
@property (strong, nonatomic) IBOutlet UIButton     *btnRemint;
@property (strong, nonatomic) IBOutlet UIButton     *btnMore;
@property (weak, nonatomic)   IBOutlet UIButton     *btnCommmentCount;
@property (weak, nonatomic)   IBOutlet UIButton     *btnShare;
@property (weak, nonatomic) IBOutlet UIView         *viewStreakGray;
@property (nonatomic, weak) IBOutlet StreakCircleView *streakUserProfile;
@property (weak, nonatomic) IBOutlet UILabel        *lblStreakInfo;
@property (weak, nonatomic) IBOutlet UIImageView    *imgMintPostedUser;
@property (weak, nonatomic) IBOutlet UIImageView    *imgMintPostedLocation;
@property (weak, nonatomic) IBOutlet UIImageView  *video_cam_icon;


@property (strong, nonatomic) IBOutlet UIButton     *btnHighFiveCount;
@property (strong, nonatomic) IBOutlet UIButton     *btnRemintCount;


@property (strong, nonatomic) IBOutlet UIImageView  *mintThumbnailView;

@property (strong, nonatomic) IBOutlet UILabel      *lblRemintTime;
@property (weak, nonatomic)   IBOutlet UIImageView  *remintedUserImage;
@property (weak, nonatomic)     IBOutlet UILabel    *lblRemintedUserName;
@property (weak, nonatomic)     IBOutlet UILabel    *lblMintType;

@property (weak, nonatomic)     IBOutlet STTweetLabel *lblRemintMessage;
@property (nonatomic,assign) id <MintDetailsCellDelegate> aDelegate;


-(void)changeHifiveStatus: (BaseFeedModel *) modelData;
-(void)changeNominationStatus: (BaseFeedModel *) modelData;
-(void)changeRemintStatus: (BaseFeedModel *) modelData;
-(void)passValue : (NSMutableArray *)commentArray;
-(void)setInfoWithAlignment : (BaseFeedModel *) modelData index: (NSIndexPath *)indexPath isSingleRecord:(BOOL)isSingleRecord;
-(void)setPlayerInfoWithAlignment : (BaseFeedModel *) modelData index: (NSIndexPath *)indexPath;


@end

