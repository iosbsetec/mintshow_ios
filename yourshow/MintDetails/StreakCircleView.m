//
//  RoundedRect.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 27/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "StreakCircleView.h"

@implementation StreakCircleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder;
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit;
{
    CALayer *layer = self.layer;
//    layer.borderColor = [UIColor redColor].CGColor;
//    layer.borderWidth = 0.0;
    layer.cornerRadius  = self.frame.size.width/2;
    layer.masksToBounds = YES;
    
    self.circularProgressView = [[StreakCircularProgress alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _max = 7;
    self.circularProgressView.progressLineWidth = 5;
//    self.circularProgressViewForCode.progressLineColor = [UIColor blackColor];
//    self.circularProgressViewForCode.circleBackgroundColor = [UIColor yellowColor];

    [self addSubview:self.circularProgressView];
}

//-(id)setStreakColorWithDays:(int)days
//{
//    //    CGContextAddArc(context, centerX, centerY, radius, startAngleRadians, endAngleRadians, 1);
//    
//    if(days <= 7)
//    {
//        self.layer.borderColor = [UIColor orangeColor].CGColor;
//    }
//    else if(days > 7 && days <= 14)
//    {
//        days = days-7;
//        self.layer.borderColor = [UIColor colorWithRed:80.0/255.0 green:172.0/255.0 blue:89.0/255.0 alpha:1.0].CGColor;
//    }
//    else if(days > 14 && days <= 21)
//    {
//        days = days-14;
//        self.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0].CGColor;
//    }
//    else if(days > 21)
//    {
//        self.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0].CGColor;
//    }
//    
//    self.layer.borderWidth = 2.0;
//    return self;
//}

-(id)setStreakColorWithDays:(int)days
{
    CGFloat progress=0.0;
    NSString *strColorName;
    UIColor *color;
    if(days <= 7)
    {
        strColorName = @"orange";
        progress = days / self.max;
        color = ORANGECOLOR;
    }
    else if(days > 7 && days <= 14)
    {
        strColorName = @"green";
        progress = (days-7) / self.max;
        color = [UIColor colorWithRed:80.0/255.0 green:172.0/255.0 blue:89.0/255.0 alpha:1.0];
    }
    else if(days > 14 && days <= 21)
    {
        strColorName = @"blue";
        progress = (days-14) / self.max;
        color = [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0];
    }
    else if(days > 21)
    {
        strColorName = @"blue";
        progress = 1.0;
        color = [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0];
    }
    
//    self.circularProgressViewForCode.progress = progress;
    self.circularProgressView.progressLineColor = color;
//    if (progress!=1.0) {
        [self.circularProgressView setGradiantColorForStreak:progress withStreakColor:strColorName withDays:days];
//    }

    return self;
}

@end
