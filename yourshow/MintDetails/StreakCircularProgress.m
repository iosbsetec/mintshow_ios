//
//  RoundedRect.h
//  Your Show
//
//  Created by MANIKANDAN on 27/07/15.
//  Copyright (c) 2015 Mani kandan. All rights reserved.
//

#import "StreakCircularProgress.h"

//1. f8c100 - 248, 193, 0
//2. fb6e00 - 251, 110, 0
//3. d23400 - 210, 52, 0
//4. f35e1b - 243, 94, 27
//
//1. 63ed0c - 99, 237, 12
//2. 319811 - 49, 152, 17
//3. 146515 - 20, 101, 21
//
//1. 00affe - 0, 175, 254
//2. 004abf - 0, 74, 191
//3. 00168e - 0, 22, 142



#define DEGREES(radians) ((radians)/180*M_PI)
#define RADIANS(degree) ((degree)/180.0*M_PI)

#define ORANGEGRADIANTSTART [UIColor colorWithRed:248.0/255.0 green:193.0/255.0 blue:0.0/255.0 alpha:1.0]
#define ORANGEGRADIANTMIDDLE [UIColor colorWithRed:251.0/255.0 green:110.0/255.0 blue:0.0/255.0 alpha:1.0]
#define ORANGEGRADIANTEND [UIColor colorWithRed:210.0/255.0 green:52.0/255.0 blue:0.0/255.0 alpha:1.0]

#define ORANGE2DAY [UIColor colorWithRed:252/255.0 green:157/255.0 blue:0.0/255.0 alpha:1.0]
#define ORANGE3DAY [UIColor colorWithRed:253/255.0 green:113/255.0 blue:0.0/255.0 alpha:1.0]
#define ORANGE4DAY [UIColor colorWithRed:239/255.0 green:92/255.0 blue:0.0/255.0 alpha:1.0]
#define ORANGE5DAY [UIColor colorWithRed:222/255.0 green:69/255.0 blue:0.0/255.0 alpha:1.0]
#define ORANGE6DAY [UIColor colorWithRed:210/255.0 green:52/255.0 blue:0.0/255.0 alpha:1.0]

//2. fc9d00 - - 252, 157, 0
//3. fd7100 - - 253, 113, 0
//4. ef5c00 - - 239, 92,  0
//5. de4500 - - 222, 69,  0
//6. d23400 - - 210, 52,  0




#define GREENGRADIANTSTART [UIColor colorWithRed:99./255.0 green:237./255.0 blue:12.0/255. alpha:1.0]
#define GREENGRADIANTMIDDLE [UIColor colorWithRed:49./255.0 green:152./255.0 blue:17.0/255. alpha:1.0]
#define GREENGRADIANTEND [UIColor colorWithRed:20./255.0 green:101./255.0 blue:21.0/255. alpha:1.0]


#define BLUEGRADIANTSTART [UIColor colorWithRed:0.0/255.0 green:175.0/255.0 blue:254.0/255. alpha:1.0]
#define BLUEGRADIANTMIDDLE [UIColor colorWithRed:0.0/255.0 green:74.0/255.0 blue:191.0/255. alpha:1.0]
#define BLUEGRADIANTEND [UIColor colorWithRed:0.0/255.0 green:22./255.0 blue:142.0/255. alpha:1.0]


#define GREENCOLOR [UIColor colorWithRed:80.0/255.0 green:172.0/255.0 blue:89.0/255.0 alpha:1.0]
#define BLUECOLOR [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0]

@interface StreakCircularProgress ()

@end

@implementation StreakCircularProgress


#pragma mark - Getters/Setters

- (void)setProgressLineColor:(UIColor *)progressLineColor
{
    _progressLineColor = progressLineColor;
    self.circlePathLayer.strokeColor = self.progressLineColor.CGColor;
}

- (void)setProgressLineWidth:(CGFloat)progressLineWidth
{
    _progressLineWidth = progressLineWidth;
    self.circlePathLayer.lineWidth = self.progressLineWidth;
}


#pragma mark - LifeCycle


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    //Defaults
    self.progressLineWidth = 2.0;
    self.circlePathLayer = [CAShapeLayer layer];
    self.circlePathLayer.frame = self.bounds;
    self.circlePathLayer.lineWidth = self.progressLineWidth;
    self.circlePathLayer.fillColor = [UIColor clearColor].CGColor;
    self.circlePathLayer.strokeColor = [UIColor clearColor].CGColor;
    self.circlePathLayer.transform  = CATransform3DMakeRotation(RADIANS(270), 0, 0, 1);
    
    [self.layer addSublayer:self.circlePathLayer];
    
    self.backgroundColor = [UIColor clearColor];
    
    // * Start from the top
    self.layer.transform = CATransform3DMakeRotation(RADIANS(0), 0, 0, 1);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.circlePathLayer.path = [UIBezierPath bezierPathWithOvalInRect:self.bounds].CGPath;
}

#pragma mark - Events

- (void)setProgress:(CGFloat)progress
{
    if (progress > 1) {
        self.circlePathLayer.strokeEnd = 1;
    } else if (progress <= 0) {
        self.circlePathLayer.strokeEnd = 0;
    } else {
        self.circlePathLayer.strokeEnd = progress;
    }
    
    [self addCircle:progress];
}

-(void)setGradiantColorForStreak:(CGFloat)progress withStreakColor:(NSString *)colorName withDays:(NSInteger)days
{
    self.progress = progress;
    
    UIColor *color1;
    UIColor *color2;
    
    CAGradientLayer *_gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.circlePathLayer.bounds;
    _gradientLayer.startPoint = CGPointMake(0.0, progress);
    _gradientLayer.endPoint = CGPointMake(progress, 0.0);
    
    if ([colorName isEqualToString:@"orange"])
    {
        if (days==7)
        {
            color1 = ORANGECOLOR;
            color2 = ORANGECOLOR;
        }
        else
        {
            color1 = ORANGEGRADIANTSTART;
            color2 = ORANGEGRADIANTEND;
        }
    }
    else if ([colorName isEqualToString:@"green"])
    {
        if (days==14)
        {
            color1 = GREENCOLOR;
            color2 = GREENCOLOR;
        }
        else
        {
            color1 = GREENGRADIANTSTART;
            color2 = GREENGRADIANTEND;
        }
    }
    else if ([colorName isEqualToString:@"blue"])
    {
        if (days>21 || days==21)
        {
            color1 = BLUECOLOR;
            color2 = BLUECOLOR;
        }
        else
        {
            color1 = BLUEGRADIANTSTART;
            color2 = BLUEGRADIANTEND;
        }
    }
    
    _gradientLayer.colors = @[(id)color2.CGColor,(id)color1.CGColor,];
    
    self.circlePathLayer.fillColor = [UIColor clearColor].CGColor;
    
    for (CALayer *layer in self.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
    
    _gradientLayer.frame = self.layer.bounds;
    [self.layer addSublayer:_gradientLayer];
    _gradientLayer.mask = self.circlePathLayer;
}
- (CAGradientLayer*) greyGradient :(UIView*)view{
    
    UIColor *colorOne       = [UIColor yellowColor];
    UIColor *colorTwo       = ORANGE2DAY;
    UIColor *colorThree     = ORANGE3DAY;
    UIColor *colorFour      = ORANGE4DAY;
    UIColor *colorFive      = ORANGEGRADIANTEND;
    UIColor *colorSix       = ORANGEGRADIANTEND;
    
    NSArray *colors =  [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, colorThree.CGColor, colorFour.CGColor, colorFive.CGColor,colorSix.CGColor, nil];
    
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:0.20];
    NSNumber *stopThree = [NSNumber numberWithFloat:0.40];
    NSNumber *stopFour     = [NSNumber numberWithFloat:0.60];
    NSNumber *stopFive = [NSNumber numberWithFloat:0.60];
    NSNumber *stopSix = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, stopThree, stopFour,stopFive,stopSix, nil];
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.frame = view.layer.bounds;
    headerLayer.transform = CATransform3DMakeRotation(RADIANS(45), 0, 0, 1);
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    headerLayer.mask = self.circlePathLayer;
    return headerLayer;
    
}

- (void)addCircle:(CGFloat)progress
{
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = 1.0;
    drawAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    drawAnimation.toValue   = [NSNumber numberWithFloat:progress];
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.circlePathLayer addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
}

@end
