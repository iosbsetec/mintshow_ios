//
//  VideoTableViewCell.m
//  Demo
//
//  Created by JITEN on 6/29/15.
//  Copyright (c) 2015 Sebastián Gómez. All rights reserved.
//

#import "MintDetailsCell.h"
#import "AppDelegate.h"
#import "CommentsCell.h"
#import "CommentModel.h"
#import "HCYoutubeParser.h"
#import "NSString+Emojize.h"
#import "NSString+Emojize.h"
#import "STTweetUserName.h"
#import "MHFacebookImageViewer.h"
#define SIGNATURECOLOR                [UIColor colorWithRed:65./255.0 green:169./255.0 blue:226.0/255. alpha:1.0]

@implementation MintDetailsCell
{


    CGFloat videoYOrigin;

}


- (void)awakeFromNib {
    self.layer.shouldRasterize      = YES;
    self.video_cam_icon.alpha=0.0;
    self.layer.rasterizationScale   = [UIScreen mainScreen].scale;
    cmtArray                        = [[NSMutableArray alloc] init];
}



-(void)passValue : (NSMutableArray *)commentArray{
    cmtArray = commentArray;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
      
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
}

-(void)setInfoWithAlignment : (BaseFeedModel *) newModel index: (NSIndexPath *)indexPath isSingleRecord:(BOOL)isSingleRecord
{
    [_btnRemintCount setTitleColor:(newModel.is_user_reminted)?ORANGECOLOR:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_btnHighFiveCount setTitleColor:(newModel.is_user_liked)?ORANGECOLOR:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
        _btnShare.hidden         =  (newModel.is_private_showroom_mint ==  1)?NO:YES;

    _viewComplimentSection.tag =indexPath.section;
    _btnAddcomplement.tag = indexPath.section;
    _btnCommmentCount.tag = indexPath.section;
    _btnHighfive.tag      = indexPath.section;
    _btnShare.tag         = indexPath.section;
    _btnRemint.tag = indexPath.section;
    _btnMore.tag = indexPath.section;
    _btnMintPostedUser.tag = indexPath.section;
    
    _btnHighFiveCount.tag = indexPath.section;
    _btnRemintCount.tag = indexPath.section;

    
    [self setUIComponentView:newModel];
    
    
    CGFloat remintInfoHeight = 0.0;
    if ( newModel.IsMintReminted)
         remintInfoHeight= [self remintComponent:newModel];
    
    
    CGRect imageFrame                       = _mintThumbnailView.frame;
    
    if ( !(newModel.mintType==MSMintTypeText)) {
        CGSize rctSizeOriginal  = CGSizeMake(newModel.feed_thumb_Width , newModel.feed_thumb_height);
        double scale            = _mintThumbnailView.frame.size.width / rctSizeOriginal.width;
        CGSize rctSizeFinal     = CGSizeMake(rctSizeOriginal.width*scale,rctSizeOriginal.height*scale);
        imageFrame.size.height  = rctSizeFinal.height;
       // self.mintThumbnailView.mintImageURL = [NSURL URLWithString:newModel.feed_thumb_image_small];
        self.mintThumbnailView.newimageURL = [NSURL URLWithString:newModel.feed_thumb_image];
        [self.mintThumbnailView setupImageViewer];
        self.video_cam_icon.alpha=(!(newModel.mintType==MSMintTypeImage))?1.0:0.0;

    } else{
        imageFrame = CGRectZero;
        self.video_cam_icon.alpha=0.0;
    }
    if ([[newModel.feed_posted_location stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length])
    {
        [self setLocationText:newModel.feed_posted_location];
    }
    
    //if the feed content remint info then the y cordinate of the image/video/text will change
    imageFrame.origin.y = (remintInfoHeight>0.0)?remintInfoHeight:0.0;
    videoYOrigin = (remintInfoHeight>0.0)?remintInfoHeight:0.0;
    
    CGRect viddeoIconFrame   = _video_cam_icon.frame;
    viddeoIconFrame.origin.y = videoYOrigin;
    _video_cam_icon.frame = viddeoIconFrame;
    
    NSString *OriginalFrontendText    = ([newModel.feed_posted_edited_text isEqualToString:@"Edited"])?[NSString stringWithFormat:@"%@ Edited",[newModel.feed_description emojizedString]]:[newModel.feed_description emojizedString];
    
    UIFont *font                      = [UIFont fontWithName:@"HelveticaNeue" size:14];
    CGRect newRect                    = [OriginalFrontendText boundingRectWithSize:CGSizeMake(_descriptionView.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin  |NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil];
    
    _descriptionView.textAlignment = (newModel.mintType==MSMintTypeText)?NSTextAlignmentCenter:NSTextAlignmentLeft;
    
    int commentHeight = 0;
    if([newModel.feed_comment_count intValue] > 0){
        commentHeight = [self createCommnetInfo:newModel.commentsArray commentedUser:newModel.commentedUserArray withRowNumber:indexPath.section];
    }
    
    CGRect frameViewDescription     = _descriptionView.frame;
    frameViewDescription.origin.y   = imageFrame.origin.y+ imageFrame.size.height+5;
    frameViewDescription.size.height = ([OriginalFrontendText length])?newRect.size.height+5:5;
    
    
    int descHolderheight = 0;
    int viewPostMintHeightForRemint = (newModel.IsMintReminted)?48:0;
    int calTotalHeight = _viewButtonComponent.frame.size.height+newRect.size.height+commentHeight+remintInfoHeight+viewPostMintHeightForRemint;
    
    if(newModel.mintType==MSMintTypeText && isSingleRecord)
    {
        if(calTotalHeight<self.frame.size.height)
        {
            int getRemintHeight=0;
            if (newModel.IsMintReminted) {
                if (remintInfoHeight>_viewMintPostedUser.frame.size.height) {
                    getRemintHeight = remintInfoHeight-_viewMintPostedUser.frame.size.height;
                }
                else
                    getRemintHeight = _viewMintPostedUser.frame.size.height-remintInfoHeight;
            }
            if (isSmallPad) {
                descHolderheight = self.frame.size.height-70-_viewButtonComponent.frame.size.height-commentHeight-remintInfoHeight-viewPostMintHeightForRemint;
            }
            else
            descHolderheight = self.frame.size.height-_viewButtonComponent.frame.size.height-commentHeight-remintInfoHeight-viewPostMintHeightForRemint;
            UIView *viewDescHolder = [[UIView alloc]initWithFrame:CGRectMake(0, remintInfoHeight, self.frame.size.width,descHolderheight)];
            CGRect frameViewButton          = viewDescHolder.frame;
            NSLog(@"%@",NSStringFromCGRect(frameViewButton));
            frameViewDescription.origin.y   = (viewDescHolder.frame.size.height-frameViewDescription.size.height)/2;
            [self addSubview:viewDescHolder];
            [viewDescHolder addSubview:_descriptionView];
        }
    }
    
    _descriptionView.frame          = frameViewDescription;
    if (newModel.IsMintReminted)
    {
        if ((newModel.mintType==MSMintTypeText && isSingleRecord) && calTotalHeight<self.frame.size.height ) {
            [self setDataForMintPostedByUser:remintInfoHeight+descHolderheight feed:newModel];
        }
        else
            [self setDataForMintPostedByUser:_descriptionView.frame.origin.y+_descriptionView.frame.size.height feed:newModel];
    }
    
    CGRect frameViewButton          = _viewButtonComponent.frame;
    frameViewButton.origin.y        = (calTotalHeight<self.frame.size.height && newModel.mintType==MSMintTypeText && isSingleRecord)?descHolderheight+remintInfoHeight+((newModel.IsMintReminted)?50:0)-(([newModel.feed_posted_location length])?0:10):_descriptionView.frame.origin.y + _descriptionView.frame.size.height+viewPostMintHeightForRemint;
    frameViewButton.origin.y = ([newModel.feed_posted_location length])?frameViewButton.origin.y:frameViewButton.origin.y-10;

    _mintThumbnailView.hidden       = (newModel.mintType==MSMintTypeText)?YES:NO;
    _mintThumbnailView.frame        = imageFrame;
    _viewButtonComponent.frame      = frameViewButton;
    
    if([newModel.feed_comment_count intValue] > 0)  {
        CGRect frameViewcomplimintSection          = _viewComplimentSection.frame;
        frameViewcomplimintSection.origin.y        = frameViewButton.origin.y + frameViewButton.size.height;
        frameViewcomplimintSection.size.height     = commentHeight;
        
        _viewComplimentSection.frame               = frameViewcomplimintSection;
        
        CGRect frameCommnetCount                   = _btnCommmentCount.frame;
        frameCommnetCount.origin.y                 = frameViewcomplimintSection.origin.y + frameViewcomplimintSection.size.height;
        _btnCommmentCount.frame                    = frameCommnetCount;
    }
    
    [self setComponentButtons:newModel.mintType mintPostedUserId:newModel.feed_user_id isRiminted:newModel.IsMintReminted remintedUserId:newModel.remint_user_id];
    
    [_btnShare setHidden:(newModel.is_private_showroom_mint)?YES:NO];
}

-(void)setLocationText:(NSString *)locationText
{
    if (locationText.length)
    {
        locationText = [NSString stringWithFormat:@" %@",locationText];
        NSAttributedString *labelAttributes = [[NSAttributedString alloc] initWithString:locationText];
        
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
        textAttachment.image = [UIImage imageNamed:@"Location.png"];
        textAttachment.bounds = CGRectMake(0, -2.0, 7, 10);
        NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
        [attrStr appendAttributedString:attrStringWithImage];
        [attrStr appendAttributedString:labelAttributes];
        _lblMintPostedLocation.attributedText = attrStr;
    }
}

-(void)setStreakInfo:(BaseFeedModel *)feedData
{
    _viewStreakGray.layer.cornerRadius = _viewStreakGray.frame.size.width/2;
    _viewStreakGray.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _viewStreakGray.layer.borderWidth = 2.0;
    
    if([feedData.streak_days integerValue])
    {
        _viewMintPostedUser.tag = [feedData.feed_user_id integerValue];
        [self.aDelegate createMintstreakInfo:feedData.streak_days lblView:_lblStreakInfo streakText:[NSString stringWithFormat:@"%@ day Streak",feedData.streak_days]isMintRemint:YES];

        
    }
    
    _lblStreakInfo.hidden     = ([feedData.streak_days integerValue])?NO:YES;
    
    if([feedData.streak_days integerValue])
        [_streakUserProfile setStreakColorWithDays:(int)[feedData.streak_days integerValue]];
    else
        [_streakUserProfile setStreakColorWithDays:0];
}

-(void)setDataForMintPostedByUser:(float)yPos feed:(BaseFeedModel *)feedData
{
//    [self setStreakInfo:feedData];
    
    if (feedData.minttype_exists == 1) {
        UITapGestureRecognizer* gestureTimeCategory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mintTypeSeleced:)];
        [_lblMintPostedTime setUserInteractionEnabled:YES];
        [_lblMintPostedTime addGestureRecognizer:gestureTimeCategory];
    }
    
    NSMutableAttributedString *textTime =
    [[NSMutableAttributedString alloc]
     initWithString: [NSString stringWithFormat:@"%@ | %@",feedData.feed_posted_time,feedData.category_type]];

    [textTime addAttribute:NSForegroundColorAttributeName
                 value:SIGNATURECOLOR
                 range:NSMakeRange(feedData.feed_posted_time.length+3, feedData.category_type.length)];
    
    _imgMintPostedUser.layer.cornerRadius = _imgMintPostedUser.frame.size.width/2;
    _imgMintPostedUser.layer.masksToBounds = YES;
    _imgMintPostedUser.clipsToBounds = YES;
    _imgMintPostedUser.mintImageURL = [NSURL URLWithString:feedData.feed_user_thumb_image];
    _lblMintPostedTime.attributedText = textTime;
    _lblMintPostedLocation.text       = feedData.feed_posted_location;
    _imgMintPostedLocation.hidden   = ([feedData.feed_posted_location length])? NO :YES;

    NSMutableAttributedString * string      = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", feedData.feed_user_name]];
    [string addAttribute:NSForegroundColorAttributeName value:_lblMintPostedUser.textColor range:NSMakeRange(0,[feedData.feed_user_name length])];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0f] range:NSMakeRange(0,[feedData.feed_user_name length])];
    
    _lblMintPostedUser.attributedText       = string;

    _viewMintPostedUser.frame = CGRectMake(0, yPos, self.frame.size.width, _viewMintPostedUser.frame.size.height-6);
    [self.contentView addSubview:_viewMintPostedUser];
    
    
}
-(void)mintTypeSeleced:(UITapGestureRecognizer *)guesture
{
    [self.aDelegate mintTypeSelecedFromHeader:guesture];
}

-(IBAction)onViewAllMints:(id)sender{
    // go to comment screen;
}


#pragma mark SET CELL INFO

-(void)changeRemintStatus: (BaseFeedModel *) modelData{
    [_btnRemint setImage:[UIImage imageNamed:(modelData.is_user_reminted)?@"Icon_ReMint_Selected":@"Icon_ReMint"] forState:UIControlStateNormal];
    [_btnRemintCount setTitle:modelData.remint_count forState:UIControlStateNormal];
    [_btnRemintCount setTitleColor:(modelData.is_user_reminted)?ORANGECOLOR:[UIColor lightGrayColor] forState:UIControlStateNormal];

 
}
-(void)changeHifiveStatus: (BaseFeedModel *) modelData{
    [_btnHighfive setImage:[UIImage imageNamed:(modelData.is_user_liked)?@"HighFive_Icon_24x24":@"hive_it_normal"] forState:UIControlStateNormal];
    [_btnHighFiveCount setTitle:modelData.feed_like_count forState:UIControlStateNormal];
    [_btnHighFiveCount setTitleColor:modelData.is_user_liked?ORANGECOLOR:[UIColor lightGrayColor] forState:UIControlStateNormal];
}

-(void)setUIComponentView: (BaseFeedModel *)newModel {
    [_descriptionView setAttributeForfontTweetLabel:[UIFont fontWithName:@"HelveticaNeue" size:14] ];
    
    if (newModel.commentsArray.count == 0) {
        _btnCommmentCount.hidden = YES;
        _viewComplimentSection.hidden = YES;
    } else {
        _btnCommmentCount.hidden = NO;
        _viewComplimentSection.hidden = NO;
    }
    _descriptionView.userInteractionEnabled = NO;
    
    
    [_btnHighFiveCount setTitle:newModel.feed_like_count forState:UIControlStateNormal];
    [_btnHighfive setImage:[UIImage imageNamed:(newModel.is_user_liked)?@"HighFive_Icon_24x24":@"hive_it_normal"] forState:UIControlStateNormal];
    
    
    
    
    [_btnRemint setImage:[UIImage imageNamed:(newModel.is_user_reminted)?@"Icon_ReMint_Selected":@"Icon_ReMint"] forState:UIControlStateNormal];
    
    [_btnRemintCount setTitle:newModel.remint_count forState:UIControlStateNormal];
    [_btnRemintCount setTitleColor:(newModel.is_user_reminted)?[UIColor orangeColor]:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    [_descriptionView setText:([newModel.feed_posted_edited_text isEqualToString:@"Edited"])?[NSString stringWithFormat:@"%@ Edited",[newModel.feed_description emojizedString]]:[newModel.feed_description emojizedString]];
    
    _descriptionView.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
    {
        if (hotWord == STTweetHashtag)
        {
            [self.aDelegate hashTagSelected:string];
        }
        else if (hotWord == STTweetStartag)
        {
            BOOL isAccess = NO;
            for (NSDictionary *dictShowroomInfo in newModel.privateShowroomListArray) {
                if(!dictShowroomInfo || ![dictShowroomInfo isKindOfClass:[NSDictionary class]])
                    continue;
                if ([[dictShowroomInfo valueForKey:@"is_private"]  isEqualToString:@"yes"] && [[dictShowroomInfo valueForKey:@"name"]  isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                    isAccess = YES;
                    break;
                }
            }
            [self.aDelegate selectdFeature:string isPrivateShowroomAccess:isAccess];
        }
        else if(hotWord==STTweetMention)
        {
            NSString *strUserId = [self checkUserNameExistWithName:[string substringFromIndex:1] model:newModel];
            [self.aDelegate gotoProfileView:strUserId];
        }
        
    };

    if(newModel.commentsArray.count  == 1)
        [_btnCommmentCount setTitle: [NSString stringWithFormat:@"1 CompliMint"] forState: UIControlStateNormal];
    else
        
        [_btnCommmentCount setTitle: [NSString stringWithFormat:@"%@ CompliMints",newModel.feed_comment_count] forState: UIControlStateNormal];
    
}



-(CGFloat)remintComponent : (BaseFeedModel *)newModel {
    CGRect   recttxtLbl                  = [[newModel.remint_message emojizedString] boundingRectWithSize:CGSizeMake(self.frame.size.width-8   , MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:11]} context:nil];
    CGRect viewFrame   = CGRectMake(0, 0, self.frame.size.width, 32);
    viewFrame.size.height = CGRectGetHeight(recttxtLbl)+14;
    _lblRemintMessage.frame  = CGRectMake(_lblRemintMessage.frame.origin.x, 7, self.frame.size.width-8, recttxtLbl.size.height);
    _lblRemintMessage.text  =  [newModel.remint_message emojizedString] ;
    _lblRemintMessage.font = [UIFont fontWithName:@"Helvetica" size:11];
    _viewRemint.frame = CGRectMake(0, 0, self.frame.size.width, viewFrame.size.height);
    
    
    _lblRemintMessage.detectionBlock = ^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range)
    {
        
        NSLog(@"string is %@",string);
        if (hotWord == STTweetHashtag)
        {
            [self.aDelegate hashTagSelected:string];
        
        }
        else if (hotWord == STTweetStartag)
        {
            BOOL isAccess = NO;
            
            for (NSDictionary *dictShowroomInfo in newModel.privateShowroomListArray) {
                if(!dictShowroomInfo || ![dictShowroomInfo isKindOfClass:[NSDictionary class]])
                    continue;
                if ([[dictShowroomInfo valueForKey:@"is_private"] isEqualToString:@"yes"] && [[dictShowroomInfo valueForKey:@"name"]isEqualToString:[string stringByReplacingOccurrencesOfString:@"*" withString:@""]]) {
                     isAccess = YES;
                     break;
                   }
            }
            [self.aDelegate selectdFeature:string isPrivateShowroomAccess:isAccess];

        }
        
        else if(hotWord==STTweetMention)
        {
            NSString *strUserId = [self checkRemintUserNameExistWithName:[string substringFromIndex:1]model:newModel];
            [self.aDelegate gotoProfileView:strUserId];
        }

        

    };
    

    
    [self.contentView addSubview:_viewRemint];
    return viewFrame.size.height;
}

#pragma mark CREATE COMMENT INFO
// ***********************************************

-(float)createCommnetInfo:(NSMutableArray *)arrayComment{
    
    NSInteger commentCount = (arrayComment.count>3)?3:arrayComment.count;
    CGFloat yCordinate = 0.0;
    for (id subview in [_viewComplimentSection subviews]) {
        [subview removeFromSuperview];
    }
    for (int i = 0; i<commentCount; i++) {
        NSString *finalText = [NSString stringWithFormat:@"%@ %@",[[arrayComment objectAtIndex:i]  achieveCommentUname],[[[arrayComment objectAtIndex:i]  achieveCommentText] emojizedString]] ;
        CGFloat commentHeight = [YSSupport getCommentTextHeight:finalText font:12.0];
        
        STTweetUserName *commentLbl   = [[STTweetUserName alloc]initWithFrame:CGRectMake(0, yCordinate+2, self.frame.size.width-10, commentHeight+2) withFont:12.0 attributeTextColor:[UIColor darkGrayColor] name:[[arrayComment objectAtIndex:i]  achieveCommentUname]];
        commentLbl.text              = [finalText emojizedString];
        
        commentLbl.detectionBlock = ^(STTweetHotUserName hotWord, NSString *string, NSString *protocol, NSRange range)
        {
            
            NSLog(@"string is %@",string);
        };
        [_viewComplimentSection addSubview:commentLbl];
        yCordinate = yCordinate + commentLbl.frame.size.height;
        
        
    }
    
    return yCordinate;
}




-(float)createCommnetInfo:(NSMutableArray *)arrayComment commentedUser:(NSMutableArray *)arrCommentedUser  withRowNumber:(NSInteger)rowNumber  //  need to change method name
{
    
    NSInteger commentCount = (arrayComment.count>3)?3:arrayComment.count;
    CGFloat yCordinate = 0.0;
    for (id subview in [_viewComplimentSection subviews]) {
        [subview removeFromSuperview];
    }
    for (int i = 0; i<commentCount; i++)
    {
        CommentModel *model = [arrayComment objectAtIndex:i];
        
        if ([model.achieveCommentUserId isEqualToString:GETVALUE(CEO_UserId)]) {
            model.achieveCommentUname = [NSString stringWithFormat:@"%@ %@",GETVALUE(CEO_UserFirstName),GETVALUE(CEO_UserLastName)];
        }
        
        NSString *finalText = [NSString stringWithFormat:@"%@ %@",model.achieveCommentUname,[[model achieveCommentText] emojizedString]] ;
        CGFloat commentHeight = [YSSupport getCommentTextHeight:finalText font:12.0];
        
        STTweetUserName *commentLbl   = [[STTweetUserName alloc]initWithFrame:CGRectMake(0, yCordinate+2, self.frame.size.width-10, commentHeight+2) withFont:12.0 attributeTextColor:[UIColor darkGrayColor] name:[[arrayComment objectAtIndex:i]  achieveCommentUname]];
        commentLbl.text              = [finalText emojizedString];
        [_viewComplimentSection addSubview:commentLbl];
        commentLbl.layer.borderColor = [UIColor clearColor].CGColor;
        yCordinate = yCordinate + commentLbl.frame.size.height;
        commentLbl.commentId = [[model achieveCommentId] integerValue];
        commentLbl.tag = [[model achieveCommentUserId] integerValue];

        __weak STTweetUserName *lblTempCommentLbl = commentLbl;
        
        commentLbl.detectionBlock = ^(STTweetHotUserName hotWord, NSString *string, NSString *protocol, NSRange range)
        {
            if(hotWord==STTweetUserNameTag)
            {
                [self.aDelegate gotoProfileView:[NSString stringWithFormat:@"%d",(int)lblTempCommentLbl.tag]];
            }
            else
            {
                [self.aDelegate commentTextClickedwithComment:lblTempCommentLbl.commentId feedId:_viewComplimentSection.tag];
            }
            
        };
    }
    
    return yCordinate;
}



//http://clips.vorwaerts-gmbh.de/VfE_html5.mp4
//http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4

-(void)setPlayerInfoWithAlignment : (BaseFeedModel *) modelData index: (NSIndexPath *)indexPath{
    CGSize rctSizeOriginal  = CGSizeMake(modelData.feed_thumb_Width , modelData.feed_thumb_height);
    double scale            = _mintThumbnailView.frame.size.width / rctSizeOriginal.width;
    CGSize rctSizeFinal     = CGSizeMake(rctSizeOriginal.width*scale,rctSizeOriginal.height*scale);

    if (modelData.mintType == MSMintTypeYouTube) {
        // Its a you tube mint
        [self playYoutubeVideoWithFeeddata:modelData andHeight:rctSizeFinal.height];
    }else if (modelData.mintType == MSMintTypeVideo){
        [self setUpPlayerToPlay:[NSString stringWithFormat:@"%@/%@.mp4",VIDEO_DIRECT_URL,modelData.media_id] andHeight:rctSizeFinal.height];
    }else if (modelData.mintType == MSMintTypeInsthaGram){
        [self setUpPlayerToPlay:modelData.app_connection_url andHeight:rctSizeFinal.height];
    }
}


-(void)playYoutubeVideoWithFeeddata:(BaseFeedModel *)modelData andHeight:(CGFloat)newheight{
    
    [self setupYouTubeUrlForModel:modelData withSuccessionBlock:^(NSString *fileUrl)  {
            [self setUpPlayerToPlay:fileUrl andHeight:newheight];
    } andFailureBlock:^(NSString *error) {
            //[self playYoutubeVideoWithFeeddata:modelData andHeight:newheight];
    }];
}



#pragma mark INITIALIZE THE PLAYER
// ***********************************************
-(void)setUpPlayerToPlay:(NSString*)fileUrl andHeight:(CGFloat)newheight{
    self.videoPlayer = [[YSVideoPlayerView alloc] initWithFrame:CGRectMake(0,videoYOrigin, self.frame.size.width, newheight) contentURL:[NSURL URLWithString:fileUrl] AndwithSuccessionBlock:^(id response) {
        self.videoPlayer.ysdelegate = self;
        [self.videoPlayer setClipsToBounds:YES];
        [self.contentView addSubview:self.videoPlayer];
        self.mintThumbnailView.alpha=1.0;
        //self.mintThumbnailView.backgroundColor=[UIColor blackColor];
        //self.mintThumbnailView.image = nil;

        // Hide or show Controls
        // ***********************************************
        [self.videoPlayer setCloseButtonHidden:YES];
        [self.videoPlayer setPlayButtonHidden:NO];
        [self.videoPlayer setSmallVolumeButtonHidden:NO];
        [self.videoPlayer setVolumeMute:YES];
        [self.video_cam_icon setAlpha:0.0];
        // [self.contentView bringSubviewToFront:_imgVAchivement];
    }];

}

-(void)ysPlayerFinishedPlayback:(YSVideoPlayerView*)view
{
    [self.videoPlayer play];
}

-(void)setupYouTubeUrlForModel:(BaseFeedModel *)modeldata  withSuccessionBlock:(void(^)(NSString *fileUrl))successBlock  andFailureBlock:(void(^)(NSString *error))failureBlock{
    NSURL *youtubeUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",modeldata.youtube_id]];
    [HCYoutubeParser h264videosWithYoutubeURL:youtubeUrl completeBlock:^(NSDictionary *videoDictionary, NSError *error){
        NSDictionary *qualities = videoDictionary;
        if ([qualities objectForKey:@"small"] != nil) {
            successBlock ([qualities objectForKey:@"small"]);
        } else if ([qualities objectForKey:@"live"] != nil)  {
            successBlock ([qualities objectForKey:@"live"]);
        }  else {
            failureBlock(@"This YouTube video is restricted to watch out side you tube.");
            return;
        }
    }];

}

-(void)setComponentButtons : (MintType)achiveMnetType mintPostedUserId: (NSString *)userId isRiminted:(NSInteger) remintStatus remintedUserId : (NSString *) remintedUserId{

    NSLog(@"logeed in user id %@",GETVALUE(CEO_UserId));
    if ([userId isEqualToString:GETVALUE(CEO_UserId)]  || [remintedUserId isEqualToString:GETVALUE(CEO_UserId)] ){
        _btnRemint.hidden      = YES;
        _btnRemintCount.hidden = YES;
    }
    
    CGRect btnHighfiveFrame = _btnHighfive.frame;
    CGRect btnRemintFrame   = _btnRemint.frame;
    
    if (achiveMnetType == MSMintTypeText) {
        
        CGRect btnHighfiveCountFrame   = _btnHighFiveCount.frame;
        btnHighfiveCountFrame.origin.x = btnHighfiveFrame.origin.x+btnHighfiveFrame.size.width+3;
        _btnHighFiveCount.frame        = btnHighfiveCountFrame;
        
        if (( [userId isEqualToString:GETVALUE(CEO_UserId)]  || [remintedUserId isEqualToString:GETVALUE(CEO_UserId)])) {
            btnHighfiveFrame.origin.x      = btnRemintFrame.origin.x;
            _btnHighfive.frame             = btnHighfiveFrame;
            
            CGRect btnHighfiveCountFrame   = _btnHighFiveCount.frame;
            btnHighfiveCountFrame.origin.x = btnHighfiveFrame.origin.x+btnHighfiveFrame.size.width+3;
            _btnHighFiveCount.frame        = btnHighfiveCountFrame;
        }

    }else if( [userId isEqualToString:GETVALUE(CEO_UserId)]  || [remintedUserId isEqualToString:GETVALUE(CEO_UserId)]){
        btnHighfiveFrame.origin.x = btnRemintFrame.origin.x;
        _btnHighfive.frame        = btnHighfiveFrame;
        
        CGRect btnHighfiveCountFrame   = _btnHighFiveCount.frame;
        btnHighfiveCountFrame.origin.x = btnHighfiveFrame.origin.x+btnHighfiveFrame.size.width+3;
        _btnHighFiveCount.frame        = btnHighfiveCountFrame;
 
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    if(!newSuperview && self.videoPlayer) {
        DLog(@"timer invalidated");
        [self.videoPlayer stop];
        //[self.videoPlayer removeFromSuperview];
       //  self.videoPlayer = nil;
    }
}

-(NSString *)checkUserNameExistWithName:(NSString *)selectUserName model:(BaseFeedModel *)fmodel
{
    NSLog(@"%@",fmodel.atMentionedArray);
    if(fmodel.atMentionedArray.count>0)
    {
        for (int i=0; i<fmodel.atMentionedArray.count; i++) {
            NSString *strName = [[fmodel.atMentionedArray objectAtIndex:i]valueForKey:@"name"];
            strName = [strName stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([[strName lowercaseString] isEqualToString:[selectUserName lowercaseString]]) {
                //                NSLog(@"%@",[[fmodel.atMentionedArray objectAtIndex:i]valueForKey:@"id"]);
                return [[fmodel.atMentionedArray objectAtIndex:i]valueForKey:@"id"];
            }
        }
    }
    return nil;
}
-(NSString *)checkRemintUserNameExistWithName:(NSString *)selectUserName model:(BaseFeedModel *)fmodel
{
    NSLog(@"%@",fmodel.remintAtMentionedArray);
    if(fmodel.remintAtMentionedArray.count>0)
    {
        for (int i=0; i<fmodel.remintAtMentionedArray.count; i++) {
            NSString *strName = [[fmodel.remintAtMentionedArray objectAtIndex:i]valueForKey:@"name"];
            strName = [strName stringByReplacingOccurrencesOfString:@" " withString:@""];
            if ([[strName lowercaseString] isEqualToString:[selectUserName lowercaseString]]) {
                return [[fmodel.remintAtMentionedArray objectAtIndex:i]valueForKey:@"id"];
            }
        }
    }
    return nil;
}

@end

