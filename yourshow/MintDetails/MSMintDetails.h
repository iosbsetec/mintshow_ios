//
//  MSMintDetails.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 26/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>



@protocol MSMintDetailsDelegate <NSObject>

// Display customization

-(void)highFiveMintOfmintDetails :(NSString *)mintId andCurerntValue : (NSString *)activityValue countValue: (NSString *)value;
-(void)nominateMintOfmintDetails;
-(void)remintMintOfmintDetails:(NSString *)mintId;
-(void)addCommentToMintOfmintDetails : (NSInteger)recordNumber CounterValue : (NSInteger)counter;
-(void)deleteMintOfmintDetails:(NSString *)mintId;
-(void)muteMintOfmintDetails : (NSString *)mintId;
-(void)muteUserOfmintDetails:(NSString *)muteUser;
-(void)editRemintDetails : (NSString *)strMessage rowId:(NSInteger)rowId;

//For Mute mint, Delete Mint. FlagInappropriate
-(void)updateThePreviouseScreenForMintManagament:(NSString *)mintId;



@optional
-(void)updateMintCountForShowroom:(NSString *)mintId;

@end

@interface MSMintDetails : UIViewController

@property (retain, nonatomic) NSMutableArray *mintArrayDetails;
@property (retain, nonatomic) NSMutableArray *profileMintArrayDetails;
@property (retain, nonatomic) NSString *mint_row_id,*strShowroomImageUrl;
@property (retain, nonatomic) NSString *mint_id,*profileuser_id;
@property (assign, nonatomic) NSInteger Selected_row_value;
@property (assign, nonatomic) NSInteger Selected_row_valueTrendingMints;

@property (nonatomic,assign) id   <MSMintDetailsDelegate> delegate;
@property (nonatomic,assign) BOOL isToshowSingleRecord;
@property (nonatomic,assign) BOOL isFromParticularShowroom,isFromDiscoverTrending;
@property (nonatomic,assign) BOOL isFromProfile;
@property (nonatomic,assign) BOOL isMintOwner;
@property (nonatomic,assign) BOOL isFromMyshow;
@property (nonatomic,assign) BOOL isFromQueryResultPage;
@property (nonatomic,assign) BOOL isFromActivity;
@property (nonatomic,assign) BOOL isFromSignatureShowroom;

@property (nonatomic,strong) NSString *selectedProfileUserName, *selectedProfileUserImage;

@property (nonatomic, strong) IBOutlet UIButton *btnJoined;



@property (nonatomic,assign) BOOL isFromTrendingTags;
//header text is for Trending tag selected MInt or Particular showroom Name
@property (retain, nonatomic) NSString *navigationHeaderText;
@property (retain, nonatomic) NSString *showtagText;
@property (retain, nonatomic) NSString *globalSearchText;

@end
