//
//  MSShareVC.h
//  Your Show
//
//  Created by JITENDRA on 10/31/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STTweetView.h"
#import "BaseFeedModel.h"


typedef NS_ENUM(NSInteger, MSShareScreenName) {
    Mint,
    Showroom,
    Profile,
};


@interface MSShareVC : UIViewController

@property (nonatomic,strong)  NSString *feedID;
@property (nonatomic,strong)  NSString *showroomName;
@property (nonatomic,strong)  NSString *showTag;
@property (nonatomic,strong)  NSString *txtDescription;
@property (nonatomic, strong) NSString *linkUrl;
@property (nonatomic, strong) NSString *imgUrl;
@property (assign)            BOOL     isFromMintDetail;
@property (assign)            BOOL     isShowroomPrivate;


@property (nonatomic, strong) ShowRoomModel *showroomdata;
@property (nonatomic, strong) BaseFeedModel *feeddata;
@property (nonatomic, assign) MSShareScreenName currentScreen;

@end
