
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "StreakCircleView.h"


@protocol MintSectionHeaderViewDelegate;


@interface MintSectionHeaderView : UITableViewHeaderFooterView

@property (strong, nonatomic)   IBOutlet UIImageView *userImage;
@property (weak, nonatomic)     IBOutlet UIImageView *imgVLocation;
@property (weak, nonatomic)     IBOutlet UIImageView *imgVBack;

@property (nonatomic, weak)     IBOutlet StreakCircleView *streakUserProfile;
@property (strong, nonatomic)   IBOutlet UIView *viewLightGray;

@property (nonatomic, weak)     IBOutlet UILabel     *lblUsername;
@property (strong, nonatomic)   IBOutlet UILabel     *lblTimeCategory;
@property (weak, nonatomic)     IBOutlet UILabel     *lblSeparator;
@property (weak, nonatomic)     IBOutlet UILabel     *lblStreakInfo;

@property (strong, nonatomic)   IBOutlet UIButton    *backButton;
@property (strong, nonatomic)   IBOutlet UIButton    *circlebtn;
@property (strong, nonatomic)   IBOutlet UIButton    *btnProfileImg;

@property (nonatomic)           NSInteger section;


-(void)setUIforHeader;
-(void)setUpRemintInfo:(NSString *)remintedUsername remintedUserImage:(NSString *)imageUrl mintPostedTime:(NSString *)time remintedUserId:(NSString *)userId;






@end



