//
//  RoundedRect.h
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 27/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreakCircularProgress.h"
@interface StreakCircleView : UIView
{
    
}

@property (nonatomic, strong) StreakCircularProgress *progressView;
@property (nonatomic, strong) StreakCircularProgress *circularProgressView;
@property (nonatomic) CGFloat max;
-(id)setStreakColorWithDays:(int)days;
@end
