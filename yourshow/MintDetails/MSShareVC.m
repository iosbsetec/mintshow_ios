//
//  MSShareVC.m
//  Your Show
//
//  Created by JITENDRA on 10/31/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//
#import "SocialVideoHelper.h"
#import "YSSupport.h"
#import "MSShareVC.h"
#import "UIViewController+CWPopup.h"
#import "FriendsInfo.h"
#import "CellInvite.h"
#import  <MessageUI/MessageUI.h>
#import "NSString+Emojize.h"
#import "STTweetLabel.h"

@interface MSShareVC ()<FBSDKSharingDelegate,MFMailComposeViewControllerDelegate,textSuggestDelegate>
{

    __weak IBOutlet UITextField *txtFldSearch;
    NSMutableArray *resultedFriends,*serachArray;
    IBOutlet UITableView *tblFriends;
    IBOutlet UITableViewCell *cellEmail;
    IBOutlet UITableViewCell *cellTwitter;
    IBOutlet UITableViewCell *cellCopyLinks;
    IBOutlet UITableViewCell *cellOtherApps;

    IBOutlet UITableViewCell *cellSms;
    IBOutlet UITableViewCell *cellFacebook;
    __weak IBOutlet UILabel *lblEmailInfo;
    BOOL isSearchingStarted;
    BOOL isRightMail;
    IBOutlet UIView *viewLink,*tableFooter;
    IBOutlet UILabel *lblLink;
    IBOutlet UILabel *lblHeaderTitle;

    NSMutableArray  *arrayShareList;

    __weak IBOutlet UITextField *txtxFldShareEmail;
    __weak IBOutlet UILabel *lblMessageForShare;
    IBOutlet UIView *viewEmailShare;
    
    
    
    IBOutlet UILabel *lblShowroomtag;
    IBOutlet UILabel *lblShowroomName;
    IBOutlet UILabel *lblShowroomUrl;
    IBOutlet UIImageView *imgViewShowroom;
    IBOutlet UIView *viewMintEmailShare;
    IBOutlet UIImageView *imgViewMint;
    IBOutlet UIImageView *imgViewMintVideo;
    IBOutlet UIView *DescriptionBGView;
    IBOutlet UIView *viewShowroomDescription;
    IBOutlet UIView *viewShowroomName;
    IBOutlet UILabel *lblMintUrl;
    
    IBOutlet UIView *viewShwroomDscrptionBg;
    IBOutlet STTweetLabel *lblShwroomDscrption;
    STTweetView *lblTwtMintdescription;
    
    
    BOOL isLoading,isDataFinishedInServer;
    int pageNum;
    NSString       * searchTxt,*screenShareStr;


}
@property (nonatomic, strong) NSTimer *searchCountdownTimer;

@property (strong, nonatomic) FBSDKShareDialog *shareCodeDialog;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIButton *canCellTapped;

@property (strong, nonatomic) IBOutlet UILabel *lblMintDescription;
//@property (strong, nonatomic) STTweetView *lblTwtMintdescription;

- (IBAction)cancelTapped:(UIButton *)sender;
- (IBAction)sendCancelEmail:(UIButton *)sender;

@end

@implementation MSShareVC


-(NSInteger)checkShareScreen:(MSShareScreenName)ShareScreen
{
    MSShareScreenName mode = ShareScreen;
    
    switch (mode) {
        case Mint:
        {
            screenShareStr = @"Mint";
        }
            break;
        case Showroom:
            
        {
            screenShareStr = @"Showroom";
        }
            break;
        case Profile:
            
        {
            screenShareStr = @"Profile";
        }
            break;
            
        default:
            break;
    }
    return mode;
}

-(IBAction)faceBookTapped:(id)sender{
    
    MSShareScreenName mode = self.currentScreen;
    [self checkShareScreen:mode];
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    if( _feeddata.mintType == MSMintTypeVideo || _feeddata.mintType == MSMintTypeImage  || _feeddata.mintType == MSMintTypeYouTube)
        content.imageURL   =  [NSURL URLWithString:_imgUrl];
    
    content.contentDescription= [NSString stringWithFormat:@"%@",_txtDescription];
    content.contentURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@",_linkUrl] stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
    content.contentTitle= [NSString stringWithFormat:@"MintShow: %@ posted by %@ ",screenShareStr,[NSString stringWithFormat:@"%@",_feeddata.feed_user_name]];
    
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
    
    
}

- (IBAction)twitterTapped:(UIButton *)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])  {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
        {
            if (result == SLComposeViewControllerResultCancelled)
            {
                NSLog(@"Cancelled");
            }
            else
            {
                [self showshare:@"Shared In Twitter!"];
            }
                [controller dismissViewControllerAnimated:YES completion:Nil];
                [self cancelTapped:nil];
            
        };
        controller.completionHandler =myBlock;
        MSShareScreenName mode = self.currentScreen;
        [self checkShareScreen:mode];
        
        NSString *apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",_linkUrl];
        NSString *shortURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@",apiEndpoint]  stringByReplacingOccurrencesOfString:@" " withString:@"%20"]]
                                                      encoding:NSASCIIStringEncoding
                                                         error:nil];
        
        [controller addURL:[NSURL URLWithString:shortURL]];
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_imgUrl]]]];
        
        [controller setInitialText:[NSString stringWithFormat:@"Check out this %@! #MintShow @mintshowapp",screenShareStr]];
        
        
        [self presentViewController:controller animated:YES completion:Nil];
    } else {
        //        showAlert(@"No Twitter Accounts",@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings.",@"OK",nil)
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings." message:@"" delegate:self cancelButtonTitle:@"dismiss" otherButtonTitles:@"settings", nil];
        [alert show];
        
        
    }
    
    
}


- (IBAction)btnDoneTapped:(UIButton *)sender {
    //  resultedFriends
    
    if ([resultedFriends count] == 0) {
        return;
    }
    
    NSString * result = [arrayShareList componentsJoinedByString:@","];

    NSLog(@"SELECTED FRIENDS %@" , result);
    
    if ([arrayShareList count])
    [self sendMailForShareToServer:@"csuite" shareOption:2 receiverId:result];
    else
    
        [self showshare:@"Please select any friends"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    resultedFriends = [[NSMutableArray alloc]init];
    serachArray = [[NSMutableArray alloc]init];
    arrayShareList =  [[NSMutableArray alloc]init];
    isSearchingStarted = NO;
    isRightMail = NO;

    pageNum = 1;
    isLoading = NO;
    isDataFinishedInServer = NO;
    searchTxt = @"";
    
   // [self getFollwerList:pageNum searchText:searchTxt isdelete:NO];


    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:@"https://www.facebook.com/FacebookDevelopers"];
    FBSDKShareButton *shareButton = [[FBSDKShareButton alloc] init];
    [shareButton setFrame:cellFacebook.frame];
    shareButton.alpha = 0.0;
   
    shareButton.shareContent = content;
    [cellFacebook addSubview:shareButton];
    lblLink.text = _linkUrl;

    tblFriends.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    lblHeaderTitle.text = (_isFromMintDetail)?@"Share this Mint":@"Share this Showroom";    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelTapped:(UIButton *)sender {
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"CLoseSharePopUp" object:nil];
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
   // delegateApp.lbl.hidden = NO;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
  }

- (IBAction)sendCancelEmail:(UIButton *)sender {
    
    if (sender.tag == 0) {
        txtxFldShareEmail.text = nil;
        [viewEmailShare removeFromSuperview];
    }
    else{

        if ([YSSupport validateEmail:txtxFldShareEmail.text]  && [txtxFldShareEmail.text length]) {
            [viewEmailShare removeFromSuperview];
            [self sendMailForShareToServer:@"email" shareOption:1 receiverId:@""];
        }
        else
            [self showshare:([txtxFldShareEmail.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]].length>0)?@"Invalid emailId":@"Enter emailId"];
    }
    

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
    
}




-(IBAction)searchFriends : (UITextField *) textField
{
    NSString *serach = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (textField.text.length == 0) {
        lblEmailInfo.textColor = [UIColor lightGrayColor];
        isSearchingStarted = NO;
        [tblFriends reloadData];
    }
    else
    {
        isSearchingStarted = YES;
        [self serachText:serach];
    }
}


-(void)serachText: (NSString *)searchStringVal
{
    //stop the current countdown
    if (self.searchCountdownTimer) {
        [self.searchCountdownTimer invalidate];
    }
    NSDate *fireDate = [NSDate dateWithTimeIntervalSinceNow:1.0];
    // add search data to the userinfo dictionary so it can be retrieved when the timer fires
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                          searchStringVal, @"searchString",
                          nil];
    self.searchCountdownTimer = [[NSTimer alloc] initWithFireDate:fireDate
                                                         interval:0
                                                           target:self
                                                         selector:@selector(searchCountdownTimerFired:)
                                                         userInfo:info
                                                          repeats:NO];
    
    [[NSRunLoop mainRunLoop] addTimer:self.searchCountdownTimer forMode:NSDefaultRunLoopMode];
}

- (void)searchCountdownTimerFired:(NSTimer *)countdownTimer {
    
    searchTxt = [countdownTimer.userInfo objectForKey:@"searchString"];
    // NSLog(@"searchString %@",[countdownTimer.userInfo objectForKey:@"searchString"]);
    
    if ([searchTxt length]) {
        pageNum = 1;
        [self getFollwerList:pageNum searchText:searchTxt isdelete:YES];
    }
    
}

/*
-(IBAction)searchFriends : (UITextField *) txtField
{
 
    if (txtFldSearch.text.length == 0) {
        lblEmailInfo.textColor = [UIColor lightGrayColor];
        isSearchingStarted = NO;
    }
    if([[txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]>0){
        isSearchingStarted = YES;
        NSString *searchString = txtField.text;

        NSPredicate *filter = [NSPredicate predicateWithFormat:@"name BEGINSWITH[cd] %@", searchString];
        //        NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", searchString];
        NSArray*  resultArray = [[resultedFriends filteredArrayUsingPredicate:filter] mutableCopy];
        
        
        if (resultArray.count == 0) {
            NSLog(@"No data From Search");
            [resultedFriends removeAllObjects];
        }
        else
            
        {
            [resultedFriends removeAllObjects];
            [resultedFriends addObjectsFromArray:resultArray];
        }
        
    } else {
        if ([resultedFriends count] != [serachArray count]) {
            
            [resultedFriends removeAllObjects];
            [resultedFriends addObjectsFromArray:serachArray];
        }

    }
    
    [tblFriends reloadData];

}
*/

-(void)twitterFriends
{
    
    if ([SocialVideoHelper userHasAccessToTwitter]) {
        
        [SocialVideoHelper getTwitterFriendsForSuccessionBlock:^(id newResponse) {
            NSLog(@"sucsses");
            
            if ([newResponse isKindOfClass:[NSArray class]]) {
                NSLog(@"Number of friends %lu",(unsigned long)[(NSArray *)newResponse count]);
               [resultedFriends addObjectsFromArray:(NSMutableArray *)newResponse];
                [serachArray addObjectsFromArray:(NSMutableArray *)newResponse];
            }
            
        } andFailureBlock:^(NSError *error) {
            
            NSLog(@"failure");
            
            
        }];
        
    }
    
}
-(void)checkTheAuthenticationForFb
{
    if (![FBSDKAccessToken currentAccessToken])
    {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithReadPermissions: @[@"public_profile",@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error) {
                NSLog(@"Process error");
            } else if (result.isCancelled) {
                NSLog(@"Cancelled");
            }
            else {
                NSLog(@"Logged in");
                
                [self getFriends];
                
                
                
                //TODO: process error or result.
            }
        }];
    }
    
    else
    {
        [self getFriends];
        
        
    }
    
    
    
}
-(void)getFriends
{
    [SocialVideoHelper getFbfriendslist:^(id newResponse) {
        NSLog(@"sucsses");
        
        if ([newResponse isKindOfClass:[NSArray class]]) {
            NSLog(@"Number of friends %lu",(unsigned long)[(NSArray *)newResponse count]);
            resultedFriends= (NSMutableArray *)newResponse;
            [tblFriends reloadData];
        }
        
    } andFailureBlock:^(NSError *error) {
        NSLog(@"failure");
        
    }];
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (isSearchingStarted)?(([resultedFriends count] == 0)?((_isShowroomPrivate)?1:5):((_isShowroomPrivate)?resultedFriends.count:resultedFriends.count+4)):((_isShowroomPrivate)?resultedFriends.count:4);

}
-(UIView *)creatorDevider
{
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];/// change size as you need.
    separatorLineView.backgroundColor = [UIColor lightGrayColor];// you can also put image here

    return separatorLineView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    NSInteger rowCount = [tableView numberOfRowsInSection:0];
    
    if (!_isShowroomPrivate) {

        if (indexPath.row == rowCount-4) {
            [cellFacebook addSubview:[self creatorDevider]];
            return  cellFacebook;
        }
        else if (indexPath.row == rowCount-3) {
            [cellTwitter addSubview:[self creatorDevider]];

            return  cellTwitter;
        }
       else if (indexPath.row == rowCount-2){
           [cellEmail addSubview:[self creatorDevider]];

            return  cellEmail;
        }
       else if (indexPath.row == rowCount-1){
           [cellCopyLinks addSubview:[self creatorDevider]];
           return  cellCopyLinks;
       }
        else
        goto MandatoryData;

    }
    else
        goto MandatoryData;
    
    

     MandatoryData:
        {
            
            static NSString *CellIdentifier = @"CellInvite";
            CellInvite *cell = (CellInvite*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
            }
            
            if ([resultedFriends count]) {
          
            [cell.btnFollow addTarget:self action:@selector(shareViaEmail:) forControlEvents:UIControlEventTouchUpInside];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.btnFollow.tag =indexPath.row;
            [cell.contentView setBackgroundColor:[UIColor clearColor]];
            
            FriendsInfo *obj   = [resultedFriends objectAtIndex:indexPath.row];
            [cell setInfo:obj.profile_image_url userName:obj.name];
           
                if ([obj.selected integerValue]) {
                [cell.btnFollow setTitle:@"Shared" forState:UIControlStateNormal];
                cell.btnFollow.layer.borderWidth = 1.0;
                [cell.btnFollow setBackgroundColor:[UIColor clearColor]];
                cell.btnFollow.layer.borderColor = ORANGECOLOR.CGColor;
                [cell.btnFollow setTitleColor:ORANGECOLOR forState:UIControlStateNormal];

            }
            else
            {
                [cell.btnFollow setTitle:@"Share" forState:UIControlStateNormal];
                [cell.btnFollow setBackgroundColor:ORANGECOLOR];
                cell.btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
                [cell.btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

            }
            [cell addSubview:[self creatorDevider]];
            }
            
            else
            {
            
                cell.lblName.text = @"No Records Found";
                cell.btnFollow.hidden = YES;
                cell.imgVUserPic.hidden = YES;
                
            }

           return cell;
        }
}

-(void)shareViaEmail : (UIButton *)sender
{
    // check_withcircle
    CellInvite *cell = (CellInvite *)[tblFriends cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    
    if (![sender isSelected]) {
        [sender         setSelected:YES];
        [cell.btnFollow setTitle:@"Shared" forState:UIControlStateNormal];
        [cell.btnFollow setBackgroundColor:[UIColor whiteColor]];
        cell.btnFollow.layer.borderColor = ORANGECOLOR.CGColor;
        cell.btnFollow.layer.borderWidth = 1.0;
        FriendsInfo *obj                 = [resultedFriends objectAtIndex:sender.tag];
        obj.selected                     = @"1";
        [resultedFriends    replaceObjectAtIndex:sender.tag withObject:obj];
        [cell.btnFollow     setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
        [arrayShareList     addObject:obj.id_str];
    }
    else
    {
        [sender            setSelected:NO];
        [cell.btnFollow    setTitle:@"Share" forState:UIControlStateNormal];
        FriendsInfo *obj   = [resultedFriends objectAtIndex:sender.tag];
        obj.selected       = @"0";
        [resultedFriends    replaceObjectAtIndex:sender.tag withObject:obj];
        [cell.btnFollow setBackgroundColor:ORANGECOLOR];
        [cell.btnFollow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cell.btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
        [arrayShareList removeObject:obj.id_str];
    }
    
}

#pragma mark - Alert
- (NSString*) urlRequestToString:(NSMutableURLRequest*)urlRequest
{
    NSString *requestPath = [[urlRequest URL] absoluteString];
    return requestPath;
}
-(void)getFollwerList:(int)pageNumber searchText:(NSString *)searchVal isdelete:(BOOL)delete
{

    NSData*  submitData = nil;
    if ([searchVal length]) {
        
       submitData    = [[NSString stringWithFormat:@"access_token=%@&user_id=%@&search_text=%@",GETVALUE(CEO_AccessToken),GETVALUE(CEO_UserId),searchVal] dataUsingEncoding:NSUTF8StringEncoding];

    }
    else
        
    submitData    = [[NSString stringWithFormat:@"access_token=%@&user_id=%@",GETVALUE(CEO_AccessToken),GETVALUE(CEO_UserId)] dataUsingEncoding:NSUTF8StringEncoding];
    
//    NSString *requestInfo = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    
    [YSAPICalls getAllFriendsInCircle:submitData methodName:METHOD_FOLLOWERLIST ForSuccessionBlock:^(id newResponse) {
        Generalmodel *model = newResponse;
        NSLog(@"response = %@",model.status_code);
        
        if ([model.status_code integerValue]) {
            
            isLoading = NO;
            
            
            
            if ([model.tempArray count] > 0) {
                isDataFinishedInServer = NO;
                
                if (delete) {
                    [resultedFriends removeAllObjects];
                }
                
                [resultedFriends addObjectsFromArray:model.tempArray];
              //  [serachArray addObjectsFromArray:model.tempArray];
            }
                else
                {
                    [resultedFriends removeAllObjects];
                    isDataFinishedInServer = YES;
                }
            [tblFriends reloadData];

        }
        
    } andFailureBlock:^(NSError *error) {
        
    }];

}
//static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.70;
//
//static BOOL ShouldLoadNextPage(UITableView *tblView)
//{
//    CGFloat yOffset = tblView.contentOffset.y;
//    CGFloat height = tblView.contentSize.height - CGRectGetHeight(tblView.frame);
//    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
//}
//
//
//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    BOOL shouldLoadNextPage = ShouldLoadNextPage(tblFriends);
//    if (shouldLoadNextPage)  {
//        
//        
//        if(!isLoading && !isDataFinishedInServer)
//        {
//            pageNum      = pageNum +1;
//            isLoading        = YES;
//            
//            [self getFollwerList:pageNum searchText:searchTxt  isdelete:NO];
//            
//        }
//        
//    }
//    
//    
//}




/*- (IBAction)twitterTapped:(UIButton *)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])  {

        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
        {
            if (result == SLComposeViewControllerResultCancelled)
            {
                NSLog(@"Cancelled");
            }
            else
            {

               // [[NSNotificationCenter defaultCenter] postNotificationName:@"CLoseSharePopUp" object:nil];
                [self showshare:@"Shared In Twitter!"];



            }
            [controller dismissViewControllerAnimated:YES completion:Nil];
            [self cancelTapped:nil];

        };
        controller.completionHandler =myBlock;

        
        [controller addURL:[NSURL URLWithString:_linkUrl]];
        [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_imgUrl]]]];
        [controller setInitialText:[NSString stringWithFormat:@"Check out this Mint! #MintShow @mintshowapp %@ (scheduled via %@)",_txtDescription,_linkUrl]];

        [self presentViewController:controller animated:YES completion:Nil];
    } else {
        //        showAlert(@"No Twitter Accounts",@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings.",@"OK",nil)
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings." message:@"" delegate:self cancelButtonTitle:@"dismiss" otherButtonTitles:@"settings", nil];
        [alert show];
        
        
    }

    
}*/
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        //Code for OK button
    }
    if (buttonIndex == 1)
    {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=TWITTER"]];
        
    }
}

/*-(IBAction)faceBookTapped:(id)sender{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    if( _feeddata.mintType == MSMintTypeVideo || _feeddata.mintType == MSMintTypeImage  || _feeddata.mintType == MSMintTypeYouTube)
        content.imageURL   =  [NSURL URLWithString:_imgUrl];
    content.contentDescription= [NSString stringWithFormat:@"%@ (scheduled via %@)",_txtDescription,_linkUrl];
    content.contentURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@",_linkUrl] stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
    content.contentTitle= @"MintShow Mint";
    
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
    
    
}*/
 
 - (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"CLoseSharePopUp" object:nil];

    if ([results  valueForKey:@"postId"]) {
  
        [self showshare:@"Shared In facebook!"];
        [self cancelTapped:nil];

       }
}
-(void)showshare :(NSString *)titleMsg
{

    NSLog(@"returned back to app from facebook post");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:titleMsg
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    [alert show];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });


}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    [self cancelTapped:nil];

    NSLog(@"canceled!");
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"sharing error:%@", error);
    NSString *message = @"There was a problem sharing. Please try again!";
    [[[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)textDidChange:(STTweetView*)commentView
{
   
    
}

- (IBAction)emailTapped:(UIButton *)sender {
    if(_isFromMintDetail)  {
        viewShowroomName.hidden = YES;
        viewShowroomDescription.hidden = YES;
        imgViewMint.hidden       = (_feeddata.mintType == MSMintTypeText)?YES:NO;
        /*!  set the video icon if it is a video mint  */
        imgViewMintVideo.alpha = ((_feeddata.mintType != MSMintTypeText && _feeddata.mintType != MSMintTypeImage))?1.0:0.0;
        
        //            _lblTwtMintdescription.text    = [[_feeddata.feed_text emojizedString]StringByChangingComplimintStringToNormalString];
        
        lblTwtMintdescription = [[STTweetView alloc]initWithFrame:CGRectMake(0, 0, DescriptionBGView.frame.size.width, DescriptionBGView.frame.size.height)];
        lblTwtMintdescription.textDelegate = self;
        lblTwtMintdescription.textView.text    = ([lblHeaderTitle.text isEqualToString:@"Share this Showroom"])?@"Check out this Showroom on MintShow #MintShow @mintshowapp":[NSString stringWithFormat:@"Check out this Mint on MintShow %@ #MintShow @mintshowapp",[_feeddata.feed_description emojizedString]];   // need to change
        [lblTwtMintdescription determineHotWordsNew];
        lblTwtMintdescription.textView.editable = NO;
        [DescriptionBGView addSubview:lblTwtMintdescription];
        imgViewMint.mintImageURL      = [NSURL URLWithString:_feeddata.feed_thumb_image_small];
        _lblMintDescription.text  = _feeddata.description;
        lblMintUrl.text           = _feeddata.mintdetail_url;
        lblMintUrl.layer.cornerRadius = 3.0;
        imgViewMint.layer.cornerRadius = 3.0;
        [imgViewMint setClipsToBounds:YES];
        
        CGRect frameViewMintEmailShare ;
        
        
        
        
        /*!  set the frame of mint view according to image and text mint  */
        
        
        if (_feeddata.mintType == MSMintTypeText) {
            CGRect recttxtLbl  = [[NSString stringWithFormat:@"Check out this Mint on MintShow %@ #MintShow @mintshowapp",[_feeddata.feed_description emojizedString]] boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:12.0]} context:nil];
            /*!  set the frame of description view according to image and text mint  */
            CGRect frameMintDescription      = DescriptionBGView.frame;
            frameMintDescription.origin.x    = 5;
            
            
            recttxtLbl.size.height = recttxtLbl.size.height +5;
            
            frameMintDescription.size.width  = viewMintEmailShare.frame.size.width - 10 ;
            frameMintDescription.size.height  = recttxtLbl.size.height  ;    // need to change
            frameMintDescription.origin.y    = 5;
            
            CGRect frameLblMintDescription      = lblTwtMintdescription.frame;
            frameLblMintDescription.origin.x    = 0;
            frameLblMintDescription.origin.y    = 0;
            
            frameLblMintDescription.size.height  = recttxtLbl.size.height ;   // need to change
            frameLblMintDescription.size.width      = frameMintDescription.size.width ;
            [lblTwtMintdescription setFrame:frameLblMintDescription];
            
            [DescriptionBGView setFrame:frameMintDescription];
            
            
            
            /*!  set the frame of link label  */
            
            CGRect frameMintlink      =  lblMintUrl.frame;
            frameMintlink.origin.y    = DescriptionBGView.frame.origin.y+DescriptionBGView.frame.size.height +8;
            lblMintUrl.frame          = frameMintlink;
            
            
            /*!  set the frame of MintEmailShare View  */
            
            frameViewMintEmailShare = viewMintEmailShare.frame;
            
            frameViewMintEmailShare.size.height = frameMintlink.origin.y + frameMintlink.size.height+10;
            viewMintEmailShare.frame = frameViewMintEmailShare;
            viewMintEmailShare.frame  = CGRectMake(10, 105,[UIScreen mainScreen].bounds.size.width - 20, frameViewMintEmailShare.size.height);
            
            
        }
        viewMintEmailShare.frame  = (_feeddata.mintType == MSMintTypeText)?CGRectMake(10, 105,[UIScreen mainScreen].bounds.size.width - 20, frameViewMintEmailShare.size.height):CGRectMake(10, 105,[UIScreen mainScreen].bounds.size.width - 20, viewMintEmailShare.frame.size.height);
        
        [viewEmailShare addSubview:viewMintEmailShare];
        
    }
    else
    {
        /*!  set the frame of showroom description  */
        
        CGRect recttxtLbl  = [[NSString stringWithFormat:@"Check out this Mint on MintShow %@ #MintShow @mintshowapp",[_showroomdata.showroom_description emojizedString]] boundingRectWithSize:CGSizeMake(lblShwroomDscrption.frame.size.width-8, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:12.0]} context:nil];
        
        
        lblShowroomUrl.layer.cornerRadius = 3.0;
        imgViewShowroom.layer.cornerRadius = 3.0;
        [imgViewShowroom setClipsToBounds:YES];
        
        
        CGRect rectViewDes = viewShwroomDscrptionBg.frame;
        rectViewDes.size.height = recttxtLbl.size.height+ 5;
        [viewShwroomDscrptionBg setFrame:rectViewDes];
        
        CGRect rectLbl = lblShwroomDscrption.frame;
        rectLbl.size.height = recttxtLbl.size.height+ 5;
        [lblShwroomDscrption setFrame:rectLbl];
        
        lblShowroomName.text = _showroomdata.showroom_name;
        lblShowroomtag.text  = [NSString stringWithFormat:@"%@",_showroomdata.show_tag];
        lblShowroomtag.textColor  = ORANGECOLOR;
        lblShwroomDscrption.text = [NSString stringWithFormat:@"Check out this Mint on MintShow %@ #MintShow @mintshowapp",[_showroomdata.showroom_description emojizedString]];
        lblShowroomUrl.text = _showroomdata.showroom_link;
        lblShowroomUrl.layer.cornerRadius = 4.0;
        /*!  set the frame of showroom image  */
        CGRect frameShowroomimage          = imgViewShowroom.frame;
        frameShowroomimage.origin.y        = lblShwroomDscrption.frame.origin.y + lblShwroomDscrption.frame.size.height+10;
        [ imgViewShowroom setFrame: frameShowroomimage];
        
        /*!  set the frame of showroom url  */
        CGRect frameShowroomlink          = lblShowroomUrl.frame;
        frameShowroomlink.origin.y        = imgViewShowroom.frame.origin.y + imgViewShowroom.frame.size.height+10;
        //         frameShowroomlink.size.height     = CGRectGetHeight(recttxtLbl)+5;
        [lblShowroomUrl setFrame: frameShowroomlink];
        
        imgViewShowroom.mintImageURL = [NSURL URLWithString:_showroomdata.showroom_img_Url];
    }
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [delegate.window addSubview:viewEmailShare];
    
    
    
}



- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self cancelTapped:nil];

    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)copyLinkTapped:(UIButton *)sender {
    
    AppDelegate *delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    [delegate.window addSubview:viewLink];
}
- (IBAction)copyLink:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string =_linkUrl;
    [viewLink removeFromSuperview];

    [self showshare:@"Copied!"];
    [self cancelTapped:nil];



}
- (IBAction)backTapped:(id)sender {
    [viewLink removeFromSuperview];
}

- (void)sendMailForShareToServer:(NSString *)shareType shareOption : (int)shareOption receiverId : (NSString *) reciverId{
    //  resultedFriends
 //share_email,site_name
    NSData*  submitData  = nil;
    NSMutableURLRequest *submitrequest = nil;
    
    if (_isFromMintDetail) {
          submitData    = [[NSString stringWithFormat:@"share_option=%d&access_token=%@&feed_id=%@&receiver_id=%@&feed_type=%@&share_type=%@&to_email=%@",shareOption,GETVALUE(CEO_AccessToken),_feedID,reciverId,@"mint",shareType,txtxFldShareEmail.text] dataUsingEncoding:NSUTF8StringEncoding];
         submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@share/mint",ServerUrl]]];
    }
    else
    {

        if ([viewEmailShare superview]) {
            submitData    = [[NSString stringWithFormat:@"showroom_tag=%@&option=2&type=%d&access_token=%@&share_email=%@&site_name=%@",_showroomName,2,GETVALUE(CEO_AccessToken),txtxFldShareEmail.text,@"email"] dataUsingEncoding:NSUTF8StringEncoding];

        }
        else
        submitData    = [[NSString stringWithFormat:@"showroom_tag=%@&option=2&type=%d&access_token=%@&receiver_id=%@",_showroomName,1,GETVALUE(CEO_AccessToken),reciverId] dataUsingEncoding:NSUTF8StringEncoding];
        
        submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@customshowroom/particular_showroom_share_invite",ServerUrl]]];
    }

    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:submitData];
    
    NSString *submitDta123 = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"submitDta123 data %@",submitDta123);
    
    [self cancelTapped:nil];
    
    NSString *submitDtaRequesr = [self urlRequestToString:submitrequest];
    
    NSLog(@"submitDtaRequesr data %@",submitDtaRequesr);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         id jsonObject1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
         
         NSLog(@"json data=%@",jsonObject1);
         
         if ([(NSHTTPURLResponse *)response statusCode]==200)
         {
             
             id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
             
             [self.view endEditing:YES];
             
             if(jsonObject)
             {
                 [self showshare:@"Shared Successfully!"];
                 
             }
             
         }
         else
         {
             
         }
         
     }];
    
}

@end
