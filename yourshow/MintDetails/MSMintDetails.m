//
//  MSMintDetails.m
//  Your Show
//
//  Created by JITENDRA KUMAR PRADHAN on 26/10/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//
static NSString *MintSectionHeaderViewIdentifier = @"MintSectionHeaderViewIdentifier";
#define SIGNATURESHOWROOMCOLOR                [UIColor colorWithRed:41./255.0 green:172./255.0 blue:230.0/255. alpha:1.0]

#import "MintDetailsCell.h"
#import "MSMintDetails.h"
#import "MintSectionHeaderView.h"
#import "MSRemintVC.h"
#import "MessageComposerView.h"
#import "CommentModel.h"
#import "MSShareVC.h"
#import "AppDelegate.h"
#import "MHFacebookImageViewer.h"
#import "CommentsViewController.h"
#import "NSString+Emojize.h"
#import "AddMintViewController.h"
#import "UIViewController+CWPopup.h"
#import "POPupTableView.h"
#import "TSPopoverController.h"
#import "EntitySupport.h"
#import "CarbonKit.h"
#import "UsersListView.h"
#import "MintShowroomViewController.h"
#import "MSProfileVC.h"
#import "CoachingTip.h"
#import "MSPSignatureShowroomVC.h"
#import "MSSettingVC.h"

#define KDESCRIPTIONHEIGHT  40.0
#define KBUTTONCOMPONENT    66.0
#define KCOMPLIMENTSVIEW    52.0
#define KVIEWALLCOMPLIMINT  28.0

#define KVIEWREMINT         80.0
#define KREMINTMESSAGEHEIGHT 73.0



typedef enum showRoomSelectionType3
{
    MSGSHOWROOM_NOTSELECTD,
    MSGSHOWROOM_PUBLICSELECTD,
    MSGSHOWROOM_PRIVATESELECTD,
} MSGShowroomType3;





@interface MSMintDetails () <UIActionSheetDelegate,UIScrollViewDelegate,RemintDelegate,MintDetailsCellDelegate,UsersListDelegate>

{

    NSData         *localData;
    NSString       *scrollDirection;
    NSString       *firstTimeApiCall;
    NSString       *privateShowRoomId;
    NSString       *privateShowRoomName;
    NSString       *selectedFeature;
    NSString       *deleteType;

    BOOL            isLoading;
    BOOL            isDataFinishedInServer;
    BOOL            ScrolVal;
    BOOL            isPrivateShowroomSelectd;

    NSInteger       selectedIndex;
    int             pagenumbers,RowId;

    
    //FOR *,@,# feature
    
    
    NSMutableArray *dataArray;
    NSMutableArray *showroomAddedArray;
    NSMutableArray *hashTagAddedArray;
    NSMutableArray *userNameAddedArray;
    NSMutableArray *currentlyPlayingIndexpath;
    NSMutableArray *arrayMintdetailsList;

    
    TSPopoverController *popoverController;
    POPupTableView      *apopupTable;
    EntityType           selectedEntitytype;
    NSRange              selectedRange;
    MSGShowroomType3    theShowroomSelectState;
    
    CarbonSwipeRefresh *refreshCarbon;
    UsersListView      *listedview;
    
    IBOutlet UIView      *viewTableHeader;
    IBOutlet UILabel     *lblHashTagInfo;
    IBOutlet UIButton    *btnInvite,*btnMore,*btnBack;
    IBOutlet UIImageView *showroomImage;
    IBOutlet UIImageView *profileUserImage;
}


@property (strong, nonatomic)    IBOutlet UITableView     *tblViewMintList;
@property (nonatomic, retain)    UIView                   *headerView;
@property (assign,nonatomic)     NSInteger                 currentAchievement;
@property (nonatomic, strong)    NSDictionary *emojiDict;

- (IBAction)btnSettingsTapped:(UIButton *)sender;


@end

@implementation MSMintDetails



- (void)viewDidLoad {
    [super viewDidLoad];
    pagenumbers     =   1;
    ScrolVal        =   NO ;
    isLoading       =   NO ;
    isDataFinishedInServer = NO;

    arrayMintdetailsList   = [NSMutableArray array];
   
    UINib *sectionHeaderNib                   = [UINib nibWithNibName:@"MintSectionHeaderView" bundle:nil];
    [self.tblViewMintList registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:MintSectionHeaderViewIdentifier];
    [self.navigationController setNavigationBarHidden:YES];
    self.tblViewMintList.sectionHeaderHeight  = 40;

    if (!_isToshowSingleRecord) {

        [self setUpSomeInfo];
    } else {
        [self singleMintDetail:_mint_id];
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(commentUpdatedSuccesFullyDetails:) name:KCOMMNETUPDATED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(mintEditedSuccesFully:) name:KMINTEDITED object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissPopup) name:@"CLoseSharePopUp" object:nil];


    self.tblViewMintList.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.emojiDict = [YSSupport reverseEmojiAliases];
    
    if ([GETVALUE(CEO_ISMINTDETAILFIRSTTIMEVISIT) isEqualToString:@"yes"] && !_isMintOwner ) {
           CoachingTip *viewCoach = [[CoachingTip alloc]initWithFrame:[UIScreen mainScreen].bounds screenName:@"Mintdetail" headerPresent:(((_isFromTrendingTags && [_navigationHeaderText length]) || _isFromParticularShowroom))?YES:NO];
            [self.view addSubview:viewCoach];

    }
    

    //Setup tableview frame accoring to the screen it traverse
    //If its traverse from the Trending tags or particular showroom  need to show the sticky header
    [self setUpNavigationHeader];
 
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self playVideo];
    
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    if (aDelegate.isCommentActionUpdate && self.isFromDiscoverTrending) {
        aDelegate.isCommentActionUpdate = NO;
        [self singleMintDetail:_mint_id];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self stopVideo];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
  
    
    //self.navigationController.scrollNavigationBar.scrollView = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --- SHow Room Mint Details API Call For Single Record ---

- (void)singleMintDetail: (NSString *)mintRowId {
    if (isLoading) {
        return;
    }
    
   // [MBProgressHUD showHUDAddedTo:self.view  animated:YES];
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&mint_id=%@",GETVALUE(CEO_AccessToken),_mint_id] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"mint/get_mint_details",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    NSLog(@"submitDta123 data %@",submitData);
    
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isLoading = NO;
        // [MBProgressHUD hideHUDForView:self.view  animated:YES];
         
         Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[YSSupport dictionaryFromResponseData:data] methodName:METHOD_SINGLE_MINTDETAIL];
         
         if ([gmodel.status_code integerValue])
         {
             if([gmodel.tempArray count]>0)
             {
                 if ([arrayMintdetailsList count]) {
                     [arrayMintdetailsList removeAllObjects];
                 }
                 [arrayMintdetailsList addObjectsFromArray: gmodel.tempArray];
                 [self.tblViewMintList reloadData];
                 BaseFeedModel *model = (BaseFeedModel*)[arrayMintdetailsList lastObject];
                 NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:model,@"model" ,nil];
                 [[NSNotificationCenter defaultCenter]postNotificationName:@"SINGLEMINTUPDATE" object:nil userInfo:dict] ;

             }
             else  {
                 
             }
         }
         
     }];
}

-(void)setUpNavigationHeader
{
    btnBack.hidden = NO;
    profileUserImage.hidden = YES;
    btnInvite.hidden = YES;
    btnMore.hidden = YES;
    lblHashTagInfo.textAlignment = NSTextAlignmentCenter;


    if ((_isFromTrendingTags && [_navigationHeaderText length]) || _isFromParticularShowroom || _isFromSignatureShowroom  || _isFromProfile) {
        
       
        
        CGRect tblViewFrame = self.tblViewMintList.frame;
        if (tblViewFrame.origin.y == 0) {
        tblViewFrame.origin.y = 44;
        tblViewFrame.size.height = CGRectGetHeight(tblViewFrame)- 44;
        _tblViewMintList.frame = tblViewFrame;
        }
        
        
        if (_isFromSignatureShowroom)
            [self setSignatureImage:[NSString stringWithFormat:@"%@",_navigationHeaderText] view:lblHashTagInfo];
        else
            lblHashTagInfo.text = _navigationHeaderText;
        
        
        if (_isFromParticularShowroom || _isFromSignatureShowroom)
        {
            showroomImage.mintImageURL = [NSURL URLWithString:_strShowroomImageUrl];
           // showroomImage.contentMode = UIViewContentModeBottom ;
        }
        
        if (_isFromTrendingTags)
        {
            if (_isFromSignatureShowroom) {
                showroomImage.mintImageURL = nil;
            }
            lblHashTagInfo.text = _navigationHeaderText;
            lblHashTagInfo.center = viewTableHeader.center;
            [self getHashTagMints:_navigationHeaderText withPageNumber:pagenumbers deleteList:YES];
        }
        
        
         if (_isFromProfile)
         
         {
             btnBack.hidden = YES;
             profileUserImage.hidden = NO;
             if ([_profileuser_id isEqualToString:GETVALUE(CEO_UserId)]) {
                 btnInvite.hidden = NO;
             }
             btnMore.hidden = NO;
             lblHashTagInfo.textAlignment = NSTextAlignmentLeft;
             lblHashTagInfo.text = _selectedProfileUserName;
             profileUserImage.clipsToBounds = YES;
             profileUserImage.mintImageURL = [NSURL URLWithString:_selectedProfileUserImage];
             lblHashTagInfo.center = viewTableHeader.center;
         }
        
        
    }

}
-(void)setUpSomeInfo
{
    //PULL TO REFRESH

    refreshCarbon = [[CarbonSwipeRefresh alloc] initWithScrollView:self.tblViewMintList withYPos:44];
    [refreshCarbon setMarginTop:0];
    [refreshCarbon setColors:@[ORANGECOLOR]];
    [self.view addSubview:refreshCarbon];
    [refreshCarbon addTarget:self action:@selector(refreshInMintDetail:) forControlEvents:UIControlEventValueChanged];
    
    
    self.tblViewMintList.separatorColor       = [UIColor blueColor];
    firstTimeApiCall                    =  @"YES";
    if (_isFromParticularShowroom || _isFromQueryResultPage || _isFromSignatureShowroom) {
        arrayMintdetailsList  = [_mintArrayDetails mutableCopy];
    } else if (_isFromProfile){
        arrayMintdetailsList  = [_profileMintArrayDetails mutableCopy];
    }
    else if (_isFromTrendingTags){
        
    }
    
    else  {
        SPHSingletonClass *objectShareClass = [SPHSingletonClass sharedSingletonClass];
        [arrayMintdetailsList  addObjectsFromArray:objectShareClass.arrayMyShowMintData];
    }
    [self.tblViewMintList reloadData];
    if (arrayMintdetailsList.count>self.Selected_row_value){
        [self.tblViewMintList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.Selected_row_value]  atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }


}
//-(void)setSignatureImage:(NSString *)showTagName view:(UILabel *​)lblView
-(void)setSignatureImage:(NSString *)showTagName view:(UILabel *)lblView
{
    showTagName = [NSString stringWithFormat:@"%@  ",showTagName];
    NSAttributedString *labelAttributes = [[NSAttributedString alloc] initWithString:showTagName];
    
    UIImage *img = [UIImage imageNamed:@"Signature_Icon.png"];
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = img;
    textAttachment.bounds = CGRectMake(0, -4.0, 20, 20);
    
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
    [attrStr appendAttributedString:labelAttributes];
    [attrStr replaceCharactersInRange:NSMakeRange(showTagName.length-1, 1) withAttributedString:attrStringWithImage];
    lblHashTagInfo.attributedText = attrStr;
}
#pragma mark - QBRefreshControlDelegate

- (void)refreshInMintDetail:(id)sender {
    if (_isFromTrendingTags)
    {
        pagenumbers = 1;
        [self getHashTagMints:_navigationHeaderText withPageNumber:pagenumbers deleteList:YES];
    }
    else  if (_isFromParticularShowroom)
    {
        pagenumbers = 1;
        [self getParticularShowrromMintswithPageNumber:pagenumbers deleted:YES];
    }
    else  if (_isFromSignatureShowroom)
    {
        pagenumbers = 1;
        [self getSignatureShowroomMints:pagenumbers deletePreviousList:YES];
    }
    
    
    else  if (_isFromSignatureShowroom)
    {
        pagenumbers = 1;
        [self getParticularShowrromMintswithPageNumber:pagenumbers deleted:YES];
    }
    
    else
    [self remintedSucssesfully];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        NSLog(@"Time");
        [refreshCarbon endRefreshing];
    });
}

- (void)dismissPopup
{
    AppDelegate *delegateApp = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegateApp.ysTabBarController.tabBar.hidden = NO;
   // delegateApp.lbl.hidden = NO;
    
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
        }];
    }
}

#pragma mark ----
#pragma mark - TrendingTags Realted Mints

-(void)getHashTagMints:(NSString *)hash_key withPageNumber : (int)pageNumber deleteList:(BOOL)delete
{
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:[hash_key stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""] forKey:@"hash_key"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page_number"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [YSAPICalls getHashTagMints:dicts ForSuccessionBlock:^(id newResponse)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         isLoading = NO;

         Generalmodel *gmodel = newResponse;
         if ([gmodel.status_code integerValue]){
             isDataFinishedInServer = NO;
             if (delete) {
                 [arrayMintdetailsList removeAllObjects];
             }
             
             if([gmodel.tempArray count]>0)
             {
                 [arrayMintdetailsList addObjectsFromArray: gmodel.tempArray];
             }
             
             if ([gmodel.tempArray2 count]>0) {
                 [arrayMintdetailsList addObjectsFromArray: gmodel.tempArray2];
                 
             }
             else
             {
                  isDataFinishedInServer = YES;
             }
         }
         [self.tblViewMintList reloadData];
         [self playVideo];

         
     }
                andFailureBlock:^(NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSLog(@"Failed from the server %@",[error description]);
     }];
}
#pragma mark - Server API Calls

-(void)getParticularShowrromMintswithPageNumber: (int)pageNumber deleted:(BOOL)isDeleted
{
    if (![YSSupport isNetworkRechable]) {
        return;
    }
    
    int NextPageNumber = 0;
    if ([arrayMintdetailsList count]) {
        
        NextPageNumber = [arrayMintdetailsList count]/30;
    }
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:_showtagText forKey:@"showroom_tag"];
    [dicts setValue:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page_number"];
   
    [YSAPICalls getParticularShowroom:dicts ForSuccessionBlock:^(id newResponse)
     {
         isLoading = NO;

         Generalmodel *gmodel = newResponse;
         
         if ([gmodel.status_code isEqualToString:@"1"])    // GET RELATED Mints
         {
             NSLog(@"** particular showroom **");
             if (isDeleted){
                 [arrayMintdetailsList removeAllObjects];
             }
             if([gmodel.tempArray count]>0)
             {
                 isDataFinishedInServer = NO;
                 [arrayMintdetailsList addObjectsFromArray: gmodel.tempArray];
                 [_tblViewMintList reloadData];
             }

             else
             {
                 isDataFinishedInServer = YES;
             }
         }
         
     }
        andFailureBlock:^(NSError *error)
     {
         NSLog(@"Failed from the server %@",[error description]);
     }];
     
     }




-(void)btnCircleClick:(id)sender {
    BaseFeedModel *feedlist = [arrayMintdetailsList objectAtIndex:[sender tag]];
    [YSAPICalls followUnfollowMint:feedlist withSuccessionBlock:^(id newResponse)  {
        Generalmodel *gModel = newResponse;
        if ([gModel.status_code isEqualToString:@"1"]) {
          if ([gModel.status_Msg isEqualToString:@"followed"])
             {
                   feedlist.is_user_followed = 1;
              } else
              {
                   feedlist.is_user_followed = 0;
              }

            if (_isFromProfile) {
                NSDictionary *dict   =   [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:feedlist.is_user_followed] forKey:@"FollowStatus"];
                [[NSNotificationCenter defaultCenter] postNotificationName:KPROFILEINCIRCLE object:nil userInfo:dict];

            }
            [self updateStatus:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id) status:feedlist.is_user_followed];
        }
        else {
            NSLog(@"Failiur Msg=%@",gModel.status_Msg);
        }
    } andFailureBlock:^(NSError *error){
        NSLog(@"Error = %@",error);
    }];
}


-(void)getGlobalSearchResult:(NSString *)text isDelete:(BOOL)delete
{
    
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&search_text=%@&page_number=%d",GETVALUE(CEO_AccessToken),[YSSupport escapedValue:_globalSearchText],pagenumbers] ;
    
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"/customshowroom/global_search_result",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    NSLog(@"searchRequest data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[YSSupport dictionaryFromResponseData:data] methodName:METHOD_MYSHOW_MINT];
         
         if (error !=  nil) {
             isLoading = NO;
         }
         if ([gmodel.status_code integerValue]){
             
             [arrayMintdetailsList addObjectsFromArray: gmodel.tempArray];
             [self.tblViewMintList reloadData];
             [self playVideo];
         }
     }];
}

-(void)updateStatus:(NSString*)currentuserid status:(int)currentStatus {
    
    
    for (int i = 0;i<[arrayMintdetailsList count]; i++){
        BaseFeedModel *feedlist = [arrayMintdetailsList objectAtIndex:i];
        if([currentuserid isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){
            feedlist.is_user_followed = currentStatus;
            [arrayMintdetailsList replaceObjectAtIndex:i withObject:feedlist];
        }
    }
    
    
    
    NSArray *paths = [_tblViewMintList indexPathsForVisibleRows];
    
    //  For getting the cells themselves
    
    for (NSIndexPath *path in paths) {
        
        
            BaseFeedModel *feedlist = [arrayMintdetailsList objectAtIndex:path.section];

        if([currentuserid isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){
                MintSectionHeaderView *sectionHeaderView = (MintSectionHeaderView *)[_tblViewMintList headerViewForSection:path.section];
                [sectionHeaderView.circlebtn  setTitle:(feedlist.is_user_followed)?@"IN CIRCLE":@"FOLLOW"     forState:UIControlStateNormal];
                [sectionHeaderView.circlebtn  setBackgroundColor:(feedlist.is_user_followed)?[UIColor whiteColor]:ORANGECOLOR];
                [sectionHeaderView.circlebtn  setTitleColor:(feedlist.is_user_followed)?ORANGECOLOR:[UIColor whiteColor] forState:UIControlStateNormal];
            if ((feedlist.is_user_followed) == 0) {
                [sectionHeaderView.circlebtn setImage:nil forState:UIControlStateNormal];
            }
            
            }

    }
    
    
 
}

-(void)btnProfilePhotoViewer:(UIButton *)btnProfile
{
    BaseFeedModel *feedlist = [arrayMintdetailsList objectAtIndex:[btnProfile tag]];
    [self showPopupWithModeldata:feedlist withrow:[btnProfile tag] isRemint:YES];
}

-(void)btnPostedProfilePhotoViewer:(UIButton *)btnProfile
{
    BaseFeedModel *feedlist = [arrayMintdetailsList objectAtIndex:[btnProfile tag]];
    [self showPopupWithModeldata:feedlist withrow:[btnProfile tag] isRemint:NO];
}

-(void)showPopupWithModeldata:(BaseFeedModel*)feeddetails withrow:(NSInteger)row isRemint:(BOOL)remint {
 
    
    [self gotoProfileView:[NSString stringWithFormat:@"%@",(remint)?feeddetails.remint_user_id:feeddetails.feed_user_id]];

    
    
}


-(void)gotoProfileView:(NSString *)selectedEnitity{

    if ([selectedEnitity isEqualToString:@"0"]) {
        [self gotoNouserFoundScreen];
    }
    else{
    MSProfileVC *prof = [[MSProfileVC alloc]initWithNibName:@"MSProfileVC" bundle:nil];
    prof.profileUserId = selectedEnitity;
    [self.navigationController pushViewController:prof animated:YES];
    }
 
}
-(void)gotoNouserFoundScreen
{
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"UserNotFoundVC" owner:nil options:nil];
    UIView *view = nibContents[0];
    UIButton *button = (UIButton *)[view viewWithTag:3];
    [button addTarget:self action:@selector(removeNouserfoundScreen:) forControlEvents:UIControlEventTouchUpInside];
    
    AppDelegate *delegate = MINTSHOWAPPDELEGATE;
    [delegate.window addSubview:view];
}
-(void)removeNouserfoundScreen:(UIButton *)button
{
    [[[button superview] superview] removeFromSuperview];
}

-(NSMutableAttributedString *)setStreakText:(NSString *)streakText imgName:(NSString *)imgName
{
    if (streakText.length)
    {
        streakText = [NSString stringWithFormat:@" %@",streakText];
        NSAttributedString *labelAttributes = [[NSAttributedString alloc] initWithString:streakText];
        
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
        textAttachment.image = [UIImage imageNamed:imgName];
        textAttachment.bounds = CGRectMake(0, -2.0, 10, 10);
        NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
        [attrStr appendAttributedString:attrStringWithImage];
        [attrStr appendAttributedString:labelAttributes];
        return attrStr;
    }
    
    return nil;
}

-(void)createMintstreakInfo:(NSString *)days lblView:(UILabel *)lblStreak streakText:(NSString *)streakText isMintRemint:(BOOL)isMintRemint
{
        if([days integerValue] <= 7)
        {
            lblStreak.attributedText = [self setStreakText:streakText imgName:@"streak_orange"];
            lblStreak.textColor = ORANGECOLOR;
        }
        else if([days integerValue] <= 14)
        {
            lblStreak.attributedText = [self setStreakText:streakText imgName:@"streak_green"];
            lblStreak.textColor = [UIColor colorWithRed:80.0/255.0 green:172.0/255.0 blue:89.0/255.0 alpha:1.0];

        }
        else if([days integerValue] <= 21)
        {
            lblStreak.attributedText = [self setStreakText:streakText imgName:@"streak_blue"];
            lblStreak.textColor = [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0];
        }
        else if([days integerValue] > 21)
        {
            lblStreak.attributedText = [self setStreakText:streakText imgName:@"streak_fire_blue"];
            lblStreak.textColor = [UIColor colorWithRed:0.0/255.0 green:148.0/255.0 blue:207.0/255.0 alpha:1.0];
        }
    
        lblStreak.tag = [days integerValue];
    
    [lblStreak setUserInteractionEnabled:YES];
    
    if (isMintRemint) {
        UITapGestureRecognizer* gestureremint = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mintSreakForRemintMint:)];
        [lblStreak addGestureRecognizer:gestureremint];
    }
    else
    {

        UITapGestureRecognizer* gestureTimeCategory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mintStreakSelecedFromHeader:)];
        [lblStreak addGestureRecognizer:gestureTimeCategory];
    }
    
}
-(void)mintSreakForRemintMint:(UITapGestureRecognizer *)guesture
{
    UILabel *lable = (UILabel*)guesture.view;
    
    //    NSIndexPath *selectedCellIndexPath = [NSIndexPath indexPathForRow:0 inSection:[[lable superview]tag]] ;
    //    [_tblViewMintList scrollToRowAtIndexPath:selectedCellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    if ([[lable superview]tag]  == [GETVALUE(CEO_UserId) integerValue]) {
        
            //[AppDelegate createStreakCoachingTips:[NSString stringWithFormat:@"%ld",(long)lable.tag]];
        AppDelegate *obj = MINTSHOWAPPDELEGATE;
        [obj createStreakPopup:[NSString stringWithFormat:@"%ld",(long)lable.tag]];
        
        return;
    }
    
    [self gotoProfileView:[NSString stringWithFormat:@"%d",[[lable superview] tag]]];

}
-(void)mintStreakSelecedFromHeader:(UITapGestureRecognizer *)guesture
{
    UILabel *lable = (UILabel*)guesture.view;
   
    if ([[[lable superview] superview] tag]  == [GETVALUE(CEO_UserId) integerValue]) {
       
        //    NSIndexPath *selectedCellIndexPath = [NSIndexPath indexPathForRow:0 inSection:[[lable superview]tag]] ;
        //    [_tblViewMintList scrollToRowAtIndexPath:selectedCellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        CGPoint touchPoint = [guesture locationInView:lable];
        
        if (touchPoint.x>15 && touchPoint.x<79) {
           // [AppDelegate createStreakCoachingTips:[NSString stringWithFormat:@"%ld",(long)lable.tag]];
            AppDelegate *obj = MINTSHOWAPPDELEGATE;
            [obj createStreakPopup:[NSString stringWithFormat:@"%ld",(long)lable.tag]];
        }
        return;
    }
    
     [self gotoProfileView:[NSString stringWithFormat:@"%d",(int)[[[lable superview] superview] tag]]];
}

#pragma mark- TableviewDelegate /data source methods

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MintSectionHeaderView *sectionHeaderView = [self.tblViewMintList dequeueReusableHeaderFooterViewWithIdentifier:MintSectionHeaderViewIdentifier];
    [sectionHeaderView.btnProfileImg removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    
    if (_isFromParticularShowroom || _isFromTrendingTags || _isFromSignatureShowroom) {
        
        [sectionHeaderView setUIforHeader];
    }
    
    BaseFeedModel *feedlist                     = [arrayMintdetailsList objectAtIndex:section];
    sectionHeaderView.tag = (feedlist.IsMintReminted)?[feedlist.remint_user_id integerValue]:[feedlist.feed_user_id integerValue];
    // [[sectionHeaderView subviews] lastObject].tag = section;
    //NSLog(@"[sectionHeaderView subviews] %@ ",[sectionHeaderView subviews]);
    
    //If the mit have been reminted by some user then the header need to change....
    
    if (feedlist.IsMintReminted) {
        
        [sectionHeaderView setUpRemintInfo:feedlist.remint_username remintedUserImage:feedlist.remint_userimg mintPostedTime:feedlist.remint_postedTime remintedUserId:feedlist.remint_user_id];
        [sectionHeaderView.btnProfileImg addTarget:self action:@selector(btnProfilePhotoViewer:) forControlEvents:UIControlEventTouchUpInside];
        [self setUserNameFrame:sectionHeaderView.lblUsername.text withView:sectionHeaderView.lblUsername];
        //For Selecting user name.
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(usernameSelecedFromHeader:)];
        // if labelView is not set userInteractionEnabled, you must do so
        [sectionHeaderView.lblUsername setUserInteractionEnabled:YES];
        [sectionHeaderView.lblUsername addGestureRecognizer:gesture];
        sectionHeaderView.lblUsername.tag = [feedlist.remint_user_id integerValue];
        
        
        if([feedlist.streak_days integerValue] > 1)
        {
            [self createMintstreakInfo:feedlist.streak_days lblView:sectionHeaderView.lblStreakInfo streakText:[NSString stringWithFormat:@"%@ day Streak",feedlist.streak_days] isMintRemint:NO];
            [sectionHeaderView.streakUserProfile setStreakColorWithDays:(int)[feedlist.streak_days integerValue]];
            sectionHeaderView.streakUserProfile.hidden     = NO;
            
        }
        else
        {
            [sectionHeaderView.streakUserProfile setStreakColorWithDays:0];
            sectionHeaderView.streakUserProfile.hidden     = YES;
        }
    }
    else
    {
        sectionHeaderView.lblStreakInfo.hidden            = ([feedlist.streak_days integerValue])?NO:YES;
        sectionHeaderView.streakUserProfile.hidden        = NO;
        sectionHeaderView.viewLightGray.layer.borderWidth = 2.5;
        sectionHeaderView.viewLightGray.layer.borderColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0].CGColor;
        sectionHeaderView.viewLightGray.layer.borderWidth = 2.5;
        sectionHeaderView.viewLightGray.layer.borderColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0].CGColor;
        
        if([feedlist.streak_days integerValue] > 1)
        {
            [self createMintstreakInfo:feedlist.streak_days lblView:sectionHeaderView.lblStreakInfo streakText:[NSString stringWithFormat:@"%@ day Streak",feedlist.streak_days] isMintRemint:NO];
            [sectionHeaderView.streakUserProfile setStreakColorWithDays:(int)[feedlist.streak_days integerValue]];
            
        }
        else
        {
            [sectionHeaderView.streakUserProfile setStreakColorWithDays:0];
            sectionHeaderView.streakUserProfile.hidden     = YES;
        }
        
        
        sectionHeaderView.lblUsername.text       = feedlist.feed_user_name;
        
        //NSString *someString = feedlist.feed_user_name;
        
        
        
        // float widthIs = ([someString boundingRectWithSize:CGSizeMake(182, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: sectionHeaderView.lblUsername.font} context:nil].size.width);
        
        //        if (widthIs < 100) {
        //            [sectionHeaderView.lblUsername sizeToFit];
        //        }
        
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithString: [NSString stringWithFormat:@"%@ | %@",feedlist.feed_posted_time,feedlist.category_type]];
        
        [text addAttribute:NSForegroundColorAttributeName
                     value:SIGNATURESHOWROOMCOLOR
                     range:NSMakeRange(feedlist.feed_posted_time.length+3, feedlist.category_type.length)];
        
        
        if (feedlist.minttype_exists == 1) {
            
            UITapGestureRecognizer* gestureTimeCategory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mintTypeSelecedFromHeader:)];
            [sectionHeaderView.lblTimeCategory setUserInteractionEnabled:YES];
            [sectionHeaderView.lblTimeCategory addGestureRecognizer:gestureTimeCategory];
        }
        sectionHeaderView.lblTimeCategory.attributedText = text;
        sectionHeaderView.userImage.mintImageURL    = [NSURL URLWithString:feedlist.feed_user_thumb_image];
        sectionHeaderView.circlebtn.hidden= ([GETVALUE(CEO_UserId) isEqualToString:feedlist.feed_user_id])?YES:NO;
        [sectionHeaderView.btnProfileImg addTarget:self action:@selector(btnPostedProfilePhotoViewer:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //For Selecting user name.
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(usernameSelecedFromHeader:)];
        // if labelView is not set userInteractionEnabled, you must do so
        [sectionHeaderView.lblUsername setUserInteractionEnabled:YES];
        [sectionHeaderView.lblUsername addGestureRecognizer:gesture];
        sectionHeaderView.lblUsername.tag = [feedlist.feed_user_id integerValue];
        [self setUserNameFrame:sectionHeaderView.lblUsername.text withView:sectionHeaderView.lblUsername];
        
        
    }
    
    // ***** same user means we should not show follow button
    //  in this if condition we have to pass correct key values
    if (feedlist.is_user_followed) {
        [sectionHeaderView.circlebtn setTitle: @"IN CIRCLE" forState: UIControlStateNormal];
        [sectionHeaderView.circlebtn setBackgroundColor:[UIColor whiteColor]];
        [sectionHeaderView.circlebtn setTitleColor:ORANGECOLOR forState:UIControlStateNormal];
    } else  {
        [sectionHeaderView.circlebtn setTitle: @"FOLLOW" forState: UIControlStateNormal];
        [sectionHeaderView.circlebtn setImage:nil forState:UIControlStateNormal];
        [sectionHeaderView.circlebtn setBackgroundColor:ORANGECOLOR];
        [sectionHeaderView.circlebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    
    [sectionHeaderView.backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [sectionHeaderView.circlebtn addTarget:self action:@selector(btnCircleClick:) forControlEvents:UIControlEventTouchUpInside];
    sectionHeaderView.lblSeparator.backgroundColor = (section == 0)?[UIColor clearColor]:[UIColor colorWithRed:(9.0/255.0) green:(26.0/255.0) blue:(53.0/255.0) alpha:1.0];
    sectionHeaderView.btnProfileImg.tag = section;
    sectionHeaderView.circlebtn.tag = section;
    sectionHeaderView.section       = section;
    
    return sectionHeaderView;
}

-(void)setUserNameFrame:(NSString *)someString withView:(UILabel *)lblView
{
    CGFloat textWidth  = [someString boundingRectWithSize:CGSizeMake(182, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:lblView.font} context:nil].size.width+5;
    
    //    lblView.backgroundColor = [UIColor redColor];
    CGRect rectLbl = lblView.frame;
    
    if(textWidth<182)
        rectLbl.size.width = textWidth;
    else
        rectLbl.size.width = 182;
    
    [lblView setFrame:rectLbl];
}
-(void)usernameSelecedFromHeader:(UITapGestureRecognizer *)guesture
{
    UILabel *lable = (UILabel*)guesture.view;
 
    [self gotoProfileView:[NSString stringWithFormat:@"%d",(int)[lable tag]]];
}

-(void)mintTypeSelecedFromHeader:(UITapGestureRecognizer *)guesture
{
    UILabel *lable = (UILabel*)guesture.view;
    CGPoint touchPoint = [guesture locationInView:lable];

    NSArray *myArray = [lable.text componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" | "]];
    if (touchPoint.x>15 && touchPoint.x<120) {
        
        if (![[myArray lastObject] isEqualToString:@"Only"]) {
            MSPSignatureShowroomVC *vcShowRoom = [[MSPSignatureShowroomVC alloc]initWithNibName:@"MSPSignatureShowroomVC" bundle:nil];
            vcShowRoom.isFromDiscoverShowroom = NO;
            vcShowRoom.selectedShowroom = [myArray lastObject];
            [self.navigationController pushViewController:vcShowRoom animated:YES];
        }
        
     
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrayMintdetailsList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier2 = @"MintDetailsCell";
    
    BaseFeedModel *newModel    = [arrayMintdetailsList objectAtIndex:indexPath.section];
    MintDetailsCell  *cell = (MintDetailsCell *)[self.tblViewMintList dequeueReusableCellWithIdentifier:CellIdentifier2];
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:CellIdentifier2 owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
   
    [cell.mintThumbnailView setBackgroundColor:[YSSupport randomColor]];
   
    cell.aDelegate                  = self;
   
    ([newModel.feed_posted_edited_text isEqualToString:@"Edited"])?(cell.descriptionView.isEditedMint = YES):(cell.descriptionView.isEditedMint = NO);
    
    if (newModel.is_user_inspired == 1) {

    UITapGestureRecognizer* gestureTimeCategory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mintTypeSelecedFromHeader:)];
    [cell.lblMintPostedTime setUserInteractionEnabled:YES];
    [cell.lblMintPostedTime addGestureRecognizer:gestureTimeCategory];
    }
    
    
    [cell.btnHighFiveCount addTarget:self     action:@selector(getHighFivedUsersList:)        forControlEvents:UIControlEventTouchUpInside];
    [cell.btnRemintCount   addTarget:self      action:@selector(getRemintUsersList:)          forControlEvents:UIControlEventTouchUpInside];
    [cell.btnHighfive      addTarget:self      action:@selector(highFive:)                    forControlEvents:UIControlEventTouchUpInside];
    [cell.btnRemint        addTarget:self      action:@selector(remint:)                      forControlEvents:UIControlEventTouchUpInside];
    [cell.btnMore          addTarget:self      action:@selector(showMoreButtonInfo:)          forControlEvents:UIControlEventTouchUpInside];
    [cell.btnShare         addTarget:self      action:@selector(shareTapped:)                 forControlEvents:UIControlEventTouchUpInside];
    [cell.btnAddcomplement addTarget:self      action:@selector(viewAllComplement:)           forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCommmentCount addTarget:self      action:@selector(viewAllComplement:)           forControlEvents:UIControlEventTouchUpInside];
    [cell.btnMintPostedUser addTarget:self     action:@selector(btnPostedProfilePhotoViewer:)           forControlEvents:UIControlEventTouchUpInside];

    
    [cell setInfoWithAlignment : newModel index:indexPath isSingleRecord:_isToshowSingleRecord];
    cell.tag = indexPath.row;
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:@"Section %zd", section];
    // return arrayMintdetailsList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 52.0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BaseFeedModel *newModel = [arrayMintdetailsList objectAtIndex:indexPath.section];
    CGFloat rowHeight        = 0.0;
    CGFloat remintviewmessageHeight = 0.0;
    
    CGFloat widthCell      = [UIScreen mainScreen].bounds.size.width;
    CGSize rctSizeOriginal = CGSizeMake(newModel.feed_thumb_Width , newModel.feed_thumb_height);
    double scale           = widthCell / rctSizeOriginal.width;
    CGSize rctSizeFinal    = CGSizeMake(rctSizeOriginal.width*scale,rctSizeOriginal.height*scale);
    
    CGFloat descriptionHeight = 0.0;
    
    // Changed
    /*! get height of reminted message of reminted mint
     */
    
    
    NSString *OriginalFrontendText    = ([newModel.feed_posted_edited_text isEqualToString:@"Edited"])?[NSString stringWithFormat:@"%@ Edited",[newModel.feed_description emojizedString]]:[newModel.feed_description emojizedString];
    
    UIFont *font                      = [UIFont fontWithName:@"HelveticaNeue" size:14];
    
    
    descriptionHeight                 = ([OriginalFrontendText length])?([OriginalFrontendText boundingRectWithSize:CGSizeMake(self.view.frame.size.width-16, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: font} context:nil].size.height):5;
    
    
    rowHeight = (newModel.mintType == MSMintTypeText)?(rowHeight + descriptionHeight + KBUTTONCOMPONENT +5) :(rctSizeFinal.height+descriptionHeight + KBUTTONCOMPONENT +5) ;
    
    
    
    if([newModel.feed_comment_count intValue]> 0)  {
        if(newModel.IsMintReminted && (newModel.mintType == MSMintTypeImage)) {
            rowHeight = rowHeight+5;
        }
        rowHeight+= newModel.feed_comment_height;
        rowHeight+= KVIEWALLCOMPLIMINT;
    }
    
    if ((newModel.IsMintReminted)){
        remintviewmessageHeight = [self getRemintedMessageHeight:[newModel.remint_message emojizedString]];
        rowHeight = rowHeight + remintviewmessageHeight +50;
    }
    
    int locationHeight = ([newModel.feed_posted_location length])?5:0;
    
    if (_isToshowSingleRecord)
    {
        int singleRowTotalHeight = (rowHeight+50)+locationHeight+remintviewmessageHeight;
        if(newModel.mintType == MSMintTypeText && _isToshowSingleRecord)
        {
            singleRowTotalHeight = (singleRowTotalHeight<self.view.frame.size.height-100)?self.view.frame.size.height-50:singleRowTotalHeight;
        }
        
        return  singleRowTotalHeight;
    }
    
    int arrear = (newModel.commentsArray.count>0)?7:15;
    
    return rowHeight+arrear+locationHeight;
}
#pragma mark--
#pragma mark- Get height info

-(CGFloat)getDescriptionHeight : (NSString *) descriptiontext{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    CGFloat commentTextHeight  = [descriptiontext boundingRectWithSize:CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds)-20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return  (commentTextHeight < KDESCRIPTIONHEIGHT )?KDESCRIPTIONHEIGHT:commentTextHeight;
}
/*! this method is used to get height of reminted message of reminted mint
 */
-(CGFloat)getRemintedMessageHeight : (NSString *) reminttext{
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:11];
    
    float remintTextHeight  = [reminttext boundingRectWithSize:CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds)-8, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size.height;
    return  (remintTextHeight < 20 )?20:remintTextHeight;
}

#pragma mark - GET USER LIST (HIGHFIVE, NOMINATED AND REMINT)

-(void)getHighFivedUsersList : (UIButton *) sender
{
    BaseFeedModel *fmodel  = [arrayMintdetailsList objectAtIndex:sender.tag];
    if ([fmodel.feed_like_count integerValue]>0)
    {
        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeHighFive numberofPeoples:[fmodel.feed_like_count intValue] feedId:fmodel.feed_id delegate:self];
        
    }
}

- (void)getNominationUsersList:(UIButton *)sender {
    BaseFeedModel *fmodel  = [arrayMintdetailsList objectAtIndex:sender.tag];
    if ([fmodel.feed_nomination_count integerValue]>0) {

        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeNomintation numberofPeoples:[fmodel.feed_nomination_count intValue] feedId:fmodel.feed_id delegate:self];

    }
}

- (void)getRemintUsersList:(UIButton *)sender {
    BaseFeedModel *fmodel  = [arrayMintdetailsList objectAtIndex:sender.tag];
    if ([fmodel.remint_count integerValue]>0) {

        [UsersListView showUserListViewTo:self.view.window viewTitle:MSUserlistTypeRemint numberofPeoples:[fmodel.remint_count intValue] feedId:fmodel.feed_id delegate:self];

    }
}

-(void)selectedUserIdFromHighfiveList:(NSString *)selectdUserId
{

    [self gotoProfileView:selectdUserId];

}
- (void)textDidChange:(MessageComposerView*)commentView
{
    //    NSLog(@"total charecter= %d", (int)commentView.counterCount);
  
}



- (BOOL)isIndexPathVisible:(NSIndexPath*)indexPath{
    NSArray *visiblePaths = [self.tblViewMintList indexPathsForVisibleRows];
    for (NSIndexPath *currentIndex in visiblePaths) {
        NSComparisonResult result = [indexPath compare:currentIndex];
        if(result == NSOrderedSame)  {
            NSLog(@"Visible");
            return YES;
        }
    }
    return NO;
}

-(IBAction)backAction:(id)sender
{

    [self.navigationController popViewControllerAnimated:YES ];
}
#pragma mark ---  Hifive Clicked ---

-(void)highFive : (UIButton *) sender
{
    NSLog(@"high five is %li ",(long)sender.tag);
    [sender setUserInteractionEnabled:NO];
    [self hifiveClicked:sender];
}
- (IBAction)hifiveClicked:(UIButton*)sender {
    
    [YSSupport  runSpinAnimationOnView:sender duration:1 rotations:1 repeat:1];

    BaseFeedModel *fmodel  = [arrayMintdetailsList objectAtIndex:sender.tag];
    [self changeHifiveStatusForRow:sender.tag];
    
    [YSAPICalls hifiveMint:fmodel withSuccessionBlock:^(id response) {
        [sender setUserInteractionEnabled:YES];

        Generalmodel *newResponse=response;
        if ([newResponse.status_code intValue] == 1) {
            
                   }
        else {
            [self changeHifiveStatusForRow:sender.tag];
        }
        if ( _isToshowSingleRecord&& self.isFromDiscoverTrending) {
       
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:fmodel,@"model" ,nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SINGLEMINTUPDATE" object:nil userInfo:dict] ;
        }
        
    } andFailureBlock:^(NSError *error)
    {
        [sender setUserInteractionEnabled:YES];

        [self changeHifiveStatusForRow:sender.tag];
    }];
}


-(void)changeHifiveStatusForRow:(NSInteger)row {
    BaseFeedModel *fmodel                   =   [arrayMintdetailsList objectAtIndex:row];
    int hiveCount                       =   [fmodel.feed_like_count intValue];
    hiveCount                           =   (fmodel.is_user_liked==0)?(hiveCount+1):(hiveCount-1);
    fmodel.feed_like_count              =   [NSString stringWithFormat:@"%d",hiveCount];
    fmodel.is_user_liked                =   (fmodel.is_user_liked==0)?1:0;
    
    [arrayMintdetailsList replaceObjectAtIndex:row withObject:fmodel];
    MintDetailsCell  *cell = (MintDetailsCell *)[self.tblViewMintList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:row]];
    [cell changeHifiveStatus:fmodel];
    if ([self.delegate respondsToSelector:@selector(highFiveMintOfmintDetails:andCurerntValue:countValue:)]) {
        [self.delegate highFiveMintOfmintDetails :fmodel.feed_id   andCurerntValue :[NSString stringWithFormat:@"%d",fmodel.is_user_liked] countValue: fmodel.feed_like_count];
    }
}


#pragma mark - Nomination


- (void)nomination :(UIButton *)sender{
    [YSSupport  runSpinAnimationOnView:sender duration:1 rotations:1 repeat:1];

    [self NominateClicked:sender];
    
}
- (IBAction)NominateClicked:(UIButton*)sender {
    BaseFeedModel *fmodel  = [arrayMintdetailsList objectAtIndex:sender.tag];
    [self changenominateStatusForRow:sender.tag];
    [YSAPICalls nominateMint:fmodel withSuccessionBlock:^(Generalmodel *newResponse)  {
        
        
        
        
         
     }  andFailureBlock:^(NSError *error) {
                 [self changenominateStatusForRow:sender.tag];
                 NSLog(@"error code = %@",error);
    }];
}


-(void)changenominateStatusForRow:(NSInteger)row {
    BaseFeedModel *fmodel   =   [arrayMintdetailsList objectAtIndex:row];
    int nominateCount   =   [fmodel.feed_nomination_count intValue];
    nominateCount       =   (fmodel.is_user_nominated==0)?(nominateCount+1):(nominateCount-1);
    fmodel.feed_nomination_count   =   [NSString stringWithFormat:@"%d",nominateCount];
    fmodel.is_user_nominated        =   (fmodel.is_user_nominated==0)?1:0;
    [arrayMintdetailsList replaceObjectAtIndex:row withObject:fmodel];
    MintDetailsCell  *cell = (MintDetailsCell *)[self.tblViewMintList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:row]];
    [cell changeNominationStatus:fmodel];
}




#pragma mark - Remint
- (void) displayContentController: (UIViewController*) content;{
    [self addChildViewController:content];
    [content  willMoveToParentViewController:self];
    [content   didMoveToParentViewController:self];
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
}
-(void)shareTapped : (UIButton *)sender {
    //self.useBlurForPopup = YES;
    BaseFeedModel *model =  (BaseFeedModel *)[arrayMintdetailsList objectAtIndex:sender.tag];
    
    AppDelegate *appdelagate  = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appdelagate.ysTabBarController .tabBar setHidden:YES];
   // appdelagate.lbl.hidden = YES;
    MSShareVC *vc = [[MSShareVC alloc]initWithNibName:@"MSShareVC" bundle:nil];
    [vc setCurrentScreen:0];
    vc.feedID = model.feed_id;
    vc.txtDescription = model.feed_description;
    vc.linkUrl = model.mintdetail_url;
    vc.imgUrl = model.feed_thumb_image;
    vc.isFromMintDetail = YES;
    vc.feeddata = model;
    [self presentViewController:vc animated:YES completion:^{
        
    }];

}

-(void)remint : (UIButton *) sender{
    BaseFeedModel *feedData         = [arrayMintdetailsList objectAtIndex:[sender tag]];
    MSRemintVC *reminytVW       = [[MSRemintVC alloc]initWithNibName:@"MSRemintVC" bundle:nil];
    reminytVW.aDelegate = self;
    reminytVW.feedData = feedData;
    
    [self presentViewController:reminytVW animated:NO completion:^{
        
    }];
}

-(void)showMoreButtonInfo : (UIButton *) button{
    NSString *destructionForMint            = @"Edit Mint"; //Action Sheet Title
    NSString *other1            = @"Mute Mint"; //Action Sheet Button Titles
    NSString *other2            = @"Mute User";
    NSString *other3            = @"Report Inappropriate";
    NSString *other4            = @"Delete Mint";
    NSString *other6            = @"Delete Remint";
    NSString *cancelTitle       = @"Cancel";
    NSString *destructionForReMint            = @"Edit Remint"; //Action Sheet Title

    
    BaseFeedModel *model = [arrayMintdetailsList objectAtIndex:button.tag];
    NSLog(@"reminted user %@ and is it reminted ? %@,logedin user %@",model.remint_user_id,model.remint_val,GETVALUE(CEO_UserId));
    UIActionSheet *popup  = [[UIActionSheet alloc]
                             initWithTitle:nil
                             delegate:self
                             cancelButtonTitle:cancelTitle
                             destructiveButtonTitle:(model.IsMintReminted)?(([[model remint_user_id] isEqualToString:GETVALUE(CEO_UserId)])?destructionForReMint:nil):(([[model feed_user_id] isEqualToString:GETVALUE(CEO_UserId)])?destructionForMint:nil)
                             otherButtonTitles:nil];
    
    NSArray *array;

    if (model.IsMintReminted) {
        
        if ([model.remint_user_id isEqualToString:GETVALUE(CEO_UserId)]) {
            array =@[other6];
        }
        else
        {
            array = @[other1,other2,other3];
        }
        
    }else{
    
 
            array = ([[[arrayMintdetailsList objectAtIndex:button.tag] feed_user_id] isEqualToString:GETVALUE(CEO_UserId)])?@[other4]:@[other1,other2,other3];
    
    
    }
    for (NSString *title in array) {
        [popup addButtonWithTitle:title];
    }

    [popup setBackgroundColor:[UIColor whiteColor]];
    popup.tag  = button.tag;
    
    if ([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)  {
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    } else if ([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad){
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)  {
            [popup showFromRect:button.frame inView:self.view animated:YES];
        }  else  {
            [popup showInView:[UIApplication sharedApplication].keyWindow];
        }
    }
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex{
        if (![[popup buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"]) {
        if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete Mint"])
        {
            deleteType = @"0";
            [self showConfirmationAlert:@"Delete this Mint?" message:@"Your Mint will be Deleted." mintTag:(int)popup.tag];
        }else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:@"Report Inappropriate"]) {
            [self setFlag:(int)popup.tag];
        }else  if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:@"Mute User"]) {
            [self muteUser:(int)popup.tag];
        }else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:@"Mute Mint"]) {
            [self muteMint:(int)popup.tag];
        }
            
        else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:@"Edit Mint"]) {
            [self EditMint:(int)popup.tag];
        }
        else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:@"Edit Remint"]) {
            [self EditRemintMint:(int)popup.tag];
        }
        else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete Remint"])
        {
            deleteType = @"1";
            [self showConfirmationAlert:@"Delete this ReMint?" message:@"Your ReMint will be Deleted." mintTag:(int)popup.tag];

        }
            //@"Cancel this Mint?"
            //@"Your Mint post will be Deleted."
    }
    [popup dismissWithClickedButtonIndex:buttonIndex animated:YES];
    
}
-(void)showConfirmationAlert:(NSString *)titleText message:(NSString *)message mintTag : (int)mintTag
{

    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:titleText
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert.view setTintColor:[UIColor blackColor]];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Yes"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction * action)
                         {
                             [self deleteMint:mintTag];
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Keep"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:cancel];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];


}

-(void)EditRemintMint : (int) tag{

    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;

    aDelegate.selectedIndex =  [NSString stringWithFormat:@"%ld",(long)tag];

    BaseFeedModel *feedData         = [arrayMintdetailsList objectAtIndex:tag];
    MSRemintVC *reminytVW       = [[MSRemintVC alloc]initWithNibName:@"MSRemintVC" bundle:nil];
    reminytVW.aDelegate = self;
    reminytVW.feedData = feedData;
    reminytVW.isEditRemint = YES;
    //    AppDelegate *appdelagate  = (AppDelegate *)[UIApplication sharedApplication].delegate;
    //    [appdelagate.window addSubview:reminytVW.view];
    
    [self presentViewController:reminytVW animated:NO completion:^{
        
    }];
}

- (void)EditMint:(int)tag
{
   
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndex        =  [NSString stringWithFormat:@"%ld",(long)tag];
    AddMintViewController *addMint = [[AddMintViewController alloc]initWithNibName:@"AddMintViewController" bundle:nil];
    addMint.isFromMintDetail       = YES;
    addMint.feedModelData          = [arrayMintdetailsList objectAtIndex:tag];
    addMint.hidesBottomBarWhenPushed  = YES;
    [self.navigationController pushViewController:addMint animated:NO];
}

#pragma mark --- SHow Room Mint Details API Call ---


-(void)getMintsProfile:(int)pageNumber isDelete:(BOOL)isDelete pull_request_time:(BOOL)pullRequestNeedToAdd
{
    isLoading = YES;
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:@"2"                       forKey:@"mint_list_type"];
    [dicts setValue:self.profileuser_id                forKey:@"profile_user_id"];
    
    [dicts setValue:[NSString stringWithFormat:@"%d",pageNumber] forKey:@"page_number"];
    [YSAPICalls getMintsForMyShowTabwithDict:dicts SuccessionBlock:^(id newResponse){
        isLoading = NO;
        Generalmodel *model = newResponse;
        if ([model.status_code integerValue]){
            
            
            
            if ([[SPHSingletonClass sharedSingletonClass] isUser_mint_streak]) {
                
                AppDelegate *obj = MINTSHOWAPPDELEGATE;
                [obj createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];
                
                [[SPHSingletonClass sharedSingletonClass]
                 setIsUser_mint_streak:NO];
                
            }
            
            
            
            NSArray *array = [NSArray arrayWithArray:arrayMintdetailsList];
            [array enumerateObjectsUsingBlock:^(BaseFeedModel* modelObject, NSUInteger idx, BOOL *stop) {
                NSLog(@"%@", modelObject);
                
                if (modelObject.IsMintReminted || [modelObject.feed_user_id isEqualToString:GETVALUE(CEO_UserId)]) {
                    
                    if ([[SPHSingletonClass sharedSingletonClass] login_user_streak] != [modelObject.streak_days integerValue]) {
                        
                        modelObject.streak_days = [NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]];
                        [arrayMintdetailsList replaceObjectAtIndex:idx withObject:modelObject];
                        
                    }
                    
                }
                
                
            }];
            
            
            [arrayMintdetailsList addObjectsFromArray: model.tempArray];
            [self.tblViewMintList reloadData];
            [self playVideo];
            isLoading = NO;
        }
    } andFailureBlock:^(NSError *error){
        
        NSLog(@"Failed from the server %@",[error description]);
    }];
}


- (void)mintDetail: (NSString *)mintRowId pagenumber:(int)pageNo{
   
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
    [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
    [dicts setValue:_mint_row_id forKey:@"mintRowId"];
    [dicts setValue:([_profileuser_id length])?_profileuser_id:@"" forKey:@"user_id"];

    
    NSLog(@"Request--%@",[dicts description]);
    [YSAPICalls myshowMintDetails:dicts ForSuccessionBlock:^(id newResponse) {
        isLoading = NO;
        NSLog(@"Response %@",newResponse);
        Generalmodel *model = newResponse;
        if ([model.status_code isEqualToString:@"1"]) {
            
            if (model.tempArray.count >0) {
                
            
            
            if ([[SPHSingletonClass sharedSingletonClass] isUser_mint_streak]) {
                
                AppDelegate *obj = MINTSHOWAPPDELEGATE;
                [obj createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];

                [[SPHSingletonClass sharedSingletonClass]
                 setIsUser_mint_streak:NO];
                
            }
            


            NSArray *array = [NSArray arrayWithArray:arrayMintdetailsList];
            [array enumerateObjectsUsingBlock:^(BaseFeedModel* modelObject, NSUInteger idx, BOOL *stop) {
                NSLog(@"%@", modelObject);
                
                if (modelObject.IsMintReminted || [modelObject.feed_user_id isEqualToString:GETVALUE(CEO_UserId)]) {
                    
                    if ([[SPHSingletonClass sharedSingletonClass] login_user_streak] != [modelObject.streak_days integerValue]) {
                        
                        modelObject.streak_days = [NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]];
                        [arrayMintdetailsList replaceObjectAtIndex:idx withObject:modelObject];
                        
                    }
                    
                }
                
                
            }];
            
            
            [arrayMintdetailsList addObjectsFromArray: model.tempArray];
            [self.tblViewMintList reloadData];
            [self playVideo];
            }
            
            else{

                isDataFinishedInServer = YES;
            }
            
        }
    } andFailureBlock:^(NSError *error) {
        isLoading = NO;
    }];
}
#pragma mark --- SHow Room Mint Details API Call ---

- (void)deleteMint:  (int) tag{
  
    
    if (![YSSupport isNetworkRechable])
        return;
    BaseFeedModel *feedmodel    = [arrayMintdetailsList objectAtIndex:tag];
    NSDictionary *dicts = [NSDictionary dictionaryWithObjectsAndKeys:GETVALUE(CEO_AccessToken),@"access_token",feedmodel.feed_id,@"feed_id",deleteType,@"deletetype",nil];

    [YSAPICalls deleteMint:dicts ForSuccessionBlock:^(id newResponse) {
        NSLog(@"Response %@",newResponse);
        Generalmodel *model = newResponse;
        if ([model.status_code isEqualToString:@"1"]) {
                [arrayMintdetailsList removeObjectAtIndex:tag];
                [self.tblViewMintList reloadData];
            if (_isFromSignatureShowroom) {
                [self.delegate updateMintCountForShowroom:feedmodel.feed_id];

            }
            else
                [self.delegate updateThePreviouseScreenForMintManagament:feedmodel.feed_id];
                
            if ([arrayMintdetailsList count] == 0) {
                [self.navigationController popViewControllerAnimated:NO];
            }
        }
    } andFailureBlock:^(NSError *error) {
        
    }];
}



-(void)setFlag : (int) tag{
   
    
    if (![YSSupport isNetworkRechable])
        return;
    BaseFeedModel *feedmodel = [arrayMintdetailsList objectAtIndex:tag];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&feed_id=%@",GETVALUE(CEO_AccessToken),feedmodel.feed_id] dataUsingEncoding:NSUTF8StringEncoding];
    [YSAPICalls flagMint:submitData  ForSuccessionBlock:^(id newResponse) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         Generalmodel *gmodel = newResponse;
         if ([gmodel.status_code integerValue])  {
             [arrayMintdetailsList removeObjectAtIndex:tag];
             [self.tblViewMintList reloadData];
             [self.delegate        updateThePreviouseScreenForMintManagament:feedmodel.feed_id];
             
             if ([arrayMintdetailsList count] == 0) {
                 [self.navigationController popViewControllerAnimated:NO];
             }
         }
    } andFailureBlock:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}


-(void)muteMint : (int) tag {
   
    if (![YSSupport isNetworkRechable])
        return;
    BaseFeedModel *feedmodel = [arrayMintdetailsList objectAtIndex:tag];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //1 => Text Mint, 2 => Photo/Video Mint
    //1 => Mute, 2 => UnMute
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&mint_unmute=%@&mint_id=%@",GETVALUE(CEO_AccessToken),@"1",feedmodel.feed_id] dataUsingEncoding:NSUTF8StringEncoding];
    [YSAPICalls muteAMint:submitData  ForSuccessionBlock:^(id newResponse) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        Generalmodel *gmodel = newResponse;
         if ([gmodel.status_code isEqualToString:@"1"])  {
             [arrayMintdetailsList removeObjectAtIndex:tag];
             [self.tblViewMintList reloadData];
              [self.delegate updateThePreviouseScreenForMintManagament:feedmodel.feed_id];
             if ([arrayMintdetailsList count] == 0) {
                 [self.navigationController popViewControllerAnimated:NO];
             }
         }
    }  andFailureBlock:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(void)muteUser : (int) tag {
   
    
    if (![YSSupport isNetworkRechable])
        return;
    BaseFeedModel *feedmodel = [arrayMintdetailsList objectAtIndex:tag];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData*  submitData    = [[NSString stringWithFormat:@"access_token=%@&mint_unmute=%@&mute_user_id=%@",GETVALUE(CEO_AccessToken),@"1",(feedmodel.IsMintReminted)?feedmodel.remint_user_id:feedmodel.feed_user_id] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"result is %@",str);
    [YSAPICalls muteAUser:submitData  ForSuccessionBlock:^(id newResponse) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         Generalmodel *gmodel = newResponse;
         if ([gmodel.status_code integerValue]) {
                     [self updateForMutedUserMint:(feedmodel.IsMintReminted)?feedmodel.remint_user_id:feedmodel.feed_user_id];

                     [self.delegate muteUserOfmintDetails:(feedmodel.IsMintReminted)?feedmodel.remint_user_id:feedmodel.feed_user_id];
                      if ([arrayMintdetailsList count] == 0) {
                              [self.navigationController popViewControllerAnimated:NO];
                           }
                          }
     }  andFailureBlock:^(NSError *error)  {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

//
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    
//    float scrollViewHeight          = scrollView.frame.size.height;
//    float scrollContentSizeHeight   = scrollView.contentSize.height;
//    float scrollOffset              = scrollView.contentOffset.y;
//    
//    if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
//    {
//        pagenumbers = pagenumbers +1;
//        NSLog(@"*** Scroll End ****");
//        ScrolVal = YES;
//        if (_isFromTrendingTags)
//            [self getHashTagMints:_trendingTag withPageNumber:pagenumbers];
//        else
//        [self mintDetail:_mint_row_id pagenumber:1]; // mintDetail: (int)row_id pagenumber:(int)pageNo
//    }
//}

//  Outside the implementation block:
static const CGFloat kNewPageLoadScrollPercentageThreshold = 0.70;

static BOOL ShouldLoadNextPage(UITableView *collectionView)
{
    CGFloat yOffset = collectionView.contentOffset.y;
    CGFloat height = collectionView.contentSize.height - CGRectGetHeight(collectionView.frame);
    return yOffset / height > kNewPageLoadScrollPercentageThreshold;
}


-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    [self playVideo];

}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    BOOL shouldLoadNextPage = ShouldLoadNextPage(self.tblViewMintList);
    if (shouldLoadNextPage && !_isToshowSingleRecord)  {
        firstTimeApiCall = @"NO";
        scrollDirection  = @"down";
        ScrolVal         = YES;
        _mint_row_id     = [[arrayMintdetailsList lastObject] feed_id];
        
        if(!isLoading && !isDataFinishedInServer)
        {
            pagenumbers      = pagenumbers +1;
            isLoading        = YES;
            if (_isFromTrendingTags)
                [self getHashTagMints:_navigationHeaderText withPageNumber:pagenumbers deleteList:NO];
            else  if (_isFromParticularShowroom)
                [self getParticularShowrromMintswithPageNumber:pagenumbers deleted:NO];
            else  if (_isFromQueryResultPage)
                [self getGlobalSearchResult:@"jiten" isDelete:NO];
            else  if (_isFromSignatureShowroom)
            {
                [self getSignatureShowroomMints:pagenumbers deletePreviousList:NO];
            }
//            else  if (_isFromProfile)
//            {
//                [self getMintsProfile:pagenumbers isDelete:NO pull_request_time:NO];
//            }
            else
            [self mintDetail:_mint_row_id pagenumber:pagenumbers];

        }
        
    }
    if(![scrollView isDecelerating] && ![scrollView isDragging]){
          [self playVideo];
    }

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(!decelerate){
        [self playVideo];
    }
}
-(BOOL)isnetAvaialbel{
    return [YSSupport isNetworkRechable];
}


-(void)stopVideo{
    if(arrayMintdetailsList.count==0||![self isnetAvaialbel]){
        return;
    }
    NSArray *paths = [self.tblViewMintList indexPathsForVisibleRows];
    for (NSIndexPath *path in paths) {
        MintDetailsCell *cell = [self.tblViewMintList cellForRowAtIndexPath:path];
        if(cell.videoPlayer){
            [cell.videoPlayer stop];
            [cell.videoPlayer removeFromSuperview];
            cell.videoPlayer = nil;
        }
    }
}


-(void)playVideo {
    if(arrayMintdetailsList.count==0||![self isnetAvaialbel]){
        return;
    }
    NSArray *paths = [self.tblViewMintList indexPathsForVisibleRows];
    for (NSIndexPath *path in paths) {
        MintDetailsCell *cell = [self.tblViewMintList cellForRowAtIndexPath:path];
        CGRect ccellRect = [[(AppDelegate *)[[UIApplication sharedApplication] delegate] window] convertRect:cell.bounds fromView:cell];
        if(ccellRect.origin.y>-100 && ccellRect.origin.y<350){
            BaseFeedModel *newModel = [arrayMintdetailsList objectAtIndex:path.section];
            if(!(newModel.mintType== MSMintTypeText || newModel.mintType == MSMintTypeImage)){
                if(!cell.videoPlayer)
                    [cell setPlayerInfoWithAlignment:newModel index:path];
                else
                    [cell.videoPlayer play];
            }
        }else {
            if(cell.videoPlayer){
            [cell.videoPlayer stop];
            //[cell.videoPlayer removeFromSuperview];
           // cell.videoPlayer = nil;
            }
        }
    }
}
-(void)textPassToKeyboard:(NSString *)str{
    
}
-(void)commentUpdatedSuccesFullyDetails : (NSNotification *) notification {
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    if (aDelegate.isCommentActionUpdate && self.isFromDiscoverTrending) {
        return;
    }
    
    
    
    CommentModel *commentObject = (CommentModel *) [[notification valueForKey:@"userInfo"] valueForKey:@"CommentData"];
    
    
    for (int i = 0; i < arrayMintdetailsList.count; i++) {
        BaseFeedModel *model = [arrayMintdetailsList objectAtIndex:i];
        
        
        if ([model.feed_id isEqualToString:[[notification valueForKey:@"userInfo"] valueForKey:@"FEEDID"]]) {
            
            
            if ([[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:DELETECOMMENT]) {
                for (int i=0; i<[model.commentsArray count]; i++) {
                    if ([[[model.commentsArray objectAtIndex:i] achieveCommentId]isEqualToString:commentObject.achieveCommentId])  {
                        [model.commentsArray removeObjectAtIndex:i];
                        model.feed_comment_count = [NSString stringWithFormat:@"%d",(int)(([model.feed_comment_count integerValue])?([model.feed_comment_count integerValue]-1):1)];
                    }
                }
            }
            
            
            
            else if ([[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:ADDCOMMENT]){
                
                if (_isFromQueryResultPage || _isFromTrendingTags || _isFromParticularShowroom || _isFromActivity || _isFromSignatureShowroom
                    ) {
                    [model.commentsArray insertObject:commentObject atIndex:0];
                    model.feed_comment_count = [NSString stringWithFormat:@"%d",(int)(([model.feed_comment_count integerValue])?([model.feed_comment_count integerValue]+1):1)];
                }
                
            }
            else if ([[[notification valueForKey:@"userInfo"] valueForKey:@"activity"] isEqualToString:EDITCOMMENT])  {
                for (int i=0; i<[model.commentsArray count]; i++) {
                    if ([[[model.commentsArray objectAtIndex:i] achieveCommentId]isEqualToString:commentObject.achieveCommentId]) {
                        [model.commentsArray replaceObjectAtIndex:i withObject:commentObject];
                    }
                }
            }
            
            NSInteger commentCount = (model.commentsArray.count>3)?3:model.commentsArray.count;
            float CommentHeight = 0.0;
            for (int i = 0; i<commentCount; i++)
            {
                NSString *strText = [NSString stringWithFormat:@"%@ %@",[[model.commentsArray objectAtIndex:i] achieveCommentUname],[[model.commentsArray objectAtIndex:i] achieveCommentText]];
                
                CommentHeight += [YSSupport getCommentTextHeight:strText font:12.0];
            }
            model.feed_comment_height       = CommentHeight+5;
            [arrayMintdetailsList replaceObjectAtIndex:i withObject:model];
            [self.tblViewMintList reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:i]] withRowAnimation:UITableViewRowAnimationNone];
            
            break;
        }
        
    }
}

-(void)viewAllComplement : (UIButton *) button {
    NSLog(@"View All Complemet Tag =%d",(int)[button tag]);
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    aDelegate.selectedIndex =  [NSString stringWithFormat:@"%d",(int)[button tag]];
    
    [self gotoComplimintListScreen:button.tag withCommentId:-1];
   
   
}
-(void)gotoComplimintListScreen :(NSInteger)selectedRow withCommentId:(NSInteger)commentId
{
    BaseFeedModel *someModel           = [arrayMintdetailsList objectAtIndex:selectedRow];
    CommentsViewController *comentVc   = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    comentVc.commentCount              = [someModel.feed_comment_count intValue];
    comentVc.achievementdetails        = [arrayMintdetailsList objectAtIndex:selectedRow] ;
    comentVc.feedId                    = someModel.feed_id ;
    
    comentVc.selectdCommentId          =  commentId ;

    [self.navigationController           pushViewController:comentVc animated:YES];
}

-(void)editRemintedSucssesfully:(NSString *)strRemintText;
{
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;

    [self.delegate editRemintDetails:strRemintText rowId:[aDelegate.selectedIndex integerValue]];
    BaseFeedModel *model =  [arrayMintdetailsList objectAtIndex:[aDelegate.selectedIndex integerValue]];
    model.remint_message = strRemintText;
    [arrayMintdetailsList replaceObjectAtIndex:[aDelegate.selectedIndex integerValue] withObject:model];
    [self.tblViewMintList reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:[aDelegate.selectedIndex integerValue]]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)remintedSucssesfully
{
    NSMutableDictionary *dicts = [[NSMutableDictionary alloc]init];
        [dicts setValue: GETVALUE(CEO_AccessToken) forKey:@"access_token"];
        [dicts setValue:([_profileuser_id length])?@"2":@"0" forKey:@"mint_list_type"];
        [dicts setValue:[NSString stringWithFormat:@"%d",1] forKey:@"page_number"];
        [dicts setValue:([_profileuser_id length])?_profileuser_id:@"" forKey:@"profile_user_id"];

        [YSAPICalls getMintsForMyShowTabwithDict:dicts SuccessionBlock:^(id newResponse){
            isLoading = NO;
            Generalmodel *gmodel = newResponse;
            if ([gmodel.status_code integerValue]){
                if([gmodel.tempArray count]>0){
                    
                    if ([[SPHSingletonClass sharedSingletonClass] isUser_mint_streak]) {
                        AppDelegate *obj = MINTSHOWAPPDELEGATE;
                        [obj createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];

                      //  [AppDelegate createStreakPopup:[NSString stringWithFormat:@"%d",(int)[[SPHSingletonClass sharedSingletonClass] login_user_streak]]];
                        [[SPHSingletonClass sharedSingletonClass]
                         setIsUser_mint_streak:NO];
                        
                    }
                    
                    
                 
                    
                    [arrayMintdetailsList removeAllObjects];
                    [arrayMintdetailsList addObjectsFromArray:gmodel.tempArray];
                    [self.tblViewMintList reloadData];
//                        [self.tblViewMintList scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                    [self playVideo];

                    }
            }
        } andFailureBlock:^(NSError *error){
            
            NSLog(@"Failed from the server %@",[error description]);
        }];
}

-(void)mintEditedSuccesFully:(NSNotification *)notification {
    AppDelegate *aDelegate  = (AppDelegate *) [UIApplication sharedApplication].delegate;
    BaseFeedModel *model =  [arrayMintdetailsList objectAtIndex:[aDelegate.selectedIndex integerValue]];
    
    model.feed_description = [[notification valueForKey:@"userInfo"] valueForKey:@"description"];
    model.feed_posted_edited_text = ([model.feed_description length])?@"Edited":@"";
    [arrayMintdetailsList replaceObjectAtIndex:[aDelegate.selectedIndex integerValue] withObject:model];
    [self.tblViewMintList reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:[aDelegate.selectedIndex integerValue]]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)commentTextClickedwithComment:(NSInteger)selectedCommentId feedId:(NSInteger)rowNumber
{

    [self gotoComplimintListScreen:rowNumber withCommentId:selectedCommentId];

}

-(void)hashTagSelected : (NSString *)selectedEnitity{
 
    pagenumbers = 1;
    _isFromTrendingTags = YES;
    _isFromProfile = NO;
    _navigationHeaderText = selectedEnitity;
    [self setUpNavigationHeader];
  //  [self getHashTagMints:selectedEnitity withPageNumber:pagenumbers deleteList:YES];
}

-(void)selectdFeature :(NSString *)selectedEnitity  isPrivateShowroomAccess : (BOOL) access

{
            if (access) {
                showAlert(@"It's a private showroom", nil, @"Ok", nil);
            }
            else
                [self gotoOldShowroom:selectedEnitity];

}


-(void)gotoOldShowroom:(NSString *)selectedEnitity {
   

    MintShowroomViewController *vcShowRoom = [[MintShowroomViewController alloc]initWithNibName:@"MintShowroomViewController" bundle:nil];
    vcShowRoom.selectedShowroom = [selectedEnitity substringFromIndex:1];
    [self.navigationController pushViewController:vcShowRoom animated:YES];
}


-(void)updateForMutedUserMint:(NSString*)currentuserid  {
    
    
    NSMutableIndexSet *indexesToDelete = [NSMutableIndexSet indexSet];
    
     for (int currentIndex = 0;currentIndex<[arrayMintdetailsList count]; currentIndex++){
        //do stuff with obj
         BaseFeedModel *feedlist = [arrayMintdetailsList objectAtIndex:currentIndex];
         if([currentuserid isEqualToString:((feedlist.IsMintReminted)?feedlist.remint_user_id:feedlist.feed_user_id)]){            [indexesToDelete addIndex:currentIndex];
        }
    }

    if ([indexesToDelete count]) {
        
        [arrayMintdetailsList removeObjectsAtIndexes:indexesToDelete];
        [_tblViewMintList reloadData];
    }
    

}
-(void)remintMintOfmintDetails :(NSString *)mintId withUserStatus: (NSString *)userStatus remintCount:(NSString *)count atIndex:(NSInteger)index{
    
    
    
}

-(void)getSignatureShowroomMints:(int)pageNumber deletePreviousList:(BOOL)deleteLst
{
    isLoading = YES;
    
    NSString*  submitData    = [NSString stringWithFormat:@"access_token=%@&showroom_tag=%@&page_number=%d",GETVALUE(CEO_AccessToken),_showtagText,pageNumber] ;
    NSURL *urlLink =  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", ServerUrl,@"customshowroom/get_particular_showroom",submitData]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:urlLink];
    
    
    NSLog(@"submitDta123 data %@",submitData);
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         isLoading = NO;
         
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         
         Generalmodel *gmodel = (Generalmodel *)[SPHSeparteParams SeparateParams:[YSSupport dictionaryFromResponseData:data] methodName:METHOD_MYSHOW_MINT];
         
         if ([gmodel.status_code integerValue])
         {
             if([gmodel.tempArray count]>0)
             {
                 if (deleteLst) {
                     [arrayMintdetailsList removeAllObjects];
                 }
                 isDataFinishedInServer = NO;
                 [arrayMintdetailsList addObjectsFromArray: gmodel.tempArray];
                 [self.tblViewMintList reloadData];
                 [self playVideo];
             }
             else
             {
                 isDataFinishedInServer = YES;
             }
         }
         
     }];
    
}

- (IBAction)btnSettingsTapped:(UIButton *)sender
{
  
    AppDelegate *obj = MINTSHOWAPPDELEGATE;
    [obj createMoreSreen];
      
}

@end




