

#define REMINTEDCOLOR                [UIColor colorWithRed:0./255.0 green:172./255.0 blue:237.0/255. alpha:1.0]


#import "MintSectionHeaderView.h"
#import <QuartzCore/QuartzCore.h>

@implementation MintSectionHeaderView

- (void)awakeFromNib {
    // set the selected image for the disclosure button
    
    self.userImage.layer.cornerRadius = self.userImage.frame.size.width/2;
    self.userImage.layer.masksToBounds = YES;
    self.userImage.clipsToBounds = YES;
    self.viewLightGray.layer.cornerRadius = self.viewLightGray.frame.size.width/2;

    self.circlebtn.layer.cornerRadius = 3.0f;
    self.circlebtn.titleLabel.font = [UIFont systemFontOfSize:10];
    self.circlebtn.layer.borderWidth = 1.5f;
    self.circlebtn.layer.borderColor = [[UIColor colorWithRed:(243/255.0f) green:(94/255.0f) blue:(27/255.0f) alpha:1.0]CGColor];
    self.circlebtn.layer.masksToBounds = YES;
}

//Set UI for the Header if the mintdetails screen is from Trending tags and particular showroom
//No need to show the back button in every where since we have already a back button in Navigatio Bar

-(void)setUIforHeader
{

    _backButton.hidden = YES;
    _imgVBack.hidden = YES;
    
    //Frame set for Userimage (user profile image)
    
    CGRect userImageFrame   = self.userImage.frame;
    userImageFrame.origin.x = 5;
    self.userImage.frame    = userImageFrame;
    self.btnProfileImg.frame    = userImageFrame;

    //Frame set for Username

    CGRect usernameFrame     = self.lblUsername.frame;
    usernameFrame.origin.x   = 50;
    usernameFrame.size.width = CGRectGetWidth(usernameFrame)+18;
    self.lblUsername.frame   = usernameFrame;

    //Frame set for Time and category

    CGRect timCategoryFrame     = self.lblTimeCategory.frame;
    timCategoryFrame.origin.x   = 50;
    timCategoryFrame.size.width = CGRectGetWidth(timCategoryFrame)+18;
    self.lblTimeCategory.frame  = timCategoryFrame;


    CGRect imgLocationFrame     = self.imgVLocation.frame;
    imgLocationFrame.origin.x   = 50;
    self.imgVLocation.frame     = imgLocationFrame;
    
    
    //Frame set for address and location image

    CGRect addressFrame     = self.lblStreakInfo.frame;
    addressFrame.origin.x   = imgLocationFrame.origin.x + CGRectGetWidth(imgLocationFrame)+2;
    addressFrame.size.width = CGRectGetWidth(addressFrame)+18;
    self.lblStreakInfo.frame   = addressFrame;
    
    CGRect viewStreakGray     = self.viewLightGray.frame;
    viewStreakGray.origin.x   = 3;
    self.viewLightGray.frame   = viewStreakGray;
    
    CGRect viewStreak     = self.streakUserProfile.frame;
    viewStreak.origin.x   = 3;
    self.streakUserProfile.frame   = viewStreak;


}
-(void)setUpRemintInfo:(NSString *)remintedUsername remintedUserImage:(NSString *)imageUrl mintPostedTime:(NSString *)time remintedUserId:(NSString *)userId
{
//    self.lblStreakInfo.hidden          = YES;
//    self.streakUserProfile.hidden      = YES;
    
    NSMutableAttributedString * string      = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"ReMint by %@", remintedUsername]];
    [string addAttribute:NSForegroundColorAttributeName value:REMINTEDCOLOR range:NSMakeRange(0,9)];
    
    
    self.lblUsername.attributedText = string;
    self.userImage.mintImageURL         = [NSURL URLWithString:imageUrl];
    self.lblTimeCategory.text        = time;
    self.circlebtn.hidden            = ([GETVALUE(CEO_UserId) isEqualToString:userId])?YES:NO;
    
    
  
}




@end
