//
//  CommentsCell.h
//  Photopinion
//
//  Created by developer on 2/13/13.
//  Copyright (c) 2013 Shiv Mohan Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "YSCommentLabel.h"
#import "STTweetLabel.h"

@interface CommentsCell : SWTableViewCell<UIGestureRecognizerDelegate>

{
IBOutlet UIImageView *imgVLoader;
}

@property(nonatomic ,strong) IBOutlet UILabel *lblTime;
@property(nonatomic ,strong) IBOutlet UIButton *btnDeletecomment;


@property(nonatomic ,strong) IBOutlet UILabel *lblUserName;
@property(nonatomic ,strong) IBOutlet STTweetLabel *lblcomment;
@property(nonatomic ,strong) IBOutlet UIImageView *imageUser;
@property (strong, nonatomic) IBOutlet UILabel *postingLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblDevider;
@property(nonatomic ,strong) IBOutlet UIView *viewLoadMoreComments;

-(void)setGestureForCell;
-(void)setLayer;
-(void)showLoadMore;


@end
