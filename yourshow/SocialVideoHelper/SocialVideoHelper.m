//
//  SocialVideoHelper.m
//

//
#define TEXTTYPECONTENT @"https://api.twitter.com/1.1/statuses/update.json"
#define MEDIATYPECONTENT @"https://api.twitter.com/1.1/statuses/update_with_media.json"
#import "SocialVideoHelper.h"
#import "FriendsInfo.h"

@implementation SocialVideoHelper

+(BOOL)userHasAccessToFacebook
{
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
}

+(BOOL)userHasAccessToTwitter
{
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
}
+(void)uploadFacebookText:(NSData*)myImageData account:(ACAccount*)account withCompletion:(dispatch_block_t)completion
{
        NSDictionary*parameters = @{@"message": @"sharedClass.descriptionText"};

        SLRequest *facebookRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                        requestMethod:SLRequestMethodPOST
                                                                  URL:[NSURL    URLWithString:@"https://graph.facebook.com/me/photos"]
                                                           parameters:parameters];

//        [facebookRequest addMultipartData: myImageData
//                                 withName:@"source"
//                                     type:@"multipart/form-data"
//                                 filename:@"TestImage"];

        facebookRequest.account = account;

        [facebookRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
        {
            // Log the result
            
            NSString *result = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
            
            
            NSLog(@"The result is %@",result);
        }];
    }


+(void)uploadFacebookVideo:(NSData*)videoData account:(ACAccount*)account withCompletion:(dispatch_block_t)completion{
    
    NSURL *facebookPostURL = [[NSURL alloc] initWithString:@"https://graph-video.facebook.com/v2.3/me/videos"];
    
    NSDictionary *postParams = @{
                                 @"access_token": account.credential.oauthToken,
                                 @"upload_phase" : @"start",
                                 @"file_size" : [NSNumber numberWithInteger: videoData.length].stringValue
                                 };
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:facebookPostURL parameters:postParams];
    request.account = account;
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSLog(@"HTTP Response: %li, responseData: %@", (long)[urlResponse statusCode], [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        if (error) {
            NSLog(@"There was an error:%@", [error localizedDescription]);
        } else {
            NSMutableDictionary *returnedData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
            
            NSLog(@"stage1 dic -> %@", returnedData);
            
            NSString *upload_session_id = returnedData[@"upload_session_id"];
            [SocialVideoHelper facebookVideoStage2:videoData upload_session_id:upload_session_id account:account withCompletion:completion];
        }
    }];
    
}

+(void)facebookVideoStage2:(NSData*)videoData upload_session_id:(NSString *)upload_session_id account:(ACAccount*)account withCompletion:(dispatch_block_t)completion{
    
    NSURL *facebookPostURL = [[NSURL alloc] initWithString:@"https://graph-video.facebook.com/v2.3/me/videos"];
    
    NSDictionary *postParams = @{
                                 @"access_token": account.credential.oauthToken,
                                 @"upload_phase" : @"transfer",
                                 @"start_offset" : @"0",
                                 @"upload_session_id" : upload_session_id
                                 };
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:facebookPostURL parameters:postParams];
    request.account = account;
    
    [request addMultipartData:videoData withName:@"video_file_chunk" type:@"video/mp4" filename:@"video"];
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSLog(@"HTTP Response: %li, responseData: %@", (long)[urlResponse statusCode], [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        if (error) {
            NSLog(@"There was an error:%@", [error localizedDescription]);
        } else {
            NSMutableDictionary *returnedData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
            
            NSLog(@"stage2 dic -> %@", returnedData);
            
            [SocialVideoHelper facebookVideoStage3:videoData upload_session_id:upload_session_id account:account withCompletion:completion];
        }
    }];
}


+(void)facebookVideoStage3:(NSData*)videoData upload_session_id:(NSString *)upload_session_id account:(ACAccount*)account withCompletion:(dispatch_block_t)completion{
    
    NSURL *facebookPostURL = [[NSURL alloc] initWithString:@"https://graph-video.facebook.com/v2.3/me/videos"];
    
    NSDictionary *postParams = @{
                                 @"access_token": account.credential.oauthToken,
                                 @"upload_phase" : @"finish",
                                 @"upload_session_id" : upload_session_id
                                 };
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:facebookPostURL parameters:postParams];
    request.account = account;
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSLog(@"HTTP Response: %li, responseData: %@", (long)[urlResponse statusCode], [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        if (error) {
            NSLog(@"There was an error:%@", [error localizedDescription]);
        } else {
            NSMutableDictionary *returnedData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"dic -> %@", returnedData);
            
            if ([urlResponse statusCode] == 200){
                NSLog(@"upload success !");
                DispatchMainThread(^(){completion();});
            }
        }
    }];
}



+(void)uploadTwitterVideo:(NSData*)videoData account:(ACAccount*)account withCompletion:(dispatch_block_t)completion{
    
    NSURL *twitterPostURL = [[NSURL alloc] initWithString:@"https://upload.twitter.com/1.1/media/upload.json"];
    
    NSDictionary *postParams = @{@"command": @"INIT",
                                @"total_bytes" : [NSNumber numberWithInteger: videoData.length].stringValue,
                                @"media_type" : @"video/mp4"
                                };
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:twitterPostURL parameters:postParams];
    request.account = account;
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSLog(@"HTTP Response: %li, responseData: %@", (long)[urlResponse statusCode], [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        if (error) {
            NSLog(@"There was an error:%@", [error localizedDescription]);
        } else {
            NSMutableDictionary *returnedData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
            
            NSString *mediaID = [NSString stringWithFormat:@"%@", [returnedData valueForKey:@"media_id_string"]];
            
            [SocialVideoHelper tweetVideoStage2:videoData mediaID:mediaID account:account withCompletion:completion];
            
            NSLog(@"stage one success, mediaID -> %@", mediaID);
        }
    }];
}

+(void)tweetVideoStage2:(NSData*)videoData mediaID:(NSString *)mediaID account:(ACAccount*)account withCompletion:(dispatch_block_t)completion{
    
    NSURL *twitterPostURL = [[NSURL alloc] initWithString:@"https://upload.twitter.com/1.1/media/upload.json"];
    NSDictionary *postParams = @{@"command": @"APPEND",
                                 @"media_id" : mediaID,
                                 @"segment_index" : @"0",
                                 };
    
    SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:twitterPostURL parameters:postParams];
    postRequest.account = account;
    
    [postRequest addMultipartData:videoData withName:@"media" type:@"video/mp4" filename:@"video"];
    [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSLog(@"Stage2 HTTP Response: %li, %@", (long)[urlResponse statusCode], [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        if (!error) {
            [SocialVideoHelper tweetVideoStage3:videoData mediaID:mediaID account:account withCompletion:completion];
        }
        else {
            NSLog(@"Error stage 2 - %@", error);
        }
    }];
}

+(void)tweetVideoStage3:(NSData*)videoData mediaID:(NSString *)mediaID account:(ACAccount*)account withCompletion:(dispatch_block_t)completion{
    
    NSURL *twitterPostURL = [[NSURL alloc] initWithString:@"https://upload.twitter.com/1.1/media/upload.json"];
    
    NSDictionary *postParams = @{@"command": @"FINALIZE",
                               @"media_id" : mediaID };
    
    SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:twitterPostURL parameters:postParams];
    
    // Set the account and begin the request.
    postRequest.account = account;
    [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSLog(@"Stage3 HTTP Response: %li, %@", (long)[urlResponse statusCode], [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        if (error) {
            NSLog(@"Error stage 3 - %@", error);
        } else {
            [SocialVideoHelper tweetVideoStage4:videoData mediaID:mediaID account:account withCompletion:completion];
        }
    }];
}

+(void)tweetVideoStage4:(NSData*)videoData mediaID:(NSString *)mediaID account:(ACAccount*)account withCompletion:(dispatch_block_t)completion{
    NSURL *twitterPostURL = [[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
     NSString *statusContent = [NSString stringWithFormat:@"#SocialVideoHelper# https://github.com/liu044100/SocialVideoHelper"];

    // Set the parameters for the third twitter video request.
    NSDictionary *postParams = @{@"status": statusContent,
                               @"media_ids" : @[mediaID]};
    
    SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:twitterPostURL parameters:postParams];
    postRequest.account = account;
    [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSLog(@"Stage4 HTTP Response: %li, %@", (long)[urlResponse statusCode], [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        if (error) {
            NSLog(@"Error stage 4 - %@", error);
        } else {
            if ([urlResponse statusCode] == 200){
                NSLog(@"upload success !");
                DispatchMainThread(^(){completion();});
            }
        }
    }];
    
}

+ (void)PostimageOntwitterAccount:(NSString *)linkUrl withImage : (UIImage *)image
{
    
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    
    //hear before posting u can allow user to select the account
    NSArray *arrayOfAccons = [account accountsWithAccountType:accountType];
    for(ACAccount *acc in arrayOfAccons)
    {
        NSLog(@"%@",acc.username); //in this u can get all accounts user names provide some UI for user to select,such as UITableview
    }
    // in below
    
    
    // Request access from the user to access their Twitter account
    [account requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)     {
        if (granted == YES)
        {
            // Populate array with all available Twitter accounts
            NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
            if ([arrayOfAccounts count] > 0)
            {
                
                
                //use the first account available
                ACAccount *acct = [arrayOfAccounts objectAtIndex:0]; //hear this line replace with selected account. than post it :)
                
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:linkUrl]];
                
                
                
                //                 UIImage *image = [self imageReduceSize:[UIScreen mainScreen].bounds.size:imgPost.image];
                //                 NSData *myData = UIImagePNGRepresentation(image);
                //                 [postRequest addMultipartData:myData withName:@"media" type:@"image/png" filename:@"TestImage"];
                //
                
                // NSDictionary *postmessage = @{@"status": message};
                NSDictionary *postmessage = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"Waooo a Mint posted via MisnShow App ! %@",linkUrl],@"status",@"true",@"wrap_links", nil];
               
                NSURL *requestURL = [NSURL
                                     URLWithString:(image!=nil)?MEDIATYPECONTENT:TEXTTYPECONTENT];
                
                //https://api.twitter.com/1.1/statuses/update.json
                //https://api.twitter.com/1.1/statuses/update_with_media.json
                
                //                                  [requestURL addMultipartData:imageData
                //                                                   withName:@"media[]"
                //                                                       type:@"image/jpeg"
                //                                                   filename:@"image.jpg"];
                
                
                
                imageData = nil;
                //                 Build a twitter request
                
                SLRequest *postRequest = [SLRequest
                                          requestForServiceType:SLServiceTypeTwitter
                                          requestMethod:SLRequestMethodPOST
                                          URL:requestURL parameters:postmessage];
                
                             if (image != nil) {
                NSData *myData = UIImagePNGRepresentation(image);
                [postRequest addMultipartData:myData withName:@"media[]" type:@"image/png" filename:@"TestImage"];
                             }
                
                
                
                //                 SLRequest *postRequest = [[SLRequest alloc] initWithURL:
                //                                           [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"]
                //                                                              parameters:[NSDictionary dictionaryWithObject:message forKey:@"status"] requestMethod:TWRequestMethodPOST];//for iOS 7
                
                
                //for iOS 6 use "https://api.twitter.com/1/statuses/update.json"
                //Post the request
                //u should get the response code 200 for successful post
                [postRequest setAccount:acct];
                
                //manage the response
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
                 {
                     if(error)
                     {
                         //if there is an error while posting the tweet
                         //                          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Twitter" message:@"Error in posting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         //                          [alert show];
                        // [self performSelectorOnMainThread:@selector(showAlert:) withObject:@"3" waitUntilDone:NO];
                         
                     }
                     else
                     {
                         // on successful posting the tweet
                         NSLog(@"Twitter response, HTTP response: %i", [urlResponse statusCode]);
                         
                         [self performSelectorOnMainThread:@selector(showAlert) withObject:@"1" waitUntilDone:NO];

                     }
                 }];
                //                 [postRequest release];
            }
            else
            {
                
               // [self performSelectorOnMainThread:@selector(showAlert) withObject:@"1" waitUntilDone:NO];
                
            }
        }
        else
        {
            //suppose user not set any of the accounts
            
//            [self performSelectorOnMainThread:@selector(showAlert:) withObject:@"2" waitUntilDone:NO];
            
            
        }
    } ];
    
    //    [account release];
    //    [widgetsHandler closeWidget:nil];
    
}

+(void)removeAlert:(UIAlertView *) alert{
    
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    
    
}
+(void)showAlert
{
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:nil message:@"Shared successfully..!" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [av show];
    [self performSelector:@selector(removeAlert:) withObject:av afterDelay:0.9];
}

#pragma  Mark - Twitter Invite work
+(void)getTwitterFriendsForSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock
{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error){
        if (granted) {
            NSArray *accounts = [accountStore accountsWithAccountType:accountType];
            // Check if the users has setup at least one Twitter account
            if (accounts.count > 0)
            {
                ACAccount *twitterAccount = [accounts objectAtIndex:0];
                
//                for(ACAccount *t in accounts)
//                {
//                    if([t.username isEqualToString:username])
//                    {
//                        twitterAccount = t;
//                        break;
//                    }
//                }
              
                    NSLog(@"FULLNAME ----> %@",twitterAccount.userFullName);
                
                //in this u can get all accounts user names provide some UI for user to select,such as UITableview
                 SLRequest *twitterInfoRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:[NSURL URLWithString:@"https://api.twitter.com/1.1/followers/list.json?"] parameters:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", twitterAccount.username], @"screen_name", @"-1", @"cursor",@"true",@"skip_status",@"false",@"include_user_entities",nil]];
               // https://api.twitter.com/1.1/followers/list.json?cursor=-1&screen_name=twitterdev&skip_status=true&include_user_entities=false
//                
//                SLRequest *twitterInfoRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:[NSURL URLWithString:@"https://api.twitter.com/1.1/friends/list.json?"] parameters:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", twitterAccount.username], @"screen_name", @"-1", @"cursor",@"true",@"skip_status",@"false",@"include_user_entities", nil]];
                [twitterInfoRequest setAccount:twitterAccount];
                // Making the request
                [twitterInfoRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Check if we reached the reate limit
                        if ([urlResponse statusCode] == 429) {
                            NSLog(@"Rate limit reached");
                            
                            return;
                        }
                        // Check if there was an error
                        if (error) {
                            failureBlock(error);
                            NSLog(@"Error: %@", error.localizedDescription);
                            return;
                        }
                        // Check if there is some response data
                        if (responseData) {
                            NSError *error = nil;
                            NSDictionary *TWData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                            NSLog(@"all info %@", TWData);
                            NSMutableArray *array = [NSMutableArray array];
                            for (NSDictionary *dict in [TWData valueForKey:@"users"]) {
                                
                                FriendsInfo *obj = [[FriendsInfo alloc]initWithFriendsInfo:[dict valueForKey:@"name"]  screenName:[dict valueForKey:@"screen_name"] profileImage:[dict valueForKey:@"profile_image_url"] accountId:[dict valueForKey:@"id_str"]];
                                [array addObject:obj];
                                obj = nil;
                            }
                            
                             successBlock(array);
                            
                        }
                    });
                }];
            }
        } else {
            NSLog(@"No access granted");
        }
    }];


}


+(void)sendPrivateMessage :(NSMutableArray*)arrSelInvitee
{
    
    if (arrSelInvitee.count > 0) {
        //[self addActivity];
        
        // Making the request
        // Request access to the Twitter accounts
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
//                     [self dismiss];
//                     [self showAlertViewWithMessage:NSLocalizedString(ERROR_NO_ACCOUNTS,nil) cancelBtn:NSLocalizedString(@"OK",nil) otherBtn:nil];
                 });
             }
             if (granted)
             {
                 NSArray *accounts = [accountStore accountsWithAccountType:accountType];
                 // Check if the users has setup at least one Twitter account
                 if (accounts.count > 0)
                 {
                     for (int i=0; i<arrSelInvitee.count; i++) {
                         
                         NSDictionary *dict=[arrSelInvitee objectAtIndex:i];
                         NSString *userID=[NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]];
                         
                         ACAccount *twitterAcc = [accounts objectAtIndex:0];
                         // Creating a request to get the info about a user on Twitter
                         NSURL* url = [NSURL URLWithString:@"https://api.twitter.com/1.1/direct_messages/new.json"];
                         
                         //NSDictionary params = @{@"text":@"Hi, I'm using Voizd!",@"user_id":[dict objectForKey:@"id"]};                 //ACAccount twitterAccount = [accountsList lastObject];
                         NSString *strUrl=@"http://mintshowapp.com/m/showroom/showroomdetailview/14709/?page=csuite";
                         //NSString *apiEndpoint = [NSString stringWithFormat:@"http:/api.l.pr/shorten?apikey=axbymc46859i685jfk9fk&longurl=%@",strUrl];
                         NSURL *apiEndpoint = [NSURL URLWithString:[NSString stringWithFormat:@"https://tinyurl.com/api-create.php?url=%@", strUrl]];
                         NSString *shortURL = [NSString stringWithContentsOfURL:apiEndpoint
                                                                       encoding:NSASCIIStringEncoding
                                                                          error:nil];
                         NSLog(@"Long: %@ - Short: %@",url,shortURL);
                         
                         
                         NSDictionary *p =@{@"text":[NSString stringWithFormat:@"%@ wants you to join their Showroom on Mintshow %@ #MintShow @MintShowapp %@",GETVALUE(CEO_UserFirstName),@"FastracNew",shortURL],@"user_id":userID};
                         SLRequest *twitterInfoRequest = [SLRequest  requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:p];
                        
                         [twitterInfoRequest setAccount:twitterAcc];
                         // Making the request
                         [twitterInfoRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                // [self dismiss];
                                 
                                 // Check if we reached the reate limit
                                 if ([urlResponse statusCode] == 429) {
                                     NSLog(@"Rate limit reached");
                                     return;
                                 }
                                 // Check if there was an error
                                 if (error) {
                                     NSLog(@"Error: %@", error.localizedDescription);
                                     return;
                                 }
                                 // Check if there is some response data
                                 if (responseData) {
//                                     NSArray *TWData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:nil];
//                                     NSLog(@"all info %@", TWData);
                                     NSLog(@"Error: %@", error.localizedDescription);

                                     
                                 }
                             });
                         }];
                     }
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{

                     
                     });
                 }
                 
             }
             else {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     

                 });
             }
         }];
        
    }
    
    else{
        
        
    }
    
}

+(void)getFbfriendslist:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock


{
    
    NSDictionary *limitDictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"200",@"limit", nil];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/taggable_friends" parameters:limitDictionary]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             NSMutableArray *friendslist;
             friendslist = [[NSMutableArray alloc]init];
       
      
             for (NSDictionary *dict in [result objectForKey:@"data"]) {
                 FriendsInfo *obj = [[FriendsInfo alloc]initWithFriendsInfo:[dict valueForKey:@"name"] screenName:[dict valueForKey:@"name"] profileImage:[dict valueForKeyPath:@"picture.data.url"] accountId:[dict valueForKey:@"id"]];
                 [friendslist addObject:obj];
             }
             
             successBlock (friendslist);
         }
         else
             failureBlock (error);
     }];
   
}

@end
