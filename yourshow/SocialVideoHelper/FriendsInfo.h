//
//  FriendsInfo.h
//  Your Show
//
//  Created by Pothiraj_BseTec on 15/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendsInfo : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *screen_name;
@property (nonatomic, strong) NSString *profile_image_url;
@property (nonatomic, strong) NSString *id_str;
@property (nonatomic, strong) NSString *selected;

-(id)initWithFriendsInfo : (NSString *)nameData screenName : (NSString *)screenData profileImage : (NSString *)profileImage accountId : (NSString *)idStr;
@end
