//
//  SocialVideoHelper.h


#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

#define DispatchMainThread(block, ...) if(block) dispatch_async(dispatch_get_main_queue(), ^{ block(__VA_ARGS__); })

@interface SocialVideoHelper : NSObject

+(void)uploadTwitterVideo:(NSData*)videoData account:(ACAccount*)account withCompletion:(dispatch_block_t)completion;

+(void)uploadFacebookVideo:(NSData*)videoData account:(ACAccount*)account withCompletion:(dispatch_block_t)completion;
+(void)uploadFacebookText:(NSData*)videoData account:(ACAccount*)account withCompletion:(dispatch_block_t)completion
;
+(BOOL)userHasAccessToFacebook;
+(BOOL)userHasAccessToTwitter;
+ (void)PostimageOntwitterAccount:(NSString *)linkUrl withImage : (UIImage *)image;

//GET FOLLOWERLIST IN TWITTER
//+(void)getTwitterFriends :(NSString *)username;
+(void)sendDirectMessage;
+(void)twitterInvitationWith:(NSString *)twitterId withMessage:(NSString *)message;
+(void)sendPrivateMessage :(NSMutableArray*)arrSelInvitee;
+(void)getTwitterFriendsForSuccessionBlock:(void(^)(id newResponse))successBlock
                           andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getFbfriendslist:(void(^)(id newResponse))successBlock
        andFailureBlock:(void(^)(NSError *error))failureBloc;
@end
