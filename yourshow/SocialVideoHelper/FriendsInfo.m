//
//  FriendsInfo.m
//  Your Show
//
//  Created by Pothiraj_BseTec on 15/12/15.
//  Copyright © 2015 Siba Prasad Hota. All rights reserved.
//

#import "FriendsInfo.h"

@implementation FriendsInfo

-(id)initWithFriendsInfo : (NSString *)nameData screenName : (NSString *)screenData profileImage : (NSString *)profileImage accountId : (NSString *)idStr
{
    self.name = nameData;
    self.screen_name = screenData;
    self.profile_image_url = profileImage;
    self.id_str = idStr;
    self.selected = @"0";
    return self;
}

@end
