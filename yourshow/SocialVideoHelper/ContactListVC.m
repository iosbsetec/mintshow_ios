//
//  ContactListVC.m
//  YACC
//
//  Created by JITENDRA on 6/22/15.
//  Copyright (c) 2015 BseTech. All rights reserved.
//

#import "ContactListVC.h"
#import  <AddressBookUI/AddressBookUI.h>
#import  <AddressBook/AddressBook.h>
#import "MBProgressHUD.h"
#import "Generalmodel.h"
#import "NBPhoneMetaDataGenerator.h"

#import "NBPhoneNumberUtil.h"

#import "NBMetadataHelper.h"
#import "NBPhoneMetaData.h"

#import "NBPhoneNumber.h"
#import "NBPhoneNumberDesc.h"
#import "NBNumberFormat.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>




@interface ContactListVC ()

@property (nonatomic, strong) NSArray *lists;
@property (strong, nonatomic) IBOutlet UITableView *tblViewList;
@property (strong, nonatomic) IBOutlet UIButton *addTapped;

- (IBAction)cancelTapped:(UIButton *)sender;

@end

@implementation ContactListVC
{
  ABAddressBookRef addressBook;
  NSMutableArray*   contactArray;
}

-(void)saveDataToContactList
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                // First time access has been granted, add the contact
                //[self _addContactToAddressBook];
            } else {
                // User denied access
                // Display an alert telling user the contact could not be added
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access, add the contact
        //[self _addContactToAddressBook];
    }
    else {
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
    }
    
    
    
    
    ABRecordRef aRecord = ABPersonCreate();
    CFErrorRef  anError = NULL;
    ABRecordSetValue(aRecord, kABPersonFirstNameProperty,(__bridge CFTypeRef)(txtFldName.text), &anError);
    //    ABRecordSetValue(aRecord, kABPersonLastNameProperty, (__bridge CFTypeRef)(@"kumar"), &anError);
    //   ABRecordSetValue(aRecord, kABPersonPhoneProperty, (__bridge CFTypeRef)(textfieldphnno.text), &anError);
    
    
    ABMutableMultiValueRef multiPhone =     ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)(txtFldPhoneNumber.text), kABPersonPhoneMainLabel, NULL);
    // ABMultiValueAddValueAndLabel(multiPhone, people.other, kABOtherLabel, NULL);
    ABRecordSetValue(aRecord, kABPersonPhoneProperty, multiPhone,nil);
    
    
    if (anError != NULL) {
        
        NSLog(@"error while creating..");
    }
    
    // ABAddressBookRef addressBook;
    CFErrorRef error = NULL;
    addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    BOOL isAdded = ABAddressBookAddRecord (addressBook,aRecord,&error);
    
    if(isAdded){
        
        NSLog(@"added..");
    }
    if (error != NULL) {
        NSLog(@"ABAddressBookAddRecord %@", error);
    }
    error = NULL;
    
    BOOL isSaved = ABAddressBookSave (addressBook,&error);
    
    if(isSaved){
        
        NSLog(@"saved succes fully");
    }
    
    if (error != NULL) {
        NSLog(@"ABAddressBookSave %@", error);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    contactArray = [[NSMutableArray alloc]init];
    _selectedLists = [[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma  mark-Methods for getting Contacts
-(NSMutableArray*)contactsAndGroups
{
    NSString *strCountry=nil;
    NSString *strState=nil;
    NSString *strCity=nil;
    NSString *strStreet=nil;
    NSString *phone3=nil;
    NSString *contactFirst=nil;
    NSString *contactLast=nil;
    
    
    addressBook = ABAddressBookCreateWithOptions(NULL, nil);

    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL)
    {
        // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
//        dispatch_release(sema);
    }
    else {
        // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted)
    {
        
        
        //get all contacts from device contacts list
        CFArrayRef allpeople =  ABAddressBookCopyArrayOfAllPeople(addressBook);
        CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
        
        
        for (int i = 0; i< nPeople; i++)
        {
            NSMutableArray *arrMutAllContacts = [[NSMutableArray alloc]initWithCapacity:0];
            
            //getting contact first name and last name
            ABRecordRef ref = CFArrayGetValueAtIndex(allpeople, i);
            contactFirst = (__bridge NSString *)ABRecordCopyValue(ref, kABPersonFirstNameProperty);
            if ([contactFirst length] == 0) {
                contactFirst = @"";
            }
            contactLast = (__bridge NSString *)ABRecordCopyValue(ref, kABPersonLastNameProperty);
            NSString *company;
            
            if ([contactLast length] == 0) {
                contactLast = @"";
            }
            company = (__bridge NSString *)ABRecordCopyValue(ref, kABPersonOrganizationProperty);
            NSLog(@"Company Name  - %@ ",company);
            if ([company length] == 0) {
                company = @"";
            }
            
//            ABMutableMultiValueRef Address  = ABRecordCopyValue(ref,kABPersonAddressProperty);
//            NSLog(@"%@",Address);
            ABMultiValueRef addressProperty = ABRecordCopyValue(ref, kABPersonAddressProperty);
            NSArray *address = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(addressProperty);
            for (NSDictionary *addressDict in address) {
                NSLog(@"address is %@",addressDict);
                NSString *country = [addressDict objectForKey:@"Country"];
                NSLog(@"country is %@",country);
            }
            CFRelease(addressProperty);
            
            // getting contact phone number
            ABMutableMultiValueRef phno  = ABRecordCopyValue(ref, kABPersonPhoneProperty);
            
            // ABMultiValueRef *phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
            for(CFIndex j = 0; j < ABMultiValueGetCount(phno); j++)
            {
                NSMutableDictionary *dictAllContacts=[[NSMutableDictionary alloc]initWithCapacity:0];
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(phno, j);
                CFStringRef locLabel = ABMultiValueCopyLabelAtIndex(phno, j);
                NSString *phoneLabel =(__bridge NSString*) ABAddressBookCopyLocalizedLabel(locLabel);
                //CFRelease(phones);
                NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
                //  NSString *phn =  (NSString *)ABMultiValueCopyValueAtIndex(phoneNumber, j);
                NSString *phoneNo = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
                NSString *phone1 = [phoneNo stringByReplacingOccurrencesOfString:@"(" withString:@""];
                NSString *phone2 = [phone1 stringByReplacingOccurrencesOfString:@")" withString:@""];
                phone3 = [phone2 stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSLog(@"All phone3=%@",phone3);
                
                if (strCity.length==0) {
                    strCity=@"";
                    
                }
                
                if (strState.length==0) {
                    strState=@"";
                }
                
                if (strCountry.length==0) {
                    strCountry=@"";
                }
                
                if (strStreet.length==0) {
                    strStreet=@"";
                }
                if (contactLast == (NSString *)[NSNull null]) {
                    contactLast=@"";
                }
                
                
                [dictAllContacts setValue:contactFirst      forKey: @"firstName"];
                [dictAllContacts setValue:strState          forKey: @"state"];
                [dictAllContacts setValue:contactLast       forKey:@"lastName"];
                [dictAllContacts setValue:strCity           forKey:@"city"];
                [dictAllContacts setValue:strCountry        forKey:@"country"];
                [dictAllContacts setValue:phone3            forKey:phoneLabel];
                [dictAllContacts setValue:strStreet         forKey:@"Street"];
                [dictAllContacts setValue:company           forKey:@"company"];
                
                NSLog(@"All contacts=%@",arrMutAllContacts);
                CFRelease(phoneNumberRef);
                CFRelease(locLabel);
                NSLog(@"- %@ (%@)", phoneNumber, phoneLabel);
                
                
                //[phoneNumber release];
                
                if ([phoneLabel isEqualToString:@"main"] || [phoneLabel isEqualToString:@"home"] || [phoneLabel isEqualToString:@"work"] || [phoneLabel isEqualToString:@"mobile"] || [phoneLabel isEqualToString:@"other"] || [phoneLabel isEqualToString:@"home fax"] || [phoneLabel isEqualToString:@"iPhone"] )
                {
                    [contactArray addObject:dictAllContacts];
                    strCity = nil ;
                    strCountry = nil;
                    
                }
                
                
                
                
            }
            
            arrMutAllContacts=nil;
            
            if(ABMultiValueGetCount(phno) > 0)
            {
                
                NSLog(@"phone3 is %@",phone3);
                
                
            }
            CFRelease(phno);
            
            
            //getting contact E-Mail ID
            //    ABMutableMultiValueRef eMail  = ABRecordCopyValue(ref, kABPersonEmailProperty);
            //
            //    if(ABMultiValueGetCount(eMail) > 0)
            //    {
            //       NSString *email =  (NSString *)ABMultiValueCopyValueAtIndex(eMail, 0);
            //
            //    }
            //        CFRelease(eMail);
            
            
            //getting all  contact states
            
            
            
            
            
            // getting contact image
            if(ABPersonHasImageData(ref))
            {
                //  tempObj.image = [UIImage imageWithData:(NSData *)ABPersonCopyImageData(ref)];
            }
            else
            {
                //  tempObj.image = [UIImage imageNamed:@"add_photo.png"];
            }
            
            
        }
        
        NSSortDescriptor *aSortDescriptor   =   [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        [contactArray sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor] ];
        
        CFRelease(allpeople);
        
    }
    
    return  contactArray;
    
}

#pragma mark - Server calls

- (void)getListsFromServer
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_queue_t callService = dispatch_queue_create("callService", nil);
    dispatch_async(callService, ^{
        
        
           [self  contactsAndGroups];
//        HttpResponse* response = [ListsCollection listsWithAccessToken:[CTCTGlobal shared].token andModificationDate:nil];
//        
//        if(response.statusCode != 200)
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:((HttpError*)[response.errors objectAtIndex:0]).message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alert show];
//        }
//        else
//        {
//            self.lists = response.data;
//        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_tblViewList reloadData];
            [MBProgressHUD  hideHUDForView:self.view animated:YES];
            
        });
    });
    //dispatch_release(callService);
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getListsFromServer];
}
#pragma mark - UITableView delegate and DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return contactArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    NSString *strLastName=[[contactArray objectAtIndex:indexPath.row] objectForKey:@"lastName"];
    
    NSLog(@"last name is %@",strLastName);
    
    NSString *fullName=[NSString stringWithFormat:@"%@",[[[contactArray objectAtIndex:indexPath.row] objectForKey:@"firstName"] stringByAppendingFormat:@"%@",strLastName]];
    NSLog(@"fullName  is %@",fullName);

    cell.textLabel.text=[[[contactArray objectAtIndex:indexPath.row] objectForKey:@"firstName"] stringByAppendingFormat:@" %@",strLastName.length>0?strLastName:@""];

    NSString *phonenumber = nil;
    
    if ([fullName length] == 0) {
        
        if ([[[contactArray objectAtIndex:indexPath.row] objectForKey:@"company"] length] > 0) {
            cell.detailTextLabel .text = [ NSString stringWithFormat:@"%@-%@ ",[[contactArray objectAtIndex:indexPath.row] objectForKey:@"company"],@"company" ];
            phonenumber = [[contactArray objectAtIndex:indexPath.row] objectForKey:@"company"];
        }
        
        
        
    }
    if ([[[contactArray objectAtIndex:indexPath.row] allKeys]containsObject:@"main"]) {
        cell.detailTextLabel .text = [ NSString stringWithFormat:@"%@-%@ ",[[contactArray objectAtIndex:indexPath.row] objectForKey:@"main"],@"main" ];
        phonenumber = [[contactArray objectAtIndex:indexPath.row] objectForKey:@"main"];

      
    }
    else if ([[[contactArray objectAtIndex:indexPath.row] allKeys]containsObject:@"work"]) {
        cell.detailTextLabel .text = [ NSString stringWithFormat:@"%@-%@ ",[[contactArray objectAtIndex:indexPath.row] objectForKey:@"work"],@"work" ];
        phonenumber = [[contactArray objectAtIndex:indexPath.row] objectForKey:@"work"];

          }
    else if ([[[contactArray objectAtIndex:indexPath.row] allKeys]containsObject:@"home"]) {
        cell.detailTextLabel .text = [ NSString stringWithFormat:@"%@-%@ ",[[contactArray objectAtIndex:indexPath.row] objectForKey:@"home"],@"home" ];
        phonenumber = [[contactArray objectAtIndex:indexPath.row] objectForKey:@"home"];


          }
    else if ([[[contactArray objectAtIndex:indexPath.row] allKeys]containsObject:@"mobile"]) {
        cell.detailTextLabel .text = [ NSString stringWithFormat:@"%@-%@",[[contactArray objectAtIndex:indexPath.row] objectForKey:@"mobile"],@"mobile" ];
        phonenumber = [[contactArray objectAtIndex:indexPath.row] objectForKey:@"mobile"];


          }
    else if ([[[contactArray objectAtIndex:indexPath.row] allKeys]containsObject:@"other"]) {
        cell.detailTextLabel .text = [ NSString stringWithFormat:@"%@-%@ ",[[contactArray objectAtIndex:indexPath.row] objectForKey:@"other"],@"other" ];
        phonenumber = [[contactArray objectAtIndex:indexPath.row] objectForKey:@"other"];


    }
    else if ([[[contactArray objectAtIndex:indexPath.row] allKeys]containsObject:@"home fax"]) {
        
        cell.detailTextLabel .text = [ NSString stringWithFormat:@"%@-%@ ",[[contactArray objectAtIndex:indexPath.row] objectForKey:@"home fax"],@"home fax" ];
        phonenumber = [[contactArray objectAtIndex:indexPath.row] objectForKey:@"home fax"];

    }
    
    else if ([[[contactArray objectAtIndex:indexPath.row] allKeys]containsObject:@"iPhone"]) {
        cell.detailTextLabel .text = [ NSString stringWithFormat:@"%@-%@ ",[[contactArray objectAtIndex:indexPath.row] objectForKey:@"iPhone"],@"iPhone" ];
        phonenumber = [[contactArray objectAtIndex:indexPath.row] objectForKey:@"iPhone"];


    }
    if ([self existsListWithId:phonenumber])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    UITapGestureRecognizer *tapGuesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cellSelected:)];
    tapGuesture.numberOfTapsRequired = 1;
    [cell.contentView addGestureRecognizer:tapGuesture];
    cell.contentView.tag =indexPath.row;

    return cell;
}


-(void)cellSelected : (id)sender {
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *) sender;
    NSLog(@"Tag = %d", (int)gesture.view.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:gesture.view.tag inSection:0];
    
    UITableViewCell *cell = [_tblViewList cellForRowAtIndexPath:indexPath];
    NSArray *items = [cell.detailTextLabel.text componentsSeparatedByString:@"-"];
    
    
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        // ContactList *list = [self.lists objectAtIndex:indexPath.row];
        [self addList:[items objectAtIndex:0]];
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self removeListWithId:[items objectAtIndex:0]];
    }
    
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    CTTelephonyNetworkInfo *network_Info = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = network_Info.subscriberCellularProvider;
    
    NSLog(@"country code is: %@", carrier.mobileCountryCode);
    NSLog(@"isoCountryCode code is: %@", carrier.isoCountryCode);
    NSString *countryCode =carrier.isoCountryCode;
    
    if (!countryCode) {
        NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
        countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
        
    }
    NSString *vietnamString = [self.selectedLists objectAtIndex:0];
    NBPhoneNumber *phoneNumberUS = [phoneUtil parse:vietnamString defaultRegion:countryCode error:nil];
    NSLog(@"- isValidNumber [%@]", [phoneUtil isValidNumber:phoneNumberUS] ? @"YES" : @"NO");
    NSLog(@"- isPossibleNumber [%@]", [phoneUtil isPossibleNumber:phoneNumberUS error:nil] ? @"YES" : @"NO");
    NSLog(@"- getRegionCodeForNumber [%@]", [phoneUtil getRegionCodeForNumber:phoneNumberUS]);
    NSError *error = nil;
    NBPhoneNumber *vietnamNumber = [[NBPhoneNumberUtil sharedInstance] parse:vietnamString defaultRegion:countryCode error:&error];
    if (error) {
        NSLog(@"err [%@]", [error localizedDescription]);
    }
    NSLog(@"getRegionCodeForNumber [%@]", [[NBPhoneNumberUtil sharedInstance] getRegionCodeForNumber:vietnamNumber]);
    // Do any additional setup after loading the view, typically from a nib.

    [_tblViewList deselectRowAtIndexPath:indexPath animated:YES];



}
- (IBAction)cancelTapped:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (IBAction)cancelAddContact:(UIButton *)sender {
    [viewAddContact removeFromSuperview];
    
}


-(void)convertToCountryCode
{
    
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    CTTelephonyNetworkInfo *network_Info = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = network_Info.subscriberCellularProvider;
    
    NSLog(@"country code is: %@", carrier.mobileCountryCode);
    NSLog(@"isoCountryCode code is: %@", carrier.isoCountryCode);
    NSString *countryCode =carrier.isoCountryCode;
    
    if (!countryCode) {
        NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
        countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
        
    }
    NSString *vietnamString = [self.selectedLists objectAtIndex:0];
    NBPhoneNumber *phoneNumberUS = [phoneUtil parse:vietnamString defaultRegion:countryCode error:nil];
    NSLog(@"- isValidNumber [%@]", [phoneUtil isValidNumber:phoneNumberUS] ? @"YES" : @"NO");
    NSLog(@"- isPossibleNumber [%@]", [phoneUtil isPossibleNumber:phoneNumberUS error:nil] ? @"YES" : @"NO");
    NSLog(@"- getRegionCodeForNumber [%@]", [phoneUtil getRegionCodeForNumber:phoneNumberUS]);
    NSError *error = nil;
    NBPhoneNumber *vietnamNumber = [[NBPhoneNumberUtil sharedInstance] parse:vietnamString defaultRegion:countryCode error:&error];
    if (error) {
        NSLog(@"err [%@]", [error localizedDescription]);
    }
    NSLog(@"getRegionCodeForNumber [%@]", [[NBPhoneNumberUtil sharedInstance] getRegionCodeForNumber:vietnamNumber]);
    // Do any additional setup after loading the view, typically from a nib.

}
#pragma mark - Utilities

- (void)removeListWithId:(NSString*)listId
{
    for (int i = 0; i < self.selectedLists.count; i++)
    {
        
        if ([[self.selectedLists objectAtIndex:i] isEqualToString:listId])
        {
            [self.selectedLists removeObjectAtIndex:i];
            break;
        }
    }
}

- (void)addList : (NSString *)number
{
    BOOL exists = NO;
    
    for (int i = 0; i < self.selectedLists.count; i++)
    {
        
        if ([number isEqualToString:[self.selectedLists objectAtIndex:i]])
        {
            exists = YES;
        }
    }
    
    if (!exists)
    {
        [self.selectedLists addObject:number];
    }
}

- (BOOL)existsListWithId:(NSString*)number
{
    BOOL exists = NO;
    
    for (int i = 0; i < self.selectedLists.count; i++)
    {
        if ([[self.selectedLists objectAtIndex:i] isEqualToString:number])
        {
            exists = YES;
            break;
        }
    }
    
    return exists;
}



//-(void)serVerCallForSendingMessage
//{
//    NSString * result = [_selectedLists componentsJoinedByString:@","];;
//
//    NSLog(@"textfield value is %@",txtFldPhoneNumber.text);
//    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
//    [f setNumberStyle:NSNumberFormatterDecimalStyle];
//    NSNumber * myNumber = [f numberFromString:@"2394739287439284734723"];
//    NSLog(@"longlong: %llu", [myNumber longLongValue]);
//    
//    NSData *submitData = [[NSString stringWithFormat:@"to_sms=%llu&rsp_id=%llu",
//                           [result longLongValue],
//                           [GETVALUE(CEO_ReferalID) longLongValue]
//                          ] dataUsingEncoding:NSUTF8StringEncoding];
//    
//    NSString *request = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
//    
//    NSLog(@"request is %@",request);
//
//SPHServiceRequest *req= [[SPHServiceRequest alloc]init];
//[req PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_SEND_MESSAGE link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,SENDMESSAGE]] withSuccessionBlock:^(id response)
// {
//     [self dismissViewControllerAnimated:YES completion:^{
//         
//     }];
//     Generalmodel *model = response;
//     if ([model.status_code intValue]==1)
//     {
//        
//         NSLog(@" response ids %@",model.status_Msg);
//         NSLog(@" response ids %@",model.status);
//         [[CommonClass getSharedInstance] alertMsg:model.status_Msg title:@"Success"];
//
//         
//     }
//     
//     else
//     {
//         [[CommonClass getSharedInstance] alertMsg:model.status_Msg title:@"Alert"];
//     }
// } andFailureBlock:^(NSError *error) {
//     
//     
//     
//     
// }];
//}



@end

