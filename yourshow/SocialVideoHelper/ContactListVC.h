//
//  ContactListVC.h
//  YACC
//
//  Created by JITENDRA on 6/22/15.
//  Copyright (c) 2015 BseTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactListVC : UIViewController
{
    IBOutlet UIView *viewAddContact;
    IBOutlet UITextField *txtFldName;
    IBOutlet UITextField *txtFldPhoneNumber;
}
@property (nonatomic, strong) NSMutableArray *selectedLists;
- (IBAction)addTapped: (UIButton *)sender;
- (IBAction)cancelAddContact:(UIButton *)sender;
- (IBAction)sendMessage:(UIButton *)sender;

@end
