
#import "SPHSingletonClass.h"
static SPHSingletonClass *sharedClass = nil;

@implementation SPHSingletonClass
@synthesize arrGlobalSignatureShowroomList;

+ (id)sharedSingletonClass
{
    static dispatch_once_t onceToken;//The way we ensure that it’s only created once is by using the dispatch_once method from Grand Central Dispatch (GCD).
    dispatch_once(&onceToken, ^{
        sharedClass = [[self alloc] init];
    });
    return sharedClass;
}
+ (void)resetSharedInstance {
    sharedClass = nil;
}
- (id)init
{
    if (self = [super init])
    {
        self.counterText=0;
        self.glblActivityRequest_count=@"0";
        self.glblActivityTotal_count=@"0";
        self.glblActivityYou_count=@"0";
        arrGlobalSignatureShowroomList = [[NSMutableArray alloc] init];

        //init instance variable
    }
    return self;
}
@end

